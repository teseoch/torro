/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/Edge.hpp"
#include "geo/Capsule.hpp"
#include "math/Matrix33.hpp"
#include "math/Matrix22.hpp"
#include "math/Vector3.hpp"
#include "math/Vector2.hpp"
#include "geo/Triangle.hpp"
#include "math/Random.hpp"
#include "geo/Barycentric.hpp"

namespace tsl{

#define ROOT_TOL 1e-9
  /**************************************************************************/
  template<class T>
  Edge<T>::Edge(){
    convex = false;

    faceIds[0] = faceIds[1] = id = -1;
  }

  /**************************************************************************/
  template<class T>
  Edge<T>::Edge(const Vector4<T>& a, const Vector4<T>& b,
                const Vector4<T>& c, const Vector4<T>& d){
    v[0]  = a;
    v[1]  = b;

    fv[0] = c;
    fv[1] = d;

    computeNormalAndTangent();

    faceVertexIds[0] = faceVertexIds[1] = faceIds[0] = faceIds[1] = id = -1;
  }

  /**************************************************************************/
  template<class T>
  Edge<T>::Edge(const Edge<T>& edge){
    *this = edge;
  }

  /**************************************************************************/
  template<class T>
  Edge<T>& Edge<T>::operator=(const Edge<T>& edge){
    if(this != &edge){
      v[0] = edge.v[0];
      v[1] = edge.v[1];

      n[0] = edge.n[0];
      n[1] = edge.n[1];

      t[0] = edge.t[0];
      t[1] = edge.t[1];

      fv[0] = edge.fv[0];
      fv[1] = edge.fv[1];

      convex = edge.convex;

      validFaces[0] = edge.validFaces[0];
      validFaces[1] = edge.validFaces[1];

      faceArea[0] = edge.faceArea[0];
      faceArea[1] = edge.faceArea[1];

      tangentDistance[0] = edge.tangentDistance[0];
      tangentDistance[1] = edge.tangentDistance[1];

      faceIds[0] = edge.faceIds[0];
      faceIds[1] = edge.faceIds[1];

      id = edge.id;

      faceVertexIds[0] = edge.faceVertexIds[0];
      faceVertexIds[1] = edge.faceVertexIds[1];
    }
    return *this;
  }

  /**************************************************************************/
  template<class T>
  void Edge<T>::computeNormalAndTangent(){
    Vector4<T> v1v0 = v[1]  - v[0];
    Vector4<T> v2v0 = fv[0] - v[0];

    n[0] = cross(v1v0, v2v0);

    faceArea[0] = n[0].length()/(T)2.0;

    v1v0 = v[0]  - v[1];
    v2v0 = fv[1] - v[1];

    n[1] = cross(v1v0, v2v0);

    faceArea[1] = n[1].length()/(T)2.0;

    n[0].normalize();
    n[1].normalize();

    Vector4<T> fv0v0 = fv[0] - v[0];
    Vector4<T> fv1v0 = fv[1] - v[0];

    Vector4<T> ee    = v[1]  - v[0];

    if(ee.length() < 1e-9){
      std::cerr << *this << std::endl;
      error("Too small edge!!");
    }

    if(fv0v0.length() < 1e-9){
      std::cerr << *this << std::endl;
      error("Too small triangle adjacent to edge!!");
    }

    if(fv1v0.length() < 1e-9){
      std::cerr << *this << std::endl;
      error("Too small triangle adjacent to edge!!");
    }

    t[0] = cross(cross(ee, fv0v0), ee).normalized();
    t[1] = cross(cross(ee, fv1v0), ee).normalized();

    tangentDistance[0] = dot(t[0], fv0v0);
    tangentDistance[1] = dot(t[1], fv1v0);

    if(Abs(tangentDistance[0]) > 1e-6){
      validFaces[0] = true;
    }else{
      validFaces[0] = false;
    }

    if(Abs(tangentDistance[1]) > 1e-6){
      validFaces[1] = true;
    }else{
      validFaces[1] = false;
    }

    /*Compute convexity*/
    {
      T dtn1 = dot(t[0], n[1]);
      T dtn2 = dot(t[1], n[0]);

      /*An edge is convex if t[0].n[1] < 0 && t[1].n[0] < 0*/
      if(validFaces[0] && validFaces[1]){
        if(dtn1 <= -(T)1e-9*0 && dtn2 <= -(T)1e-9*0){
          convex = true;
        }else{
          if(Sign(dtn1) != Sign(dtn2)){
            if(Abs(dtn1 + dtn2) > 1e-10){
              std::cout << *this << std::endl;
              warning("%10.10e, %10.10e", dtn1, dtn2);
              warning("Indeterminate flat");
            }
          }
          convex = false;
        }
      }else{
        /*If one of both faces is valid, then the face isconvex by
          definition, otherwise it will not be considered during
          collision checks.*/
        if(validFaces[0]){
          convex = true;
        }else if(validFaces[1]){
          convex = true;
        }else if(!validFaces[0] && !validFaces[1]){
          /*faces of edge are singular*/
          convex = false;
        }
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void Edge<T>::set(const Vector4<T>& a, const Vector4<T>& b,
                    const Vector4<T>& c, const Vector4<T>& d){
    v[0]  = a;
    v[1]  = b;
    fv[0] = c;
    fv[1] = d;

    computeNormalAndTangent();
  }

  /**************************************************************************/
  /*Tests is the edge is convex*/
  template<class T>
  bool Edge<T>::isConvex()const{
    return convex;
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::isConcave()const{
    return !convex;
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::isIndeterminate()const{
    T area1 = getFaceArea(0);
    T area2 = getFaceArea(1);

    if(area1 < 1e-10 && area2 < 1e-10){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  T Edge<T>::getFaceArea(int f)const{
    return faceArea[f];
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::pointOutside(const Vector4<T>& p)const{
    Vector4<T> da = p - v[0];
    Vector4<T> db = p - v[1];

    if(dot(da, n[0]) >= 0.0){
      return true;
    }
    if(dot(da, n[1]) >= 0.0){
      return true;
    }
    if(dot(db, n[0]) >= 0.0){
      return true;
    }
    if(dot(db, n[1]) >= 0.0){
      return true;
    }
    return false;
  }

#define DET_EPS 1e-12*0

  /**************************************************************************/
  template<class T>
  bool Edge<T>::faceEdgeIntersection(const Edge<T>& edge,
                                     int faceId,
                                     Vector4<T>& intersection,
                                     bool& parallel,
                                     bool& degenerate,
                                     T eps,
                                     bool debug)const{
    Vector4<T> dir = edge.v[1] - edge.v[0];
    Vector4<T> s = edge.v[0];

    parallel = false;

    if(faceId < 2){
      if(!validFaces[faceId]){
        //degenerate = true;
        //return false;
      }
    }else if(faceId == 2){
      if(!validFaces[0] && !validFaces[1]){
        //degenerate = true;
        //return false;
      }
    }

    degenerate = false;

    /*And displace the faces in the normal direction with distance
      eps*/

    //Vector4<T> dirN = dir.normalized();

    /*
    if(faceId == 2){
      if(Abs(dot(dirN, t[0])) < 1e-8 &&
         Abs(dot(dirN, t[1])) < 1e-8 ){
        parallel = true;
        return false;
      }
    }else{
      if(Abs(dot(dirN, n[faceId])) < 1e-8){
        parallel = true;
        return false;
      }
    }
    */

    Matrix44<T> faceVertices;
    if(faceId == 0){
      faceVertices[0] = v[0]  + eps * n[0];
      faceVertices[1] = v[1]  + eps * n[0];
      faceVertices[2] = fv[0] + eps * n[0];

    }else if(faceId == 1){
      faceVertices[0] = v[1]  + eps * n[1];
      faceVertices[1] = v[0]  + eps * n[1];
      faceVertices[2] = fv[1] + eps * n[1];

    }else if(faceId == 2){
      if(isConvex()){
        if(validFaces[0] && validFaces[1]){
          Vector4<T> nn = (n[0] + n[1]).normalized();
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - nn;

        }else if(validFaces[0]){
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - n[0];

        }else if(validFaces[1]){
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - n[1];

        }else{
          error("both faces are invalid");
        }
      }else{
        /*Concave*/
        if(validFaces[0] && validFaces[1]){
          Vector4<T> nn = (n[0] + n[1]).normalized();
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - nn;

        }else if(validFaces[0]){
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - n[0];

        }else if(validFaces[1]){
          faceVertices[0] = v[0];
          faceVertices[1] = v[1];
          faceVertices[2] = (T)0.5*(v[0] + v[1]) - n[1];

        }else{
          error("both faces are invalid");
        }
      }
    }else{
      error("Wrong faceId provided");
    }

    Matrix33<T> A;
    A.m[0] = faceVertices[1][0] - faceVertices[0][0];
    A.m[1] = faceVertices[2][0] - faceVertices[0][0];
    A.m[2] = -dir[0];

    A.m[3] = faceVertices[1][1] - faceVertices[0][1];
    A.m[4] = faceVertices[2][1] - faceVertices[0][1];
    A.m[5] = -dir[1];

    A.m[6] = faceVertices[1][2] - faceVertices[0][2];
    A.m[7] = faceVertices[2][2] - faceVertices[0][2];
    A.m[8] = -dir[2];

    Vector3<T> b(s[0]- faceVertices[0][0],
                 s[1]- faceVertices[0][1],
                 s[2]- faceVertices[0][2]);

    T det = 0;
    Vector3<T> x = A.inverse(&det) * b;

    if(Abs(det) <= DET_EPS){
      parallel = true;
      /*Edge is parallel to face*/
      return false;
    }else{
      Vector4<T> bb((T)1.0 - x.m[0] - x.m[1], x.m[0], x.m[1], 0);

      if(debug){
        message("bary, x ");
        std::cout << bb << std::endl;
        std::cout << x << std::endl;
      }

      intersection =
        faceVertices[0] +
        x.m[0] * (faceVertices[1] - faceVertices[0]) +
        x.m[1] * (faceVertices[2] - faceVertices[0]);

      if(x.m[1] >= 0.0){
        return true;
      }else{
        /*Intersection on other side of edge.*/
        return false;
      }

      return false;
    }
    return false;
  }

  /**************************************************************************/
  /*Two way edge test. A->B && B->A*/
  template<class T>
  bool Edge<T>::edgesIntersect(const Edge<T>& edge,
                               T eps,
                               bool forced,
                               bool debug)const{
    /*First check that at least one pair of faces should face each
      other*/
    bool p1 = dot(n[0], edge.n[0]) <= 0.0;
    bool p2 = dot(n[0], edge.n[1]) <= 0.0;
    bool p3 = dot(n[1], edge.n[0]) <= 0.0;
    bool p4 = dot(n[1], edge.n[1]) <= 0.0;

    if(debug){
      message("dot1 = %10.10e", dot(n[0], edge.n[0]));
      message("dot2 = %10.10e", dot(n[0], edge.n[1]));
      message("dot3 = %10.10e", dot(n[1], edge.n[0]));
      message("dot4 = %10.10e", dot(n[1], edge.n[1]));
    }

    if(p1 || p2 || p3 || p4){
      //ok
    }else{
      return false;
    }

    bool i1 = edgeIntersectsWith(edge, eps, forced, debug);
    bool i2 = edge.edgeIntersectsWith(*this, eps, forced, debug);

    if(debug){
      message("intersection 1 = %d", i1);
      message("intersection 2 = %d", i2);
    }

    return i1 && i2;
  }

  /**************************************************************************/
  /*One way test for intersection.

    This edge intersects with the provided edge iff the provided edge
    intersects both faces described by this edge. If this edge is
    concave, an intersection occurs only if the other edge is
    completely behind both faces.

    When used in a collision detection system, the edge is connected
    to other edges through vertices. In case of a concave edge, other
    edges and/or vertices generate collisions before collisions with
    concave edges are detected. The collisions found before, also
    resolve theoretical collision with the concave edge. Therefore
    these cases are discarded here. */
  template<class T>
  bool Edge<T>::edgeIntersectsWith(const Edge<T>& edge,
                                   T eps,
                                   bool forced,
                                   bool debug)const{
    Vector4<T> cc;
    bool degc = false;
    bool parallelC = false;

    bool intersectionC =
      faceEdgeIntersection(edge, 2, cc, parallelC, degc, eps, debug);

    if(degc || parallelC){
      return false;
    }

    if(isConvex() /*|| (isConcave() && forced)*/){
      if(intersectionC){
        return true;
      }else{
        /*No intersection with any face, check if it is in the cylinder*/
        if(eps == 0.0){
          return false;
        }
        /*No intersection with faces, check if the other edge
          intersects the cylinder defined by this edge (epsilon). If
          the edge does intersect the cylinder, the edges are
          separated less than eps from each other, which is
          considdered to be an intersection.*/

        if(eps < 0.0){
          Capsule<T> capsule(eps, v[0], v[1]);
          Vector4<T> pa, pb;
          /*Negative epsilon, so if there is an intersection with
            the capsule, there is no intersection with the edge*/
          return !capsule.intersectionWithLine(edge.v[0], edge.v[1], &pa, &pb);
        }else{
          Capsule<T> capsule(eps, v[0], v[1]);
          Vector4<T> pa, pb;

          return capsule.intersectionWithLine(edge.v[0], edge.v[1], &pa, &pb);
        }
      }
      return false;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Edge<T>::getProjection(const Vector4<T>& p)const{
    return getCoordinates(getBarycentricCoordinates(p));
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Edge<T>::getBarycentricCoordinates(const Vector4<T>& p)const{
    Vector4<T> v0 = v[1] - v[0];
    Vector4<T> v1 = p    - v[0];

    return getBarycentricCoordinatesEdge(v0, v1);
  }

  /**************************************************************************/
  template<class T>
  T Edge<T>::getLength()const{
    return (v[0] - v[1]).length();
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Edge<T>::getCoordinates(const Vector4<T>& bary)const{
    return v[0] * bary[0] + v[1] * bary[1];
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Edge<T>::clampWeights(const Vector4<T>& bary, T mn, T mx){
    T b0 = Clamp(bary[1], mn, mx);
    T r  = Clamp((T)1.0 - b0, mn, mx);
    return Vector4<T>(r, b0, 0, 0);
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::barycentricOnEdge(const Vector4<T>& bary, T eps)const{
    if((bary[0] >= -eps) && (bary[1] >= -eps) &&
       (bary[0] <= (T)1.0+eps) && (bary[1] <= (T)1.0+eps)){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::barycentricOnEdgeBand(const Vector4<T>& bary, T eps)const{
    return barycentricOnEdgeDist(bary, eps);
    if(barycentricOnEdge(bary, (T)0.0)){
      return true;
    }else{
      Vector4<T> clamped = clampWeights(bary, (T)0.0, (T)1.0);

      Vector4<T> p = getCoordinates(bary);
      Vector4<T> pClamped = getCoordinates(clamped);

      if((pClamped - p).length() < eps){
        return true;
      }
      return false;
    }
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::barycentricOnEdgeDist(const Vector4<T>& bary, T dist)const{
    if(barycentricOnEdge(bary, (T)0.0)){
      /*On edge*/
      return true;
    }else{
      Vector4<T> p = getCoordinates(bary);

      T d1 = (p - v[0]).length();
      T d2 = (p - v[1]).length();

      T d = Min(d1, d2);

      if(d < dist){
        return true;
      }
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  bool Edge<T>::pointProjectsOnEdge(const Vector4<T>& p, T eps)const{
    Vector4<T> bary = getBarycentricCoordinates(p);
    return barycentricOnEdge(bary, eps);
  }

  /**************************************************************************/
  /*Computes the distance of p from a plane defined by n, such that
    this edge lies in that plane*/
  template<class T>
  T Edge<T>::getPlaneDistance(const Vector4<T>& p, const Vector4<T>& n)const{
    Vector4<T> A, B;
    if((p-v[0]).length() < (p-v[1]).length()){
      A = v[0];
      B = v[1];
    }else{
      A = v[1];
      B = v[0];
    }

    /*Least-Square approximation for p = A + (B-A)d*/

    Vector4<T> BA = B - A;
    Vector4<T> PA = p - A;

    double BABA  = (double)dot(BA, BA);
    double BAPA  = (double)dot(BA, PA);

    double d = BAPA / BABA;

    Vector4<T> C = A + BA*(T)d;

    Vector4<T> dir = p - C;

    return dot(dir, n);
  }

  /**************************************************************************/
  /*Gives the contact normal from this edge to the other. If a
    reference vector is provided, the normal is aligned with the
    reference.*/
  template<class T>
  Vector4<T> Edge<T>::getOutwardNormal(const Edge<T>& edge,
                                       Vector4<T>* reference,
                                       int recursiondepth,
                                       bool* potentialError)const{
    Vector4<T> p1, p2;
    bool parallel = false;

    /*Compute projections of each edge onto the other edge. If the
      edges are parallel, the projections are not unique.*/
    infiniteProjections(edge, &p1, &p2, 0, 0, &parallel);

    if(parallel){
      /*The edges are parallel, try to compute a normal vector from
        this edge to the other.*/

      Vector4<T> e   = v[1] - v[0];
      Vector4<T> mid = (T)0.5*(edge.v[0] + edge.v[1]);
      Vector4<T> ee  = mid - v[0];
      Vector4<T> cr  = cross(e, ee);
      Vector4<T> normal = cross(e, cr).normalized();

      if(dot(normal, ee) < 0.0){
        normal *= -(T)1.0;
      }

      /*Correct with reference*/
      if(reference){
        if(dot(*reference, normal) < 0.0){
          normal *= -(T)1.0;
        }
      }

      return normal;
    }else{
      /*Edges are not parallel*/
      if(reference){
        /*Reference provided, use cross product and correct sign
          afterwards*/
        Vector4<T> v0 = (     v[1] -      v[0]).normalized();
        Vector4<T> v1 = (edge.v[1] - edge.v[0]).normalized();
        Vector4<T> normal = cross(v0, v1);

        if(normal.length() < 1e-12){
          /*For some reason, the edges are parallel*/
          std::cout << *this << std::endl;
          std::cout << v0 << std::endl;
          std::cout << v1 << std::endl;
          std::cout << normal << std::endl;
          return *reference;
          error("Does seems to be parallel");
        }

        normal.normalize();

        /*Correct sign using reference*/
        if(dot(*reference, normal) < 0.0){
          normal *= -(T)1.0;
        }
        return normal;
      }else{
        /*No reference and not parallel. Compute normal using
          difference between projections.*/
        Vector4<T> normal = p2 - p1;

        /*If the tolerance in the edge-edge intersection test is say
          1e-8, then if edges are separated less than this distance,
          an intersection could be recorded. If edges do intersect,
          then their signed distance must be negative. If the distance
          at the beginning of the time step is less than this
          tolerance, then one edge may lie above or below one edge. If
          the outward normal is computed then, it might see this as an
          intersection, for which the signed distance should be
          negative. So, in order to be sure that edges are not
          intersecting are separated at least the tolerance, one edge
          needs to be displaced backm given the normals on the
          edge. This works best if the edge is not completely flat, so
          we select the one that has the flattest configuration. Once
          an edge is pushed back, we then try to determine the outward
          normal for non-intersecting pairs. We continue until a good
          configuration is found. This assures that when the outward
          normal is computed for non-intersecting pairs, the normal
          always points in the correct direction.*/

        T rnd = genRand<T>();
        if(rnd > (T)0.5){
          normal *= (T)1.0;
        }

        if(normal.length() < 5e-9){
          /*Edges do nearly intersect with each other, computing a
            normal is not possible*/

          Vector4<T> v0 = (     v[1] -      v[0]).normalized();
          Vector4<T> v1 = (edge.v[1] - edge.v[0]).normalized();
          Vector4<T> normal2 = cross(v0, v1).normalized();

          return normal2;

          if(recursiondepth > 10){
            warning("recursion depth too large, %d", recursiondepth);
            warning("normal length = %10.10e", normal.length());
            warning("this edge");
            std::cout << *this << std::endl;

            warning("other edge");
            std::cout << edge << std::endl;
            if(potentialError){
              *potentialError = true;
            }
          }

          DBG(warning("Should not be possible %10.10e", normal.length()));
          //normal.set(1,1,1,0);
          //error("Distance too small");

          Vector4<T> averageNormal1 =
            (n[0] + n[1] - t[0] - t[1]).normalized();

          if(recursiondepth > 10){
            std::cout << averageNormal1 << std::endl;
          }

          Vector4<T> averageNormal2 =
            (edge.n[0] + edge.n[1] - edge.t[0] - edge.t[1]).normalized();

          //std::cout << averageNormal1 << std::endl;
          //std::cout << averageNormal2 << std::endl;

          /*Displace this edge slightly in negative normal direction*/

          Edge<T> displaced1(v[0]  - averageNormal1*(T)5e-8,
                             v[1]  - averageNormal1*(T)5e-8,
                             fv[0] - averageNormal1*(T)5e-8,
                             fv[1] - averageNormal1*(T)5e-8);

          Edge<T> displaced2(edge.v[0]  - averageNormal2*(T)5e-8,
                             edge.v[1]  - averageNormal2*(T)5e-8,
                             edge.fv[0] - averageNormal2*(T)5e-8,
                             edge.fv[1] - averageNormal2*(T)5e-8);

#if 1
          T normalDot1 = dot(n[0], n[1]);
          T normalDot2 = dot(edge.n[0], edge.n[1]);
          //T tangentDot1 = dot(t[0], t[1]);
          //T tangentDot2 = dot(edge.t[0], edge.t[1]);

          int displace = -1;

          if(Pos(normalDot1) > 0.0 && Pos(normalDot2) > 0.0){
            //Both normal
            if(normalDot1 > normalDot2){
              displace = 0;
            }else{
              displace = 1;
            }
          }else if(Pos(normalDot1) <= 0.0 && Pos(normalDot2) > 0.0){
            //This flat, edge normal
            displace = 1;
          }else if(Pos(normalDot1) >  0.0 && Pos(normalDot2) <= 0.0){
            //This normal, edge flat
            displace = 0;
          }else{
            //Both flat
            // Use the one that is the least flat
            if(Abs(normalDot1) > Abs(normalDot2)){
              displace = 0;
            }else{
              displace = 1;
            }
          }

          if(displace == 0){
            return displaced1.getOutwardNormal(edge, reference,
                                               recursiondepth+1,
                                               potentialError);
          }else{
            return getOutwardNormal(displaced2, reference,
                                    recursiondepth+1, potentialError);
          }
#else
          if(Abs(dot(n[0], n[1])) > Abs(dot(edge.n[0], edge.n[1]))){
            return displaced1.getOutwardNormal(edge, reference,
                                               recursiondepth+1,
                                               potentialError);
          }else{
            return getOutwardNormal(displaced2, reference,
                                    recursiondepth+1, potentialError);
          }
#endif
        }

        normal.normalize();

        return normal;
      }
    }
  }

  /**************************************************************************/
  /*Computes the normal from this edge to vertex v, which is
    orthogonal to this edge*/
  template<class T>
  Vector4<T> Edge<T>::getNormalToVertex(const Vector4<T>& v)const{
    Vector4<T> projection = getProjection(v);
    return (v - projection).normalized();
  }

  /**************************************************************************/
  /*Computes the two projections (p1, p2) of two edges (this, e) on
    each other, with b1, b2 the computed Barycentric coordinates of
    p1, p2*/
  template<class T>
  void Edge<T>::infiniteProjections(const Edge<T>& e,
                                    Vector4<T>* p1, Vector4<T>* p2,
                                    Vector4<T>* b1, Vector4<T>* b2,
                                    bool* parallel)const{
    if(p1 == 0 && p2 == 0 && b1 == 0 && b2 == 0 && parallel == 0){
      return;
    }

    /*
      equations for intersecting lines:

      p1 = a + (b - a) * t1
      p2 = c + (d - c) * t2
      p1 = p2

      =>

      a + (b - a) * t1 = c + (d - c) * t2

      =>

      (b - a) * t1 - (d - c) * t2 = c - a

      //a, b, c, d are points in 3d, so we have an overdetermined system
      //furthermore, p1 != p2 in most cases
      //Use least square approximation min_x(Ax-b) which finds t1 and t2
      //which minimizes the distance between both line segments
      //(is just wat we want).
      //Solved using the normal equation x = (A'A)^{-1}A'b
      */

    Vector4<T> v1mv0   =     v[1] -   v[0];
    Vector4<T> ev1mev0 =   e.v[1] - e.v[0];

    Vector4<T> bb = e.v[0] - v[0];

    T dd[4];
    dd[0] = dot(v1mv0, v1mv0);
    dd[1] = dot(v1mv0, ev1mev0);
    dd[2] = dd[1];
    dd[3] = dot(ev1mev0, ev1mev0);

    Matrix22<T> AtA2(dd);

    T determinant = 0;

    Matrix22<T> AtAi = AtA2.inverse(&determinant);

    if(Abs(determinant) <= DET_EPS){
      if(parallel){
        *parallel = true;
      }
    }

    Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                            dot(ev1mev0, bb));

    solution[1] *= (T)-1;

    if(p1){
      *p1 =   v[0] + (v1mv0)*solution[0];
    }
    if(b1){
      b1->set((T)1.0 - solution[0], solution[0], 0, 0);
    }
    if(p2){
      *p2 = e.v[0] + (ev1mev0)*solution[1];
    }
    if(b2){
      b2->set((T)1.0 - solution[1], solution[1], 0, 0);
    }
  }

  /**************************************************************************/
  template<class T>
  std::ostream& operator<<(std::ostream& os, const Edge<T>& e){
    if(e.isConvex()){
      os << "Convex edge" << std::endl;
    }else{
      os << "Concave edge" << std::endl;
    }
    os << "Valid faces = " << e.validFaces[0] << ", " << e.validFaces[1] << std::endl;
    os << "Face areas  = " << e.faceArea[0] << ", " << e.faceArea[1] << std::endl;
    os << "Tangent distance = " << e.tangentDistance[0] << ", " << e.tangentDistance[1] << std::endl;
    os << "Edge vertices      (\n" << e.v[0]  << e.v[1]  << ")" << std::endl;
    os << "Edge normals       (\n" << e.n[0]  << e.n[1]  << ")" << std::endl;
    os << "Edge tangents      (\n" << e.t[0]  << e.t[1]  << ")" << std::endl;
    os << "Edge face vertices (\n" << e.fv[0] << e.fv[1] << ")" << std::endl;

    return os;
  }

  /**************************************************************************/
  template<class T>
  T Edge<T>::getDistance(const Vector4<T>& p, Vector4<T>* c){
    /*project p on axis*/
    Vector4<T> L = v[1] - v[0];
    T alpha = (dot(L, p) - dot(L, v[0]))/dot(L, L);

    Vector4<T> D = v[0] + alpha * L;

    if(c){
      *c = D;
    }

    return (p - D).length();
  }

  /**************************************************************************/
  template<class T>
  T Edge<T>::getDistance(const Edge<T>& edge,
                         Vector4<T>* pp1, Vector4<T>* pp2,
                         Vector4<T>* pb1, Vector4<T>* pb2,
                         bool* degenerate)const{
    Vector4<T> p1, p2, b1, b2;
    bool parallel = false;

    infiniteProjections(edge, &p1, &p2, &b1, &b2, &parallel);

    if(!parallel){
      if(degenerate) *degenerate = false;

      if(pp1) *pp1 = p1;
      if(pp2) *pp2 = p2;
      if(pb1) *pb1 = b1;
      if(pb2) *pb2 = b2;

      Vector4<T> ee1 =      v[1] -      v[0];
      Vector4<T> ee2 = edge.v[1] - edge.v[0];
      Vector4<T> nn  = cross(ee1, ee2).normalized();

      T dist = Abs(dot(p1 - p2, nn));

      T dp = dot((p1 - p2).normalized(), nn);

      if(Abs(dp - (T)1.0) > (T)1e-6){
        //error("p1-p2 not in normal direction");
      }

      return dist;
    }else{
      if(degenerate) *degenerate = true;

      Vector4<T> v0 = edge.v[0] - v[0];
      Vector4<T> v1 = edge.v[0] - v[1];
      Vector4<T> ee = v[1] - v[0];

      T d0 = v0.length();
      T d1 = v1.length();

      Vector4<T> tangent;

      if(d0 < d1){
        tangent = cross(v1, ee);
      }else{
        tangent = cross(v0, ee);
      }

      if(tangent.length() < 1e-10){
        //return (T)0.0;
      }

      Vector4<T> n = cross(tangent, ee);

      n.normalize();

      if(d0 < d1){
        return Abs(dot(n, v1));
      }else{
        return Abs(dot(n, v0));
      }
      return 0;
    }
  }

  /**************************************************************************/
  template<class T>
  T Edge<T>::getSignedDistance(const Edge<T>& edge,
                               Vector4<T>* pp1,
                               Vector4<T>* pp2,
                               Vector4<T>* pb1,
                               Vector4<T>* pb2,
                               bool* degenerate,
                               const Vector4<T>* reference )const{
    bool parallel = false;
    Vector4<T> p1, p2, b1, b2;

    T dist = getDistance(edge, &p1, &p2, &b1, &b2, &parallel);

    if(parallel){
      if(degenerate) *degenerate = true;
    }else{
      if(degenerate) *degenerate = false;
    }

    if(pp1) *pp1 = p1;
    if(pp2) *pp2 = p2;
    if(pb1) *pb1 = b1;
    if(pb2) *pb2 = b2;

    if(parallel){
      Vector4<T> normal = edge.v[0] - v[0];
      if(reference){
        T sd = dist * Sign(dot(normal, *reference));

        return sd;
      }else{
        return normal.length();
      }
    }else{
      Vector4<T> normal = (p2 - p1);
      if(reference){
        T sd = dist * Sign(dot(normal, *reference));

        return sd;
      }else{
        return normal.length();
      }
    }
  }

#define EDGE_TOL 1E-7
  /**************************************************************************/
  template<class T>
  class Interpolator<T, Edge<T>, Edge<T> >{
  public:
    Interpolator(){
      rigidEdge1 = false;
      rigidEdge2 = false;
      initialized = false;
      derivative  = false;
      addEps = false;
    }

    void init(){
      if(!initialized){
        A = e11.v[0];
        B = e11.v[1];
        C = e12.v[0];
        D = e12.v[1];

        FA = e11.fv[0];
        FB = e11.fv[1];
        FC = e12.fv[0];
        FD = e12.fv[1];

        E = e21.v[0];
        F = e21.v[1];
        G = e22.v[0];
        H = e22.v[1];

        FE = e21.fv[0];
        FF = e21.fv[1];
        FG = e22.fv[0];
        FH = e22.fv[1];

        initialized = true;
        /*When used in multiple rootfinding methods, init can be used
          multiple times!*/

        T d0 = evaluate((T)0.0);
        if(Abs(d0) < eps){
          addEps = true;
        }
      }
    }

    /**************************************************************************/
    T evaluate(T s){
      if(derivative){
        return (localEvaluate(s + dx) - localEvaluate(s - dx))/((T)2.0 * dx);
      }else{
        return localEvaluate(s);
      }
    }

    /**************************************************************************/
    T localEvaluate(T s){
      if(rigidEdge1){
        Vector4<T> com = com11 * ((T)1.0 - s) + com12 * s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s) * rot11.inverse();

        Vector4<T> v11 = com + rot1 * (A  - com11);
        Vector4<T> v12 = com + rot1 * (B  - com11);
        Vector4<T> v13 = com + rot1 * (FA - com11);
        Vector4<T> v14 = com + rot1 * (FB - com11);

        e11.set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C  -  A) * s;
        Vector4<T> v12 = B  + (D  -  B) * s;
        Vector4<T> v13 = FA + (FC - FA) * s;
        Vector4<T> v14 = FB + (FD - FB) * s;

        e11.set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21 * ((T)1.0 - s) + com22 * s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s) * rot21.inverse();

        Vector4<T> v21 = com + rot2 * (E  - com21);
        Vector4<T> v22 = com + rot2 * (F  - com21);
        Vector4<T> v23 = com + rot2 * (FE - com21);
        Vector4<T> v24 = com + rot2 * (FF - com21);

        e22.set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G  -  E) * s;
        Vector4<T> v22 = F  + (H  -  F) * s;
        Vector4<T> v23 = FE + (FG - FE) * s;
        Vector4<T> v24 = FF + (FH - FF) * s;

        e22.set(v21, v22, v23, v24);
      }

      Vector4<T> p1, p2, b1, b2;

      bool parallel = false;

      T d = e11.getSignedDistance(e22, &p1, &p2, &b1, &b2, &parallel,
                                  &reference);

      T d2 = Abs(d);

      return d2 * Sign(dot(p2-p1, reference)) - eps;
    }

    /**************************************************************************/
    void getResult(T s, Edge<T>* p, Edge<T>* e){
      if(rigidEdge1){
        Vector4<T> com = com11 * ((T)1.0 - s) + com12 * s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s)*rot11.inverse();

        Vector4<T> v11 = com + rot1 * (A  - com11);
        Vector4<T> v12 = com + rot1 * (B  - com11);
        Vector4<T> v13 = com + rot1 * (FA - com11);
        Vector4<T> v14 = com + rot1 * (FB - com11);

        p->set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C  -  A) * s;
        Vector4<T> v12 = B  + (D  -  B) * s;
        Vector4<T> v13 = FA + (FC - FA) * s;
        Vector4<T> v14 = FB + (FD - FB) * s;

        p->set(v11, v12, v13, v14);
      }

      if(rigidEdge2){
        Vector4<T> com = com21 * ((T)1.0 - s) + com22 * s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s)*rot21.inverse();

        Vector4<T> v21 = com + rot2 * (E  - com21);
        Vector4<T> v22 = com + rot2 * (F  - com21);
        Vector4<T> v23 = com + rot2 * (FE - com21);
        Vector4<T> v24 = com + rot2 * (FF - com21);

        e->set(v21, v22, v23, v24);
      }else{
        Vector4<T> v21 = E  + (G  -  E) * s;
        Vector4<T> v22 = F  + (H  -  F) * s;
        Vector4<T> v23 = FE + (FG - FE) * s;
        Vector4<T> v24 = FF + (FH - FF) * s;

        e->set(v21, v22, v23, v24);
      }
    }

    /**************************************************************************/
    void setComputeDerivative(bool _der, T _dx){
      derivative = _der;
      dx = _dx;
    }

    Edge<T> e11;
    Edge<T> e12;

    Edge<T> e21;
    Edge<T> e22;

    Vector4<T> com11;
    Vector4<T> com12;
    Vector4<T> com21;
    Vector4<T> com22;

    Quaternion<T> rot11;
    Quaternion<T> rot12;
    Quaternion<T> rot21;
    Quaternion<T> rot22;

    T eps;
    bool rigidEdge1;
    bool rigidEdge2;

    Vector4<T> reference;

  protected:
    Vector4<T> A;
    Vector4<T> B;
    Vector4<T> C;
    Vector4<T> D;

    Vector4<T> FA;
    Vector4<T> FB;
    Vector4<T> FC;
    Vector4<T> FD;

    Vector4<T> E;
    Vector4<T> F;
    Vector4<T> G;
    Vector4<T> H;

    Vector4<T> FE;
    Vector4<T> FF;
    Vector4<T> FG;
    Vector4<T> FH;
    bool initialized;

    bool derivative;
    T dx;
    bool addEps;
  };

  /**************************************************************************/
  template<class T>
  int intersection(const Edge<T>& p,  const Edge<T>& x,
                   const Edge<T>& eu, const Edge<T>& e,
                   Vector4<T>& res, const T eps,
                   Vector4<T>* pb1, Vector4<T>* pb2,
                   Edge<T>* ppp, Edge<T>* eee,
                   T beps, int method, const Vector4<T>* ref, T* s){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;

    Edge<T> pp;
    Edge<T> ee;

    Interpolator<T, Edge<T>, Edge<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;

    func.reference = *ref;

    /*Perform a root finding on the distance function of the given
      edges*/
    //T root = brentDekker<T, Edge<T>, Edge<T> >(&func, 0, 1,
    //                                         (T)EDGE_TOL);

    DoubleRootResult<T> root;

    //try{
      root = doubleRootFinder<T, Edge<T>, Edge<T> >(&func, 0, 1,
                                                    (T)ROOT_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &ee);

      pp.computeNormalAndTangent();
      ee.computeNormalAndTangent();

      if(s)     *s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to negative*/

      warning("double root detected, %10.10e, %10.10e",
              root.roots[0], root.roots[1]);
      warning("function values, %10.10e, %10.10e",
              root.functionValues[0],
              root.functionValues[1]);

      T fx1 = func.evaluate(root.roots[0]);
      T fx2 = func.evaluate(root.roots[1]);

      warning("function values, %10.10e, %10.10e",
              fx1,
              fx2);

      warning("derivative function values, %10.10e, %10.10e",
              root.derivatives[0],
              root.derivatives[1]);

      if(root.derivatives[1] < 0.0){
        func.getResult(root.roots[1], &pp, &ee);

        pp.computeNormalAndTangent();
        ee.computeNormalAndTangent();

        if(s)     *s = root.roots[1];

      }else{
        func.getResult(root.roots[0], &pp, &ee);

        pp.computeNormalAndTangent();
        ee.computeNormalAndTangent();

        if(s)     *s = root.roots[0];
      }
    }else{
      return 0;
    }

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(pb1)   *pb1 = b1;
    if(pb2)   *pb2 = b2;
    if(eee)   *eee = ee;
    if(ppp)   *ppp = pp;

    if(ee.barycentricOnEdgeDist(b1, beps) &&
       ee.barycentricOnEdgeDist(b2, beps)){
         return 1;
    }

    //if(ee.barycentricOnEdge(b1, beps) &&
    //   ee.barycentricOnEdge(b2, beps)){
    //
    //  return 1;
    //}
    return 0;
  }

  /**************************************************************************/
  template<class T>
  int rigidIntersection(const Edge<T>& p,
                        const Vector4<T>& comp,
                        const Quaternion<T>& rotp,
                        const Edge<T>& x,
                        const Vector4<T>& comx,
                        const Quaternion<T>& rotx,
                        const Edge<T>& eu,
                        const Vector4<T>& comeu,
                        const Quaternion<T>& roteu,
                        const Edge<T>& e,
                        const Vector4<T>& come,
                        const Quaternion<T>& rote,
                        Vector4<T>& res, const T eps,
                        Vector4<T>* pb1, Vector4<T>* pb2,
                        Edge<T>* ppp, Edge<T>* eee,
                        T beps, int method, bool r1, bool r2,
                        T* s,
                        const Vector4<T>* reference){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    Edge<T> pp;
    Edge<T> ee;

    Interpolator<T, Edge<T>, Edge<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.e21 = e;
    func.e22 = eu;
    func.eps = eps;
    func.rigidEdge1 = r1;
    func.rigidEdge2 = r2;
    func.com11 = comx;
    func.com12 = comp;
    func.com21 = come;
    func.com22 = comeu;

    func.rot11 = rotx;
    func.rot12 = rotp;
    func.rot21 = rote;
    func.rot22 = roteu;

    func.reference = *reference;

    /*Perform a root finding on the distance function of the given
      edges*/
    //T root = brentDekker<T, Edge<T>, Edge<T> >(&func, 0, 1,
    //                                         (T)EDGE_TOL);
    //func.getResult(root, &pp, &ee);

    //*s = root;

    //pp.computeNormalAndTangent();
    //ee.computeNormalAndTangent();


    DoubleRootResult<T> root;

    //try{
      root = doubleRootFinder<T, Edge<T>, Edge<T> >(&func, 0, 1,
                                                    (T)ROOT_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &ee);

      pp.computeNormalAndTangent();
      ee.computeNormalAndTangent();

      if(s)     *s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to negative*/

      if(root.functionValues[1] > 0.0){
        func.getResult(root.roots[1], &pp, &ee);

        pp.computeNormalAndTangent();
        ee.computeNormalAndTangent();

        if(s)     *s = root.roots[1];

      }else{
        func.getResult(root.roots[0], &pp, &ee);

        pp.computeNormalAndTangent();
        ee.computeNormalAndTangent();

        if(s)     *s = root.roots[0];
      }
    }else{
      return 0;
    }

    Vector4<T> p1, p2, b1, b2;

    pp.infiniteProjections(ee, &p1, &p2, &b1, &b2);

    res = p1;

    if(pb1) *pb1 = b1;
    if(pb2) *pb2 = b2;
    if(eee) *eee = ee;
    if(ppp) *ppp = pp;

    if(ee.barycentricOnEdgeDist(b1, beps) &&
       ee.barycentricOnEdgeDist(b2, beps)){
      return 1;
    }
#if 0
    else{
      p.infiniteProjections(eu, &p1, &p2, &b1, &b2);
      if( p.barycentricOnEdgeDist(b1, beps) &&
         eu.barycentricOnEdgeDist(b2, beps)){
        return 1;
      }else{
        x.infiniteProjections(e, &p1, &p2, &b1, &b2);
        if(x.barycentricOnEdgeDist(b1, beps) &&
           e.barycentricOnEdgeDist(b2, beps)){
          return 1;
        }
      }
    }
#endif
    //if(ee.barycentricOnEdge(b1, beps) &&
    //   ee.barycentricOnEdge(b2, beps)){
    //  return 1;
    //}
    return 0;
  }

  /**************************************************************************/
  template<class T>
  class Interpolator<T, Edge<T>, Vector4<T> >{
  public:
    /**************************************************************************/
    Interpolator(){
      rigidEdge = false;
      rigidVertex = false;
      initialized = false;
      derivative  = false;
    }

    /**************************************************************************/
    void init(){
      if(!initialized){
        A = e11.v[0];
        B = e11.v[1];
        C = e12.v[0];
        D = e12.v[1];

        FA = e11.fv[0];
        FB = e11.fv[1];
        FC = e12.fv[0];
        FD = e12.fv[1];

        E = v21;
        //F = e21.v[1];
        G = v22;
        //H = e22.v[1];

        //FE = e21.fv[0];
        //FF = e21.fv[1];
        //FG = e22.fv[0];
        //FH = e22.fv[1];

        normal = e11.getNormalToVertex(v21);

        initialized = true;
        /*When used in multiple rootfinding methods, init can be used
          multiple times!*/
      }
    }

    /**************************************************************************/
    T evaluate(T s){
      if(derivative){
        return (localEvaluate(s + dx) - localEvaluate(s - dx))/((T)2.0 * dx);
      }else{
        return localEvaluate(s);
      }
    }

    /**************************************************************************/
    T localEvaluate(T s){
      if(rigidEdge){
        Vector4<T> com = com11 * ((T)1.0 - s) + com12 * s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s) * rot11.inverse();

        Vector4<T> v11 = com + rot1 * (A  - com11);
        Vector4<T> v12 = com + rot1 * (B  - com11);
        Vector4<T> v13 = com + rot1 * (FA - com11);
        Vector4<T> v14 = com + rot1 * (FB - com11);

        e11.set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C  -  A) * s;
        Vector4<T> v12 = B  + (D  -  B) * s;
        Vector4<T> v13 = FA + (FC - FA) * s;
        Vector4<T> v14 = FB + (FD - FB) * s;

        e11.set(v11, v12, v13, v14);
      }

      if(rigidVertex){
        Vector4<T> com = com21 * ((T)1.0 - s) + com22 * s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s) * rot21.inverse();

        v22 = com + rot2 * (E  - com21);
      }else{
        v22 = E  + (G - E) * s;
      }

      normal = e11.getNormalToVertex(v22);

      return e11.getPlaneDistance(v22, normal) - eps;
    }

    /**************************************************************************/
    void getResult(T s, Edge<T>* p, Vector4<T>* v){
      if(rigidEdge){
        Vector4<T> com = com11 * ((T)1.0 - s) + com12 * s;
        Quaternion<T> rot1 = slerp(rot11, rot12, s) * rot11.inverse();

        Vector4<T> v11 = com + rot1 * (A  - com11);
        Vector4<T> v12 = com + rot1 * (B  - com11);
        Vector4<T> v13 = com + rot1 * (FA - com11);
        Vector4<T> v14 = com + rot1 * (FB - com11);

        p->set(v11, v12, v13, v14);
      }else{
        Vector4<T> v11 = A  + (C  -  A) * s;
        Vector4<T> v12 = B  + (D  -  B) * s;
        Vector4<T> v13 = FA + (FC - FA) * s;
        Vector4<T> v14 = FB + (FD - FB) * s;

        p->set(v11, v12, v13, v14);
      }

      if(rigidVertex){
        Vector4<T> com = com21 * ((T)1.0 - s) + com22 * s;
        Quaternion<T> rot2 = slerp(rot21, rot22, s) * rot21.inverse();

        Vector4<T> v21 = com + rot2 * (E  - com21);

        *v = v21;
      }else{
        Vector4<T> v21 = E  + (G - E) * s;

        *v = v21;
      }
    }

    void setComputeDerivative(bool _der, T _dx){
      derivative = _der;
      dx = _dx;
    }

    Edge<T> e11;
    Edge<T> e12;

    Vector4<T> v21;
    Vector4<T> v22;

    Vector4<T> com11;
    Vector4<T> com12;
    Vector4<T> com21;
    Vector4<T> com22;

    Quaternion<T> rot11;
    Quaternion<T> rot12;
    Quaternion<T> rot21;
    Quaternion<T> rot22;

    T eps;
    bool rigidEdge;
    bool rigidVertex;

    //Vector4<T> reference;
    //bool finerTest;
  protected:
    Vector4<T> A;
    Vector4<T> B;
    Vector4<T> C;
    Vector4<T> D;

    Vector4<T> FA;
    Vector4<T> FB;
    Vector4<T> FC;
    Vector4<T> FD;

    Vector4<T> E;
    Vector4<T> G;

    Vector4<T> normal;

    bool initialized;
    bool derivative;
    T dx;
  };

  /**************************************************************************/
  template<class T>
  int intersection(const Edge<T>& p,  const Edge<T>& x,
                   const Vector4<T>& vu, const Vector4<T>& v,
                   Vector4<T>& res, const T eps,
                   Vector4<T>* pb,
                   Edge<T>* ppp, Vector4<T>* vvv,
                   T beps, int method, T* s){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;

    Edge<T>    pp;
    Vector4<T> vv;

    Interpolator<T, Edge<T>, Vector4<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.v21 = v;
    func.v22 = vu;
    func.eps = eps;

    /*Perform a root finding on the distance function of the given
      edges*/
    DoubleRootResult<T> root =
      doubleRootFinder<T, Edge<T>, Vector4<T> >(&func, 0, 1,
                                                (T)ROOT_TOL);

    //try{
      root = doubleRootFinder<T, Edge<T>, Vector4<T> >(&func, 0, 1,
                                                       (T)ROOT_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &vv);

      if(s)     *s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to
        negative, i.e., a negative derivative*/

      warning("double root detected, %10.10e, %10.10e",
              root.roots[0], root.roots[1]);
      warning(" fx = %10.10e, %10.10e",
              root.functionValues[0], root.functionValues[1]);
      warning("dfx = %10.10e, %10.10e",
              root.derivatives[0], root.derivatives[1]);

      if(root.derivatives[0] > 0.0){
        func.getResult(root.roots[1], &pp, &vv);

        if(s)     *s = root.roots[1];

      }else{
        func.getResult(root.roots[0], &pp, &vv);

        if(s)     *s = root.roots[0];
      }
    }else{
      /*Hack for initialization of too close edges*/
      func.getResult(0, &pp, &vv);

      if(s)     *s = 0;

    }

    Vector4<T> b1;

    b1 = pp.getBarycentricCoordinates(vv);

    //res = p1;

    if(pb)    *pb  = b1;
    if(vvv)   *vvv = vv;
    if(ppp)   *ppp = pp;

    if(pp.barycentricOnEdgeDist(b1, eps) ){
      return 1;
    }
    return 0;
  }

  /**************************************************************************/
  template<class T>
  int rigidIntersection(const Edge<T>& p,
                        const Vector4<T>& comp,
                        const Quaternion<T>& rotp,
                        const Edge<T>& x,
                        const Vector4<T>& comx,
                        const Quaternion<T>& rotx,
                        const Vector4<T>& vu,
                        const Vector4<T>& comvu,
                        const Quaternion<T>& rotvu,
                        const Vector4<T>& v,
                        const Vector4<T>& comv,
                        const Quaternion<T>& rotv,
                        Vector4<T>& res, const T eps,
                        Vector4<T>* pb1,
                        Edge<T>* ppp, Vector4<T>* vvv,
                        T beps, int method, bool r1, bool r2,
                        T* s){
    Vector4<T> v11, v12, v13, v14, v21, v22, v23, v24;
    Edge<T>    pp;
    Vector4<T> vv;

    Interpolator<T, Edge<T>, Vector4<T> > func;
    func.e11 = x;
    func.e12 = p;
    func.v21 = v;
    func.v22 = vu;
    func.eps = eps;
    func.rigidEdge = r1;
    func.rigidVertex = r2;

    func.com11 = comx;
    func.com12 = comp;
    func.com21 = comv;
    func.com22 = comvu;

    func.rot11 = rotx;
    func.rot12 = rotp;
    func.rot21 = rotv;
    func.rot22 = rotvu;

    DoubleRootResult<T> root;

    //try{
      root = doubleRootFinder<T, Edge<T>, Vector4<T> >(&func, 0, 1,
                                                       (T)ROOT_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &vv);


      if(s)     *s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to negative*/

      if(root.functionValues[1] > 0.0){
        func.getResult(root.roots[1], &pp, &vv);

        if(s)     *s = root.roots[1];

      }else{
        func.getResult(root.roots[0], &pp, &vv);

        if(s)     *s = root.roots[0];
      }
    }else{
      /*Hack for initialization of too close edges*/
      func.getResult(0.0, &pp, &vv);

      if(s)     *s = 0.0;
    }

    Vector4<T> b1;

    b1 = pp.getBarycentricCoordinates(vv);

      //res = p1;

    if(pb1)   *pb1 = b1;
    if(vvv)   *vvv = vv;
    if(ppp)   *ppp = pp;

    if(pp.barycentricOnEdgeDist(b1, eps) ){
      return 1;
    }
    return 0;
  }

  /**************************************************************************/
  /*Checks if an edge deformation e1 -> e2*/
  template<class T>
  bool edgeCollapsed(const Edge<T>& e1, const Edge<T>& e2, bool debug){
    /*Check that we are checking the same edges*/
    if(debug){
      message("e1.edgeId  = %d", e1.edgeId());
      message("e2.edgeId  = %d", e2.edgeId());
      message("e1.faceId1 = %d", e1.faceId(0));
      message("e1.faceId2 = %d", e1.faceId(1));
      message("e2.faceId1 = %d", e2.faceId(0));
      message("e2.faceId2 = %d", e2.faceId(1));

    }
    if(e1.edgeId()  == e2.edgeId() &&
       e1.faceId(0) == e2.faceId(0) &&
       e1.faceId(1) == e2.faceId(1) &&
       e1.edgeId()  != -1 &&
       e1.faceId(0) != -1 &&
       e1.faceId(1) != -1){

      if(debug){
        message("e1.convex = %d", e1.isConvex());
        message("e2.convex = %d", e2.isConvex());
      }

      if( (e1.isConvex()  && e2.isConvex()) ||
          (e1.isConcave() && e2.isConcave() )){
        /*No convexity change for this edge, it cant be collapsed*/
        if(debug){
          message("no convexity change");
        }
        return false;
      }else{
#if 0
        if(dot(e1.t[0], e1.t[1]) > (T)0.0 &&
           dot(e2.t[0], e2.t[1]) > (T)0.0){
          /*Edge changed convexity while it was in a flat
            configuration. This is a collapse*/
          return true;
        }

        return false;
        /*This check doesnt make sense*/
#endif

        /*Edge has changed from convex to concave or from concave to
          convex. This could be a potential collapse if the a
          particular face vertex goes through the other face. If one
          pair does, also the other pair goes through the other
          face.*/

        /*If a facevertex projects on the other face, it has collapsed*/

        Triangle<T> tr1(e1.v[0], e1.v[1], e1.fv[0]);
        Triangle<T> tr2(e1.v[1], e1.v[0], e1.fv[1]);

        Triangle<T> tr3(e2.v[0], e2.v[1], e2.fv[0]);
        Triangle<T> tr4(e2.v[1], e2.v[0], e2.fv[1]);

        Vector4<T> b1 = tr1.getBarycentricCoordinates(e1.fv[1]);
        Vector4<T> b2 = tr2.getBarycentricCoordinates(e1.fv[0]);
        Vector4<T> b3 = tr3.getBarycentricCoordinates(e2.fv[1]);
        Vector4<T> b4 = tr4.getBarycentricCoordinates(e2.fv[0]);

        if(debug){
          message("barycentric coordinates");
          std::cout << b1 << std::endl;
          std::cout << b2 << std::endl;
          std::cout << b3 << std::endl;
          std::cout << b4 << std::endl;
        }

        if(b1[2] >= 0.0 && b2[2] >= 0.0 && b3[2] >= 0.0 && b4[2] >= 0.0){
          return true;
        }

        if(tr3.getSignedDistance(e2.fv[1]) < 9e-10 && b3[2] >= 0.0){
          //return true;
        }

        if(tr4.getSignedDistance(e2.fv[0]) < 9e-10 && b4[2] >= 0.0){
          //return true;
        }
      }
    }else{
      warning("%d, %d", e1.edgeId(), e2.edgeId());
      warning("%d, %d", e1.faceId(0), e2.faceId(0));
      warning("%d, %d", e1.faceId(1), e2.faceId(1));
      error("Comparing different edges!!");
    }

    if(debug){
      message("seems that ther is no collapse");
    }

    return false;
  }

  /**************************************************************************/
  template class Edge<float>;
  template class Edge<double>;

  /**************************************************************************/
  template bool edgeCollapsed(const Edge<float>& e1, const Edge<float>& e2,
                              bool debug);

  /**************************************************************************/
  template bool edgeCollapsed(const Edge<double>& e1, const Edge<double>& e2,
                              bool debug);

  /**************************************************************************/
  template int intersection(const Edge<float>& p,  const Edge<float>& x,
                            const Edge<float>& eu, const Edge<float>& e,
                            Vector4<float>& res, const float eps,
                            Vector4<float>* pb1, Vector4<float>* pb2,
                            Edge<float>* ppp, Edge<float>* eee,
                            float beps, int method, const Vector4<float>* ref,
                            float* s);

  /**************************************************************************/
  template int intersection(const Edge<double>& p,  const Edge<double>& x,
                            const Edge<double>& eu, const Edge<double>& e,
                            Vector4<double>& res, const double eps,
                            Vector4<double>* pb1, Vector4<double>* pb2,
                            Edge<double>* ppp, Edge<double>* eee,
                            double beps, int method, const Vector4<double>* ref,
                            double* s);

  /**************************************************************************/
  template int rigidIntersection(const Edge<float>& p,
                                 const Vector4<float>& comp,
                                 const Quaternion<float>& rotp,
                                 const Edge<float>& x,
                                 const Vector4<float>& comx,
                                 const Quaternion<float>& rotx,
                                 const Edge<float>& eu,
                                 const Vector4<float>& comeu,
                                 const Quaternion<float>& roteu,
                                 const Edge<float>& e,
                                 const Vector4<float>& come,
                                 const Quaternion<float>& rote,
                                 Vector4<float>& res, const float eps,
                                 Vector4<float>* pb1, Vector4<float>* pb2,
                                 Edge<float>* ppp, Edge<float>* eee,
                                 float beps, int method, bool r1, bool r2,
                                 float* s, const Vector4<float>* reference);

  /**************************************************************************/
  template int rigidIntersection(const Edge<double>& p,
                                 const Vector4<double>& comp,
                                 const Quaternion<double>& rotp,
                                 const Edge<double>& x,
                                 const Vector4<double>& comx,
                                 const Quaternion<double>& rotx,
                                 const Edge<double>& eu,
                                 const Vector4<double>& comeu,
                                 const Quaternion<double>& roteu,
                                 const Edge<double>& e,
                                 const Vector4<double>& come,
                                 const Quaternion<double>& rote,
                                 Vector4<double>& res, const double eps,
                                 Vector4<double>* pb1, Vector4<double>* pb2,
                                 Edge<double>* ppp, Edge<double>* eee,
                                 double beps, int method, bool r1, bool r2,
                                 double* s, const Vector4<double>* reference);

  /**************************************************************************/
  template int intersection(const Edge<float>& p,
                            const Edge<float>& x,
                            const Vector4<float>& vu,
                            const Vector4<float>& v,
                            Vector4<float>& res,
                            const float eps,
                            Vector4<float>* b,
                            Edge<float>* ppp, Vector4<float>* vvv,
                            float beps, int method, float* s);

  /**************************************************************************/
  template int intersection(const Edge<double>& p,
                            const Edge<double>& x,
                            const Vector4<double>& vu,
                            const Vector4<double>& v,
                            Vector4<double>& res,
                            const double eps,
                            Vector4<double>* b,
                            Edge<double>* ppp, Vector4<double>* vvv,
                            double beps, int method, double* s);

  /**************************************************************************/
  template int rigidIntersection(const Edge<float>& p,
                                 const Vector4<float>& comp,
                                 const Quaternion<float>& rotp,
                                 const Edge<float>& x,
                                 const Vector4<float>& comx,
                                 const Quaternion<float>& rotx,
                                 const Vector4<float>& vu,
                                 const Vector4<float>& comvu,
                                 const Quaternion<float>& rotvu,
                                 const Vector4<float>& v,
                                 const Vector4<float>& come,
                                 const Quaternion<float>& rote,
                                 Vector4<float>& res, const float eps,
                                 Vector4<float>* b,
                                 Edge<float>* ppp, Vector4<float>* vvv,
                                 float beps, int method,
                                 bool re, bool rv,
                                 float* s);

  /**************************************************************************/
  template int rigidIntersection(const Edge<double>& p,
                                 const Vector4<double>& comp,
                                 const Quaternion<double>& rotp,
                                 const Edge<double>& x,
                                 const Vector4<double>& comx,
                                 const Quaternion<double>& rotx,
                                 const Vector4<double>& vu,
                                 const Vector4<double>& comvu,
                                 const Quaternion<double>& rotvu,
                                 const Vector4<double>& v,
                                 const Vector4<double>& come,
                                 const Quaternion<double>& rote,
                                 Vector4<double>& res, const double eps,
                                 Vector4<double>* b,
                                 Edge<double>* ppp, Vector4<double>* vvv,
                                 double beps, int method,
                                 bool re, bool rv,
                                 double* s);
}
