/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/Cylinder.hpp"
#include "math/Matrix22.hpp"
#include "math/Vector2.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  Cylinder<T>::Cylinder():radius(0){
  }

  /**************************************************************************/
  template<class T>
  Cylinder<T>::Cylinder(T r):radius(r){
  }

  /**************************************************************************/
  template<class T>
  Cylinder<T>::Cylinder(T r,
                        const Vector4<T>& a,
                        const Vector4<T>& b):radius(r),
                                             start(a),
                                             end(b){
  }

  /**************************************************************************/
  template<class T>
  Cylinder<T>::Cylinder(const Cylinder<T>& cyl):radius(cyl.radius),
                                                start(cyl.start),
                                                end(cyl.end){
  }

  /**************************************************************************/
  template<class T>
  Cylinder<T>::~Cylinder(){
  }

  /**************************************************************************/
  template<class T>
  Cylinder<T>& Cylinder<T>::operator=(const Cylinder<T>& cyl){
    if(this != &cyl){
      radius = cyl.radius;
      start  = cyl.start;
      end    = cyl.end;
    }
    return *this;
  }

  /**************************************************************************/
  template<class T>
  T Cylinder<T>::signedDistance(const Vector4<T>& p, T* palpha)const{
    /*project p on axis*/
    Vector4<T> L = end - start;
    T alpha = (dot(L, p) - dot(L, start))/dot(L, L);

    if(palpha){
      *palpha = alpha;
    }

    Vector4<T> D = start + alpha * L;
    return (p - D).length() - radius;
  }

#define DET_EPS 1e-15
  /**************************************************************************/
  template<class T>
  T Cylinder<T>::signedDistance(const Vector4<T>& a,
                                const Vector4<T>& b,
                                T* palpha,
                                T* pbeta,
                                bool* pparallel)const{
    Vector4<T> v1mv0   = end - start;
    Vector4<T> ev1mev0 =   b - a;

    Vector4<T> bb = a - start;

    T dd[4];
    dd[0] = dot(v1mv0, v1mv0);
    dd[1] = dot(v1mv0, ev1mev0);
    dd[2] = dd[1];
    dd[3] = dot(ev1mev0, ev1mev0);

    if(dd[3] < 1e-8){
      /*Degenerate line*/
      if(pbeta){
        *pbeta = 0.0;
        return signedDistance(a, palpha);
      }
    }

    Matrix22<T> AtA2(dd);

    T determinant = 0;

    Matrix22<T> AtAi = AtA2.inverse(&determinant);

    if(Abs(determinant) <= DET_EPS){
      if(pparallel){
        *pparallel = true;
      }

      Vector4<T> v1 = a - end;
      Vector4<T> v2 = b - end;

      Vector4<T> normal = cross(v1, v2);
      normal = cross(normal, (start - end));

      if(normal.length() < DET_EPS){
        /*line and cylinder share same axis*/
        return -radius;
      }

      normal.normalize();

      if(palpha){
        *palpha = 0.0;
      }

      if(pbeta){
        *pbeta = 0.0;
      }

      return dot(normal, v1) - radius;

      error("Parallel");
    }

    Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                            dot(ev1mev0, bb));

    Vector4<T> p1 = start +   (v1mv0)*solution[0];
    Vector4<T> p2 =     a - (ev1mev0)*solution[1];

    if(palpha){
      *palpha = solution[0];
    }

    if(pbeta){
      *pbeta = -solution[1];
    }

    return (p2 - p1).length() - radius;
  }

  /**************************************************************************/
  template<class T>
  bool Cylinder<T>::intersectionWithLine(const Vector4<T>& a,
                                         const Vector4<T>& b,
                                         Vector4<T>* ca,
                                         Vector4<T>* cb,
                                         T* palpha1,
                                         T* pbeta1,
                                         T* palpha2,
                                         T* pbeta2,
                                         bool* pparallel)const{

    /*Compute location that minimizes distance between edges*/
    Vector4<T> v1mv0   = end - start;
    Vector4<T> ev1mev0 =   b - a;

    Vector4<T> bb = a - start;

    T dd[4];
    dd[0] = dot(v1mv0, v1mv0);
    dd[1] = dot(v1mv0, ev1mev0);
    dd[2] = dd[1];
    dd[3] = dot(ev1mev0, ev1mev0);

    if(dd[3] < 1e-8){
      message("degenerate line");
      return false;
    }

    if(dd[0] < 1e-8){
      message("degenerate cylinder");
      return false;
    }

    Matrix22<T> AtA2(dd);

    T determinant = 0;

    Matrix22<T> AtAi = AtA2.inverse(&determinant);

    if(Abs(determinant) <= DET_EPS){
      if(pparallel){
        *pparallel = true;
      }
      return false;
    }

    if(pparallel){
      *pparallel = false;
    }

    Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                            dot(ev1mev0, bb));

    Vector4<T> p1 = start +   (v1mv0)*solution[0];
    Vector4<T> p2 =     a - (ev1mev0)*solution[1];

    T height = (p2-p1).length();

    if(height > radius){
      return false;
    }else{
      Vector4<T> e1 = v1mv0;
      Vector4<T> e2 = ev1mev0;

      e1.normalize();
      e2.normalize();

      T pa = dot(e1, e2);
      T pb = Sqrt((T)1.0 - Sqr(pa));
      T factor = pa / pb;

      T beta = Sqrt(Sqr(radius) - Sqr(height));
      T alpha = factor * beta;

      T gamma = Sqrt(Sqr(alpha) + Sqr(beta));

      Vector4<T> p11 = p1 + e1 * alpha;
      Vector4<T> p22 = p2 + e2 * gamma;

      *ca = p22;

      if(palpha1){
        *palpha1 = dot(v1mv0, (p11 - start))/dot(v1mv0, v1mv0);
      }

      if(pbeta1){
        *pbeta1 = dot(ev1mev0, (p22 - a))/dot(ev1mev0, ev1mev0);
      }

      p11 = p1 - e1 * alpha;
      p22 = p2 - e2 * gamma;

      *cb = p22;

      if(palpha2){
        *palpha2 = dot(v1mv0, (p11 - start))/dot(v1mv0, v1mv0);
      }

      if(pbeta2){
        *pbeta2 = dot(ev1mev0, (p22 - a))/dot(ev1mev0, ev1mev0);
      }

      return true;
    }
  }

  template class Cylinder<float>;
  template class Cylinder<double>;
}
