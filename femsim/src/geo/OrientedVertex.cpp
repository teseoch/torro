/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/OrientedVertex.hpp"
#include "geo/Triangle.hpp"
#include <stdio.h>

namespace tsl{
  /**************************************************************************/
  template<class T>
  OrientedVertex<T>::OrientedVertex(){
    p = Vector4<T>(0,0,0,0);
    subEdges.clear();
    smallestEdge = 1000000.0;
  }

  /**************************************************************************/
  template<class T>
  OrientedVertex<T>::OrientedVertex(const OrientedVertex<T>& ov){
    p = ov.p;
    subEdges = ov.subEdges;
    smallestEdge = ov.smallestEdge;
  }

  /**************************************************************************/
  template<class T>
  OrientedVertex<T>& OrientedVertex<T>::operator=(const OrientedVertex<T>& ov){
    if(&ov != this){
      /*No self assignment*/
      p = ov.p;
      subEdges = ov.subEdges;
      smallestEdge = ov.smallestEdge;
    }

    return *this;
  }

  /**************************************************************************/
  template<class T>
  const Vector4<T>& OrientedVertex<T>::getVertex()const{
    return p;
  }

  /**************************************************************************/
  template<class T>
  void OrientedVertex<T>::setVertex(const Vector4<T>& _p){
    p = _p;
  }

  /**************************************************************************/
  template<class T>
  void OrientedVertex<T>::set(const Vector4<T>& _p,
                              List<Vector4<T> >& edgeVertices,
                              List<Vector4<T> >& edgeNormals,
                              List<int>& edgeIds,
                              List<int>& vertexIds){
    p = _p;

    subEdges.clear();

    /*Convert to edges*/
    typename List<Vector4<T> >::Iterator itv = edgeVertices.begin();
    typename List<Vector4<T> >::Iterator itn = edgeNormals.begin();
    List<int>::Iterator iteid = edgeIds.begin();
    List<int>::Iterator itvid = vertexIds.begin();

    while(itv != edgeVertices.end()){
      SubEdge subEdge;

      subEdge.v      = (*itv++ - _p);
      subEdge.normal = *itn++;
      subEdge.edgeId = *iteid++;
      subEdge.vertexId = *itvid++;

      T length = subEdge.v.length();

      if(length < smallestEdge){
        smallestEdge = length;
      }

      subEdge.v /= length;

      subEdges.append(subEdge);
    }
  }

  /**************************************************************************/
  template<class T>
  void OrientedVertex<T>::getEdgeIndices(List<int>& l)const{
    typename List<SubEdge>::Iterator it = subEdges.begin();
    while(it != subEdges.end()){
      l.append(it->edgeId);
      it++;
    }
  }

  /**************************************************************************/
  template<class T>
  bool OrientedVertex<T>::planeIntersection(const Vector4<T>& planeVertex,
                                            const Vector4<T>& planeNormal,
                                            const int* indices,
                                            bool forced,
                                            bool debug)const{
    /*An oriented vertex is a vertex with an approximation of the
      surrounding faces. Each surrounding face is described by two
      adjacent normalized vectors, and its weighted normal (the cross
      product of the two vectors). A plane intersects this oriented
      vertex iff the plane intersects all normalized vectors.*/

    Vector4<T> vp = p - planeVertex;

    if(dot(vp, planeNormal) >= 0.0){
      if(debug){
        message("positive distance, no intersection, %10.10e",
                dot(vp, planeNormal));
      }
      return false;
    }

    if(debug){
      message("Negative distance %10.10e", dot(vp, planeNormal));
    }

    if(forced){
      return true;
    }

    typename List<SubEdge>::Iterator it = subEdges.begin();

    T closestProjection = 10000.0;
    Vector4<T> closestNormal;

    /*Since all edges are stored twice, so we test only the first*/
    bool intersectionAll = true;
    bool intersectionNone = true;
    bool projection = false;

    bool connected = false;

    while(it != subEdges.end()){
      SubEdge subEdge = *it++;

      /*Note, when one of both projections are negative, there is an
        intersection for that face.

        If intersected, then all vertices that create the faces lie on
        one side of the plane. In order to determine if there is a
        face pointing to the plane surface, select the closest vertex
        and then select the face that is closest to the plane. For
        this face we check the normals. Please note that none of the
        edges can be collapsed, otherwise, this ordering is not valid.*/

      if(indices){
        /*If the indices of the face are provided, check if this
          vertex is connected to the face directly. If so, skip this
          test since the projection of the edge is always positive if
          the edge is connected to the face. In the extreme, the
          projection is zero when the signed distance between the face
          and vertex is zero. Since this can raise errors in properly
          detecting the intersection, it is safe to ignore this.*/
        int vertexId = subEdge.vertexId;
        bool skip = false;
        for(int i=0;i<3;i++){
          if(vertexId == indices[i]){
            skip = true;
            connected = true;
          }
        }

        if(skip){
          continue;
        }
      }

      T projection = dot(subEdge.v, planeNormal);

      if(debug){
        message("Projections = %10.10e", projection);
      }

      if(projection <= (T)0.0){
        intersectionAll = false;

        if(debug){
          message("no intersection");
        }
        return false;
      }else{
        intersectionNone = false;
      }

      if(Abs(projection) < closestProjection){
        closestProjection = Abs(projection);
        closestNormal = subEdge.normal;
      }
    }

    if(dot(closestNormal, planeNormal) < 0.0){
      projection = true;
    }

    if(connected){
      /*If the vertex is connected with the face, i.e., when one of
        the endpoints of the connected edges is a vertex of the face,
        then there is always an intersection if the signed distance is
        negative. This because the face and vertex do belong to the
        same sub-patch of the surface. The vertex basically belongs to
        a neighboring face of the tested face.

        The problem with these cases is that when the signed distance
        goes to zero, also some edges become parallel to the face,
        which might not result in an intersection. If in a later stage
        the signed distance becomes slightly negative, then the edge
        should intersect the face. However, it is possible that this
        intersection is detected too late and that no valid
        intersection is found.
      */
      return true;
    }else{
      return
        (intersectionAll && !intersectionNone && projection);
    }


    //      ||
    // (intersectionNone && !intersectionAll && projection);
  }

  /**************************************************************************/
  template<class T>
  bool orientedVertexCollapsed(const OrientedVertex<T>& v1,
                               const OrientedVertex<T>& v2){
    typename List<typename OrientedVertex<T>::SubEdge>::Iterator it1a =
      v1.subEdges.begin();
    typename List<typename OrientedVertex<T>::SubEdge>::Iterator it2a =
      v2.subEdges.begin();

    typename List<typename OrientedVertex<T>::SubEdge>::Iterator it1b =
      v1.subEdges.begin();
    typename List<typename OrientedVertex<T>::SubEdge>::Iterator it2b =
      v2.subEdges.begin();

    if(v1.subEdges.size() != v2.subEdges.size()){
      error("testing different sized vertices");
    }

    it1b++;
    it2b++;

    Vector4<T> p1 = v1.p;
    Vector4<T> p2 = v2.p;

    /*Check for each face if there is a crossing with a vertex*/
    while(it1a != v1.subEdges.end()){
      if(it1a->edgeId != it2a->edgeId){
        error("Wrong ids");
      }

      if(it1b == v1.subEdges.end()){
        it1b = v1.subEdges.begin();
      }
      if(it2b == v2.subEdges.end()){
        it2b = v2.subEdges.begin();
      }

      Vector4<T> startV1 = it1a->v + p1;
      Vector4<T> startV2 = it2a->v + p2;
      Vector4<T> endV1   = it1b->v + p1;
      Vector4<T> endV2   = it2b->v + p2;

      typename List<typename OrientedVertex<T>::SubEdge>::Iterator itv1 =
        v1.subEdges.begin();
      typename List<typename OrientedVertex<T>::SubEdge>::Iterator itv2 =
        v2.subEdges.begin();

      while(itv1 != v1.subEdges.end()){
        if(itv1 == it1a || itv1 == it1b ||
           itv2 == it2a || itv2 == it2b){

          itv1++;
          itv2++;

          continue;
        }

        Vector4<T> ev1 = itv1->v + p1;
        Vector4<T> ev2 = itv2->v + p2;

        Vector4<T> normal1 = cross(startV1 - p1, endV1 - p1).normalized();
        Vector4<T> normal2 = cross(startV2 - p2, endV2 - p2).normalized();

        Vector4<T> vv1 = itv1->v;
        Vector4<T> vv2 = itv2->v;


        T d1 = dot(normal1, vv1);
        T d2 = dot(normal2, vv2);

        //message("%10.10e, %10.10e", d1, d2);

        if(Sign(d1) != Sign(d2)){
          Triangle<T> t1(startV1, endV1, p1);
          Triangle<T> t2(startV2, endV2, p2);

          Vector4<T> res;
          Vector4<T> bary;
          Triangle<T> ti;
          T root = 0;
          int inters = -1;

          try{
            inters = intersection(t1, t2, ev1, ev2, res, (T)0.0, &bary, &ti,
                                  (T)0.0, (T)0.0, 0, &root, false);
          }catch(Exception* e){
            /*No intersection*/
            delete e;
          }

          if(inters == 1){
            message("Vertex collapsed, face vertex intersection, %10.10e, %10.10e", d1, d2);
            std::cout << ti << std::endl;
            std::cout << res << std::endl;
            std::cout << bary << std::endl;
            message("root = %10.10e", root);
            message("distance = %10.10e", ti.getSignedDistance(res));
            return true;
          }
        }

        itv1++;
        itv2++;
      }

      it1a++;
      it2a++;
      it1b++;
      it2b++;
    }

    return false;
  }

  /**************************************************************************/
  template class OrientedVertex<float>;
  template class OrientedVertex<double>;

  /**************************************************************************/
  template bool orientedVertexCollapsed(const OrientedVertex<float>& v1,
                                        const OrientedVertex<float>& v2);

  /**************************************************************************/
  template bool orientedVertexCollapsed(const OrientedVertex<double>& v1,
                                        const OrientedVertex<double>& v2);

}
