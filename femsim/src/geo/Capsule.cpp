/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/Capsule.hpp"
#include "geo/Cylinder.hpp"
#include "geo/Sphere.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  inline T getBarycentricCoordinatesEdge(Vector4<T>& v0,
                                         Vector4<T>& v1){
    T dot00 = dot(v0, v0);
    T dot01 = dot(v0, v1);
    T u = (dot01)/(dot00);
    return u;
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>::Capsule():radius(0){
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>::Capsule(T r):radius(r){
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>::Capsule(T r,
                      const Vector4<T>& s,
                      const Vector4<T>& e):radius(r),
                                           start(s),
                                           end(e){
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>::Capsule(const Capsule<T>& capsule):radius(capsule.radius),
                                                 start(capsule.start),
                                                 end(capsule.end){
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>::~Capsule(){
  }

  /**************************************************************************/
  template<class T>
  Capsule<T>& Capsule<T>::operator=(const Capsule<T>& cyl){
    if(this != &cyl){
      radius = cyl.radius;
      start  = cyl.start;
      end    = cyl.end;
    }
    return *this;
  }

  /**************************************************************************/
  template<class T>
  T Capsule<T>::signedDistance(const Vector4<T>& p, T* palpha)const{
    Cylinder<T> cyl(radius, start, end);
    T alpha = 0.0;

    T distance = cyl.signedDistance(p, &alpha);

    if(palpha){
      *palpha = alpha;
    }

    if(alpha < 0.0){
      Sphere<T> sphere(radius, start);
      return sphere.signedDistance(p);
    }else if(alpha > 1.0){
      Sphere<T> sphere(radius, end);
      return sphere.signedDistance(p);
    }else{
      return distance;
    }
  }

  /**************************************************************************/
  template<class T>
  T Capsule<T>::signedDistance(const Vector4<T>& a,
                               const Vector4<T>& b,
                               T* palpha,
                               T* pbeta,
                               bool* pparallel)const{
    Cylinder<T> cyl(radius, start, end);

    T alpha = 0.0;
    T beta  = 0.0;
    bool parallel = false;

    T distance = cyl.signedDistance(a, b, &alpha, &beta, &parallel);

    if(palpha){
      *palpha = alpha;
    }

    if(pbeta){
      *pbeta = beta;
    }

    if(pparallel){
      *pparallel = parallel;
    }

    if(alpha < 0.0){
      Sphere<T> sphere(radius, start);
      return sphere.signedDistance(a, b, pbeta);
    }else if(alpha > 1.0){
      Sphere<T> sphere(radius, end);
      return sphere.signedDistance(a, b, pbeta);
    }else{
      return distance;
    }
  }

  /**************************************************************************/
  template<class T>
  bool Capsule<T>::intersectionWithLine(const Vector4<T>& a,
                                        const Vector4<T>& b,
                                        Vector4<T>* ca,
                                        Vector4<T>* cb,
                                        T* palpha1,
                                        T* pbeta1,
                                        T* palpha2,
                                        T* pbeta2,
                                        bool* pparallel)const{
    Cylinder<T> cyl(radius, start, end);

    T alpha1, beta1, alpha2, beta2;
    bool parallel = false;

    bool intersects = cyl.intersectionWithLine(a, b, ca, cb,
                                               &alpha1, &beta1,
                                               &alpha2, &beta2, &parallel);

    if(pparallel){
      *pparallel = parallel;
    }

    if(parallel){
      return false;
    }

    if(!intersects){
      if(parallel){
        Sphere<T> sphereStart(radius, start);
        Sphere<T> sphereEnd(radius, end);

        /*Since the line is parallel to the cylinder, there are either
          no or four intersection points.*/
        Vector4<T> startA, startB, endA, endB;
        bool iA = sphereStart.intersectionWithLine(a, b, &startA, &startB);
        bool iB = sphereEnd  .intersectionWithLine(a, b, &endA,   &endB);

        if(!iA || !iB){
          return false;
        }

#if 0
        message("intersection points");
        std::cout << startA << std::endl;
        std::cout << startB << std::endl;
        std::cout << endA << std::endl;
        std::cout << endB << std::endl;
#endif

        Vector4<T> v1, v2, v3, v4;
        v1 = startA - start;
        v2 = startB - start;

        v3 = endA - start;
        v4 = endB - start;

        Vector4<T> ee = end - start;

        T b1 = getBarycentricCoordinatesEdge(ee, v1);
        T b2 = getBarycentricCoordinatesEdge(ee, v2);
        T b3 = getBarycentricCoordinatesEdge(ee, v3);
        T b4 = getBarycentricCoordinatesEdge(ee, v4);

        T first = Min(Min(b1, b2), Min(b3, b4));
        T last  = Max(Max(b1, b2), Max(b3, b4));

        //message("bary :: %10.10e, %10.10e", first, last);

        Vector4<T> pa = start + first * ee;
        Vector4<T> pb = start + last  * ee;

        if(palpha1){
          *palpha1 = first;
        }

        if(palpha2){
          *palpha2 = last;
        }

        Vector4<T> ee2 = b - a;
        v1 = pa - a;
        v2 = pb - a;

        b1 = getBarycentricCoordinatesEdge(ee2, v1);
        b2 = getBarycentricCoordinatesEdge(ee2, v2);

        if(pbeta1){
          *pbeta1 = b1;
        }

        if(pbeta2){
          *pbeta2 = b2;
        }

        if(ca){
          *ca = a + b1 * ee2;
        }

        if(cb){
          *cb = a + b2 * ee2;
        }


        return true;

      }
      return false;
    }else{
      bool i1 = false;
      bool i2 = false;

      if(alpha1 < 0.0){
        Sphere<T> sphere(radius, start);
        Vector4<T> pa, pb;
        T b1 = (T)0.0;
        T b2 = (T)0.0;

        i1 = sphere.intersectionWithLine(a, b, &pa, &pb, &b1, &b2);

        if(Abs(beta1 - b1) < Abs(beta1 - b2)){
          beta1 = b1;
          *ca = pa;
        }else{
          beta1 = b2;
          *ca = pb;
        }

      }else if(alpha1 > 1.0){

        Sphere<T> sphere(radius, end);
        Vector4<T> pa, pb;
        T b1 = (T)0.0;
        T b2 = (T)0.0;

        i1 = sphere.intersectionWithLine(a, b, &pa, &pb, &b1, &b2);

        if(Abs(beta1 - b1) < Abs(beta1 - b2)){
          beta1 = b1;
          *ca = pa;
        }else{
          beta1 = b2;
          *ca = pb;
        }

      }else{
        i1 = intersects;
      }

      if(alpha2 < 0.0){
        Sphere<T> sphere(radius, start);
        Vector4<T> pa, pb;
        T b1 = (T)0.0;
        T b2 = (T)0.0;

        i2 = sphere.intersectionWithLine(a, b, &pa, &pb, &b1, &b2);

        if(Abs(beta2 - b1) < Abs(beta2 - b2)){
          beta2 = b1;
          *cb = pa;
        }else{
          beta2 = b2;
          *cb = pb;
        }

      }else if(alpha2 > 1.0){

        Sphere<T> sphere(radius, end);
        Vector4<T> pa, pb;
        T b1 = (T)0.0;
        T b2 = (T)0.0;

        i2 = sphere.intersectionWithLine(a, b, &pa, &pb, &b1, &b2);

        if(Abs(beta2 - b1) < Abs(beta2 - b2)){
          beta2 = b1;
          *cb = pa;
        }else{
          beta2 = b2;
          *cb = pb;
        }
      }else{
        i2 = intersects;
      }

      if(!i1 || !i2){

        return false;
      }

      return true;
    }
  }

  template class Capsule<float>;
  template class Capsule<double>;
}
