/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/Triangle.hpp"
#include "math/Matrix33.hpp"
#include "math/Matrix22.hpp"
#include "math/Vector3.hpp"
#include "math/Vector2.hpp"
#include "geo/Barycentric.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  Triangle<T>::Triangle(){
  }

  /**************************************************************************/
  template<class T>
  Triangle<T>::Triangle(const Vector4<T>& a,
                        const Vector4<T>& b,
                        const Vector4<T>& c){
    v[0] = a, v[1] = b, v[2] = c;
    computeNormal();
  }

  /**************************************************************************/
  template<class T>
  Triangle<T>::Triangle(const Triangle<T>& t){
    if(&t != this){
      v[0] = t.v[0];
      v[1] = t.v[1];
      v[2] = t.v[2];

      n = t.n;

      //computeNormal();
    }
  }

  /**************************************************************************/
  template<class T>
  void Triangle<T>::computeNormal(){
    n = cross(v[1] - v[0], v[2] - v[0]);

    T normalLength = n.length();

    if(normalLength < DET_EPS || IsNan(normalLength)){
      warning("normal length = %10.10e", normalLength);
      throw new DegenerateCaseException(__LINE__, __FILE__, "singular face");
    }

    n /= normalLength;
  }

  /**************************************************************************/
  template<class T>
  Triangle<T>& Triangle<T>::operator=(const Triangle<T>& t){
    if(&t != this){
      v[0] = t.v[0];
      v[1] = t.v[1];
      v[2] = t.v[2];
      n = t.n;
    }
    return *this;
  }

  /**************************************************************************/
  template<class T>
  void Triangle<T>::set(const Vector4<T>& a,
                        const Vector4<T>& b,
                        const Vector4<T>& c){
    v[0] = a;
    v[1] = b;
    v[2] = c;

    computeNormal();
  }

  /**************************************************************************/
  /*Returns projection of p on the triangle*/
  template<class T>
  Vector4<T> Triangle<T>::getProjection(const Vector4<T>& p)const{
    Vector4<T> vp = p - v[0];

    T distance = dot(vp, n);
    return p - distance * n;
  }

  /**************************************************************************/
  /*Projects a vector on the plane of the triangle*/
  template<class T>
  Vector4<T> Triangle<T>::getProjectedVector(const Vector4<T>& vec)const{
    Vector4<T> r;
    if(vec.length() > 1E-8){
      Vector4<T> vp = cross(vec, n).normalized();
      vp = cross(vp, n);
      r = vp * dot(vp, vec);
    }
    return r;
  }

  /**************************************************************************/
  /*Checks for an intersection of a ray, defined by a point p and
    vector dir. If there is an intersection, the function returns true
    and res contains the intersection point and t such that res = p +
    dir * t*/
  template<class T>
  bool Triangle<T>::rayIntersection(const Vector4<T>& p,
                                    const Vector4<T>& dir,
                                    Vector4<T>& res,
                                    T* t,
                                    Vector4<T>* bary,
                                    T deps,
                                    T* detp)const{
    /*Solve v0 + (v1-v0)*t1 + (v2-v0)*t2 = p0 + (p1-p0)*t3 where t1,
      t2 and t3 are the bary centric coordinates in the plane (t1 &
      t2) and on the line segment (t3)
    */
    Matrix33<T> A;
    A.m[0] = v[1][0] - v[0][0];
    A.m[1] = v[2][0] - v[0][0];
    A.m[2] = -dir[0];

    A.m[3] = v[1][1] - v[0][1];
    A.m[4] = v[2][1] - v[0][1];
    A.m[5] = -dir[1];

    A.m[6] = v[1][2] - v[0][2];
    A.m[7] = v[2][2] - v[0][2];
    A.m[8] = -dir[2];

    Vector3<T> b(p[0] - v[0][0],
                 p[1] - v[0][1],
                 p[2] - v[0][2]);

    T det = 0;
    Vector3<T> x = A.inverse(&det) * b;
    Vector4<T> bb((T)1.0 - x.m[0] - x.m[1], x.m[0], x.m[1], 0);

    if(detp) *detp = det;
    if(t)   *t = -(T)1000;

    if(Abs(det) <= DET_EPS){
      /*Point does not move-> singular system, i.e., dir = 0*/

      Vector4<T> bb;
      T sd = getSignedDistance(p, &bb);

      if(Abs(sd) < 1E-6){
        /*Point does not move, but lies on the plane, check
          barycentric coordinates.*/
        if(bary) *bary = bb;

        res = p;
        return barycentricInTriangle(bb, deps);
      }

      res = p;

      if(t) *t = 0;

      res *= 0;
      if(bary) *bary = res;

      return false;
    }

    if(t)    *t = x.m[2];
    if(bary) *bary = bb;

    res = p + x.m[2] * dir;

    if(x.m[2] > 1 || x.m[2] < 0){
      return false;
    }
    return barycentricInTriangle(bb, deps);
  }

  /**************************************************************************/
  /*Projects a vector on the normal of the plane*/
  template<class T>
  Vector4<T> Triangle<T>::getNormalProjectedVector(const Vector4<T>& vec)const{
    return dot(vec, n)*n;
  }

  /**************************************************************************/
  template<class T>
  T Triangle<T>::getPlaneDistance(const Vector4<T>& p)const{
    return getSignedDistance(p);
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Triangle<T>::getBarycentricCoordinates(const Vector4<T>& p)const{
    Vector4<T> v0 = v[1] - v[0];
    Vector4<T> v1 = v[2] - v[0];
    Vector4<T> v2 = p    - v[0];

    return getBarycentricCoordinatesFace(v0, v1, v2);
  }

  /**************************************************************************/
  template<class T>
  void Triangle<T>::getEdgeProjections(const Vector4<T>& p,
                                       Vector4<T>* ep)const{
    Vector4<T> ba = v[1]-v[0];
    Vector4<T> ca = v[2]-v[0];
    Vector4<T> bc = v[1]-v[2];
    Vector4<T> da = p   -v[0];
    Vector4<T> dc = p   -v[2];

    ba[3] = ca[3] = bc[3] = da[3] = dc[3] = 0;

    T tt[3];

    tt[0] = dot(ba, da)/dot(ba, ba);
    tt[1] = dot(ca, da)/dot(ca, ca);
    tt[2] = dot(bc, dc)/dot(bc, bc);

    ep[0] = v[0] + tt[0] * ba;
    ep[2] = v[0] + tt[1] * ca;
    ep[1] = v[2] + tt[2] * bc;

    ep[0][3] = ep[1][3] = ep[2][3] = 0;
  }

  /**************************************************************************/
  template<class T>
  void Triangle<T>::getClampedEdgeProjections(const Vector4<T>& p,
                                              Vector4<T>* ep)const{
    Vector4<T> ba = v[1]-v[0];
    Vector4<T> ca = v[2]-v[0];
    Vector4<T> bc = v[1]-v[2];
    Vector4<T> da = p   -v[0];
    Vector4<T> dc = p   -v[2];

    ba[3] = ca[3] = bc[3] = da[3] = dc[3] = 0;

    T tt[3];

    tt[0] = Clamp(dot(ba, da)/dot(ba, ba), (T)0.0, (T)1.0);
    tt[1] = Clamp(dot(ca, da)/dot(ca, ca), (T)0.0, (T)1.0);
    tt[2] = Clamp(dot(bc, dc)/dot(bc, bc), (T)0.0, (T)1.0);

    ep[0] = v[0] + tt[0] * ba;
    ep[2] = v[0] + tt[1] * ca;
    ep[1] = v[2] + tt[2] * bc;

    ep[0][3] = ep[1][3] = ep[2][3] = 0;
  }

  /**************************************************************************/
  template<class T>
  T Triangle<T>::getArea()const{
    Vector4<T> v0 = v[1] - v[0];
    Vector4<T> v1 = v[2] - v[0];
    v0[3] = v1[3] = 0;

    Vector4<T> normal = cross(v0, v1);
    return (T)0.5 * normal.length();
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Triangle<T>::getCoordinates(const Vector4<T>& bary)const{
    return v[0] * bary[0] + v[1] * bary[1] + v[2] * bary[2];
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Triangle<T>::clampWeights(const Vector4<T>& bary,
                                       T mn, T mx)const{
    T b0 = Clamp(bary[1], mn, mx);
    T b1 = Clamp(bary[2], mn, mx);
    T r  = Clamp((T)1.0 - (b0 + b1), mn, mx);
    warning("clamping of barycentric coordinates not correct!!");
    return Vector4<T>(r, b0, b1, 0);
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Triangle<T>::
  edgeClampedBarycentricCoordinates(const Vector4<T>& p)const{
    Vector4<T> bary = getBarycentricCoordinates(p);
    return bary;

    if(barycentricInTriangle(bary, (T)0.0)){
      return bary;
    }else{
      Vector4<T> ep[3];
      getClampedEdgeProjections(p, ep);

      Vector4<T> projection;

      T closestDistance = (T)10000.0;
      for(int i=0;i<3;i++){
        T dist = (ep[i] - p).length();
        if(dist < closestDistance){
          closestDistance = dist;
          projection = ep[i];
        }
      }
      return getBarycentricCoordinates(projection);
    }
  }

  /**************************************************************************/
  template<class T>
  Vector4<T> Triangle<T>::
  edgeCrossedBarycentricCoordinates(const Vector4<T>& p)const{
    Vector4<T> bary = getBarycentricCoordinates(p);
    //return bary;
    if(barycentricInTriangle(bary, (T)0.0)){
      return bary;
    }

    int edgeList[3][3] = {{0, 1, 2},
                          {1, 2, 0},
                          {2, 0, 1}};

    /*Check barycentric coordinates of edge*/

    Vector4<T> bestBary1, bestBary2;
    Vector4<T> p1, p2;

    int bestCandidate = -1;

    for(int i = 0;i<3;i++){
      Vector4<T> v1mv0   = v[edgeList[i][0]] - p;
      Vector4<T> ev1mev0 = v[edgeList[i][1]] - v[edgeList[i][2]];

      Vector4<T> bb = v[edgeList[i][2]] - p;

      T dd[4];
      dd[0] = dot(v1mv0, v1mv0);
      dd[1] = dot(v1mv0, ev1mev0);
      dd[2] = dd[1];
      dd[3] = dot(ev1mev0, ev1mev0);

      Matrix22<T> AtA2(dd);

      T determinant = 0;

      Matrix22<T> AtAi = AtA2.inverse(&determinant);

      if(Abs(determinant) <= DET_EPS){
        continue;
      }

      Vector2<T> solution = AtAi * Vector2<T>(dot(v1mv0, bb),
                                              dot(ev1mev0, bb));

      solution[1] *= (T)-1;

      Vector4<T> bary1((T)1.0 - solution[0], solution[0], 0, 0);
      Vector4<T> bary2((T)1.0 - solution[1], solution[1], 0, 0);

      if(bary1[0] >= 0.0 && bary1[0] <= 1.0 &&
         bary1[1] >= 0.0 && bary1[1] <= 1.0 &&
         bary2[0] >= 0.0 && bary2[0] <= 1.0 &&
         bary2[1] >= 0.0 && bary2[1] <= 1.0 ){
        bestBary1 = bary1;
        bestBary2 = bary2;

        bestCandidate = i;

        p1 =                 p +   (v1mv0)*solution[0];
        p2 = v[edgeList[i][2]] + (ev1mev0)*solution[1];
      }
    }

    if(bestCandidate == -1){
      return edgeClampedBarycentricCoordinates(p);
    }

    return getBarycentricCoordinates(p2);
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::barycentricInTriangle(const Vector4<T>& bary, T eps)const{
    if((bary[0] >= -eps) && (bary[0] <= (T)1.0+eps) &&
       (bary[1] >= -eps) && (bary[1] <= (T)1.0+eps) &&
       (bary[2] >= -eps) && (bary[2] <= (T)1.0+eps)){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::barycentricInTriangleBand(const Vector4<T>& bary,
                                              T eps)const{
    if(barycentricInTriangle(bary, (T)0.0)){
      /*Barycentric coordinates inside triangle, so it is also in
        the EPS band around the triangle.*/
      return true;
    }else{
      /*Barycentric outside triangle, compute edge clamped
        barycentric coordinates*/

      /*Check if there is a projection on one of the three edges*/

      Vector4<T> p = getCoordinates(bary);

      Vector4<T> ep[3];
      getClampedEdgeProjections(p, ep);

      Vector4<T> projection;

      T closestDistance = (T)10000.0;
      for(int i=0;i<3;i++){
        T dist = (ep[i] - p).length();
        if(dist < closestDistance){
          closestDistance = dist;
          projection = ep[i];
        }
      }

      if(closestDistance < eps){
        return true;
      }
      return false;
    }
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::barycentricInTriangleDist(const Vector4<T>& bary,
                                              T dist)const{
    return barycentricInTriangleBand(bary, dist);
    if(barycentricInTriangle(bary, (T)0.0)){
      return true;
    }else{
      Vector4<T> point = getCoordinates(bary);
      Vector4<T> clampedBary = clampWeights(bary, (T)0.0, (T)1.0);
      Vector4<T> clampedPoint = getCoordinates(clampedBary);

      T distance = (clampedPoint - point).length();

      if(distance < dist){
        return true;
      }
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::pointProjectsOnTriangle(const Vector4<T>& p, T eps,
                                            Vector4<T>* b)const{
    if(b){
      *b = getBarycentricCoordinates(p);
      return barycentricInTriangle(*b, eps);
    }

    Vector4<T> bary = getBarycentricCoordinates(p);
    return barycentricInTriangle(bary, eps);
  }

  /**************************************************************************/
  template<class T>
  T Triangle<T>::getSignedDistance2(const Vector4<T>& p, T* sgn)const{
    T r = getSignedDistance(p);
    *sgn = Sign(r);

    return r;
  }

  /**************************************************************************/
  template<class T>
  T Triangle<T>::getSignedDistance(const Vector4<T>& p,
                                   Vector4<T>*       bary,
                                   Vector4<T>*       projection,
                                   T*                ddd)const{
    /*Solve v0 + (v1-v0)*t1 + (v2-v0)*t2 = p for t1 and t2 using the
      normal equation.*/
    Vector4<T> v1mv0 = v[1]-v[0];
    Vector4<T> v2mv0 = v[2]-v[0];

    T dd[4];

    dd[0] = dot(v1mv0, v1mv0);
    dd[1] = dot(v1mv0, v2mv0);
    dd[2] = dd[1];
    dd[3] = dot(v2mv0, v2mv0);

    Matrix22<T> ATA(dd);

    Matrix22<T> ATAI = ATA.inverse(ddd);

    Vector4<T> b = p-v[0];

    Vector2<T> x = (ATAI*Vector2<T>(dot(v1mv0, b),
                                    dot(v2mv0, b)));

    Vector4<T> r = v[0] + v1mv0*x[0] + v2mv0*x[1];

    if(bary)       bary->set((T)1.0 - x[0] - x[1], x[0], x[1], 0);
    if(projection) *projection = r;

    return dot(p-r, n);
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::intersects(const Ray<T>& ray, Vector4<T>& colPoint,
                               T* t, Vector4<T>* bary)const{
    return rayIntersection(ray.getOrigin(),
                           ray.getDirection(), colPoint, t, bary);
  }

  /**************************************************************************/
  template<class T>
  bool Triangle<T>::lineIntersection(const Vector4<T>& p,
                                     const Vector4<T>& dir,
                                     Vector4<T>& res,
                                     T* t,
                                     Vector4<T>* bary,
                                     T deps,
                                     T* detp)const{
    /*Solve v0 + (v1-v0)*t1 + (v2-v0)*t2 = p0 + (p1-p0)*t3 where t1,
      t2 and t3 are the bary centric coordinates in the plane (t1 &
      t2) and on the line segment (t3)
    */

    Matrix33<T> A;
    A.m[0] = v[1][0] - v[0][0];
    A.m[1] = v[2][0] - v[0][0];
    A.m[2] = -dir[0];

    A.m[3] = v[1][1] - v[0][1];
    A.m[4] = v[2][1] - v[0][1];
    A.m[5] = -dir[1];

    A.m[6] = v[1][2] - v[0][2];
    A.m[7] = v[2][2] - v[0][2];
    A.m[8] = -dir[2];


    Vector3<T> b(p[0] - v[0][0],
                 p[1] - v[0][1],
                 p[2] - v[0][2]);

    T det = 0;

    Vector3<T> x = A.inverse(&det) * b;
    Vector4<T> bb((T)1.0 - x.m[0] - x.m[1], x.m[0], x.m[1], 0);

    if(detp) *detp = det;

    if(Abs(det) <= DET_EPS){
      /*Line segment parallel to face*/

      Vector4<T> bb;
      T sd = getSignedDistance(p, &bb);

      if(Abs(sd) < 1E-6){
        /*Point doe not move, but lies on the plane, check
          barycentric coordinates.*/
        if(bary) *bary = bb;
        if(t)    *t = 0;

        res = p;
        return barycentricInTriangle(bb, deps);
      }

      res = p;
      if(t) *t = 0;

      //res*=0;
      if(bary) *bary = bb;

      return barycentricInTriangle(bb, deps);
    }

    if(t){
      *t = x.m[2];
    }

    if(bary){
      *bary = bb;
    }

    res = p + x.m[2] * dir;

    return barycentricInTriangle(bb, deps);
  }

#if 0
  /**************************************************************************/
  /*Check for intersections with a plane defined by triangle tri*/
  template<class T>
  bool Triangle<T>::planeIntersection(const Triangle<T>& tri,
                                      Vector4<T>& res1,
                                      Vector4<T>& res2,
                                      T* t1,
                                      T* t2,
                                      Vector4<T>* bary1,
                                      Vector4<T>* bary2,
                                      Vector4<T>* bary3,
                                      Vector4<T>* bary4,
                                      T beps,
                                      T* detp1,
                                      T* detp2)const{
    int code = 0;
    T d1 = getSignedDistance(tri.v[0]);
    T d2 = getSignedDistance(tri.v[1]);
    T d3 = getSignedDistance(tri.v[2]);

    PRINT(d1);
    PRINT(d2);
    PRINT(d3);

    if(d1 < (T)0.0){
      code += 4;
    }
    if(d2 < (T)0.0){
      code += 2;
    }
    if(d3 < (T)0.0){
      code += 1;
    }

    PRINT(code);
    DBG(message("code = %d, %f, %f, %f", code, d1, d2, d3));

    if(edgeIntersections[code * 3] == 0 ||
       edgeIntersections[code * 3] == 7){
      return false;
    }

    int edge1 = edgeIntersections[code * 3 + 1];
    int edge2 = edgeIntersections[code * 3 + 2];

    DBG(message("edges = %d, %d", edge1, edge2));

    /*Compute intersections with edges*/
    lineIntersection(tri.v[edgeVertices[edge1*2+0]],
                     tri.v[edgeVertices[edge1*2+1]] -
                     tri.v[edgeVertices[edge1*2+0]],
                     res1, t1, 0, beps, detp1);

    lineIntersection(tri.v[edgeVertices[edge2*2+0]],
                     tri.v[edgeVertices[edge2*2+1]] -
                     tri.v[edgeVertices[edge2*2+0]],
                     res2, t2, 0, beps, detp2);

    *bary1 =     getBarycentricCoordinates(res1);
    *bary2 =     getBarycentricCoordinates(res2);
    *bary3 = tri.getBarycentricCoordinates(res1);
    *bary4 = tri.getBarycentricCoordinates(res2);

    return true;
  }
#endif

  /**************************************************************************/
  template<class T>
  std::ostream& operator<<(std::ostream& os, const Triangle<T>& t){
    os << "Triangle vertices (\n" << t.v[0] << t.v[1] << t.v[2] << ")" <<std::endl;
    os << "Triangle normal (" << t.n << ")" << std::endl;
    return os;
  }

  /*Given two triangles and two vertices where x, v is the initial
    configuration and p, vu is the current configuration, find the
    intersection point and edges for::

    x*(1-c)+p*(c) = v*(1-c)+vu*(c)

    for some value of c.

    If the initial distance (x-v) and the current distance (p-vu)
    differs in sign, there must be a root for c between 0 and 1. This
    root can be found using a root-finder. In this case we use the
    Secant method.
  */

  /**************************************************************************/
  template<class T>
  class Interpolator<T, Triangle<T>, Vector4<T> >{
  public:
    /**************************************************************************/
    Interpolator(){
      rigidFace   = false;
      rigidVertex = false;
      initialized = false;
      derivative  = false;

      rt1.set(0,0,0,1);
      rv1.set(0,0,0,1);

      rt2.set(0,0,0,1);
      rv2.set(0,0,0,1);
    }

    /**************************************************************************/
    void init(){
      if(!initialized){
        A = t1.v[0];
        B = t1.v[1];
        C = t1.v[2];

        D = t2.v[0];
        E = t2.v[1];
        F = t2.v[2];

        G = v1;
        H = v2;

        initialized = true;
      }
    }

    /**************************************************************************/
    T evaluate(T s){
      if(derivative){
        return (localEvaluate(s + dx) - localEvaluate(s - dx))/((T)2.0 * dx);
      }else{
        return localEvaluate(s);
      }
    }

    /**************************************************************************/
    T localEvaluate(T s){
      if(rigidFace){
        //Vector4f comt = comt1 + (comt2-comt1)*s;
        Vector4<T> comt = comt1 * ((T)1.0 - s) + comt2 * s;

        Quaternion<T> rt = slerp(rt1, rt2, s)*rt1.inverse();

        Vector4<T> v11 = comt + rt * (A - comt1);
        Vector4<T> v12 = comt + rt * (B - comt1);
        Vector4<T> v13 = comt + rt * (C - comt1);

        t1.set(v11, v12, v13);
      }else{
        Vector4<T> v11 = A + (D - A) * s;
        Vector4<T> v12 = B + (E - B) * s;
        Vector4<T> v13 = C + (F - C) * s;

        t1.set(v11, v12, v13);
      }

      if(rigidVertex){
        Vector4<T> comv = comv1 * ((T)1.0 - s) + comv2 * s;

        Quaternion<T> rv = slerp(rv1, rv2, s) * rv1.inverse();

        v1 = comv + rv * (G - comv1);
      }else{
        v1 = G + (H - G)*s;
      }

      return t1.getSignedDistance(v1) - eps;
    }

    /**************************************************************************/
    void getResult(T s, Triangle<T>* t, Vector4<T>* v){
      if(rigidFace){
        Vector4<T> comt = comt1 * ((T)1.0 - s) + comt2 * s;

        Quaternion<T> rt = slerp(rt1, rt2, s) * rt1.inverse();

        Vector4<T> v11 = comt + rt * (A - comt1);
        Vector4<T> v12 = comt + rt * (B - comt1);
        Vector4<T> v13 = comt + rt * (C - comt1);

        t->set(v11, v12, v13);
      }else{
        Vector4<T> v11 = A + (D - A) * s;
        Vector4<T> v12 = B + (E - B) * s;
        Vector4<T> v13 = C + (F - C) * s;

        t->set(v11, v12, v13);
      }

      if(rigidVertex){
        Vector4<T> comv = comv1 * ((T)1.0 - s) + comv2 * s;

        Quaternion<T> rv = slerp(rv1, rv2, s) * rv1.inverse();

        *v = comv + rv * (G - comv1);
      }else{
        *v = G + (H - G) * s;
      }
    }

    /**************************************************************************/
    void setComputeDerivative(bool _der, T _dx){
      derivative = _der;
      dx = _dx;
    }

    /**************************************************************************/
    Triangle<T> t1;
    Triangle<T> t2;

    Vector4<T> v1;
    Vector4<T> v2;

    T eps;

    Vector4<T> comt1;
    Vector4<T> comt2;
    Vector4<T> comv1;
    Vector4<T> comv2;

    Quaternion<T> rt1;
    Quaternion<T> rt2;
    Quaternion<T> rv1;
    Quaternion<T> rv2;

    bool rigidFace;
    bool rigidVertex;

  protected:
    Vector4<T> A, B, C; //t1
    Vector4<T> D, E, F; //t2
    Vector4<T> G;       //v1
    Vector4<T> H;       //v2
    bool initialized;

    bool derivative;
    T dx;
  };

#define TRI_TOL 1E-7

  /**************************************************************************/
  template<class T>
  int intersection(const Triangle<T>& p, const Triangle<T>& x,
                   const Vector4<T>& vu, const Vector4<T>& v,
                   Vector4<T>& res,  const T eps,
                   Vector4<T>* bary1, Triangle<T>* ppp, T deps,
                   T normalFactor, int method, T* s, bool forced){
    Vector4<T> v11, v12, v13, v2;
    Triangle<T> pp;
    Vector4<T> b1;

    Interpolator<T, Triangle<T>, Vector4<T> > func;
    func.rigidFace = false;
    func.rigidVertex = false;
    func.t1 = x;
    func.t2 = p;
    func.v1 = v;
    func.v2 = vu;
    func.eps = eps;

    //T root = brentDekker<T, Triangle<T>, Vector4<T> >(&func, 0, 1,
    //                                                (T)TRI_TOL);

    DoubleRootResult<T> root;

    //try{
      root = doubleRootFinder<T, Triangle<T>, Vector4<T> >(&func, 0, 1,
                                                           (T)EDGE_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &res);
      pp.getSignedDistance(res, &b1);

      if(s)*s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to negative*/

      if(root.functionValues[1] > 0.0){
        func.getResult(root.roots[1], &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(s)*s = root.roots[1];
      }else{
        func.getResult(root.roots[0], &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(s)*s = root.roots[0];
      }
    }else{
#if 0
      if(forced){
        func.getResult((T)0.0, &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(ppp)*ppp = pp;
        if(bary1)*bary1 = b1;

        return 1;
      }
#endif
      return 0;
    }

    //func.getResult(root, &pp, &res);
    //pp.getSignedDistance(res, &b1);

    //if(s)*s = root;
    if(ppp)*ppp = pp;
    if(bary1)*bary1 = b1;

    if(forced){
      return 1;
    }

    if(pp.barycentricInTriangleDist(b1, deps)){
      return 1;
    }

    //if(pp.barycentricInTriangle(b1, deps)){
    //  return 1;
    //}
    return 0;
  }

  /**************************************************************************/
  template<class T>
  inline int rigidIntersection(const Triangle<T>& p, const Vector4<T>& cp,
                               const Quaternion<T>& rp,
                               const Triangle<T>& t, const Vector4<T>& ct,
                               const Quaternion<T>& rt,
                               const Vector4<T>& vu, const Vector4<T>& cvu,
                               const Quaternion<T>& rvu,
                               const Vector4<T>& v, const Vector4<T>& cv,
                               const Quaternion<T>& rv,
                               Vector4<T>& res, const T eps,
                               Vector4<T>* bary1, Triangle<T>* ppp, T deps,
                               T normalFactor, int method, bool rigidFace,
                               bool rigidVertex, T* s, bool forced){
    Vector4<T> v11, v12, v13, v2;
    Triangle<T> pp;
    Vector4<T> b1;

    Interpolator<T, Triangle<T>, Vector4<T> > func;
    func.rigidFace   = rigidFace;
    func.rigidVertex = rigidVertex;
    func.t1    = t;
    func.comt1 = ct;
    func.rt1   = rt;
    func.t2    = p;
    func.comt2 = cp;
    func.rt2   = rp;

    func.v1    = v;
    func.comv1 = cv;
    func.rv1   = rv;
    func.v2    = vu;
    func.comv2 = cvu;
    func.rv2   = rvu;

    func.eps   = eps;

    //T root = brentDekker<T, Triangle<T>, Vector4<T> >(&func, 0, 1,
    //                                                (T)TRI_TOL);

    //func.getResult(root, &pp, &res);
    //pp.getSignedDistance(res, &b1);

    //*s = root;

    DoubleRootResult<T> root;

    //try{
    root = doubleRootFinder<T, Triangle<T>, Vector4<T> >(&func, (T)0.0, (T)1.0,
                                                           (T)EDGE_TOL);
      /*}catch(Exception* e){
      warning("ignore not computed root, use zero");
      delete e;
      root.defined[0] = true;
      root.roots[0] = 0.0;
      }*/

    if(!root.defined[1] && root.defined[0]){
      /*Standard single root*/
      func.getResult(root.roots[0], &pp, &res);
      pp.getSignedDistance(res, &b1);

      if(s)*s = root.roots[0];
    }else if(root.defined[0] && root.defined[1]){
      /*Double root*/
      /*We are only interested in a crossing from positive to negative*/

      //if(root.functionValues[1] > 0.0){
      if(root.derivatives[0] < 0.0){
        func.getResult(root.roots[0], &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(s)*s = root.roots[0];
      }else{
        func.getResult(root.roots[1], &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(s)*s = root.roots[1];
      }
    }else{
#if 1
      if(forced){
        func.getResult((T)0.0, &pp, &res);
        pp.getSignedDistance(res, &b1);

        if(ppp)*ppp = pp;
        if(bary1)*bary1 = b1;

        return 1;
      }
#endif
      return 0;
    }


    if(ppp)*ppp = pp;
    if(bary1)*bary1 = b1;

    if(forced){
      return 1;
    }

    if(pp.barycentricInTriangle(b1, deps)){
      return 1;
    }
#if 0
    else{

      //b1 = t.getBarycentricCoordinates(v);

      //if(t.barycentricInTriangle(b1, deps)){
      //  return 1;

      //}else{
        b1 = p.getBarycentricCoordinates(vu);

        if(p.barycentricInTriangle(b1, deps)){
          return 1;
        }
        //}
    }

    //if(pp.barycentricInTriangleDist(b1, deps)){
    //  return 1;
    //}
#endif
    return 0;

  }

  /**************************************************************************/
  template class Triangle<float>;
  template class Triangle<double>;

  /**************************************************************************/
  template std::ostream& operator<<(std::ostream& os, const Triangle<float>& t);
  template std::ostream& operator<<(std::ostream& os, const Triangle<double>& t);

  /**************************************************************************/
  template int intersection(const Triangle<float>& p,
                            const Triangle<float>& x,
                            const Vector4<float>& vu,
                            const Vector4<float>& v,
                            Vector4<float>& res,
                            const float eps,
                            Vector4<float>* bary1,
                            Triangle<float>* ppp,
                            float deps,
                            float normalFactor, int method, float* s,
                            bool forced);

  /**************************************************************************/
  template int intersection(const Triangle<double>& p,
                            const Triangle<double>& x,
                            const Vector4<double>& vu,
                            const Vector4<double>& v,
                            Vector4<double>& res,
                            const double eps,
                            Vector4<double>* bary1,
                            Triangle<double>* ppp,
                            double deps,
                            double normalFactor, int method, double* s,
                            bool forced);

  /**************************************************************************/
  template int rigidIntersection(const Triangle<float>& p,
                                 const Vector4<float>& cp,
                                 const Quaternion<float>& rp,
                                 const Triangle<float>& t,
                                 const Vector4<float>& ct,
                                 const Quaternion<float>& rt,
                                 const Vector4<float>& vu,
                                 const Vector4<float>& cvu,
                                 const Quaternion<float>& rvu,
                                 const Vector4<float>& v,
                                 const Vector4<float>& cv,
                                 const Quaternion<float>& rv,
                                 Vector4<float>& res, const float eps,
                                 Vector4<float>* bary1,
                                 Triangle<float>* ppp,
                                 float deps,
                                 float normalFactor,
                                 int method, bool rigidFace,
                                 bool rigidVertex, float* s, bool forced);

  /**************************************************************************/
  template int rigidIntersection(const Triangle<double>& p,
                                 const Vector4<double>& cp,
                                 const Quaternion<double>& rp,
                                 const Triangle<double>& t,
                                 const Vector4<double>& ct,
                                 const Quaternion<double>& rt,
                                 const Vector4<double>& vu,
                                 const Vector4<double>& cvu,
                                 const Quaternion<double>& rvu,
                                 const Vector4<double>& v,
                                 const Vector4<double>& cv,
                                 const Quaternion<double>& rv,
                                 Vector4<double>& res, const double eps,
                                 Vector4<double>* bary1,
                                 Triangle<double>* ppp,
                                 double deps,
                                 double normalFactor,
                                 int method, bool rigidFace,
                                 bool rigidVertex, double* s, bool forced);
}
