/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/Sphere.hpp"
#include "math/Vector2.hpp"
#include "math/Matrix22.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  inline T getBarycentricCoordinatesEdge(Vector4<T>& v0,
                                         Vector4<T>& v1){
    T dot00 = dot(v0, v0);
    T dot01 = dot(v0, v1);
    T u = (dot01)/(dot00);
    return u;
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>::Sphere():radius(0.0){
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>::Sphere(T r):radius(r){
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>::Sphere(T r, const Vector4<T>& c):radius(r), center(c){
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>::Sphere(const Sphere<T>& sphere):radius(sphere.radius),
                                             center(sphere.center){
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>::~Sphere(){
  }

  /**************************************************************************/
  template<class T>
  Sphere<T>& Sphere<T>::operator=(const Sphere<T>& sphere){
    if(this != &sphere){
      radius = sphere.radius;
      center = sphere.center;
    }
    return *this;
  }

  /**************************************************************************/
  template<class T>
  T Sphere<T>::signedDistance(const Vector4<T>& p)const{
    Vector4<T> dist = p - center;
    return dist.length() - radius;
  }

  /**************************************************************************/
  template<class T>
  T Sphere<T>::signedDistance(const Vector4<T>& a,
                              const Vector4<T>& b, T* pbeta)const{
    Vector4<T> L = b - a;

    T LL = dot(L, L);

    if(LL < 1e-8){
      /*Degenerate line*/
      if(pbeta){
        *pbeta = 0.0;
      }
      return signedDistance(a);
    }

    /*Compute closest point on line*/
    T alpha = (dot(L, center) - dot(L, a))/LL;
    if(pbeta){
      *pbeta = alpha;
    }

    Vector4<T> P = a + alpha * L;
    return (P - center).length() - radius;
  }

  /**************************************************************************/
  template<class T>
  bool Sphere<T>::pointInside(const Vector4<T>& p)const{
    if(signedDistance(p) < 0.0){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  bool Sphere<T>::intersectionWithLine(const Vector4<T>& a,
                                       const Vector4<T>& b,
                                       Vector4<T>* ca,
                                       Vector4<T>* cb,
                                       T* pbeta1, T* pbeta2)const{
    Vector4<T> L = b - a;

    T LL = dot(L, L);

    if(LL < 1e-9){
      message("degenerate line");
      return false;
    }

    /*Compute closest point on line*/
    T alpha = (dot(L, center) - dot(L, a))/LL;

    Vector4<T> P = a + alpha * L;
    T dpc = (P - center).length();

    if(dpc > radius){
      /*Closest point further away from center than radius,
        intersection not possible*/
      return false;
    }else{
      T beta = Sqrt(Sqr(radius) - Sqr(dpc));

      L.normalize();

      *ca = P - beta * L;
      *cb = P + beta * L;

      Vector4<T> ee = b - a;
      Vector4<T> v1 = *ca - a;
      Vector4<T> v2 = *cb - a;

      if(pbeta1){
        *pbeta1 = getBarycentricCoordinatesEdge(ee, v1);
      }

      if(pbeta2){
        *pbeta2 = getBarycentricCoordinatesEdge(ee, v2);
      }

      return true;
    }
  }

  /**************************************************************************/
  template class Sphere<float>;
  template class Sphere<double>;
}
