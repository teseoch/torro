/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintSetTriangleEdge.hpp"
#include "collision/ConstraintCandidateTriangleEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace tsl{

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetTriangleEdge<T, CC>::ConstraintSetTriangleEdge():
    AbstractConstraintSet<T, Edge<T>, Vector4<T>, CC>(){
    this->typeName = "edge - vertex";
  }

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetTriangleEdge<T, CC>::~ConstraintSetTriangleEdge(){
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintSetTriangleEdge<T, CC>::evaluate(Edge<T>& p, Vector4<T>& com,
                                                  Quaternion<T>& rot,
                                                  const EvalType& etype,
                                                  EvalStats& stats){
    CandidateMapIterator it = this->candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    if(evaluateGeometry){
      /*Perform a simple plane vertex test for all candidates*/

      while(it != this->candidates.end()){
        Candidate* c = (*it++).getData();

        if(false){
          if(c->getStatus() == Enabled){
            /*An invalid candidate with an enabled constraint is not
              possible*/
            error("Invalid candidate is still enabled");
          }

          c->setInitial(this->ctx->mesh->vertices[c->getCandidateId()].coord);
          //this->ctx->mesh->getVertex(&c->v0, c->vertexId, false);
          //this->ctx->mesh->getTriangle(&c->t0, c->faceId, backFace);
        }

        /*Update geometry information of associated faces*/
        c->updateGeometry();

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/

        /*Evaluate signed distance of pair*/
        if(c->evaluate(p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform rootfinding*/
      this->basicRootFinding(p, com, rot,
                             changedCandidates, changedIndices,
                             constraintsChanged, ActiveConstraints<CC>());
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    if(evaluateConstraints){
      this->basicEvaluation(changedIndices, etype, stats,
                            constraintsChanged, true, ActiveConstraints<CC>());
    }

    if(evaluateGeometry){
      this->lastPosition     = p; /*Store last position*/
      this->lastCenterOfMass = com;
      this->lastOrientation  = rot;
    }
    return constraintsChanged;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintSetTriangleEdge<T, CC>::checkAndUpdate(Edge<T>& p,
                                                        Vector4<T>& com,
                                                        Quaternion<T>& rot,
                                                        EvalStats& stats,
                                                        bool friction,
                                                        bool force, T bnorm,
                                                        bool* allPositive){
    CandidateMapIterator it = this->candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    /*Simple plane point check for a fast detection in change*/
    while(it != this->candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->getStatus() != Enabled){
        continue;
      }

      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(c->getEnabledId());

      if(!ActiveConstraints<CC>()){
        if(cc->status != Active){
          //continue;
        }
      }

      /*Check corresponding lambda*/
      if(!cc->valid(*this->ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      DBG(message("update geometry"));
      /*Update geometry information of associated faces*/
      c->updateGeometry();

      if(false){
        if(c->getStatus() == Enabled){
          error("Invalid candidate is enabled");
        }
      }

      /*Update value for current distance*/
      c->evaluate(p, com, rot);

      /*Check geometric distance and state of the constraint*/
#if 0
      if(!this->basicUpdateCheck(c, p, com, rot,
                                 false, ActiveConstraints<CC>(),
                                 constraintsChanged, allPositive,
                                 stats,
                                 this->ctx->distance_epsilon,
                                 this->ctx->distance_tol,
                                 this->ctx->safety_distance,
                                 (T)2.0,
                                 (T)0.1,
                                 (T)1.0,
                                 (T)0.0)){
        continue;
      }
#endif
      if(true){
#if 0
      if(Abs(c->getCurrentDistance() - this->ctx->distance_tol) >
         (T)0.0*this->ctx->distance_tol /*&&
                                                                   c->tu.barycentricInTriangle(c->bary2, BARY_EPS)*/){
#endif
        warning("updating EV constraint, distance = %10.10e, %10.10e, %10.10e",
                c->getCurrentDistance(), c->getCurrentDistance() -
                this->ctx->distance_epsilon,
                c->getCurrentDistance() - this->ctx->distance_tol);
#if 1
        if(ActiveConstraints<CC>()){
#if 0
          if(!cc->valid(*this->ctx->getSolver()->getx())){
            if(c->getCurrentDistance() > this->ctx->safety_distance){
              /*Constraint is not active, distance positive*/
              if(cc->getStatus() != Active){
                c->disableConstraint();
              }
              continue;
            }
          }else{
            //if(c->getCurrentDistance() - this->ctx->distance_epsilon + this->ctx->distance_tol > (T)0.0){
            if(c->getCurrentDistance() < (T)(this->ctx->distance_epsilon +
                                             this->ctx->distance_tol) &&
               c->getCurrentDistance() > this->ctx->distance_tol){

              /*Constraint is active, distance positive*/
              if(cc->getStatus() != Active){
                c->disableConstraint();
              }
              continue;
            }
          }
#else//0
          if(!cc->valid(*this->ctx->getSolver()->getx())){
            if(c->getCurrentDistance() > this->ctx->distance_epsilon*2.0 ){
              /*Constraint is not active, distance positive*/
              /*Experimantal, just get rid of it*/
              message("not valid, distance = %10.10e, disableConstraint",
                      c->getCurrentDistance());
              c->disableConstraint();
              continue;
            }
          }
#endif//0
        }else{
          if(cc->status != Active){
            //if(false){
            if(c->getCurrentDistance() > this->ctx->distance_epsilon*2.0 ){
              /*Constraint is not active, distance positive*/
              /*Experimantal, just get rid of it*/
              c->disableConstraint();
              continue;
            }
            if(c->getCurrentDistance() > this->ctx->distance_epsilon*0.1 ){
              /*Constraint is not active, distance positive*/
              //continue;
            }
          }else{
            if(c->getCurrentDistance() - this->ctx->distance_epsilon +
               this->ctx->distance_tol > (T)0.0){
              //continue;
            }

            if(c->getCurrentDistance() - this->ctx->distance_epsilon -
               this->ctx->distance_tol > (T)0.0){
              //continue;
            }

            if(Abs(c->getCurrentDistance() -
                   this->ctx->distance_epsilon) < this->ctx->distance_tol){
              /*Constraint is active, distance positive*/
              //continue;
            }

#if 0
            {
              Vector4<T> weights =
                c->tu.getBarycentricCoordinates(p.getVertex());
              if(!c->tu.barycentricInTriangle(weights, (T)BARY_EPS)){
                c->disableConstraint();
                constraintsChanged = true;
                continue;
              }
            }
#endif//0
            DBG(message("distance in testA = %10.10e",
                        c->getCurrentDistance() - this->ctx->distance_epsilon +
                        this->ctx->distance_tol));

            DBG(message("distance in testB = %10.10e",
                        c->getCurrentDistance() - this->ctx->distance_epsilon -
                        this->ctx->distance_tol));
          }

#if 0
          if(!cc->valid(*this->ctx->getSolver()->getx())){
            if(c->getCurrentDistance() > this->ctx->safety_distance){
              /*Constraint is not active or multiplier is very small,
                distance is positive*/
              continue;
            }
          }else{
            /*Valid*/
            //if(c->getCurrentDistance() - this->ctx->distance_epsilon +
            //this->ctx->distance_tol > (T)0.0){
            //continue;
            //}

            if(Abs(c->getCurrentDistance() - this->ctx->distance_epsilon) <
               this->ctx->distance_tol){
              continue;
            }
          }
#endif//0
        }
#endif//1
        /*Geometric distance and constraint distance are not in sync, update*/
        (message("update EV constraint %d - %d",
                 this->setId, c->getCandidateId()));
        bool skipped = false;

        //c->updateConstraint(this->startPosition, p, com, rot, &skipped);

        if(ActiveConstraints<CC>()){
          if(!cc->valid(*this->ctx->getSolver()->getx())){
            c->disableConstraint();
            message("not valid, disable");
            continue;
          }else{
            if(c->getCurrentDistance() - this->ctx->distance_epsilon +
               this->ctx->distance_tol > (T)0.0){
              message("skip, distance = %10.10e", c->getCurrentDistance());
              continue;
            }

            if(c->getCurrentDistance() - this->ctx->distance_epsilon -
               this->ctx->distance_tol > (T)0.0){
              message("skip, distance = %10.10e", c->getCurrentDistance());
              continue;
            }
          }
        }else{
          if(cc->status != Active){
            c->disableConstraint();
            continue;
            if(c->getCurrentDistance() > this->ctx->distance_epsilon*1.0 ){
              continue;
            }
          }

          if(cc->status == Active){
            if(c->getCurrentDistance() - this->ctx->distance_epsilon +
               this->ctx->distance_tol > (T)0.0){
              continue;
            }

            if(c->getCurrentDistance() - this->ctx->distance_epsilon -
               this->ctx->distance_tol > (T)0.0){
              continue;
            }
          }
        }

        c->updateConstraint(this->startPosition, p, com, rot, &skipped, stats);

        if(skipped){
          //constraintsChanged = true;
          message("skipped");
          continue;
        }

        if(c->getCurrentDistance() < this->ctx->distance_epsilon){
          *allPositive = false;
        }

        //cc->status = Active;

        if(cc->status != Active){
          cc->status = Active;
          cc->frictionStatus[0] = Static;
          cc->frictionStatus[1] = Static;
        }

        message("Updated and enabled EV constraint");

        constraintsChanged = true;

        (message("EV CONSTRAINT NOT CORRECT\n\n\n"));
        (message("real distance = %10.10e", c->getCurrentDistance() +
                 this->ctx->safety_distance));
        (warning("EV CONSTRAINT NOT CORRECT %d, %d, backFace = %d\n\n\n",
                 this->setId, c->getCandidateId()));
        (warning("real distance = %10.10e", c->getCurrentDistance() +
                 this->ctx->safety_distance));
      }
      }
      return constraintsChanged;
    }

    /**************************************************************************/
    template<class T, class CC>
    bool ConstraintSetTriangleEdge<T, CC>::updateInitial(Edge<T>& p,
                                                         Vector4<T>& com,
                                                         Quaternion<T>& rot,
                                                         EvalStats& stats,
                                                         bool friction,
                                                         bool force, T bnorm){
    CandidateMapIterator it = this->candidates.begin();

    bool constraintsChanged = false;

    while(it != this->candidates.end()){
      Candidate* c = (*it++).getData();

      /*Update geometry information of associated faces*/
      c->updateGeometry();

      this->startPosition     = p;
      this->startCenterOfMass = com;
      this->startOrientation  = rot;

      bool skipped = false;
      c->updateInitial(this->startPosition, p, com, rot, &skipped, stats);

      if(skipped){
        constraintsChanged = true;
      }
    }
    return constraintsChanged;
  }

  /**************************************************************************/
  /*Updates the geometry information*/
  template<class T, class CC>
  void ConstraintSetTriangleEdge<T, CC>::update(){
    Edge<T> x;
    if(Mesh::isRigid(this->type)){
      return;
    }
    this->ctx->mesh->getEdge(&x, this->setId, false);

    Vector4<T> com(0,0,0,0);
    Quaternion<T> rot(0,0,0,1);

    if(Mesh::isRigid(this->type)){
      this->ctx->mesh->setCurrentEdge(this->setId);
      int objectId =
        this->ctx->mesh->vertexObjectMap[this->ctx->mesh->getOriginVertex()];

      com = this->ctx->mesh->centerOfMass[objectId];
      rot = this->ctx->mesh->orientations[objectId];
      return;
    }

    /*Find potential faces for vertex (vertexId)*/
    Tree<int> currentPotentials;
    this->ctx->extractPotentialEdgeVertices(this->setId, currentPotentials);

    /*Add new candidates which are note yet in the candidate list*/
    Tree<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      int index = this->candidatesTree.findIndex(candidate);

      if(index == -1){
        /*Current candidate not found in set, add*/

        if(Mesh::isRigidOrStatic(this->ctx->mesh->vertices[candidate].type)){
          continue;
        }

        Candidate* cc = new CandidateType(this->setId, candidate, this->ctx);

        cc->setCandidateType(this->ctx->mesh->vertices[candidate].type);
        cc->setSetType(this->type);

        this->candidates.insert(candidate, cc);
        this->candidatesTree.insert(candidate, 1);
      }
    }

    /*-----------------------------------------------------*/

    /*Check for disappeared candidates, remove them from the set.*/
    CandidateMapIterator tcit = this->candidates.begin();

    while(tcit != this->candidates.end()){
      Candidate* cc = (*tcit).getData();

      /*If not found in new potential set, remove.*/
      if(currentPotentials.findIndex(cc->getCandidateId()) == -1){
        /*Disable and remove*/
        /*Check if constraint is enabled*/

        if(cc->getStatus() == Enabled){
          //error("Deleting enabled constraint!!");
          tcit++;
        }else{
          cc->disableConstraint();
          this->candidates.remove(tcit);
          int cid = cc->getCandidateId();
          warning("Possible bug fixed here, was remove cc->edgeId, which was the setId, not candidateId");
          this->candidatesTree.remove(cid);
          delete cc;
        }
      }else{
        tcit++;
      }
    }

    /*Recompute states at beginning of timestep*/
    CandidateMapIterator it3 = this->candidates.begin();

    while(it3 != this->candidates.end()){
      Candidate* s = (*it3++).getData();
      s->recompute(this, x, com, rot);
    }

    /*-----------------------------------------------------*/

    this->startPosition     = x;
    this->startCenterOfMass = com;
    this->startOrientation  = rot;
    this->lastPosition      = x;
    this->lastCenterOfMass  = com;
    this->lastOrientation   = rot;
  }

  /**************************************************************************/
  template<class T, class CC>
    void ConstraintSetTriangleEdge<T, CC>::showVertexCandidateState(int vertex){

  }

  /**************************************************************************/
  template class ConstraintSetTriangleEdge<float, ContactConstraintCR<2, float> >;
  template class ConstraintSetTriangleEdge<double, ContactConstraintCR<2, double> >;
}
