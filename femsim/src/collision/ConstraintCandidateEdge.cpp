/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintCandidateEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "collision/ConstraintSetEdge.hpp"
#include "collision/ConstraintSet.hpp"

#define DEBUG_UPDATE
#undef DEBUG_UPDATE

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  ConstraintCandidateEdge<T, CC>::
  ConstraintCandidateEdge(int eid,
                          int oeid,
                          bool b,
                          CollisionContext<T, CC>* ctx):
    AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>(b, ctx){
    this->candidateId = eid;
    this->setId = oeid;

    tangentialDefined = false;
    anyReferenceSet = false;
    collapsed = false;
    hasIntersectedBefore = false;

    refYx.set(1,1,1,0);
    refYx.normalize();
    refXy = -refYx;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateEdge<T, CC>::checkCollision(const Edge<T>& X,
                                                      const Vector4<T>& com,
                                                      const Quaternion<T>& rot,
                                                      T* dist,
                                                      bool* plane){
    int colCode = -1;

    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    bool debug = false;

    if(false){

      debug = true;

      warning("check collision %d, %d, backFace = %d",
              this->candidateId, this->setId, this->backFace);
    }

    T root = (T)0.0;

    try{
      if(!rigidEdge1 && !rigidEdge2){
        if(debug){
          warning("deformable intersection");
        }

        /*Test for intersection between path X0->X and Y0->Yu*/
        colCode = intersection(X, this->X0,
                               this->Yu, this->Y0,
                               colPoint,
                               this->ctx->safety_distance,
                               &baryi1, &baryi2,
                               &this->Xi, &this->Yi,
                               this->ctx->distance_epsilon, 1,
                               &refXy, &root);

        if(dist) *dist = root;

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){

        }else{
          return false;
        }
      }else{
        if(debug){
          warning("rigid intersection, com, comp0, comeu, come0, rots");
          std::cerr << com << std::endl;
          std::cerr << this->comX0 << std::endl;
          std::cerr << this->comYu << std::endl;
          std::cerr << this->comY0 << std::endl;
          std::cerr << rot << std::endl;
          std::cerr << this->rotX0 << std::endl;
          std::cerr << this->rotYu << std::endl;
          std::cerr << this->rotY0 << std::endl;
        }

        /*Test for intersection between path X0->X and Y0->Yu*/
        colCode = rigidIntersection(X,  com,   rot,
                                    this->X0, this->comX0, this->rotX0,
                                    this->Yu, this->comYu, this->rotYu,
                                    this->Y0, this->comY0, this->rotY0,
                                    colPoint,
                                    this->ctx->safety_distance,
                                    &baryi1, &baryi2,
                                    &this->Xi, &this->Yi,
                                    this->ctx->distance_epsilon, 1,
                                    rigidEdge1, rigidEdge2, &root,
                                    &refXy);

        if(debug){
          message("root = %10.10e", root);
        }

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          this->comXi = this->comX0 * ((T)1.0 - root) + root * com;
          this->comYi = this->comY0 * ((T)1.0 - root) + root * this->comYu;

          this->rotXi = slerp(this->rotX0, rot,         root).normalized();
          this->rotYi = slerp(this->rotY0, this->rotYu, root).normalized();
        }else{
          return false;
        }
        if(dist) *dist = root;
      }
    }catch(Exception* e){
      message("Exception in root finding:: %s", e->getError().c_str());
      delete e;
      return false;
    }

    if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
      if(this->Yi.getTangentDistance(0) < 5e-4 ||
         this->Yi.getTangentDistance(1) < 5e-4 ||
         this->Xi.getTangentDistance(0) < 5e-4 ||
         this->Xi.getTangentDistance(1) < 5e-4){
        warning("tangent distances too small, wait for other constraints");
        return false;
      }
    }

    /*If the method has converged, then the distance between Yi and Xi
      is positive, hence, Yi.getOutwardNormal will give a properly
      defined normal*/

    refYx = this->Yi.getOutwardNormal(this->Xi, &refYx);
    refXy = -refYx;

    /*In case the edge is concave and the projection of the tangent
      vectors on the normal vector become 0.5 or more, we can skip
      this combination since a proper intersection is unlikely.*/

    if(dot(refYx, this->Yi.faceTangent(0)) > 0.5 ||
       dot(refYx, this->Yi.faceTangent(1)) > 0.5){
      std::cout << refYx << std::endl;
      message("1 normals vector for edge %d - %d too close to plane",
              this->candidateId, this->setId);
      return false;
    }

    if(dot(refXy, this->Xi.faceTangent(0)) > 0.5 ||
       dot(refXy, this->Xi.faceTangent(1)) > 0.5){
      std::cout << refYx << std::endl;
      message("2 normals vector for edge %d - %d too close to plane",
              this->candidateId, this->setId);
      return false;
    }

    bool deg = false;

    Vector4<T> pp1, pp2, bb1, bb2;

    /*Compute signed distance, projections of one edge on the other,
      with their Barycentric coordinates for the edges at the moment
      of contact.*/
    T sd = this->Yi.getSignedDistance(this->Xi,
                                      &pp1, &pp2,
                                      &bb1, &bb2,
                                      &deg, &refYx);

    if(deg){
      /*Degenerate geometry*/
      return false;
    }

    /*Signed distance must be positive for Yi Xi pair*/
    if(sd < 0.0 && colCode == 1){
      refYx = -refYx;
      refXy = -refXy;
      message("intersection0 = %d", this->intersection0);
      message("intersectionc = %d", this->intersectionc);
      message("d0 = %10.10e", this->distance0);
      message("dc = %10.10e", this->distancec);
      error("interpolated signed distance is negative, should be positive, sd = %10.10e", sd);
      return false;
    }

    if(debug){
      warning("colcode = %d", colCode);
      message("intersection0 = %d", this->intersection0);
      message("intersectionc = %d", this->intersectionc);
      message("d0 = %10.10e", this->distance0);
      message("dc = %10.10e", this->distancec);
      message("di = %10.10e", sd);

      warning("refYx");
      std::cerr << refYx << std::endl;
      std::cerr << this->Yi << std::endl;
      std::cerr << this->Xi << std::endl;

      message("input e0 X0, eu, pu");
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << X  << std::endl;

      getchar();
      bool deg = false;

      T sdc = this->Yu.getSignedDistance(X,
                                         &pp1, &pp2,
                                         &bb1, &bb2,
                                         &deg, &refYx);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      T sd0 = this->Y0.getSignedDistance(this->X0,
                                         &pp1, &pp2,
                                         &bb1, &bb2,
                                         &deg, &refYx);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      T sd = this->Yi.getSignedDistance(this->Xi,
                                        &pp1, &pp2,
                                        &bb1, &bb2,
                                        &deg, &refYx);

      std::cerr << pp1 << std::endl;
      std::cerr << pp2 << std::endl;
      std::cerr << bb1 << std::endl;
      std::cerr << bb2 << std::endl;

      message("sdc = %10.10e", sdc);
      message("sd0 = %10.10e", sd0);
      message("sd  = %10.10e", sd);

      warning("distance = %10.10e", sd);
      warning("root = %10.10e", *dist);

      getchar();
    }

    if(colCode == -1 || colCode == 0){
      /*No intersection*/
      return false;
    }

    dv = this->Yi.getOutwardNormal(this->Xi, &refYx);

    if(IsNan(dv)){
      std::cout << this->Yi << std::endl;
      std::cout << this->Xi << std::endl;
      std::cout << refYx << std::endl;
      std::cout << this->Yi.getOutwardNormal(this->Xi) << std::endl;
      std::cout << dv << std::endl;
      error("NaN");
    }

    if(dot(dv, refYx) < 0){
      error("new dv not in sync");
    }

    if(colCode == 1){
      Vector4<T> x1, x2;
      bool parallel = false;

      this->Yi.infiniteProjections(this->Xi, &x1, &x2, 0, 0, &parallel);

      if((x1-x2).length() > 2e-8){
        /*If the distance between x1 and x2 is too large, the
          collision might not be correct. The intersection test should
          return a configuration for which x1 and x2 are almost
          identical.*/
        return false;
      }

      if(parallel){
        /*Edges are parallel, skip.*/
        return false;
      }

      return true;
    }
    return false;
  }

  /**************************************************************************/
  /*Evaluate signed distance*/
  template<class T, class CC>
  bool ConstraintCandidateEdge<T, CC>::evaluate(const Edge<T>& edge,
                                                const Vector4<T>& com,
                                                const Quaternion<T>& rot,
                                                Vector4<T>* bb1,
                                                Vector4<T>* bb2,
                                                bool* updated){
    bool debug = false;

    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    if(false){
      warning("%d - %d, backFace = %d", this->candidateId,
              this->setId, this->backFace);

      debug = true;
    }

    /*Test for collapsed edges. An edge is collapsed if there is a
      change in convexity/concavity, and the face vertex of one of the
      two faces crosses the other face. In this case we can not detect
      a proper edge-edge intersection since the geometry is locally
      inverted. However, the corresponding vertex face intersections
      should resolve the collisions. Therefore we need to enforce
      these constraints instead of relying on the vertex-face
      collision detection. Additionally, since the geometry is locally
      inverted, we can not use it for a proper intersection test.*/
    bool collapsedY = edgeCollapsed(this->Y0, this->Yu);
    bool collapsedX = edgeCollapsed(this->X0, edge);

    if(this->backFace){
      this->ctx->collapsedBackEdges[this->candidateId] = collapsedY;
      this->ctx->collapsedBackEdges[this->setId] =       collapsedX;
    }else{
      this->ctx->collapsedEdges[this->candidateId] = collapsedY;
      this->ctx->collapsedEdges[this->setId] =       collapsedX;
    }

    if(this->Yu.getTangentDistance(0) < 5e-4 ||
       this->Yu.getTangentDistance(1) < 5e-4 ||
       edge.getTangentDistance(0) < 5e-4 ||
       edge.getTangentDistance(1) < 5e-4){

      /*At least one adjacent face of the edges is too flat or too small*/

      return false;
    }

    if(collapsedY || collapsedX){
      collapsed = true;

      warning("COLLAPSED %d - %d, backface = %d",
              this->candidateId, this->setId, this->backFace);

      message("collapsed Y = %d, X = %d", collapsedY, collapsedX);

      std::cerr << this->Y0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << edge << std::endl;

      /*If there is a transition from concave to convex, force the
        corresponding vertex/face pair to be active.

        If a back or frontface must be activated, depends on the
        configuration of the edge and the type of the edge. Not only
        on the type of the edge. If a frontface edge collapses, a
        backface constraint must be enabled if the initial
        configuration is convex and the current one is concave (and
        collapsed)*/

      if(collapsedY){
        if(this->Y0.isConvex() && this->Yu.isConcave()){
          if(!this->backFace){
            this->ctx->backSets[this->Y0.faceVertexId(0)].
              forceConstraint(this->Y0.faceId(1));

            this->ctx->backSets[this->Y0.faceVertexId(1)].
              forceConstraint(this->Y0.faceId(0));
          }else{
            this->ctx->sets[this->Y0.faceVertexId(0)].
              forceConstraint(this->Y0.faceId(1));

            this->ctx->sets[this->Y0.faceVertexId(1)].
              forceConstraint(this->Y0.faceId(0));
          }
        }
      }

      if(collapsedX){
        if(this->X0.isConvex() && edge.isConcave()){
          if(!this->backFace){
            this->ctx->backSets[this->X0.faceVertexId(0)].
              forceConstraint(this->X0.faceId(1));

            this->ctx->backSets[this->X0.faceVertexId(1)].
              forceConstraint(this->X0.faceId(0));
          }else{
            this->ctx->sets[this->X0.faceVertexId(0)].
              forceConstraint(this->X0.faceId(1));

            this->ctx->sets[this->X0.faceVertexId(1)].
              forceConstraint(this->X0.faceId(0));
          }
        }
      }

      if(debug){
        getchar();
      }

      /*Since this pair of edges contains at least one collapsed edge,
        we can't properly check for intersections, so we stop here.*/
      return false;
    }else{
      /*Explicitly reset this flag.*/
      collapsed = false;
    }

    /*Intersection test*/
    this->intersectionc = this->Yu.edgesIntersect(edge,
                                                  this->ctx->safety_distance,
                                                  rigidEdge1 || rigidEdge2,
                                                  debug);

    if(updated){
      *updated = true;
    }

    if(debug){
      message("prev refYx");
      std::cout << refYx << std::endl;
    }

    /*Update reference vectors*/
    refYx = this->Yu.getOutwardNormal(edge, &refYx);
    refXy = -refYx;

    if(debug){
      message("new refYx");
      std::cout << refYx << std::endl;
    }

    bool degenerate = false;

    /*Compute current distance*/
    this->distancec =
      this->Yu.getSignedDistance(edge, 0, 0, 0, 0, &degenerate, &refYx);

    if(debug){
      message("intersection0 = %d", this->intersection0);
      message("intersectionc = %d", this->intersectionc);
      message("distance0 = %10.10e", this->distance0);
      message("distancec = %10.10e", this->distancec);
      message("dc degenerate = %d", degenerate);
      message("e0.isConvex() = %d", this->Y0.isConvex());
      message("p0.isConvex() = %d", this->X0.isConvex());
      message("eu.isConvex() = %d", this->Yu.isConvex());
      message("pu.isConvex() = %d", edge.isConvex());
    }

    /*Compute initial distance*/
    this->distance0 =
      this->Y0.getSignedDistance(this->X0, 0, 0, 0, 0,
                                 &degenerate, &refYx);

    if(debug){
      message("d0 degenerate = %d", degenerate);
      message("distance0 = %10.10e", this->distance0);
    }

    if(this->status == Enabled){
      if(debug){
        message("constraint already enabled");

        CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

        cc->print(std::cout);
      }
      return false;
    }

#if 1
    if( ((/*this->intersection0 == false &&*/ this->intersectionc)
         )
       //&&
       //(Sign(this->distance0) != Sign(this->distancec))
        ){

      hasIntersectedBefore = true;

      if(Abs(this->distancec) < this->ctx->safety_distance){
        /*Current edge crosses through cylinder area, figure out
          outward normal*/
        warning("current distance too small %10.10e", this->distancec);

        /*Separate edges by displacing them in the negative normal
          direction, edges are convex by definition*/

        Vector4<T> normalFromFacesAndTangentsY =
          (this->Yu.faceNormal(0)  + this->Yu.faceNormal(1) -
           this->Yu.faceTangent(0) - this->Yu.faceTangent(1) ).normalized();

        Vector4<T> normalFromFacesAndTangentsX =
          (edge.faceNormal(0)  + edge.faceNormal(1) -
           edge.faceTangent(0) - edge.faceTangent(1) ).normalized();


        Vector4<T> vYu0 =
          this->Yu.vertex(0) - (T)1e-7 * normalFromFacesAndTangentsY;

        Vector4<T> vYu1 =
          this->Yu.vertex(1) - (T)1e-7 * normalFromFacesAndTangentsY;

        Vector4<T> fvYu0 =
          this->Yu.faceVertex(0) - (T)1e-7 * normalFromFacesAndTangentsY;

        Vector4<T> fvYu1 =
          this->Yu.faceVertex(1) - (T)1e-7 * normalFromFacesAndTangentsY;

        Edge<T> Yu2(vYu0, vYu1, fvYu0, fvYu1);

        Vector4<T> vXu0 =
          edge.vertex(0) - (T)1e-7 * normalFromFacesAndTangentsX;

        Vector4<T> vXu1 =
          edge.vertex(1) - (T)1e-7 * normalFromFacesAndTangentsX;

        Vector4<T> fvXu0 =
          edge.faceVertex(0) - (T)1e-7 * normalFromFacesAndTangentsX;

        Vector4<T> fvXu1 =
          edge.faceVertex(1) - (T)1e-7 * normalFromFacesAndTangentsX;

        Edge<T> Xu2(vXu0, vXu1, fvXu0, fvXu1);

        warning("Not use Xu2 Yu2?");

        this->intersectionc =
          this->Yu.edgesIntersect(edge, this->ctx->safety_distance,
                                  rigidEdge1 || rigidEdge2, true);

        message("refYxx2");
        std::cout << refYx << std::endl;

        /*Compute outward normal without reference, this forces the
          edges to be separated before computing the normal.*/
        message("get outward normal, %d - %d", this->candidateId, this->setId);

        refYx = Yu2.getOutwardNormal(Xu2);
        refXy = -refYx;

        std::cout << refYx << std::endl;

        T dist = Yu2.getSignedDistance(Xu2, 0, 0, 0, 0, 0, &refYx);

        message("dist after recomputing normal = %10.10e", dist);

        if(Abs(dist) < this->ctx->safety_distance){
          message("not improved");
          /*not improved*/
          return false;
        }

        /*Distance must be positive since the edges are displaced in
          the negative normal direction*/

        /*To prevent that we hit zero again*/
        if(dist - (T)0.5*this->ctx->safety_distance < 0.0){
          /*Distance must be positive*/
          refYx = -refYx;
          refXy = -refXy;
          warning("flipped due to some error");
          this->distancec =
            this->Yu.getSignedDistance(edge, 0, 0, 0, 0,
                                       &degenerate, &refYx);

          this->distance0 =
            this->Y0.getSignedDistance(this->X0, 0, 0, 0, 0,
                                       &degenerate, &refYx);
        }
      }else{
        /*Distance must be negative since edges are intersecting*/

        if(this->distancec - this->ctx->safety_distance > 0.0){
          DBG(message("flipping ref"));
          DBG(std::cout << refYx << std::endl);
          refYx = -refYx;
          refXy = -refXy;

          this->distancec =
            this->Yu.getSignedDistance(edge, 0, 0, 0, 0,
                                       &degenerate, &refYx);
          this->distance0 =
            this->Y0.getSignedDistance(this->X0, 0, 0, 0, 0,
                                       &degenerate, &refYx);
          DBG(std::cout << refYx << std::endl);
          DBG(message("new distancec = %10.10e", this->distancec));
        }
      }
    }
#endif

    if(debug){
      warning("distance0     = %10.10e", this->distance0);
      warning("distancec     = %10.10e", this->distancec);
      warning("intersection0 = %d", this->intersection0);
      warning("intersectionc = %d", this->intersectionc);
      warning("backface      = %d", this->backFace);
      warning("enabled       = %d", this->status == Enabled);
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << edge << std::endl;
      warning("ref dv");
      std::cerr << refYx << std::endl;


      getchar();

    }

    if(debug && ( (this->Y0.isConvex()  && this->Yu.isConcave()) ||
                  (this->Y0.isConcave() && this->Yu.isConvex()) ||
                  (this->X0.isConvex()  && edge.isConcave()) ||
                  (this->X0.isConcave() && edge.isConvex())) && this->backFace){
      warning("edge changed from concave to convex or the other way around");
      warning("distance0     = %10.10e", this->distance0);
      warning("distancec     = %10.10e", this->distancec);
      warning("intersection0 = %d", this->intersection0);
      warning("intersectionc = %d", this->intersectionc);
      warning("backface      = %d", this->backFace);
      warning("enabled       = %d", this->status == Enabled);

      T sd1 = this->Yu.getSignedDistance(edge, 0, 0, 0, 0, 0, &refYx);
      warning("sd1 = %10.10e", sd1);

      T sd2 = edge.getSignedDistance(this->Yu, 0, 0, 0, 0, 0, &refXy);
      warning("sd2 = %10.10e", sd2);

      Vector4<T> normalFromFacesAndTangents =
        (this->Yu.faceNormal(0)  + this->Yu.faceNormal(1) -
         this->Yu.faceTangent(0) - this->Yu.faceTangent(1) ).normalized();

      warning("normal from tangent and normal");
      std::cerr << normalFromFacesAndTangents << std::endl;

      std::cerr << this->Yu << std::endl;
      std::cerr << edge << std::endl;
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->X0 << std::endl;
    }

    Vector4<T> bary01, bary02, baryc1, baryc2;
    this->Y0.infiniteProjections(this->X0, 0, 0, &bary01, &bary02, 0);
    this->Yu.infiniteProjections(edge,     0, 0, &baryc1, &baryc2, 0);

    bool projection =
      barycentricEdgeMatch(bary01, baryc1) &&
      barycentricEdgeMatch(bary02, baryc2);

    projection = false;

    if(this->status != Enabled){
      bool test = false;

      if(projection){
        /*Collisionpoint stays 'inside' the edges. A collision can
          only be due to a change from positive to negative.*/
        if(
           (!this->intersection0 && this->intersectionc) &&
           (this->distance0 > this->ctx->safety_distance &&
            this->distancec < this->ctx->safety_distance)
           ){
          test = true;
        }
      }else{
        if(
           ((!this->intersection0 && this->intersectionc &&
             this->distance0 > this->ctx->safety_distance &&
             this->distancec < this->ctx->safety_distance)
            ||
            (this->intersection0 && this->intersectionc &&
             this->distance0 < this->ctx->safety_distance &&
             this->distancec < this->ctx->safety_distance)
            )
           ){
          test = true;
        }
        if(this->intersection0 && this->distancec < this->ctx->safety_distance){
          test = true;
        }

        if(this->intersectionc && this->distance0 < this->ctx->safety_distance){
          test = true;
        }
      }
      if(test){
        if(true){
          /*Get normal after getting reference*/
          Vector4<T> oldref = refYx;

          refYx = this->Yu.getOutwardNormal(edge, &refYx);
          refXy = -refYx;

          if(debug){
            message("old ref");
            std::cout << oldref << std::endl;
            message("new ref");
            std::cout << refYx << std::endl;
          }

          /*Construct normal from face normals and tangent vectors*/
          Vector4<T> normalFromFacesAndTangents;

          if(this->Yu.isConvex()){
            normalFromFacesAndTangents =
              (this->Yu.faceNormal(0)  + this->Yu.faceNormal(1) -
               this->Yu.faceTangent(0) - this->Yu.faceTangent(1) ).normalized();
          }else{
            normalFromFacesAndTangents =
              (this->Yu.faceNormal(0)  + this->Yu.faceNormal(1) +
               this->Yu.faceTangent(0) + this->Yu.faceTangent(1) ).normalized();
          }

          if(dot(refYx, normalFromFacesAndTangents) < 0.0){
            refYx = -refYx;
            refXy = -refYx;
            (warning("corrected reference vector using tangents and normals\n\n"));
            if(debug){
              message("corrected reference vector using tangents, tangent = ");
              std::cout << normalFromFacesAndTangents << std::endl;
            }
          }


          if(dot(this->Yu.faceNormal(0), refYx) < 0.0 &&
             dot(this->Yu.faceNormal(1), refYx) < 0.0 ){
            warning("%d - %d", this->candidateId, this->setId);
            warning("distance0     = %10.10e", this->distance0);
            warning("distancec     = %10.10e", this->distancec);
            warning("intersection0 = %d", this->intersection0);
            warning("intersectionc = %d", this->intersectionc);
            warning("backface      = %d", this->backFace);
            warning("enabled       = %d", this->status == Enabled);

            std::cerr << refYx << std::endl;
            std::cerr << this->Yu << std::endl;
            std::cerr << edge << std::endl;
            std::cerr << this->Y0 << std::endl;
            std::cerr << this->X0 << std::endl;

            error("reference does not match facenormals");
          }

          if(IsNan(refYx)){
            std::cout << refYx << std::endl;
            error("dv = NAN");
          }
        }

        if(debug){
          warning("distance0     = %10.10e", this->distance0);
          warning("distancec     = %10.10e", this->distancec);
          warning("intersection0 = %d", this->intersection0);
          warning("intersectionc = %d", this->intersectionc);
          warning("backface      = %d", this->backFace);
          warning("enabled       = %d", this->status == Enabled);

          T sd1 = this->Yu.getSignedDistance(edge, 0,0,0,0,0,&refYx);
          warning("sd1 = %10.10e", sd1);

          T sd2 = edge.getSignedDistance(this->Yu, 0,0,0,0,0,&refXy);
          warning("sd2 = %10.10e", sd2);

          /*eu should be convex*/
          Vector4<T> normalFromFacesAndTangents =
            (this->Yu.faceNormal(0)  + this->Yu.faceNormal(1) -
             this->Yu.faceTangent(0) - this->Yu.faceTangent(1) ).normalized();

          warning("normal from tangent and normal");
          std::cerr << normalFromFacesAndTangents << std::endl;

          std::cerr << this->Yu << std::endl;
          std::cerr << edge << std::endl;
          std::cerr << this->Y0 << std::endl;
          std::cerr << this->X0 << std::endl;
        }

        /*Check this pair further using a rootfinder*/
        return true;
      }
    }
    if(debug){
      getchar();
    }
    return false;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>
  ::updateInactiveConstraint(ConstraintSetEdge<T, CC>* c,
                             const Edge<T>& Y,
                             const Edge<T>& X,
                             const Vector4<T>& com,
                             const Quaternion<T>& rot){

    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    this->Y0 = Y;
    this->Yi = Y;
    this->Yu = Y;

    this->comY0 = this->comYu = this->comYi = Vector4<T>();
    this->rotY0 = this->rotYu = this->rotYi = Quaternion<T>();

    this->ctx->mesh->setCurrentEdge(this->candidateId);

    int vertex = this->ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(this->ctx->mesh->vertices[vertex].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[vertex];

      this->comY0 = this->comYu = this->comYi =
        this->ctx->mesh->centerOfMass[objectId];
      this->rotY0 = this->rotYu = this->rotYi =
        this->ctx->mesh->orientations[objectId];

      this->comYu = this->ctx->mesh->centerOfMassDispl[objectId];
      this->rotYu = this->ctx->mesh->orientationsDispl[objectId];
    }

    this->X0 = X;
    this->Xi = X;

    this->comX0 = this->comXi = com;
    this->rotX0 = this->rotXi = rot;

    this->intersection0 = this->intersectionc =
      this->Y0.edgesIntersect(this->X0, this->ctx->safety_distance,
                              rigidEdge1 || rigidEdge2);

    if(this->intersection0 == false){
      message("2::edgeIds = %d, %d, backface = %d",
              this->candidateId, this->setId,
              this->backFace);

      refYx = this->Y0.getOutwardNormal(this->X0, 0);
      refXy = -refYx;

      if(IsNan(refYx)){
        error("dv = NAN");
      }
    }
  }

  /**************************************************************************/
  /*Update position of edge*/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::
  updateGeometry(){
    this->ctx->mesh->getDisplacedEdge(&this->Yu, this->candidateId,
                                      this->backFace);

    this->ctx->mesh->setCurrentEdge(this->candidateId);
    int vertex = this->ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(this->ctx->mesh->vertices[vertex].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[vertex];

      this->comYu = this->ctx->mesh->centerOfMassDispl[objectId];
      this->rotYu = this->ctx->mesh->orientationsDispl[objectId];
    }
  }

  /**************************************************************************/
  /*Update the constraint such that is corresponds to the current
    state of the geometry.*/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::
  updateConstraint(const Edge<T>& x,
                   const Edge<T>& p,
                   const Vector4<T>& com,
                   const Quaternion<T>& rot,
                   bool* skipped,
                   EvalStats& stats){

    CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

    bool debug = false;
    Vector4<T> baryi1Old = baryi1;
    Vector4<T> baryi2Old = baryi2;

    if(false){
      debug = true;
    }

#ifdef DEBUG_UPDATE
    debug = true;
#endif

    if(debug){
      warning("updateConstraint");
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << p << std::endl;
      std::cerr << x << std::endl;
      std::cerr << this->Yi << std::endl;
      std::cerr << this->Xi << std::endl;
      std::cerr << this->enabledYu << std::endl;
      std::cerr << this->enabledXu << std::endl;
      std::cerr << this->enabledY0 << std::endl;
      std::cerr << this->enabledX0 << std::endl;

      warning("refYx0");
      std::cerr << refYx0 << std::endl;
      std::cerr << refYx << std::endl;
      std::cerr << dv << std::endl;
      std::cerr << cn << std::endl;
    }

    Vector4<T> cnOld = this->Yi.getOutwardNormal(this->Xi, &dv);

    if(debug){
      message("old contact normal");
      std::cerr << cnOld << std::endl;
    }

    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    T initialWeight = (T)1.2;

    Edge<T> Yii;
    Edge<T> Xii;

    Vector4<T> weights1;
    Vector4<T> weights2;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

    if(debug){
      cc->print(std::cerr);
    }


    /*Interpolate between configuration at impact and the current
      configuration. Accept interpolated version when the projection
      of the new normal on the current normal is > 0.85. If this is
      not the case, choose the weights such that the new configuration
      will be closer to the configuration at impact. Repeat intil a
      good configuration is found.*/

    while(true){
      weight1 = (T)1.0 - (T)1.0/(T)initialWeight;
      weight2 = (T)1.0 - weight1;

      Edge<T> Yii2;
      Edge<T> Xii2;

      if(rigidEdge1){
        Vector4<T> newComXi = weight1 * this->comXi + weight2 * com;
        Quaternion<T> newRotXi =
          slerp(this->rotXi, rot, weight2).normalized();

        Vector4<T> v11 =
          newComXi + newRotXi*(this->X00.vertex(0)     - this->comX00);
        Vector4<T> v12 =
          newComXi + newRotXi*(this->X00.vertex(1)     - this->comX00);
        Vector4<T> v13 =
          newComXi + newRotXi*(this->X00.faceVertex(0) - this->comX00);
        Vector4<T> v14 =
          newComXi + newRotXi*(this->X00.faceVertex(1) - this->comX00);

        Xii2.set(v11, v12, v13, v14);
      }else{
        Xii2 = interpolateEdges(this->Xi,  p, (T)1, (T)0, weight1);
      }

      if(rigidEdge2){
        Vector4<T> newComYi = weight1 * this->comYi + weight2 * this->comYu;
        Quaternion<T> newRotYi =
          slerp(this->rotYi, this->rotYu, weight2).normalized();

        Vector4<T> v11 =
          newComYi + newRotYi*(this->Y00.vertex(0)     - this->comY00);
        Vector4<T> v12 =
          newComYi + newRotYi*(this->Y00.vertex(1)     - this->comY00);
        Vector4<T> v13 =
          newComYi + newRotYi*(this->Y00.faceVertex(0) - this->comY00);
        Vector4<T> v14 =
          newComYi + newRotYi*(this->Y00.faceVertex(1) - this->comY00);

        Yii2.set(v11, v12, v13, v14);
      }else{
        Yii2 = interpolateEdges(this->Yi, this->Yu, (T)1, (T)0, weight1);
      }

      cn = Yii2.getOutwardNormal(Xii2, &dv);

      if(debug){
        message("contactNormal = ");
        std::cout << cn << std::endl;
        message("contactNormalOld = ");
        std::cout << cnOld << std::endl;
        message("dot = %10.10e", dot(cn, cnOld));
      }

      bool parallel = false;

      Yii2.infiniteProjections(Xii2, 0, 0, &weights2, &weights1, &parallel);

      if(debug){
        message("weights");
        std::cout << weights1 << std::endl;
        std::cout << weights2 << std::endl;

        warning("weight = %10.10e, %10.10e", weight1, weight2);
      }

      if(parallel){
        throw new DegenerateCaseException(__LINE__, __FILE__,
                                          "parallel edges after update");
      }

      if(dot(cn, cnOld) > 0.85){
        /*The new configuration is reasonable, accept this one.*/
        Yii = Yii2;
        Xii = Xii2;
        baryi1 = weights1;
        baryi2 = weights2;
        break;
      }

      /*The current configuration is has a large change compared to
        the previous configuration, choose one closer to the previous
        approximation.*/
      initialWeight *= (T)1.25;
    }

    bool convex1 = Yii.isConvex();
    bool convex2 = Xii.isConvex();

    message("convex1 = %d", convex1);
    message("convex2 = %d", convex2);

    Vector4<T> diff1 = (Yii.getCoordinates(weights2) -
                        this->Yi.getCoordinates(baryi2Old));

    Vector4<T> diff2 = (Xii.getCoordinates(weights1) -
                        this->Xi.getCoordinates(baryi1Old));

#if 1
    message("points");
    std::cout << Yii.getCoordinates(weights2) << std::endl;
    std::cout << Yii.getCoordinates(baryi2Old) << std::endl;
    std::cout << Xii.getCoordinates(weights1) << std::endl;
    std::cout << Xii.getCoordinates(baryi1Old) << std::endl;
    message("bary");
    std::cout << weights2 << std::endl;
    std::cout << baryi2Old << std::endl;
    std::cout << weights1 << std::endl;
    std::cout << baryi1Old << std::endl;

    message("dot = %10.10e", dot(cnOld, cn));
    message("weights");
    std::cout << baryi1 << std::endl;
    std::cout << weights1 << std::endl;
    std::cout << baryi1-weights1 << std::endl;
    std::cout << (baryi1-weights1).length() << std::endl;
    std::cout << baryi2 << std::endl;
    std::cout << weights2 << std::endl;
    std::cout << baryi2-weights1 << std::endl;
    std::cout << (baryi2-weights2).length() << std::endl;

    message("diff1 = %10.10e", diff1.length());
    message("diff2 = %10.10e", diff2.length());
#endif

    ////    if((dot(cnOld, cn) < (T)0.9999
    ////   &&
    ////    rootCheck) ||
    ////   true||
       //(baryi1 - weights1).length() > 1e-4 ||
       //(baryi2 - weights2).length() > 1e-4 ){
    ////   (T)diff1.length() > 1e-2 ||
    ////   (T)diff2.length() > 1e-2){
    {
      refYx = cn;
      refXy = -cn;
      dv = cn;

      if(debug){
        warning("ref Xy");
        std::cerr << refXy << std::endl;
        warning("ref Yx");
        std::cerr << refYx << std::endl;

        warning("p0e0");
        std::cerr << this->X0 << std::endl;
        std::cerr << this->Y0 << std::endl;

        warning("piei");
        std::cerr << this->Xi << std::endl;
        std::cerr << this->Yi << std::endl;

        warning("XiiYii");
        std::cerr << Xii << std::endl;
        std::cerr << Yii << std::endl;
      }

      if(debug){
        warning("weights of point");
        std::cerr << weights1 << std::endl;
        std::cerr << weights2 << std::endl;
      }

      /*Update intersecting geometry*/
      this->Yi = Yii;
      this->Xi = Xii;

      if(rigidEdge1){
        this->comXi = this->comXi * weight1 + com * weight2;
        this->rotXi = slerp(this->rotXi, rot,   weight2).normalized();
      }

      if(rigidEdge2){
        this->comYi = this->comYi * weight1 + this->comYu * weight2;
        this->rotYi = slerp(this->rotYi, this->rotYu, weight2).normalized();
      }

      if(ClampsWeights<CC>()){
        baryi1 = this->X0.clampWeights(weights1, WEIGHT_MIN, WEIGHT_MAX);
        baryi2 = this->Y0.clampWeights(weights2, WEIGHT_MIN, WEIGHT_MAX);
      }else{
        baryi1 =  weights1;
        baryi2 =  weights2;
      }

      weights1 = baryi1;
      weights2 = baryi2;

      if((!this->Xi.barycentricOnEdgeDist(weights1,
                                          this->ctx->distance_epsilon) ||
          !this->Yi.barycentricOnEdgeDist(weights2,
                                          this->ctx->distance_epsilon))){
        message("contact off edge");
        std::cout << weights1 << std::endl;
        std::cout << weights2 << std::endl;
      }

      /*Check if new edges project on each other*/
      if((!this->Xi.barycentricOnEdgeDist(weights1,
                                          this->ctx->distance_epsilon +
                                          this->ctx->distance_tol) ||
          !this->Yi.barycentricOnEdgeDist(weights2,
                                          this->ctx->distance_epsilon +
                                          this->ctx->distance_tol)) &&
         this->distancec > 10*this->ctx->safety_distance
         ){
        /*The contact point is outside one of the edges, but has a
          positive signed distance. Update the constraint, but do not
          report it.*/
        *skipped = true;
      }

      if(debug){
        warning("Edges1 = %10.10e",
                (this->Yu.vertex(0) - this->Yu.vertex(1)).length());
        warning("Edges2 = %10.10e", (p.vertex(0) - p.vertex(1)).length());
        warning("Normal error = %10.10e", dot(cn, cnOld));
        getchar();
      }

      if(dot(cn, cnOld) < (T)0.0){
        /*Should be always positive by definition*/
        warning("Normal flip Yii Xii Yi Xi");
        std::cerr << Yii << std::endl;
        std::cerr << Xii << std::endl;
        getchar();
        error("should not be possible");
      }

      Vector4<T> t1, t2;

      int indices[4];
      this->ctx->mesh->getEdgeIndices(this->setId      , indices    );
      this->ctx->mesh->getEdgeIndices(this->candidateId, indices + 2);

      T dt = this->ctx->getDT();

      bool bodyEdge1  = true; //=edge-set edge
      bool bodyEdge2  = true; //=this edge
      bool rigidEdge1 = false;//=edge-set edge
      bool rigidEdge2 = false;//=this edge

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge1 = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge2 = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      if(debug){
        warning("coordinates");
        std::cerr << Xii.getCoordinates(weights1) << std::endl;
        std::cerr << Yii.getCoordinates(weights2) << std::endl;
      }

      if(debug){
        warning("weights of point");
        std::cerr << weights1 << std::endl;
        std::cerr << weights2 << std::endl;


        warning("coordinates");
        std::cerr << Xii.getCoordinates(weights1) << std::endl;
        std::cerr << Yii.getCoordinates(weights2) << std::endl;
      }

      Vector4<T> re1, re2;

      if(bodyEdge1){
        if(rigidEdge1){
          re1  = Xii.getCoordinates(weights1) - this->comXi;
          t1 += -cc->getRow(1, 0);
          t2 += -cc->getRow(2, 0);
        }else{
          t1 += -cc->getRow(1, 0) - cc->getRow(1,1);
          t2 += -cc->getRow(2, 0) - cc->getRow(2,1);
        }
      }

      if(bodyEdge2){
        if(rigidEdge2){
          re2  = Yii.getCoordinates(weights2) - this->comYi;
          t1 += cc->getRow(1, 2);
          t2 += cc->getRow(2, 2);
        }else{
          t1 += cc->getRow(1, 2) + cc->getRow(1,3);
          t2 += cc->getRow(2, 2) + cc->getRow(2,3);
        }
      }

      t1.normalize();
      t2.normalize();

      if(debug){
        warning("t1");
        std::cerr << t1 << std::endl;
        warning("t2");
        std::cerr << t2 << std::endl;
      }

      /*Re-align tangent vectors with contact normal*/
      Vector4<T> tt2 = cross(t1, cn).normalized();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      Vector4<T> tt1 = cross(t2, cn).normalized();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

      if(debug){
        warning("t1");
        std::cerr << t1 << std::endl;
        warning("t2");
        std::cerr << t2 << std::endl;
      }

      /*Compute normal and tangential distances*/
      T totalDistance =
        (this->Yi.getPlaneDistance(this->X00.vertex(0), dv)*baryi1[0] +
         this->Yi.getPlaneDistance(this->X00.vertex(1), dv)*baryi1[1] -
         this->Yi.getPlaneDistance(this->Y00.vertex(0), dv)*baryi2[0] -
         this->Yi.getPlaneDistance(this->Y00.vertex(1), dv)*baryi2[1]) -
        this->ctx->safety_distance - this->ctx->distance_epsilon;

      /*T1*/
      T totalDistanceT1 =
        -(dot(t1, Xii.vertex(0) - this->X00.vertex(0)) * baryi1[0] +
          dot(t1, Xii.vertex(1) - this->X00.vertex(1)) * baryi1[1] -
          dot(t1, Yii.vertex(0) - this->Y00.vertex(0)) * baryi2[0] -
          dot(t1, Yii.vertex(1) - this->Y00.vertex(1)) * baryi2[1]);

      /*T2*/
      T totalDistanceT2 =
        -(dot(t2, Xii.vertex(0) - this->X00.vertex(0)) * baryi1[0] +
          dot(t2, Xii.vertex(1) - this->X00.vertex(1)) * baryi1[1] -
          dot(t2, Yii.vertex(0) - this->Y00.vertex(0)) * baryi2[0] -
          dot(t2, Yii.vertex(1) - this->Y00.vertex(1)) * baryi2[1]);

      totalDistanceT1 = dot(gap, t1);
      totalDistanceT2 = dot(gap, t2);

      if(debug){
        warning("distance0       = %10.10e", this->distance0);
        warning("distancec       = %10.10e", this->distancec);
        warning("totalDistance   = %10.10e", totalDistance);
        warning("totalDistanceT1 = %10.10e", totalDistanceT1);
        warning("totalDistanceT2 = %10.10e", totalDistanceT2);
      }

      if(this->distancec > 10*this->ctx->safety_distance &&
         this->distancec < (this->ctx->distance_tol +
                            this->ctx->distance_epsilon)){
        *skipped = true;
        message("in range");
      }

      /*Update constraint with new normals and tangential vectors and
        updated distances.*/
      if(bodyEdge1){
        if(rigidEdge1){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(re1, cn)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(-cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(-cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-cn*baryi1[0]*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*baryi1[0]*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*baryi1[0]*dt, totalDistanceT2, 2, 0, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(-cn*baryi1[1]*dt, totalDistance,   0, 1, velIndex);
          cc->setRow(-t1*baryi1[1]*dt, totalDistanceT1, 1, 1, velIndex);
          cc->setRow(-t2*baryi1[1]*dt, totalDistanceT2, 2, 1, velIndex);
        }
      }
      if(bodyEdge2){
        if(rigidEdge2){
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(cn*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(t2*dt, totalDistanceT2, 2, 2, velIndex);

          cc->setRow(cross(re2, cn)*dt, totalDistance,   0, 3, velIndex+1);
          cc->setRow(cross(re2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
          cc->setRow(cross(re2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(cn*baryi2[0]*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(t1*baryi2[0]*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(t2*baryi2[0]*dt, totalDistanceT2, 2, 2, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[3]].vel_index/3;

          cc->setRow(cn*baryi2[1]*dt, totalDistance,   0, 3, velIndex);
          cc->setRow(t1*baryi2[1]*dt, totalDistanceT1, 1, 3, velIndex);
          cc->setRow(t2*baryi2[1]*dt, totalDistanceT2, 2, 3, velIndex);
        }
      }
    }

    if(debug){
      getchar();
    }

    /*Update constraint*/
    cc->init(this->ctx->getMatrix());
    cc->update(*this->ctx->getSolver()->getb(),
               *this->ctx->getSolver()->getx(),
               *this->ctx->getSolver()->getb2());

    stats.n_geometry_check++;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::updateInitial(const Edge<T>& x,
                                                     const Edge<T>& p,
                                                     const Vector4<T>& com,
                                                     const Quaternion<T>& rot,
                                                     bool* skipped,
                                                     EvalStats& stats){
    Vector4<T> weights1, weights2;
    bool parallel = false;
    bool active = false;

    if(this->status == Enabled){
      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(cc->status == Active){
        active = true;
      }

      if(cc->status != Active){
        if(this->distancec < this->ctx->safety_distance
           /*|| c->distancec > this->ctx->distance_epsilon +
           //this->ctx->distance_tol*/){
          disableConstraint();
          //constraintsChanged = true;
          //continue;
          *skipped = true;
        }
      }
    }

#if 0
    if(this->distance0 < 0.0){

    }else{
      if(this->distancec < 0.0){
        return;
      }
    }
#endif

    /*As long edges have not intersected before, their signed distance
      may have the wrong sign.*/
    if(this->intersectionc){
      /*Edges are intersecting, do not update*/
      return;
    }

    active = false;

    if(!active){
#if 1
      bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
      bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

      T interp0 = (T)0.125;
      T interp1 = (T)1.0 - interp0;

      if(rigidEdge1){
        this->rotX0 = slerp(this->rotX0, rot, interp1).normalized();
        this->comX0 = this->comX0*interp0 + com*interp1;

        Vector4<T> v1  = this->comX0 +
          this->rotX0*(this->X00.vertex(0) - this->comX00);

        Vector4<T> v2  = this->comX0 +
          this->rotX0*(this->X00.vertex(1) - this->comX00);

        Vector4<T> fv1 = this->comX0 +
          this->rotX0*(this->X00.faceVertex(0) - this->comX00);

        Vector4<T> fv2 = this->comX0 +
          this->rotX0*(this->X00.faceVertex(1) - this->comX00);

        this->X0.set(v1, v2, fv1, fv2);
      }else{
        Vector4<T> v1  = this->X0.vertex(0)*interp0     + p.vertex(0)*interp1;
        Vector4<T> v2  = this->X0.vertex(1)*interp0     + p.vertex(1)*interp1;
        Vector4<T> fv1 = this->X0.faceVertex(0)*interp0 + p.faceVertex(0)*interp1;
        Vector4<T> fv2 = this->X0.faceVertex(1)*interp0 + p.faceVertex(1)*interp1;

        this->X0.set(v1, v2, fv1, fv2);
      }

      if(rigidEdge2){
        this->rotY0 = slerp(this->rotY0, this->rotYu, interp1).normalized();
        this->comY0 = this->comY0*interp0 + this->comYu*interp1;

        Vector4<T> v1  = this->comY0 +
          this->rotY0*(this->Y00.vertex(0) - this->comY00);

        Vector4<T> v2  = this->comY0 +
          this->rotY0*(this->Y00.vertex(1) - this->comY00);

        Vector4<T> fv1 = this->comY0 +
          this->rotY0*(this->Y00.faceVertex(0) - this->comY00);

        Vector4<T> fv2 = this->comY0 +
          this->rotY0*(this->Y00.faceVertex(1) - this->comY00);

        this->Y0.set(v1, v2, fv1, fv2);
      }else{
        Vector4<T> v1  =
          this->Y0.vertex(0)*interp0     + this->Yu.vertex(0)*interp1;

        Vector4<T> v2  =
          this->Y0.vertex(1)*interp0     + this->Yu.vertex(1)*interp1;

        Vector4<T> fv1 =
          this->Y0.faceVertex(0)*interp0 + this->Yu.faceVertex(0)*interp1;

        Vector4<T> fv2 =
          this->Y0.faceVertex(1)*interp0 + this->Yu.faceVertex(1)*interp1;

        this->Y0.set(v1, v2, fv1, fv2);
      }

      //this->distance0 = this->distancec;
      //this->intersection0 = this->intersectionc;

      this->intersection0 =
        this->Y0.edgesIntersect(this->X0, this->ctx->safety_distance,
                                rigidEdge1 || rigidEdge2);

      refYx = this->Y0.getOutwardNormal(this->X0, &refYx);
      refXy = -refYx;

      this->distance0 =
        this->Y0.getSignedDistance(this->X0, 0, 0, 0, 0, 0, &refYx);

      if(this->intersection0){
        if(this->distance0 < 0.0){
          this->distance0 *= (T)-1.0;
          refYx = -refYx;
          refXy = -refXy;
        }
      }
#else
      this->Y0 = this->Yu;
      this->rotY0 = this->rotYu;
      this->comY0 = this->comYu;

      this->X0 = p;
      this->rotX0 = rot;
      this->comX0 = com;

      this->distance0 = this->distancec;
      this->intersection0 = this->intersectionc;
#endif
    }

    this->Yu.infiniteProjections(p, 0, 0, &weights2, &weights1, &parallel);

    if(this->status == Enabled){
      /*Check if new edges project on each other*/
      if((! p.barycentricOnEdgeDist(weights1,
                                    this->ctx->distance_epsilon) ||
          !this->Yu.barycentricOnEdgeDist(weights2,
                                          this->ctx->distance_epsilon))
         //&&
         //this->distancec > 10*this->ctx->safety_distance//ILON+this->ctx->distance_tol
         //this->distancec > this->ctx->distance_tol
         //this->distancec > this->ctx->distance_epsilon - this->ctx->distance_tol
         ){

          message("update initial disable EE %d - %d",
                  this->candidateId, this->setId);
          message("distancec = %10.10e", this->distancec);
          std::cout << weights1 << std::endl;
          std::cout << weights2 << std::endl;

        CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

        if(cc->status == Active){
          *skipped = true;
        }

        disableConstraint();

        /*
        this->Y0 = this->Yu;
        this->rotY0 = this->rotYu;
        this->comY0 = this->comYu;

        this->X0 = p;
        this->rotX0 = rot;
        this->comX0 = com;

        this->distance0 = this->distancec;
        this->intersection0 = this->intersectionc;*/
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateEdge<T, CC>::enableConstraint(const Edge<T>& s,
                                                        const Edge<T>& p,
                                                        Vector4<T>* __gap){
    if(this->status == Enabled){
      /*Constraint was already active, do nothing*/
      return false;
    }else{
      CC* cc = new CC();

      cc->sourceType = 1;
      cc->sourceA = this->candidateId;
      cc->sourceB = this->setId;

      if(this->backFace){
        cc->sourceType = 4;
      }

      this->enabledXu = p;
      this->enabledYu = this->Yu;
      this->enabledX0 = this->X0;
      this->enabledY0 = this->Y0;

      refYx0 = refYx;

      bool fault = false;

      try{
        /*Compute barycentric coordinates on both edges*/
        Vector4<T> weights1 =  baryi1;
        Vector4<T> weights2 =  baryi2;

        cc->setMu((T)MU);

        if(this->backFace){
          cc->setMu((T)MU*(T)0.0);
        }

        int indices[4];
        this->ctx->mesh->getEdgeIndices(this->setId      , indices    );
        this->ctx->mesh->getEdgeIndices(this->candidateId, indices + 2);

        T dt = this->ctx->getDT();

        bool bodyEdge1  = true; //=edge-set edge
        bool bodyEdge2  = true; //=this edge
        bool rigidEdge1 = false;//=edge-set edge
        bool rigidEdge2 = false;//=this edge

        for(int i=0;i<2;i++){
          if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
            bodyEdge1 = false;
          }

          if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
            rigidEdge1 = true;
          }
        }

        for(int i=2;i<4;i++){
          if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
            bodyEdge2 = false;
          }

          if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
            rigidEdge2 = true;
          }
        }

        T totalDistance =
          (this->Yi.getPlaneDistance(this->X00.vertex(0), dv)*weights1[0] +
           this->Yi.getPlaneDistance(this->X00.vertex(1), dv)*weights1[1] -
           this->Yi.getPlaneDistance(this->Y00.vertex(0), dv)*weights2[0] -
           this->Yi.getPlaneDistance(this->Y00.vertex(1), dv)*weights2[1]) -
          this->ctx->safety_distance - this->ctx->distance_epsilon;

        if(Abs(totalDistance) > 1E-2){
          /*When a constraint is enabled and its corresponding
            distance is too large, there can be a problem.*/
          warning("dump edges");
          std::cerr << s << std::endl;
          std::cerr << p << std::endl;
          std::cerr << this->Y0 << std::endl;
          std::cerr << this->Yu << std::endl;
          std::cerr << this->Xi << std::endl;
          std::cerr << this->Yi << std::endl;

          PRINT(s);
          PRINT(p);
          PRINT(this->Y0);
          PRINT(this->Yu);
          std::cerr << weights1 << std::endl;
          std::cerr << weights2 << std::endl;
          std::cerr << dv << std::endl;
          std::cerr << refYx << std::endl;
          std::cerr << this->Yi.getOutwardNormal(this->Xi, &refYx) << std::endl;
          std::cerr << this->Yi.getOutwardNormal(this->Xi, &refYx) << std::endl;

          warning("too large distance");


          warning("total distance = %10.10e", totalDistance);


          fault = true;
        }

        Vector4<T> t1, t2;
        Vector4<T> re1, re2;

        cn = this->Yi.getOutwardNormal(this->Xi, &refYx);

        if(HasFrictionCone<CC>()){
          if(tangentialDefined){
            t1 = -tangentReference[0];

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;
            tt2 = cross(t1,  cn).normalized();
            tt1 = cross(tt2, cn).normalized();

            t1 = tt1;
            t2 = tt2;

            if(bodyEdge1){
              if(rigidEdge1){
                re1  = this->Xi.getCoordinates(baryi1) - this->comXi;
              }
            }

            if(bodyEdge2){
              if(rigidEdge2){
                re2  = this->Yi.getCoordinates(baryi2) - this->comYi;
              }
            }
            cc->tangent1 = t1;
            cc->tangent2 = t2;
            cc->normal   = cn;
          }else{
            /*No previous tangent vector defined*/
            Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;

            tt2 = cross(rnd, cn).normalized();
            tt1 = cross(tt2, cn).normalized();

            t1 = tt1;
            t2 = tt2;

            if(bodyEdge1){
              if(rigidEdge1){
                re1  = this->Xi.getCoordinates(baryi1) - this->comXi;
              }
            }

            if(bodyEdge2){
              if(rigidEdge2){
                re2  = this->Yi.getCoordinates(baryi2) - this->comYi;
              }
            }

            cc->tangent1 = t1;
            cc->tangent2 = t2;
            cc->normal   = cn;
          }
        }else{
          /*No friction cone case*/
          if(tangentialDefined){
            t1 = -tangentReference[0];
            t2 = -tangentReference[1];

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;
            tt2 = cross(t1, cn).normalized();
            if(dot(tt2, t2) < 0){
              tt2 *= -1;
            }

            tt1 = cross(t2, cn).normalized();
            if(dot(tt1, t1) < 0){
              tt1 *= -1;
            }

            t1 = tt1;
            t2 = tt2;

            if(rigidEdge1){
              re1  = this->Xi.getCoordinates(baryi1) - this->comXi;
            }

            if(rigidEdge2){
              re2  = this->Yi.getCoordinates(baryi2) - this->comYi;
            }
          }else{
            Vector4<T> vel;
            Vector<T>* vx = this->ctx->getSolver()->getx();

            /*Compute current relative velocity*/
            if(bodyEdge1){
              if(rigidEdge1){
                int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;

                vel[0] -= (*vx)[vel_index+0];
                vel[1] -= (*vx)[vel_index+1];
                vel[2] -= (*vx)[vel_index+2];

                Vector4<T> ang_vel;

                ang_vel[0] = (*vx)[vel_index+3];
                ang_vel[1] = (*vx)[vel_index+4];
                ang_vel[2] = (*vx)[vel_index+5];

                /*Compute angular displacement*/
                re1 = this->Xi.getCoordinates(baryi1) - this->comXi;
                ang_vel = cross(ang_vel, re1);

                vel -= ang_vel;
              }else{
                for(int l=0;l<2;l++){
                  int vel_index =
                    this->ctx->mesh->vertices[indices[l+0]].vel_index;

                  vel[0] -= (*vx)[vel_index+0] * weights1[l];
                  vel[1] -= (*vx)[vel_index+1] * weights1[l];
                  vel[2] -= (*vx)[vel_index+2] * weights1[l];
                }
              }
            }

            if(bodyEdge2){
              if(rigidEdge2){
                int vel_index = this->ctx->mesh->vertices[indices[2]].vel_index;

                vel[0] += (*vx)[vel_index+0];
                vel[1] += (*vx)[vel_index+1];
                vel[2] += (*vx)[vel_index+2];

                Vector4<T> ang_vel;

                ang_vel[0] = (*vx)[vel_index+3];
                ang_vel[1] = (*vx)[vel_index+4];
                ang_vel[2] = (*vx)[vel_index+5];

                /*Compute angular displacement*/

                re2 = this->Yi.getCoordinates(baryi2) - this->comYi;
                ang_vel = cross(ang_vel, re2);

                vel += ang_vel;
              }else{
                for(int l=0;l<2;l++){
                  int vel_index =
                    this->ctx->mesh->vertices[indices[l+2]].vel_index;

                  vel[0] += (*vx)[vel_index+0] * weights2[l];
                  vel[1] += (*vx)[vel_index+1] * weights2[l];
                  vel[2] += (*vx)[vel_index+2] * weights2[l];
                }
              }
            }

            Vector4<T> vn = cross(vel, cn);

            /*Use velocity to align tangent vectors*/
            if(vn.length() < 1e-6){
              /*Choose some arbitrary direction*/
              Vector4<T> vec(1, 1, 1, 0);
              Vector4<T> vn2 = cross(vec, cn);

              if(vn2.length() < 1e-6){
                vec.set(-1, 1, 1, 0);
                vn2 = cross(vec, cn);
              }

              t2 = vn2.normalized();
              t1 = cross(t2, cn).normalized();
            }else{
              t2 = vn.normalized();
              t1 = cross(t2, cn).normalized();
            }
          }
        }

        T totalDistanceT1 =
          -(dot(t1, this->Xi.vertex(0) - this->X00.vertex(0)) * weights1[0] +
            dot(t1, this->Xi.vertex(1) - this->X00.vertex(1)) * weights1[1] -
            dot(t1, this->Yi.vertex(0) - this->Y00.vertex(0)) * weights2[0] -
            dot(t1, this->Yi.vertex(1) - this->Y00.vertex(1)) * weights2[1]);

        T totalDistanceT2 =
          -(dot(t2, this->Xi.vertex(0) - this->X00.vertex(0)) * weights1[0] +
            dot(t2, this->Xi.vertex(1) - this->X00.vertex(1)) * weights1[1] -
            dot(t2, this->Yi.vertex(0) - this->Y00.vertex(0)) * weights2[0] -
            dot(t2, this->Yi.vertex(1) - this->Y00.vertex(1)) * weights2[1]);

        gap = cn * totalDistance + t1 * totalDistanceT1 + t2 * totalDistanceT2;

        if(fault){
          warning("tangent vectors");
          std::cerr << t1 << std::endl;
          std::cerr << t2 << std::endl;
          std::cerr << re1 << std::endl;
          std::cerr << re2 << std::endl;

          warning("td1 = %10.10e", totalDistanceT1);
          warning("td2 = %10.10e", totalDistanceT2);
        }

        if(bodyEdge1){
          if(rigidEdge1){
            int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

            cc->setRow(-cross(re1, cn)*dt, totalDistance,   0, 1, velIndex+1);
            cc->setRow(-cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
            cc->setRow(-cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
          }else{
            int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(-cn*weights1[0]*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(-t1*weights1[0]*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(-t2*weights1[0]*dt, totalDistanceT2, 2, 0, velIndex);

            velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

            cc->setRow(-cn*weights1[1]*dt, totalDistance,   0, 1, velIndex);
            cc->setRow(-t1*weights1[1]*dt, totalDistanceT1, 1, 1, velIndex);
            cc->setRow(-t2*weights1[1]*dt, totalDistanceT2, 2, 1, velIndex);
          }
        }
        if(bodyEdge2){
          if(rigidEdge2){
            int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(cn*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(t2*dt, totalDistanceT2, 2, 2, velIndex);

            cc->setRow(cross(re2, cn)*dt, totalDistance,   0, 3, velIndex+1);
            cc->setRow(cross(re2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
            cc->setRow(cross(re2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
          }else{
            int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(cn*weights2[0]*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(t1*weights2[0]*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(t2*weights2[0]*dt, totalDistanceT2, 2, 2, velIndex);

            velIndex = this->ctx->mesh->vertices[indices[3]].vel_index/3;

            cc->setRow(cn*weights2[1]*dt, totalDistance,   0, 3, velIndex);
            cc->setRow(t1*weights2[1]*dt, totalDistanceT1, 1, 3, velIndex);
            cc->setRow(t2*weights2[1]*dt, totalDistanceT2, 2, 3, velIndex);
          }
        }
      }catch(DegenerateCaseException* e){
        std::cout << e->getError();
        delete cc;
        return false;
      }

      /*Set transposed values*/
      cc->init(this->ctx->getMatrix());

      cc->status = Active;

      this->enabledId = this->ctx->getSolver()->addConstraint(cc);

      cc->id = this->enabledId;

      if(this->enabledId < 0){
        error("Negative ID");
      }

      if(fault){
        cc->print(std::cerr);
      }

      if(ActiveConstraints<CC>()){
        cc->resetMultipliers(*this->ctx->getSolver()->getx(),
                             this->cachedMultipliers);
      }else{
        Vector4<T> zero;
        cc->resetMultipliers(*this->ctx->getSolver()->getx(), zero);
      }
      this->status = Enabled;
    }
    return true;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::
  recompute(AbstractConstraintSet<T, Edge<T>, Edge<T>, CC>* c,
            Edge<T>& p,
            Vector4<T>& com,
            Quaternion<T>& rot){
    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    if(collapsed){
      error("edge in collapsed state %d - %d", this->candidateId, this->setId);
    }

    this->ctx->mesh->getEdge(&this->Y0, this->candidateId, this->backFace);

    this->Yi  = this->Y0;
    this->Yu  = this->Y0;
    this->Y00 = this->Y0;

    this->comY00 = this->comY0 = this->comYu = this->comYi = Vector4<T>();
    this->rotY00 = this->rotY0 = this->rotYu = this->rotYi = Quaternion<T>();

    this->ctx->mesh->setCurrentEdge(this->candidateId);

    int vertex = this->ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(this->ctx->mesh->vertices[vertex].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[vertex];

      this->comY00 = this->comY0 = this->comYu = this->comYi =
        this->ctx->mesh->centerOfMass[objectId];

      this->rotY00 = this->rotY0 = this->rotYu = this->rotYi =
        this->ctx->mesh->orientations[objectId];
    }

    this->X0  = p;
    this->Xi  = p;
    this->X00 = p;

    this->comX00 = this->comX0 = this->comXi = com;
    this->rotX00 = this->rotX0 = this->rotXi = rot;

    this->intersection0 = this->intersectionc =
      this->Y0.edgesIntersect(this->X0,
                              this->ctx->safety_distance,
                              rigidEdge1 || rigidEdge2);

    if(this->intersection0 == false && this->status != Enabled){
      /*If there is no intersection and there is no enabled
        constraint, then the actual reference is not of interest. When
        there is no intersection0, a new intersection will reset the
        reference correctly. If there is a constraint enabled, then
        there should be no intersection, so in that case we need to
        compute the new reference using the old one.*/
      bool potentialError = false;
      refYx = this->Y0.getOutwardNormal(this->X0, 0, 0, &potentialError);
      refXy = -refYx;
      refYx0 = refYx;

      if(potentialError){
        message("get outward normal took too long, %d - %d",
                this->candidateId, this->setId);
      }
      if(IsNan(refYx)){
        error("dv = NAN");
      }
    }

    cn = refYx;

    /*Try to invalidate combination p-e as soon as possible*/
    if(this->status != Enabled){
      Vector4<T> ldv;

      if(this->Y0.isIndeterminate() || this->X0.isIndeterminate()){
        //return;
      }
    }else{
      refYx = this->Y0.getOutwardNormal(this->X0, &refYx);
      refXy = -refYx;

      refYx0 = refYx;

      if(IsNan(refYx)){
        error("dv = NAN");
      }
    }

    Vector4<T> dv1 = refYx;

    if(this->Y0.isIndeterminate() || this->X0.isIndeterminate()){
      if(this->status == Enabled){
        //disableConstraint();
      }
      //return;
    }

    tslassert(Abs(dv1.length() -1.0) < 1E-5);

    if(this->status == Enabled){
      /*Distance here IS positive by definition*/
      bool degenerate = false;

      refYx = this->Y0.getOutwardNormal(this->X0, &refYx);
      refXy = -refYx;
      dv1 = refYx;

      if(IsNan(refYx)){
        error("dv = NAN");
      }


      this->distance0 = this->distancec =
        this->Y0.getSignedDistance(this->X0, &colPoint, 0,
                                   &baryi2, &baryi1, &degenerate,
                                   &refYx);

      if(degenerate){
        disableConstraint();
        return;
      }

      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);

      if(cc->status == Inactive){
        if(this->distancec > (this->ctx->distance_epsilon +
                              this->ctx->distance_tol)){
          /*Constraint is inactive and there is no collision -> disable.*/

          disableConstraint();
          return;
        }
      }

      if(!this->X0.barycentricOnEdgeDist(baryi1, this->ctx->safety_distance) ||
         !this->Y0.barycentricOnEdgeDist(baryi2, this->ctx->safety_distance)){
        DBG(message("disable non-projecting edge constraint"));
        DBG(cc->print(std::cout));
        disableConstraint();
        warning("check barycentric coordinates differently");
        return;
      }

      Vector4<T> weights1 =  baryi1;
      Vector4<T> weights2 =  baryi2;

      dv = dv1;

      cc->setMu((T)MU);

      if(this->backFace){
        cc->setMu((T)MU*(T)0.0);
      }

      if(HasFrictionCone<CC>()){
        cc->cumulativeVector = cc->kineticVector * (T)60;

        if(cc->cumulativeVector.length() > 1e-6){
          cc->cumulativeVector.normalize();
        }

        if(cc->frictionStatus[0] != Kinetic){
          cc->cumulativeVector.set(0,0,0,0);
          cc->kineticVector.set(0,0,0,0);
          cc->acceptedKineticVector.set(0,0,0,0);
        }
      }

      int indices[4];

      this->ctx->mesh->getEdgeIndices(    c->getSetId(), indices  );
      this->ctx->mesh->getEdgeIndices(this->candidateId, indices+2);

      T dt = this->ctx->getDT();

      bool bodyEdge1 = true;
      bool bodyEdge2 = true;
      bool rigidEdge1 = false;
      bool rigidEdge2 = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge1 = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge2 = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      T totalDistance =
        (this->Yi.getPlaneDistance(this->X00.vertex(0), dv1) * weights1[0] +
         this->Yi.getPlaneDistance(this->X00.vertex(1), dv1) * weights1[1] -
         this->Yi.getPlaneDistance(this->Y00.vertex(0), dv1) * weights2[0] -
         this->Yi.getPlaneDistance(this->Y00.vertex(1), dv1) * weights2[1]) +

        -this->ctx->safety_distance - this->ctx->distance_epsilon;

      if(Abs(totalDistance) > 1E-2){
        warning("dump edges");
        std::cerr << this->X0 << std::endl;
        std::cerr << this->Y0 << std::endl;

        bool degenerate = false;
        warning("d0 = %10.10e",
                this->X0.getSignedDistance(this->Y0, 0, 0, 0, 0,
                                           &degenerate, &refYx));
        warning("degenerate = %d", degenerate);

        //PRINT(s);
        PRINT(p);
        PRINT(this->Y0);
        PRINT(this->Yu);
        warning("too large distance");
      }

      /*Compute tangent vectors*/
      Vector4<T> t1, t2;
      Vector4<T> re1, re2;

      if(HasFrictionCone<CC>()){
        if(bodyEdge1){
          if(rigidEdge1){
            t1 += cc->getRow(1,0);
            t2 += cc->getRow(2,0);
            re1  = this->Xi.getCoordinates(baryi1) - this->comXi;
          }else{
            t1 += cc->getRow(1,0);
            t1 += cc->getRow(1,1);
            t2 += cc->getRow(2,0);
            t2 += cc->getRow(2,1);
          }
        }

        if(bodyEdge2){
          if(rigidEdge2){
            t1 -= cc->getRow(1,2);
            t2 -= cc->getRow(2,2);
            re2  = this->Yi.getCoordinates(baryi2) - this->comYi;
          }else{
            t1 -= cc->getRow(1,2);
            t1 -= cc->getRow(1,3);
            t2 -= cc->getRow(2,2);
            t2 -= cc->getRow(2,3);
          }
        }

        t1 *= -1;
        t2 *= -1;

        Vector4<T> tt1, tt2;
        tt2 = cross(t1, dv).normalized();
        tt1 = cross(tt2, dv).normalized();

        t1 = -tt1;
        t2 = -tt2;

        cc->tangent1 = t1;
        cc->tangent2 = t2;
        cc->normal   = dv;
      }else{
        /*Compute current relative velocity*/
        Vector4<T> vel;
        Vector<T>* vx = this->ctx->getSolver()->getx();

        if(bodyEdge1){
          if(rigidEdge1){
            int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;

            vel[0] -= (*vx)[vel_index+0];
            vel[1] -= (*vx)[vel_index+1];
            vel[2] -= (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            re1  = this->Xi.getCoordinates(baryi1) - this->comXi;
            ang_vel = cross(ang_vel, re1);

            vel -= ang_vel;
          }else{
            for(int l=0;l<2;l++){
              int vel_index = this->ctx->mesh->vertices[indices[l+0]].vel_index;

              vel[0] -= (*vx)[vel_index+0] * weights1[l];
              vel[1] -= (*vx)[vel_index+1] * weights1[l];
              vel[2] -= (*vx)[vel_index+2] * weights1[l];
            }
          }
        }

        if(bodyEdge2){
          if(rigidEdge2){
            int vel_index = this->ctx->mesh->vertices[indices[2]].vel_index;

            vel[0] += (*vx)[vel_index+0];
            vel[1] += (*vx)[vel_index+1];
            vel[2] += (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            re2  = this->Yi.getCoordinates(baryi2) - this->comYi;
            ang_vel = cross(ang_vel, re2);

            vel += ang_vel;
          }else{
            for(int l=0;l<2;l++){
              int vel_index = this->ctx->mesh->vertices[indices[l+2]].vel_index;
              vel[0] += (*vx)[vel_index+0] * weights2[l];
              vel[1] += (*vx)[vel_index+1] * weights2[l];
              vel[2] += (*vx)[vel_index+2] * weights2[l];
            }
          }
        }

        ////////////////
        t1 = t2.set(0,0,0,0);

        if(rigidEdge1){
          t1 += cc->getRow(1,0);
          t2 += cc->getRow(2,0);
        }else{
          t1 += cc->getRow(1,0);
          t1 += cc->getRow(1,1);
          t2 += cc->getRow(2,0);
          t2 += cc->getRow(2,1);
        }

        if(rigidEdge2){
          t1 -= cc->getRow(1,2);
          t2 -= cc->getRow(2,2);
        }else{
          t1 -= cc->getRow(1,2);
          t1 -= cc->getRow(1,3);
          t2 -= cc->getRow(2,2);
          t2 -= cc->getRow(2,3);
        }

        t1 *= -1;
        t2 *= -1;

        t1.normalize();
        t2.normalize();

        Vector4<T> vn = cross(vel, dv);

        /*Re-align with velocity and/or normal*/
        if(vn.length() < 1e-6){
          /*Fixed point, only align with normal*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, dv).normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(t2, dv).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }

          t1 = tt1;
          t2 = tt2;
        }else{
          Vector4<T> tt1, tt2;
          tt2 = vn.normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(tt2, dv).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }
          t1 = tt1;
          t2 = tt2;
        }
      }

      gap = dv * totalDistance;

      /*Setup existing constraint*/
      if(bodyEdge1){
        if(rigidEdge1){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-dv*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*dt,             0, 2, 0, velIndex);

          cc->setRow(-cross(re1, dv)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(-cross(re1, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(-cross(re1, t2)*dt,             0, 2, 1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(-dv*weights1[0]*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*weights1[0]*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*weights1[0]*dt,             0, 2, 0, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(-dv*weights1[1]*dt, totalDistance, 0, 1, velIndex);
          cc->setRow(-t1*weights1[1]*dt,             0, 1, 1, velIndex);
          cc->setRow(-t2*weights1[1]*dt,             0, 2, 1, velIndex);
        }
      }
      if(bodyEdge2){
        if(rigidEdge2){
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(dv*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(t1*dt,             0, 1, 2, velIndex);
          cc->setRow(t2*dt,             0, 2, 2, velIndex);

          cc->setRow(cross(re2, dv)*dt, totalDistance, 0, 3, velIndex+1);
          cc->setRow(cross(re2, t1)*dt,             0, 1, 3, velIndex+1);
          cc->setRow(cross(re2, t2)*dt,             0, 2, 3, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(dv*weights2[0]*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(t1*weights2[0]*dt,             0, 1, 2, velIndex);
          cc->setRow(t2*weights2[0]*dt,             0, 2, 2, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[3]].vel_index/3;

          cc->setRow(dv*weights2[1]*dt, totalDistance, 0, 3, velIndex);
          cc->setRow(t1*weights2[1]*dt,             0, 1, 3, velIndex);
          cc->setRow(t2*weights2[1]*dt,             0, 2, 3, velIndex);
        }
      }

      /*Set transposed values*/
      cc->init(this->ctx->getMatrix());

      cc->update(*this->ctx->getSolver()->getb(),
                 *this->ctx->getSolver()->getx(),
                 *this->ctx->getSolver()->getb2());


    }else{
      bool degenerate = false;
      this->distance0 = this->distancec =
        this->Y0.getSignedDistance(this->X0, &colPoint, 0,
                                   &baryi2, &baryi1,
                                   &degenerate, &refYx);
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::
  updateInitialState(const Edge<T>& edge,
                     const Vector4<T>& com,
                     const Quaternion<T>& rot,
                     bool thisSet){
    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    if(thisSet){
      this->X0    = edge;
      this->comX0 = com;
      this->rotX0 = rot;
    }else{
      this->Y0    = edge;
      this->comY0 = com;
      this->rotY0 = rot;
    }

    message("intersection was %d", this->intersection0);

    this->intersection0 =
      this->Y0.edgesIntersect(this->X0, this->ctx->safety_distance,
                              rigidEdge1 || rigidEdge2);

    message("intersection becomes %d", this->intersection0);

    cn = refYx;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateEdge<T, CC>::
  showCandidateState(const Edge<T>& pu){
    bool rigidEdge1 = Mesh::isRigidOrStatic(this->setType);
    bool rigidEdge2 = Mesh::isRigidOrStatic(this->candidateType);

    message("\n\nShow candidate state %d - %d", this->candidateId, this->setId);

    message("p0 e0");
    std::cout << this->X0 << std::endl;
    std::cout << this->Y0 << std::endl;

    message("pu eu");
    std::cout << pu << std::endl;
    std::cout << this->Yu << std::endl;

    message("pi ei");
    std::cout << this->Xi << std::endl;
    std::cout << this->Yi << std::endl;

    message("distance0 = %10.10e", this->distance0);
    message("distancec = %10.10e", this->distancec);

    this->intersection0 =
      this->Y0.edgesIntersect(this->X0, this->ctx->safety_distance,
                              rigidEdge1 || rigidEdge2, true);

    message("intersection0 = %d", this->intersection0);

    this->intersectionc =
      this->Yu.edgesIntersect(pu, this->ctx->safety_distance,
                              rigidEdge1 || rigidEdge2, true);

    message("intersectionc = %d", this->intersectionc);

    message("collapsed = %d", collapsed);

    message("refYx");
    std::cout << refYx << std::endl;
    message("refXy");
    std::cout << refXy << std::endl;

    message("recompute distances");

    message("d0 = %10.10e",
            this->Y0.getSignedDistance(this->X0, 0, 0, 0, 0, 0, &refYx));
    message("dc = %10.10e",
            this->Yu.getSignedDistance(pu, 0, 0, 0, 0, 0, &refYx));
    message("di = %10.10e",
            this->Yi.getSignedDistance(this->Xi, 0, 0, 0, 0, 0, &refYx));

    message("barycentric weights");
    std::cout << baryi1 << std::endl;
    std::cout << baryi2 << std::endl;

    Vector4<T> weights1, weights2;

    this->Y0.infiniteProjections(this->X0, 0, 0, &weights2, &weights1);
    std::cout << weights1 << std::endl;
    std::cout << weights2 << std::endl;

    this->Yu.infiniteProjections(pu, 0, 0, &weights2, &weights1);
    std::cout << weights1 << std::endl;
    std::cout << weights2 << std::endl;

    message("enabledId = %d", this->enabledId);

    if(this->status == Enabled){
      message("Constraint enabled");
      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);
      cc->print(std::cout);
    }

    int indices[4];
    this->ctx->mesh->getEdgeIndices(      this->setId, indices    );
    this->ctx->mesh->getEdgeIndices(this->candidateId, indices + 2);

    message("indices p: %d, %d", indices[0], indices[1]);
    message("indices e: %d, %d", indices[2], indices[3]);
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateEdge<T, CC>::
  disableConstraint(){
    if(this->status == Enabled){
      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);

      if(ActiveConstraints<CC>()){
        cc->cacheMultipliers(this->cachedMultipliers,
                             *this->ctx->getSolver()->getx());
      }

      this->status = Disabled;

      tslassert(this->enabledId == cc->id);

      /*Reset tangent reference*/
      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      /*Figure out constraint setup*/
      int indices[4];
      this->ctx->mesh->getEdgeIndices(this->setId      , indices    );
      this->ctx->mesh->getEdgeIndices(this->candidateId, indices + 2);

      bool rigidEdge1 = false;//=edge set edge
      bool rigidEdge2 = false;//=this edge

      for(int i=0;i<2;i++){
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge1 = true;
        }
      }

      for(int i=2;i<4;i++){
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge2 = true;
        }
      }

      /*Save tangentvectors*/
      if(rigidEdge1){
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[1] += cc->getRow(2,0);
      }else{
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[0] += cc->getRow(1,1);
        tangentReference[1] += cc->getRow(2,0);
        tangentReference[1] += cc->getRow(2,1);
      }

      if(rigidEdge2){
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[1] -= cc->getRow(2,2);
      }else{
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[0] -= cc->getRow(1,3);
        tangentReference[1] -= cc->getRow(2,2);
        tangentReference[1] -= cc->getRow(2,3);
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      this->ctx->getSolver()->removeConstraint(this->enabledId);
      this->status = Disabled;

      delete cc;

      return true;
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }

  /**************************************************************************/
  template class ConstraintCandidateEdge<float, ContactConstraintCR<2, float> >;
  template class ConstraintCandidateEdge<double, ContactConstraintCR<2, double> >;
}
