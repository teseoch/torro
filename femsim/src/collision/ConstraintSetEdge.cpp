/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintSetEdge.hpp"
#include "collision/ConstraintSet.hpp"
#include "collision/ConstraintCandidateEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace tsl{

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetEdge<T, CC>::ConstraintSetEdge():
    AbstractConstraintSet<T, Edge<T>, Edge<T>, CC>(){
    this->typeName = "edge - edge";
  }

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetEdge<T, CC>::~ConstraintSetEdge(){
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintSetEdge<T, CC>::evaluate(Edge<T>& p,
                                          Vector4<T>& com,
                                          Quaternion<T>& rot,
                                          const EvalType& etype,
                                          EvalStats& stats){
    CandidateMapIterator it = this->candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged  = false;

    if(evaluateGeometry){
      /*Simple vertex-plane check for a fast collision detection*/

      /*Check if edge is collapsed*/
      bool collapsed = edgeCollapsed(this->startPosition, p);

      if(collapsed){
        DBG(warning("COLLAPSED EDGE %d, backface = %d",
                    this->setId, this->backFace));
      }

      if(this->backFace){
        this->ctx->collapsedBackEdges[this->setId] = collapsed;
      }else{
        this->ctx->collapsedEdges[this->setId] = collapsed;
      }

      if(collapsed){
        if(this->startPosition.isConvex() && p.isConcave()){
          if(!this->backFace){
            this->ctx->backSets[this->startPosition.faceVertexId(0)].
              forceConstraint(this->startPosition.faceId(1));

            this->ctx->backSets[this->startPosition.faceVertexId(1)].
              forceConstraint(this->startPosition.faceId(0));
          }else{
            this->ctx->sets[this->startPosition.faceVertexId(0)].
              forceConstraint(this->startPosition.faceId(1));

            this->ctx->sets[this->startPosition.faceVertexId(1)].
              forceConstraint(this->startPosition.faceId(0));
          }
        }
      }

      while(it != this->candidates.end()){
        Candidate* c = (*it++).getData();

        /*Update geometry information of associated edges*/
        try{
          c->updateGeometry();
        }catch(Exception* e){
          warning("Degenerate edge found 1 %s", e->getError().c_str());
          delete e;

          if(c->getStatus() == Enabled){
            c->disableConstraint();
          }

          continue;
        }

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/
        if(c->evaluate(p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform rootfinding*/
      this->basicRootFinding(p, com, rot,
                             changedCandidates, changedIndices,
                             constraintsChanged, ActiveConstraints<CC>());
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    if(evaluateConstraints){
      this->basicEvaluation(changedIndices, etype, stats,
                            constraintsChanged, false, ActiveConstraints<CC>());
    }

    if(evaluateGeometry){
      this->lastPosition = p;
      this->lastCenterOfMass = com;
      this->lastOrientation = rot;
    }

    return constraintsChanged;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintSetEdge<T, CC>::checkAndUpdate(Edge<T>& p,
                                                Vector4<T>& com,
                                                Quaternion<T>& rot,
                                                EvalStats& stats,
                                                bool friction,
                                                bool force, T bnorm,
                                                bool* allPositive){
    CandidateMapIterator it = this->candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    while(it != this->candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->getStatus() != Enabled){
        continue;
      }

      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(c->getEnabledId());

      if(!ActiveConstraints<CC>()){
        if(cc->status != Active){
          //continue;
        }
      }

      /*Check corresponding lambda*/
      if(!cc->valid(*this->ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      /*Update geometry information of associated edges*/
      try{
        c->updateGeometry();
      }catch(Exception* e){
        cc->status = Inactive;
        warning("Degenerate edge found 2 %s", e->getError().c_str());
        delete e;

        c->disableConstraint();

        continue;
      }

      /*Update value for distancec*/
      Vector4<T> b1, b2;
      bool distanceUpdated = false;

      c->evaluate(p, com, rot, &b1, &b2, &distanceUpdated);

      /*If distanceUpdated == false, the edge-edge pair is invalid*/

      if(distanceUpdated == false){
        constraintsChanged = true;
        message("distance not updated due to exception");
        getchar();
        //c->disableConstraint();
        continue;
      }

      bool offEdge = false;
      {
        Vector4<T> weights1, weights2;
        c->getCurrent().infiniteProjections(p, 0, 0, &weights2, &weights1, 0);
        if(!c->getCurrent().barycentricOnEdgeDist(weights1,
                                                  this->ctx->distance_epsilon*0) ||
           !p.barycentricOnEdgeDist(weights2, this->ctx->distance_epsilon*0)){
          message("off edges, weights");
          std::cout << weights1 << std::endl;
          std::cout << weights2 << std::endl;
          offEdge = true;
        }
      }

      /*Check geometric distance and state of the constraint*/
      if(!this->basicUpdateCheck(c, p, com, rot,
                                 offEdge, ActiveConstraints<CC>(),
                                 constraintsChanged, allPositive,
                                 stats,
                                 this->ctx->distance_epsilon,
                                 this->ctx->distance_tol,
                                 this->ctx->safety_distance,
                                 (T)5.0,
                                 (T)0.1,
                                 (T)5.0,
                                 (T)0.0)){
        continue;
      }
    }

    it = this->candidates.begin();

    while(it != this->candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->getStatus() == Enabled){
        continue;
      }

      //ContactConstraint<2, T>* cc =
      //(ContactConstraint<2, T>*)this->ctx->getMatrix()->getConstraint(c->enabledId);

      //c->updateInactiveConstraint(this, this->startPosition, p, com, rot);
    }

    return constraintsChanged;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSetEdge<T, CC>::incrementalUpdate(bool onlyForced){
    Edge<T> x;
    this->ctx->mesh->getEdge(&x, this->setId, this->backFace);

    Vector4<T> com;
    Quaternion<T> rot;

    if(Mesh::isRigid(this->type)){
      this->ctx->mesh->setCurrentEdge(this->setId);

      int objectId =
        this->ctx->mesh->vertexObjectMap[this->ctx->mesh->getOriginVertex()];

      com = this->ctx->mesh->centerOfMass[objectId];
      rot = this->ctx->mesh->orientations[objectId];
    }

    List<int> currentPotentials;  /*Stores face ids of found potentials*/
    if(!onlyForced){
      this->ctx->extractPotentialEdgeEdges(this->setId, currentPotentials,
                                           this->backFace);
    }

    /*Set visited flag*/
    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentEdge = *lit++;

      this->ctx->mesh->halfEdges[currentEdge].visited = true;
    }

    /*Add forced candidates*/
    Tree<int>::Iterator forcedIt = this->forcedPotentials.begin();
    while(forcedIt != this->forcedPotentials.end()){
      int forcedPotential = *forcedIt++;

      if(this->ctx->mesh->halfEdges[forcedPotential].visited == false){
        //currentPotentials.uniqueInsert(forcedPotential, forcedPotential);

        currentPotentials.append(forcedPotential);
        this->ctx->mesh->halfEdges[forcedPotential].visited = true;
      }
    }

    /*Reset visited flag*/
    lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentEdge = *lit++;
      this->ctx->mesh->halfEdges[currentEdge].visited = false;
    }

    this->forcedPotentials.clear();

    tslassert(this->candidatesTree.size() == candidates.size());

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      this->ctx->mesh->setCurrentEdge(candidate);
      int twinCandidate = this->ctx->mesh->getTwinEdge();

      if(twinCandidate < candidate){
        continue;
      }

      int index = this->candidatesTree.findIndex(candidate);

      if(index == -1){
        if(this->backFace){
          if(Mesh::isRigidOrStatic(this->ctx->mesh->halfEdges[candidate].type)){
            /*A rigid object can have internal collisions, hence Rigid*/
            continue;
          }

          int faceId = this->ctx->mesh->halfEdges[this->setId].half_face;
          int candidateFaceId = this->ctx->mesh->halfEdges[candidate].half_face;

          if(!this->ctx->stree->facesShareSameRoot(faceId, candidateFaceId)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new CandidateType(candidate,
                                          this->setId,
                                          this->backFace,
                                          this->ctx);

        this->ctx->mesh->setCurrentEdge(candidate);
        int edgeVertexId = this->ctx->mesh->getOriginVertex();

        cc->setSetType(this->type);
        cc->setCandidateType(this->ctx->mesh->vertices[edgeVertexId].type);

        this->candidates.insert(candidate, cc);
        this->candidatesTree.insert(candidate, 1);

        //this->ctx->mesh->getEdgeIndices(candidate, cc->indices);

        try{
          cc->recompute(this, x, com, rot);
        }catch(DegenerateCaseException* e){
          std::cerr << e->getError();
          delete e;
        }
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSetEdge<T, CC>::update(){
    Edge<T> x;
    this->ctx->mesh->getEdge(&x, this->setId, this->backFace);

    Vector4<T> com;
    Quaternion<T> rot;

    if(Mesh::isRigid(this->type)){
      this->ctx->mesh->setCurrentEdge(this->setId);

      int objectId =
        this->ctx->mesh->vertexObjectMap[this->ctx->mesh->getOriginVertex()];

      com = this->ctx->mesh->centerOfMass[objectId];
      rot = this->ctx->mesh->orientations[objectId];
    }

    /*Clear forced potentials*/
    this->forcedPotentials.clear();

    List<int> currentPotentials;
    this->ctx->extractPotentialEdgeEdges(this->setId, currentPotentials,
                                         this->backFace);

    tslassert(this->candidatesTree.size() == candidates.size());

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;

      this->ctx->mesh->setCurrentEdge(candidate);
      int twinCandidate = this->ctx->mesh->getTwinEdge();

      if(twinCandidate < candidate){
        continue;
      }

      int index = this->candidatesTree.findIndex(candidate);

      if(index == -1){
        if(this->backFace){
          if(Mesh::isRigidOrStatic(this->ctx->mesh->halfEdges[candidate].type)){
            /*A rigid object can not have internal collisions, hence
              Rigid*/
            continue;
          }

          int faceId = this->ctx->mesh->halfEdges[this->setId].half_face;
          int candidateFaceId = this->ctx->mesh->halfEdges[candidate].half_face;

          if(!this->ctx->stree->facesShareSameRoot(faceId, candidateFaceId)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        Candidate* cc = new CandidateType(candidate,
                                          this->setId,
                                          this->backFace,
                                          this->ctx);

        this->ctx->mesh->setCurrentEdge(candidate);

        int edgeVertexId = this->ctx->mesh->getOriginVertex();

        cc->setSetType(this->type);
        cc->setCandidateType(this->ctx->mesh->vertices[edgeVertexId].type);

        this->candidates.insert(candidate, cc);
        this->candidatesTree.insert(candidate, 1);

        //this->ctx->mesh->getEdgeIndices(candidate, cc->indices);
      }
    }

    /*-----------------------------------------------------*/

    tslassert(this->candidatesTree.size() == candidates.size());

    /*Check for disapeared potential collisions -> remove them from
      the set.*/

    CandidateMapIterator cit2 = this->candidates.begin();

    while(cit2 != this->candidates.end()){
      Candidate* cc = (*cit2++).getData();
      cc->setFlag(false);
    }

    it = currentPotentials.begin();
    while(it != currentPotentials.end()){
      CandidateMapIterator mit = this->candidates.find(*it++);

      if(mit != this->candidates.end()){
        (*mit).getData()->setFlag(true);
      }
    }

    cit2 = this->candidates.begin();
    while(cit2 != this->candidates.end()){
      Candidate* cc = (*cit2).getData();

      if(cc->isFlagged() == false){
        /*Edge disappeared, disable and remove*/

        /*This can only happen if the constraint is not active*/
        if(cc->getStatus() == Enabled){
          cit2++;
        }else{
          cc->disableConstraint();

          this->candidates.remove(cit2);
          int cid = cc->getCandidateId();
          this->candidatesTree.remove(cid);

          delete cc;
        }
      }else{
        cit2++;
      }
    }

    /*-----------------------------------------------------*/

    tslassert(this->candidatesTree.size() == candidates.size());

    /*Recompute normals and update existing constraints----*/
    CandidateMapIterator it3 = this->candidates.begin();

    while(it3 != this->candidates.end()){
      Candidate* s = (*it3++).getData();

      try{
        /*Also sets the reference vectors such that the initial
          configuration yields a positive signed distance.*/
        s->recompute(this, x, com, rot);
      }catch(DegenerateCaseException* e){
        std::cerr << e->getError();
        delete e;
      }
    }

    /*-----------------------------------------------------*/

    this->startPosition = x;
    this->startCenterOfMass = com;
    this->startOrientation = rot;
    this->lastPosition = x;
    this->lastCenterOfMass = com;
    this->lastOrientation = rot;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSetEdge<T, CC>::showEdgeCandidateState(int edge){
    message("Show edge set %d", this->setId);
    message("vertex ids = %d, %d", vertexId[0], vertexId[1]);
    message("face ids = %d, %d", faceId[0], faceId[1]);

    message("Start position, com, orientation");

    std::cout << this->startPosition << std::endl;
    std::cout << this->startCenterOfMass << std::endl;
    std::cout << this->startOrientation << std::endl;

    message("Updated position, com, orientation");

    std::cout << this->lastPosition << std::endl;
    std::cout << this->lastCenterOfMass << std::endl;
    std::cout << this->lastOrientation << std::endl;

    message("set has %d candidates", this->candidates.size());

    CandidateMapIterator it = this->candidates.begin();
    while(it != this->candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(edge == -1){
        candidate->showCandidateState(this->lastPosition);
      }else if(candidate->getCandidateId() == edge){
        candidate->showCandidateState(this->lastPosition);
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSetEdge<T, CC>::updateCandidateStartPosition(int id,
                                                              const Edge<T>& edge,
                                                              const Vector4<T>& com,
                                                              const Quaternion<T>& rot){
    CandidateMapIterator it = this->candidates.find(id);

    if(it != this->candidates.end()){
      Candidate* candidate = (*it++).getData();
      candidate->updateInitialState(edge, com, rot, false);
    }
  }

  /**************************************************************************/
  template class ConstraintSetEdge<float, ContactConstraintCR<2, float> >;
  template class ConstraintSetEdge<double, ContactConstraintCR<2, double> >;
}
