/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintSet.hpp"
#include "collision/ConstraintCandidateFace.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace tsl{

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSet<T, CC>::ConstraintSet():
    AbstractConstraintSet<T, OrientedVertex<T>, Triangle<T>, CC>(){
    this->typeName = "vertex - face";
  }

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSet<T, CC>::~ConstraintSet(){
  }

  /**************************************************************************/
  /*Evaluate all candidates and constraints in this set given the
    provided EvalType value.*/
  template<class T, class CC>
  bool ConstraintSet<T, CC>::evaluate(OrientedVertex<T>& p, Vector4<T>& com,
                                      Quaternion<T>& rot,
                                      const EvalType& etype, EvalStats& stats){
    CandidateMapIterator it = this->candidates.begin();

    Tree<Candidate*> changedCandidates;
    Tree<Candidate*> changedIndices;

    bool evaluateGeometry    = etype.geometryCheck();
    bool evaluateConstraints = etype.existingCheck();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    if(evaluateGeometry){
      /*Perform a simple plane vertex test for all candidates*/

      while(it != this->candidates.end()){
        Candidate* c = (*it++).getData();

        if(!c->isValid()){
          if(c->getStatus() == Enabled){
            /*An invalid candidate with an enabled constraint is not
              possible*/
            error("Invalid candidate is still enabled");
          }

          try{
            this->ctx->mesh->getTriangle(&c->getInitial(),
                                         c->getCandidateId(),
                                         this->backFace);
            /*No exception -> pair is valid and can be used in the
              collision test.*/
            c->setValid(true);
          }catch(Exception* e){
            delete e;
            c->setValid(false);
          }
          if(!c->isValid()){
            /*Still not valid, skip*/
            continue;
          }
        }

        if(IsNan(p.getVertex()[0])){
          warning("p is nan");
          std::cout << p.getVertex() << std::endl;
        }

        /*Update geometry information of associated faces*/
        try{
          c->updateGeometry();
          stats.n_geometry_check++;
        }catch(Exception* e){
          warning("Exception updateGeometry 2 %s", e->getError().c_str());
          delete e;

          /*Update of geometry failed -> invalid candidate. Disable
            associated constraint, if available.*/
          c->setValid(false);
          if(c->getStatus() == Enabled){
            AbstractRowConstraint<2, T>* ccc =
              (AbstractRowConstraint<2, T>*)
              this->ctx->getMatrix()->getConstraint(c->getEnabledId());
            ccc->status = Inactive;
            c->disableConstraint();
          }
          continue;
        }

        /*Evaluate distance of current configuration, if distance
          becomes negative there is a collision*/

        /*Evaluate signed distance of pair*/
        if(c->evaluate(p, com, rot)){
          /*Changed in sign, inspect further*/
          changedCandidates.insert(c, 1);
        }
      }

      /*Perform rootfinding*/
      this->basicRootFinding(p, com, rot,
                             changedCandidates, changedIndices,
                             constraintsChanged, ActiveConstraints<CC>());
    }

    /*Evaluate all enabled constraints---------------------*/

    /*Evaluate all enabled constraints (except for new constraints)
      and perform some sub-constraint operations (friction)*/

    if(evaluateConstraints){
      this->basicEvaluation(changedIndices, etype, stats, constraintsChanged,
                            false, ActiveConstraints<CC>());
    }

    if(evaluateGeometry){
      this->lastPosition     = p; /*Store last position*/
      this->lastCenterOfMass = com;
      this->lastOrientation  = rot;
    }
    return constraintsChanged;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintSet<T, CC>::enableSlidedConstraint(int _faceId,
                                                    Vector4<T> gap){
    CandidateMapIterator it = this->candidates.begin();

    (warning("forcing slided vertex %d, face %d, backface %d",
             this->setId, _faceId, this->backFace));

    while(it != this->candidates.end()){
      CandidateType* candidate = (CandidateType*)(*it++).getData();

      if(candidate->getCandidateId() == _faceId){
        return candidate->enableSlidedConstraint(gap, this,
                                                 this->lastPosition,
                                                 this->lastCenterOfMass,
                                                 this->lastOrientation);
      }
    }

    warning("request for forcing a slided constraint for which no candidate exists!! Added to hotlist.");

    this->forcedPotentials.uniqueInsert(_faceId, _faceId);

    incrementalUpdate(true);

    it = this->candidates.begin();

    while(it != this->candidates.end()){
      CandidateType* candidate = (CandidateType*)(*it++).getData();

      if(candidate->getCandidateId() == _faceId){
        return candidate->enableSlidedConstraint(gap, this,
                                                 this->lastPosition,
                                                 this->lastCenterOfMass,
                                                 this->lastOrientation);
      }
    }
    return false;
  }

  /**************************************************************************/
  /*Checks the current state of the constraint using the actual
    geometric information. Since the constraint in the solver is
    linearized at x0, there will be a deviation between the measured
    distance in the solver and the actual geometric distance. In the
    worst case, the collision will be resolved according the solver,
    but might still be present in the actual geometric
    representation.

    This check measures the actual geometric distance. If the
    correcponding constraint is active, the actual distance should
    should be close to EPS +/- DISTANCE_TOL. If not active, the
    distance must be positive.

    If the actual distance at convergence is not within this range,
    the constraint must be updated (linearized again) given the
    current positions. If the contact normal is correct, only the
    constraint distance is updated.*/

  template<class T, class CC>
  bool ConstraintSet<T, CC>::checkAndUpdate(OrientedVertex<T>& p,
                                            Vector4<T>& com,
                                            Quaternion<T>& rot,
                                            EvalStats& stats,
                                            bool friction,
                                            bool force,
                                            T bnorm,
                                            bool* allPositive){
    CandidateMapIterator it = this->candidates.begin();

    /*if true, the solver is notified*/
    bool constraintsChanged = false;

    /*Simple plane point check for a fast detection in change*/
    while(it != this->candidates.end()){
      Candidate* c = (*it++).getData();

      if(c->getStatus() != Enabled){
        continue;
      }

      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(c->getEnabledId());

      if(!ActiveConstraints<CC>()){
        if(cc->status != Active){
          //continue;
        }
      }

      /*Check corresponding lambda*/
      if(!cc->valid(*this->ctx->getSolver()->getx())){
        DBG(message("CONSTRAINT NOT VALID"));
        //continue;
      }

      /*Update geometry information of associated faces*/
      try{
        c->updateGeometry();
      }catch(Exception* e){
        warning("Exception updateGeometry 1 %s", e->getError().c_str());
        error("exception!!");

        delete e;

        cc->status = Inactive;
        c->setValid(false);

        if(c->getStatus() == Enabled){
          c->disableConstraint();
        }
        continue;
      }

      if(!c->isValid()){
        if(c->getStatus() == Enabled){
          error("Invalid candidate is enabled");
        }
        continue;
      }

      /*Update value for current distance*/
      c->evaluate(p, com, rot);

      /*Check geometric distance and state of the constraint*/

      /*For enabled constraints an addtional distance of EPS is added,
        so a current custance of EPS corresponds to a constraint
        distance of 0. This constraint distance should be smaller than
        half the epsilon.*/

      /*In case of a collision from inside, this will push the vertex
        on the wrong side!!*/

      bool offTriangle = false;
      {
        Vector4<T> weights =
          c->getCurrent().getBarycentricCoordinates(p.getVertex());
        if(!c->getCurrent().barycentricInTriangle(weights,
                                                  this->ctx->distance_epsilon*0)){
          offTriangle = true;
          message("off triangle, weights");
          std::cout << weights << std::endl;
        }
      }

      if(!this->basicUpdateCheck(c, p, com, rot,
                                 offTriangle, ActiveConstraints<CC>(),
                                 constraintsChanged, allPositive,
                                 stats,
                                 this->ctx->distance_epsilon,
                                 this->ctx->distance_tol,
                                 this->ctx->safety_distance,
                                 (T)2.0,
                                 (T)0.1,
                                 (T)1.0,
                                 (T)0.0)){
        continue;
      }
    }

    return constraintsChanged;
  }


  /**************************************************************************/
  /*Updates the candidatelist due to some updated geometry. Each
    potential face that is not in the candidatelist, will be added.*/
  template<class T, class CC>
  void ConstraintSet<T, CC>::incrementalUpdate(bool onlyForced){
    if(this->ctx->mesh->vertices[this->setId].leaving == Mesh::UndefinedIndex){
      /*Internal vertex, is not a part of the outher boundary, skip*/
      return;
    }

    OrientedVertex<T> x;
    this->ctx->mesh->getOrientedVertex(&x, this->setId, this->backFace);

    Vector4<T> com;
    Quaternion<T> rot;

    if(Mesh::isRigid(this->type)){
      int objectId = this->ctx->mesh->vertexObjectMap[this->setId];

      com = this->ctx->mesh->centerOfMass[objectId];
      rot = this->ctx->mesh->orientations[objectId];
    }

    List<int> currentPotentials;  /*Stores face ids of found potentials*/

    if(!onlyForced){
      this->ctx->extractPotentialVertexFace(this->setId, currentPotentials,
                                            this->backFace);
    }

    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentFace = *lit++;
      this->ctx->mesh->halfFaces[currentFace].visited = true;
    }

    /*Add forced candidates*/
    Tree<int>::Iterator forcedIt = this->forcedPotentials.begin();
    while(forcedIt != this->forcedPotentials.end()){
      int forcedPotential = *forcedIt++;

      if(this->ctx->mesh->halfFaces[forcedPotential].visited == false){
        currentPotentials.append(forcedPotential);
        this->ctx->mesh->halfFaces[forcedPotential].visited = true;
        //currentPotentials.uniqueInsert(forcedPotential, forcedPotential);
      }
    }

    lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      int currentFace = *lit++;
      this->ctx->mesh->halfFaces[currentFace].visited = false;
    }

    this->forcedPotentials.clear();

    /*Add new candidates-----------------------------------*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;
      int index = this->candidatesTree.findIndex(candidate);

      if(index == -1){
        if(this->backFace){
          if(Mesh::isRigidOrStatic(this->ctx->mesh->halfFaces[candidate].type)){
            /*A rigid object can not have internal collisions, hence Rigid*/
            continue;
          }

          int faceId =
            this->ctx->mesh->halfEdges[this->ctx->mesh->vertices[this->setId].
                                       leaving].half_face;

          if(!this->ctx->stree->facesShareSameRoot(faceId, candidate)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        CandidateType* cc = new CandidateType(this->ctx, candidate,
                                              this->setId, this->backFace);

        this->ctx->mesh->setCurrentEdge(this->ctx->mesh->halfFaces[candidate].
                                        half_edge);

        int faceVertexId = this->ctx->mesh->getOriginVertex();

        cc->setSetType(this->type);
        cc->setCandidateType(this->ctx->mesh->vertices[faceVertexId].type);

        this->candidates.insert(candidate, cc);
        this->candidatesTree.insert(candidate, 1);

        /*Update geometrical info*/
        this->ctx->mesh->getTriangleIndices(candidate, cc->indices,
                                            this->backFace);

        cc->recompute(this, x, com, rot);
      }
    }
  }

  /**************************************************************************/
  /*Updates the geometry information*/
  template<class T, class CC>
  void ConstraintSet<T, CC>::update(){
    if(this->ctx->mesh->vertices[this->setId].leaving == Mesh::UndefinedIndex){
      /*Internal vertex, is not a part of the outher boundary, skip*/
      return;
    }

    if(this->backFace){
      if(Mesh::isRigidOrStatic(this->ctx->mesh->vertices[this->setId].type)){
        /*A rigid object can not have internal collisions*/
        return;
      }
    }

    OrientedVertex<T> x;
    this->ctx->mesh->getOrientedVertex(&x, this->setId, this->backFace);

    Vector4<T> com;
    Quaternion<T> rot;

    if(Mesh::isRigid(this->type)){
      int objectId = this->ctx->mesh->vertexObjectMap[this->setId];

      com = this->ctx->mesh->centerOfMass[objectId];
      rot = this->ctx->mesh->orientations[objectId];
    }

    /*Clear forced potentials*/
    this->forcedPotentials.clear();

    /*Find potential faces for vertex (this->setId)*/
    List<int> currentPotentials;
    this->ctx->extractPotentialVertexFace(this->setId, currentPotentials,
                                          this->backFace);

    /*Add new candidates which are note yet in the candidate list*/
    List<int>::Iterator it = currentPotentials.begin();

    while(it != currentPotentials.end()){
      int candidate = *it++;
      int index = this->candidatesTree.findIndex(candidate);

      if(index == -1){
        /*Current candidate not found in set, add*/

        if(this->backFace){
          if(Mesh::isRigidOrStatic(this->ctx->mesh->halfFaces[candidate].type)){
            /*A rigid object can not have internal collisions, hence Rigid*/
            continue;
          }

          int faceId =
            this->ctx->mesh->halfEdges[this->ctx->mesh->vertices[this->setId].
                                       leaving].half_face;

          if(!this->ctx->stree->facesShareSameRoot(faceId, candidate)){
            /*Both faces belong to different objects. By definition,
              there can't be internal backface collisions*/
            continue;
          }
        }

        CandidateType* cc = new CandidateType(this->ctx, candidate,
                                              this->setId, this->backFace);

        this->ctx->mesh->setCurrentEdge(this->ctx->mesh->halfFaces[candidate].
                                        half_edge);

        int faceVertexId = this->ctx->mesh->getOriginVertex();

        cc->setSetType(this->type);
        cc->setCandidateType(this->ctx->mesh->vertices[faceVertexId].type);

        this->candidates.insert(candidate, cc);
        this->candidatesTree.insert(candidate, 1);

        /*Update geometrical info*/
        this->ctx->mesh->getTriangleIndices(candidate, cc->indices,
                                            this->backFace);
      }
    }

    /*-----------------------------------------------------*/

    /*Check for disappeared candidates, remove them from the set.*/
    CandidateMapIterator tcit = this->candidates.begin();
    while(tcit != this->candidates.end()){
      Candidate* cc = (*tcit++).getData();
      cc->setFlag(false);
    }

    List<int>::Iterator lit = currentPotentials.begin();
    while(lit != currentPotentials.end()){
      tcit = this->candidates.find(*lit++);

      if(tcit != this->candidates.end()){
        (*tcit).getData()->setFlag(true);
      }
    }

    tcit = this->candidates.begin();
    while(tcit != this->candidates.end()){
      Candidate* cc = (*tcit).getData();

      /*If not found in new potential set, remove.*/
      if(cc->isFlagged() == false){
        /*Disable and remove*/
        /*Check if constraint is enabled*/

        if(cc->getStatus() == Enabled){
          //error("Deleting enabled constraint!!");
          tcit++;
        }else{
          cc->disableConstraint();
          this->candidates.remove(tcit);
          int cid = cc->getCandidateId();
          this->candidatesTree.remove(cid);
          delete cc;
        }
      }else{
        tcit++;
      }
    }

    /*Recompute states at beginning of timestep*/
    CandidateMapIterator it3 = this->candidates.begin();

    while(it3 != this->candidates.end()){
      Candidate* s = (*it3++).getData();
      s->recompute(this, x, com, rot);
    }

    /*-----------------------------------------------------*/

    this->startPosition     = x;
    this->startCenterOfMass = com;
    this->startOrientation  = rot;
    this->lastPosition      = x;
    this->lastCenterOfMass  = com;
    this->lastOrientation   = rot;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSet<T, CC>::showFaceCandidateState(int face){
    message("Show vertex set %d", this->setId);

    message("Start position, com, orientation");

    std::cout << this->startPosition << std::endl;
    std::cout << this->startCenterOfMass << std::endl;
    std::cout << this->startOrientation << std::endl;

    message("Updated position, com, orientation");

    std::cout << this->lastPosition << std::endl;
    std::cout << this->lastCenterOfMass << std::endl;
    std::cout << this->lastOrientation << std::endl;

    message("set has %d candidates", this->candidates.size());

    CandidateMapIterator it = this->candidates.begin();
    while(it != this->candidates.end()){
      Candidate* candidate = (*it++).getData();

      if(face == -1){
        candidate->showCandidateState(this->lastPosition);
      }else if(candidate->getCandidateId() == face){
        candidate->showCandidateState(this->lastPosition);
      }
    }
  }

  /**************************************************************************/
  template class ConstraintSet<float, ContactConstraintCR<2, float> >;
  template class ConstraintSet<double, ContactConstraintCR<2, double> >;
}
