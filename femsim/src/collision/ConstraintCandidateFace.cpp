/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintCandidateFace.hpp"
#include "collision/ConstraintCandidateEdge.hpp"
#include "collision/ConstraintSetEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "collision/ConstraintSet.hpp"
#include "core/Exception.hpp"
#include "geo/Barycentric.hpp"

#define DEBUG_UPDATE
#undef DEBUG_UPDATE

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  ConstraintCandidateFace<T, CC>::
  ConstraintCandidateFace(CollisionContext<T, CC>* ctx,
                          int f, int vid, bool b):
    AbstractConstraintCandidate<T, OrientedVertex<T>, Triangle<T>, CC>(b, ctx){
    this->candidateId = f;
    this->setId = vid;

    tangentialDefined = false;
    vertexCollapsed = false;

    this->ctx->mesh->getEdgeIndicesOfTriangle(f, edgeIndices);
  }

  /**************************************************************************/
  /*Check if there is an intersection */
  template<class T, class CC>
  bool ConstraintCandidateFace<T, CC>::
  checkCollision(const OrientedVertex<T>& p,
                 const Vector4<T>& com,
                 const Quaternion<T>& rot,
                 T* dist,
                 bool* plane){
    int colCode = -1;

    bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

    bool debug = false;

    if(false){
      debug = true;
      warning("checkCollision vf %d, %d", this->setId, this->candidateId);

      message("state + rigid transformations");

      std::cout << this->Yu << std::endl;
      std::cout << this->comYu << std::endl;
      std::cout << this->rotYu << std::endl;
      std::cout << this->Y0 << std::endl;
      std::cout << this->comY0 << std::endl;
      std::cout << this->rotY0 << std::endl;
      std::cout << p  << std::endl;
      std::cout << com << std::endl;
      std::cout << rot << std::endl;
      std::cout << this->X0 << std::endl;
      std::cout << this->comX0 << std::endl;
      std::cout << this->rotX0 << std::endl;
      getchar();
    }

    try{
      if(!rigidFace && !rigidVertex){
        T root = 0;

        Vector4<T> intersectedVertex;

        colCode = intersection(this->Yu, this->Y0, p.getVertex(),
                               this->X0.getVertex(),
                               intersectedVertex,
                               this->ctx->safety_distance,
                               &baryi, &this->Yi,
                               this->ctx->distance_epsilon,
                               (T)1.0, 1, &root,
                               this->forced);

        if(debug){
          message("root = %10.10e", root);
          message("bary = ");
          std::cout << baryi << std::endl;
        }

        if(dist)*dist = root;

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          this->Xi.setVertex(intersectedVertex);
        }else{
          return false;
        }
      }else{
        T root = 0;

        Vector4<T> intersectedVertex;

        colCode = rigidIntersection(this->Yu, this->comYu, this->rotYu,
                                    this->Y0, this->comY0, this->rotY0,
                                    p.getVertex(),  com,   rot,
                                    this->X0.getVertex(), this->comX0,
                                    this->rotX0,
                                    intersectedVertex,
                                    this->ctx->safety_distance,
                                    &baryi, &this->Yi,
                                    this->ctx->distance_epsilon,
                                    (T)1.0, 1,
                                    rigidFace, rigidVertex, &root,
                                    this->forced);

        if(debug){
          message("root = %10.10e", root);
          message("bary = ");
          std::cout << baryi << std::endl;
        }

        if(colCode == 1 && root >= (T)0.0 && root <= (T)1.0){
          this->comXi = this->comX0 * ((T)1.0 - root) + root * com;
          this->comYi = this->comY0 * ((T)1.0 - root) + root * this->comYu;

          this->rotXi = slerp(this->rotX0, rot,   root).normalized();
          this->rotYi = slerp(this->rotY0, this->rotYu, root).normalized();

          this->Xi.setVertex(intersectedVertex);
        }else{
          return false;
        }
        if(dist) *dist = root;
      }
    }catch(Exception* e){
      message("Exception in root finding:: %s", e->getError().c_str());
#if 1
      if(debug){
        warning("rootfinder failed");
        warning("Exception in root finding:: %s", e->getError().c_str());
        warning("rigidFace = %d", rigidFace);
        warning("rigidVertex = %d", rigidVertex);
        warning("face = %d", this->candidateId);
        warning("vertex = %d", this->setId);
        warning("t0, tu, x0, p");
        std::cerr << this->Y0 << std::endl;
        std::cerr << this->Yu << std::endl;
        std::cerr << this->X0 << std::endl;
        std::cerr << p << std::endl;

        getchar();
      }
#endif

      delete e;
      this->valid = false;
      return false;
    }

    if(debug){
      warning("colcode = %d", colCode);
      warning("barycentric = ");
      std::cerr << baryi << std::endl;

      warning("rigidFace = %d", rigidFace);
      warning("rigidVertex = %d", rigidVertex);
      warning("face = %d", this->candidateId);
      warning("vertex = %d", this->setId);
      warning("t0, tu, x0, p");
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << p << std::endl;

      getchar();
    }

    if(colCode == 1){
      int vertexEdges[3][3] = {{0, 1, 2},
                               {1, 2, 0},
                               {2, 1, 0}};
      for(int i=0;i<3;i++){
        /*Check distances of vertices and edges*/
        Vector4<T> edge = (this->Yi.v[vertexEdges[i][1]] -
                           this->Yi.v[vertexEdges[i][2]]).normalized();

        Vector4<T> pp =
          this->Yi.v[vertexEdges[i][0]] - this->Yi.v[vertexEdges[i][2]];

        T projection = dot(edge, pp);

        Vector4<T> projectedpp =
          this->Yi.v[vertexEdges[i][2]] + projection * edge;

        if((projectedpp - pp).length() < 5e-4){
          /*Too close*/
          warning("vertex too close to edge 1, triangle too small");
          return false;
        }
      }
    }

    if(colCode == 1){
      T dist = this->Yi.getSignedDistance(this->Xi.getVertex());

      if(Abs(dist) > 2e-8){
        return false;
      }

      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateFace<T, CC>::
  enableSlidedConstraint(Vector4<T> _gap,
                         ConstraintSet<T, CC>* c,
                         OrientedVertex<T> p,
                         Vector4<T> com,
                         Quaternion<T> rot){
    if(this->status == Enabled){
      //return false;
      //disableConstraint();
    }

    gap = _gap;

    this->Yi = this->Yu;
    this->Xi = p;

    this->comYi = this->comYu;
    this->rotYi = this->rotYu;

    this->comXi = com;
    this->rotXi = rot;

    this->Yi  = this->Yu;
    this->Yu  = this->Yu;
    //this->Y00 = this->Yu;

    //this->comY00 =
    this->comY0 = this->comYu = this->comYi;
    //this->rotY00 =
    this->rotY0 = this->rotYu = this->rotYi;

    this->X0  = p;
    this->Xi  = p;
    //this->X00 = p;

    //this->comX00 =
    this->comX0 = this->comXi = com;
    //this->rotX00 =
    this->rotX0 = this->rotXi = rot;


    baryi = this->Yi.getBarycentricCoordinates(p.getVertex());

    message("slided baryi = ");
    std::cout << baryi << std::endl;

    if(!this->Yi.barycentricInTriangleDist(baryi, this->ctx->distance_epsilon +
                                           this->ctx->distance_tol)){
      return false;
    }

    return enableConstraint(this->X0, p, &gap);
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateFace<T, CC>::evaluate(const OrientedVertex<T>& p,
                                                const Vector4<T>& com,
                                                const Quaternion<T>& rot,
                                                Vector4<T>* __bb1,
                                                Vector4<T>* __bb2,
                                                bool* __updated){
    T distance = 0.0;

    bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

    /*Check if one of the edges associated with p is collapsed, if so,
      skip evaluation until the collapsed state is resolved. If this
      pair is forced to resolve a collapse, then we should perform the
      test.*/

    if(!this->forced){
      List<int> edgeIds;
      p.getEdgeIndices(edgeIds);

      List<int>::Iterator eIdIt = edgeIds.begin();

      while(eIdIt != edgeIds.end()){
        int edgeId = *eIdIt++;

        if(this->backFace){
          if(this->ctx->collapsedBackEdges[edgeId]){
            //message("edge connected to vertex is collapsed");
            return false;
          }
        }else{
          if(this->ctx->collapsedEdges[edgeId]){
            //message("edge connected to vertex is collapsed");
            return false;
          }
        }
      }

      bool collapsed = false;

      if(rigidFace || rigidVertex){
        collapsed = false;
      }else{
        collapsed = orientedVertexCollapsed(this->X0, p);
      }

      if(collapsed){
        START_DEBUG;
        std::cout << this->X0 << std::endl;
        std::cout << p  << std::endl;
        warning("collapsed vertex v %d - f %d - backface = %d",
                this->setId, this->candidateId, this->backFace);

        END_DEBUG;
        vertexCollapsed = true;
        return false;
      }else{
        vertexCollapsed = false;
      }
    }else{
      /*Forced*/
      vertexCollapsed = false;
    }

    this->intersectionc =
      p.planeIntersection(this->Yu.v[0], this->Yu.n,
                          indices,
                          this->forced || rigidFace || rigidVertex,
                          false);

    //if(this->distance0 >= 0.0){
      /*Initial distance is positive, so there can only be a frontface
        collision*/
    distance = this->Yu.getSignedDistance(p.getVertex(), &bary2) -
      (T)(this->ctx->safety_distance*0.0);
    this->distancec = distance;
      //}else{
      /*Initial distance is negative, so there can only be a backface
        collision*/
      //distance = this->Yu.getSignedDistance(p, &bary2) + this->ctx->safety_distance;
      //this->distancec = distance;
      //}

    if(!this->forced){
      /*Not a forced pair, check for collapsed edges forming this triangle*/
      for(int i=0;i<3;i++){
        //if(this->backFace){
          if(this->ctx->collapsedBackEdges[edgeIndices[i]]){
            DBG(warning("Ignored vertex-face case due to collapsed edge"));
            return false;
          }
          //}else{
          if(this->ctx->collapsedEdges[edgeIndices[i]]){
            DBG(warning("Ignored vertex-face case due to collapsed edge"));
            return false;
          }
          //}
      }
    }

    int vertexEdges[3][3] = {{0, 1, 2},
                             {1, 2, 0},
                             {2, 1, 0}};
    for(int i=0;i<3;i++){
      /*Check distances of vertices and edges*/
      Vector4<T> edge =
        (this->Yu.v[vertexEdges[i][1]] -
         this->Yu.v[vertexEdges[i][2]]).normalized();

      Vector4<T> pp =
        this->Yu.v[vertexEdges[i][0]] - this->Yu.v[vertexEdges[i][2]];

      T projection = dot(edge, pp);

      Vector4<T> projectedpp =
        this->Yu.v[vertexEdges[i][2]] + projection * edge;

      if((projectedpp - pp).length() < 5e-4){
        /*Too close*/
        //warning("vertex too close to edge, triangle too small");
        //return false;
      }
    }

    /*Check if oriented vertex does not have a too small edge*/
    if(p.getSmallestEdge() < 5e-4){
      warning("vertex connected to too small edge");
      return false;
    }

    bool debug = false;

    if(false){
      warning("face     = %d", this->candidateId);
      warning("vertex   = %d", this->setId);
      warning("backface = %d", this->backFace);
      warning("enabled  = %d", this->status == Enabled);

      std::cerr << p << std::endl;
      std::cerr << this->Yu << std::endl;
      warning("distance0     = %10.10e", this->distance0);
      warning("distancec     = %10.10e", this->distancec);
      warning("intersection0 = %d", this->intersection0);
      warning("intersectionc = %d", this->intersectionc);
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->Yu << std::endl;
      std::cerr << this->X0 << std::endl;
      std::cerr << p << std::endl;

      message("intersection0");
      this->intersection0 =
        this->X0.planeIntersection(this->Y0.v[0], this->Y0.n,
                                   indices, false, true);

      message("intersectionc");
      this->intersectionc = p.planeIntersection(this->Yu.v[0], this->Yu.n,
                                                indices, false, true);
      debug = true;


      message("state + rigid transformations");


      std::cout << this->Yu << std::endl;
      std::cout << this->comYu << std::endl;
      std::cout << this->rotYu << std::endl;
      std::cout << this->Y0 << std::endl;
      std::cout << this->comY0 << std::endl;
      std::cout << this->rotY0 << std::endl;
      std::cout << p  << std::endl;
      std::cout << com << std::endl;
      std::cout << rot << std::endl;
      std::cout << this->X0 << std::endl;
      std::cout << this->comX0 << std::endl;
      std::cout << this->rotX0 << std::endl;


      getchar();
    }



    if(IsNan(distance)){
      std::cout << this->Yu << std::endl;
      std::cout << p << std::endl;
      error("NAN detected");
    }

    if(this->status != Enabled){
      /*In the old approach we used the signed distance to determine
        if a collision had occured. This works fine as long the motion
        is approximately linear. However, in case of extreme
        deformations, triangles can become small and the non-linearity
        of the motion increases. In general, when there is a
        transistion between a non-intersecting to an intersecting
        state, one can assume that in between there should be a
        configuration in which the triangles are just
        touching. However, it is possible that the initial
        configuration is also 'intersecting'. Since we only test
        infinite planes and edges, they could have a collision, while
        the collision points are outside the triangle/edge. Normally
        such a case is resolved by surrounding faces/edges such that
        at the beginning of the next time-step, that collision is
        'resolved'.  There was however no real collision, but now the
        entities are on the proper side of each other. If they not
        start to collide, we have a clear transistion between a
        non-intersecting to an intersecting case.

        If the motion is highly non-linear, it is possible that a
        particular vertex is behind a face at the beginning of the
        time-step (but outside the triangle and touching neighbouring
        faces). When the vertex moves in the direction of the face,
        eventually it goes of the current face and crosses the
        neighboring face from behind, resulting in a non-intersecting
        state. If the neighboring face does not change much, then at
        the next time-step, it will probably intersect the face again
        from the front side. However, if in the previous time-step,
        where it crosses the backface, the face changes orientation,
        it is possible that it crosses through the front-face first. A
        clear intersection/collision. However, if we only look at the
        signed distances, they are both negative at the beginning and
        end of the time-step. So testing for a sign-change on the
        signed distance won't work. They are in both cases
        negative. Also the face and vertex are intersecting at both
        the beginning and end of the timestep. So there is no
        transistion between a non-intersecting to an intersecting
        state. Hence, the crossing is not detected. This eventually
        result in a failure of the simulation since collisions are
        missed and the system relies on a collision free state.

        In the new approach we don't look for a crossing, but start
        the rootfinder immediately after an intersecting state is
        found. Using the knowledge that multiple zero crossings exist
        (for the signed distance) we are able to find them using a
        different root finding technique. This guarantees that all
        collisions are found.

        The described problem is especially notable for vertices close
        to the edge of a triangle and is about to cross the edge to a
        neighboring triangle. */

      Vector4<T> bary0 =
        this->Y0.getBarycentricCoordinates(this->X0.getVertex());

      Vector4<T> baryc =
        this->Yu.getBarycentricCoordinates(p.getVertex());

      bool projection = barycentricFaceMatch(bary0, baryc);
      projection = false;

      if(rigidVertex || rigidFace){
        if(debug){message("rigid vertex or face");getchar();}

        if(projection){
          if(this->distancec <= 0.0 && this->distance0 > 0.0){
            if(debug){
              message("intersection based on distances %10.10e, %10.10e",
                      this->distancec, this->distance0);
              getchar();
            }
            return true;
          }
        }else{
          if((this->distancec <= 0.0 && this->distance0 >= 0.0)
             ||
             (this->distancec <= 0.0 && this->distance0 <= 0.0)){

            if(debug){
              message("intersection based on distances %10.10e, %10.10e",
                      this->distancec, this->distance0);
              getchar();
            }
            return true;
          }
        }
#if 0
        if( (this->distancec <= 0.0 && this->distance0 > 0)
            ||
            (this->distancec >= 0.0 && this->distance0 > 0.0)
            ){
          if(debug){
            message("intersection based on distances %10.10e, %10.10e",
                    this->distancec, this->distance0);
            getchar();
          }
          return true;
        }
#endif
      }else{
        if(projection){
          if(this->distancec <= 0.0 && this->distance0 > 0.0 &&
             this->intersectionc && !this->intersection0){
            if(debug){
              message("intersection based on distances %10.10e, %10.10e",
                      this->distancec, this->distance0);
              getchar();
            }
            return true;
          }
        }else{
          if((this->distancec <= 0.0 && this->distance0 >= 0.0 &&
              this->intersectionc && !this->intersection0)
             ||
             (this->distancec <= 0.0 && this->distance0 <= 0.0 &&
              this->intersectionc && this->intersection0
              )){
            if(debug){
              message("intersection based on distances %10.10e, %10.10e",
                      this->distancec, this->distance0);
              getchar();
            }
            if(debug){
              message("intersection based on status %d, %d",
                      this->intersection0, this->intersectionc);
              getchar();
            }
            return true;
          }
        }
        //
      }
#if 0
      }else if((
                (this->distancec <= 0 && this->distance0 >= 0)
                ||
                (this->distancec >= 0 && this->distance0 >= 0)
                )
               &&
               ((this->intersection0 == false && this->intersectionc == true)
                ||
               //(this->intersection0 && !this->intersectionc)||
                (!this->intersection0 && !this->intersectionc))
               //)
               //(this->intersection0 == false && this->distancec < 0.0 &&
               //intersectionc == true
               //&& this->distancec <= 0.0 && this->distance0 > 0.0
         //||
         //(forced && this->distancec <= 1e-9 && this->distance0 >= 0.0)
         ){
        if(debug){
          message("intersection based on stated %d, %d",
                  this->intersection0, this->intersectionc);
          getchar();
        }
        DBG(message("intersection"));
        if(this->forced){
          DBG(message("forced intersection"));
        }
        return true;
      }
#endif
    }else{
      /*Constraint is enabled check projection*/
    }
    return false;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::
  updateGeometry(){
    try{
      this->ctx->mesh->getDisplacedTriangle(&this->Yu, this->candidateId,
                                            this->backFace);
      this->valid = true;
    }catch(Exception* e){
      warning("Singular displaced triangle, %s", e->getError().c_str());
      warning("face id = %d", this->candidateId);
      std::cout << this->Yu << std::endl;
      getchar();
      this->valid = false;
      throw e;
    }

    /*If normal is ok, compute signed distance of the face with its
      own vertex*/

    T det = (T)0.0;
    T sd = this->Yu.getSignedDistance(this->Yu.v[0], 0, 0, &det);

    if(IsNan(sd) || Abs(det) < 1e-19) {
      warning("sd = %10.10e, det = %10.10e", sd, det);
      throw new DegenerateCaseException(__LINE__, __FILE__, "Degenerate case");
    }

    this->ctx->mesh->setCurrentEdge(this->ctx->mesh->
                                    halfFaces[this->candidateId].half_edge);

    int vertex = this->ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(this->ctx->mesh->vertices[vertex].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[vertex];

      this->comYu = this->ctx->mesh->centerOfMassDispl[objectId];
      this->rotYu = this->ctx->mesh->orientationsDispl[objectId];
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::
  updateConstraint(const OrientedVertex<T>& x,
                   const OrientedVertex<T>& p,
                   const Vector4<T>& com,
                   const Quaternion<T>& rot,
                   bool* skipped,
                   EvalStats& stats){
    CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);

#ifdef DEBUG_UPDATE
    warning("updateConstraint");
    std::cerr << this->Y0 << std::endl;
    std::cerr << this->Yu << std::endl;
    std::cerr << this->X0 << std::endl;
    std::cerr << p << std::endl;
    std::cerr << x << std::endl;
    std::cerr << this->Xi << std::endl;
    std::cerr << this->Yi << std::endl;
    std::cerr << this->enabledX0 << std::endl;
    std::cerr << this->enabledXu << std::endl;
    std::cerr << this->enabledY0 << std::endl;
    std::cerr << this->enabledYu << std::endl;
#endif

    T initialWeight = (T)1.2;

    bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

    Triangle<T> newFace;
    Vector4<T> newVertex;
    Vector4<T> newNormal;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

#ifdef DEBUG_UPDATE
    cc->print(std::cerr);
#endif

    /*Search for a new interpolated instance of the vertex and face in
      which the contactnormal does not change too much.*/
    while(true){
      weight1 = (T)1.0 - (T)1.0/initialWeight;
      weight2 = (T)1.0 - weight1;

      Triangle<T> newFace2;
      Vector4<T> newVertex2;

      if(rigidFace){
        Vector4<T> newCom = weight1 * this->comYi + weight2 * this->comYu;
        Quaternion<T> newRot = slerp(this->rotYi,
                                     this->rotYu, weight2).normalized();
        Vector4<T> v11 = newCom + newRot*(this->Y00.v[0] - this->comY00);
        Vector4<T> v12 = newCom + newRot*(this->Y00.v[1] - this->comY00);
        Vector4<T> v13 = newCom + newRot*(this->Y00.v[2] - this->comY00);

        newFace2.set(v11, v12, v13);
      }else{
        newFace2.set(weight1 * this->Yi.v[0] + weight2 * this->Yu.v[0],
                     weight1 * this->Yi.v[1] + weight2 * this->Yu.v[1],
                     weight1 * this->Yi.v[2] + weight2 * this->Yu.v[2]);
      }

      if(rigidVertex){
        Vector4<T> newCom = weight1 * this->comXi + weight2 * com;
        Quaternion<T> newRot = slerp(this->rotXi, rot, weight2);

        Vector4<T> v1 = newCom + newRot*(this->X00.getVertex() - this->comX00);
        newVertex2 = v1;
      }else{
        newVertex2 = weight1 * this->Xi.getVertex() + weight2 * p.getVertex();
      }

      if(dot(this->Yi.n, newFace2.n) > 0.85){
        /*Accept current face and vertex*/
        newFace   = newFace2;
        newVertex = newVertex2;
        break;
      }
      initialWeight *= (T)1.25;
    }

#ifdef DEBUG_UPDATE
    warning("Face edges1 = %10.10e", (this->Yu.v[0] - this->Yu.v[1]).length());
    warning("Face edges2 = %10.10e", (this->Yu.v[1] - this->Yu.v[2]).length());
    warning("Face edges3 = %10.10e", (this->Yu.v[2] - this->Yu.v[0]).length());
    warning("Normal error = %10.10e", dot(this->Yi.n, newFace.n));
#endif

    Vector4<T> bary = newFace.getBarycentricCoordinates(newVertex);
    Vector4<T> weights = bary;

    message("dot = %10.10e", dot(this->Yi.n, newFace.n));
    message("weights");
    std::cout << bary << std::endl;
    std::cout << baryi << std::endl;
    std::cout << weights << std::endl;
    std::cout << baryi-weights << std::endl;
    std::cout << (baryi-weights).length() << std::endl;

    ////if((dot(this->Yi.n, newFace.n) < (T)0.9999 && rootCheck) ||
    ////   true ||
    ////   (T)diff.length() > 1e-2){
       //(baryi-weights).length() > 1e-4){
    {
#ifdef DEBUG_UPDATE
      warning("t0 x0");
      std::cerr << this->Y0 << std::endl;
      std::cerr << this->X0 << std::endl;

      warning("sd = %10.10e", this->Y0.getSignedDistance(this->X0.getVertex()));

      warning("ti xi");
      std::cerr << this->Yi << std::endl;
      std::cerr << this->Xi << std::endl;

      warning("sd = %10.10e", this->Yi.getSignedDistance(this->Xi.getVertex()));
#endif

      /*Update intersecting geometry*/
      this->Yi = newFace;

      if(rigidFace){
        this->comYi = weight1 * this->comYi + weight2 * this->comYu;
        this->rotYi = slerp(this->rotYi, this->rotYu, weight2).normalized();
      }

      if(rigidVertex){
        this->comXi = weight1 * this->comXi + weight2 * com;
        this->rotXi = slerp(this->rotXi, rot, weight2).normalized();
      }

      this->Xi.setVertex(newVertex);

#ifdef DEBUG_UPDATE
      warning("tii xii");
      std::cerr << this->Yi << std::endl;
      std::cerr << this->Xi << std::endl;

      warning("sd = %10.10e", this->Yi.getSignedDistance(this->Xi.getVertex()));

      warning("coordinates");
      std::cerr << this->Yi.getCoordinates(weights) << std::endl;
      std::cerr << this->Xi << std::endl;
#endif

      if(ClampsWeights<CC>()){
        if(this->forced){
          weights =
            this->Yi.edgeCrossedBarycentricCoordinates(this->Yi.getCoordinates(weights));
        }else{
          weights = this->Yi.clampWeights(weights, WEIGHT_MIN, WEIGHT_MAX);
        }
        baryi = weights;
      }else{
        baryi = weights;
      }

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;

      warning("coordinates");
      std::cerr << this->Yi.getCoordinates(weights) << std::endl;
      std::cerr << this->Xi << std::endl;
#endif

      if(!this->Yi.barycentricInTriangleDist(bary, this->ctx->total_epsilon)){
        message("contact off triangle");
        std::cout << weights << std::endl;
      }

      /*By testing the barycentric coordinates alone does not
        guarantee that a neighboring face takes over this contact
        point once this candidate is disabled. The problem is that in
        case of non-linear motion (i.e., with rigid bodies), */

      if(!this->forced &&
         !this->Yi.barycentricInTriangleDist(bary,
                                             this->ctx->distance_epsilon +
                                             this->ctx->distance_tol) &&
         this->distancec > 10.0*this->ctx->safety_distance){
        /*The contact point is outside the triangle, but has a
          positive signed distance. Update the constraint, but do not
          report it.*/
        *skipped = true;
      }

      Vector4<T> t1, t2;

      int indices[3];
      this->ctx->mesh->getTriangleIndices(this->candidateId, indices,
                                          this->backFace);

      T dt = this->ctx->getDT();

      /*Check if face id is from the body or the constraining geometry*/
      bool bodyVertex  = true;
      bool bodyFace    = true;
      bool rigidVertex = false;
      bool rigidFace   = false;

      if(Mesh::isStatic(this->ctx->mesh->vertices[this->setId].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(this->ctx->mesh->vertices[this->setId].type)){
        rigidVertex = true;
      }

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      int cidx2 = 0;

      if(bodyVertex){
        if(rigidVertex){
          cidx2 = 2;
        }else{
          cidx2 = 1;
        }
      }

      /*Retrieve tangent vectors from current constraint*/
      if(bodyVertex){
        if(rigidVertex){
          t1 += -cc->getRow(1, 0);
          t2 += -cc->getRow(2, 0);
        }else{
          t1 += -cc->getRow(1, 0);
          t2 += -cc->getRow(2, 0);
        }
      }

      if(bodyFace){
        if(rigidFace){
          t1 += cc->getRow(1, cidx2);
          t2 += cc->getRow(2, cidx2);
        }else{
          t1 += (cc->getRow(1, cidx2) + cc->getRow(1, cidx2+1) +
                 cc->getRow(1, cidx2+2));
          t2 += (cc->getRow(2, cidx2) + cc->getRow(2, cidx2+1) +
                 cc->getRow(2, cidx2+2));
        }
      }

      t1.normalize();
      t2.normalize();

      /*Re-align tangent vectors with contact normal*/
      Vector4<T> tt2 = cross(t1, this->Yi.n).normalized();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      Vector4<T> tt1 = cross(t2, this->Yi.n).normalized();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

      Vector4<T> rv, rf;

      if(rigidVertex){
        rv = this->Xi.getVertex() - this->comXi;
      }

      if(rigidFace){
        rf = this->Yi.getCoordinates(baryi) - this->comYi;
      }

      T totalDistance =
        (this->Yi.getSignedDistance(this->X00.getVertex()) -
         this->Yi.getSignedDistance(this->Y00.v[0]) * baryi[0] -
         this->Yi.getSignedDistance(this->Y00.v[1]) * baryi[1] -
         this->Yi.getSignedDistance(this->Y00.v[2]) * baryi[2]) -
        this->ctx->safety_distance - this->ctx->distance_epsilon;

      int cidx = 0;

      /*Compute normal and tangential distances*/
      T totalDistanceT1 =
        -(dot(t1, this->Xi.getVertex() - this->X00.getVertex()) -
          dot(t1, this->Yi.v[0] - this->Y00.v[0]) * baryi[0] -
          dot(t1, this->Yi.v[1] - this->Y00.v[1]) * baryi[1] -
          dot(t1, this->Yi.v[2] - this->Y00.v[2]) * baryi[2]);

      T totalDistanceT2 =
        -(dot(t2, this->Xi.getVertex() - this->X00.getVertex()) -
          dot(t2, this->Yi.v[0] - this->Y00.v[0]) * baryi[0] -
          dot(t2, this->Yi.v[1] - this->Y00.v[1]) * baryi[1] -
          dot(t2, this->Yi.v[2] - this->Y00.v[2]) * baryi[2]);

      /*Different approach*/
      totalDistanceT1 = dot(gap, t1);
      totalDistanceT2 = dot(gap, t2);

      if(this->distancec > 10 * this->ctx->safety_distance &&
         this->distancec < (this->ctx->distance_tol +
                            this->ctx->distance_epsilon)){
        /*Constraint ok, but don't report a change*/
        *skipped = true;
        message("in range");
      }

      /*UpdateConstraint with new normal and tangential vectors, and
        distances*/
      Vector4<T> cn = this->Yi.n;

      if(bodyVertex){
        int velIndex = this->ctx->mesh->vertices[this->setId].vel_index/3;

        if(rigidVertex){
          cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(rv, cn)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(-cross(rv, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(-cross(rv, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

          cidx = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(t1*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(t2*dt, totalDistanceT2, 2, cidx, velIndex);

          cc->setRow(cross(rf,cn)*dt, totalDistance,   0, cidx+1, velIndex+1);
          cc->setRow(cross(rf,t1)*dt, totalDistanceT1, 1, cidx+1, velIndex+1);
          cc->setRow(cross(rf,t2)*dt, totalDistanceT2, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*baryi[0]*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(t1*baryi[0]*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(t2*baryi[0]*dt, totalDistanceT2, 2, cidx, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(cn*baryi[1]*dt, totalDistance,   0, cidx+1, velIndex);
          cc->setRow(t1*baryi[1]*dt, totalDistanceT1, 1, cidx+1, velIndex);
          cc->setRow(t2*baryi[1]*dt, totalDistanceT2, 2, cidx+1, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(cn*baryi[2]*dt, totalDistance,   0, cidx+2, velIndex);
          cc->setRow(t1*baryi[2]*dt, totalDistanceT1, 1, cidx+2, velIndex);
          cc->setRow(t2*baryi[2]*dt, totalDistanceT2, 2, cidx+2, velIndex);
        }
      }
    }

    updated = true;

    /*Set transposed values*/
    cc->init(this->ctx->getMatrix());
    cc->update(*this->ctx->getSolver()->getb(),
               *this->ctx->getSolver()->getx(),
               *this->ctx->getSolver()->getb2());

    stats.n_geometry_check++;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::updateInitial(const OrientedVertex<T>& x,
                                                     const OrientedVertex<T>& p,
                                                     const Vector4<T>& com,
                                                     const Quaternion<T>& rot,
                                                     bool* skipped,
                                                     EvalStats& stats){
    Vector4<T> weights = this->Yu.getBarycentricCoordinates(p.getVertex());
    bool active = false;

    if(this->status == Enabled){
      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(cc->status == Active){
        active = true;
      }

      if(cc->status != Active){
        if(this->distancec < this->ctx->safety_distance
           /*|| c->distancec > this->ctx->distance_epsilon +
             this->ctx->distance_tol*/){
          //disableConstraint();
          //constraintsChanged = true;
          //continue;
          //*skipped = true;
        }
      }
    }

    //if(this->distance0 < 0.0){

    //}else{
    if(this->distancec < 0.0*this->ctx->safety_distance){//-this->ctx->distance_tol){
      //return;
    }
      //}

    active = false;
    active = active;
#if 0
    if(!active){
#if 0
      //this->Y0 = this->Yu;

      T interp0 = (T)0.125;
      T interp1 = (T)1.0 - interp0;

      bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
      bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

      if(rigidFace){
        this->comY0 = this->comYu*interp1 + this->comY0*interp0;
        this->rotY0 = slerp(this->rotY0, this->rotYu, interp1).normalized();

        Vector4<T> tv00 = this->comY0 + this->rotY0*(this->Y00.v[0] -
                                                     this->comY00);
        Vector4<T> tv01 = this->comY0 + this->rotY0*(this->Y00.v[1] -
                                                     this->comY00);
        Vector4<T> tv02 = this->comY0 + this->rotY0*(this->Y00.v[2] -
                                                     this->comY00);

        this->Y0 = Triangle<T>(tv00, tv01, tv02);
      }else{
        Vector4<T> tv00 = this->Y0.v[0]*interp0 + this->Yu.v[0]*interp1;
        Vector4<T> tv01 = this->Y0.v[1]*interp0 + this->Yu.v[1]*interp1;
        Vector4<T> tv02 = this->Y0.v[2]*interp0 + this->Yu.v[2]*interp1;

        this->Y0 = Triangle<T>(tv00, tv01, tv02);
      }

      //this->X0 = p;

      if(rigidVertex){
        this->comX0 = com*interp1 + this->comX0*interp0;
        this->rotX0 = slerp(this->rotX0, rot, interp1).normalized();

        this->X0.setVertex(this->comX0 + this->rotX0*(this->X00.getVertex() -
                                                      this->comX00));
      }else{
        this->X0.setVertex(p.getVertex() * interp1 + this->X0.getVertex() * interp0);
      }

      this->distance0 = this->distancec;
      this->intersection0 = this->intersectionc;

      this->intersection0 =
        this->X0.planeIntersection(this->Y0.v[0], this->Y0.n, indices, rigidFace || rigidVertex, false);
      this->distance0 =
        this->Y0.getPlaneDistance(this->X0.getVertex()) -
        (T)(this->ctx->safety_distance*0.0);
#else
      this->Y0 = this->Yu;
      this->comY0 = this->comYu;
      this->rotY0 = this->rotYu;

      this->X0 = p;
      this->comX0 = com;
      this->rotX0 = rot;

      this->distance0 = this->distancec;
      this->intersection0 = this->intersectionc;
#endif
    }
#endif

    if(this->status == Enabled){
      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(cc->status != Active){
        *skipped = false;
        return;
      }


      if(!this->forced
         &&
         !this->Yu.barycentricInTriangleDist(weights,
                                             this->ctx->distance_epsilon +
                                             this->ctx->distance_tol)
         //&&
         //this->distancec > 10*this->ctx->safety_distance//+this->ctx->distance_tol
         //this->distancec > this->ctx->distance_tol
         //this->distancec > this->ctx->distance_epsilon - this->ctx->distance_tol
         ){

        //if((this->setId == 32186 && this->candidateId == 18822) ||
        //   (this->setId == 32186 && this->candidateId == 18823)){
          message("try to disabling constraint  VF %d - %d, update initial, active = %d",
                  this->setId, this->candidateId, cc->status == Active);
          message("distance = %10.10e", this->distancec);
          std::cout << weights << std::endl;
          //  getchar();
          //}

        /*
        this->Y0 = this->Yu;
        this->comY0 = this->comYu;
        this->rotY0 = this->rotYu;

        this->X0 = p;
        this->comX0 = com;
        this->rotX0 = rot;

        this->distance0 = this->distancec;
        this->intersection0 = this->intersectionc;*/

        /*Figure put which neighboring face should be enabled.*/
        int edgeIndices[3];
        int targetFace = -1;
        int targetEdge = -1;
        Vector4<T> targetGap;

        this->ctx->mesh->getEdgeIndicesOfTriangle(this->candidateId,
                                                  edgeIndices);

        for(int i=0;i<3;i++){
          int neighboringFaceIndices[2];

          this->ctx->mesh->setCurrentEdge(edgeIndices[i]);

          message("current edge = %d", edgeIndices[i]);

          neighboringFaceIndices[0] = this->ctx->mesh->getFace();
          neighboringFaceIndices[1] = this->ctx->mesh->getTwinFace();

          int currentFace = -1;
          for(int j=0;j<2;j++){
            message("face = %d", neighboringFaceIndices[j]);
            if(neighboringFaceIndices[j] == this->candidateId){
              continue;
            }
            currentFace = neighboringFaceIndices[j];
          }

          message("current face = %d", currentFace);

          Edge<T> currentEdge;

          this->ctx->mesh->getEdge(&currentEdge, edgeIndices[i],
                                   this->backFace);

          Vector4<T> center =
            (this->Yu.v[0] + this->Yu.v[1] + this->Yu.v[2])/(T)3.0;

          Vector4<T> res, baryEdge;

          T root = (T)0.0;

          if(lineSegmentEdgeIntersection(center, p.getVertex(),
                                         currentEdge.vertex(0),
                                         currentEdge.vertex(1),
                                         this->Yu.n, res,
                                         &root, &baryEdge)){
            targetFace = currentFace;
            targetEdge = edgeIndices[i];
            targetGap = res;
            message("intersection");
            std::cout << res << std::endl;
          }
          message("t = %10.10e, intersection bary %10.10e, %10.10e",
                  root, baryEdge[0], baryEdge[1]);
        }

        /*Enable targetFace*/
        if(targetFace == -1){
          warning("No target face found for vertex - face %d, %d",
                  this->setId, this->candidateId);
          *skipped = false;
          return;
        }

        Edge<T> boundaryEdge;
        this->ctx->mesh->getEdge(&boundaryEdge, targetEdge, this->backFace);
        bool neighborEnabled = false;

        targetGap = targetGap - this->X00.getVertex();

        bool edgeEdgeActive = false;

        /*Figure out if an edge-edge constraint is active between the
          crossed edge, and an edge connected to this vertex*/
        Tree<int> connectedEdges;
        this->ctx->mesh->getConnectedEdgesOfVertex(this->setId, connectedEdges);

        Tree<int>::Iterator it = connectedEdges.begin();

        while(it != connectedEdges.end()){
          int currentConnectedEdge = *it++;
          int edgeset = -1;
          int edgecandidate = -1;

          if(targetEdge < currentConnectedEdge){
            edgeset = targetEdge;
            edgecandidate = currentConnectedEdge;
          }else{
            edgeset = currentConnectedEdge;
            edgecandidate = targetEdge;
          }

          message("inspect edge - edge %d - %d", edgeset, edgecandidate);

          if(this->backFace){
            typename Map<int, AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>*>::Iterator cit =
              this->ctx->backEdgeSets[edgeset].
              getCandidates().find(edgecandidate);

            if(cit != this->ctx->backEdgeSets[edgeset].getCandidates().end()){
              message("backedge candidate exists");
              AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>* edgeCandidate = cit->getData();
              if(edgeCandidate->getStatus() == Enabled){
                /*There is an Enabled constraint*/
                edgeEdgeActive = true;
                message("active");
              }
            }
          }else{
            typename Map<int, AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>*>::Iterator cit =
              this->ctx->edgeSets[edgeset].getCandidates().find(edgecandidate);

            if(cit != this->ctx->edgeSets[edgeset].getCandidates().end()){
              message("edge candidate exists");
              AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>* edgeCandidate = cit->getData();
              if(edgeCandidate->getStatus() == Enabled){
                /*There is an Enabled constraint*/
                edgeEdgeActive = true;
                message("active");
              }
            }
          }
        }

        if(edgeEdgeActive){
          message("Edge-Edge constraint active");
        }

        //if(!boundaryEdge.isConvex()){
        if(!edgeEdgeActive){
          if(this->backFace){
            neighborEnabled =
              this->ctx->backSets[this->setId].
              enableSlidedConstraint(targetFace, targetGap);
          }else{
            neighborEnabled =
              this->ctx->sets[this->setId].
              enableSlidedConstraint(targetFace, targetGap);
          }
        }
        //}
        message("neighbor enabled = %d", neighborEnabled);

        if(cc->status != Active){
          *skipped = false;
        }else{
          if(neighborEnabled){
            *skipped = true;
          }
        }

        if(boundaryEdge.isConvex()){
          message("convex edge, disable constraint");
          if(cc->status == Active){
            *skipped = true;
          }

          disableConstraint();
        }else{
          /*Concave edge. By keeping this constraint enabled, we allow
            to move the vertex in over the edge.*/
        }

        message("changed = %d", *skipped);
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateFace<T, CC>::
  enableConstraint(const OrientedVertex<T>& s,
                   const OrientedVertex<T>& p,
                   Vector4<T>* gapOverride){
    if(this->status == Enabled){
      return false;
    }else{
      CC* cc = new CC();

      cc->sourceType = 2;
      cc->sourceA = this->candidateId;
      cc->sourceB = this->setId;

      if(this->backFace){
        cc->sourceType = 3;
      }

      this->enabledX0 = this->X0;
      this->enabledXu = p;
      this->enabledY0 = this->Y0;
      this->enabledYu = this->Yu;

      Vector4<T> weights = baryi;
      if(ClampsWeights<CC>()){
        if(this->forced){
          weights = this->Yi.edgeCrossedBarycentricCoordinates(this->Yi.getCoordinates(baryi));
        }
      }

      cc->setMu((T)MU);

      if(this->backFace || this->forced){
        cc->setMu((T)MU*(T)0.0);
      }

      int indices[3];
      this->ctx->mesh->getTriangleIndices(this->candidateId,
                                          indices, this->backFace);

      T dt = this->ctx->getDT();

      /*Check if face id is from the body or the constraining geometry*/
      bool bodyVertex  = true;
      bool bodyFace    = true;
      bool rigidVertex = false;
      bool rigidFace   = false;

      if(Mesh::isStatic(this->ctx->mesh->vertices[this->setId].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(this->ctx->mesh->vertices[this->setId].type)){
        rigidVertex = true;
      }

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      T totalDistance =
        (this->Yi.getSignedDistance(this->X00.getVertex()) -
         this->Yi.getSignedDistance(this->Y00.v[0]) * weights[0] -
         this->Yi.getSignedDistance(this->Y00.v[1]) * weights[1] -
         this->Yi.getSignedDistance(this->Y00.v[2]) * weights[2]) -
        this->ctx->safety_distance - this->ctx->distance_epsilon;

      Vector4<T> t1, t2;
      Vector4<T> rv, rf;

      if(rigidVertex){
        Vector4<T> displVec = this->Xi.getVertex() - this->X0.getVertex();
        Vector4<T> arm = (this->X0.getVertex() - this->comXi).normalized();

        Vector4<T> perp = cross(displVec, arm).normalized();
        perp = cross(perp, arm).normalized();

        if(dot(perp, displVec) < 0){
          perp *= -1;
        }

        Triangle<T> tip = this->Yi; /*Displaced collision plane*/

        Vector4<T> offset = this->Yi.n * this->ctx->distance_epsilon;
        tip.v[0] += offset;
        tip.v[1] += offset;
        tip.v[2] += offset;

        /*Find intersection of displVec with ti*/
        Vector4<T> newxi;
        T tt;
        Vector4<T> bxi;

        tip.rayIntersection(this->X0.getVertex(), perp, newxi, &tt, &bxi);
      }

      if(HasFrictionCone<CC>()){
        if(tangentialDefined){
          t1 = -tangentReference[0];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1,  this->Yi.n).normalized();
          tt1 = cross(tt2, this->Yi.n).normalized();

          t1 = tt1;
          t2 = tt2;

          if(rigidVertex){
            rv  = this->Xi.getVertex() - this->comXi;
          }

          if(rigidFace){
            rf = this->Yi.getCoordinates(baryi) - this->comYi;
          }
          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = this->Yi.n;
        }else{
          /*No previous tangent vector defined*/
          Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;

          tt2 = cross(rnd, this->Yi.n).normalized();
          tt1 = cross(tt2, this->Yi.n).normalized();

          t1 = tt1;
          t2 = tt2;

          if(rigidVertex){
            rv  = this->Xi.getVertex() - this->comXi;
          }

          if(rigidFace){
            rf = this->Yi.getCoordinates(baryi) - this->comYi;
          }

          cc->tangent1 = t1;
          cc->tangent2 = t2;
          cc->normal   = this->Yi.n;
        }
      }else{
        /*No friction cone case*/
        if(tangentialDefined){
          t1 = -tangentReference[0];
          t2 = -tangentReference[1];

          /*Align updated tangential vectors with old tangential vectors*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, this->Yi.n).normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(t2, this->Yi.n).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }

          t1 = tt1;
          t2 = tt2;

          if(rigidVertex){
            rv  = this->Xi.getVertex() - this->comXi;
          }

          if(rigidFace){
            rf = this->Yi.getCoordinates(baryi) - this->comYi;
          }
        }else{
          Vector4<T> vel;
          Vector<T>* vx = this->ctx->getSolver()->getx();

          /*Compute current relative velocity*/
          if(bodyVertex){
            int vel_index = this->ctx->mesh->vertices[this->setId].vel_index;

            vel[0] = -(*vx)[vel_index+0];
            vel[1] = -(*vx)[vel_index+1];
            vel[2] = -(*vx)[vel_index+2];

            Vector4<T> ang_vel;

            if(rigidVertex){
              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              /*Compute angular displacement*/
              rv = this->Xi.getVertex() - this->comXi;
              ang_vel = cross(ang_vel, rv);

              vel -= ang_vel;
            }
          }

          if(bodyFace){
            if(rigidFace){
              int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;

              vel[0] += (*vx)[vel_index+0];
              vel[1] += (*vx)[vel_index+1];
              vel[2] += (*vx)[vel_index+2];

              Vector4<T> ang_vel;

              ang_vel[0] = (*vx)[vel_index+3];
              ang_vel[1] = (*vx)[vel_index+4];
              ang_vel[2] = (*vx)[vel_index+5];

              rf = this->Yi.getCoordinates(baryi) - this->comYi;
              ang_vel = cross(ang_vel, rf);

              vel += ang_vel;
            }else{
              for(int l=0;l<3;l++){
                int vel_index = this->ctx->mesh->vertices[indices[l]].vel_index;

                vel[0] += (*vx)[vel_index+0] * weights[l];
                vel[1] += (*vx)[vel_index+1] * weights[l];
                vel[2] += (*vx)[vel_index+2] * weights[l];
              }
            }
          }

          Vector4<T> vn = cross(vel, this->Yi.n);

          /*Use velocity to align tangent vectors*/
          if(vn.length() < 1e-6){
            /*Choose some arbitrary direction*/
            Vector4<T> vec(1, 1, 1, 0);
            Vector4<T> vn2 = cross(vec, this->Yi.n);

            if(vn2.length() < 1e-6){
              vec.set(-1, 1, 1, 0);
              vn2 = cross(vec, this->Yi.n);
            }

            t2 = vn2.normalized();
            t1 = cross(t2, this->Yi.n).normalized();
          }else{
            t2 = vn.normalized();
            t1 = cross(t2, this->Yi.n).normalized();
          }
        }
      }

      int cidx = 0;

      /*Compute tangential gap*/
      T totalDistanceT1 =
        -(dot(t1, this->Xi.getVertex() - this->X00.getVertex()) -
          dot(t1, this->Yi.v[0] - this->Y00.v[0]) * weights[0] -
          dot(t1, this->Yi.v[1] - this->Y00.v[1]) * weights[1] -
          dot(t1, this->Yi.v[2] - this->Y00.v[2]) * weights[2]);

      T totalDistanceT2 =
        -(dot(t2, this->Xi.getVertex() - this->X00.getVertex()) -
          dot(t2, this->Yi.v[0] - this->Y00.v[0]) * weights[0] -
          dot(t2, this->Yi.v[1] - this->Y00.v[1]) * weights[1] -
          dot(t2, this->Yi.v[2] - this->Y00.v[2]) * weights[2]);

      if(gapOverride){
        gap = *gapOverride;
        totalDistanceT1 = dot(gap, t1);
        totalDistanceT2 = dot(gap, t2);
      }else{
        gap = this->Yi.n * totalDistance +
          t1 * totalDistanceT1 + t2 * totalDistanceT2;
      }

      Vector4<T> cn = this->Yi.n;

      if(bodyVertex){
        int velIndex = this->ctx->mesh->vertices[this->setId].vel_index/3;

        if(rigidVertex){
          cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(-cross(rv, cn)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(-cross(rv, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(-cross(rv, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 0, velIndex);

          cidx = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(t1*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(t2*dt, totalDistanceT2, 2, cidx, velIndex);

          cc->setRow(cross(rf,cn)*dt, totalDistance,   0, cidx+1, velIndex+1);
          cc->setRow(cross(rf,t1)*dt, totalDistanceT1, 1, cidx+1, velIndex+1);
          cc->setRow(cross(rf,t2)*dt, totalDistanceT2, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*weights[0]*dt, totalDistance,   0, cidx, velIndex);
          cc->setRow(t1*weights[0]*dt, totalDistanceT1, 1, cidx, velIndex);
          cc->setRow(t2*weights[0]*dt, totalDistanceT2, 2, cidx, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(cn*weights[1]*dt, totalDistance,   0, cidx+1, velIndex);
          cc->setRow(t1*weights[1]*dt, totalDistanceT1, 1, cidx+1, velIndex);
          cc->setRow(t2*weights[1]*dt, totalDistanceT2, 2, cidx+1, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(cn*weights[2]*dt, totalDistance,   0, cidx+2, velIndex);
          cc->setRow(t1*weights[2]*dt, totalDistanceT1, 1, cidx+2, velIndex);
          cc->setRow(t2*weights[2]*dt, totalDistanceT2, 2, cidx+2, velIndex);
        }
      }

      /*Set transposed values*/
      cc->init(this->ctx->mat);

      cc->status = Active;

      this->enabledId = this->ctx->getSolver()->addConstraint(cc);

      cc->id = this->enabledId;

      if(this->enabledId < 0){
        error("Negative ID");
      }

      if(ActiveConstraints<CC>()){
        cc->resetMultipliers(*this->ctx->getSolver()->getx(),
                             this->cachedMultipliers);
      }else{
        Vector4<T> zero;
        cc->resetMultipliers(*this->ctx->getSolver()->getx(), zero);
      }

      this->status = Enabled;
    }
    return true;
  }

  /**************************************************************************/
  /*Recompute geometric properties of constraint*/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::
  recompute(AbstractConstraintSet<T, OrientedVertex<T>, Triangle<T>, CC>* c,
            OrientedVertex<T>& p,
            Vector4<T>& com,
            Quaternion<T>& rot){
    bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

    if(vertexCollapsed){
      message("vertex %d collapsed", this->setId);
      std::cout << this->X0 << std::endl;
      std::cout << p << std::endl;
    }

    try{
      this->ctx->mesh->getTriangle(&this->Y0, this->candidateId,
                                   this->backFace);
      this->valid = true;
    }catch(Exception* e){
      warning("Singularity %s", e->getError().c_str());
      message("face id = %d", this->candidateId);
      std::cout << this->Y0 << std::endl;
      error("Singular face");
      getchar();
      delete e;
      this->valid = false;

      if(this->status == Enabled){
        disableConstraint();
      }
      return;
    }
    this->valid = true;

    this->Yi  = this->Y0;
    this->Yu  = this->Y0;
    this->Y00 = this->Y0;

    this->comY00 = this->comY0 = this->comYu = this->comYi = Vector4<T>();
    this->rotY00 = this->rotY0 = this->rotYu = this->rotYi = Quaternion<T>();

    this->ctx->mesh->setCurrentEdge(this->ctx->mesh->
                                    halfFaces[this->candidateId].half_edge);

    int vertex = this->ctx->mesh->getOriginVertex();

    if(Mesh::isRigid(this->ctx->mesh->vertices[vertex].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[vertex];

      this->comY00 = this->comY0 = this->comYu = this->comYi =
        this->ctx->mesh->centerOfMass[objectId];

      this->rotY00 = this->rotY0 = this->rotYu = this->rotYi =
        this->ctx->mesh->orientations[objectId];
    }

    this->X0  = p;
    this->Xi  = p;
    this->X00 = p;

    this->comX00 = this->comX0 = this->comXi = com;
    this->rotX00 = this->rotX0 = this->rotXi = rot;

    baryi = bary2 = this->Y0.getBarycentricCoordinates(p.getVertex());

    this->intersection0 = this->intersectionc =
      this->X0.planeIntersection(this->Y0.v[0],
                                 this->Y0.n,
                                 indices,
                                 rigidFace || rigidVertex,
                                 false);


    this->distance00 = this->distance0 = this->distancec =
      this->Y0.getPlaneDistance(p.getVertex()) -
      (T)(this->ctx->safety_distance*0.0);

    if(vertexCollapsed){
      error("vertex is in a collapsed state, i.e., the cone has self intersections");
    }

    updated = false;

    this->forced = false;

    if(this->status == Enabled){
      /*Check if p projects on the triangle, if not, disable the
        constraint*/

      if(!this->forced &&
         !this->Y0.barycentricInTriangleDist(baryi, this->ctx->safety_distance)
         /*&& this->distancec > this->ctx->safety_distance/(T)2.0*/){
        disableConstraint();
        return;
      }

      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(cc->status == Inactive){
        if(this->distancec > (this->ctx->distance_epsilon +
                              this->ctx->distance_tol)){
          /*Constraint is inactive and there is no collision -> disable.*/

          disableConstraint();
          return;
        }
      }

      Vector4<T> weights = baryi;

      if(ClampsWeights<CC>()){
        if(this->forced){
          weights = this->Y0.edgeCrossedBarycentricCoordinates(this->Y0.getCoordinates(baryi));
        }else{
          weights = this->Y0.getBarycentricCoordinates(this->Y0.getCoordinates(baryi));
        }
      }else{
        weights = baryi;
      }

      cc->setMu((T)MU);

      if(this->backFace || this->forced){
        cc->setMu((T)MU*(T)0.0);
      }

      if(HasFrictionCone<CC>()){
        cc->cumulativeVector = cc->kineticVector * (T)60;

        if(cc->cumulativeVector.length() > 1e-6){
          cc->cumulativeVector.normalize();
        }

        if(cc->frictionStatus[0] != Kinetic){
          cc->cumulativeVector.set(0,0,0,0);
          cc->kineticVector.set(0,0,0,0);
          cc->acceptedKineticVector.set(0,0,0,0);
        }
      }

      int indices[3];

      this->ctx->mesh->getTriangleIndices(this->candidateId, indices,
                                          this->backFace);

      T dt = this->ctx->getDT();

      bool bodyFace    = true;
      bool bodyVertex  = true;
      bool rigidFace   = false;
      bool rigidVertex = false;

      for(int i=0;i<3;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyFace = false;
        }
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
      }

      if(Mesh::isStatic(this->ctx->mesh->vertices[c->getSetId()].type)){
        bodyVertex = false;
      }
      if(Mesh::isRigid(this->ctx->mesh->vertices[c->getSetId()].type)){
        rigidVertex = true;
      }

      T totalDistance =
        (this->Y0.getSignedDistance(p.getVertex()) -
         this->Y0.getSignedDistance(this->Y0.v[0]) * weights[0] -
         this->Y0.getSignedDistance(this->Y0.v[1]) * weights[1] -
         this->Y0.getSignedDistance(this->Y0.v[2]) * weights[2]) +
        -this->ctx->safety_distance - this->ctx->distance_epsilon;

      /*Compute tangent vectors*/
      Vector4<T> t1, t2;
      Vector4<T> rv, rf;

      if(HasFrictionCone<CC>()){
        int cindex = 0;
        if(bodyVertex){
          if(rigidVertex){
            t1 += cc->getRow(1, 0);
            t2 += cc->getRow(2, 0);
            cindex = 2;

            rv  = this->Xi.getVertex() - this->comXi;
          }else{
            t1 += cc->getRow(1, 0);
            t2 += cc->getRow(2, 0);
            cindex = 1;
          }
        }

        if(bodyFace){
          if(rigidFace){
            t1 -= cc->getRow(1, cindex + 0);
            t2 -= cc->getRow(2, cindex + 0);

            Vector4<T> txi = this->Yi.getCoordinates(baryi);
            rf = txi - this->comYi;
          }else{
            t1 -= cc->getRow(1, cindex + 0);
            t1 -= cc->getRow(1, cindex + 1);
            t1 -= cc->getRow(1, cindex + 2);

            t2 -= cc->getRow(2, cindex + 0);
            t2 -= cc->getRow(2, cindex + 1);
            t2 -= cc->getRow(2, cindex + 2);
          }
        }

        t1 *= -1;
        t2 *= -1;

        Vector4<T> tt1, tt2;
        tt2 = cross(t1,  this->Yi.n).normalized();
        tt1 = cross(tt2, this->Yi.n).normalized();

        t1 = -tt1;
        t2 = -tt2;

        cc->tangent1 = t1;
        cc->tangent2 = t2;
        cc->normal   = this->Yi.n;
      }else{
        /*Compute current relative velocity*/
        Vector4<T> vel;
        Vector<T>* vx = this->ctx->getSolver()->getx();

        if(bodyVertex){
          int vel_index = this->ctx->mesh->vertices[c->getSetId()].vel_index;

          vel[0] = -(*vx)[vel_index+0];
          vel[1] = -(*vx)[vel_index+1];
          vel[2] = -(*vx)[vel_index+2];

          Vector4<T> ang_vel;

          if(rigidVertex){
            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            rv  = this->Xi.getVertex() - this->comXi;
            ang_vel = cross(ang_vel, rv);

            vel -= ang_vel;
          }
        }

        if(bodyFace){
          if(rigidFace){
            int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;
            vel[0] += (*vx)[vel_index+0];
            vel[1] += (*vx)[vel_index+1];
            vel[2] += (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            Vector4<T> txi = this->Yi.getCoordinates(baryi);
            rf = txi - this->comYi;

            ang_vel = cross(ang_vel, rf);

            vel += ang_vel;
          }else{
            for(int l=0;l<3;l++){
              int vel_index = this->ctx->mesh->vertices[indices[l]].vel_index;

              vel[0] += (*vx)[vel_index+0]*weights[l];
              vel[1] += (*vx)[vel_index+1]*weights[l];
              vel[2] += (*vx)[vel_index+2]*weights[l];
            }
          }
        }

        t1 = t2.set(0,0,0,0);

        int cindex = 0;
        if(bodyVertex){
          if(rigidVertex){
            t1 += cc->getRow(1, 0);
            t2 += cc->getRow(2, 0);
            cindex = 2;
          }else{
            t1 += cc->getRow(1, 0);
            t2 += cc->getRow(2, 0);
            cindex = 1;
          }
        }

        if(bodyFace){
          if(rigidFace){
            t1 -= cc->getRow(1, cindex + 0);
            t2 -= cc->getRow(2, cindex + 0);
          }else{
            t1 -= cc->getRow(1, cindex + 0);
            t1 -= cc->getRow(1, cindex + 1);
            t1 -= cc->getRow(1, cindex + 2);

            t2 -= cc->getRow(2, cindex + 0);
            t2 -= cc->getRow(2, cindex + 1);
            t2 -= cc->getRow(2, cindex + 2);
          }
        }
        t1 *= -1;
        t2 *= -1;

        t1.normalize();
        t2.normalize();

        Vector4<T> vn = cross(vel, this->Yi.n);

        /*Re-align with velocity and/or normal*/
        if(vn.length() < 1e-6){
          /*Fixed point, only align with normal*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, this->Yi.n).normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(t2, this->Yi.n).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }

          t1 = tt1;
          t2 = tt2;
        }else{
          Vector4<T> tt1, tt2;
          tt2 = vn.normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
          }

          tt1 = cross(tt2, this->Yi.n).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
          }
          t1 = tt1;
          t2 = tt2;
        }
      }

      gap = this->Yi.n * totalDistance;

      int cidx = 0;

      Vector4<T> cn = this->Yi.n;

      /*Setup existing constraint*/
      if(bodyVertex){
        int velIndex = this->ctx->mesh->vertices[c->getSetId()].vel_index/3;

        if(rigidVertex){
          cc->setRow(-cn*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*dt,             0, 2, 0, velIndex);

          cc->setRow(-cross(rv, cn)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(-cross(rv, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(-cross(rv, t2)*dt,             0, 2, 1, velIndex+1);

          cidx = 2;
        }else{
          cc->setRow(-cn*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(-t1*dt,             0, 1, 0, velIndex);
          cc->setRow(-t2*dt,             0, 2, 0, velIndex);

          cidx = 1;
        }
      }

      if(bodyFace){
        if(rigidFace){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*dt, totalDistance, 0, cidx, velIndex);
          cc->setRow(t1*dt,             0, 1, cidx, velIndex);
          cc->setRow(t2*dt,             0, 2, cidx, velIndex);

          cc->setRow(cross(rf, cn)*dt, totalDistance, 0, cidx+1, velIndex+1);
          cc->setRow(cross(rf, t1)*dt,             0, 1, cidx+1, velIndex+1);
          cc->setRow(cross(rf, t2)*dt,             0, 2, cidx+1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*weights[0]*dt, totalDistance, 0, cidx, velIndex);
          cc->setRow(t1*weights[0]*dt,             0, 1, cidx, velIndex);
          cc->setRow(t2*weights[0]*dt,             0, 2, cidx, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(cn*weights[1]*dt, totalDistance, 0, cidx+1, velIndex);
          cc->setRow(t1*weights[1]*dt,             0, 1, cidx+1, velIndex);
          cc->setRow(t2*weights[1]*dt,             0, 2, cidx+1, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(cn*weights[2]*dt, totalDistance, 0, cidx+2, velIndex);
          cc->setRow(t1*weights[2]*dt,             0, 1, cidx+2, velIndex);
          cc->setRow(t2*weights[2]*dt,             0, 2, cidx+2, velIndex);
        }
      }

      /*Set transposed values*/
      cc->init(this->ctx->getMatrix());

      cc->update(*this->ctx->getSolver()->getb(),
                 *this->ctx->getSolver()->getx(),
                 *this->ctx->getSolver()->getb2());
    }else{
      //warning("No recompute of initial distance?");
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::
  updateInitialState(const OrientedVertex<T>& v,
                     const Vector4<T>& com,
                     const Quaternion<T>& rot,
                     bool _notUsed){
    bool rigidFace   = Mesh::isRigidOrStatic(this->candidateType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->setType);

    this->X0 = v;
    this->comX0 = com;
    this->rotX0 = rot;

    this->Y0 = this->Yu;
    this->comY0 = this->comYu;
    this->rotY0 = this->rotYu;

    this->intersection0 =
      this->X0.planeIntersection(this->Y0.v[0], this->Y0.n,
                                 indices, rigidFace || rigidVertex, false);

    this->distance0 =
      this->Y0.getPlaneDistance(this->X0.getVertex()) -
      (T)(this->ctx->safety_distance*0.0);
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateFace<T, CC>::
  showCandidateState(const OrientedVertex<T>& xu){
    message("\n\nShow triangle candidate state %d - %d",
            this->candidateId, this->setId);

    message("x0 t0");
    std::cout << this->X0 << std::endl;
    std::cout << this->Y0 << std::endl;

    message("xu tu");
    std::cout << xu << std::endl;
    std::cout << this->Yu << std::endl;

    message("xi ti");
    std::cout << this->Xi << std::endl;
    std::cout << this->Yi << std::endl;

    message("distance0 = %10.10e", this->distance0);
    message("distancec = %10.10e", this->distancec);

    message("intersection0 = %d", this->intersection0);
    message("intersectionc = %d", this->intersectionc);

    message("forced = %d", this->forced);

    message("indiced      = %d, %d, %d", indices[0], indices[1], indices[2]);
    message("edge indiced = %d, %d, %d",
            edgeIndices[0], edgeIndices[1], edgeIndices[2]);

    message("enabledId = %d", this->enabledId);

    if(this->status == Enabled){
      message("Constraint enabled");
      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      cc->print(std::cout);
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateFace<T, CC>::disableConstraint(){
    this->forced = false;
    if(this->status == Enabled){
      /*Check if we can disable this constraint, i.e., its
        corresponding multiplier must then also be negative*/
      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(ActiveConstraints<CC>()){
        cc->cacheMultipliers(this->cachedMultipliers,
                             *this->ctx->getSolver()->getx());
      }

      this->status = Disabled;

      /*Reset tangent reference*/
      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      /*Figure out constraint setup*/
      bool rigidFace   = false;
      bool rigidVertex = false;
      bool bodyFace    = false;
      bool bodyVertex  = false;

      for(int i=0;i<3;i++){
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidFace = true;
        }
        if(Mesh::isNotStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyFace = true;
        }
      }

      if(Mesh::isRigid(this->ctx->mesh->vertices[this->setId].type)){
        rigidVertex = true;
      }

      if(Mesh::isNotStatic(this->ctx->mesh->vertices[this->setId].type)){
        bodyVertex = true;
      }

      /*Save tangentvectors*/
      int cindex = 0;
      if(bodyVertex){
        if(rigidFace){
          tangentReference[0] += cc->getRow(1, 0);
          tangentReference[1] += cc->getRow(2, 0);
          cindex = 2;
        }else{
          tangentReference[0] += cc->getRow(1, 0);
          tangentReference[1] += cc->getRow(2, 0);
          cindex = 1;
        }
      }

      if(bodyFace){
        if(rigidVertex){
          tangentReference[0] -= cc->getRow(1, cindex + 0);
          tangentReference[1] -= cc->getRow(2, cindex + 0);
        }else{
          tangentReference[0] -= cc->getRow(1, cindex + 0);
          tangentReference[0] -= cc->getRow(1, cindex + 1);
          tangentReference[0] -= cc->getRow(1, cindex + 2);

          tangentReference[1] -= cc->getRow(2, cindex + 0);
          tangentReference[1] -= cc->getRow(2, cindex + 1);
          tangentReference[1] -= cc->getRow(2, cindex + 2);
        }
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      this->ctx->getSolver()->removeConstraint(this->enabledId);
      delete cc;

      return true;
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }

  /**************************************************************************/
  template class ConstraintCandidateFace<float, ContactConstraintCR<2, float> >;
  template class ConstraintCandidateFace<double, ContactConstraintCR<2, double> >;
}
