/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintSetFace.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetFaces<T, CC>::ConstraintSetFaces(){
  }

  /**************************************************************************/
  template<class T, class CC>
  ConstraintSetFaces<T, CC>::~ConstraintSetFaces(){
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintSetFaces<T, CC>::update(){
    candidates.clear();

    /*Performs a raw-collision check for the face associated with this
      set and the rest of the scene stored in stree*/
    /*Two faces belonging to the same rigid object are ignored, also
      two faces from static objects are neglected.*/
    this->ctx->stree->findPotentialFaceCollisions(faceId, candidates);
  }

  /**************************************************************************/
  template class ConstraintSetFaces<float, ContactConstraintCR<2, float> >;
  template class ConstraintSetFaces<double, ContactConstraintCR<2, double> >;
}
