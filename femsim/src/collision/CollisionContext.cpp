/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/BBox.hpp"
#include "collision/ConstraintSetFace.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"

#include "collision/ConstraintSet.hpp"
#include "collision/ConstraintSetEdge.hpp"
#include "collision/ConstraintSetTriangleEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"

//#define BBEPS 5e-3
namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::initializeBBoxes(){
    int nEdges = mesh->getNHalfEdges();
    bboxEdge = new Vector4<T>[nEdges * 2];

    int nVertices = mesh->getNVertices();
    bboxVertex = new Vector4<T>[nVertices * 2];

    /*Extended bboxes of faces are stored in stree*/
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::deleteBBoxes(){
    delete[] bboxEdge;
    delete[] bboxVertex;
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::updateBBoxes(bool keepCurrentBoxes){
    /*Compute boxes for each vertex*/

    for(int i=0;i<mesh->getNVertices();i++){
      if(mesh->vertices[i].leaving == Mesh::UndefinedIndex){
        /*Internal vertex, is not a part of the outher boundary, skip*/
        continue;
      }

      BBox<T> box(mesh->vertices[i].coord,
                  mesh->vertices[i].coord);

      if(keepCurrentBoxes){
        box.addPoint(bboxVertex[i*2+0]);
        box.addPoint(bboxVertex[i*2+1]);
      }

      box.addPoint(mesh->vertices[i].displCoordExt);
      box.addEpsilon((T)BBOXEPS);

      bboxVertex[i*2+0] = box.getMin();
      bboxVertex[i*2+1] = box.getMax();
    }

    /*Compute boxes for each edge*/
    for(int i=0;i<mesh->getNHalfEdges();i++){
      int vertices[2];

      mesh->getEdgeIndices(i, vertices);

      BBox<T> box(mesh->vertices[vertices[0]].coord,
                  mesh->vertices[vertices[0]].coord);

      box.addPoint(mesh->vertices[vertices[1]].coord);

      if(keepCurrentBoxes){
        box.addPoint(bboxEdge[i*2+0]);
        box.addPoint(bboxEdge[i*2+1]);
      }

      box.addPoint(mesh->vertices[vertices[0]].displCoordExt);
      box.addPoint(mesh->vertices[vertices[1]].displCoordExt);

      box.addEpsilon((T)BBOXEPS);

      bboxEdge[i*2+0] = box.getMin();
      bboxEdge[i*2+1] = box.getMax();
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::checkBBoxesVertex(List<int>* invalidVertices){
    int n_vertices = mesh->getNVertices();

    for(int i=0;i<n_vertices;i++){
      if(mesh->vertices[i].leaving == Mesh::UndefinedIndex){
        /*Internal vertex, is not a part of the outher boundary, skip*/
        continue;
      }

      BBox<T> box(bboxVertex[i*2+0], bboxVertex[i*2+1]);

      if(box.inside(mesh->vertices[i].displCoord)){
        /*vertex still in original box, ok*/
      }else{
        /*vertex has moved out of box, possible collisions could be missed*/
        invalidVertices->append(i);
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::checkBBoxesEdge(List<int>* invalidEdges){
    int n_edges = mesh->getNHalfEdges();

    for(int i=0;i<n_edges;i++){
      mesh->setCurrentEdge(i);

      int twin = mesh->getTwinEdge();

      if(i < twin){
        int vertices[2];
        mesh->getEdgeIndices(i, vertices);

        BBox<T> box(bboxEdge[i*2+0], bboxEdge[i*2+1]);

        if(!box.inside(mesh->vertices[vertices[0]].displCoord)){
          /*edge moved out of box, possible collisions could be missed*/
          invalidEdges->append(i);
        }else if(!box.inside(mesh->vertices[vertices[1]].displCoord)){
          /*edge moved out of box, possible collisions could be missed*/
          invalidEdges->append(i);
        }else{
          /*edge still in original box, ok*/
        }
      }
    }
  }

  /**************************************************************************/
  /*Finds all vertex-face potential collisions given the already found
    face-face potentials.*/
  template<class T, class CC>
  void CollisionContext<T, CC>::extractPotentialVertexFace(int vertex,
                                                           List<int>& faces,
                                                           bool internal){
    /*Get all connected faces of this vertex*/
    List<int> connectedFaces;
    List<int> foundFaces;

    mesh->getConnectedFacesOfVertex(vertex, connectedFaces);

    /*Find all proximate faces of the faces connected to this vertex*/
    List<int>::Iterator it = connectedFaces.begin();
    while(it != connectedFaces.end()){
      int currentFace = *it++;

      List<int>::Iterator it2 = faceFaceSets[currentFace].candidates.begin();

      while(it2 != faceFaceSets[currentFace].candidates.end()){
        int proximateFace = *it2++;

        if(internal){
          /*When searching for internal collisions, the neighboring
            face of the requested edge must have the same root as the
            found faces.*/

          if(stree->facesShareSameRoot(currentFace,
                                       proximateFace) == false){
            /*Faces belong to different objects, hence there can't be
              an internal collision.*/
            continue;
          }
        }

        /*If the proximate face contains the queried vertex,
          discard, since they can never intersect*/
        int faceVertices[3];
        mesh->getTriangleIndices(proximateFace, faceVertices, false);

        bool connectedFace = false;
        for(int i=0;i<3;i++){
          if(faceVertices[i] == vertex){
            connectedFace = true;
          }
        }

        if(mesh->halfFaces[proximateFace].visited == false){
          if(!connectedFace){
            foundFaces.append(proximateFace);
            //foundFaces.insert(proximateFace, proximateFace);
            mesh->halfFaces[proximateFace].visited = true;
          }
        }
      }
    }

    //foundFaces.removeDuplicates();

    BBox<T> box(bboxVertex[vertex*2+0], bboxVertex[vertex*2+1]);

    List<int>::Iterator it2 = foundFaces.begin();

    while(it2 != foundFaces.end()){
      int currentFace = *it2++;

      mesh->halfFaces[currentFace].visited = false;

      if(box.intersection(stree->leafNodes[currentFace].box)){
        //faces.insert(currentFace, currentFace);
        faces.append(currentFace);
      }
    }
  }

  /**************************************************************************/
  /*Finds all edge-edge potential collisions given the already found
    face-face potentials.*/
  template<class T, class CC>
  void CollisionContext<T, CC>::extractPotentialEdgeEdges(int edge,
                                                          List<int>& edges,
                                                          bool internal){
    List<int> foundEdges;
    //Tree<int> foundEdges;

    int vertices[2];
    int faces[2];

    /*Obtain edge indices*/
    mesh->getEdgeIndices(edge, vertices);

    /*Obtain face indices of edge*/
    mesh->setCurrentEdge(edge);
    faces[0] = mesh->getFace();
    faces[1] = mesh->getTwinFace();

    Tree<int> foundFaces;
    for(int i=0;i<2;i++){
      int currentNeighboringFace = faces[i];

      List<int>::Iterator it2 =
        faceFaceSets[currentNeighboringFace].candidates.begin();

      while(it2 != faceFaceSets[currentNeighboringFace].candidates.end()){
        int currentFace = *it2++;
        //if(currentFace == lastFace){
        //  /*Duplicate*/
        //  continue;
        //}

        //lastFace = currentFace;

        if(internal){
          /*When searching for internal collisions, the neighboring
            face of the requested edge must have the same root as the
            found faces.*/

          if(stree->facesShareSameRoot(currentNeighboringFace,
                                       currentFace) == false){
            /*Faces belong to different objects, hence there can't be
              an internal collision.*/
            continue;
          }
        }

        mesh->setCurrentEdge(mesh->halfFaces[currentFace].half_edge);
        for(int j=0;j<3;j++){
          int cedge = (int)mesh->getCurrentEdge();
          int tedge = (int)mesh->getTwinEdge();

          int selectedEdge = cedge<tedge?cedge:tedge;

          if(mesh->halfEdges[selectedEdge].visited == false){
            if(edge < selectedEdge){
              int edgeVertices[2];
              mesh->getEdgeIndices(selectedEdge, edgeVertices);

              /*Check if the current edge and proximate edge do not
                have a vertex in common. If so, they are connected and
                should be discarded. Two connected edges do have a
                zero distance and may produce false positives in the
                fine collision check.*/
              bool connected = false;
              for(int k=0;k<2;k++){
                for(int l=0;l<2;l++){
                  if(vertices[k] == edgeVertices[l]){
                    connected = true;
                  }
                }
              }
              if(!connected){
                /*Edge is not connected with this edge.*/
                mesh->halfEdges[selectedEdge].visited = true;
                foundEdges.append(selectedEdge);
                //foundEdges.insert(selectedEdge, selectedEdge);
              }
            }
          }
          mesh->nextEdge();
        }
      }
    }

    /*Remove duplicate edges*/
    //foundEdges.removeDuplicates();

    /*Check for box-box intersections of the edge boxes*/

    BBox<T> box(bboxEdge[edge*2+0], bboxEdge[edge*2+1]);
    List<int>::Iterator it = foundEdges.begin();
    while(it != foundEdges.end()){
      int currentEdge = *it++;
      mesh->halfEdges[currentEdge].visited = false;

      BBox<T> edgeBox(bboxEdge[currentEdge*2+0], bboxEdge[currentEdge*2+1]);

      if(box.intersection(edgeBox)){
        //edges.insert(currentEdge, currentEdge);
        edges.append(currentEdge);
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::
  extractPotentialEdgeVertices(int edge,
                               Tree<int>& vertices){
    int faceVertices[2];

    /*Obtain face indices of edge*/
    mesh->setCurrentEdge(edge);
    mesh->nextEdge();
    mesh->nextEdge();
    faceVertices[0] = mesh->getOriginVertex();
    mesh->setCurrentEdge(edge);
    mesh->twinEdge();
    mesh->nextEdge();
    mesh->nextEdge();
    faceVertices[1] = mesh->getOriginVertex();

    for(int i=0;i<2;i++){
      int currentVertex = faceVertices[i];

      vertices.insert(currentVertex, currentVertex);
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::showStateVertexFace(int vertex,
                                                    int face,
                                                    bool backFace){
    message("this pointer = %p", this);
    message("mesh pointer = %p", mesh);
    message("showStateVertexFace %d, %d, %d", vertex, face, backFace);
    message("nVertices = %d", mesh->getNVertices());
    message("nFaces    = %d", mesh->getNHalfFaces());

    if(vertex >= mesh->getNVertices()){
      return;
    }

    if(face >= mesh->getNHalfFaces()){
      return;
    }

    if(backFace){
      backSets[vertex].showFaceCandidateState(face);
    }else{
      sets[vertex].showFaceCandidateState(face);
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::showStateEdgeEdge(int edgeA, int edgeB,
                                                  bool backFace){
    message("showStateEdgeEdge %d, %d, %d", edgeA, edgeB, backFace);
    message("nEdges = %d", mesh->getNHalfEdges());
    message("this pointer = %p", this);

    if(edgeA >= mesh->getNHalfEdges()){
      return;
    }

    if(edgeB >= mesh->getNHalfEdges()){
      return;
    }

    if(backFace){
      backEdgeSets[edgeA].showEdgeCandidateState(edgeB);
    }else{
      edgeSets[edgeA].showEdgeCandidateState(edgeB);
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::showStateEdgeVertex(int edge, int vertex){
    if(edge >= mesh->getNHalfEdges()){
      return;
    }

    triangleEdgeSets[edge].showVertexCandidateState(vertex);
  }

  /**************************************************************************/
  template<class T, class CC>
  void CollisionContext<T, CC>::updateStartEdge(int indexA,
                                                int indexB,
                                                const Edge<T>& edgeA,
                                                const Vector4<T>& comA,
                                                const Quaternion<T>& rotA,
                                                const Edge<T>& edgeB,
                                                const Vector4<T>& comB,
                                                const Quaternion<T>& rotB,
                                                bool backEdge){
    int n_edges = mesh->getNHalfEdges();

    /*Update side of edge-edge pairs associated to edgeSets*/
    if(backEdge){
      backEdgeSets[indexA].updateStartPositions(edgeA, comA, rotA, 0, true);
      backEdgeSets[indexB].updateStartPositions(edgeB, comB, rotB, 0, true);
    }else{
      edgeSets[indexA].updateStartPositions(edgeA, comA, rotA, 0, true);
      edgeSets[indexB].updateStartPositions(edgeB, comB, rotB, 0, true);
    }

    /*Update other side of edge-edge pairs*/
    for(int i=0;i<n_edges;i++){
      if(backEdge){
        backEdgeSets[i].updateCandidateStartPosition(indexA, edgeA, comA, rotA);
        backEdgeSets[i].updateCandidateStartPosition(indexB, edgeB, comB, rotB);
      }else{
        edgeSets[i].updateCandidateStartPosition(indexA, edgeA, comA, rotA);
        edgeSets[i].updateCandidateStartPosition(indexB, edgeB, comB, rotB);
      }
    }
  }

  template class CollisionContext<float, ContactConstraintCR<2, float> >;
  template class CollisionContext<double, ContactConstraintCR<2, double> >;
}
