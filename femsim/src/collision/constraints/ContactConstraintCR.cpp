/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/constraints/ContactConstraintCR.hpp"
#include "collision/constraints/ContactBlockConstraint.hpp"
#include "datastructures/AdjacencyList.hpp"

namespace tsl{

  extern double scf;

#define RERROR (1e-5)
#define HV_EPS  1e-6*1

  /**************************************************************************/
  extern double frictionNorm;

  /**************************************************************************/
  template<int N, class T>
  ContactConstraintCR<N, T>::ContactConstraintCR(){
    this->blockConstraint = 0;
  }

  /**************************************************************************/
  template<int N, class T>
  T& ContactConstraintCR<N, T>::getConstraintValue(int i){
    if(i == 0){
      return this->c[i];
    }else{
      if(this->frictionStatus[i-1] == Kinetic){
        this->zero = (T)0.0;
        return this->zero;
      }
    }
    return this->c[i];
  }

  /**************************************************************************/
  template<int N, class T>
  const T& ContactConstraintCR<N, T>::getConstraintValue(int i)const{
    if(i == 0){
      return this->c[i];
    }else{
      if(this->frictionStatus[i-1] == Kinetic){
        this->zero = 0;
        return this->zero;
      }
    }
    return this->c[i];
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::
  evaluateAndUpdateFriction(Vector4<T>& lm,
                            Vector4<T>& lr,
                            VectorC<T>& x,
                            VectorC<T>& b,
                            bool* changed,
                            bool* frictionChanged,
                            EvalStats& stats){
    int height = x.getSize();
    int row = this->row_id;

    tslassert(this->frictionStatus[0] == this->frictionStatus[1]);

    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    Vector4<T> rVec(0, lr[1], lr[2], 0);

    if(rVec.length() > 1e-14){
      rVec.normalize();
    }else{
      rVec.set(0,1,1,0);
      rVec.normalize();
    }

    Vector4<T> hvVec(0, lr[1], lr[2], 0);
    Vector4<T>  mVec(0, lm[1], lm[2], 0);

    /*
      mVec is ONLY defined when the constraint is in static friction
      mode. In case of kinetic friction, the multipliers are stored in
      acceptedMultipliers.
     */

    if(mVec.length() > 1e-14){
      mVec.normalize();
    }else{
      mVec = hvVec;
      if(mVec.length() > 1e-14){
        mVec.normalize();
      }
    }

    /*Tangential mulitipliers too large and relative motion positive*/
    if( (Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >= Pos(this->mu * lm[0]) &&
         dot(hvVec, mVec) > (T)0.0)){
      /*Static friction*/
      if(this->frictionStatus[0] == Static){
        /*Switch to kinetic?*/

        /*Compute difference of multiplier magnitudes*/
        T difference = Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) -
                           Pos(this->mu * lm[0]));

        /*Convert multiplier difference to constraint error distance*/
        T relativeError = Max(difference / (this->usedScale * bc->S[1][1]),
                              difference / (this->usedScale * bc->S[2][2]));

        if(relativeError > (T)RERROR){
          /*Constraint error too large, switch to kinetic friction*/
          kineticVector = mVec;

          /*Reset average friction direction and magnitude*/
          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();

          if(kineticVector.length() > 1e-14){
            /*Use current kinetic friction direction for init*/
            kineticVector.normalize();
            averageFrictionDirection.addSample(kineticVector);
          }
          else{
            if(hvVec.length() > 1e-14){
              /*Use sliding direction for init*/
              hvVec.normalize();
              averageFrictionDirection.addSample(hvVec);
              kineticVector.normalize();
            }
          }
          /*Use current multiplier values for init*/
          averageFrictionMagnitude.addSample(Sqrt(Sqr(lm[1]) + Sqr(lm[2])));

          (message("\t[%d] friction update switch to kinetic, value = %10.10e, %10.10e, %10.10e, %10.10e",
                   this->row_id,
                   Sqrt(Sqr(lm[1]) + Sqr(lm[2])),
                   Pos(this->mu * lm[0]),
                   dot(hvVec, mVec),
                   relativeError
                   ));
          DBG(this->printIndices(std::cout));

          START_DEBUG;
          message("mVec, hvVec, kineticVector");
          std::cout << mVec << std::endl;
          std::cout << hvVec << std::endl;
          std::cout << kineticVector << std::endl;
          END_DEBUG;

          /*Update state*/
          this->frictionStatus[0] = Kinetic;
          this->frictionStatus[1] = Kinetic;

          /*Set friction vectors*/
          cumulativeVector      = kineticVector;
          acceptedKineticVector = kineticVector;

          /*Set kinetic multipliers from static ones*/
          this->setMultipliers[1] = lm[1];
          this->setMultipliers[2] = lm[2];

          this->acceptedMultipliers[1] = this->setMultipliers[1];
          this->acceptedMultipliers[2] = this->setMultipliers[2];

          DBG(message("accepted multipliers = %10.10e, %10.10e",
                      this->acceptedMultipliers[1],
                      this->acceptedMultipliers[2]));

          /*Notify solver*/
          *changed = true;
          stats.n_friction_check++;

          /*Reset x and b, i.e., constraint rhs and multipliers*/
          x[height + row * this->offset + 1] = 0;
          x[height + row * this->offset + 2] = 0;

          b[height + row * this->offset + 1] = 0;
          b[height + row * this->offset + 2] = 0;
        }
      }
    }else{
      /*Kinetic friction*/
      Vector4<T> acmVec(0,
                        this->acceptedMultipliers[1],
                        this->acceptedMultipliers[2], 0);

      if(this->frictionStatus[0] == Kinetic &&
         dot(hvVec, acmVec) < (T)0.0){
        /*Switch to static friction: sliding direction and accepted
          multipliers have negative projection*/

        (message("\t[%d] friction update switch to static, value = %10.10e, %10.10e, %10.10e",
                 this->row_id,
                 kineticVector[1] * lr[1] + kineticVector[2] * lr[2],
                 Sqrt(Sqr(lm[1]) + Sqr(lm[2])), this->mu * lm[0]
                 )  );

        DBG(this->printIndices(std::cout));

        START_DEBUG;
        message("amVec, hvVec");
        std::cout << acmVec << std::endl;
        std::cout << hvVec << std::endl;
        END_DEBUG;

        /*Update state*/
        this->frictionStatus[0] = Static;
        this->frictionStatus[1] = Static;

        /*Notify solver*/
        *changed = true;
        stats.n_friction_check++;

        /*Set x and b, i.e., set multipliers and constraint rhs*/
        x[height + row * this->offset + 1] = (T)this->acceptedMultipliers[1];
        x[height + row * this->offset + 2] = (T)this->acceptedMultipliers[2];

        b[height + row * this->offset + 1] = this->c[1];
        b[height + row * this->offset + 2] = this->c[2];

        lm[1] = x[height + row * this->offset + 1];
        lm[2] = x[height + row * this->offset + 2];

        *frictionChanged = true;

        /*Reset kinetic friction state*/
        kineticVector.set(0,0,0,0);

        this->setMultipliers[1] = (T)0.0;
        this->setMultipliers[2] = (T)0.0;

        this->acceptedMultipliers[1] = this->setMultipliers[1];
        this->acceptedMultipliers[2] = this->setMultipliers[2];
      }
    }
  }

  /**************************************************************************/
  /*Update state of friction at convergence according to gamma and
    lambda alone*/
  template<int N, class T>
  void ContactConstraintCR<N, T>::forceFriction(Vector4<T>& lm,
                                                Vector4<T>& lr,
                                                VectorC<T>& x,
                                                VectorC<T>& b,
                                                bool* changed,
                                                bool* frictionChanged,
                                                EvalStats& stats){
    int height = x.getSize();
    int row = this->row_id;

    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    DBG(message("FR force, %10.10e, %10.10e, %10.10e, %10.10e",
                lr[1], lr[2], lm[1], lm[2]));
    DBG(message("FR      , %10.10e, %10.10e, %10.10e, %10.10e",
                this->setMultipliers[1], this->setMultipliers[2],
                this->acceptedMultipliers[1], this->acceptedMultipliers[2]));

    if(this->frictionStatus[0] == Kinetic){
      /*Kinetic friction case*/
      if(lm[0] <= (T)0.0){
        /*If normal multiplier is invalid, kinetic friction is
          reset*/
        if(Abs(this->acceptedMultipliers[1]) > (T)0.0 ||
           Abs(this->acceptedMultipliers[2]) > (T)0.0){

          this->acceptedMultipliers[1] = (T)0.0;
          this->acceptedMultipliers[2] = (T)0.0;

          this->setMultipliers[1] = (T)0.0;
          this->setMultipliers[2] = (T)0.0;

          *frictionChanged = true;
          *changed = true;

          DBG(message("\t[%d] friction force reset kinetic to zero",
                      this->row_id));
          DBG(this->printIndices(std::cout));
          stats.n_friction_force++;
        }
      }else{
        /*Positive (valid) normal multiplier, check sliding direction*/
        Vector4<T> rVec(0, lr[1], lr[2], 0);

        if(rVec.length() > 1e-14){
          rVec.normalize();
        }else{
          rVec.set(0,1,1,0);
          rVec.normalize();
        }

        if((kineticVector[1] * lr[1] +
            kineticVector[2] * lr[2]) < (T)0.0){

          /*Sliding direction opposite to kinetic friction force ->
            switch to static friction*/
          this->frictionStatus[0] = Static;
          this->frictionStatus[1] = Static;

          DBG(message("\t[%d] friction force switch to static, value = %10.10e",
                      this->row_id,
                      kineticVector[1] * lr[1] + kineticVector[2] * lr[2] ));
          DBG(this->printIndices(std::cout));

          /*Notify solver*/
          *changed = true;
          stats.n_friction_force++;

          /*Reset constraint rhs and multipliers*/
          x[height + row * this->offset + 1] = this->acceptedMultipliers[1];
          x[height + row * this->offset + 2] = this->acceptedMultipliers[2];

          b[height + row * this->offset + 1] = this->c[1];
          b[height + row * this->offset + 2] = this->c[2];

          *frictionChanged = true;
          kineticVector.set(0,0,0,0);

          /*Reset history*/
          averageFrictionDirection.clear();
          averageFrictionMagnitude.clear();

          this->setMultipliers[1] = (T)0.0;
          this->setMultipliers[2] = (T)0.0;

          this->acceptedMultipliers[1] = this->setMultipliers[1];
          this->acceptedMultipliers[2] = this->setMultipliers[2];
        }
      }
    }else{
      /*Static friction case*/
      Vector4<T> hvVec(0, lr[1], lr[2], 0);
      Vector4<T> mVec (0, lm[1], lm[2], 0);

      if(mVec.length() > 1e-14){
        mVec.normalize();
      }

      /*Measure realtive error to constraint switch*/
      T difference = (T)0.0;

      T relativeError = (T)0.0;

      /*Compute difference in multipliers*/
      difference = Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) - Pos(this->mu * lm[0]));

      /*Convert multiplier distance to constraint error (distance)*/
      relativeError = Max(difference / (this->usedScale * bc->S[1][1]),
                          difference / (this->usedScale * bc->S[2][2]));

      difference = Sqrt(difference);

      T relativeDifference =
        Abs(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) -
            Pos(this->mu*lm[0])) / Pos(this->mu*lm[0]);

      if(relativeDifference > (T)1.0){
        relativeDifference = (T)1.0/relativeDifference;
      }

      if( Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >= Pos(this->mu * lm[0]) &&
          relativeError > (T)RERROR){
        START_DEBUG;
        warning("Should be forced static to kinetic");
        warning("lambda mu %10.10e", Pos(this->mu*lm[0]));
        warning("gamma     %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        std::cerr << lm << std::endl;
        std::cerr << lr << std::endl;
        std::cerr << kineticVector << std::endl;
        std::cerr << hvVec << std::endl;
        std::cerr << mVec << std::endl;
        END_DEBUG;
      }

      if(Sqrt(Sqr(lm[1]) + Sqr(lm[2])) >= Pos(this->mu * lm[0]) &&
         relativeError > (T)RERROR){
        /*Error between tangential multipliers and normal multiplier
          is too large and the relative error is too large, force the
          constraint to kinetic friction.*/
        DBG(message("\t[%d] friction force switch to kinetic, value = %10.10e, %10.10e, %10.10e, %10.10e",
                this->row_id,
                Sqrt(Sqr(lm[1]) + Sqr(lm[2])),
                Pos(this->mu * lm[0]),
                dot(hvVec, mVec),
                relativeError
                    ));
        DBG(this->printIndices(std::cout));

        START_DEBUG;
        message("gamma   = %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        message("mlambda = %10.10e", Pos(this->mu*lm[0]));
        message("difference = %10.10e", difference);
        (message("FORCED static to kinetic friction %d", this->row_id));

        warning("Forced static to kinetic");
        warning("lambda mu %10.10e", Pos(this->mu*lm[0]));
        warning("gamma     %10.10e", Sqrt(Sqr(lm[1]) + Sqr(lm[2])));
        std::cerr << lm << std::endl;
        std::cerr << lr << std::endl;
        std::cerr << kineticVector << std::endl;
        std::cerr << hvVec << std::endl;
        std::cerr << mVec << std::endl;
        END_DEBUG;

        /*Reset kinetic vector*/
        kineticVector = mVec;
        if(kineticVector.length() > 1e-14){
          kineticVector.normalize();
        }else{
          if(hvVec.length() > 1e-14){
            hvVec.normalize();
            //kineticVector += hvVec;
            kineticVector.normalize();
          }
        }

        cumulativeVector      = kineticVector;
        acceptedKineticVector = kineticVector;

        /*Switch to kinetic friction*/
        this->frictionStatus[0] = Kinetic;
        this->frictionStatus[1] = Kinetic;

        /*Set approximated kinetic friction multipliers to current
          value*/
        this->setMultipliers[1] = lm[1];
        this->setMultipliers[2] = lm[2];
        this->acceptedMultipliers[1] = this->setMultipliers[1];
        this->acceptedMultipliers[2] = this->setMultipliers[2];

        /*Notify solver*/
        *changed = true;
        *frictionChanged = true;
        stats.n_friction_force++;

        /*Reset x, b, i.e., reset constraint rhs and multipliers*/
        x[height + row * this->offset + 1] = 0;
        x[height + row * this->offset + 2] = 0;

        b[height + row * this->offset + 1] = 0;
        b[height + row * this->offset + 2] = 0;
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::update(VectorC<T>& b,
                                         VectorC<T>& x,
                                         VectorC<T>& b2){
    if(this->mu != (T)0.0){
      Vector4<T> lm, lr, lc;

      this->loadLocalValues(x, b, lm, lr, lc);

      bool changed = false;
      EvalStats stats;
      evaluateAndUpdateFriction(lm, lr,	x, b, &changed, &changed, stats);
    }
  }

  /**************************************************************************/
  /*Returns true id normal multiplier is positive*/
  template<int N, class T>
  bool ContactConstraintCR<N, T>::valid(const VectorC<T>& v)const{
    int row = this->row_id;
    int height = v.getSize();

    T lambda = v[height + row * this->offset + 0];

    return lambda > 1e-4;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::evaluateDepth(VectorC<T>& x,
                                                VectorC<T>& b,
                                                bool evaluatePenetration,
                                                Vector4<T>& lm,
                                                Vector4<T>& lr,
                                                bool* penetrationViolated,
                                                bool* changed,
                                                bool* active,
                                                int height,
                                                int row,
                                                bool removeDuplicates,
                                                EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    if(evaluatePenetration){
      /*Evaluate distance with multiplier value*/
      /*Cache last values*/

      DBG(message("\t[%d] NP evaluate, %10.10e, %10.10e, status = %d",
                  this->row_id, lr[0], lm[0], this->status == Active));

      /*Add current multiplier values to averages*/
      averageNPMagnitude.addSample(lm[0]);
      averageT1Magnitude.addSample(lm[1]);
      averageT2Magnitude.addSample(lm[2]);

      if((lr[0] <=  (T)0.0) /*Penetration depth*/
         &&
         (lm[0] <=  (T)0.0) /*Multiplier value*/){

        /*Negative penetration depth -> no penetration and a negative
          multiplier -> non active constraint.*/

        *penetrationViolated = true;

        /*Compute constraint error introduced by disabling this
          constraint*/
        T relativeError = Abs(lm[0]) / this->usedScale * bc->S[0][0];

        if(this->status == Active && relativeError > (T)0.1*(T)RERROR){
          /*Constraint is active and constraint error is above
            tolerance -> deactivate constraint.*/

          this->status = Inactive;
          (warning("status -> inactive, %10.10e, %10.10e, %d",
                   lr[0], lm[0], row));

          (message("\t[%d] NP switch to inactive, %10.10e, %10.10e, %10.10e, %10.10e",
                      this->row_id, lr[0], lm[0], relativeError, RERROR*this->bb2norm/scf));
          DBG(message("RERROR = %10.10e, bb2norm = %10.10e, scf = %10.10e",
                      RERROR, this->bb2norm, scf));
          DBG(this->printIndices(std::cout));

          /*Notify solvers*/
          *changed = true;
          stats.n_penetration_check++;

          /*Set cache and reset multipliers*/
          this->cache   = x[height + row * this->offset + 0];
          this->cacheT1 = x[height + row * this->offset + 1];
          this->cacheT2 = x[height + row * this->offset + 2];

          cumulativeVector = kineticVector;

          for(int i=0;i<this->offset;i++){
            x[height + row * this->offset + i] = 0;
          }
        }

        if(active){
          *active = true;
        }
      }else{
        if(this->status == Inactive && lr[0] > (T)0.0){
          /*Inactive constraint and constraint distance positive ->
            activate*/
          this->status = Active;

          (warning("status -> active, %10.10e, %10.10e, %d", lr[0], lm[0], row));
          (message("\t[%d] NP switch to active, %10.10e, %10.10e",
                   this->row_id, lr[0], lm[0]));
          DBG(this->printIndices(std::cout));

          /*Notify solver*/
          *changed = true;
          stats.n_penetration_check++;

          /*Set all multipliers using running averages*/
          lm[0] = averageNPMagnitude.getAverage();
          lm[1] = averageT1Magnitude.getAverage();
          lm[2] = averageT2Magnitude.getAverage();

          x[height + row * this->offset+0] = lm[0];
          x[height + row * this->offset+1] = lm[1];
          x[height + row * this->offset+2] = lm[2];

          /*Set initial friction state to kinetic*/
          this->frictionStatus[0] = Kinetic;
          this->frictionStatus[1] = Kinetic;
        }
        if(active){
          *active = true;
        }
      }
    }else if(evaluatePenetration && false){
      /*Keep constraint, although it can not change*/
      if(active){
        *active = true;
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::forceEvaluateDepth(VectorC<T>& x,
                                                     VectorC<T>& b,
                                                     bool evaluatePenetration,
                                                     Vector4<T>& lm,
                                                     Vector4<T>& lr,
                                                     bool* penetrationViolated,
                                                     bool* changed,
                                                     bool* active, int height,
                                                     int row,
                                                     bool removeDuplicates,
                                                     EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    *active = true;

    if(evaluatePenetration){
      /*Evaluate distance with multiplier value*/
      /*Cache last values*/

      DBG(message("\t[%d] NP forced evaluate, %10.10e, %10.10e",
                  this->row_id, lr[0], lm[0]));

      if(lm[0] <= (T)0.0){
        /*Negative multiplier*/

        if(this->status == Active){
          /*Active constraint and negative multiplier -> deactivate*/

          *penetrationViolated = true;

          (message("Active constraint forced, l = %10.10e", lm[0]));

          T relativeError = (T)0.0;

          T diff = Abs(lm[0]) / this->usedScale * bc->S[0][0];

          relativeError = diff;

          if(this->status == Active && relativeError > (T)0.1*(T)RERROR){
            /*Active constraint and constraint error due to
              deactivation above threshold -> deactivate*/

            (message("\t[%d] NP force to inactive, %10.10e, %10.10e, %10.10e",
                     this->row_id, lr[0], lm[0], relativeError));
            DBG(this->printIndices(std::cout));

            /*Deactivate constraint*/
            this->status = Inactive;

            /*Notify solver*/
            *changed = true;
            stats.n_penetration_force++;

            (message("FORCED status -> inactive %d", this->row_id));
            START_DEBUG;
            warning("Forced to inactive %d", this->row_id);
            std::cerr << lm << std::endl;
            std::cerr << lr << std::endl;
            END_DEBUG;

            /*Set cache and reset multipliers*/
            this->cache   = x[height + row * this->offset + 0];
            this->cacheT1 = x[height + row * this->offset + 1];
            this->cacheT2 = x[height + row * this->offset + 2];

            for(int i=0;i<this->offset;i++){
              x[height + row * this->offset + i] = 0;
            }
          }
          if(active){
            *active = true;
          }
        }
      }else if(lr[0] > (T)0.0 && this->status != Active){
        if(this->status == Inactive){
          /*Constraint distance positive and constraint inactive ->
            activate constraint*/
          this->status = Active;

          (warning("FORCED status -> active %d", this->row_id));
          (message("\t[%d] NP force to active, %10.10e, %10.10e",
                      this->row_id, lr[0], lm[0]));
          (this->printIndices(std::cout));

          START_DEBUG;
          std::cerr << lm << std::endl;
          std::cerr << lr << std::endl;
          END_DEBUG;

          /*Notify solver*/
          *changed = true;
          stats.n_penetration_force++;

          /*Set all multipliers using saved cache*/
          lm[0] = -this->cache*(T)0.5;
          lm[1] = -this->cacheT1;
          lm[2] = -this->cacheT2;

          x[height + row * this->offset+0] = lm[0];
          x[height + row * this->offset+1] = lm[1];
          x[height + row * this->offset+2] = lm[2];

          /*Set initial state to kinetic friction*/
          this->frictionStatus[0] = Kinetic;
          this->frictionStatus[1] = Kinetic;
        }

        if(active){
          *active = true;
        }
      }
    }else if(evaluatePenetration && false){
      /*Keep constraint, although it can not change*/
      if(active){
        *active = true;
      }
    }
  }

  /**************************************************************************/
  /*Evaluates Hx - b <=> lambda*/
  template<int N, class T>
  bool ContactConstraintCR<N, T>::evaluate(VectorC<T>& x,
                                           VectorC<T>& b, T meps,
                                           const EvalType& etype,
                                           EvalStats& stats,
                                           bool* active,
                                           VectorC<T>* kb){
    bool changed               = false;
    bool frictionChanged       = false;

    bool evaluatePenetration   = etype.penetrationCheck();
    bool evaluateFriction      = etype.frictionCheck();
    bool updateKineticFriction = etype.updateKineticFrictionCheck();
    bool acceptFriction        = etype.acceptFrictionCheck();
    bool evForceFriction       = etype.forceFrictionCheck();
    bool updateKineticVector   = etype.updateKineticVectorCheck();
    bool evForcePenetration    = etype.forcePenetrationCheck();
    bool removeDuplicates      = etype.forceDuplicateRemovalCheck();

    if(acceptFriction && (this->frictionStatus[0] == Kinetic ||
                          this->frictionStatus[1] == Kinetic) &&
       this->status == Active){

      /*Update acceptedFrictionMultipliers with latest values*/
      if(this->frictionStatus[0] == Kinetic){
        this->acceptedMultipliers[1] = this->setMultipliers[1];
      }
      if(this->frictionStatus[1] == Kinetic){
        this->acceptedMultipliers[2] = this->setMultipliers[2];
      }

      if(this->frictionStatus[0] == Kinetic){
        /*Update acceptedKineticVector*/
        acceptedKineticVector = kineticVector;
      }

      if(active){
        *active = true;
      }
      return false;
    }

    if(acceptFriction){
      if(active){
        *active = true;
      }
      /*Required to return after */
      return false;
    }

    START_DEBUG;
    message("status            = %d", this->status == Active);
    message("check penetration = %d", evaluatePenetration);
    message("check friction    = %d", evaluateFriction);
    message("update kinetic    = %d", updateKineticFriction);
    message("update kinetic vec= %d", updateKineticVector);
    message("remove duplicates = %d", removeDuplicates);
    message("force penetration = %d", evForcePenetration);
    message("force friction    = %d", evForceFriction);
    message("accept friction   = %d", acceptFriction);
    END_DEBUG;

    Vector4<T> lr, lc, lm;
    this->loadLocalValues(x, b, lm, lr, lc);

    int height = this->matrix->getHeight();
    int row    = this->row_id;

    START_DEBUG;
    message("multiplier value = %10.10e", lm[0]);
    message("depth      value = %10.10e", lr[0]);
    message("distance   value = %10.10e", lr[0] + lc[0]);
    message("if m < 0 && d < 0 -> not active");
    message("multiplier epsilon = %10.10e", meps);
    END_DEBUG;

    bool penetrationViolated = false;

    /*Evaluate non-penetration constraint*/
    if(evForcePenetration){
      /*Forced penetration evaluation*/
      forceEvaluateDepth(x, b, evaluatePenetration, lm, lr,
                         &penetrationViolated, &changed, active,
                         height, row, removeDuplicates, stats);
    }else{
      /*Regular penetration evaluation*/
      evaluateDepth(x, b, evaluatePenetration, lm, lr,
                    &penetrationViolated, &changed, active,
                    height, row, removeDuplicates, stats);
    }

    /*Evaluate friction constraint*/
    if(evaluateFriction && this->status == Active && this->mu != (T)0.0){
      if(evForceFriction){
        /*Forced friction evaluation*/
        forceFriction(lm, lr, x, b, &changed, &frictionChanged, stats);
      }else{
        /*Regular friction evaluation*/
        evaluateAndUpdateFriction(lm, lr, x, b, &changed, &frictionChanged,
                                  stats);
      }

      if(updateKineticFriction){
        /*Evaluate kinetic friction state*/
        evaluateKineticFriction(lm, lr, x, b, updateKineticFriction,
                                updateKineticVector,
                                &changed, &frictionChanged, stats);

        frictionChanged = false;
      }
    }

    updateRHS(kb);

    return changed;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::updateRHS(VectorC<T>* kb)const{
    if(kb == 0){
      return;
    }

    if(this->status == Inactive){
      /*Constraint not active -> skip*/
      return;
    }

    for(int i=0;i<2;i++){
      if(this->frictionStatus[i] == Kinetic){
        /*Move approximated constraint to RHS*/
        for(int j=0;j<5;j++){
          Vector4<T> res = -this->normals[j][i+1] * this->setMultipliers[i+1];

          if(this->index[j] == Constraint::undefined){
            continue;
          }

          tslassert(kb != 0);

          int row = this->index[j]*3;
          for(int k=0;k<3;k++){
            (*kb)[row+k] += res[k];
          }
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::multiply(VectorC<T>& r,
                                           const VectorC<T>& x)const{
    /*Multiply constraint r += Cx*/
    ConstraintMatrixStatus status = this->matrix->getStatus();

    if(status == Individual){
      if(this->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = this->getSize();

    const T* xdata  = x.getData();
    T*       rdatax = r.getExtendedData();

    Vector4<T> lr;
    Vector4<T> lcm;

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      Vector4<T> lx;
      int column = this->index[j]*3;

      /*Multiply if this constraint is active, or its owner has
        enabled all constraints.*/
      for(int i=0;i<3;i++){
        lx[i] = xdata[column+i];
      }

      lr += this->normals[j] * lx;

      //Compute Hv

      //Ignore active constraints
      if(status == AllActive){
        //Keep values
      }else if(status == Individual){
        if(this->frictionStatus[0] == Kinetic){
          lr[1] = 0;
        }
        if(this->frictionStatus[1] == Kinetic){
          lr[2] = 0;
        }
      }

      for(int i=0;i<3;i++){
        rdatax[cindex * offset + i] = lr[i];
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::multiplyTransposed(VectorC<T>& r,
                                                     const VectorC<T>& x)const{
    /*Multiply constraint r += C^T x*/
    Vector4<T> lx;
    Vector4<T> lcm;

    ConstraintMatrixStatus status = this->matrix->getStatus();

    if(status == Individual){
      if(this->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = this->getSize();

    const T* xdatax = x.getExtendedData();
    T*       rdata  = r.getData();

    for(int i=0;i<3;i++){
      //Ignore active constraints
      if(i>0){
        if(status == Individual){
          if(this->frictionStatus[i-1] == Kinetic){
            //Skip active constraint
            continue;
          }
        }else if(status == AllActive){
          //do not stop and load the multiplier
        }
      }

      lx[i] = xdatax[cindex * offset + i];
    }

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      Vector4<T> res = this->normalsT[j] * lx;

      int row = this->index[j]*3;

      for(int i=0;i<3;i++){
        rdata[row+i] += res[i];
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::computeFBFunction(VectorC<T>& np,
                                                    VectorC<T>& sf,
                                                    VectorC<T>& kf,
                                                    SpMatrixC<N, T>& C,
                                                    const VectorC<T>& x,
                                                    const VectorC<T>& b,
                                                    const VectorC<T>& b2){
    ContactBlockConstraint<N, T>* c =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    message("FB value comp, %d", this->row_id);
    Vector4<T> lm;
    Vector4<T> lr;
    Vector4<T> lc;

    this->loadLocalValues(x, b, lm, lr, lc);

    std::cout << lm;
    std::cout << lr;

    Vector4<T> fx; /*Multipliers*/
    Vector4<T> fy; /*Distances*/

    int cindex = this->row_id;
    int offset = this->getOffset();

    T* npdatax = np.getExtendedData();
    T* sfdatax = sf.getExtendedData();
    T* kfdatax = kf.getExtendedData();

    /*Non penetration*/
    if(this->status == Active){
      fx[0] = lm[0];

      if(this->frictionStatus[0] == Kinetic){
        T diff = (this->mu * lm[0] -
                  Sqrt(Sqr(this->acceptedMultipliers[1]) +
                       Sqr(this->acceptedMultipliers[2])));

        Vector4<T> dir(0, this->acceptedMultipliers[1],
                       this->acceptedMultipliers[2], 0);

        if(dir.length() > (T)1e-14){
          dir.normalize();
          dir *= diff;
        }

        fx[1] = Sqrt(Sqr(dir[1]) + Sqr(dir[2]));
        fx[2] = Sqrt(Sqr(dir[1]) + Sqr(dir[2]));
        message("Kinetic");
      }else{
        fx[1] = lm[1];
        fx[2] = lm[2];
      }

      std::cout << c->S;

      fx[0] /= this->usedScale*c->S[0][0];
      fx[1] /= this->usedScale*c->S[1][1];
      fx[2] /= this->usedScale*c->S[2][2];

      fy[0] = lr[0];
      fy[1] = lr[1];
      fy[2] = lr[2];
    }

    message("x, y");
    std::cout << fx;
    std::cout << fy;

    message("Accepted %10.10e, %10.10e",
            this->acceptedMultipliers[1], this->acceptedMultipliers[2]);

    for(int i=0;i<3;i++){
      T value = Abs(fx[i]) + Abs(fy[i]) - Sqrt(Sqr(fx[i]) + Sqr(fy[i]));

      if(i==0){
        npdatax[cindex * offset + i] = value;
        message("value[%d] = %10.10e", i, npdatax[cindex * offset + i]);
      }else{
        if(this->frictionStatus[0] == Kinetic){
          kfdatax[cindex * offset + i] = value;
          message("value[%d] = %10.10e", i, kfdatax[cindex * offset + i]);
        }else{
          sfdatax[cindex * offset + i] = value;
          message("value[%d] = %10.10e", i, sfdatax[cindex * offset + i]);
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::evaluateKineticFriction(Vector4<T>& lm,
                                                          Vector4<T>& lr,
                                                          VectorC<T>& x,
                                                          VectorC<T>& b,
                                                          bool updateKinetic,
                                                          bool updateVector,
                                                          bool* changed,
                                                          bool* frictionChanged,
                                                          EvalStats& stats){
    ContactBlockConstraint<N, T>* bc =
      (ContactBlockConstraint<N, T>*)this->blockConstraint;

    if(updateKinetic &&
       this->frictionStatus[0] == Kinetic && this->mu != (T)0.0){
      /*Kinetic friction and non-zero mu*/

      T oldValue = Sqrt(Sqr(this->acceptedMultipliers[1]) +
                        Sqr(this->acceptedMultipliers[2]));

      /*Update friction magnitude*/
      averageFrictionMagnitude.addSample(Pos(lm[0] * this->mu));

      T newValue = averageFrictionMagnitude.getWeightedAverage();

      if(lm[0] < (T)0.0){
        newValue = (T)0.0;
      }

      DBG(message("oldValue = %10.10e", oldValue));
      DBG(message("newValue = %10.10e", newValue));
      DBG(message("curValue = %10.10e", Pos(this->mu*lm[0])));

      bool kineticFrictionUpdated = false;

      T factor = newValue/oldValue;

      if(factor > (T)1.0){
        factor = (T)1.0/factor;
      }

      if(Abs(oldValue) < 1e-9){
        factor = (T)0.0;
      }

      DBG(message("factor = %10.10e", factor));

      /*Check for violation with current kinetic vector*/
      bool vectorUpdated = false;

      Vector4<T> hvVec(0, lr[1], lr[2], 0);
      Vector4<T> curVector(0, lr[1], lr[2], 0);
      Vector4<T> oldVector = kineticVector;

      DBG(message("curVector = %10.10e", curVector.length()));
      DBG(std::cout << curVector << std::endl);

      if(curVector.length() > 1e-14){
        curVector.normalize();
      }else{
        curVector.set(0, 1, 1, 0);
        curVector.normalize();
      }

      T weight = (T)1.0;
      T length = hvVec.length();

      /*Compute weight*/
      if(length > Abs(HV_EPS*15.0)){
        /*Sliding distance significant*/
        weight = 1;
      }else if(length < Abs(HV_EPS*1.0)){
        /*Sliding distance not significant*/
        weight = 0;
      }else{
        /*Length = between 10 * HV_EPS and HV_eps compute weight*/
        weight = (length - (T)(Abs(HV_EPS)*1.0))/(T)(14.0*Abs(HV_EPS));
      }

      DBG(message("weigth = %10.10e", weight));

      tslassert(weight <= (T)1.0);
      tslassert(weight >= (T)0.0);

      Vector4<T> weightedHalfVector =
        (T)1.0*kineticVector + (T)1.0*curVector * weight;

      DBG(message("weighted halfVector = %10.10e",
                  weightedHalfVector.length()));
      DBG(std::cout << weightedHalfVector << std::endl);

      if(weightedHalfVector.length() < 1e-14){
        /*If zero, kinetic vector is undefined, and hvVec
          might be very small. However, hvVec is the only
          vector we currently have.*/
        weightedHalfVector = curVector;
      }else{
        weightedHalfVector.normalize();
      }

      //averageFrictionDirection.addSample(weightedHalfVector);
      //averageFrictionDirection.addSample(curVector*length);

      DBG(message("multipliers = %10.10e, %10.10e, %10.10e",
                  lm[0], lm[1], lm[2]));

      if( (updateVector || kineticVector.length() < 1e-14)
          &&
          lm[0] > (T)0.0){
        DBG(message("update vector = %d", updateVector));

        if(kineticVector.length() > 1e-14 &&
           (kineticVector[1] * lr[1] + kineticVector[2] * lr[2]) <
           Abs(HV_EPS/(T)10.0)){
          /*Kinetic vector can not be updated since it is too small*/
          DBG(message("not updated"));
        }else{
          Vector4<T> lnormal(1,0,0,0);
          oldVector = oldVector;

          DBG(message("old vector"));
          DBG(std::cout << oldVector << std::endl);

          if(this->frictionStatus[0] == Kinetic){
            /*Kinetic friction*/

            DBG(message("dot(kv, cv) = %10.10e",
                        dot(kineticVector, curVector)));

            if(dot(kineticVector, curVector) > 0 ||
               kineticVector.length() < 1e-14){

              T val = (T)1.0-dot(acceptedKineticVector, weightedHalfVector);
              T val2 = dot(acceptedKineticVector, curVector);

              START_DEBUG;
              message("val = %10.10e", val);
              message("accepted * weighted = %10.10e",
                      dot(acceptedKineticVector, weightedHalfVector));
              message("hv * kinetic = %10.10e",
                      dot(curVector, kineticVector));
              END_DEBUG;

              bool force = false;
              if(kineticVector.length() < 1e-14){
                force = true;
                val = 1;
                val2 = 1;
              }

              DBG(message("hv * kinetic = %10.10e", dot(hvVec, kineticVector)));

              /*If HV is small, it makes no sense to update the
                kinetic vector, since it can point in any direction.*/
              Vector4<T> lastVector = oldVector;//kineticVector;
              if( (Abs(val2) < 0.99984 &&
                   dot(hvVec, kineticVector) > HV_EPS &&
                   hvVec.length() > Abs(HV_EPS)) || force){

                DBG(message("update cumulative vector"));

                Vector4<T> diffVector = (kineticVector - curVector)/(T)50.0;

                kineticVector += weightedHalfVector;

                if(kineticVector.length() > 1e-14){
                  kineticVector.normalize();
                }

                cumulativeVector +=
                  kineticVector * (T)0.125;

                cumulativeVector +=
                  lastVector * (T)0.25;

                cumulativeVector /= (T)1.1;

                cumulativeVector = lastVector - diffVector;
                cumulativeVector.normalize();

                averageFrictionDirection.addSample(cumulativeVector);

                cumulativeVector =
                  ((T)1*averageFrictionDirection.getWeightedAverage() +
                   (T)0*averageFrictionDirection.getCumulativeAverage())/(T)1.0;

                vectorUpdated = true;

                START_DEBUG;
                message("lastVector * cumulativeVector = %10.10e",
                        dot(lastVector,
                            cumulativeVector/cumulativeVector.length()));
                message("lastVec.length() = %10.10e", lastVector.length());

                message("hvVec * cumulativeVector = %10.10e",
                        dot(curVector,
                            cumulativeVector/cumulativeVector.length()));
                END_DEBUG;

                if(dot(lastVector,
                       cumulativeVector/cumulativeVector.length()) > 0.999 &&
                   lastVector.length() > 1e-6 && !force){
                  vectorUpdated = false;
                  kineticVector = lastVector;
                  DBG(message("kinetic vector not updated"));
                }
              }

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }else{
                kineticVector.set(0,1,1,0);
                kineticVector = weightedHalfVector;
                kineticVector.normalize();
                //vectorUpdated = false;
              }

              START_DEBUG;
              message("weighted half vector");
              std::cout << weightedHalfVector << std::endl;
              message("lastVector");
              std::cout << lastVector << std::endl;
              message("kineticVector");
              std::cout << kineticVector << std::endl;

              message("proj = %10.10e", dot(lastVector, kineticVector));
              END_DEBUG;
            }else{
              START_DEBUG;
              message("cumulative not updated");
              message("hvec,length = %10.10e", hvVec.length());
              END_DEBUG;
            }

            if(vectorUpdated){
              DBG(message("Vector updated"));

              kineticVector = cumulativeVector;

              DBG(message("cumulativeVector"));
              DBG(std::cout << cumulativeVector << std::endl);

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }

              message("Kinetic vector and cumulative vector");
              std::cout << kineticVector << std::endl;
              std::cout << cumulativeVector << std::endl;
            }else if(kineticVector.length() > 1e-6){
              /*Update vector without trigger iff*/
              DBG(message("Vector not updated"));

              kineticVector = oldVector;

              Vector4<T> lastVector = kineticVector;

              kineticVector += weightedHalfVector;
              kineticVector.normalize();

              cumulativeVector += kineticVector * (T)0.01;
              cumulativeVector += lastVector * (T)0.99;

              cumulativeVector /= (T)1.1;

              kineticVector = cumulativeVector;

              if(kineticVector.length() > 1e-14){
                kineticVector.normalize();
              }

              START_DEBUG;
              message("weighted half vector");
              std::cout << weightedHalfVector << std::endl;
              message("lastVector");
              std::cout << lastVector << std::endl;
              message("kineticVector");
              std::cout << kineticVector << std::endl;

              message("proj = %10.10e", dot(lastVector, kineticVector));
              END_DEBUG;
            }
          }
        }
      }else if(lm[0] < 0){
        /*kineticVector = curVector;
        kineticVector.normalize();

        cumulativeVector = kineticVector;*/
      }

      START_DEBUG;
      message("Cumulative vector = %10.10e", cumulativeVector.length());
      std::cout << cumulativeVector << std::endl;

      message("setting multipliers");
      END_DEBUG;

      this->setMultipliers[1] = kineticVector[1] * newValue;
      this->setMultipliers[2] = kineticVector[2] * newValue;

      T difference = 0;
      T magnitude  = 0;

      T diff =
        Sqrt(Sqr(this->setMultipliers[1]) +
             Sqr(this->setMultipliers[2]))
        -
        Sqrt(Sqr(this->acceptedMultipliers[1]) +
             Sqr(this->acceptedMultipliers[2])
             );

      DBG(message("diff = %10.10e", diff));

      Vector4<T> dir(0, this->setMultipliers[1], this->setMultipliers[2], 0);

      DBG(message("dir"));
      DBG(std::cout << dir);

      if(dir.length() > (T)1e-14){
        dir.normalize();
        dir *= diff;
      }

      DBG(message("dir"));
      DBG(std::cout << dir);

      dir[1] /= this->usedScale*bc->S[1][1];
      dir[2] /= this->usedScale*bc->S[2][2];

      DBG(std::cout << dir);

      T relativeError = Max(Abs(dir[1]), Abs(dir[2]));

      DBG(message("relative error = %10.10e", relativeError));

      if(vectorUpdated){
        stats.n_kinetic_friction_vector_update++;
      }


      if((vectorUpdated || true) &&
         relativeError > (T)RERROR){

        if(this->frictionStatus[0] == Kinetic){
          START_DEBUG;
          DBG(message("\t[%d] friction update kinetic 1, value = %10.10e, %10.10e, %10.10e, %10.10e, factor = %10.10e",
                  this->row_id,
                  relativeError,
                  newValue,
                  oldValue,
                   dot(oldVector, kineticVector), factor
                   ));
          DBG(std::cout << oldVector << std::endl);
          DBG(std::cout << kineticVector << std::endl);
          DBG(std::cout << hvVec << std::endl);
          DBG(std::cout << curVector << std::endl);
          DBG(message("dot hvvec kinetic vector = %10.10e", dot(hvVec, kineticVector)));
#ifdef _DEBUG
          Vector4<T> acc(0, this->acceptedMultipliers[1],
                         this->acceptedMultipliers[2], 0);
#endif
          DBG(message("dot hvvec multipliers = %10.10e", dot(hvVec, acc)));

          DBG(this->printIndices(std::cout));
          END_DEBUG;

#ifdef _DEBUG
          ContactBlockConstraint<N, T>* ccc =
            (ContactBlockConstraint<N, T>*)this->blockConstraint;
#endif
          DBG(std::cout << ccc->S << std::endl);

          DBG(std::cout << kineticVector << std::endl);

          //START_DEBUG;
          message("Updating %d", this->row_id);
          message("difference = %10.10e", difference);
          message("factor     = %10.10e", factor);
          message("diff/magn  = %10.10e", difference/magnitude);
          message("gamma      = %10.10e", lm[0]*this->mu);
          message("gamma'     = %10.10e", oldValue);
          message("relative error = %10.10e", relativeError);
          std::cout << hvVec << std::endl;
          std::cout << lr << std::endl;
          std::cout << oldVector << std::endl;
          std::cout << kineticVector << std::endl;
          std::cout << curVector << std::endl;
          std::cout << weightedHalfVector << std::endl;
          std::cout << cumulativeVector / cumulativeVector.length() << std::endl;
          std::cout << acceptedKineticVector << std::endl;
          message("hv * kin = %10.10e", dot(hvVec, kineticVector));
          //END_DEBUG;

          /*Update RHS*/
          *changed = true;
          kineticFrictionUpdated = true;
          stats.n_kinetic_friction_update++;

          *frictionChanged = true;
        }
      }else if(vectorUpdated &&
               difference > (T)0.0 &&
               factor < (T)1.0 &&
               relativeError > (T)RERROR){

        (message("\t[%d] friction update kinetic 2, value = %10.10e, %10.10e, %10.10e, %10.10e, factor = %10.10e",
                 this->row_id,
                 relativeError,
                 newValue,
                 oldValue,
                 dot(oldVector, kineticVector), factor));
        (this->printIndices(std::cout));

        DBG(std::cout << kineticVector << std::endl);

        ContactBlockConstraint<N, T>* ccc =
          (ContactBlockConstraint<N, T>*)this->blockConstraint;

        (std::cout << ccc->S << std::endl);

        START_DEBUG;
        message("Updating2 %d", this->row_id);
        message("difference = %10.10e", difference);
        message("factor     = %10.10e", factor);
        message("diff/magn  = %10.10e", difference/magnitude);
        message("gamma      = %10.10e", lm[0]*this->mu);
        message("gamma'     = %10.10e", oldValue);
        std::cout << hvVec << std::endl;
        std::cout << lr << std::endl;
        std::cout << oldVector << std::endl;
        std::cout << kineticVector << std::endl;
        std::cout << curVector << std::endl;
        std::cout << weightedHalfVector << std::endl;
        std::cout << cumulativeVector / cumulativeVector.length() << std::endl;
        std::cout << acceptedKineticVector << std::endl;

        message("hv * kin = %10.10e", dot(hvVec, kineticVector));
        END_DEBUG;

        /*Update RHS*/
        *changed = true;
        kineticFrictionUpdated = true;
        *frictionChanged = true;
        stats.n_kinetic_friction_vector_update++;
      }

      if(kineticFrictionUpdated){
        this->acceptedMultipliers[1] = this->setMultipliers[1];
        this->acceptedMultipliers[2] = this->setMultipliers[2];

        acceptedKineticVector = kineticVector;
      }

      if(*frictionChanged == false){
        /*Not active or no update requested, update shadow values,
          in case some other update is triggered*/

        T oldValue = Sqrt(Sqr(this->acceptedMultipliers[1]) +
                          Sqr(this->acceptedMultipliers[2]));

        T newValue = (Pos(lm[0] * this->mu) + (T)5.0*oldValue)/(T)6.0;

        if(lm[0] < 0.0){
          //newValue /= (T)2.0;
          newValue = (T)0.0;
        }

        this->setMultipliers[1] = kineticVector[1] * newValue;
        this->setMultipliers[2] = kineticVector[2] * newValue;
        DBG(message("Shadow update"));
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  AbstractBlockConstraint<N, T>*
  ContactConstraintCR<N, T>::constructBlockConstraint()const{
    AbstractBlockConstraint<N, T>* cc =
      new ContactBlockConstraint<N, T>(UsesFullPreconditioner<SelfType>());

    cc->status = this->status;
    this->blockConstraint = cc;
    return cc;
  }



  /**************************************************************************/
  template<int N, class T>
  RowConstraintMultiplier<N, T>*
  ContactConstraintCR<N, T>::constructConstraintMultiplier()const{
    //return new RowContactConstraintMultiplier<N, T>();
    return 0;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::computePreconditioner(VectorC<T>& C)const{
    this->preconditioner = &C;

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size = C.getSize();

    for(int i=0;i<3;i++){
      /*Clear constraint part in diagonal preconditioner, these values
        are stored in the precondtioner matrix.*/
      C[size + cindex * offset + i] = 0.0;
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraintCR<N, T>::
  computePreconditionerMatrix(VectorC<T>& DD,
                              SpMatrixC<N, T>& C,
                              const SpMatrixC<N, T>& B,
                              AdjacencyList* l,
                              VectorC<T>* scaling)const{
    ContactBlockConstraint<N, T>* c =
      (ContactBlockConstraint<N, T>*)C.getConstraint(this->row_id);


    if(this->status == Inactive){
      //return;
    }

    c->cSize = this->getSize();
    int size = this->getSize();

    /*Copy all indices*/
    for(int i=0;i<size;i++){
      c->setIndex(i, this->getIndex(i));
    }

    /*Compute S*/
    c->S.clear();
    c->OS.clear();

    Matrix44<T> tmp;

    for(int j=0;j<size;j++){
      if(this->getIndex(j) == Constraint::undefined){
        continue;
      }

      Matrix44<T> diag;

      int col = this->getIndex(j) * 3;

      for(int k=0;k<3;k++){
        diag[k][k] = DD[col+k];
      }

      tmp += this->normals[j] * diag * this->normalsT[j];
    }

    if(tmp[0][0] == (T)0.0){
      tmp[0][0] = 1.0;
    }
    if(tmp[1][1] == (T)0.0){
      tmp[1][1] = 1.0;
    }
    if(tmp[2][2] == (T)0.0){
      tmp[2][2] = 1.0;
    }

    T factor = (T)1.0;

    if(UsesFullPreconditioner<SelfType>()){
      T* xdata = scaling->getExtendedData();
      factor = xdata[this->row_id * this->getOffset()];
    }

    if(factor < (T)1.0){
      factor = (T)1.0;
    }

    this->usedScale = Max(factor, (T)1.5);

    if(this->usedScale == (T)1.5){
      //warning("usedScale = 1.5...");
    }

    bool singular = false;

    for(int j=0;j<3;j++){
      c->S[j][j]  = ((T)1.0/(tmp[j][j]))/(this->usedScale);
      c->OS[j][j] = ((T)1.0/(tmp[j][j]))/(this->usedScale);

      if(IsNan(c->S[j][j]) || IsNan(c->OS[j][j])){
        singular = true;
      }
    }

    if(singular){
      std::cout << tmp << std::endl;
      std::cout << c->S << std::endl;
      std::cout << c->OS << std::endl;
      this->print(std::cout);

      for(int j=0;j<size;j++){
        if(this->getIndex(j) == Constraint::undefined){
          continue;
        }

        Matrix44<T> diag;

        int col = this->getIndex(j)*3;

        for(int k=0;k<3;k++){
          diag[k][k] = DD[col+k];
        }
        std::cout << diag << std::endl;
      }

      error("singular contact preconditioner");
    }

    for(int i=0;i<size;i++){
      if(this->getIndex(i) == Constraint::undefined){
        continue;
      }

      int col = this->getIndex(i)*3;

      Matrix44<T> diag;

      for(int j=0;j<3;j++){
        diag[j][j] = (DD[col+j]);
      }

      c->HA[i]  = this->normals[i] * diag;
      c->AHT[i] = diag * this->normalsT[i];

      c->SHA[i] = (c->OS * this->normals[i]) * diag;
      c->AHS[i] = diag * (this->normalsT[i] * c->OS);

      c->SHAT[i] = c->SHA[i].transpose();
      c->AHST[i] = c->AHS[i].transpose();
      c->HAT[i]  = c->HA[i].transpose();
    }
  }

  template class ContactConstraintCR<1, float>;
  template class ContactConstraintCR<2, float>;
  template class ContactConstraintCR<4, float>;
  template class ContactConstraintCR<8, float>;

  template class ContactConstraintCR<1, double>;
  template class ContactConstraintCR<2, double>;
  template class ContactConstraintCR<4, double>;
  template class ContactConstraintCR<8, double>;
}
