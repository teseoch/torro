/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/constraints/ContactConstraint2.hpp"
#include "collision/constraints/ContactBlockConstraint.hpp"
#include "collision/ConstraintTol.hpp"
#include "datastructures/AdjacencyList.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  ContactConstraint<N, T>::ContactConstraint(){
    frictionStatus[0] = Kinetic; //->kinetic friction
    frictionStatus[1] = Kinetic; //->kinetic friction

    this->offset = 3;

    for(int i=0;i<5;i++){
      index[i] = Constraint::undefined;
    }

    c.set(0, 0, 0, 0);

    for(int i=0;i<3;i++){
      setMultipliers[i] = 0;
      acceptedMultipliers[i] = 0;
    }

    cache   = (T)0.0;
    cacheT1 = (T)0.0;
    cacheT2 = (T)0.0;

    usedScale = (T)1.0;

    this->subType = CONTACT_CONSTRAINT_SUBTYPE;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::preMultiply(VectorC<T>& r,
                                            const VectorC<T>& x,
                                            const SpMatrixC<N, T>* mat,
                                            bool transposed,
                                            const VectorC<T>* mask)const{
    /*Do nothing, this type of constraint does not need any special
      preMultiplication.*/
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::setMu(T m){
    mu = m;
  }

  /**************************************************************************/
  /*r = vector4, i = row in constraint, j = block, idx = first
    absolute index*/
  template<int N, class T>
  void ContactConstraint<N, T>::setRow(const Vector4<T>& r,
                                       T fc, int i, int j, int idx){
    tslassert(i < 4);
    tslassert(j < 5);

    if(IsNan(r) || IsNan(fc)){
      std::cout << r << std::endl;
      std::cout << fc << std::endl;
      error("NAN constraint vector");
    }

    normals[j][i] = r;
    c[i] = fc;
    this->index[j] = idx;

    if((j+1) > this->cSize){
      this->cSize = j+1;
    }
  }

  /**************************************************************************/
  template<int N, class T>
  Vector4<T> ContactConstraint<N, T>::getRow(int i, int j)const{
    tslassert(i < 4);
    tslassert(j < 5);
    return normals[j][i];
  }

  /**************************************************************************/
  template<int N, class T>
  ConstraintStatus ContactConstraint<N, T>::getStatus()const{
    return this->status;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::resetMultipliers(VectorC<T>& x,
                                                 Vector4<T>& old)const{
    int row = this->row_id;
    T* xdatax = x.getExtendedData();

    cache   = xdatax[row * this->offset + 0];
    cacheT1 = xdatax[row * this->offset + 1];
    cacheT2 = xdatax[row * this->offset + 2];

    for(int i=0;i<this->offset;i++){
      xdatax[row * this->offset + i] = old[i];
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::cacheMultipliers(Vector4<T>& old,
                                                 VectorC<T>& x)const{
    int row = this->row_id;
    T* xdatax = x.getExtendedData();

    //cache = xdatax[row * this->offset + 0];
    for(int i=0;i<this->offset;i++){
      old[i] = xdatax[row * this->offset + i];
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::loadLocalValues(const VectorC<T>& x,
                                                const VectorC<T>& b,
                                                Vector4<T>& lm,
                                                Vector4<T>& lr,
                                                Vector4<T>& lc)const{
    int row  = this->row_id;
    int size = this->getSize();

    const T* xdatax = x.getExtendedData();
    const T* xdata  = x.getData();

    for(int i=0;i<3;i++){
      lm[i]  = xdatax[row * this->offset + i]; /*Get corresponding
                                                 multipliers*/
      lc[i] = c[i];
    }

    /*Evaluate distance*/
    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }
      Vector4<T> lx;

      for(int i=0;i<3;i++){
        lx[i] = xdata[this->index[j] * 3 + i];  /*Get vector x*/
      }

      lr[0] += dot(normals[j][0], lx);
      lr[1] += dot(normals[j][1], lx);
      lr[2] += dot(normals[j][2], lx);
    }

    for(int j=0;j<3;j++){
      lr[j] -= lc[j];
    }
  }

  /**************************************************************************/
  /*initialize constraint*/
  template<int N, class T>
  void ContactConstraint<N, T>::init(SpMatrixC<N, T>* owner){
    AbstractMatrixConstraint<N, T>::init(owner);
    /*Copy matrices*/
    normalsT[0] = normals[0].transpose();
    normalsT[1] = normals[1].transpose();
    normalsT[2] = normals[2].transpose();
    normalsT[3] = normals[3].transpose();
    normalsT[4] = normals[4].transpose();
  }

  /**************************************************************************/
  template<int N, class T>
  bool ContactConstraint<N, T>::project(VectorC<T>& x,
                                        VectorC<T>& b,
                                        T meps,
                                        const EvalType& etype,
                                        bool* active,
                                        VectorC<T>* kb){
    int row = this->row_id;

    T* xdatax = x.getExtendedData();

    /*NP*/
    if(xdatax[row * this->offset + 0] < (T)0.0){
      xdatax[row * this->offset + 0] = (T)0.0;
    }

    /*Friction*/
    if(frictionStatus[0] == Kinetic){
      T length = Sqrt(Sqr(xdatax[row * this->offset + 1]) +
                      Sqr(xdatax[row * this->offset + 2]));

      if(length > xdatax[row * this->offset + 0] * this->mu){
        Vector4<T> dir(xdatax[row * this->offset + 1],
                       xdatax[row * this->offset + 2],
                       0,0);

        dir.normalize();
        dir *= mu * xdatax[row * this->offset + 0];

        xdatax[row * this->offset + 1] = dir[0];
        xdatax[row * this->offset + 2] = dir[1];
      }
    }

    return false;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::print(std::ostream& os)const{
    bool st = false;

    if(this->status == Active){
      st = true;
    }

    os << "Row = " << this->row_id << ", column[0] = " << index[0]*3 <<
      ", column[1] = " << index[1]*3 << ", column[2] = " << index[2]*3 <<
      ", column[3] = " << index[3]*3 << ", column[4] = " << index[4]*3 <<
      ", status = " << st << std::endl;

    os << "Source Type = ";
    if(this->sourceType == 1){
      os << "Edge - Edge ";
    }else if(this->sourceType == 2){
      os << "Face - Vertex ";
    }else if(this->sourceType == 3){
      os << "BackFace - Vertex ";
    }else if(this->sourceType == 4){
      os << "BackEdge - BackEdge ";
    }

    os << this->sourceType << ", A = " << this->sourceA <<
      ", B = " << this->sourceB << std::endl;

    bool f1, f2;

    f1 = f2 = false;

    if(frictionStatus[0] == Kinetic){
      f1 =true;
    }

    if(frictionStatus[1] == Kinetic){
      f2 =true;
    }

    message("this = %p", this);

    os << "f[0] = " << f1 << ", f[1] = " << f2 << ", mu = "
       << mu << ", c[0] = " << c[0]
       << ", c[1] = " << c[1]
       << ", c[2] = " << c[2] << std::endl;

    for(int i=0;i<5;i++){
      message("normals");
      os << normals[i] << std::endl;

      message("normalsT");
      os << normalsT[i] << std::endl;
    }

    if(Abs(c[0]) > 1E-2){
      warning("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      message("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      //getchar();
      //abort();
    }
    message("this = %p", this);
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::printIndices(std::ostream& os)const{
    bool st = false;

    if(this->status == Active){
      st = true;
    }

    os << "Row = " << this->row_id << ", column[0] = " << index[0]*3 <<
      ", column[1] = " << index[1]*3 << ", column[2] = " << index[2]*3 <<
      ", column[3] = " << index[3]*3 << ", column[4] = " << index[4]*3 <<
      ", status = " << st << std::endl;

    os << "Source Type = ";

    if(this->sourceType == 1){
      os << "Edge - Edge ";
    }else if(this->sourceType == 2){
      os << "Face - Vertex ";
    }else if(this->sourceType == 3){
      os << "BackFace - BackVertex ";
    }else if(this->sourceType == 4){
      os << "BackEdge - BackEdge ";
    }else if(this->sourceType == 8){
      os << "Edge - Vertex";
    }

    os << this->sourceType << ", A = " << this->sourceA <<
      ", B = " << this->sourceB << std::endl;

    bool f1, f2;

    f1 = f2 = false;

    if(frictionStatus[0] == Kinetic){
      f1 = true;
    }

    if(frictionStatus[1] == Kinetic){
      f2 = true;
    }

    message("this = %p", this);

    os << "f[0] = " << f1 << ", f[1] = " << f2 << ", mu = "
       << mu << ", c[0] = " << c[0]
       << ", c[1] = " << c[1]
       << ", c[2] = " << c[2] << std::endl;

    if(Abs(c[0]) > 1E-2){
      warning("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      message("c too high = %10.10e, row_id = %d, %p", c[0], this->row_id,
              this);
      //getchar();
      //abort();
    }
    message("used scale = %10.10e", usedScale);
    message("this = %p", this);
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::collectElements(List<ConstraintElement<T> >&
                                                elements)const{
    int row = this->row_id;
    int offset = 3;
    int size = this->getSize();

    for(int i=0;i<size;i++){
      if(this->getIndex(i) == Constraint::undefined){
        continue;
      }

      int column = this->getIndex(i)*3;
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          if(k>0){
            if(frictionStatus[k-1] == Kinetic){
              continue;
            }
          }
          ConstraintElement<T> element;
          element.row   = this->matrix->getHeight() + row * offset + j;
          element.col   = column + k;
          element.value = normals[i][j][k];
          elements.append(element);
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  int ContactConstraint<N, T>::getIndex(int i)const{
    tslassert(i>=0);
    tslassert(i<5);
    return index[i];
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactConstraint<N, T>::setIndex(int i, int idx){
    tslassert(i>=0);
    tslassert(i<5);
    index[i] = idx;
  }

  /**************************************************************************/
  /*Store values in SparseMatrix*/
  template<int N, class T>
  void ContactConstraint<N, T>::extractValues(SpMatrix<N, T>* L,
                                              SpMatrix<N, T>* LT,
                                              Tree<int>* columns,
                                              int row,
                                              Vector<T>* rhs)const{
    int offset = 3;
    int size = this->getSize();

    for(int j=0;j<size;j++){
      if(this->index[j] == Constraint::undefined){
        continue;
      }

      Vector4<T> lx;
      int column = this->index[j]*3;

      for(int k=0;k<3;k++){
        for(int l=0;l<3;l++){
          int column2 = column+l;
          columns->uniqueInsert(column2, column2);

          (*L )[row * offset + k][column2] = normals[j][k][l];
          (*LT)[column2][row * offset + k] = normals[j][k][l];
        }
      }
    }
    if(rhs){
      for(int j=0;j<offset;j++){
        (*rhs)[row * offset + j] = c[j];
      }
    }
  }

  template class ContactConstraint<1, float>;
  template class ContactConstraint<2, float>;
  template class ContactConstraint<4, float>;
  template class ContactConstraint<8, float>;

  template class ContactConstraint<1, double>;
  template class ContactConstraint<2, double>;
  template class ContactConstraint<4, double>;
  template class ContactConstraint<8, double>;
}
