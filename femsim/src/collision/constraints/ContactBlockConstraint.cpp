/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/constraints/ContactBlockConstraint.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "math/VectorC.hpp"


namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  ContactBlockConstraint<N, T>::
  ContactBlockConstraint(bool full):fullPreconditioner(full){
    this->offset = 3;
    index[0] = index[1] = index[2] = index[3] = index[4] =
      Constraint::undefined;
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::multiplyAHSHA(VectorC<T>& r,
                                                   const VectorC<T>& x,
                                                   const SpMatrixC<N, T>* mat,
                                                   bool transposed,
                                                   const VectorC<T>* mask)const{
    if(fullPreconditioner){
      ConstraintMatrixStatus status = mat->getStatus();

      if(status == Individual){
        if(this->rowConstraint->status == Inactive){
          return;//continue;
        }
      }else if(status == AllActive){
        //Just multiply
      }else if(status == NoneActive){
        return;
      }else{
        error("Unimplemented mode");
      }

      int cindex = this->row_id;
      int offset = this->getOffset();
      int size   = this->getSize();

      const T*  maskx  = 0;

      if(mask){
        maskx = mask->getExtendedData();
      }

      /*Multiply SHA*x = r*/
      const T* xdata  = x.getData();
      T*       rdatax = r.getExtendedData();
      T*       rdata  = r.getData();

      ContactConstraint<N, T>* constraint =
        (ContactConstraint<N, T>*)this->rowConstraint;

      Vector4<T> lx;
      Vector4<T> lr;

      for(int i=0;i<size;i++){
        if(this->getIndex(i) == Constraint::undefined){
          continue;
        }

        /*Load x for sub-constraint i*/
        int col = this->getIndex(i)*3;

        if(mask){
          for(int j=0;j<3;j++){
            lx[j] = xdata[col+j] * (*mask)[col+j];
          }
        }else{
          for(int j=0;j<3;j++){
            lx[j] = xdata[col+j];
          }
        }

        if(transposed){
          lr += this->AHST[i] * lx;
        }else{
          lr += this->SHA[i]  * lx;
        }
      }

      for(int i=0;i<3;i++){
        if(mask){
          lr[i] *= maskx[cindex * offset + i];
      }

        if(i > 0){
          if(status == Individual){
            if(constraint->frictionStatus[i-1] == Kinetic){
              ///Important
              //Skip active constraint
              lr[i] = 0;
            }
          }
        }

        /*Also store lr in r, since this is the result of the AHS
          preconditioner*/
        rdatax[cindex * offset + i] = lr[i];
      }

      /*Result of multiplication stored in lr*/

      /*Multiply AHT*lr  and store A - AHT*lr = r*/

      /*Multiply AHT*/
      for(int i=0;i<size;i++){
        if(this->getIndex(i) == Constraint::undefined){
          continue;
        }

        Vector4<T> res;
        if(transposed){
          res = this->HAT[i] * lr;
        }else{
          res = this->AHT[i] * lr;
        }

        int col = this->getIndex(i)*3;

        if(mask){
          for(int j=0;j<3;j++){
            rdata[col+j] += -res[j] * (*mask)[col+j];
          }
        }else{
          for(int j=0;j<3;j++){
            rdata[col+j] += -res[j];
          }
        }
      }
    }else{
      return;
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::generalMultiply(VectorC<T>& r,
                                                     const VectorC<T>& x,
                                                     bool transposed)const{
    ConstraintMatrixStatus status = this->matrix->getStatus();

    if(status == Individual){
      if(this->rowConstraint->status == Inactive){
        return;
      }
    }else if(status == AllActive){
      //Just multiply
    }else if(status == NoneActive){
      return;
    }else{
      error("Unimplemented mode");
    }

    int cindex = this->row_id;
    int offset = this->getOffset();
    int size   = -1;

    if(fullPreconditioner){
      size   = this->getSize();
    }

    const T* xdatax = x.getExtendedData();
    T* rdata = 0;

    if(fullPreconditioner){
      rdata  = r.getData();
    }

    T*       rdatax = r.getExtendedData();

    ContactConstraint<N, T>* constraint =
      (ContactConstraint<N, T>*)this->rowConstraint;

    /*Multiply S*/

    Vector4<T> lx;
    for(int i=0;i<3;i++){
      lx[i] = xdatax[cindex * offset + i];

      if(i>0){
        if(status == Individual){
          if(constraint->frictionStatus[i-1] == Kinetic){
            //Skip active constraint
            lx[i] = (T)0.0;
          }
        }else if(status == AllActive){

        }
      }
    }

    Vector4<T> res;
    if(fullPreconditioner){
      res = -S * lx;
    }else{
      res =  S * lx;
    }

    if(fullPreconditioner){
      /*Multiply AHS*/
      for(int i=0;i<size;i++){
        if(getIndex(i) == Constraint::undefined){
          continue;
        }

        int col = getIndex(i)*3;
        Vector4<T> res2;

        if(transposed){
          res2 = SHAT[i] * lx;
        }else{
          res2 = AHS[i]  * lx;
        }

        for(int j=0;j<3;j++){
          rdata[col+j] += res2[j];
        }
      }
    }

    for(int k=0;k<3;k++){
      if(k>0){
        if(status == Individual){
          if(constraint->frictionStatus[k-1] == Kinetic){
            //Skip active constraint
            res[k] = 0;
          }
        }else if(status == AllActive){
          //do not stop and load the multiplier
        }
      }

      rdatax[cindex * offset + k] += res[k];
    }
  }


  /**************************************************************************/
  /*Performs a multiplication r = Q x*/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::
  multiply(VectorC<T>& r, const VectorC<T>& x)const{
    generalMultiply(r, x, false);
  }

  /**************************************************************************/
  /*Performs a multiplication r = Q^T x*/
  template<int N, class T>
  void ContactBlockConstraint<N, T>::
  multiplyTransposed(VectorC<T>& r, const VectorC<T>& x)const{
    generalMultiply(r, x, true);
  }

  /**************************************************************************/
  template class ContactBlockConstraint<1, float>;
  template class ContactBlockConstraint<2, float>;
  template class ContactBlockConstraint<4, float>;
  template class ContactBlockConstraint<8, float>;

  template class ContactBlockConstraint<1, double>;
  template class ContactBlockConstraint<2, double>;
  template class ContactBlockConstraint<4, double>;
  template class ContactBlockConstraint<8, double>;
}
