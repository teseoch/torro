/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "collision/ConstraintCandidateTriangleEdge.hpp"
#include "collision/constraints/ContactConstraintCR.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "collision/CollisionContext.hpp"
#include "math/IECLinSolve.hpp"
#include "collision/ConstraintSetTriangleEdge.hpp"

#define DEBUG_UPDATE
//#undef DEBUG_UPDATE

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  ConstraintCandidateTriangleEdge<T, CC>::
  ConstraintCandidateTriangleEdge(int eId,
                                  int vId,
                                  CollisionContext<T, CC>* ctx):
    AbstractConstraintCandidate<T, Edge<T>, Vector4<T>, CC>(false, ctx){
    this->setId = eId;
    this->candidateId = vId;

    tangentialDefined = false;
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateTriangleEdge<T, CC>::
  checkCollision(const Edge<T>& p,
                 const Vector4<T>& com,
                 const Quaternion<T>& rot,
                 T* dist,
                 bool* plane){
    int colCode = -1;

    bool rigidEdge   = Mesh::isRigidOrStatic(this->setType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->candidateType);

    T eps = this->ctx->safety_distance + this->ctx->min_edge_distance;

    try{
      if(!rigidEdge && !rigidVertex){
        colCode = intersection(p, this->X0, this->Yu, this->Y0, colPoint, eps,
                               &baryi, &this->Xi, &this->Yi, (T)BARY_EPS, 1,
                               dist);
      }else{
        T root = 0;

        colCode = rigidIntersection(p,  com,   rot,
                                    this->X0, this->comX0, this->rotX0,
                                    this->Yu, this->comYu, this->rotYu,
                                    this->Y0, this->comY0, this->rotY0,
                                    colPoint, eps,
                                    &baryi,
                                    &this->Xi, &this->Yi, (T)BARY_EPS, 1,
                                    rigidEdge, rigidVertex, &root);

        if(colCode == 1){
          this->comXi = this->comX0 * ((T)1.0 - root) + com   * root;
          this->comYi = this->comY0 * ((T)1.0 - root) + this->comYu * root;

          this->rotXi = slerp(this->rotX0, rot,   root);
          this->rotYi = slerp(this->rotY0, this->rotYu, root);
        }
        if(dist) *dist = root;
      }
    }catch(Exception* e){
      delete e;
      return false;
    }

    if(colCode == -1){
      return false;
    }


    /*Distance between ei and pi is most likely zero, provide
      referencevector for correction*/

    if(colCode == 1){
      /*An intersection can be invalid due to flipped normals*/

      /*Check if edges are in a valid configuration*/

      cn = this->Xi.getNormalToVertex(this->Yi);

      T dist = (this->Xi.getCoordinates(baryi) - this->Yi).length();

      if(dist - this->ctx->min_edge_distance > 2e-8){
        //return false;
      }

      return true; /*Valid*/
    }
    return false; /*No coliision*/
  }

  /**************************************************************************/
  /*Evaluate signed distance*/
  template<class T, class CC>
  bool ConstraintCandidateTriangleEdge<T, CC>::
  evaluate(const Edge<T>& edge,
           const Vector4<T>& com,
           const Quaternion<T>& rot,
           Vector4<T>* bb,
           Vector4<T>* __bb,
           bool* updated){

    this->distancec = edge.getPlaneDistance(this->Yu, cn) -
      this->ctx->min_edge_distance;

    if(updated){
      *updated = true;
    }

    this->distancec -= this->ctx->safety_distance;

    if(this->status != Enabled){
      if(this->distancec < (T)0.0){

        return true;
      }
    }else{

    }
    return false;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateTriangleEdge<T, CC>::updateGeometry(){
    this->Yu = this->ctx->mesh->vertices[this->candidateId].displCoord;

    if(Mesh::isRigid(this->ctx->mesh->vertices[this->candidateId].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[this->candidateId];

      this->comYu = this->ctx->mesh->centerOfMassDispl[objectId];
      this->rotYu = this->ctx->mesh->orientationsDispl[objectId];
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateTriangleEdge<T, CC>::
  disableConstraint(){
    if(this->status == Enabled){
      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);

      if(ActiveConstraints<CC>()){
        cc->cacheMultipliers(this->cachedMultipliers,
                             *this->ctx->getSolver()->getx());
      }

      this->status = Disabled;

      tslassert(this->enabledId == cc->id);

      tangentReference[0] = tangentReference[1].set(0,0,0,0);

      int indices[3];
      this->ctx->mesh->getEdgeIndices(this->setId, indices);
      indices[2] = this->candidateId;

      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

      if(rigidEdge){
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[1] += cc->getRow(2,0);
      }else{
        tangentReference[0] += cc->getRow(1,0);
        tangentReference[0] += cc->getRow(1,1);
        tangentReference[1] += cc->getRow(2,0);
        tangentReference[1] += cc->getRow(2,1);
      }

      if(rigidVertex){
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[1] -= cc->getRow(2,2);
      }else{
        tangentReference[0] -= cc->getRow(1,2);
        tangentReference[0] -= cc->getRow(1,3);
        tangentReference[1] -= cc->getRow(2,2);
        tangentReference[1] -= cc->getRow(2,3);
      }

      tangentReference[0].normalize();
      tangentReference[1].normalize();

      tangentialDefined = true;

      this->ctx->getSolver()->removeConstraint(this->enabledId);
      this->status = Disabled;

      delete cc;

      return true;
    }else{
      /*Constraint was already disabled*/
    }
    return false;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateTriangleEdge<T, CC>::
  updateConstraint(const Edge<T>& x,
                   const Edge<T>& p,
                   const Vector4<T>& com,
                   const Quaternion<T>& rot,
                   bool* skipped,
                   EvalStats& stats){
    CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

#ifdef DEBUG_UPDATE
    warning("updateConstraint");
    std::cerr << this->Y0 << std::endl;

    std::cerr << this->Yu << std::endl;
    std::cerr << this->Yi << std::endl;

    std::cerr << this->X0 << std::endl;

    std::cerr << p << std::endl;
    std::cerr << this->Xi << std::endl;

    std::cerr << x << std::endl;
#endif

    Vector4<T> cnOld = this->Xi.getNormalToVertex(this->Yi);

#ifdef DEBUG_UPDATE
    std::cerr << cnOld << std::endl;
#endif

    bool rigidEdge   = Mesh::isRigidOrStatic(this->setType);
    bool rigidVertex = Mesh::isRigidOrStatic(this->candidateType);

    T initialWeight = (T)3.0;

    Vector4<T> Yii;
    Edge<T> Xii;

    Vector4<T> weights;

    T weight1 = (T)1.0;
    T weight2 = (T)0.0;

#ifdef DEBUG_UPDATE
    cc->print(std::cerr);
#endif

    /*Find a configuration between Xi and Xu for which the contact
      normal does not change too much and use this in the
      constraint.*/
    while(true){
      weight1 = (T)1.0 - (T)1.0/(T)initialWeight;
      weight2 = (T)1.0 - weight1;

      Vector4<T> Yii2;
      Edge<T>    Xii2;

      if(rigidEdge){
        Vector4<T>    newComXi = weight1 * this->comXi + weight2 * com;
        Quaternion<T> newRotXi = slerp(this->rotXi, rot, weight2);

        Vector4<T> v11 =
          newComXi + newRotXi*(this->X0.vertex(0)     - this->comX0);
        Vector4<T> v12 =
          newComXi + newRotXi*(this->X0.vertex(1)     - this->comX0);
        Vector4<T> v13 =
          newComXi + newRotXi*(this->X0.faceVertex(0) - this->comX0);
        Vector4<T> v14 =
          newComXi + newRotXi*(this->X0.faceVertex(1) - this->comX0);

        Xii2.set(v11, v12, v13, v14);
      }else{
        Xii2 = interpolateEdges(this->Xi,  p, (T)1, (T)0, weight1);
      }

      if(rigidVertex){
        Vector4<T>    newComYi = weight1 * this->comYi + weight2 * this->comYu;
        Quaternion<T> newRotYi = slerp(this->rotYi, this->rotYu, weight2);

        Vector4<T> v11 = newComYi + newRotYi*(this->Y0  - this->comY0);

        Yii2 = v11;

      }else{
        Yii2 = linterp(weight1, &this->Yi, &this->Yu, (T)1.0, (T)0.0);
      }

      /*This can and will go wrong if the change in the contactnormal
        changes significantly.*/
      cn = Xii2.getNormalToVertex(Yii2);
      weights = Xii2.getBarycentricCoordinates(Yii2);

      if(dot(cn, cnOld) > 0.95){
        /*The new configuration is reasonable, accept this one.*/
        Yii = Yii2;
        Xii = Xii2;
        break;
      }

      /*The current configuration is has a large change compared to
        the previous configuration, choose one closer to the previous
        approximation.*/
      initialWeight *= (T)2.0;
    }

    if(dot(cnOld, cn) < (T)0.9 ||
       (baryi - weights).length() > 1e-4){
      /*The contact normal has changed significantly, we need to
        update the constraint and so we need to update the
        preconditioner and residual vector.*/
      warning("Change in contact normal detected");
#ifdef DEBUG_UPDATE
      warning("p0v0");
      std::cerr << this->X0 << std::endl;
      std::cerr << this->Y0 << std::endl;

      warning("pivi");
      std::cerr << this->Xi << std::endl;
      std::cerr << this->Yi << std::endl;

      warning("XiiYii");
      std::cerr << Xii << std::endl;
      std::cerr << Yii << std::endl;
#endif

      this->Yi = Yii;
      this->Xi = Xii;

      //DO NOT UPDATE Collisionpoint
      baryi = weights;

      if(rigidEdge){
        this->comXi = this->comXi * weight1 + com * weight2;
        this->rotXi = slerp(this->rotXi, rot,   weight2);
      }

      if(rigidVertex){
        this->comYi = this->comYi * weight1 + this->comYu * weight2;
        this->rotYi = slerp(this->rotYi, this->rotYu, weight2);
      }

      int indices[3];
      this->ctx->mesh->getEdgeIndices(this->setId, indices);
      indices[2] = this->candidateId;

      T dt = this->ctx->getDT();

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;
#endif

#if 1
      /*Check if new edges project on each other*/
      if(!this->Xi.barycentricOnEdgeBand(weights,
                                         this->ctx->distance_epsilon *
                                         (T)2.0 * (T)0.0)){
        /*Could be a source of potential errors!!*/
        disableConstraint();
        *skipped = true;
        return;
      }
#endif

      if(cc->getStatus() != Active){
        disableConstraint();
        *skipped = true;
        return;
      }

      /*Extract tangent vectors*/
      Vector4<T> t1, t2;

      bool bodyEdge    = true;
      bool bodyVertex  = true;
      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyVertex = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

#ifdef DEBUG_UPDATE
      warning("coordinates");
      std::cerr << Xii.getCoordinates(weights) << std::endl;
#endif

      if(ClampsWeights<CC>()){
        //weights = this->X0.clampWeights(weights, WEIGHT_MIN, WEIGHT_MAX);
      }else{
        weights =  weights;
      }

#ifdef DEBUG_UPDATE
      warning("weights of point");
      std::cerr << weights << std::endl;

      warning("coordinates");
      std::cerr << Xii.getCoordinates(weights) << std::endl;
#endif

      Vector4<T> re1, rv2;

      t1 = t2 *= 0.0;

      if(bodyEdge){
        if(rigidEdge){
          re1  = Xii.getCoordinates(weights) - this->comXi;
          t1 = cc->getRow(1, 0);
          t2 = cc->getRow(2, 0);
        }else{
          t1 = cc->getRow(1, 0) + cc->getRow(1,1);
          t2 = cc->getRow(2, 0) + cc->getRow(2,1);
        }
      }

      if(bodyVertex){
        t1 = t2 *= 0.0;
        if(rigidVertex){
          rv2  = Yii - this->comYi;
          t1 = -cc->getRow(1, 2);
          t2 = -cc->getRow(2, 2);
        }else{
          t1 = -cc->getRow(1, 2) - cc->getRow(1,3);
          t2 = -cc->getRow(2, 2) - cc->getRow(2,3);
        }
      }

#ifdef DEBUG_UPDATE
      warning("t1");
      std::cerr << t1 << std::endl;
      warning("t2");
      std::cerr << t2 << std::endl;
#endif

      t1.normalize();
      t2.normalize();

      /*Re-align tangent vectors with contact normal*/
      Vector4<T> tt1, tt2;
      tt2 = cross(t1, cn).normalized();
      if(dot(tt2, t2) < 0){
        tt2 *= -1;
      }

      tt1 = cross(t2, cn).normalized();
      if(dot(tt1, t1) < 0){
        tt1 *= -1;
      }

      t1 = tt1;
      t2 = tt2;

#ifdef DEBUG_UPDATE
      warning("t1");
      std::cerr<< t1 << std::endl;
      warning("t2");
      std::cerr << t2 << std::endl;
#endif

      //cc->status = Inactive;
      //cc->frictionStatus[0] = Kinetic;
      //cc->frictionStatus[1] = Kinetic;

      /*Compute normal and tangential distances*/
      T totalDistance =
        (-this->Xi.getPlaneDistance(this->X0.vertex(0), cn)*baryi[0] -
          this->Xi.getPlaneDistance(this->X0.vertex(1), cn)*baryi[1] +
          this->Xi.getPlaneDistance(this->Y0,           cn)) -
        this->ctx->safety_distance - this->ctx->distance_epsilon * 2 -
        this->ctx->min_edge_distance;

#ifdef DEBUG_UPDATE
      warning("update constraint");
      warning("totaldistance = %10.10e", totalDistance);
      warning("distancec = %10.10e", this->distancec);
      warning("distance0 = %10.10e", this->distance0);
#endif

#if 0
      /*Measure the error between the desired and current normal
        distance of the constraint. Use this error distance to update
        the constraint. This errordistance is then weighted/averaged
        in order to move the current configuration closer to the
        desired solution*/
      T errorDist = this->ctx->safety_distance + this->ctx->distance_epsilon * 2 + this->ctx->min_edge_distance*0.0)*(T)1.0 - this->distancec;

      //if(offEdge){
      // totalDistance = (d5+d6-d7-d8) - offEdgeEps;
      //errorDist = offEdgeEps - this->distancec;
      //}

      //errorDist = Sign(errorDist)*Max(Abs(errorDist), this->ctx->safety_distance + this->ctx->distance_epsilon*2);

      //T sgn = Sign(errorDist);
      //T mag = Abs(errorDist);

      //errorDist = sgn * Min(mag, (T)1e-3);

      totalDistance = (cc->getConstraintValue(0) + errorDist*(T)0.25);

      warning("errordist = %10.10e", errorDist);
      warning("totaldistance = %10.10e", totalDistance);
#endif

      /*T1*/
      T totalDistanceT1 =
        (dot(t1, Xii.vertex(0) - this->X0.vertex(0)) * baryi[0] +
         dot(t1, Xii.vertex(1) - this->X0.vertex(1)) * baryi[1] -
         dot(t1, Yii           - this->Y0));

      /*T2*/
      T totalDistanceT2 =
        (dot(t2, Xii.vertex(0) - this->X0.vertex(0)) * baryi[0] +
         dot(t2, Xii.vertex(1) - this->X0.vertex(1)) * baryi[1] -
         dot(t2, Yii           - this->Y0));

      /*Update constraint with new normals and tangential vectors and
        updated distances.*/
      if(bodyEdge){
        if(rigidEdge){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(t1*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(t2*dt, totalDistanceT2, 2, 0, velIndex);

          cc->setRow(cross(re1, cn)*dt, totalDistance,   0, 1, velIndex+1);
          cc->setRow(cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
          cc->setRow(cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*baryi[0]*dt, totalDistance,   0, 0, velIndex);
          cc->setRow(t1*baryi[0]*dt, totalDistanceT1, 1, 0, velIndex);
          cc->setRow(t2*baryi[0]*dt, totalDistanceT2, 2, 0, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(cn*baryi[1]*dt, totalDistance,   0, 1, velIndex);
          cc->setRow(t1*baryi[1]*dt, totalDistanceT1, 1, 1, velIndex);
          cc->setRow(t2*baryi[1]*dt, totalDistanceT2, 2, 1, velIndex);
        }
      }
      if(bodyVertex){
        if(rigidVertex){
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-cn*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);

          cc->setRow(-cross(rv2, cn)*dt, totalDistance,   0, 3, velIndex+1);
          cc->setRow(-cross(rv2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
          cc->setRow(-cross(rv2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-cn*dt, totalDistance,   0, 2, velIndex);
          cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
          cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);
        }
      }
    }else{
      warning("no change in contact normal");
      warning("weights of point");
      std::cerr << weights << std::endl;

#if 1
      /*Check if new edges project on each other*/
      if(!this->Xi.barycentricOnEdgeBand(weights,
                                         this->ctx->distance_epsilon *
                                         (T)2.0 * (T)0.0) ){
        /*Could be a source of potential errors!!*/
        disableConstraint();
        *skipped = true;
        //updateInactiveConstraint(c, x, p, com, rot);
        //return;

        //offEdge = true;
        //if(this->distancec > offEdgeEps){
          //*skipped = true;
        return;
        //}

      }
#endif
      /*The normal has not changed significantly, just update the
        constraint normal distance*/

      CC* cc = (CC*)this->ctx->mat->getConstraint(this->enabledId);

      if(cc->status != Active){
        /*Do not update inactive constraint this way*/
        //return;
      }

      /*Error wrt desired distance*/
      T errorDist = (T)(this->ctx->safety_distance + this->ctx->distance_epsilon*2) - this->distancec;

      //errorDist = Sign(errorDist)*Max(Abs(errorDist*(T)0.45), this->ctx->safety_distance + this->ctx->distance_epsilon*2);
      //errorDist = Sign(errorDist)*Max(Abs(errorDist), this->ctx->safety_distance + this->ctx->distance_epsilon*2);

      T totalDistance = cc->getConstraintValue(0) - (T)(errorDist*0.25);

      warning("update distance");
      warning("totaldistance = %10.10e", totalDistance);
      warning("distancec = %10.10e", this->distancec);
      warning("distance0 = %10.10e", this->distance0);
      warning("errdist = %10.10e", errorDist);


      cc->getConstraintValue(0) = totalDistance;
    }

    stats.n_geometry_check++;

    /*Update constraint*/
    cc->init(this->ctx->getMatrix());
    cc->update(*this->ctx->getSolver()->getb(),
               *this->ctx->getSolver()->getx(),
               *this->ctx->getSolver()->getb2());
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateTriangleEdge<T, CC>::
  updateInitial(const Edge<T>& x,
                const Edge<T>& p,
                const Vector4<T>& com,
                const Quaternion<T>& rot,
                bool* skipped,
                EvalStats& stats){
    Vector4<T> weights = this->Xi.getBarycentricCoordinates(this->Yi);

    this->Y0 = this->Yu;

    this->comY0 = this->comYu;
    this->rotY0 = this->rotYu;

    this->X0 = p;

    this->comX0 = com;
    this->rotX0 = rot;

    this->distance0 = this->distancec;
    //intersection0 = intersectionc;

    if(this->status == Enabled){
      /*Check if new edges project on each other*/
      if(!this->Xi.barycentricOnEdgeBand(weights,
                                         this->ctx->distance_epsilon*
                                         (T)2.0 * (T)0.0) ){
        /*Could be a source of potential errors!!*/
        disableConstraint();
        *skipped = true;
      }
    }
  }

  /**************************************************************************/
  template<class T, class CC>
  bool ConstraintCandidateTriangleEdge<T, CC>::
  enableConstraint(const Edge<T>& s,
                   const Edge<T>& p,
                   Vector4<T>* __gap){
    if(this->status == Enabled){
      return false;
    }else{
      CC* cc = new CC();
      cc->sourceType = 8;
      cc->sourceA = this->setId;
      cc->sourceB = this->candidateId;

      try{
        /*Compute barycentric coordinates on both edges*/
        Vector4<T> weights =  baryi;
        if(ClampsWeights<CC>()){
          weights =  baryi;
        }else{
          weights =  baryi;
        }

        cc->setMu((T)MU * 0.0);

        int indices[3];
        this->ctx->mesh->getEdgeIndices(this->setId, indices);
        indices[2] = this->candidateId;

        T dt = this->ctx->getDT();

        bool bodyEdge    = true;
        bool bodyVertex  = true;
        bool rigidEdge   = false;
        bool rigidVertex = false;

        for(int i=0;i<2;i++){
          if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
            bodyEdge = false;
          }

          if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
            rigidEdge = true;
          }
        }

        for(int i=2;i<3;i++){
          if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
            bodyVertex = false;
          }

          if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
            rigidVertex = true;
          }
        }

        T totalDistance =
          (-this->Xi.getPlaneDistance(this->X0.vertex(0), cn)*weights[0] -
            this->Xi.getPlaneDistance(this->X0.vertex(1), cn)*weights[1] +
            this->Xi.getPlaneDistance(this->Y0,           cn))
          - this->ctx->safety_distance - this->ctx->distance_epsilon*2 -
          this->ctx->min_edge_distance;


        if(Abs(totalDistance) > 1E-2){
          /*When a constraint is enabled and its corresponding
            distance is too large, there can be a problem.*/
          warning("dump data");
          std::cerr << s << std::endl;
          std::cerr << p << std::endl;
          std::cerr << this->Y0 << std::endl;
          std::cerr << this->Yu << std::endl;
          std::cerr << this->Xi << std::endl;
          std::cerr << this->Yi << std::endl;

          PRINT(s);
          PRINT(p);
          //PRINT(e0);
          //PRINT(eu);
          std::cerr << weights << std::endl;

          warning("too large distance");


          warning("total distance = %10.10e", totalDistance);

        }

        Vector4<T> t1, t2;
        Vector4<T> re1, rv2;

        if(HasFrictionCone<CC>()){
          if(tangentialDefined){
            t1 = -tangentReference[0];

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;
            tt2 = cross(t1,  cn).normalized();
            tt1 = cross(tt2, cn).normalized();

            t1 = tt1;
            t2 = tt2;

            if(bodyEdge){
              if(rigidEdge){
                re1  = this->Xi.getCoordinates(baryi) - this->comXi;
              }
            }

            if(bodyVertex){
              if(rigidVertex){
                rv2  = this->Yi - this->comYi;
              }
            }
            cc->tangent1 = t1;
            cc->tangent2 = t2;
            cc->normal   = cn;
          }else{
            Vector4<T> rnd(genRand<T>(), genRand<T>(), genRand<T>(), 0);

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;

            tt2 = cross(rnd, cn).normalized();
            tt1 = cross(tt2, cn).normalized();

            t1 = tt1;
            t2 = tt2;

            if(bodyEdge){
              if(rigidEdge){
                re1  = this->Xi.getCoordinates(baryi) - this->comXi;
              }
            }

            if(bodyVertex){
              if(rigidVertex){
                rv2  = this->Yi - this->comYi;
              }
            }

            cc->tangent1 = t1;
            cc->tangent2 = t2;
            cc->normal   = cn;
          }
        }else{
          if(ActiveConstraints<CC>()){
            /*In case of ICA, all constraints are removed and newly
              added. Using old tangent vectors wihout re-aligning them
              is not correct.*/

            //tangentialDefined = false;
          }

          if(tangentialDefined){
            t1 = -tangentReference[0];
            t2 = -tangentReference[1];

            /*Align updated tangential vectors with old tangential vectors*/
            Vector4<T> tt1, tt2;
            tt2 = cross(t1, cn).normalized();
            if(dot(tt2, t2) < 0){
              tt2 *= -1;
            }

            tt1 = cross(t2, cn).normalized();
            if(dot(tt1, t1) < 0){
              tt1 *= -1;
            }

            t1 = tt1;
            t2 = tt2;

            if(rigidEdge){
              re1  = this->Xi.getCoordinates(baryi) - this->comXi;
            }

            if(rigidVertex){
              rv2  = this->Yi - this->comYi;
            }
          }else{
            Vector4<T> vel;
            Vector<T>* vx = this->ctx->getSolver()->getx();

            if(bodyEdge){
              if(rigidEdge){
                int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;
                tslassert(vel_index != -1);

                vel[0] += (*vx)[vel_index+0];
                vel[1] += (*vx)[vel_index+1];
                vel[2] += (*vx)[vel_index+2];

                Vector4<T> ang_vel;

                ang_vel[0] = (*vx)[vel_index+3];
                ang_vel[1] = (*vx)[vel_index+4];
                ang_vel[2] = (*vx)[vel_index+5];

                /*Compute angular displacement*/
                re1  = this->Xi.getCoordinates(baryi) - this->comXi;
                ang_vel = cross(ang_vel, re1);

                vel += ang_vel;
              }else{
                for(int l=0;l<2;l++){
                  int vel_index =
                    this->ctx->mesh->vertices[indices[l+0]].vel_index;

                  tslassert(vel_index != -1);

                  vel[0] += (*vx)[vel_index+0] * weights[l];
                  vel[1] += (*vx)[vel_index+1] * weights[l];
                  vel[2] += (*vx)[vel_index+2] * weights[l];
                }
              }
            }

            if(bodyVertex){
              if(rigidVertex){
                int vel_index = this->ctx->mesh->vertices[indices[2]].vel_index;

                tslassert(vel_index != -1);

                vel[0] -= (*vx)[vel_index+0];
                vel[1] -= (*vx)[vel_index+1];
                vel[2] -= (*vx)[vel_index+2];

                Vector4<T> ang_vel;

                ang_vel[0] = (*vx)[vel_index+3];
                ang_vel[1] = (*vx)[vel_index+4];
                ang_vel[2] = (*vx)[vel_index+5];

                /*Compute angular displacement*/

                rv2  = this->Yi - this->comYi;
                ang_vel = cross(ang_vel, rv2);

                vel -= ang_vel;
              }else{
                for(int l=0;l<1;l++){
                  int vel_index =
                    this->ctx->mesh->vertices[indices[l+2]].vel_index;

                  tslassert(vel_index != -1);

                  vel[0] -= (*vx)[vel_index+0];
                  vel[1] -= (*vx)[vel_index+1];
                  vel[2] -= (*vx)[vel_index+2];
                }
              }
            }

            Vector4<T> vn = cross(vel, cn);

            /*Use velocity to align tangent vectors*/
            if(vn.length() < 1e-6){
              /*Choose some arbitrary direction*/
              Vector4<T> vec(1, 1, 1, 0);
              Vector4<T> vn2 = cross(vec, cn);

              if(vn2.length() < 1e-6){
                vec.set(-1, 1, 1, 0);
                vn2 = cross(vec, cn);
              }

              t2 = vn2.normalized();
              t1 = cross(t2, cn).normalized();
            }else{
              t2 = vn.normalized();
              t1 = cross(t2, cn).normalized();
            }
          }
        }

        T totalDistanceT1 =
          (dot(t1, this->Xi.vertex(0) - this->X0.vertex(0)) * weights[0] +
           dot(t1, this->Xi.vertex(1) - this->X0.vertex(1)) * weights[1] -
           dot(t1, this->Yi           - this->Y0          ));

        T totalDistanceT2 =
          (dot(t2, this->Xi.vertex(0) - this->X0.vertex(0)) * weights[0] +
           dot(t2, this->Xi.vertex(1) - this->X0.vertex(1)) * weights[1] -
           dot(t2, this->Yi           - this->Y0          ));

        if(HasFrictionCone<CC>()){
          //cc->kineticVector.set(0, -totalDistanceT1, -totalDistanceT2, 0);
          //cc->kineticVector.normalize();
        }

        if(bodyEdge){
          if(rigidEdge){
            int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(cn*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(t1*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(t2*dt, totalDistanceT2, 2, 0, velIndex);

            cc->setRow(cross(re1, cn)*dt, totalDistance,   0, 1, velIndex+1);
            cc->setRow(cross(re1, t1)*dt, totalDistanceT1, 1, 1, velIndex+1);
            cc->setRow(cross(re1, t2)*dt, totalDistanceT2, 2, 1, velIndex+1);
          }else{
            int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

            cc->setRow(cn*weights[0]*dt, totalDistance,   0, 0, velIndex);
            cc->setRow(t1*weights[0]*dt, totalDistanceT1, 1, 0, velIndex);
            cc->setRow(t2*weights[0]*dt, totalDistanceT2, 2, 0, velIndex);

            velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

            cc->setRow(cn*weights[1]*dt, totalDistance,   0, 1, velIndex);
            cc->setRow(t1*weights[1]*dt, totalDistanceT1, 1, 1, velIndex);
            cc->setRow(t2*weights[1]*dt, totalDistanceT2, 2, 1, velIndex);
          }
        }
        if(bodyVertex){
          if(rigidVertex){
            int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(-cn*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);

            cc->setRow(-cross(rv2, cn)*dt, totalDistance,   0, 3, velIndex+1);
            cc->setRow(-cross(rv2, t1)*dt, totalDistanceT1, 1, 3, velIndex+1);
            cc->setRow(-cross(rv2, t2)*dt, totalDistanceT2, 2, 3, velIndex+1);
          }else{
            int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

            cc->setRow(-cn*dt, totalDistance,   0, 2, velIndex);
            cc->setRow(-t1*dt, totalDistanceT1, 1, 2, velIndex);
            cc->setRow(-t2*dt, totalDistanceT2, 2, 2, velIndex);
          }
        }
      }catch(DegenerateCaseException* e){
        std::cout << e->getError();
        delete cc;
        return false;
      }

      cc->init(this->ctx->getMatrix());

      cc->status = Active;
      //cc->frictionStatus[0] = Static;
      //cc->frictionStatus[1] = Static;

      this->enabledId = this->ctx->getSolver()->addConstraint(cc);
      cc->id = this->enabledId;

      if(ActiveConstraints<CC>()){
        cc->resetMultipliers(*this->ctx->getSolver()->getx(),
                             this->cachedMultipliers);
      }else{
        Vector4<T> zero;
        cc->resetMultipliers(*this->ctx->getSolver()->getx(), zero);
      }
      this->status = Enabled;
    }
    return true;
  }

  /**************************************************************************/
  template<class T, class CC>
  void ConstraintCandidateTriangleEdge<T, CC>::
  recompute(AbstractConstraintSet<T, Edge<T>, Vector4<T>, CC>* c,
            Edge<T>& p,
            Vector4<T>& com,
            Quaternion<T>& rot){

    this->Y0 = this->ctx->mesh->vertices[this->candidateId].coord;
    this->Yi = this->Y0;
    this->Yu = this->Y0;

    this->comY0 = this->comYu = this->comYi = Vector4<T>();
    this->rotY0 = this->rotYu = this->rotYi = Quaternion<T>();

    if(Mesh::isRigid(this->ctx->mesh->vertices[this->candidateId].type)){
      int objectId = this->ctx->mesh->vertexObjectMap[this->candidateId];

      this->comY0 = this->comYu = this->comYi =
        this->ctx->mesh->centerOfMass[objectId];

      this->rotY0 = this->rotYu = this->rotYi =
        this->ctx->mesh->orientations[objectId];
    }

    this->X0 = p;
    this->Xi = p;

    this->comX0 = this->comXi = com;
    this->rotX0 = this->rotXi = rot;

    cn = this->X0.getNormalToVertex(this->Y0);

    baryi = this->X0.getBarycentricCoordinates(this->Y0);

    if(this->status == Enabled){
      /*Distance here IS positive by definition*/

      this->distance0 = this->distancec =
        this->X0.getPlaneDistance(this->Y0, cn) -
        this->ctx->safety_distance - this->ctx->min_edge_distance;

      if(this->distance0 < 0.0){
        //error("Initial distance negative %10.10e", this->distance0);
      }

      CC* cc = (CC*)this->ctx->getMatrix()->getConstraint(this->enabledId);

      if(cc->status == Inactive){
        if(this->distancec > this->ctx->safety_distance*1.1){
          /*Constraint is inactive and there is no collision -> disable.*/
          disableConstraint();
          return;
        }else{
          /*Constraint was disabled, but there is a collision -> reenable*/
          //cc->status = Active;
        }
      }

      if(!this->X0.barycentricOnEdge(baryi, (T)BARY_EPS * 0.0) ){
        disableConstraint();
        return;
      }

      Vector4<T> weights =  baryi;
      if(ClampsWeights<CC>()){
        //Vector4<T> weights1 = this->X0.clampWeights(baryi1, WEIGHT_MIN, WEIGHT_MAX);
        //Vector4<T> weights2 = e0.clampWeights(baryi2, WEIGHT_MIN, WEIGHT_MAX);
        weights =  baryi;
      }else{
        weights =  baryi;
      }

      cc->setMu((T)MU * 0.0);

      if(HasFrictionCone<CC>()){
        cc->cumulativeVector = cc->kineticVector * (T)60;

        if(cc->cumulativeVector.length() > 1e-6){
          cc->cumulativeVector.normalize();
        }

        if(cc->frictionStatus[0] != Kinetic){
          cc->cumulativeVector.set(0,0,0,0);
          cc->kineticVector.set(0,0,0,0);
          cc->acceptedKineticVector.set(0,0,0,0);
        }
      }

      int indices[3];
      this->ctx->mesh->getEdgeIndices(this->setId, indices);
      indices[2] = this->candidateId;

      T dt = this->ctx->getDT();

      bool bodyEdge    = true;
      bool bodyVertex  = true;
      bool rigidEdge   = false;
      bool rigidVertex = false;

      for(int i=0;i<2;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyEdge = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidEdge = true;
        }
      }

      for(int i=2;i<3;i++){
        if(Mesh::isStatic(this->ctx->mesh->vertices[indices[i]].type)){
          bodyVertex = false;
        }

        if(Mesh::isRigid(this->ctx->mesh->vertices[indices[i]].type)){
          rigidVertex = true;
        }
      }

      T totalDistance =
        (-this->Xi.getPlaneDistance(this->X0.vertex(0), cn) * weights[0] -
          this->Xi.getPlaneDistance(this->X0.vertex(1), cn) * weights[1] +
          this->Xi.getPlaneDistance(this->Y0,           cn)) -
        this->ctx->safety_distance - this->ctx->distance_epsilon*2 -
        this->ctx->min_edge_distance;

      Vector4<T> t1, t2;
      Vector4<T> re1, rv2;

      if(HasFrictionCone<CC>()){
        if(bodyEdge){
          if(rigidEdge){
            t1 -= cc->getRow(1,0);
            t2 -= cc->getRow(2,0);
            re1 = this->Xi.getCoordinates(baryi) - this->comXi;
          }else{
            t1 -= cc->getRow(1,0);
            t1 -= cc->getRow(1,1);
            t2 -= cc->getRow(2,0);
            t2 -= cc->getRow(2,1);
          }
        }

        if(bodyVertex){
          if(rigidVertex){
            t1 += cc->getRow(1,2);
            t2 += cc->getRow(2,2);
            rv2  = this->Yi - this->comYi;
          }else{
            t1 += cc->getRow(1,2);
            t1 += cc->getRow(1,3);
            t2 += cc->getRow(2,2);
            t2 += cc->getRow(2,3);
          }
        }

        t1 *= -1;
        t2 *= -1;

        t1.normalize();
        t2.normalize();

        Vector4<T> tt1, tt2;
        tt2 = cross(t1, cn).normalized();
        tt1 = cross(tt2, cn).normalized();

        t1 = -tt1;
        t2 = -tt2;

        cc->tangent1 = t1;
        cc->tangent2 = t2;
        cc->normal   = cn;
      }else{
        Vector4<T> vel(0,0,0,0);
        Vector<T>* vx = this->ctx->getSolver()->getx();

        if(bodyEdge){
          if(rigidEdge){
            int vel_index = this->ctx->mesh->vertices[indices[0]].vel_index;

            vel[0] += (*vx)[vel_index+0];
            vel[1] += (*vx)[vel_index+1];
            vel[2] += (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            re1 = this->Xi.getCoordinates(baryi) - this->comXi;
            ang_vel = cross(ang_vel, re1);

            vel += ang_vel;
          }else{
            for(int l=0;l<2;l++){
              int vel_index = this->ctx->mesh->vertices[indices[l+0]].vel_index;

              vel[0] += (*vx)[vel_index+0] * weights[l];
              vel[1] += (*vx)[vel_index+1] * weights[l];
              vel[2] += (*vx)[vel_index+2] * weights[l];
            }
          }
        }

        if(bodyVertex){
          if(rigidVertex){
            int vel_index = this->ctx->mesh->vertices[indices[2]].vel_index;

            vel[0] -= (*vx)[vel_index+0];
            vel[1] -= (*vx)[vel_index+1];
            vel[2] -= (*vx)[vel_index+2];

            Vector4<T> ang_vel;

            ang_vel[0] = (*vx)[vel_index+3];
            ang_vel[1] = (*vx)[vel_index+4];
            ang_vel[2] = (*vx)[vel_index+5];

            /*Compute angular displacement*/
            rv2  = this->Yi - this->comYi;
            ang_vel = cross(ang_vel, rv2);

            vel -= ang_vel;
          }else{
            for(int l=0;l<1;l++){
              int vel_index = this->ctx->mesh->vertices[indices[l+2]].vel_index;
              vel[0] -= (*vx)[vel_index+0];
              vel[1] -= (*vx)[vel_index+1];
              vel[2] -= (*vx)[vel_index+2];
            }
          }
        }

        ////////////////
        t1 = t2.set(0,0,0,0);

        if(rigidEdge){
          t1 -= cc->getRow(1,0);
          t2 -= cc->getRow(2,0);
        }else{
          t1 -= cc->getRow(1,0);
          t1 -= cc->getRow(1,1);
          t2 -= cc->getRow(2,0);
          t2 -= cc->getRow(2,1);
        }

        if(rigidVertex){
          t1 += cc->getRow(1,2);
          t2 += cc->getRow(2,2);
        }else{
          t1 += cc->getRow(1,2);
          t1 += cc->getRow(1,3);
          t2 += cc->getRow(2,2);
          t2 += cc->getRow(2,3);
        }

        t1 *= -1;
        t2 *= -1;

        t1.normalize();
        t2.normalize();

        Vector4<T> vn = cross(vel, cn);

        START_DEBUG;
        message("edge: t1, t2");
        std::cout << t1;
        std::cout << t2;

        std::cout << vel;

        std::cout << vn;
        END_DEBUG;

        /*Re-align with velocity and/or normal*/
        if(vn.length() < 1e-6){
          DBG(message("realign without velocity"));
          /*Fixed point, only align with normal*/
          Vector4<T> tt1, tt2;
          tt2 = cross(t1, cn).normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
            DBG(message("flip tt2"));
          }

          tt1 = cross(t2, cn).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
            DBG(message("flip tt1"));
          }

          t1 = tt1;
          t2 = tt2;
        }else{
          DBG(message("realign with velocity"));
          Vector4<T> tt1, tt2;
          tt2 = vn.normalized();
          if(dot(tt2, t2) < 0){
            tt2 *= -1;
            DBG(message("flip tt2"));
          }

          tt1 = cross(tt2, cn).normalized();
          if(dot(tt1, t1) < 0){
            tt1 *= -1;
            DBG(message("flip tt1"));
          }
          t1 = tt1;
          t2 = tt2;
        }
        START_DEBUG;
        message("edge: after realign, %10.10e, %10.10e", dot(t1, vn), dot(t2,vn));
        std::cout << t1;
        std::cout << t2;
        END_DEBUG;
      }

      if(bodyEdge){
        if(rigidEdge){
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(t1*dt,             0, 1, 0, velIndex);
          cc->setRow(t2*dt,             0, 2, 0, velIndex);

          cc->setRow(cross(re1, cn)*dt, totalDistance, 0, 1, velIndex+1);
          cc->setRow(cross(re1, t1)*dt,             0, 1, 1, velIndex+1);
          cc->setRow(cross(re1, t2)*dt,             0, 2, 1, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[0]].vel_index/3;

          cc->setRow(cn*weights[0]*dt, totalDistance, 0, 0, velIndex);
          cc->setRow(t1*weights[0]*dt,             0, 1, 0, velIndex);
          cc->setRow(t2*weights[0]*dt,             0, 2, 0, velIndex);

          velIndex = this->ctx->mesh->vertices[indices[1]].vel_index/3;

          cc->setRow(cn*weights[1]*dt, totalDistance, 0, 1, velIndex);
          cc->setRow(t1*weights[1]*dt,             0, 1, 1, velIndex);
          cc->setRow(t2*weights[1]*dt,             0, 2, 1, velIndex);
        }
      }
      if(bodyVertex){
        if(rigidVertex){
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-cn*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(-t1*dt,             0, 1, 2, velIndex);
          cc->setRow(-t2*dt,             0, 2, 2, velIndex);

          cc->setRow(-cross(rv2, cn)*dt, totalDistance, 0, 3, velIndex+1);
          cc->setRow(-cross(rv2, t1)*dt,             0, 1, 3, velIndex+1);
          cc->setRow(-cross(rv2, t2)*dt,             0, 2, 3, velIndex+1);
        }else{
          int velIndex = this->ctx->mesh->vertices[indices[2]].vel_index/3;

          cc->setRow(-cn*dt, totalDistance, 0, 2, velIndex);
          cc->setRow(-t1*dt,             0, 1, 2, velIndex);
          cc->setRow(-t2*dt,             0, 2, 2, velIndex);
        }
      }

      cc->init(this->ctx->getMatrix());


      cc->update(*this->ctx->getSolver()->getb(),
                 *this->ctx->getSolver()->getx(),
                 *this->ctx->getSolver()->getb2());


    }else{
      //bool degenerate = false;
      this->distance0 = this->distancec =
        this->X0.getPlaneDistance(this->Y0, cn) - this->ctx->safety_distance -
        this->ctx->min_edge_distance;

      if(this->distance0 < 0.0){
        //error("Initial distance negative %10.10e", this->distance0);
      }
    }
  }

  /**************************************************************************/
  template class
  ConstraintCandidateTriangleEdge<float, ContactConstraintCR<2, float> >;
  template class
  ConstraintCandidateTriangleEdge<double, ContactConstraintCR<2, double> >;
}
