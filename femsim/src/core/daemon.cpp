/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/daemon.hpp"
#include "core/tsldefs.hpp"
#ifdef _WIN32

#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

namespace tsl{

  int daemon_process = 0;

  /**************************************************************************/
  int continue_as_daemon_process(){
    pid_t pid;

    pid = fork();

    if(pid){
      daemon_process = 0;
      printf("Daemon: %d\n", pid);
      return 0;
    }

    setsid();
    umask(0);

    daemon_process = 1;
    return 1;
  }

  /**************************************************************************/
  int is_daemon(){
    return daemon_process;
  }

  /**************************************************************************/
  void redirect_std_file_descriptors(){
    /*Redirect stdout*/
    char buffer[1024];
    sprintf(buffer, "%d.stdout.txt", getpid());
    FILE* f1 = freopen(buffer, "w", stdout);

    /*Redirect stderr*/
    sprintf(buffer, "%d.stderr.txt", getpid());
    FILE* f2 = freopen(buffer, "w", stderr);

    if(f1 != stdout){
      error("Error redirecting stdout");
    }
    if(f2 != stderr){
      error("Error redirecting stderr");
    }
  }

  /**************************************************************************/
  void redirect_std_file_descriptors_null(){
    FILE* f1 = freopen("/dev/null", "w", stdout);
    FILE* f2 = freopen("/dev/null", "w", stderr);

    if(f1 != stdout){
      error("Error redirecting stdout");
    }
    if(f2 != stderr){
      error("Error redirecting stderr");
    }
  }
}
