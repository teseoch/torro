/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/Exception.hpp"
#include <sstream>

namespace tsl{
  /**************************************************************************/
  const std::string TimeOutException::getError() const{
    std::stringstream stream;

    stream << "TimeOutException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg;
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string CUDAException::getError() const{
    std::stringstream stream;

    stream << "CUDAException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg;
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string FileNotFoundException::getError() const{
    std::stringstream stream;

    stream << "FileNotFoundException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t file " << msg << " not found.";
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string ThreadPoolException::getError() const{
    std::stringstream stream;

    stream << "ThreadPoolException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg << ".";
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string MathException::getError() const{
    std::stringstream stream;

    stream << "MathException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg << ".";
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string SolutionNotFoundException::getError() const{
    std::stringstream stream;

    stream << "SolutionNotFoundException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg << ".";
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string AlgException::getError() const{
    std::stringstream stream;

    stream << "AlgException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg << ".";
    }
    stream << std::endl;
    return stream.str();
  }

  /**************************************************************************/
  const std::string DegenerateCaseException::getError() const{
    std::stringstream stream;

    stream << "DegenerateCaseException occured in " << file << ":" << line;
    if(msg != 0){
      stream << "\t|\t" << msg << ".";
    }
    stream << std::endl;
    return stream.str();
  }
}
