/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/Timer.hpp"

namespace tsl{
  /**************************************************************************/
  Timer::Timer(){
    gettimeofday(&t1, 0);
    gettimeofday(&t2, 0);
  }

  /**************************************************************************/
  Timer::~Timer(){
  }

  /**************************************************************************/
  void Timer::start(){
    gettimeofday(&t1,0);
  }

  /**************************************************************************/
  void Timer::stop(){
    gettimeofday(&t2,0);
    timersub(&t2, &t1, &diff);
    usec = diff.tv_sec * 1000000 + diff.tv_usec;
  }

  /**************************************************************************/
  std::ostream& operator<<(std::ostream& os, const Timer& t){
    os << t.usec << " usec elapsed" << std::endl;
    return os;
  }
}
