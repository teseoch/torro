/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/BenchmarkTimer.hpp"

namespace tsl{
  /**************************************************************************/
  void BenchmarkTimer::start(const char* timerName){
    std::map<std::string, timer>::iterator it;
    std::string key(timerName);

    it = timeMap.find(key);
    if(it == timeMap.end()){
      /*Timer does not exist*/
      timer t;
      timerclear(&t.total_time);
      timerclear(&t.last_time);
      t.n = 0;
      t.state = 0;
      timeMap[key] = t;
    }
    /*The timer must be in a stoppped state*/
    if(timeMap[key].state != 0){
      error("timer %s not stopped", timerName);
    }
    tslassert(timeMap[key].state == 0);

    timeMap[key].state = 1;
    gettimeofday(&(timeMap[key].last_time), 0);
  }

  /**************************************************************************/
  void BenchmarkTimer::stop(const char* timerName){
    struct timeval now;
    struct timeval diff;
    gettimeofday(&now, 0);

    std::string key(timerName);

    /*Timer must be in a running state in order to stop*/
    tslassert(timeMap[key].state == 1);

    std::map<std::string, timer>::iterator it = timeMap.find(key);
    if(it == timeMap.end()){
      error("Trying to access non existing timer");
    }

    timersub(&now, &(timeMap[key].last_time), &diff);
    timeradd(&diff, &(timeMap[key].total_time),
             &(timeMap[key].total_time));
    timeMap[key].n++;
    timeMap[key].state = 0;
  }

  /**************************************************************************/
  long BenchmarkTimer::getAverageTimeUSec(const char* timerName){
    long usec = 0;

    std::string key(timerName);

    std::map<std::string, timer>::iterator it = timeMap.find(key);
    if(it == timeMap.end()){
      error("timer %s not found", timerName);
      return 0;
    }else{
      usec = timeMap[key].total_time.tv_sec * 1000000 +
        timeMap[key].total_time.tv_usec;
      usec = (long)((double) usec/(double)timeMap[key].n);
      return usec;
    }
  }

  /**************************************************************************/
  long BenchmarkTimer::getTotalTimeUSec(const char* timerName){
    long usec = 0;

    std::string key(timerName);

    std::map<std::string, timer>::iterator it = timeMap.find(key);
    if(it == timeMap.end()){
      return 0;
    }else{
      usec = timeMap[key].total_time.tv_sec * 1000000 +
        timeMap[key].total_time.tv_usec;
      return usec;
    }
  }

  /**************************************************************************/
  long BenchmarkTimer::getTotalCalls(const char* timerName){
    std::string key(timerName);

    std::map<std::string, timer>::iterator it = timeMap.find(key);
    if(it == timeMap.end()){
      return 0;
    }else{
      return timeMap[key].n;
    }
  }

  /**************************************************************************/
  long BenchmarkTimer::getAccumulativeUSec(){
    std::map<std::string, timer>::iterator it;
    struct timeval acc;
    timerclear(&acc);

    for(it = timeMap.begin(); it != timeMap.end();it++){
      timeradd(&acc, &((*it).second.total_time), &acc);
    }

    long usec = acc.tv_sec * 1000000 + acc.tv_usec;
    return usec;
  }

  /**************************************************************************/
  void BenchmarkTimer::printAverageUSec(const char* timerName){
    long usec = getAverageTimeUSec(timerName);
    message("Timer %s took %u usec on avarage after %d calls",
            timerName, usec, timeMap[std::string(timerName)].n);
  }

  /**************************************************************************/
  void BenchmarkTimer::printTotalUSec(const char* timerName){
    long usec = getTotalTimeUSec(timerName);
    message("Timer %s took %u usec", timerName, usec);
  }

  /**************************************************************************/
  void BenchmarkTimer::printAllAverageUSec(){
    std::map<std::string, timer>::iterator it;
    for(it = timeMap.begin(); it != timeMap.end();it++){
      timer t = (*it).second;

      long usec = t.total_time.tv_sec * 1000000 +
        t.total_time.tv_usec;
      usec = (long)((double) usec/(double)t.n);
      message("Timer %s took %u usec on avarage after %d calls",
              (*it).first.c_str(), usec, t.n);
    }
  }

  /**************************************************************************/
  void BenchmarkTimer::printAllTotalUSec(){
    std::map<std::string, timer>::iterator it;
    for(it = timeMap.begin(); it != timeMap.end();it++){
      timer t = (*it).second;

      long usec = t.total_time.tv_sec * 1000000 +
        t.total_time.tv_usec;
      message("Timer %s took %u usec",
              (*it).first.c_str(), usec);
    }
  }

  /**************************************************************************/
  void BenchmarkTimer::printAccumulativeUSec(){
    std::map<std::string, timer>::iterator it;
    struct timeval acc;
    timerclear(&acc);

    for(it = timeMap.begin(); it != timeMap.end();it++){
      timeradd(&acc, &((*it).second.total_time), &acc);
    }

    long usec = acc.tv_sec * 1000000 + acc.tv_usec;
    message("Accumulative time %u usec", usec);
  }
}
