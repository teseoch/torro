/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/tsldefs.hpp"
#include "core/UnitTest.hpp"
#include "string.h"
#include "stdio.h"

namespace tsl{
  /**************************************************************************/
  void registerVector4Tests(struct test_function**);

  /**************************************************************************/
  void registerMatrix44Tests(struct test_function**);

  /**************************************************************************/
  void registerMatrixTests(struct test_function** node);

  /**************************************************************************/
  void registerMathTests(struct test_function** node);

  /**************************************************************************/
  void runUnitTests(){
    struct test_function testFunctions;
    struct test_function* lastFunction = &testFunctions;

    message("Registering tests");

    registerVector4Tests(&lastFunction);
    registerMatrix44Tests(&lastFunction);
    registerMatrixTests(&lastFunction);
    registerMathTests(&lastFunction);

    message("Done registering tests");

    message("Start unit tests");
    lastFunction = &testFunctions;
    while(lastFunction->func != 0){
      lastFunction->test();
      lastFunction = lastFunction->next;
    }
    message("End unit tests");

    //getchar();

    /*Clear*/
    lastFunction = testFunctions.next;
    while(lastFunction){
      struct test_function* func = lastFunction;
      message("pointer = %p", func);
      if(lastFunction->func){
        lastFunction = lastFunction->next;
        free(func);
      }else{
        free(func);
        lastFunction = 0;
      }
    }
  }

  /**************************************************************************/
  void registerTestFunction(test_result (*func)(void),
                            const char* descr,
                            test_function** pptest){
    test_function* ptest = *pptest;

    ptest->func = func;
    strcpy(ptest->name, descr);
    ptest->next = (test_function*)malloc(sizeof(test_function));
    ptest->next->func = 0;
    memset(ptest->next->name, 0, sizeof(char)*1024);
    *pptest = ptest->next;
  }
}
