/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "CorotationalFEM.hpp"
#include "math/SVD.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  CorotationalFEM<T>::CorotationalFEM(DCTetraMesh<T>* _mesh,
                                      int _nElements):
    FEMModel<T>(_mesh, _nElements){
  }

  /**************************************************************************/
  template<class T>
  CorotationalFEM<T>::~CorotationalFEM(){
  }

  /**************************************************************************/
  template<class T>
  void CorotationalFEM<T>::
  computeElementStiffnessMatrixAndForce(MatrixT<12, 12, T>& K,
                                        MatrixT<12, 1, T>& rhs,
                                        MatrixT<12, 1, T>& ipos,
                                        MatrixT<12, 1, T>& cpos,
                                        TetrahedronData<T>& tet,
                                        int id, T dt, bool initial,
                                        bool verbose){
      if(initial){
        MatrixT<3, 4, T> partialDerivatives;
        MatrixT<6, 12, T> b;
        MatrixT<12, 6, T> bt;
        MatrixT<6, 12, T> db;

        computePartialDerivatives(partialDerivatives, id);

        b.clear();

        b[0][0] = partialDerivatives[0][0];
        b[0][3] = partialDerivatives[0][1];
        b[0][6] = partialDerivatives[0][2];
        b[0][9] = partialDerivatives[0][3];

        b[1][1] = partialDerivatives[1][0];
        b[1][4] = partialDerivatives[1][1];
        b[1][7] = partialDerivatives[1][2];
        b[1][10] = partialDerivatives[1][3];

        b[2][2] = partialDerivatives[2][0];
        b[2][5] = partialDerivatives[2][1];
        b[2][8] = partialDerivatives[2][2];
        b[2][11] = partialDerivatives[2][3];

        b[3][0] = partialDerivatives[1][0];
        b[3][1] = partialDerivatives[0][0];
        b[3][3] = partialDerivatives[1][1];
        b[3][4] = partialDerivatives[0][1];

        b[3][6] = partialDerivatives[1][2];
        b[3][7] = partialDerivatives[0][2];
        b[3][9] = partialDerivatives[1][3];
        b[3][10] = partialDerivatives[0][3];

        b[4][0] = partialDerivatives[2][0];
        b[4][2] = partialDerivatives[0][0];
        b[4][3] = partialDerivatives[2][1];
        b[4][5] = partialDerivatives[0][1];

        b[4][6] = partialDerivatives[2][2];
        b[4][8] = partialDerivatives[0][2];
        b[4][9] = partialDerivatives[2][3];
        b[4][11] = partialDerivatives[0][3];

        b[5][1] = partialDerivatives[2][0];
        b[5][2] = partialDerivatives[1][0];
        b[5][4] = partialDerivatives[2][1];
        b[5][5] = partialDerivatives[1][1];

        b[5][7] = partialDerivatives[2][2];
        b[5][8] = partialDerivatives[1][2];
        b[5][10] = partialDerivatives[2][3];
        b[5][11] = partialDerivatives[1][3];

        bt = b.transpose();

        K = bt * computeMaterialMatrix(this->E, this->mu) * b;

        tet.initialK = K;

        //K *= tet.initialVolume;
      }else{
        Matrix44<T> inv;
        Matrix44<T> rot;
        Matrix44<T> u;

        //MatrixT<12, 12, T> re;
        //MatrixT<12, 12, T> rei;

        /*Compute deformation gradient*/
#if 0
        Matrix44<T> x0 = tet.initialPos.transpose();
        x0[0] -= x0[3];
        x0[1] -= x0[3];
        x0[2] -= x0[3];
        x0[3] -= x0[3];
        x0[3][3] = (T)1.0;
        x0 = x0.transpose();

        Matrix44<T> xx = tet.c.transpose();
        xx[0] -= xx[3];
        xx[1] -= xx[3];
        xx[2] -= xx[3];
        xx[3] -= xx[3];
        xx[3][3] = (T)1.0;
        xx = xx.transpose();

        Matrix44<T> G = xx * x0.inverse();

        Matrix44<T> xp = tet.lastPos.transpose();
        xp[0] -= xp[3];
        xp[1] -= xp[3];
        xp[2] -= xp[3];
        xp[3] -= xp[3];
        xp[3][3] = (T)1.0;
        xp = xp.transpose();

        Matrix44<T> Gp = xp * x0.inverse();
#else
        const Matrix44<T>& x0 = tet.initialPos;
        const Matrix44<T>& xx = tet.c;
        Matrix44<T> G = xx * x0.inverse();

        const Matrix44<T>& xp = tet.lastPos;
        Matrix44<T> Gp = xp * x0.inverse();
#endif
        try{
          bool debug = false;
          extractRotationMatrix(&u, &inv, &G, &Gp, debug);
        }catch(Exception* e){

          std::cout << x0 << std::endl;
          std::cout << xx << std::endl;

          std::cout << G << std::endl;
          std::cout << inv << std::endl;
          std::cout << u << std::endl;

          delete e;

          error("Singular tetrahedron");
        }
        inv[0][3] = (T)0.0;
        inv[1][3] = (T)0.0;
        inv[2][3] = (T)0.0;
        inv[3][3] = (T)1.0;
        inv[3][2] = (T)0.0;
        inv[3][1] = (T)0.0;
        inv[3][0] = (T)0.0;

        u[0][3] = (T)0.0;
        u[1][3] = (T)0.0;
        u[2][3] = (T)0.0;
        u[3][3] = (T)1.0;
        u[3][2] = (T)0.0;
        u[3][1] = (T)0.0;
        u[3][0] = (T)0.0;

        rot = inv;

        inv = rot.transpose();

        //K = tet.initialK * tet.initialVolume;

        ////K = (re * K * rei);// * dt * dt;
        K = multiplyBlockL(multiplyBlockR(tet.initialK, inv), rot) *
          (tet.initialVolume * dt * dt);


        //elastic = -re * kc * rei * localCurrentPositions;
        //rhs = - re * tet.initialK * rei * cpos * tet.initialVolume;
        rhs = - multiplyBlockL(multiplyBlockR(tet.initialK, inv), rot) *
          (cpos * (tet.initialVolume * dt));

        //rhs += re * tet.initialK * ipos * tet.initialVolume;
        rhs += multiplyBlockL(tet.initialK, rot) *
          (ipos * (tet.initialVolume * dt));
      }
  }

  /**************************************************************************/
  template<class T>
  void CorotationalFEM<T>::computePartialDerivatives(MatrixT<3, 4, T>& par,
                                                     int id){
    Matrix44<T> coord, inverse;

    this->mesh->getTetrahedronCoords(id, coord);

    inverse = coord.inverse();

    for(int i=0;i<3;i++){
      for(int j=0;j<4;j++){
        par[i][j] = inverse[i][j];
      }
    }
  }

  /**************************************************************************/
  template<class T>
  MatrixT<6, 6, T> CorotationalFEM<T>::computeMaterialMatrix(T lE, T lMu){
    MatrixT<6, 6, T> dm; /*Material matrix*/

    T d11 = lE * ((T)1.0 - lMu)/(((T)1.0 + lMu) * ((T)1.0 - (T)2.0 * lMu));
    T d12 = lE * lMu           /(((T)1.0 + lMu) * ((T)1.0 - (T)2.0 * lMu));
    T d44 = lE                 /( (T)1.0 + lMu);

    dm[0][0] = d11;
    dm[0][1] = d12;
    dm[0][2] = d12;

    dm[1][0] = d12;
    dm[1][1] = d11;
    dm[1][2] = d12;

    dm[2][0] = d12;
    dm[2][1] = d12;
    dm[2][2] = d11;

    dm[3][3] = d44;
    dm[4][4] = d44;
    dm[5][5] = d44;

    return dm;
  }

  /**************************************************************************/
  template<class T>
  void CorotationalFEM<T>::extractRotationMatrix(Matrix44<T>* u,
                                                 Matrix44<T>* m,
                                                 Matrix44<T>* a,
                                                 Matrix44<T>* ap,
                                                 bool debug){

    Matrix44<T> uu;
    Matrix44<T> iu;
    Matrix44<T> aa;

    aa = *a;

    aa[0][3] = aa[1][3] = aa[2][3] = (T)0.0;

    uu = aa * aa.transpose3();
    *u = uu.sqrt();
    iu = u->inverse3x3();

    /*F = RS
      S^{-1}F = R*/
    *m = iu * aa;

#if 0
    for(int i=0;i<4;i++){
      for(int j=0;j<4;j++){
        if(IsNan((*u)[i][j]) ||
           IsNan((*m)[i][j]) ||
           IsNan((*a)[i][j]) ){

          message("IsNan = %d", IsNan((*u)[i][j]));
          message("IsNan = %d", IsNan((*m)[i][j]));
          message("IsNan = %d", IsNan((*a)[i][j]));

          std::cout << *u << std::endl;
          std::cout << *m << std::endl;
          std::cout << *a << std::endl;
          error("singular rotation");
          throw new DegenerateCaseException(__LINE__, __FILE__, "Rotation");
        }
      }
    }
#endif
  }

  /**************************************************************************/
  template<class T>
  MatrixT<12, 12, T> CorotationalFEM<T>::
  multiplyBlockR(const MatrixT<12, 12, T>& mat,
                 Matrix44<T>& r){
    MatrixT<12, 12, T> res;

    for(int i=0;i<12;i++){
      for(int j=0;j<12;j++){
        T val = (T)0.0;

        for(int k=0;k<3;k++){
          int col = j/3;
          int index = col*3 + k;
          val += (mat)[i][index] * (r)[k][j%3];
        }
        res[i][j] = val;
      }
    }
    return res;
  }

  /**************************************************************************/
  template<class T>
  MatrixT<12, 12, T> CorotationalFEM<T>::
  multiplyBlockL(const MatrixT<12, 12, T>& mat,
                 Matrix44<T>& r){
    MatrixT<12, 12, T> res;
    for(int i=0;i<12;i++){
      for(int j=0;j<12;j++){
        T val = (T)0.0;

        for(int k=0;k<3;k++){
          int row = i/3;
          int index = row*3 + k;
          val += r[i%3][k] * (mat)[index][j];
        }
        res[i][j] = val;
      }
    }
    return res;
  }

  /**************************************************************************/
  template<class T>
  void CorotationalFEM<T>::convert44To1212Matrix(MatrixT<12, 12, T>* res,
                                                 const Matrix44<T>* mat){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        T val = (*mat)[i][j];
        (*res)[i+0][j+0] = val;
        (*res)[i+3][j+3] = val;
        (*res)[i+6][j+6] = val;
        (*res)[i+9][j+9] = val;
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void CorotationalFEM<T>::
  computeRotationDerivative(const Matrix44<T>* r,
                            const Matrix44<T>* s,
                            const Matrix44<T>* pos,
                            const Matrix44<T>* ipos,
                            MatrixT<12, 12, T>* initialK,
                            MatrixT<12, 12, T>* derK,
                            MatrixT<12, 12, T>* derRHS){
    /*Not used at the moment*/
    Matrix44<T> rt = r->transpose();

    MatrixT<12, 12, T> rot12;
    MatrixT<12, 12, T> rot12T;

    convert44To1212Matrix(&rot12,  r);
    convert44To1212Matrix(&rot12T, &rt);

    Matrix44<T> inv = ipos->inverse();

    MatrixT<9, 12, T> dfdx;

    for(int i=0;i<12;i++){
      int offset = 3*(i%3);
      int column = i/3;
      for(int j=0;j<3;j++){
        dfdx[offset + j][i] = inv[column][j];
      }
    }

    MatrixT<9, 9, T> drdf;

    /*Compute dR/dF*/
    Matrix44<T> baseVectors = Matrix44<T>::identity();

    //std::cout << rt << std::endl;
    //std::cout << *s << std::endl;
    //message("trace(s) = %10.10e", s->trace());

    Matrix44<T> G = (Matrix44<T>::identity() * s->trace() - *s)*rt;
    G[3][3] = (T)1.0;

    //message("G");
    //std::cout << G << std::endl;

    Matrix44<T> GI = G.inverse();
    //message("GI");
    //std::cout << GI << std::endl;

    //getchar();
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        Matrix44<T> rteiejt = rt*baseVectors[i].outherProduct(baseVectors[j]);
        Vector4<T> skew2 = (T)2.0 * rteiejt.getSkewVector();

        //std::cout << skew2 << std::endl;

        Vector4<T> omega = GI * skew2;

        //message("omega");
        //std::cout << omega << std::endl;

        Matrix44<T> omegaSkewR = omega.toSkewCross() * *r;
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            drdf[i*3+k][j*3+l] = omegaSkewR[k][l];
          }
        }
        //getchar();
      }
    }
    //std::cout << drdf << std::endl;
    //getchar();

    MatrixT<9, 12, T> drdfdfdx;
    //std::cout << drdfdfdx << std::endl;
    drdfdfdx = drdf * dfdx;
    //std::cout << drdfdfdx << std::endl;

    MatrixT<12, 1, T> xx;
    MatrixT<12, 1, T> x0;

    for(int i=0;i<4;i++){
      for(int j=0;j<3;j++){
        xx[i*3+j][0] = ( *pos)[j][i];
        x0[i*3+j][0] = (*ipos)[j][i];
      }
    }

    MatrixT<12, 12, T> rotDer;
    MatrixT<12, 12, T> KRT = *initialK * rot12T;
    MatrixT<12, 1,  T> KRTX = *initialK * (rot12T * xx);
    MatrixT<12, 1,  T> KX0 = *initialK * x0;
    //MatrixT<12, 12, T KRTXXX0 = *initialK *(rot12T * xx - x0);

    //MatrixT<12, 12, T> RK  = RKT.transpose();//rot12 * *initialK;
    //MatrixT<12, 1, T> KRM = *initialK * rot12 * mm;

    for(int i=0;i<12;i++){
      Matrix44<T> drotl;
      //std::cout << drdfdfdx << std::endl;
      for(int j=0;j<9;j++){
        drotl[j/3][j%3] = drdfdfdx[j][i];
      }

      //message("rotl = %d", i);
      //std::cout << drotl << std::endl;

      MatrixT<12, 12, T> drotl12;
      convert44To1212Matrix(&drotl12, &drotl);

      MatrixT<12, 1, T> dr = drotl12*KRTX;
      //std::cout << dr << std::endl;


      for(int j=0;j<12;j++){
        rotDer[j][i] = dr[j][0];
      }

      dr = drotl12*KX0;
      for(int j=0;j<12;j++){
        rotDer[j][i] -= dr[j][0];
        //(*derRHS)[j][i] = dr[j][0];
      }

      //RKdrtx
      dr = KRT.transpose() * drotl12.transpose() * xx;

      //std::cout << dr << std::endl;

      //MatrixT<12, 1, T> dr = KRM;

      for(int j=0;j<12;j++){
        rotDer[j][i] += dr[j][0];
      }

      //getchar();
    }

    *derK = rot12 * *initialK * rot12T + rotDer;

    //std::cout << *initialK << std::endl;
    //std::cout << *derK << std::endl;
    //std::cout << *derRHS << std::endl;
    //*derRHS =

    //getchar();
  }

  template class CorotationalFEM<float>;
  template class CorotationalFEM<double>;
}
