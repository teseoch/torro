/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "HyperElasticityFEM.hpp"
#include "math/Math.hpp"
#include "math/SVD.hpp"

#define DBGF
#undef DBGF

/*
  Based on:
  Energetically Consistent Invertible Elasticity,
  Alexey Stomakhin, Russel Howes, Craig Schroeder, and Joseph M. Teran
  Proc. of the ACM SIGGRAPH/Eurographics Symp. on Computer Animation
  SCA '12
*/

namespace tsl{
  /**************************************************************************/
  template<class T>
  bool tensor3Symmetric(Matrix44<T> H3[3]){
    bool symmetric = true;
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          T val = H3[i][j][k];
          bool err = false;
          int errorCase = 0;

          if(H3[i][j][k] != val){
            err = true;
            errorCase = 1;
          }
          if(H3[i][j][k] != val){
            err = true;
            errorCase = 2;
          }
          if(H3[i][k][j] != val){
            err = true;
            errorCase = 3;
          }
          if(H3[i][k][j] != val){
            err = true;
            errorCase = 4;
          }
          if(H3[i][k][j] != val){
            err = true;
            errorCase = 5;
          }
          if(H3[i][j][k] != val){
            err = true;
            errorCase = 6;
          }


          if(H3[j][i][k] != val){
            err = true;
            errorCase = 7;
          }
          if(H3[j][i][k] != val){
            err = true;
            errorCase = 8;
          }
          if(H3[k][i][j] != val){
            err = true;
            errorCase = 9;
          }
          if(H3[k][i][j] != val){
            err = true;
            errorCase = 10;
          }
          if(H3[i][k][j] != val){
            err = true;
            errorCase = 11;
          }
          if(H3[i][j][k] != val){
            err = true;
            errorCase = 12;
          }

          if(H3[j][i][k] != val){
            err = true;
            errorCase = 13;
          }
          if(H3[j][k][i] != val){
            err = true;
            errorCase = 14;
          }
          if(H3[k][j][i] != val){
            err = true;
            errorCase = 15;
          }
          if(H3[k][i][j] != val){
            err = true;
            errorCase = 16;
          }
          if(H3[k][i][j] != val){
            err = true;
            errorCase = 17;
          }
          if(H3[j][i][k] != val){
            err = true;
            errorCase = 18;
          }

          if(H3[j][k][i] != val){
            err = true;
            errorCase = 19;
          }
          if(H3[j][k][i] != val){
            err = true;
            errorCase = 20;
          }
          if(H3[k][j][i] != val){
            err = true;
            errorCase = 21;
          }
          if(H3[k][j][i] != val){
            err = true;
            errorCase = 22;
          }
          if(H3[k][j][i] != val){
            err = true;
            errorCase = 23;
          }
          if(H3[j][k][i] != val){
            err = true;
            errorCase = 24;
          }

          if(err){
            message("not symmetric H3 - %d, %d, %d | %d", i, j, k, errorCase);
            symmetric = false;
          }
        }
      }
    }
    if(!symmetric){
      message("H3");
      std::cout << H3[0] << std::endl;
      std::cout << H3[1] << std::endl;
      std::cout << H3[2] << std::endl;
    }
    return symmetric;
  }

  /**************************************************************************/
  template<class T>
  HyperElasticityFEM<T>::HyperElasticityFEM(DCTetraMesh<T>* _mesh,
                                            int _nElements):
    FEMModel<T>(_mesh, _nElements){
    shapeFunctions = new Matrix44<T>[this->nElements];
  }

  /**************************************************************************/
  template<class T>
  HyperElasticityFEM<T>::~HyperElasticityFEM(){
    delete[] shapeFunctions;
  }

  /**************************************************************************/
  template<class T>
  void HyperElasticityFEM<T>::
  computeElementStiffnessMatrixAndForce(MatrixT<12, 12, T>& K,
                                        MatrixT<12, 1,  T>& rhs,
                                        MatrixT<12, 1,  T>& ipos,
                                        MatrixT<12, 1,  T>& cpos,
                                        TetrahedronData<T>& tet,
                                        int id, T dt, bool initial,
                                        bool verbose){
    /*                           0       1       2       3    */
    int vertexFace[4][3]   = {{1,2,3},{0,3,2},{0,1,3},{0,2,1}};
    int vertexFace2[4][3]  = {{1,2,3},{0,2,3},{0,1,3},{0,1,2}};

    if(initial){
      /*Compute initial values*/
      Matrix44<T> A = tet.initialPos.transpose();

      Triangle<T> faces[4];

      Vector4<T> center;

      for(int i=0;i<4;i++){
        faces[i].set(A[vertexFace[i][1]],
                     A[vertexFace[i][0]],
                     A[vertexFace[i][2]]);

        center += A[i];
      }

      center /= (T)4.0;

      Matrix44<T> B;
      for(int i=0;i<4;i++){
        for(int j=0;j<3;j++){
          B[i] += -faces[vertexFace2[i][j]].n *
            faces[vertexFace2[i][j]].getArea() / (T)3.0;
        }
      }

      Vector4<T> avgN;

      for(int i=0;i<4;i++){
        avgN += faces[i].n * faces[i].getArea();
        T dist = faces[i].getSignedDistance(center);

        if(dist > 0.0){
          message("distance = %10.10e", dist);
        }
      }

      shapeFunctions[id] = B;
    }else{
      /*Compute deformation gradient*/
      const Matrix44<T>& x0 = tet.initialPos;
      const Matrix44<T>& xx = tet.c;

      /*Compute deformation gradient*/
      Matrix44<T> AI = x0.inverse();
      Matrix44<T> G = xx * AI;
      Matrix44<T> U, V, D, VT;

      for(int m = 0;m < 4; m++){
        G[m][3]  = 0.0;
        AI[m][3] = 0.0;
      }

      G[3].set(0,0,0,1);
      AI[3].set(0,0,0,1);

      Matrix44<T> OG = G;

      /*Singular Value Decomposition*/
      SVD<T, 3>(G, U, D, V);
      SVDCorrection<T>(G, U, D, V, VT);

      /*Compute derivative of SVD and compute derivative of force*/
      Matrix44<T> derivatives  [3][3];
      Matrix44<T> derivativesRL[3][3];
      Matrix44<T> derivativesRR[3][3];
      Matrix44<T> derivativesDH[3][3];

      Matrix44<T> P, H, Q;
      computeHP(H, P, D, Q, verbose);

      /*Q contains the corrected diagonalized deformation. The SVD
        derivative should take Q into account, rather than the
        original diagonalized deformation D.*/
      D = Q;

      T peps = (T)1e-8;
      T rpeps = (T)1.0 + peps;

      /*Scale singular values to prevent errors with SVD derivatives*/
      if(Abs(D[2][2] - D[0][0]) < (T)peps){
      }

      if(Abs(D[1][1] - D[2][2]) < (T)peps){
        D[1][1] *= rpeps;
        D[0][0] *= rpeps;
      }
      if(Abs(D[0][0] - D[1][1]) < (T)peps){
        D[0][0] *= rpeps;
      }

      /*Redo SVD with updated D*/
      G = U * D * V.transpose();
      SVD<T, 3>(G, U, D, V);
      SVDCorrection<T>(G, U, D, V, VT);

      /*Compute SVD derivatives*/
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          computeForceDerivative(derivatives[i][j],
                                 derivativesRL[i][j],
                                 derivativesRR[i][j],
                                 derivativesDH[i][j],
                                 U, D, Q, V, VT, P, H, OG, i, j,
                                 verbose);
        }
      }

      MatrixT<12, 12, T> derRL;
      MatrixT<12, 12, T> derRR;
      MatrixT<12, 12, T> derDH;

      /*Compute force derivative per individual force*/
      for(int i=0;i<3;i++){
        /*gi*/
        for(int j=i*3;j<9;j++){
          Vector4<T> dgi;
          Vector4<T> dgiRL;
          Vector4<T> dgiRR;
          Vector4<T> dgiDH;

          for(int y=0;y<3;y++){
            T factor = AI[j/3][y] * dt * dt;

            if(factor == (T)0.0){
              continue;
            }

            dgi   += derivatives  [j%3][y] * shapeFunctions[id][i] * factor;
            dgiRL += derivativesRL[j%3][y] * shapeFunctions[id][i] * factor;
            dgiRR += derivativesRR[j%3][y] * shapeFunctions[id][i] * factor;
            dgiDH += derivativesDH[j%3][y] * shapeFunctions[id][i] * factor;
          }

          for(int k=0;k<3;k++){
            K    [i*3+k][j] = -dgi  [k];
            derRL[i*3+k][j] = -dgiRL[k];
            derRR[i*3+k][j] = -dgiRR[k];
            derDH[i*3+k][j] = -dgiDH[k];

            if(i*3+k < j){
              K    [j][i*3+k] = -dgi[k];

              derRL[j][i*3+k] = -dgiRL[k];
              derRR[j][i*3+k] = -dgiRR[k];
              derDH[j][i*3+k] = -dgiDH[k];
            }
          }
        }
      }

      /*Fill rest of matrix*/
      for(int i=0;i<9;i++){
        for(int j=0;j<3;j++){
          T sum   = (T)0.0;
          T sumRL = (T)0.0;
          T sumRR = (T)0.0;
          T sumDH = (T)0.0;

          for(int k=0;k<3;k++){
            sum   += K    [j+k*3][i];
            sumRL += derRL[j+k*3][i];
            sumRR += derRR[j+k*3][i];
            sumDH += derDH[j+k*3][i];
          }

          K    [9+j][i] = -sum;
          K    [i][9+j] = -sum;

          derRL[9+j][i] = -sumRL;
          derRL[i][9+j] = -sumRL;

          derRR[9+j][i] = -sumRR;
          derRR[i][9+j] = -sumRR;

          derDH[9+j][i] = -sumDH;
          derDH[i][9+j] = -sumDH;
        }
      }

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          if(i>j){
            continue;
          }

          T sum   = (T)0.0;
          T sumRL = (T)0.0;
          T sumRR = (T)0.0;
          T sumDH = (T)0.0;

          for(int k=0;k<3;k++){
            sum   += K    [9+j][i+k*3];
            sumRL += derRL[9+j][i+k*3];
            sumRR += derRR[9+j][i+k*3];
            sumDH += derDH[9+j][i+k*3];
          }

          K    [9+i][9+j] = -sum;
          derRL[9+i][9+j] = -sumRL;
          derRR[9+i][9+j] = -sumRR;
          derDH[9+i][9+j] = -sumDH;


          if(i != j){
            K    [9+j][9+i] = -sum;
            derRL[9+j][9+i] = -sumRL;
            derRR[9+j][9+i] = -sumRR;
            derDH[9+j][9+i] = -sumDH;
          }
        }
      }

      /*Check eigenvalues*/
#if 0
      {
        bool check = false;

        if(D.det() < -0.1){
          //check = true;
        }
        T evmin, evmax, cond;
        T eps = (T)-1e-15;

        cond = (K/(dt*dt)).conditionNumber(true, (T)1e-15, &evmin, &evmax);

        if(evmin < eps ||evmax < eps){
          warning("Small eigenvalues K, %10.10e, %10.10e, %10.10e",
                  evmin, evmax, cond);
          check = true;
          std::cout << (K)/(dt*dt) << std::endl;
        }

        if(IsNan(K)){
          warning("Nan in K");
          check = true;
          std::cout << K << std::endl;
        }

#if 0
        cond = (derRL/(dt*dt)).conditionNumber(true, (T)1e-15, &evmin, &evmax);

        if(evmin < eps ||evmax < eps){
          warning("Small eigenvalues RL, %10.10e, %10.10e, %10.10e",
                  evmin, evmax, cond);
          //check = true;
          std::cout << (derRL)/(dt*dt) << std::endl;
        }
#endif
#if 0
        cond = (derRR/(dt*dt)).conditionNumber(true, (T)1e-15, &evmin, &evmax);

        if(evmin < eps ||evmax < eps){
          warning("Small eigenvalues RR, %10.10e, %10.10e, %10.10e",
                  evmin, evmax, cond);
          //check = true;
          std::cout << (derRR)/(dt*dt) << std::endl;
        }
#endif

        if(IsNan(derRR)){
          warning("Nan in RR");
          check = true;
          std::cout << derRR << std::endl;
        }

        if(IsNan(derRL)){
          warning("Nan in RL");
          check = true;
          std::cout << derRL << std::endl;
        }

#if 1
        cond = ((derRR+derRL)/(dt*dt)).conditionNumber(true, (T)1e-15, &evmin, &evmax);

        if(evmin < eps ||evmax < eps){
          warning("Small eigenvalues RR+RL, %10.10e, %10.10e, %10.10e",
                  evmin, evmax, cond);
          check = true;
          std::cout << (derRR+derRL)/(dt*dt) << std::endl;
        }
#endif
        cond = (derDH/(dt*dt)).conditionNumber(true, (T)1e-15, &evmin, &evmax);

        if(evmin < eps ||evmax < eps){
          warning("Small eigenvalues DH, %10.10e, %10.10e, %10.10e",
                  evmin, evmax, cond);
          check = true;
          std::cout << derDH/(dt*dt) << std::endl;
        }

        if(IsNan(derDH)){
          warning("Nan in DH");
          check = true;
        }


        if(check){
          for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              computeForceDerivative(derivatives[i][j],
                                     derivativesRL[i][j],
                                     derivativesRR[i][j],
                                     derivativesDH[i][j],
                                     U, D, Q, V, VT, P, H, OG, i, j,
                                     true);
            }
          }


          warning("K");
          std::cerr << K/(dt*dt) << std::endl;

          warning("RL");
          std::cerr << derRL/(dt*dt) << std::endl;

          warning("RR");
          std::cerr << derRR/(dt*dt) << std::endl;

          warning("DH");
          std::cerr << derDH/(dt*dt) << std::endl;



          warning("faces");

          Matrix44<T> A = tet.initialPos.transpose();

          Triangle<T> faces[4];

          for(int i=0;i<4;i++){
            faces[i].set(A[vertexFace[i][1]],
                         A[vertexFace[i][0]],
                         A[vertexFace[i][2]]);

            std::cerr << faces[i] << std::endl;
          }

          warning("x0");
          std::cerr << x0 << std::endl;

          warning("xx");
          std::cerr << xx << std::endl;

          warning("G");
          std::cerr << G << std::endl;

          warning("UDV");
          std::cerr << U << std::endl;
          std::cerr << D << std::endl;
          std::cerr << V << std::endl;
          std::cerr << U*D*VT << std::endl;
          warning("G = %10.10e", G.det());
          warning("U = %10.10e", U.det());
          warning("D = %10.10e", D.det());
          warning("V = %10.10e", V.det());


          warning("HP");
          std::cerr << H << std::endl;
          std::cerr << P << std::endl;

          computeHP(H, P, D, Q, true);

          warning("SVD");

          warning("UDVT");
          std::cerr << U*D*VT << std::endl;

          SVD<T, 3>(G, U, D, V);
          warning("G = %10.10e", G.det());
          warning("U = %10.10e", U.det());
          warning("D = %10.10e", D.det());
          warning("V = %10.10e", V.det());

          Matrix44<T> x01 = tet.initialPos.transpose();
          x01[0] -= x01[3];
          x01[1] -= x01[3];
          x01[2] -= x01[3];
          x01[3] -= x01[3];
          x01[3][3] = (T)1.0;
          x01 = x01.transpose();

          Matrix44<T> xxx = tet.c.transpose();
          xxx[0] -= xxx[3];
          xxx[1] -= xxx[3];
          xxx[2] -= xxx[3];
          xxx[3] -= xxx[3];
          xxx[3][3] = (T)1.0;
          xxx = xxx.transpose();

          warning("old AI");
          std::cerr << AI << std::endl;

          AI = x01.inverse();

          warning("AI");
          std::cerr << AI << std::endl;

          G = xxx * AI;

          warning("G");
          std::cerr << G << std::endl;

          SVD<T, 3>(G, U, D, V);
          warning("G = %10.10e", G.det());
          warning("U = %10.10e", U.det());
          warning("D = %10.10e", D.det());
          warning("V = %10.10e", V.det());

          std::cerr << G << std::endl;
          std::cerr << U << std::endl;
          std::cerr << D << std::endl;
          std::cerr << V << std::endl;

          std::cerr << U*D*V.transpose() << std::endl;

          warning("derivatives");
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              std::cerr << derivatives[j][k] << std::endl;
            }
          }

          warning("derivatives RL");
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              std::cerr << derivativesRL[j][k] << std::endl;
            }
          }

          warning("derivatives RR");
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              std::cerr << derivativesRR[j][k] << std::endl;
            }
          }

          warning("derivatives DH");
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              std::cerr << derivativesDH[j][k] << std::endl;
            }
          }

          error("not correct");
        }
      }
#endif

      /*Compute forces*/
      Matrix44<T> RP = U*P*VT;

      for(int i=0;i<4;i++){
        Vector4<T> gi = RP*shapeFunctions[id][i] * dt;
        for(int j=0;j<3;j++){
          rhs[i*3+j][0] = gi[j];
        }
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void HyperElasticityFEM<T>::
  evaluateStrainEnergyDensityFunction(Matrix44<T>& F){
    T l1 = Max(F[0][0], (T)1e-6);
    T l2 = Max(F[1][1], (T)1e-6);
    T l3 = Max(F[2][2], (T)1e-6);

    T mu = (T)1.5 * (T)1e6; /*Shear modulus*/
    T k  = (T)0.5 * (T)1e9; /*Bulk modulus*/

    T c1 = mu / (T)2.0;
    T d1 = k  / (T)2.0;

    T J = l1 * l2 * l3;

    T I1 = Sqr(l1) + Sqr(l2) + Sqr(l3);
    T I2 = Pow(J, -(T)2.0/(T)3.0) * I1;

    message("W = %10.10e", c1 * (I2 - 3.0) + d1 * Sqr(J - (T)1.0));
    message("W = %10.10e", c1 * I2 - c1 * (T)3.0 + d1 * Sqr(J) -
            (T)2.0 * d1 * J + d1);
  }

  /**************************************************************************/
  template<class T>
  class Interpolator<T, Vector4<T>, Vector4<T> >{
  public:
    /**************************************************************************/
    Interpolator(){
      eps1 = (T)0.9;
      eps2 = (T)0.2;
      mode = 1;
    }

    /**************************************************************************/
    void init(){
    }

    /**************************************************************************/
    T evaluate(T s){
      if(mode == 1){
        Vector4<T> Q = R + s * (S - R);

        T J = Q[0] * Q[1] * Q[2];

        return J - eps1;
      }
      return (T)0.0;
    }

    void getResult(T s, Vector4<T>* a, Vector4<T>* b){
    }

    Vector4<T> R;
    Vector4<T> S;
    T eps1;
    T eps2;
    int mode;
  };

  /**************************************************************************/
  bool inverted = false;

  /**************************************************************************/
  template<class T>
  void HyperElasticityFEM<T>::computeSDerivatives(Vector4<T>& ds,
                                                  Matrix44<T>& dds,
                                                  Vector4<T>& q,
                                                  Matrix44<T>& dq,
                                                  Matrix44<T> (& ddq)[3],
                                                  const T s,
                                                  const Vector4<T>& sigma){
    /*Compute q and its derivatives,
      compute derivatives of s, which depend on dq*/
    Vector4<T> r((T)1.0, (T)1.0, (T)1.0, (T)0.0);

    for(int i=0;i<3;i++){
      q[i] = r[i] + (sigma[i] - r[i])*s;
    }

    T gg;
    gg =
      sigma[0]*q[1]*q[2] +
      q[0]*sigma[1]*q[2] +
      q[0]*q[1]*sigma[2] - q[1]*q[2] - q[0]*q[2] - q[0]*q[1];

    T ggSquared = Sqr(gg);

    ds[0] = -s*q[1]*q[2] / gg;
    ds[1] = -q[0]*s*q[2] / gg;
    ds[2] = -q[0]*q[1]*s / gg;

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        dq[i][j] = kroneckerDelta<T>(i,j) * s + (sigma[i] - r[i]) * ds[j];
      }
    }

    Vector4<T> dgg;

    dgg[0] =
      (T)1.0*q[1]*q[2] + sigma[0]*dq[1][0]*q[2] + sigma[0]*q[1]*dq[2][0] +
      dq[0][0]*sigma[1]*q[2] + q[0]*(T)0.0*q[2] + q[0]*sigma[1]*dq[2][0] +
      dq[0][0]*q[1]*sigma[2] + q[0]*dq[1][0]*sigma[2] + q[0]*q[1]*(T)0.0 -
      dq[0][0]*q[1] - q[0]*dq[1][0] -
      dq[0][0]*q[2] - q[0]*dq[2][0] -
      dq[1][0]*q[2] - q[1]*dq[2][0];

    dgg[1] =
      (T)0.0*q[1]*q[2] + sigma[0]*dq[1][1]*q[2] + sigma[0]*q[1]*dq[2][1] +
      dq[0][1]*sigma[1]*q[2] + q[0]*(T)1.0*q[2] + q[0]*sigma[1]*dq[2][1] +
      dq[0][1]*q[1]*sigma[2] + q[0]*dq[1][1]*sigma[2] + q[0]*q[1]*(T)0.0 -
      dq[0][1]*q[1] - q[0]*dq[1][1] -
      dq[0][1]*q[2] - q[0]*dq[2][1] -
      dq[1][1]*q[2] - q[1]*dq[2][1];

    dgg[2] =
      (T)0.0*q[1]*q[2] + sigma[0]*dq[1][2]*q[2] + sigma[0]*q[1]*dq[2][2] +
      dq[0][2]*sigma[1]*q[2] + q[0]*(T)0.0*q[2] + q[0]*sigma[1]*dq[2][2] +
      dq[0][2]*q[1]*sigma[2] + q[0]*dq[1][2]*sigma[2] + q[0]*q[1]*(T)1.0 -
      dq[0][2]*q[1] - q[0]*dq[1][2] -
      dq[0][2]*q[2] - q[0]*dq[2][2] -
      dq[1][2]*q[2] - q[1]*dq[2][2];


    dds[0][0] =
      -ds[0]*q[1]*q[2]/gg
      -s*dq[1][0]*q[2]/gg
      -s*q[1]*dq[2][0]/gg
      -s*q[1]*q[2]*(-dgg[0]/ggSquared);

    dds[0][1] =
      -ds[1]*q[1]*q[2]/gg
      -s*dq[1][1]*q[2]/gg
      -s*q[1]*dq[2][1]/gg
      -s*q[1]*q[2]*(-dgg[1]/ggSquared);

    dds[0][2] =
      -ds[2]*q[1]*q[2]/gg
      -s*dq[1][2]*q[2]/gg
      -s*q[1]*dq[2][2]/gg
      -s*q[1]*q[2]*(-dgg[2]/ggSquared);

    ///////////////////////////////////////////

    dds[1][0] =
      -dq[0][0]*s*q[2]/gg
      -q[0]*ds[0]*q[2]/gg
      -q[0]*s*dq[2][0]/gg
      -q[0]*s*q[2]*(-dgg[0]/ggSquared);

    dds[1][1] =
      -dq[0][1]*s*q[2]/gg
      -q[0]*ds[1]*q[2]/gg
      -q[0]*s*dq[2][1]/gg
      -q[0]*s*q[2]*(-dgg[1]/ggSquared);

    dds[1][2] =
      -dq[0][2]*s*q[2]/gg
      -q[0]*ds[2]*q[2]/gg
      -q[0]*s*dq[2][2]/gg
      -q[0]*s*q[2]*(-dgg[2]/ggSquared);

    ///////////////////////////////////////////

    dds[2][0] =
      -dq[0][0]*q[1]*s/gg
      -q[0]*dq[1][0]*s/gg
      -q[0]*q[1]*ds[0]/gg
      -q[0]*q[1]*s*(-dgg[0]/ggSquared);

    dds[2][1] =
      -dq[0][1]*q[1]*s/gg
      -q[0]*dq[1][1]*s/gg
      -q[0]*q[1]*ds[1]/gg
      -q[0]*q[1]*s*(-dgg[1]/ggSquared);

    dds[2][2] =
      -dq[0][2]*q[1]*s/gg
      -q[0]*dq[1][2]*s/gg
      -q[0]*q[1]*ds[2]/gg
      -q[0]*q[1]*s*(-dgg[2]/ggSquared);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          ddq[i][j][k] =
            (kroneckerDelta<T>(i,j) * ds[k] +
             kroneckerDelta<T>(i,k) * ds[j]) +
            (sigma[i] - r[i]) * dds[j][k];
        }
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void HyperElasticityFEM<T>::computeHDerivatives(T& h, Vector4<T>& dh,
                                                  Matrix44<T>& ddh,
                                                  const Vector4<T>& sigma,
                                                  const Vector4<T>& q,
                                                  const Matrix44<T>& dq,
                                                  const Matrix44<T> (& ddq)[3],
                                                  const Vector4<T>& u,
                                                  const Matrix44<T>& du,
                                                  const Matrix44<T> (&ddu) [3]){
    /*Compute h and its derivatives*/

    h = (T)0.0;

    for(int i=0;i<3;i++){
      h += (sigma[i] - q[i]) * u[i];
    }

    for(int j=0;j<3;j++){
      dh[j] = (T)0.0;
      for(int i=0;i<3;i++){
        dh[j] +=
          (kroneckerDelta<T>(i,j) - dq[i][j]) * u[i] +
          (sigma[i] - q[i]) * du[i][j];
      }
    }

    for(int j=0;j<3;j++){
      for(int k=0;k<3;k++){
        ddh[j][k] = (T)0.0;

        for(int i=0;i<3;i++){
          ddh[j][k] +=
            -ddq[i][j][k] * u[i] +
            ((kroneckerDelta<T>(i,j) - dq[i][j]) * du[i][k] +
             (kroneckerDelta<T>(i,k) - dq[i][k]) * du[i][j]) +
            (sigma[i] - q[i]) * ddu[i][j][k];
        }
      }
    }
  }

  template<class T>
  void HyperElasticityFEM<T>::computeMDerivatives(T& m, Vector4<T>& dm,
                                                  Matrix44<T>& ddm,
                                                  const Vector4<T>& sigma){
    /*Compute m and its derivatives*/
    Vector4<T> r((T)1.0, (T)1.0, (T)1.0, (T)0.0);

    m = (T)1.0/(sigma - r).length();

    T m3 = m*m*m;
    T m5 = m3*m*m;

    dm = -(sigma - r) * m3;

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        ddm[i][j] =
          (T)3.0*(sigma[i] - r[i])*(sigma[j] - r[j])*m5 -
          kroneckerDelta<T>(i,j)*m3;
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void HyperElasticityFEM<T>::computeUDerivatives(Vector4<T>& u,
                                                  Matrix44<T>& du,
                                                  Matrix44<T> (& ddu)[3],
                                                  const T m,
                                                  const Vector4<T>& dm,
                                                  const Matrix44<T>& ddm,
                                                  const Vector4<T>& sigma){
    /*Compute u and its derivatives*/
    Vector4<T> r((T)1.0, (T)1.0, (T)1.0, (T)0.0);

    u = m * (sigma - r);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        du[i][j] =
          dm[j] * (sigma[i] - r[i]) + m * kroneckerDelta<T>(i,j);
      }
    }

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          ddu[i][k][j] =
            ddm[k][j] * (sigma[i] - r[i]) +
            (dm[k] * kroneckerDelta<T>(i,j) + dm[j]*kroneckerDelta<T>(i,k));
        }
      }
    }
  }

  template<class T>
  void HyperElasticityFEM<T>::computeHP(Matrix44<T>& H,
                                        Matrix44<T>& P,
                                        Matrix44<T>& F,
                                        Matrix44<T>& MQ,
                                        bool verbose){
    T epsilon1 = (T)0.7;
    T epsilon2 = (T)0.7;

    T mu      = (T)1.0 * (T)12.0 * (T)1e4; /*Shear modulus*/
    T lambda  = (T)1.0 * (T)12.0 * (T)1e4; /*Bulk modulus*/

    T E = (T)5e5;
    T nu = (T)0.2;

    /*Convert Young's modulus and Poisson ratio to Lame parameters*/
    lambda = nu * E/(((T)1.0+nu)*((T)1.0-(T)2.0*nu));
    mu = E / ((T)2.0*((T)1.0+nu));

    inverted = false;

    if(F[0][0] * F[1][1] * F[2][2] < 0.0){
      inverted = true;
    }

    /*psi = mu/2 * (f1^2 + f2^2 + f3^2 -3) - mu * ln(J) + lambda/2 (ln(J))^2 */
    if(F[0][0] * F[1][1] * F[2][2] < epsilon1){
      /*Too much compression*/
      Vector4<T> S(F[0][0], F[1][1], F[2][2], 0.0);
      Vector4<T> R(1.0, 1.0, 1.0, 0.0);

      /*Find intersection along S-R where J = epsilon*/
      Interpolator<T, Vector4<T>, Vector4<T> >func;
      func.R = R;
      func.S = S;

      func.eps1 = epsilon1;
      func.eps2 = epsilon2;

      T root =
        brentDekker<T, Vector4<T>, Vector4<T> >(&func, (T)0.0, (T)1.0, (T)0.0);

      Vector4<T> Q = R + root * (S - R);

      MQ[0][0] = Q[0];
      MQ[1][1] = Q[1];
      MQ[2][2] = Q[2];

      T h = (S - Q).length();
      if(verbose){
        message("J = %10.10e", Q[0] * Q[1] * Q[2]);

        message("root = %10.10e, h = %10.10e", root, h);

        message("Q");
        std::cout << Q << std::endl;
      }

      T m = 0.0;
      Vector4<T>  dm;
      Matrix44<T> ddm;
      Vector4<T>  dh;
      Matrix44<T> ddh;
      Vector4<T>  u;
      Matrix44<T> du;
      Matrix44<T> ddu[3];
      Vector4<T>  ds;
      Matrix44<T> dds;
      Matrix44<T> dq;
      Matrix44<T> ddq[3];

      computeMDerivatives(m, dm, ddm, S);
      computeUDerivatives(u, du, ddu, m, dm, ddm, S);
      computeSDerivatives(ds, dds, Q, dq, ddq, root, S);
      computeHDerivatives(h, dh, ddh, S,  Q, dq, ddq, u, du, ddu);

      if(verbose){
        message("distance s-r = %10.10e, %10.10e",
                (S-R).length(), 1.0/(S-R).length());
        message("m = %10.10e dm ddm", m);
        std::cout << dm << std::endl;
        std::cout << ddm << std::endl;

        message("h = %10.10e dh ddh", h);
        std::cout << dh << std::endl;
        std::cout << ddh << std::endl;

        message("s = %10.10e ds dds", root);
        std::cout << ds << std::endl;
        std::cout << dds << std::endl;

        message("u du ddu", h);
        std::cout << u << std::endl;
        std::cout << du << std::endl;
        std::cout << ddu[0] << std::endl;
        std::cout << ddu[1] << std::endl;
        std::cout << ddu[2] << std::endl;

        message("q dq ddq", h);
        std::cout << Q << std::endl;
        std::cout << dq << std::endl;
        std::cout << ddq[0] << std::endl;
        std::cout << ddq[1] << std::endl;
        std::cout << ddq[2] << std::endl;
      }

      Vector4<T> Q2(Sqr(Q[0]),  Sqr(Q[1]),  Sqr(Q[2]), 0.0);
      Vector4<T> Q3(Q2[0]*Q[0], Q2[1]*Q[1], Q2[2]*Q[2], 0.0);
      Vector4<T> Q4(Sqr(Q2[0]), Sqr(Q2[1]), Sqr(Q2[2]), 0.0);

      /*Linearize around Q*/
      T J  = Q[0] * Q[1] * Q[2];
      T LJ = Log(J);

      if(verbose){
        message("J = %10.10e, LJ = %10.10e", J, LJ);
        message("Q234");
        std::cout << Q << std::endl;
        std::cout << Q2 << std::endl;
        std::cout << Q3 << std::endl;
        std::cout << Q4 << std::endl;
      }

      /*Derivatives of Energy Density function*/
      Vector4<T>  H1;
      Matrix44<T> H2;
      Matrix44<T> H3[3];
      Matrix44<T> H4[3][3];

      /*The extended Energy Density function is defined for parameter
        sigma (S), therefore, we need to take the derivatives of the
        extended Energy Density Function wrt sigma. This includes also
        the derivatives of h and s since they both depend on
        sigma. See Stomakhin's PhD thesis for the full derivation, as
        used in this file.*/

      /*In this context d1 means the derivative wrt sigma_1 and
        d3 wrt sigma_3. Sigma */

      /*First derivatives of Energy Density function (psi) at q*/
      H1[0] = (lambda * LJ + mu * (Q2[0] - (T)1.0)) / Q[0];         //d1
      H1[1] = (lambda * LJ + mu * (Q2[1] - (T)1.0)) / Q[1];         //d2
      H1[2] = (lambda * LJ + mu * (Q2[2] - (T)1.0)) / Q[2];         //d3

      /*Second derivatives of Energy Density function (psi) at q*/
      H2[0][0] = (mu + mu * Q2[0] + lambda - lambda * LJ) / Q2[0]; //d1d1
      H2[0][1] = lambda / (Q[0] * Q[1]);                           //d2d1
      H2[0][2] = lambda / (Q[0] * Q[2]);                           //d3d1

      H2[1][0] = lambda / (Q[1] * Q[0]);                           //d1d2
      H2[1][1] = (mu + mu * Q2[1] + lambda - lambda * LJ) / Q2[1]; //d2d2
      H2[1][2] = lambda / (Q[1] * Q[2]);                           //d3d2

      H2[2][0] = lambda / (Q[2] * Q[0]);                           //d1d3
      H2[2][1] = lambda / (Q[2] * Q[1]);                           //d2d3
      H2[2][2] = (mu + mu * Q2[2] + lambda - lambda * LJ) / Q2[2]; //d3d3

      /*Third derivatives of Energy Density function (psi) at q*/
      ////////////////////////////////////////////////////////////////////////

      H3[0][0][0] =
        ((T)2.0 * LJ * lambda - (T)3.0 * lambda - (T)2.0 * mu) / Q3[0];//d1d1d1
      H3[0][0][1] = - lambda / (Q2[0] * Q[1]);                     //d2d1d1
      H3[0][0][2] = - lambda / (Q2[0] * Q[2]);                     //d3d1d1

      H3[0][1][0] = - lambda / (Q2[0] * Q[1]);                     //d1d2d1
      H3[0][1][1] = - lambda / (Q2[1] * Q[0]);                     //d2d2d1
      H3[0][1][2] = 0.0;                                           //d3d2d1

      H3[0][2][0] = - lambda / (Q2[0] * Q[2]);                     //d1d3d1
      H3[0][2][1] = 0.0;                                           //d2d3d1
      H3[0][2][2] = - lambda / (Q2[2] * Q[0]);                     //d3d3d1

      ////////////////////////////////////////////////////////////////////////

      H3[1][0][0] = - lambda / (Q2[0] * Q[1]);                     //d1d1d2
      H3[1][0][1] = - lambda / (Q2[1] * Q[0]);                     //d2d1d2
      H3[1][0][2] = 0.0;                                           //d3d1d2

      H3[1][1][0] = - lambda / (Q2[1] * Q[0]);                     //d1d2d2
      H3[1][1][1] =
        ((T)2.0 * LJ * lambda - (T)3.0 * lambda - (T)2.0 * mu) / Q3[1];//d2d2d2
      H3[1][1][2] = - lambda / (Q2[1] * Q[2]);                     //d3d2d2

      H3[1][2][0] = 0.0;                                           //d1d3d2
      H3[1][2][1] = - lambda / (Q2[1] * Q[2]);                     //d2d3d2
      H3[1][2][2] = - lambda / (Q2[2] * Q[1]);                     //d3d3d2

      ////////////////////////////////////////////////////////////////////////

      H3[2][0][0] = - lambda / (Q2[0] * Q[2]);                     //d1d1d3
      H3[2][0][1] = 0.0;                                           //d2d1d3
      H3[2][0][2] = - lambda / (Q2[2] * Q[0]);                     //d3d1d3

      H3[2][1][0] = 0.0;                                           //d1d2d3
      H3[2][1][1] = - lambda / (Q2[1] * Q[2]);                     //d2d2d3
      H3[2][1][2] = - lambda / (Q2[2] * Q[1]);                     //d3d2d3

      H3[2][2][0] = - lambda / (Q2[2] * Q[0]);                     //d1d3d3
      H3[2][2][1] = - lambda / (Q2[2] * Q[1]);                     //d2d3d3
      H3[2][2][2] =
        ((T)2.0 * LJ * lambda - (T)3.0 * lambda - (T)2.0 * mu) / Q3[2];//d3d3d3

      /*Fourth derivatives of Energy Density function (psi) at q*/
      H4[0][0][0][0] =
        ((T)6.0*mu + (T)11.0*lambda - (T)6.0*lambda*LJ)/Q4[0];     //d1d1d1d1
      H4[0][0][0][1] = (T)2.0 * lambda / (Q3[0] * Q[1]);           //d2d1d1d1
      H4[0][0][0][2] = (T)2.0 * lambda / (Q3[0] * Q[2]);           //d3d1d1d1

      H4[0][0][1][0] = (T)2.0 * lambda / (Q3[0] * Q[1]);           //d1d2d1d1
      H4[0][0][1][1] = lambda / (Q2[0] * Q2[1]);                   //d2d2d1d1
      H4[0][0][1][2] = (T)0.0;                                     //d3d2d1d1

      H4[0][0][2][0] = (T)2.0 * lambda / (Q3[0] * Q[2]);           //d1d3d1d1
      H4[0][0][2][1] = (T)0.0;                                     //d2d3d1d1
      H4[0][0][2][2] = lambda / (Q2[0] * Q2[2]);                   //d3d3d1d1

      H4[0][1][0][0] = (T)2.0 * lambda / (Q3[0] * Q[1]);           //d1d1d2d1
      H4[0][1][0][1] = lambda / (Q2[0] * Q2[1]);                   //d2d1d2d1
      H4[0][1][0][2] = (T)0.0;                                     //d3d1d2d1

      H4[0][1][1][0] = lambda / (Q2[0] * Q2[1]);                   //d1d2d2d1
      H4[0][1][1][1] = (T)2.0 * lambda / (Q3[1] * Q[0]);           //d2d2d2d1
      H4[0][1][1][2] = (T)0.0;                                     //d3d2d2d1

      H4[0][1][2][0] = (T)0.0;                                     //d1d3d2d1
      H4[0][1][2][1] = (T)0.0;                                     //d2d3d2d1
      H4[0][1][2][2] = (T)0.0;                                     //d3d3d2d1

      H4[0][2][0][0] = (T)2.0 * lambda / (Q3[0] * Q[2]);           //d1d1d3d1
      H4[0][2][0][1] = (T)0.0;                                     //d2d1d3d1
      H4[0][2][0][2] = lambda / (Q2[0] * Q2[2]);                   //d3d1d3d1

      H4[0][2][1][0] = (T)0.0;                                     //d1d2d3d1
      H4[0][2][1][1] = (T)0.0;                                     //d2d2d3d1
      H4[0][2][1][2] = (T)0.0;                                     //d3d2d3d1

      H4[0][2][2][0] = lambda / (Q2[0] * Q2[2]);                   //d1d3d3d1
      H4[0][2][2][1] = (T)0.0;                                     //d2d3d3d1
      H4[0][2][2][2] = (T)2.0 * lambda / (Q3[2] * Q[0]);           //d3d3d3d1

      ///////////////////////////////////////////////////////////////////////

      H4[1][0][0][0] = (T)2.0 * lambda / (Q3[0] * Q[1]);           //d1d1d1d2
      H4[1][0][0][1] = lambda / (Q2[0] * Q2[1]);                   //d2d1d1d2
      H4[1][0][0][2] = (T)0.0;                                     //d3d1d1d2

      H4[1][0][1][0] = lambda / (Q2[0] * Q2[1]);                   //d1d2d1d2
      H4[1][0][1][1] = (T)2.0 * lambda / (Q3[1] * Q[0]);           //d2d2d1d2
      H4[1][0][1][2] = (T)0.0;                                     //d3d2d1d2

      H4[1][0][2][0] = (T)0.0;                                     //d1d3d1d2
      H4[1][0][2][1] = (T)0.0;                                     //d2d3d1d2
      H4[1][0][2][2] = (T)0.0;                                     //d3d3d1d2

      H4[1][1][0][0] = lambda / (Q2[0] * Q2[1]);                   //d1d1d2d2
      H4[1][1][0][1] = (T)2.0 * lambda / (Q3[1] * Q[0]);           //d2d1d2d2
      H4[1][1][0][2] = (T)0.0;                                     //d3d1d2d2

      H4[1][1][1][0] = (T)2.0 * lambda / (Q3[1] * Q[0]);           //d1d2d2d2
      H4[1][1][1][1] =
        ((T)6.0*mu + (T)11.0*lambda - (T)6.0*lambda*LJ)/Q4[1];     //d2d2d2d2
      H4[1][1][1][2] = (T)2.0 * lambda / (Q3[1] * Q[2]);           //d3d2d2d2

      H4[1][1][2][0] = (T)0.0;                                     //d1d3d2d2
      H4[1][1][2][1] = (T)2.0 * lambda / (Q3[1] * Q[2]);           //d2d3d2d2
      H4[1][1][2][2] = lambda / (Q2[1] * Q2[2]);                   //d3d3d2d2

      H4[1][2][0][0] = (T)0.0;                                     //d1d1d3d2
      H4[1][2][0][1] = (T)0.0;                                     //d2d1d3d2
      H4[1][2][0][2] = (T)0.0;                                     //d3d1d3d2

      H4[1][2][1][0] = (T)0.0;                                     //d1d2d3d2
      H4[1][2][1][1] = (T)2.0 * lambda / (Q3[1] * Q[2]);           //d2d2d3d2
      H4[1][2][1][2] = lambda / (Q2[1] * Q2[2]);                   //d3d2d3d2

      H4[1][2][2][0] = (T)0.0;                                     //d1d3d3d2
      H4[1][2][2][1] = lambda / (Q2[1] * Q2[2]);                   //d2d3d3d2
      H4[1][2][2][2] = (T)2.0 * lambda / (Q3[2] * Q[1]);           //d3d3d3d2

      ///////////////////////////////////////////////////////////////////////

      H4[2][0][0][0] = (T)2.0 * lambda / (Q3[0] * Q[2]);           //d1d1d1d3
      H4[2][0][0][1] = (T)0.0;                                     //d2d1d1d3
      H4[2][0][0][2] = lambda / (Q2[0] * Q2[2]);                   //d3d1d1d3

      H4[2][0][1][0] = (T)0.0;                                     //d1d2d1d3
      H4[2][0][1][1] = (T)0.0;                                     //d2d2d1d3
      H4[2][0][1][2] = (T)0.0;                                     //d3d2d1d3

      H4[2][0][2][0] = lambda / (Q2[0] * Q2[2]);                   //d1d3d1d3
      H4[2][0][2][1] = (T)0.0;                                     //d2d3d1d3
      H4[2][0][2][2] = (T)2.0 * lambda / (Q3[2] * Q[0]);           //d3d3d1d3

      H4[2][1][0][0] = (T)0.0;                                     //d1d1d2d3
      H4[2][1][0][1] = (T)0.0;                                     //d2d1d2d3
      H4[2][1][0][2] = (T)0.0;                                     //d3d1d2d3

      H4[2][1][1][0] = (T)0.0;                                     //d1d2d2d3
      H4[2][1][1][1] = (T)2.0 * lambda / (Q3[1] * Q[2]);           //d2d2d2d3
      H4[2][1][1][2] = lambda / (Q2[1] * Q2[2]);                   //d3d2d2d3

      H4[2][1][2][0] = (T)0.0;                                     //d1d3d2d3
      H4[2][1][2][1] = lambda / (Q2[1] * Q2[2]);                   //d2d3d2d3
      H4[2][1][2][2] = (T)2.0 * lambda / (Q3[2] * Q[1]);           //d3d3d2d3

      H4[2][2][0][0] = lambda / (Q2[0] * Q2[2]);                   //d1d1d3d3
      H4[2][2][0][1] = (T)0.0;                                     //d2d1d3d3
      H4[2][2][0][2] = (T)2.0 * lambda / (Q3[2] * Q[0]);           //d3d1d3d3

      H4[2][2][1][0] = (T)0.0;                                     //d1d2d3d3
      H4[2][2][1][1] = lambda / (Q2[1] * Q2[2]);                   //d2d2d3d3
      H4[2][2][1][2] = (T)2.0 * lambda / (Q3[2] * Q[1]);           //d3d2d3d3

      H4[2][2][2][0] = (T)2.0 * lambda / (Q3[2] * Q[0]);           //d1d3d3d3
      H4[2][2][2][1] = (T)2.0 * lambda / (Q3[2] * Q[1]);           //d2d3d3d3
      H4[2][2][2][2] =
        ((T)6.0*mu + (T)11.0*lambda - (T)6.0*lambda*LJ)/Q4[2];     //d3d3d3d3


      if(verbose){
        message("H1");
        std::cout << H1 << std::endl;

        message("H2");
        std::cout << H2 << std::endl;

        message("H3");
        std::cout << H3[0] << std::endl;
        std::cout << H3[1] << std::endl;
        std::cout << H3[2] << std::endl;

        message("H4");
        std::cout << H4[0][0] << std::endl;
        std::cout << H4[0][1] << std::endl;
        std::cout << H4[0][2] << std::endl;
        std::cout << H4[1][0] << std::endl;
        std::cout << H4[1][1] << std::endl;
        std::cout << H4[1][2] << std::endl;
        std::cout << H4[2][0] << std::endl;
        std::cout << H4[2][1] << std::endl;
        std::cout << H4[2][2] << std::endl;
      }

      /*Extend and compute P*/
      for(int i=0;i<3;i++){
        P[i][i] = 0.0;
        T phii  = 0.0;
        T b     = 0.0;
        T bi    = 0.0;
        T c     = 0.0;
        T ci    = 0.0;

        for(int k=0;k<3;k++){
          T gk  = H1[k];

          phii += gk * dq[k][i];
          b    += gk * u[k];

          T gki = 0.0;
          for(int l=0;l<3;l++){
            T Hkl  = H2[k][l];
            T Hkli = 0.0;

            gki += Hkl * dq[l][i];
            c   += Hkl * u[k] * u[l];

            for(int m=0;m<3;m++){
              Hkli += H3[k][l][m] * dq[m][i];
            }

            ci  += Hkli * u[k] * u[l] + (T)2.0 * Hkl * du[k][i] * u[l];
          }
          bi += gki * u[k] + gk * du[k][i];
        }

        P[i][i] +=
          phii + dh[i] * b + h * bi + h * dh[i] * c + (T)0.5 * Sqr(h) * ci;

        if(verbose){
          message("phii             = %10.10e", phii);
          message("dh[i] * b        = %10.10e", dh[i] * b);
          message("h * bi           = %10.10e", h * bi);
          message("h * dh[i] * c    = %10.10e", h * dh[i] * c);
          message("1/2 * h * h * ci = %10.10e", (T)0.5 * Sqr(h) * ci);
        }
      }

      if(verbose){
        message("P");
        std::cout << P << std::endl;
      }

      /*Extend and compute H*/
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          H[i][j] = 0.0;

          T phiij = 0.0;
          T b     = 0.0;
          T bj    = 0.0;
          T bi    = 0.0;
          T bij   = 0.0;
          T c     = 0.0;
          T ci    = 0.0;
          T cj    = 0.0;
          T cij   = 0.0;

          for(int k=0;k<3;k++){
            T gk   = H1[k];
            T gkj  = 0.0;
            T gki  = 0.0;
            T gkij = 0.0;

            for(int l=0;l<3;l++){
              T Hkli  = 0.0;
              T Hklj  = 0.0;
              T Hklij = 0.0;
              T Hkl   = H2[k][l];

              for(int m=0;m<3;m++){
                T Hklm = H3[k][l][m];

                Hkli += Hklm * dq[m][i];
                Hklj += Hklm * dq[m][j];
                gkij += Hklm * dq[l][i] * dq[m][j];

                for(int n=0;n<3;n++){
                  Hklij +=
                    H4[k][l][m][n] * dq[m][i] * dq[n][j];
                }
                Hklij += Hklm * ddq[m][i][j];
              }

              gkj  += Hkl * dq[l][j];
              gki  += Hkl * dq[l][i];
              gkij += Hkl * ddq[l][i][j];

              c += Hkl * u[k] * u[l];

              ci += Hkli * u[k] * u[l] + (T)2.0 * Hkl * du[k][i] * u[l];
              cj += Hklj * u[k] * u[l] + (T)2.0 * Hkl * du[k][j] * u[l];

              cij += Hklij * u[k] * u[l] +
                ((T)2.0 * Hkli * du[k][j] * u[l] +
                 (T)2.0 * Hklj * du[k][i] * u[l]) +
                (T)2.0 * Hkl * ddu[k][i][j] * u[l] +
                (T)2.0 * Hkl * du[k][i] * du[l][j];
            }

            phiij += gkj * dq[k][i] + gk * ddq[k][i][j];

            b   += gk * u[k];
            bi  += gki * u[k] + gk * du[k][i];
            bj  += gkj * u[k] + gk * du[k][j];
            bij += gkij * u[k] +
              (gki * du[k][j] + gkj * du[k][i]) +
              gk * ddu[k][i][j];
          }

          H[i][j] =
            phiij +
            ddh[i][j] * b +
            (dh[i] * bj + dh[j] * bi) +
            h * bij +
            dh[j] * dh[i] * c +
            h * ddh[i][j] * c +
            (h * dh[i] * cj + h * dh[j] * ci) +
            (T)0.5 * Sqr(h) * cij;

          if(verbose){
            message("------%d - %d", i, j);
            message("phiij = %10.10e", phiij);
            message("ddhij * b = %10.10e", ddh[i][j] * b);
            message("dhi * bj + dhj * bi = %10.10e", dh[i] * bj + dh[j] * bi);
            message("h * bij = %10.10e", h * bij);
            message("dh[j] * dh[i] * c = %10.10e", dh[j] * dh[i] * c);
            message("h * ddh[i][j] * c = %10.10e", h * ddh[i][j] * c);
            message("h * dhi * cj + h * dhj * ci = %10.10e", h * dh[i] * cj + h * dh[j] * ci);
            message("0.5 * h2 * cij = %10.10e", 0.5*h*h*cij);
          }
        }
      }

      H[3][3] = (T)1.0;

#if 0
      Matrix44<T> eigenVectors;
      Vector4<T> eigenValues = H.eigenValues(&eigenVectors);

      bool negative = false;
      for(int l=0;l<3;l++){
        if(eigenValues[l] <= 0.0){
          negative = true;
          //error("Negative eigenvalues H");
        }
      }

      if(negative){
        warning("negative eigenvalues, %10.10e, %10.10e, %10.10e",
                eigenValues[0], eigenValues[1], eigenValues[2]);
      }

      if(negative){
        message("H ev eval");
        std::cerr << H << std::endl;
        std::cerr << eigenVectors << std::endl;
        std::cerr << eigenVectors.inverse() << std::endl;
        std::cerr << eigenVectors.transpose() << std::endl;
        std::cerr << eigenValues << std::endl;

        Matrix44<T> JJ;
        JJ[0][0] = eigenValues[0];
        JJ[1][1] = eigenValues[1];
        JJ[2][2] = eigenValues[2];

        message("reconstructed 1");
        std::cerr << eigenVectors.transpose() * JJ * eigenVectors << std::endl;
        //error("nd");

        Matrix44<T> JJJ;
        JJJ[0][0] = Pos(eigenValues[0]);
        JJJ[1][1] = Pos(eigenValues[1]);
        JJJ[2][2] = Pos(eigenValues[2]);

        JJJ[0][0] = Abs(eigenValues[0]);
        JJJ[1][1] = Abs(eigenValues[1]);
        JJJ[2][2] = Abs(eigenValues[2]);

        std::cerr << JJJ << std::endl;

        message("reconstructed 2");
        H = eigenVectors.transpose() * JJJ * eigenVectors;
        std::cerr << H << std::endl;
        //error("nd");
      }else{
        //message("eigenvalues");
        //std::cout << eigenValues << std::endl;
      }
#endif

      if(verbose){
        message("H");
        std::cout << H << std::endl;
      }
    }else{
      /*Linearize at F*/
      T J = F[0][0] * F[1][1] * F[2][2];

      T LJ = Log(J);

      Vector4<T> Q(F[0][0], F[1][1], F[2][2], 0);
      Vector4<T> Q2(Sqr(Q[0]), Sqr(Q[1]), Sqr(Q[2]), 0);

      if(Abs(Abs(J) - (T)1.0) < 1e-12){
        LJ = 0.0;
      }

      if(verbose){
        message("J = %10.10e, Log(J) = %10.10e", J, LJ);
        message("J = %10.10e, Log(J) = %10.10e", J, LJ);
        message("%20.20e, %20.20e, %20.20e", Q[0], Q[1], Q[2]);
      }

      P[0][0] = mu * Q[0] - mu / Q[0] + lambda * LJ / Q[0];
      P[1][1] = mu * Q[1] - mu / Q[1] + lambda * LJ / Q[1];
      P[2][2] = mu * Q[2] - mu / Q[2] + lambda * LJ / Q[2];

      H[0][0] = mu + (mu + lambda - lambda * LJ)/Q2[0];
      H[0][1] = lambda / (Q[0] * Q[1]);
      H[0][2] = lambda / (Q[0] * Q[2]);

      H[1][0] = lambda / (Q[1] * Q[0]);
      H[1][1] = mu + (mu + lambda - lambda * LJ)/Q2[1];
      H[1][2] = lambda / (Q[1] * Q[2]);

      H[2][0] = lambda / (Q[2] * Q[0]);
      H[2][1] = lambda / (Q[2] * Q[1]);
      H[2][2] = mu + (mu + lambda - lambda * LJ)/Q2[2];

      MQ = F;
    }
  }

  template<class T>
  void HyperElasticityFEM<T>::computeForceDerivative(Matrix44<T>& der,
                                                     Matrix44<T>& derRL,
                                                     Matrix44<T>& derRR,
                                                     Matrix44<T>& derDH,
                                                     Matrix44<T>& U,
                                                     Matrix44<T>& D,
                                                     Matrix44<T>& Q,
                                                     Matrix44<T>& V,
                                                     Matrix44<T>& VT,
                                                     Matrix44<T>& P,
                                                     Matrix44<T>& H,
                                                     Matrix44<T>& G,
                                                     int x, int y,
                                                     bool verbose){
    Matrix44<T> dU, dD, dV, dVT;
    SVDDerivative<T, 3>(&dU, &dD, &dV, &U, &D, &VT, x, y, verbose);

    dVT = dV;
    dV = dVT.transpose();

    T detDU = dU.det();
    T detDV = dV.det();

    if(Abs(detDU) > 1e-8 || Abs(detDV) > 1e-8){
      warning("derivatives have non zero determinant");
      std::cerr << dU << std::endl;
      std::cerr << dD << std::endl;
      std::cerr << dV << std::endl;
      std::cerr << U << std::endl;
      std::cerr << D << std::endl;
      std::cerr << V << std::endl;

      error("stop");
    }

    if(verbose){
      message("U D V");
      std::cout << U << std::endl;
      std::cout << D << std::endl;
      std::cout << V << std::endl;

      message("dU dD dV, HD");
      std::cout << dU << std::endl;
      std::cout << dD << std::endl;
      std::cout << dV << std::endl;
    }

    Matrix44<T> HD;

    for(int i=0;i<3;i++){
      HD[i][i] = (H[0][i] * dD[0][0] +
                  H[1][i] * dD[1][1] +
                  H[2][i] * dD[2][2]);
    }

    HD[3][3] = (T)1.0;

    //if(verbose){
    if(true){


      //Matrix44<T> HD = *H1 * dD[0][0] + *H2 * dD[1][1] + *H3 * dD[2][2];
      Matrix44<T> RR = dU * D * VT + U * dD * VT + U * D * dVT;
      //Matrix44<T> RR = U * dD * VT;

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          if(i==x && j==y){
            if(Abs(RR[x][y] - (T)1.0) > 1e-3){
              message("svd derivative error");
              std::cout << RR << std::endl;
              std::cout << HD << std::endl;

              message("U D V");
              std::cout << U << std::endl;
              std::cout << D << std::endl;
              std::cout << V << std::endl;

              message("dU dD dV");
              std::cout << dU << std::endl;
              std::cout << dD << std::endl;
              std::cout << dV << std::endl;

              message("G");
              std::cout << G << std::endl;


              getchar();
              SVDDerivative<T, 3>(&dU, &dD, &dV, &U, &D, &VT, x, y, true);
              error("SVDDerivative");
            }
          }else{
            if(Abs(RR[i][j]) > 1e-3){
              message("svd derivative error");
              std::cout << RR << std::endl;
              std::cout << HD << std::endl;

              message("U D V");
              std::cout << U << std::endl;
              std::cout << D << std::endl;
              std::cout << V << std::endl;

              message("dU dD dV");
              std::cout << dU << std::endl;
              std::cout << dD << std::endl;
              std::cout << dV << std::endl;

              message("G");
              std::cout << G << std::endl;

              getchar();
              SVDDerivative<T, 3>(&dU, &dD, &dV, &U, &D, &VT, x, y, true);
              error("SVDDerivative");
            }
          }
        }
      }
    }

    der = dU * P * VT + U * HD * VT + U * P * dVT;
#if 1
    derDH = U * HD * VT;

    derRL = dU * P * VT;// + U * P * dV;

    derRR = /*dU * P * VT +*/ U * P * dVT;
#endif

    if(verbose){
      message("derivative components");
      std::cout << dU * P * VT << std::endl;
      std::cout << U * HD * VT << std::endl;
      std::cout << U * P * dVT << std::endl;

      message("der");
      std::cout << der << std::endl;

      getchar();
    }
  }

  template class HyperElasticityFEM<float>;
  template class HyperElasticityFEM<double>;

}
