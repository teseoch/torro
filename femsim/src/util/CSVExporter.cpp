/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "util/CSVExporter.hpp"
#include <string.h>

namespace tsl{
  /**************************************************************************/
  CSVExporter::CSVExporter(FILE* f){
    file = f;
    headerWritten = false;
    n_columns = 0;
  }

  /**************************************************************************/
  CSVExporter::~CSVExporter(){
  }

  /**************************************************************************/
  void CSVExporter::addColumn(const char* columnName){
    std::map<std::string, std::string>::iterator it;
    std::string key(columnName);

    it = valueMap.find(key);
    if(it == valueMap.end()){
      /*Column does not exist, create an entry*/
      std::string value;
      valueMap[key] = value;
      indexMap[n_columns] = key;
      n_columns++;
    }else{
      /*Column already exists*/
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, float value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%10.10e%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, double value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%10.10e%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, int value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%d%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, uint value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%d%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, long value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%ld%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, ulong value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      char buffer[255];
      sprintf(buffer, "%lu%c", value,'\0');
      std::string strval(buffer);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::setValue(const char* column, const char* value){
    std::map<std::string, std::string>::iterator it;
    std::string key(column);

    it = valueMap.find(key);

    if(it != valueMap.end()){
      std::string strval(value);
      valueMap[key] = strval;
    }else{
      warning("Column %s unknown", column);
    }
  }

  /**************************************************************************/
  void CSVExporter::saveHeader(){
    for(int i=0;i<n_columns;i++){
      fprintf(file, "%s", indexMap[i].c_str());

      if(i+1 == n_columns){
        fprintf(file, "\n");
      }else{
        fprintf(file, ";");
      }
    }

    headerWritten = true;
  }

  /**************************************************************************/
  void CSVExporter::saveRow(){
    if(!headerWritten){
      saveHeader();
    }

    for(int i=0;i<n_columns;i++){
      fprintf(file, "%s", valueMap[indexMap[i]].c_str());

      if(i+1 == n_columns){
        fprintf(file, "\n");
      }else{
        fprintf(file, ";");
      }
    }
    fflush(file);
  }
}
