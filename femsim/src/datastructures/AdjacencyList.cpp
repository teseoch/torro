/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "core/tsldefs.hpp"
#include "datastructures/AdjacencyList.hpp"
#include "core/Exception.hpp"
#include <string.h>
#include <ostream>
#include <fstream>
namespace tsl{

#ifdef ADJV2
  /**************************************************************************/
  AdjacencyList::AdjacencyList(long _size, long _max_degree){
    size = _size;
    max_degree = _max_degree;

    adj     = new int*[size];
    sizes   = new int[size];
    degrees = new int[size];

    /*Initialize arrays*/

    for(int i=0;i<size;i++){
      adj[i] = new int[max_degree];
      sizes[i] = (int)max_degree;
      degrees[i] = 0;

      for(int j=0;j<sizes[i];j++){
        adj[i][j] = undefinedIndex;
      }
    }

    if(max_degree <= 0){
      error("can not initialize a zero sized array");
    }
  }

  /**************************************************************************/
  AdjacencyList::~AdjacencyList(){
    for(int i=0;i<size;i++){
      delete[] adj[i];
    }
    delete[] adj;
    delete[] sizes;
    delete[] degrees;
  }
#else
  /**************************************************************************/
  AdjacencyList::AdjacencyList(long _size, long _max_degree){
    size = _size;
    max_degree = _max_degree+1;
    size_t s = (uint)size * (uint)max_degree;
    adj = new int[s];
    memset(adj, 0, sizeof(int)*s);
    message("list size = %l bytes", sizeof(int) * s);
    message("size = %d, max_degree = %d", size, max_degree);

    if(max_degree <= 0){
      error("can not initialize a zero sized array");
    }
  }

  /**************************************************************************/
  AdjacencyList::~AdjacencyList(){
    delete [] adj;
  }

#endif


#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::readFromFile(std::ifstream& file){
  }

  /**************************************************************************/
  void AdjacencyList::saveToFile(std::ofstream& file)const{
  }
#else
  /**************************************************************************/
  void AdjacencyList::readFromFile(std::ifstream& file){
    /*Read size from file*/
    file.read((char*)&size, sizeof(long));
    /*Read max_degree from file*/
    file.read((char*)&max_degree, sizeof(long));

    /*Free data*/
    if(adj){delete[] adj;}

    /*Allocate data*/
    adj = new int[size * max_degree];

    /*Read adj from file*/
    file.read((char*)adj, (long)sizeof(int) * size * max_degree);
    message("list size = %l bytes", (long)sizeof(int) * size * max_degree);
    message("size = %d, max_degree = %d", size, max_degree);
  }

  /**************************************************************************/
  void AdjacencyList::saveToFile(std::ofstream& file)const{
    /*Write size to file*/
    file.write((char*)&size, sizeof(long));
    /*Write max_degree to file*/
    file.write((char*)&max_degree, sizeof(long));

    /*Write adj to file*/
    file.write((char*)adj, (long)sizeof(int) * size * max_degree);
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::connect(int a, int b){
    tslassert(a >= 0);

    DBG(message("connect %d, %d", a, b));
    while(a >= size){
      extendSize();
    }

    tslassert(a < size);

    if(degrees[a] == sizes[a]){
      /*extend adj*/
      int* tmp = new int[sizes[a]*2];
      for(int i=0;i<sizes[a];i++){
        tmp[i] = adj[a][i];
      }
      for(int i=sizes[a];i<sizes[a]*2;i++){
        tmp[i] = undefinedIndex;
      }

      delete[] adj[a];
      adj[a] = tmp;
      sizes[a]*=2;
    }

    adj[a][degrees[a]] = b;
    degrees[a]++;
  }
#else
  /**************************************************************************/
  void AdjacencyList::connect(int a, int b){
    tslassert(a >= 0);

    DBG(message("connect %d, %d", a, b));
    while(a >= size){
      extendSize();
    }

    tslassert(a < size);

    int n = adj[a * max_degree];
    if(n == (max_degree - 1)){
      //message("extending degrees for %d, degree = %d", a, n);
      for(int i=0;i<n;i++){
        //message("Adding %d, stored[%d] = %d", b, i, getConnectedItem(a, i));
      }
      //throw new AlgException(__LINE__, __FILE__, "Degree error");
      extendDegree();
    }

    adj[a*max_degree + 1 + n] = b;
    adj[a*max_degree]++;
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::disconnect(int a, int b){
    tslassert(a >= 0);
    tslassert(a < size);

    DBG(message("disconnect %d, %d", a, b));

    int n = degrees[a];
    int idx = undefinedIndex;
    for(int i=0;i<n;i++){
      if(adj[a][i] == b){
        idx = i;
        break;
      }
    }

    if(idx == undefinedIndex){
      error("Can not happen");
    }

    /*swap elements*/
    degrees[a]--;
    adj[a][idx] = adj[a][degrees[a]];
    adj[a][degrees[a]] = undefinedIndex;
  }
#else
  /**************************************************************************/
  void AdjacencyList::disconnect(int a, int b){
    tslassert(a >= 0);
    tslassert(a < size);

    DBG(message("disconnect %d, %d", a, b));

    int n = adj[a*max_degree];
    int idx = undefinedIndex;
    for(int i=0;i<n;i++){
      if(adj[a*max_degree + 1 + i] == b){
        idx = i;
        break;
      }
    }

    if(idx == undefinedIndex){
      error("Can not happen");
    }

    /*Swap elements*/
    adj[a*max_degree]--;
    adj[a*max_degree + 1 + idx] = adj[a*max_degree + 1 + adj[a*max_degree]];
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  int AdjacencyList::getDegree(int a)const{
    if((long)a < size && (long)a >= 0){
      /*This should be safe. If out of bounds, just return zero. Later
        on, when an out of bounds element is connected for the first
        time, the array is extended.*/
      return degrees[a];
    }else{
      error("Asking for degree of non existing vertex, a = %d, size = %d",
            a, size);
      return 0;
    }
  }
#else
  /**************************************************************************/
  int AdjacencyList::getDegree(int a)const{
    if((long)a < size && (long)a >= 0){
      /*This should be safe. If out of bounds, just return zero. Later
        on, when an out of bounds element is connected for the first
        time, the array is extended.*/
      return adj[a * max_degree];
    }else{
      error("Asking for degree of non existing vertex, a = %d, size = %d",
            a, size);
      return 0;
    }
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  int AdjacencyList::getConnectedItem(int a, int idx)const{
    tslassert(a < size);
    tslassert(a >= 0);
    tslassert(idx < degrees[a]);
    int v = adj[a][idx];
    if(v == undefinedIndex){
      error("undefined index");
    }
    return v;
  }

  void AdjacencyList::clear(int a){
    tslassert(a < size);
    tslassert(a >= 0);
    degrees[a] = 0;
    for(int i=0;i<sizes[a];i++){
      adj[a][i] = undefinedIndex;
    }
  }
#else
  /**************************************************************************/
  int AdjacencyList::getConnectedItem(int a, int idx)const{
    tslassert(a < size);
    tslassert(a >= 0);
    tslassert(idx < adj[a * max_degree]);
    return adj[a * max_degree + idx + 1];
  }

  /**************************************************************************/
  void AdjacencyList::clear(int a){
    tslassert(a < size);
    tslassert(a >= 0);
    adj[a * max_degree] = 0;
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::extendSize(){
    error("extendSize called");
  }

  /**************************************************************************/
  void AdjacencyList::extendDegree(){
    error("extendDegree called");
  }
#else
  /**************************************************************************/
  void AdjacencyList::extendSize(){
    /*Extend neighbor information*/
    int* new_adj = new int[size*2*max_degree];
    tslassert(new_adj != 0);

    size_t s = (size_t)(size * 2 * max_degree);

    memset(new_adj, 0, sizeof(int)*s);

    message("list size = %l bytes, p = %p",
            (long)sizeof(int) * size * max_degree * 2, new_adj);

    for(long i=0;i<size*2;i++){
      long vIndex = i*max_degree;

      if(i<size){
        int n = adj[vIndex];

        tslassert(n <= (max_degree -1));

        new_adj[vIndex] = n;

        for(int j=0;j<n;j++){
          new_adj[vIndex + j + 1] = adj[vIndex + j + 1];
        }
      }else{
        tslassert(vIndex < size*2*max_degree);
        new_adj[vIndex] = 0;
      }
    }
    delete [] adj;
    adj = new_adj;
    size*=2;
  }

  /**************************************************************************/
  void AdjacencyList::extendDegree(){
    PRINT_FUNCTION;

    long new_max_degree = max_degree+12;
    message("max_degree = %d, new_max_degree = %d", max_degree, new_max_degree);

    size_t s = (size_t)(size * new_max_degree);
    int* new_adj = new int[s];

    tslassert(new_adj != 0);

    memset(new_adj, 0, sizeof(int)*s);

    message("list size = %l bytes, p = %p",
            (long)sizeof(int) * (long)s * max_degree, new_adj);

    for(int i=0;i<size;i++){
      long vIndex = i*max_degree;
      int n = adj[vIndex];

      long nvIndex = i*new_max_degree;
      new_adj[nvIndex] = n;


      tslassert(n <= (max_degree -1));
      /*Copy adjacency vector/matrix*/
      /*length, t[0], t[1], t[2], t[3], t[4]*/
      /*length = 5, max_degree = 5, max_degree+1 = 6*/
      for(int j=0;j<n;j++){
        new_adj[nvIndex + j + 1] = adj[vIndex + j + 1];
      }
    }
    delete [] adj;
    adj = new_adj;
    max_degree = new_max_degree;
  }

#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::remap(Tree<int>& mapping){
    for(int i=0;i<size;i++){
      int degree = degrees[i];

      for(int j=0;j<degree;j++){
        int old_index = adj[i][j];
        int new_index = mapping.findIndex(old_index);
        tslassert(new_index != undefinedIndex);
        adj[i][j] = new_index;
      }
    }
  }
#else
  /**************************************************************************/
  void AdjacencyList::remap(Tree<int>& mapping){
    error("called?");
    for(int i=0;i<size;i++){
      int degree = getDegree(i);

      for(int j=0;j<degree;j++){
        int old_index = adj[i*max_degree+1+j];
        int new_index = mapping.findIndex(old_index);
        tslassert(new_index != undefinedIndex);
        adj[i*max_degree+1+j] = new_index;
      }
    }
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::compress(Tree<int>& mapping, int new_size){
  }
#else

  /**************************************************************************/
  void AdjacencyList::compress(Tree<int>& mapping, int new_size){
    int* new_adj = new int[new_size * max_degree];
    message("list size = %l bytes", (long)sizeof(int) * new_size * max_degree);
    message("new_size = %d, max_degree = %d", new_size, max_degree);
    message("old size = %l", size);
    int count = 0;

    size_t s = (size_t)(new_size*max_degree);
    memset(new_adj, 0, sizeof(int)*s);

    for(int i=0;i<(int)size;i++){
      int degree = getDegree(i);

      if(degree != 0){
        /*Copy data*/
        int new_index = mapping.findIndex(i);
        tslassert(new_index != undefinedIndex);
        tslassert(new_index < (int)new_size);

        for(int j=0;j<max_degree;j++){
          new_adj[new_index * max_degree + j] = adj[i*max_degree + j];
        }
        count++;
      }
    }

    delete[] adj;
    adj = new_adj;
    size = new_size;
  }
#endif

#ifdef ADJV2
  /**************************************************************************/
  void AdjacencyList::print(){
  }

#else

  /**************************************************************************/
  void AdjacencyList::print(){
    for(int i=0;i<size;i++){
      long vIndex = i * max_degree;

      int n = adj[vIndex];

      std::cout << i << " | " << n << " | ";

      for(int j=0;j<n;j++){
        std::cout << adj[vIndex + j + 1] << " ";
      }

      std::cout << std::endl;
    }
  }
#endif
}
