#include "math/SVD.hpp"
#include "math/Matrix22.hpp"

namespace tsl{

#define SIGN(a, b) ((b) >= 0.0 ? Abs(a) : -Abs(a))


#define SVDTYPE double
  /**************************************************************************/
  /*Adopted from Numerical Recipes,
    http://numerical.recipes/webnotes/nr3web2.pdf*/
  template<class T, int d>
  void SVD(const Matrix44<T>& A, Matrix44<T>& U,
           Matrix44<T>& S, Matrix44<T>& V){
    const int m = d;
    const int n = d;
    int flag, i, its, j, jj, k, l, nm;
    SVDTYPE c, f, h, s, x, y, z;
    SVDTYPE anorm = (SVDTYPE)0.0;
    SVDTYPE g     = (SVDTYPE)0.0;
    SVDTYPE scale = (SVDTYPE)0.0;
    SVDTYPE rvl[4];
    Matrix44<T> AA = A;
    Vector4<T>  w;

    if(m < n){
      error("#rows must be > #cols");
    }

    /*Householder reduction to bidiagonal form*/
    for(i = 0; i < n; i++){
      /*left-hand reduction*/
      l = i + 1;
      rvl[i] = scale * g;
      g = s = scale = (SVDTYPE)0.0;
      if(i < m){
        for(k = i; k < m; k++){
          scale += Abs((SVDTYPE)AA[k][i]);
        }
        if(scale){
          for(k = i; k < m; k++){
            AA[k][i] = (T)((SVDTYPE)AA[k][i]/scale);
            s += ((SVDTYPE)AA[k][i] * (SVDTYPE)AA[k][i]);
          }
          f = (SVDTYPE)AA[i][i];
          g = -SIGN(Sqrt(s), f);
          h = f * g - s;
          AA[i][i] = (T)(f - g);
          if(i != n - 1){
            for(j = l; j < n; j++){
              for(s = (SVDTYPE)0.0, k = i; k < m; k++){
                s += ((SVDTYPE)AA[k][i] * (SVDTYPE)AA[k][j]);
              }
              f = s / h;
              for(k = i; k < m; k++){
                AA[k][j] += (T)(f * (SVDTYPE)AA[k][i]);
              }
            }
          }
          for(k = i; k < m; k++){
            AA[k][i] = (T)((SVDTYPE)AA[k][i]*scale);
          }
        }
      }
      w[i] = (T)(scale * g);

      /*Right-hand reduction*/
      g = s = scale = (SVDTYPE)0.0;
      if(i < m && i != n - 1){
        for(k = l; k < n; k++){
          scale += Abs((SVDTYPE)(AA[i][k]));
        }
        if(scale){
          for(k = l; k < n; k++){
            AA[i][k] = (T)((SVDTYPE)AA[i][k] / scale);
            s += ((SVDTYPE)AA[i][k] * (SVDTYPE)AA[i][k]);
          }
          f = (SVDTYPE)AA[i][l];
          g = -SIGN(Sqrt(s), f);
          h = f * g - s;
          AA[i][l] = (T)(f - g);
          for(k = l; k < n; k++){
            rvl[k] = (SVDTYPE)AA[i][k] / h;
          }
          if(i != m - 1){
            for(j = l; j < m; j++){
              for(s = (SVDTYPE)0.0, k = l; k < n; k++){
                s += ((SVDTYPE)AA[j][k] * (SVDTYPE)AA[i][k]);
              }
              for(k = l; k < n; k++){
                AA[j][k] += (T)(s * rvl[k]);
              }
            }
          }
          for(k = l; k < n; k++){
            AA[i][k] = (T)((SVDTYPE)AA[i][k] * scale);
          }
        }
      }
      anorm = Max(anorm, (Abs((SVDTYPE)w[i]) + Abs(rvl[i])));
    }

    /*Accumulate the right-hand transform*/
    for(i = n - 1; i >= 0; i--){
      if(i < n - 1){
        if(g){
          for(j = l; j < n; j++){
            V[j][i] = (T)(((SVDTYPE)AA[i][j] / (SVDTYPE)AA[i][l]) / g);
          }
          /*Double division to avoid underflow*/
          for(j = l; j < n; j++){
            for(s = (SVDTYPE)0.0, k = l; k < n; k++){
              s += ((SVDTYPE)AA[i][k] * (SVDTYPE)V[k][j]);
            }
            for(k = l; k < n; k++){
              V[k][j] += (T)(s * (SVDTYPE)V[k][i]);
            }
          }
        }
        for(j = l; j < n; j++){
          V[i][j] = V[j][i] = 0.0;
        }
      }
      V[i][i] = 1.0;
      g = rvl[i];
      l = i;
    }

    /*Accumulate the left-hand transformation*/
    for(i = n - 1; i >= 0; i--){
      l = i + 1;
      g = (SVDTYPE)w[i];
      if(i < n - 1){
        for(j = l; j < n; j++){
          AA[i][j] = 0.0;
        }
      }
      if(g){
        g = 1.0 / g;
        if(i != n - 1){
          for(j = l; j < n; j++){
            for(s = (SVDTYPE)0.0, k = l; k < m; k++){
              s += ((SVDTYPE)AA[k][i] * (SVDTYPE)AA[k][j]);
            }
            f = (s / (SVDTYPE)AA[i][i]) * g;
            for(k = i; k < m; k++){
              AA[k][j] += (T)(f * (SVDTYPE)AA[k][i]);
            }
          }
        }
        for(j = i; j < m; j++){
          AA[j][i] = (T)((SVDTYPE)AA[j][i] * g);
        }
      }else{
        for(j = i; j < m; j++){
          AA[j][i] = 0.0;
        }
      }
      ++AA[i][i];
    }

    /*diagonalize the bidiagonal form*/
    if(d > 1){
      for(k = n - 1; k >= 0; k--){
        for(its = 0; its < 30; its++){
          flag = 1;
          for(l = k; l >= 0; l--){
            /*test for splitting*/
            nm = l - 1;
            if(Abs(rvl[l]) + anorm == anorm){
              flag = 0;
              break;
            }
            if(Abs((SVDTYPE)w[nm]) + anorm == anorm){
              break;
            }
          }
          if(flag){
            c = (SVDTYPE)0.0;
            s = (SVDTYPE)1.0;
            for(i = l; i <= k; i++){
              f = s * rvl[i];
              if(Abs(f) + anorm != anorm){
                g = (SVDTYPE)w[i];
                h = pythagoras(f, g);
                w[i] = (T)h;
                h = 1.0 / h;
                c = g * h;
                s = (-f * h);
                for(j = 0; j < m; j++){
                  y = (SVDTYPE)AA[j][nm];
                  z = (SVDTYPE)AA[j][i];
                  AA[j][nm] = (T)(y * c + z * s);
                  AA[j][i]  = (T)(z * c - y * s);
                }
              }
            }
          }
          z = (SVDTYPE)w[k];
          if(l == k){
            /*convergence*/
#if 1
            if(z < 0.0){
            /*make singular value nonnegative*/
              w[k] = (T)(-z);
              for(j = 0; j < n; j++){
                V[j][k] = -V[j][k];
              }
            }
#endif
            break;
          }
          if(its >= 30){
            error("SVD::No convergence");
          }

          /*Shift from bottom 2x2 minor*/
          x = (SVDTYPE)w[l];
          nm = k - 1;
          y = (SVDTYPE)w[nm];
          g = rvl[nm];
          h = rvl[k];
          f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
          g = pythagoras(f, (SVDTYPE)1.0);
          f = ((x - z) * (x + z) + h * ((y / (f + SIGN(g, f))) - h)) / x;

          /*Next QR transformation*/
          c = s = (SVDTYPE)1.0;
          for(j = l; j <= nm; j++){
            i = j + 1;
            g = rvl[i];
            y = (SVDTYPE)w[i];
            h = s * g;
            g = c * g;
            z = pythagoras(f, h);
            rvl[j] = z;
            c = f / z;
            s = h / z;
            f = x * c + g * s;
            g = g * c - x * s;
            h = y * s;
            y = y * c;
            for(jj = 0; jj < n; jj++){
              x = (SVDTYPE)V[jj][j];
              z = (SVDTYPE)V[jj][i];
              V[jj][j] = (T)(x * c + z * s);
              V[jj][i] = (T)(z * c - x * s);
            }
            z = pythagoras(f, h);
            w[j] = (T)z;
            if(z){
              z = 1.0 / z;
              c = f * z;
              s = h * z;
            }
            f = (c * g) + (s * y);
            x = (c * y) - (s * g);
            for(jj = 0; jj < m; jj++){
              y = (SVDTYPE)AA[jj][j];
              z = (SVDTYPE)AA[jj][i];
              AA[jj][j] = (T)(y * c + z * s);
              AA[jj][i] = (T)(z * c - y * s);
            }
          }
          rvl[l] = (SVDTYPE)0.0;
          rvl[k] = f;
          w[k] = (T)x;
        }
      }
    }

    T sw;
    T su[4];
    T sv[4];
    int inc = 1;

    /*Sort*/
    do{
      inc *= 3;
      inc++;
    }while(inc <= n);

    do{
      inc /= 3;
      for(i = inc; i < n; i++){
        sw = w[i];
        for(k=0;k<m;k++){
          su[k] = AA[k][i];
        }
        for(k=0;k<n;k++){
          sv[k] = V[k][i];
        }
        j = i;
        while(w[j-inc] < sw){
          w[j] = w[j-inc];
          for(k=0;k<m;k++) AA[k][j] = AA[k][j-inc];
          for(k=0;k<n;k++)  V[k][j] = V[k][j-inc];
          j -= inc;
          if(j < inc)break;
        }
        w[j] = sw;
        for(k=0;k<m;k++) AA[k][j] = su[k];
        for(k=0;k<n;k++)  V[k][j] = sv[k];
      }
    }while(inc > 1);

    for(k=0;k<n;k++){
      s = 0;
      for(i=0;i<m;i++)if(AA[i][k] < 0.0) s++;
      for(j=0;j<n;j++)if( V[j][k] < 0.0) s++;
      if(s > (m+n)/2){
        for(i=0;i<m;i++) AA[i][k] = -AA[i][k];
        for(j=0;j<n;j++)  V[j][k] =  -V[j][k];
      }
    }

    U = AA;
    for(int i=0;i<d;i++){
      S[i][i] = w[i];
    }
    for(int i = d; i < 4; i++){
      U[i][i] = (T)1.0;
      V[i][i] = (T)1.0;
      S[i][i] = (T)1.0;
    }
  }

  /**************************************************************************/
  /*As described in "Estimating the Jacobian of the Singular Value
    Decomposition: Theory and Applications" T. Papadopoulo and
    M.I.A. Lourakis*/
  template<class T, int d>
  void SVDDerivative(Matrix44<T>*dU, Matrix44<T>* dD, Matrix44<T>* dV,
                     Matrix44<T>* U, Matrix44<T>*  D, Matrix44<T>* VT,
                     int x, int y, bool verbose){
    Matrix44<T> omegaU;
    Matrix44<T> omegaV;

    for(int i=0;i<d;i++){
      (*dD)[i][i] = (*VT)[i][y] * (*U)[x][i];
    }

    if(verbose){
      message("D = ");
      std::cout << *D << std::endl;
    }

    for(int k=0;k<d;k++){
      for(int l=k;l<d;l++){
        if(k==l){
          omegaU[k][l] = 0.0;
          omegaV[k][l] = 0.0;
        }else{
          T l1 = (*D)[l][l];
          T l2 = (*D)[k][k];

          Matrix22<T> M(l1, l2, l2, l1);
          Vector2<T>  R(  (*U)[x][k] * (*VT)[l][y],
                         -(*U)[x][l] * (*VT)[k][y]);

          if(verbose){
            message("R = ");
            std::cout << R << std::endl;
            message("M = ");
            std::cout << M << std::endl;

          }

          Vector2<T> RR = R;

          //Matrix22<T> I = Matrix22<T>::Identity * Sqr((T)0.00001);
          //Matrix22<T> I = Matrix22<T>::Identity * Sqr((T)0.001);

          //R = (M*M + I).inverse() * (M*R);
          R = M.inverse() * R;

          if(verbose){
            message("R = ");
            std::cout << R << std::endl;
            RR = M.inverse() * RR;
            std::cout << RR << std::endl;
          }

          /*Matrices dU and dV are skew symmetric*/
          omegaU[k][l] =  R[0];
          omegaU[l][k] = -R[0];

          omegaV[k][l] =  R[1];
          omegaV[l][k] = -R[1];
        }
      }
    }

    if(verbose){
      message("omega UV");
      std::cout << omegaU << std::endl;
      std::cout << omegaV << std::endl;
    }

    *dU = *U * omegaU;
    *dV = omegaV * *VT;
  }

  template void SVD<float, 1>(const Matrix44<float>& A, Matrix44<float>& U,
                              Matrix44<float>& S, Matrix44<float>& V);
  template void SVD<float, 2>(const Matrix44<float>& A, Matrix44<float>& U,
                              Matrix44<float>& S, Matrix44<float>& V);
  template void SVD<float, 3>(const Matrix44<float>& A, Matrix44<float>& U,
                              Matrix44<float>& S, Matrix44<float>& V);
  template void SVD<float, 4>(const Matrix44<float>& A, Matrix44<float>& U,
                              Matrix44<float>& S, Matrix44<float>& V);

  template void SVD<double, 1>(const Matrix44<double>& A, Matrix44<double>& U,
                               Matrix44<double>& S, Matrix44<double>& V);
  template void SVD<double, 2>(const Matrix44<double>& A, Matrix44<double>& U,
                               Matrix44<double>& S, Matrix44<double>& V);
  template void SVD<double, 3>(const Matrix44<double>& A, Matrix44<double>& U,
                               Matrix44<double>& S, Matrix44<double>& V);
  template void SVD<double, 4>(const Matrix44<double>& A, Matrix44<double>& U,
                               Matrix44<double>& S, Matrix44<double>& V);

  template void SVDDerivative<float, 1>(Matrix44<float>* dU,
                                        Matrix44<float>* dD,
                                        Matrix44<float>* dV,
                                        Matrix44<float>* U,
                                        Matrix44<float>* D,
                                        Matrix44<float>* V,
                                        int x, int y, bool verbose);
  template void SVDDerivative<float, 2>(Matrix44<float>* dU,
                                        Matrix44<float>* dD,
                                        Matrix44<float>* dV,
                                        Matrix44<float>* U,
                                        Matrix44<float>* D,
                                        Matrix44<float>* V,
                                        int x, int y, bool verbose);
  template void SVDDerivative<float, 3>(Matrix44<float>* dU,
                                        Matrix44<float>* dD,
                                        Matrix44<float>* dV,
                                        Matrix44<float>* U,
                                        Matrix44<float>* D,
                                        Matrix44<float>* V,
                                        int x, int y, bool verbose);
  template void SVDDerivative<float, 4>(Matrix44<float>* dU,
                                        Matrix44<float>* dD,
                                        Matrix44<float>* dV,
                                        Matrix44<float>* U,
                                        Matrix44<float>* D,
                                        Matrix44<float>* V,
                                        int x, int y, bool verbose);

  template void SVDDerivative<double, 1>(Matrix44<double>* dU,
                                         Matrix44<double>* dD,
                                         Matrix44<double>* dV,
                                         Matrix44<double>* U,
                                         Matrix44<double>* D,
                                         Matrix44<double>* V,
                                         int x, int y, bool verbose);
  template void SVDDerivative<double, 2>(Matrix44<double>* dU,
                                         Matrix44<double>* dD,
                                         Matrix44<double>* dV,
                                         Matrix44<double>* U,
                                         Matrix44<double>* D,
                                         Matrix44<double>* V,
                                         int x, int y, bool verbose);
  template void SVDDerivative<double, 3>(Matrix44<double>* dU,
                                         Matrix44<double>* dD,
                                         Matrix44<double>* dV,
                                         Matrix44<double>* U,
                                         Matrix44<double>* D,
                                         Matrix44<double>* V,
                                         int x, int y, bool verbose);
  template void SVDDerivative<double, 4>(Matrix44<double>* dU,
                                         Matrix44<double>* dD,
                                         Matrix44<double>* dV,
                                         Matrix44<double>* U,
                                         Matrix44<double>* D,
                                         Matrix44<double>* V,
                                         int x, int y, bool verbose);

}
