/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "math/Matrix44.hpp"
#include "math/Matrix22.hpp"
#include "core/UnitTest.hpp"

namespace tsl{

  /*UnitTests*/
  /****************************************************************************/
#define COMPONENT_0 3
#define COMPONENT_1 5
#define COMPONENT_2 7
#define COMPONENT_3 11
#define COMPONENT_4 13
#define COMPONENT_5 17
#define COMPONENT_6 19
#define COMPONENT_7 23
#define COMPONENT_8 29
#define COMPONENT_9 31
#define COMPONENT_10 37
#define COMPONENT_11 41
#define COMPONENT_12 43
#define COMPONENT_13 47
#define COMPONENT_14 53
#define COMPONENT_15 59

#define COMPONENT_SQ_0  (2.0*375.0)
#define COMPONENT_SQ_1  (2.0*417.0)
#define COMPONENT_SQ_2  (2.0*479.0)
#define COMPONENT_SQ_3  (2.0*542.0)
#define COMPONENT_SQ_4  (2.0*900.0)
#define COMPONENT_SQ_5  (2.0*1012.0)
#define COMPONENT_SQ_6  (2.0*1168.0)
#define COMPONENT_SQ_7  (2.0*1335.0)
#define COMPONENT_SQ_8  (2.0*1663.0)
#define COMPONENT_SQ_9  (2.0*1873.0)
#define COMPONENT_SQ_10 (2.0*2167.0)
#define COMPONENT_SQ_11 (2.0*2484.0)
#define COMPONENT_SQ_12 (2.0*2407.0)
#define COMPONENT_SQ_13 (2.0*2715.0)
#define COMPONENT_SQ_14 (2.0*3141.0)
#define COMPONENT_SQ_15 (2.0*3604.0)

#define COMPONENT_DETERMINANT -448
#define COMPONENT_SQ_DETERMINANT 200704

#define COMPONENT_INV_0  ( 44.0/112.0)
#define COMPONENT_INV_1  (-80.0/112.0)
#define COMPONENT_INV_2  (-36.0/112.0)
#define COMPONENT_INV_3  ( 48.0/112.0)
#define COMPONENT_INV_4  (-31.0/112.0)
#define COMPONENT_INV_5  ( 50.0/112.0)
#define COMPONENT_INV_6  (-37.0/112.0)
#define COMPONENT_INV_7  ( 12.0/112.0)
#define COMPONENT_INV_8  (-75.0/112.0)
#define COMPONENT_INV_9  ( 74.0/112.0)
#define COMPONENT_INV_10 (111.0/112.0)
#define COMPONENT_INV_11 (-92.0/112.0)
#define COMPONENT_INV_12 ( 60.0/112.0)
#define COMPONENT_INV_13 (-48.0/112.0)
#define COMPONENT_INV_14 (-44.0/112.0)
#define COMPONENT_INV_15 ( 40.0/112.0)

  namespace Test{
    /**************************************************************************/
    template<class T>
    static test_result matrix44_constructor_tests(){
      T array[16] = {(T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                     (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                     (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                     (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                     (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                     (T)COMPONENT_15};

      Matrix44<T> mat1((T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                       (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                       (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                       (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                       (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                       (T)COMPONENT_15);

      Matrix44<T> mat2(array);

      Matrix44<T> mat3(mat1);

      Matrix44<T> mat4(mat1[0], mat1[1], mat1[2], mat1[3]);

      Matrix44<T> mat5 = mat1;

      Matrix44<T> mat6;

      Matrix44<T> mat7;

      Matrix44<T> mat8;

      mat6.set(mat1);

      mat7.set((T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
               (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
               (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
               (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
               (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
               (T)COMPONENT_15);

      mat8.set(mat1[0], mat1[1], mat1[2], mat1[3]);

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(mat1[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat2[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat3[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat4[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat5[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat6[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat7[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat8[i][j] != array[i*4+j])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    /**************************************************************************/
    template<class T>
    static test_result matrix44_scalar_tests(){
      T array[16] = {(T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                     (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                     (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                     (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                     (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                     (T)COMPONENT_15};

      Matrix44<T> mat1((T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                       (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                       (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                       (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                       (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                       (T)COMPONENT_15);

      Matrix44<T> mat2(array);

      Matrix44<T> mat3 = mat1 - mat2;

      Matrix44<T> mat4 = mat2;
      mat4 -= mat2;

      Matrix44<T> mat5 = mat1 + mat2;

      Matrix44<T> mat6 = mat1;
      mat6 += mat2;

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(mat3[i][j] != array[i*4+j] - array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat4[i][j] != array[i*4+j] - array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat5[i][j] != array[i*4+j] + array[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat6[i][j] != array[i*4+j] + array[i*4+j])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      return test_result(true, __FILE__, __LINE__);
    }

    /**************************************************************************/
    template<class T>
    static test_result matrix44_multiply_tests(){
      T array[16] = {(T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                     (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                     (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                     (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                     (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                     (T)COMPONENT_15};

      T arraySQ[16] = {(T)COMPONENT_SQ_0, (T)COMPONENT_SQ_1, (T)COMPONENT_SQ_2,
                       (T)COMPONENT_SQ_3, (T)COMPONENT_SQ_4, (T)COMPONENT_SQ_5,
                       (T)COMPONENT_SQ_6, (T)COMPONENT_SQ_7, (T)COMPONENT_SQ_8,
                       (T)COMPONENT_SQ_9, (T)COMPONENT_SQ_10,(T)COMPONENT_SQ_11,
                       (T)COMPONENT_SQ_12,(T)COMPONENT_SQ_13,(T)COMPONENT_SQ_14,
                       (T)COMPONENT_SQ_15};

      Matrix44<T> mat1(array);

      Matrix44<T> mat2 = mat1*mat1;

      Matrix44<T> mat3(mat1);
      mat3 *= mat1;

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(mat2[i][j] != arraySQ[i*4+j])
            return test_result(false, __FILE__, __LINE__);

          if(mat3[i][j] != arraySQ[i*4+j])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      Vector4<T> vec((T)COMPONENT_0, (T)COMPONENT_1,
                     (T)COMPONENT_2, (T)COMPONENT_3);

      Vector4<T> res = mat1*vec;

      for(int i=0;i<4;i++){
        T sum = (T)0.0;

        for(int j=0;j<4;j++){
          sum += mat1[i][j] * vec[j];
        }

        if(res[i] != sum)
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    /**************************************************************************/
    template<class T>
    static test_result matrix44_inverse_tests(){
      T array[16] = {(T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                     (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                     (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                     (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                     (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                     (T)COMPONENT_15};

      T arrayInv[16] = {(T)COMPONENT_INV_0, (T)COMPONENT_INV_1,
                        (T)COMPONENT_INV_2, (T)COMPONENT_INV_3,
                        (T)COMPONENT_INV_4, (T)COMPONENT_INV_5,
                        (T)COMPONENT_INV_6, (T)COMPONENT_INV_7,
                        (T)COMPONENT_INV_8, (T)COMPONENT_INV_9,
                        (T)COMPONENT_INV_10,(T)COMPONENT_INV_11,
                        (T)COMPONENT_INV_12,(T)COMPONENT_INV_13,
                        (T)COMPONENT_INV_14,(T)COMPONENT_INV_15};

      Matrix44<T> mat1(array);
      Matrix44<T> matInv(arrayInv);

      T det1 = mat1.det();

      if(det1 != (T)COMPONENT_DETERMINANT)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      Matrix44<T> inv = mat1.inverse();

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(Abs(inv[i][j] - arrayInv[i*4+j]) > (T)1e-7){
            return test_result(false, __FILE__, __LINE__);
          }
        }
      }

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    /**************************************************************************/
    template<class T>
    static test_result matrix44_transpose_tests(){
      T array[16] = {(T)COMPONENT_0, (T)COMPONENT_1, (T)COMPONENT_2,
                     (T)COMPONENT_3, (T)COMPONENT_4, (T)COMPONENT_5,
                     (T)COMPONENT_6, (T)COMPONENT_7, (T)COMPONENT_8,
                     (T)COMPONENT_9, (T)COMPONENT_10,(T)COMPONENT_11,
                     (T)COMPONENT_12,(T)COMPONENT_13,(T)COMPONENT_14,
                     (T)COMPONENT_15};

      Matrix44<T> mat1(array);

      Matrix44<T> matT = mat1.transpose();

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(matT[i][j] != array[j*4+i])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }
  }

  /**************************************************************************/
  void registerMatrix44Tests(test_function** node){
    REGISTER_FUNCTION(Test::matrix44_constructor_tests<float>, node);
    REGISTER_FUNCTION(Test::matrix44_constructor_tests<double>, node);
    REGISTER_FUNCTION(Test::matrix44_scalar_tests<float>, node);
    REGISTER_FUNCTION(Test::matrix44_scalar_tests<double>, node);
    REGISTER_FUNCTION(Test::matrix44_multiply_tests<float>, node);
    REGISTER_FUNCTION(Test::matrix44_multiply_tests<double>, node);
    REGISTER_FUNCTION(Test::matrix44_inverse_tests<float>, node);
    REGISTER_FUNCTION(Test::matrix44_inverse_tests<double>, node);
    REGISTER_FUNCTION(Test::matrix44_transpose_tests<float>, node);
    REGISTER_FUNCTION(Test::matrix44_transpose_tests<double>, node);
  }
}
