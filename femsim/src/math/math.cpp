/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "math/Math.hpp"
#include "core/BenchmarkTimer.hpp"
#include "core/UnitTest.hpp"

namespace tsl{

  BenchmarkTimer* spmv_timer;

  namespace Test{
    /**************************************************************************/
    template<class T>
    static test_result math_nan_tests(){
      T pnan = (T)0.0/(T)0.0;
      T nnan = -pnan;

      T val = (T)0.0;

      if(!IsNan(pnan)){
        return test_result(false, __FILE__, __LINE__);
      }

      if(!IsNan(nnan)){
        return test_result(false, __FILE__, __LINE__);
      }

      if(IsNan(val)){
        return test_result(false, __FILE__, __LINE__);
      }
      return test_result(true, __FILE__, __LINE__);
    }
  }

  /**************************************************************************/
  void registerMathTests(test_function** node){
    REGISTER_FUNCTION(Test::math_nan_tests<float>, node);
    REGISTER_FUNCTION(Test::math_nan_tests<double>, node);
  }
}
