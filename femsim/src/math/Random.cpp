#include "math/Random.hpp"
#include "core/types.hpp"

/*
  MT19937 | Mersenne Twister: A 623-Dimensionally Equidistributed
  Uniform Pseudo-Random Number Generator

  Makoto Matsumoto & Takuji Nishimura

  ACM Transactions on Modeling and Computer
  Simulation. Vol. 8. No1. Jan. 1998
*/

/* Period parameters */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

namespace tsl{
  template<class T>
  Random<T>::Random(){
    mt = new ulong[N];
    mti = N+1;
  }

  template<class T>
  Random<T>::~Random(){
    delete[] mt;
    mt = 0;
  }

  template<class T>
  void Random<T>::seed(ulong seed){
    /* setting initial seeds to mt[N] using         */
    /* the generator Line 25 of Table 1 in          */
    /* [KNUTH 1981, The Art of Computer Programming */
    /*    Vol. 2 (2nd Ed.), pp102]                  */
    mt[0]= seed & 0xffffffff;
    for (mti=1; mti<N; mti++)
      mt[mti] = (69069 * mt[mti-1]) & 0xffffffff;
  }

  template<class T>
  long unsigned int Random<T>::genRandInt(){
    long unsigned int y;
    long unsigned int mag01[2]={0x0, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
      int kk;

      if (mti == N+1)   /* if seed() has not been called, */
        seed(4357);     /* a default initial seed is used   */

      for (kk=0;kk<N-M;kk++) {
        y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
        mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
      }
      for (;kk<N-1;kk++) {
        y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
        mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
      }
      y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
      mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];

      mti = 0;
    }

    y = mt[mti++];
    y ^= TEMPERING_SHIFT_U(y);
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
    y ^= TEMPERING_SHIFT_L(y);

    return y;
  }

  template<class T>
  T Random<T>::genRand(){
    long unsigned int y = genRandInt();
    return ( (T)y / (T)0xffffffff);
  }

  template<>
  float Random<float>::genRand(){
    long unsigned int y = genRandInt();
    return ((float)(y >> 8)) / ((float)(1 << 24));
  }

  template<>
  double Random<double>::genRand(){
    long unsigned int y = genRandInt();
    long unsigned int z = genRandInt();
    return (double)((((unsigned long)(y >> 6)) << 27) + (z >> 5)) /
      (double)(1L << 53);
  }

  template class Random<float>;
  template class Random<double>;

  template<>
  float genRand<float>(){
    return randomFloat.genRand();
  }

  template<>
  double genRand<double>(){
    return randomDouble.genRand();
  }
}
