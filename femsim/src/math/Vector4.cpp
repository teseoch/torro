/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "core/UnitTest.hpp"

namespace tsl{

#define COMPONENT_0 3.0
#define COMPONENT_1 5.0
#define COMPONENT_2 7.0
#define COMPONENT_3 11.0
#define COMPONENT_SUM 26.0
#define COMPONENT_SQUARED_SUM (COMPONENT_0 * COMPONENT_0 +   \
                               COMPONENT_1 * COMPONENT_1 +   \
                               COMPONENT_2 * COMPONENT_2 +   \
                               COMPONENT_3 * COMPONENT_3)

#define CROSS_0 -2.0
#define CROSS_1  4.0
#define CROSS_2 -2.0

  //template<class T>
  //const Vector4<T> Vector4<T>::Zero((T)0.0, (T)0.0, (T)0.0, (T)0.0);

  namespace Test{

    template<class T>
    static test_result vector4_sum_test(){
      Vector4<T> vec((T)COMPONENT_0,
                     (T)COMPONENT_1,
                     (T)COMPONENT_2,
                     (T)COMPONENT_3);

      if(Abs(vec.sum() - (T)COMPONENT_SUM) > (T)1e-7)
        return test_result(false, __FILE__, __LINE__);

      return test_result(true, __FILE__, __LINE__);
    }

    /*Tests constructors, index operator and (in)equality check*/
    template<class T>
    static test_result vector4_constructor_test(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);
      Vector4<T> vec2(vec);
      Vector4<T> vec3;

      /*--------------------------------------------------------------------*/

      if(vec  != vec2)          return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(vec.cwEqual(vec2).sum() != (T)4.0)
                                return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(vec.cwNotEqual(vec2).sum() != (T)0.0)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i]) return test_result(false, __FILE__, __LINE__);
        if(vec3[i] != (T)0.0)   return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec3 = vec2;
      if(vec3 != vec2)          return test_result(false, __FILE__, __LINE__);
      if(!(vec3 == vec2))       return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      vec3.set(array[0], array[1], array[2], array[3]);
      if(vec3 != vec)           return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      vec3.set(array);
      if(vec3 != vec)           return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      vec3.set(vec);
      if(vec3 != vec)           return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_scalar_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      const T multiplier = (T)10.0;

      Vector4<T> vec(array[0], array[1], array[2], array[3]);
      Vector4<T> vec2 = vec;

      vec2 *= multiplier;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] * multiplier)
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2 * multiplier;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] * multiplier)
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 /= multiplier;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] / multiplier)
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2 / multiplier;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] / multiplier)
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 += vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] + array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2 + vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] + array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 -= vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] - array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2 - vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] - array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = +vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = -vec2;

      for(int i=0;i<4;i++){
        if(vec2[i] != -array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      T* tmpArray = (T*)vec2;

      for(int i=0;i<4;i++){
        if(tmpArray[i] != array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2.emul(vec);

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] * array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2.emulassign(vec);

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] * array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2 = vec2.ediv(vec);

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] / array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/

      vec2 = vec;
      vec2.edivassign(vec);

      for(int i=0;i<4;i++){
        if(vec2[i] != array[i] / array[i])
          return test_result(false, __FILE__, __LINE__);
      }

      /*--------------------------------------------------------------------*/
      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_comparison_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);
      Vector4<T> vecs[4];

      vecs[0].set(array[0], array[0], array[0], array[0]);
      vecs[1].set(array[1], array[1], array[1], array[1]);
      vecs[2].set(array[2], array[2], array[2], array[2]);
      vecs[3].set(array[3], array[3], array[3], array[3]);

      Vector4<T> res;

      for(int i=0;i<4;i++){
        res = vec < vecs[i];
        for(int j=0;j<4;j++){
          if(vec[j] < vecs[i][j] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] >= vecs[i][j] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec > vecs[i];
        for(int j=0;j<4;j++){
          if(vec[j] > vecs[i][j] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] <= vecs[i][j] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec <= vecs[i];
        for(int j=0;j<4;j++){
          if(vec[j] <= vecs[i][j] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] > vecs[i][j] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec >= vecs[i];
        for(int j=0;j<4;j++){
          if(vec[j] >= vecs[i][j] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] < vecs[i][j] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec < array[i];
        for(int j=0;j<4;j++){
          if(vec[j] < array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] >= array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec > array[i];
        for(int j=0;j<4;j++){
          if(vec[j] > array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] <= array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec <= array[i];
        for(int j=0;j<4;j++){
          if(vec[j] <= array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] > array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = vec >= array[i];
        for(int j=0;j<4;j++){
          if(vec[j] >= array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] < array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = array[i] > vec;
        for(int j=0;j<4;j++){
          if(vec[j] < array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] >= array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = array[i] < vec;
        for(int j=0;j<4;j++){
          if(vec[j] > array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] <= array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = array[i] >= vec;
        for(int j=0;j<4;j++){
          if(vec[j] <= array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] > array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      for(int i=0;i<4;i++){
        res = array[i] <= vec;
        for(int j=0;j<4;j++){
          if(vec[j] >= array[i] && res[j] != (T)1.0)
            return test_result(false, __FILE__, __LINE__);
          else if(vec[j] < array[i] && res[j] == (T)1.0)
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      const T multiplier = (T)10.0;
      Vector4<T> vec2 = vec * multiplier;

      max(res, vec, vec2);
      if(res != vec2) return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      max(res, vec2, vec);
      if(res != vec2) return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      min(res, vec, vec2);
      if(res != vec)  return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      min(res, vec2, vec);
      if(res != vec)  return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_dot_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);

      if(dot(vec, vec) != (T)COMPONENT_SQUARED_SUM)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(dot(vec, -vec) != -(T)COMPONENT_SQUARED_SUM)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(dot(-vec, vec) != -(T)COMPONENT_SQUARED_SUM)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(dot(-vec, -vec) != (T)COMPONENT_SQUARED_SUM)
        return test_result(false, __FILE__, __LINE__);

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_cross_tests(){
      T arrayA[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                     (T)COMPONENT_2, (T)COMPONENT_3};

      T arrayB[4] = {(T)1.0, (T)1.0, (T)1.0, (T)1.0};

      Vector4<T> vecA(arrayA[0], arrayA[1], arrayA[2], arrayA[3]);
      Vector4<T> vecB(arrayB[0], arrayB[1], arrayB[2], arrayB[3]);

      Vector4<T> res = cross(vecA, vecB);

      Vector4<T> cr((T)CROSS_0, (T)CROSS_1, (T)CROSS_2, (T)0.0);

      if(cr != res)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      res = cross(vecB, vecA);
      if(-cr != res)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      res = cross(vecA, vecA);
      if(dot(res, res) != (T)0.0)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_norm_length_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                     (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);

      T sum = (T)0.0;
      T sqsum = (T)0.0;
      for(int i=0;i<4;i++){
        sum += array[i];
        sqsum += Sqr(array[i]);
      }

      if(sum != vec.sum())
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(sqsum != dot(vec, vec))
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(Sqrt(sqsum) != vec.length())
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      vec.normalize();
      if(vec.length() != (T)1.0)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_product_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);

      Matrix44<T> res = vec.outherProduct(vec);

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(res[i][j] != array[i]*array[j])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      res = vec.directProduct(vec);

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          if(res[i][j] != array[i]*array[j])
            return test_result(false, __FILE__, __LINE__);
        }
      }

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_reduction_tests(){
      T array[4] = {(T)COMPONENT_0, (T)COMPONENT_1,
                    (T)COMPONENT_2, (T)COMPONENT_3};

      Vector4<T> vec(array[0], array[1], array[2], array[3]);

      /*--------------------------------------------------------------------*/

      if(vec.reduceMax() != (T)COMPONENT_3)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      if(vec.reduceMin() != (T)COMPONENT_0)
        return test_result(false, __FILE__, __LINE__);

      /*--------------------------------------------------------------------*/

      return test_result(true, __FILE__, __LINE__);
    }

    template<class T>
    static test_result vector4_NaN_tests(){
      T pnan = (T)0.0/(T)0.0;
      T nnan = -pnan;

      Vector4<T> vecPNaN;
      Vector4<T> vecNNaN;

      for(int i=0;i<4;i++){
        vecPNaN[i] = pnan;
        vecNNaN[i] = nnan;
      }

      if(!IsNan(vecPNaN)){
        return test_result(false, __FILE__, __LINE__);
      }

      if(!IsNan(vecNNaN)){
        return test_result(false, __FILE__, __LINE__);
      }
      return test_result(true, __FILE__, __LINE__);
    }
  }

  void registerVector4Tests(test_function** node){
    REGISTER_FUNCTION(Test::vector4_sum_test<float>, node);
    REGISTER_FUNCTION(Test::vector4_sum_test<double>, node);
    REGISTER_FUNCTION(Test::vector4_constructor_test<float>, node);
    REGISTER_FUNCTION(Test::vector4_constructor_test<double>, node);
    REGISTER_FUNCTION(Test::vector4_scalar_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_scalar_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_comparison_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_comparison_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_dot_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_dot_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_cross_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_cross_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_norm_length_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_norm_length_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_product_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_product_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_reduction_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_reduction_tests<double>, node);
    REGISTER_FUNCTION(Test::vector4_NaN_tests<float>, node);
    REGISTER_FUNCTION(Test::vector4_NaN_tests<double>, node);
  }
}
