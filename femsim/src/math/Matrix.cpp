/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "math/Matrix.hpp"
#include "core/Exception.hpp"
#include "core/UnitTest.hpp"
#include <stdio.h>

namespace tsl{
  namespace Test{
    /**************************************************************************/
    template<class T, int M, int N, int O>
    struct MatTTest{
      static test_result matrix_constructor_test(){
        message("Test:  %s", __PRETTY_FUNCTION__);

        MatrixT<N, M, T> mat1;

        for(int i=0;i<N;i++){
          for(int j=0;j<M;j++){
            mat1[i][j] = (T)(i*M+j);
          }
        }

        for(int i=0;i<N;i++){
          for(int j=0;j<M;j++){
            if(mat1[i][j] != (T)(i*M+j)){
              return test_result(false, __FILE__, __LINE__);
            }
          }
        }

        MatrixT<N, M, T> mat2(mat1);

        for(int i=0;i<N;i++){
          for(int j=0;j<M;j++){
            if(mat2[i][j] != (T)(i*M+j)){
              return test_result(false, __FILE__, __LINE__);
            }
          }
        }

        MatrixT<N, M, T> mat3;
        mat3 = mat1;

        for(int i=0;i<N;i++){
          for(int j=0;j<M;j++){
            if(mat3[i][j] != (T)(i*M+j)){
              return test_result(false, __FILE__, __LINE__);
            }
          }
        }

        test_result res = MatTTest<T, M, N-1, O>::matrix_constructor_test();

        if(res.success == false){
          return res;
        }

        if(N == O){
          res = MatTTest<T, M-1, N, O>::matrix_constructor_test();
        }

        return res;
      }
    };

    /**************************************************************************/
    template<class T, int M, int O>
    struct MatTTest<T, M, 0, O>{
      static test_result matrix_constructor_test(){
        return test_result(true, __FILE__, __LINE__);
      }
    };

    /**************************************************************************/
    template<class T, int N, int O>
    struct MatTTest<T, 0, N, O>{
      static test_result matrix_constructor_test(){
        return test_result(true, __FILE__, __LINE__);
      }
    };

    /**************************************************************************/
    template<class T, int M, int N, int O, int P>
    struct MatMulTest{
      static test_result mul_test(){
        message("test: %s", __PRETTY_FUNCTION__);
        getchar();
        MatrixT<N, M, T> matA;
        MatrixT<M, O, T> matB;

        for(int i=0;i<N;i++){
          for(int j=0;j<M;j++){
            matA[i][j] = (T)(i*M+j);
          }
        }

        for(int i=0;i<M;i++){
          for(int j=0;j<O;j++){
            matB[i][j] = (T)(i*O+j);
          }
        }

        MatrixT<N, O, T> matAB = matA * matB;

        for(int i=0;i<N;i++){
          for(int j=0;j<O;j++){
            T sum = (T)0.0;
            for(int k=0;k<M;k++){
              sum += matA[i][k] * matB[k][j];
            }

            if(Abs(sum - matAB[i][j]) > (T)1e-6){
              return test_result(false, __FILE__, __LINE__);
            }
          }
        }

        test_result res = MatMulTest<T, M, N, O-1, P>::mul_test();

        if(res.success == false){
          return res;
        }

        if(O==P){
          res = MatMulTest<T, M, N-1, O, P>::mul_test();
        }

        if(res.success == false){
          return res;
        }

        if(N==P && O==P){
          message("Reduce M");
          res = MatMulTest<T, M-1, N, O, P>::mul_test();
        }

        return res;
      }
    };

    /**************************************************************************/
    template<class T, int M, int N, int P>
    struct MatMulTest<T, M, N, 0, P>{
      static test_result mul_test(){
        return test_result(true, __FILE__, __LINE__);
      }
    };

    /**************************************************************************/
    template<class T, int M, int O, int P>
    struct MatMulTest<T, M, 0, O, P>{
      static test_result mul_test(){
        return test_result(true, __FILE__, __LINE__);
      }
    };

    /**************************************************************************/
    template<class T, int N, int O, int P>
    struct MatMulTest<T, 0, N, O, P>{
      static test_result mul_test(){
        return test_result(true, __FILE__, __LINE__);
      }
    };
  }

  void registerMatrixTests(test_function** node){
#if 0
    registerTestFunction(Test::MatTTest<float, 12, 12, 12>::matrix_constructor_test,
                         "Test::MatTTest<float, 12, 12, 12>::matrix_constructor_test",
                         node);

    registerTestFunction(Test::MatTTest<double, 12, 12, 12>::matrix_constructor_test,
                         "Test::MatTTest<double, 12, 12, 12>::matrix_constructor_test",
                         node);

    registerTestFunction(Test::MatMulTest<float, 12, 12, 12, 12>::mul_test,
                         "Test::MatMulTest<float, 12, 12, 12, 12>::mul_test",
                         node);

    registerTestFunction(Test::MatMulTest<double, 12, 12, 12, 12>::mul_test,
                         "Test::MatMulTest<double, 12, 12, 12, 12>::mul_test",
                         node);
#endif
  }
}
