/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#include "geo/grid_io.hpp"
#include "geo/GridFile.hpp"
#include "FEMSim.hpp"
#include "util/CSVExporter.hpp"

using namespace tsl;

namespace tsl{
  bool simPaused = false;
}

int main(int argc, char* argv[]){
  tsl::init(Console); /*Starts as a normal process*/
  //tsl::init(Daemon); /*Starts as a daemon process and redirects all streams to files*/
  //tsl::init(DaemonNullRedirected); /*Starts as a daemon process and trashes all stdio streams*/

  FEMSim* sim;

  if(argc == 2 || argc == 3){
    FILE* file = fopen("stats.csv", "w");

    CSVExporter* exporter = new CSVExporter(file);

    exporter->addColumn("n_iterations");
    exporter->addColumn("n_outer_iterations");
    exporter->addColumn("n_inner_iterations");
    exporter->addColumn("n_clean_iterations");
    exporter->addColumn("n_changed_iterations");
    exporter->addColumn("n_cg_iterations");
    exporter->addColumn("n_constraints");
    exporter->addColumn("n_update_iterations");
    exporter->addColumn("n_changed_eval_iterations");
    exporter->addColumn("delassus_operator");
    exporter->addColumn("n_delassus_operator");
    exporter->addColumn("inverse_operator");
    exporter->addColumn("solverTime");
    exporter->addColumn("bNorm");
    exporter->addColumn("FBValueNP");
    exporter->addColumn("FBValueSF");
    exporter->addColumn("FBValueKF");
#ifndef METHOD_GS
    exporter->addColumn("preconditionerTime");
    exporter->addColumn("n_preconditionerCalls");
    exporter->addColumn("preSolverTime");
    exporter->addColumn("spmvTime");
    exporter->addColumn("spmvCalls");
    exporter->addColumn("evaluationTime");
    exporter->addColumn("n_evalCalls");
    exporter->addColumn("evaluationTime2");
    exporter->addColumn("n_evalCalls2");
    exporter->addColumn("evaluationTime3");
    exporter->addColumn("n_evalCalls3");
    exporter->addColumn("updateTime");
    exporter->addColumn("n_updateCalls");
    exporter->addColumn("spmv_conventional");
    exporter->addColumn("n_spmv_conventional");
    exporter->addColumn("spmv_pre");
    exporter->addColumn("n_spmv_pre");
    exporter->addColumn("spmv_constraints");
    exporter->addColumn("n_spmv_constraints");
    exporter->addColumn("precond_vec");
    exporter->addColumn("n_precond_vec");
    exporter->addColumn("vector");
    exporter->addColumn("n_vector");
    exporter->addColumn("vector_red");
    exporter->addColumn("n_vector_red");
    exporter->addColumn("prec_update");
    exporter->addColumn("n_prec_update");
    exporter->addColumn("prec_scaling_update");
    exporter->addColumn("n_prec_scaling_update");
    exporter->addColumn("residualRecompute");
    exporter->addColumn("n_residualRecompute");
    exporter->addColumn("errorComputation");
    exporter->addColumn("n_errorComputation");
#else
    exporter->addColumn("spmvTime");
    exporter->addColumn("unconstrainedTime");
    exporter->addColumn("n_unconstrainedCalls");
    exporter->addColumn("evaluationTime");
    exporter->addColumn("evaluationTime2");
    exporter->addColumn("updateTime");
    exporter->addColumn("LCPConstructTime");
    exporter->addColumn("LCPRHSTime");
    exporter->addColumn("LCPSolveTime");
#endif
    exporter->addColumn("check_time");
    exporter->addColumn("n_check");

    message("loading %s", argv[1]);

    GridFile<T> grid(argv[1]);

    if(argc == 3){
      int continueFrame = atoi(argv[2]);
      sim = new FEMSim(grid, continueFrame);
    }else{
      sim = new FEMSim(grid);
    }

    sim->setExporter(exporter);
    sim->initializeSolver();

    if(argc == 3){
      sim->resume();
    }

    sim->simulate();

    delete exporter;
    fclose(file);

    delete sim;
  }

  tsl::destroy();
}
