/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef GRID_IO_HPP
#define GRID_IO_HPP

#include "datastructures/DCEList.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  DCTetraMesh<T>* load_grid(const char* file, int* dv,
                            int* sv, int* rv, int* cv);
}

#endif/*GRID_IO_HPP*/
