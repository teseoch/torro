/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CYLINDER_HPP
#define CYLINDER_HPP

#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Cylinder{
  public :
    /**************************************************************************/
    Cylinder();

    /**************************************************************************/
    Cylinder(T r);

    /**************************************************************************/
    Cylinder(T r, const Vector4<T>& a, const Vector4<T>& b);

    /**************************************************************************/
    Cylinder(const Cylinder<T>& cylinder);

    /**************************************************************************/
    ~Cylinder();

    /**************************************************************************/
    Cylinder<T>& operator=(const Cylinder<T>& cyl);

    /**************************************************************************/
    /*Compute signed distance with point to infinite cylinder*/
    T signedDistance(const Vector4<T>& p, T* palpha = 0)const;

    /**************************************************************************/
    /*Compute signed distance with line to infinite cylinder*/
    T signedDistance(const Vector4<T>& a, const Vector4<T>& b,
                     T* palpha = 0, T* pbeta = 0, bool* pparallel = 0)const;

    /**************************************************************************/
    bool intersectionWithLine(const Vector4<T>& a, const Vector4<T>&b,
                              Vector4<T>* ca, Vector4<T>* cb,
                              T* palpha1 = 0, T* pbeta1 = 0,
                              T* palpha2 = 0, T* pbeta2 = 0,
                              bool* pparallel = 0)const;
  protected:
    /**************************************************************************/
    T radius;
    Vector4<T> start;
    Vector4<T> end;
  };
}

#endif/*CYLINDER_HPP*/
