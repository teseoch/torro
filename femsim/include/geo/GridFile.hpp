/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef GRIDFILE_HPP
#define GRIDFILE_HPP

#include "geo/BBox.hpp"
#include "math/Vector4.hpp"
#include "math/rotations.hpp"
#include "geo/ply_io.hpp"
#include "datastructures/DCEList.hpp"

#include <fstream>
#include <sstream>
#include <iostream>

namespace tsl{
  /**************************************************************************/
  template<class T>
  class GridVertex{
  public:
    /**************************************************************************/
    GridVertex(){
      mapping = 0;
    }

    /**************************************************************************/
    Vector4<T> v;
    int mapping;

    /**************************************************************************/
    template<class Y>
    friend std::istream& operator >> (std::istream& is, GridVertex<Y>& vertex);

    /**************************************************************************/
    template<class Y>
    friend std::ostream& operator << (std::ostream& os, GridVertex<Y>& vertex);
  };

  /**************************************************************************/
  template<class T>
  inline std::istream& operator>>(std::istream& is, GridVertex<T>& vertex){
    if(!is.eof()){
      is >> vertex.v[0] >> vertex.v[1] >> vertex.v[2];
      vertex.v[3] = 1.0;
      if(!is.eof()){
        /*If the vertex has a fourth coordinate,
          it is an additional mapping which maps the vertex to some object.*/
        is >> vertex.mapping;
      }else{
        vertex.mapping = 0;
      }
    }else{
      vertex.v.set(0,0,0,0);
      vertex.mapping = 0;
    }

    return is;
  }

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os, GridVertex<T>& vertex){
    os << vertex.v[0] << " "
       << vertex.v[1] << " "
       << vertex.v[2] << " " << vertex.mapping;
    return os;
  }

  /**************************************************************************/
  class GridElement{
  public:
    /**************************************************************************/
    int v[4];

    /**************************************************************************/
    friend std::istream& operator>>(std::istream& is, GridElement& e);

    /**************************************************************************/
    friend std::ostream& operator<<(std::ostream& os, GridElement& e);
  };

  /**************************************************************************/
  inline std::istream& operator>>(std::istream& is, GridElement& e){
    if(!is.eof()){
      is >> e.v[0] >> e.v[1] >> e.v[2] >> e.v[3];
    }else{
      e.v[0] = e.v[1] = e.v[2] = e.v[3] = -1;
    }

    return is;
  }

  /**************************************************************************/
  inline std::ostream& operator<<(std::ostream& os, GridElement& e){
    os << e.v[0] << " "
       << e.v[1] << " "
       << e.v[2] << " "
       << e.v[3];

    return os;
  }

  /**************************************************************************/
  class GridFace{
  public:
    /**************************************************************************/
    GridFace(){
      v[0] = v[1] = v[2] = -1;
      material = -2;
      kd = ks = 0.0f;
    }

    /**************************************************************************/
    int v[3];
    int material;
    Vector4<float> diffuseColor;
    float kd;
    float ks;

    /**************************************************************************/
    friend std::istream& operator>>(std::istream& is, GridFace& face);

    /**************************************************************************/
    friend std::ostream& operator<<(std::ostream& os, GridFace& face);
  };

  /**************************************************************************/
  inline std::istream& operator>>(std::istream& is, GridFace& face){
    if(!is.eof()){
      is >> face.v[0] >> face.v[1] >> face.v[2];

      if(!is.eof()){
        is >> face.material;
        is >> face.diffuseColor[0] >> face.diffuseColor[1];
        is >> face.diffuseColor[2] >> face.diffuseColor[3];
        is >> face.kd >> face.ks;
      }else{
        face.material = -2;
        face.diffuseColor.set(0,0,0,0);
        face.kd = 0;
        face.ks = 0;
      }
    }else{
      face.v[0] = -1;
      face.v[1] = -1;
      face.v[2] = -1;
      face.material = -2;
    }

    return is;
  }

  /**************************************************************************/
  inline std::ostream& operator<<(std::ostream& os, GridFace& face){
    os << face.v[0] << " "
       << face.v[1] << " "
       << face.v[2] << " "
       << face.material << " "
       << face.diffuseColor[0] << " "
       << face.diffuseColor[1] << " "
       << face.diffuseColor[2] << " "
       << face.diffuseColor[3] << " "
       << face.kd << " "
       << face.ks;
    return os;
  }

  /**************************************************************************/
  template<class T>
  static void addVertex(DCTetraMesh<T>* mesh, T x, T y, T z, int id,
                        Mesh::GeometryType type, int offset,
                        int n_offset_vertices){
    DCELVertex<T> vertex;

    int index = mesh->addVertex(vertex);

    mesh->vertices[index].coord.set(x, y, z, 0);
    mesh->vertices[index].displCoord.set(x, y, z, 0);
    mesh->vertices[index].displCoordExt.set(x, y, z, 0);
    mesh->vertices[index].id   = index;
    mesh->vertices[index].type = type;

    tslassert(id == index);

    //message("Add vertex %d, %d, %d", id, offset, n_offset_vertices);

    if(type == Mesh::Rigid){
      mesh->vertices[index].vel_index =
        offset + (index - n_offset_vertices) * 6;
    }else if(type == Mesh::Static){
      mesh->vertices[index].vel_index = -1;
    }else{
      mesh->vertices[index].vel_index =
        offset + (index - n_offset_vertices) * 3;
    }

    //mesh->vertices[index].print();
    //getchar();
  }

  /**************************************************************************/
  template<class T>
  static void addRigidVertexMap(DCTetraMesh<T>* mesh,
                                int vertex_id, int object_id){
    mesh->vertexObjectMap[vertex_id] = object_id;
  }

  template<class T>
  static void addTetrahedron(DCTetraMesh<T>* mesh, int v1,
                             int v2, int v3, int v4,
                             Mesh::GeometryType type){
    DCELTet<> tetrahedron;
    int index = mesh->addTetrahedron(tetrahedron);
    mesh->tetrahedra[index].v1 = v1;
    mesh->tetrahedra[index].v2 = v2;
    mesh->tetrahedra[index].v3 = v3;
    mesh->tetrahedra[index].v4 = v4;
    mesh->tetrahedra[index].type = type;
  }

#if 0
  /**************************************************************************/
  static void addRigidTetrahedronMap(DCTetraMesh<>* mesh,
                                     int tet_id, int object_id){
    mesh->tetObjectMap[tet_id] = object_id;
  }
#endif

  /**************************************************************************/
  template<class T>
  static void addFace(DCTetraMesh<T>* mesh, int v1, int v2, int v3,
                      Mesh::GeometryType type){
    DCELHalfFace<T> halfFace;

    int hf_index = mesh->addHalfFace(halfFace);
    int edgeList[3];
    int vertices[3];

    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;

    //message("Add face [%d] %d, %d, %d", hf_index, v1, v2, v3);

    for(int j=0;j<3;j++){
      int index = vertices[j];
      DCELHalfEdge<> he;

      index = mesh->addHalfEdge(he);

      mesh->halfEdges[index].origin    = vertices[j];
      mesh->halfEdges[index].half_face = hf_index;
      mesh->halfEdges[index].twin      = Mesh::UndefinedIndex;
      mesh->halfEdges[index].next      = Mesh::UndefinedIndex;
      mesh->halfEdges[index].next_twin = Mesh::UndefinedIndex;
      mesh->halfEdges[index].type      = type;

      //mesh->halfEdges[index].print();
      edgeList[j] = index;
    }

    for(int j=0;j<3;j++){
      int index = edgeList[j];
      int next = edgeList[(j+1)%3];

      mesh->halfEdges[index].next      = next;
      mesh->halfEdges[index].half_face = hf_index;
      mesh->halfEdges[index].type      = type;

      mesh->vertices[mesh->halfEdges[index].origin].leaving = index;

      mesh->halfFaces[hf_index].half_edge = index;
      mesh->halfFaces[hf_index].type      = type;

      //message("edge = %d", index);
      //mesh->halfEdges[index].print();
    }
  }

#if 0
  /**************************************************************************/
  static void addRigidFaceMap(DCTetraMesh<>* mesh,
                              int face_id, int object_id){
    mesh->faceObjectMap[face_id] = object_id;

    int halfEdge = mesh->halfFaces[face_id].half_edge;

    mesh->setCurrentEdge(halfEdge);

    for(int i=0;i<3;i++){
      int currentEdge = mesh->getCurrentEdge();
      mesh->edgeObjectMap[currentEdge] = object_id;
      /*Edges not connected, set twin edge later*/
      mesh->nextEdge();
    }
  }
#endif

  /**************************************************************************/
  template<class T>
  class GeneralConstraintDescriptor{
  public:
    /**************************************************************************/
    GeneralConstraintDescriptor(){
      startFrame = endFrame = -1;
    }

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const GeneralConstraintDescriptor<Y>& d);

    /**************************************************************************/
    int startFrame;
    int endFrame;
  };

  /**************************************************************************/
  template<class T>
  class ConstraintDescriptor : public GeneralConstraintDescriptor<T>{
  public:
    /**************************************************************************/
    enum ConstraintType{Fixed, Linked};

    /**************************************************************************/
    ConstraintDescriptor(){
      type = Fixed;
      objectId[0] = objectId[1] = vertexId[0][0] = vertexId[0][1] =
        vertexId[0][2] = vertexId[1][0] = vertexId[1][1] = vertexId[1][2] = -1;
      interpolated[0] = interpolated[1] = false;
    }

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const ConstraintDescriptor<Y>& d);

    /**************************************************************************/
    ConstraintType type;
    int objectId[2];
    int vertexId[2][3];

    /*If the constraint point is located on an edge of face*/
    bool interpolated[2];

    Vector4<T> bary[2];

    Vector4<T> p[2];

    /**************************************************************************/
    bool isVertex(int i){
      tslassert(i==0 || i == 1);
      if(objectId[i] == -1){
        if(vertexId[i][0] != -1 &&
           vertexId[i][1] == -1 &&
           vertexId[i][2] == -1){
          return true;
        }
      }
      return false;
    }

    /**************************************************************************/
    bool isEdge(int i){
      tslassert(i==0 || i == 1);
      if(objectId[i] == -1){
        if(vertexId[i][0] != -1 &&
           vertexId[i][1] != -1 &&
           vertexId[i][2] == -1){
          return true;
        }
      }
      return false;
    }

    /**************************************************************************/
    bool isFace(int i){
      tslassert(i==0 || i == 1);
      if(objectId[i] == -1){
        if(vertexId[i][0] != -1 &&
           vertexId[i][1] != -1 &&
           vertexId[i][2] != -1){
          return true;
        }
      }
      return false;
    }
  };

  /**************************************************************************/
  template<class T>
  class ExternalVelocityDescriptor : public GeneralConstraintDescriptor<T>{
  public:
    /**************************************************************************/
    int rigidObject;
    Vector4<T> angVelocity;
    Vector4<T> linVelocity;

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const ExternalVelocityDescriptor<Y>& d);
  };

  /**************************************************************************/
  template<class T>
  class ExternalForceDescriptor : public GeneralConstraintDescriptor<T>{
  public:
    /**************************************************************************/
    int rigidObject;
    Vector4<T> angForce;
    Vector4<T> linForce;

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const ExternalForceDescriptor<Y>& d);
  };

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const GeneralConstraintDescriptor<T>& d){
    os << "StartFrame " << d.startFrame << std::endl;
    os << "EndFrame " << d.endFrame << std::endl;
    return os;
  }

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const ExternalVelocityDescriptor<T>& d){
    os << "Velocity descriptor " << std::endl;
    os << "Rigid object        " << d.rigidObject << std::endl;
    os << "Linear velocity     " << d.linVelocity;
    os << "Angular velocity    " << d.angVelocity;

    const GeneralConstraintDescriptor<T>*g = &d;
    os << *g;

    return os;
  }

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const ExternalForceDescriptor<T>& d){
    os << "Force descriptor " << std::endl;
    os << "Rigid object     " << d.rigidObject << std::endl;
    os << "Linear force     " << d.linForce;
    os << "Angular force    " << d.angForce;

    const GeneralConstraintDescriptor<T>*g = &d;
    os << *g;

    return os;
  }

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const ConstraintDescriptor<T>& d){
    os << "ConstraintDescriptor ";
    if(d.type == ConstraintDescriptor<T>::Fixed){
      os << "Fixed" << std::endl;
    }else if(d.type == ConstraintDescriptor<T>::Linked){
      os << "Linked" << std::endl;
    }

    for(int i=0;i<2;i++){
      os << "Object " << i << std::endl;
      os << "Vertex " << i << "1 " << d.vertexId[i][0] << std::endl;
      os << "Vertex " << i << "2 " << d.vertexId[i][1] << std::endl;
      os << "Vertex " << i << "3 " << d.vertexId[i][2] << std::endl;

      os << "Interpolated " << i << " " << d.interpolated[i] << std::endl;

      os << "Bary " << i << " " << d.bary[i] << std::endl;

      os << "P " << i << " " << d.p[i] << std::endl;
    }

    const GeneralConstraintDescriptor<T>* g = &d;
    os << *g;

    return os;
  }

  /**************************************************************************/
  template<class T>
  class GridFile{
  public:
    /**************************************************************************/
    typedef ConstraintDescriptor<T>            Descriptor;

    /**************************************************************************/
    typedef ExternalVelocityDescriptor<T>      VelocityDescriptor;

    /**************************************************************************/
    typedef ExternalForceDescriptor<T>         ForceDescriptor;

    /**************************************************************************/
    typedef typename List<Descriptor>::Iterator DescriptorListIterator;

    /**************************************************************************/
    typedef typename List<VelocityDescriptor>::Iterator
    VelocityDescriptorListIterator;

    /**************************************************************************/
    typedef typename List<ForceDescriptor>::Iterator
    ForceDescriptorListIterator;

    /**************************************************************************/
    GridFile():n_grid_vertices(0), n_grid_elements(0), n_grid_faces(0),
               n_mesh_vertices(0), n_mesh_faces(0), n_static_vertices(0),
               n_static_faces(0),
               n_rigid_vertices(0), n_rigid_faces(0), n_rigid_elements(0),
               n_cloth_vertices(0), n_cloth_faces(0),
               gridVertices(0),gridElements(0), gridFaces(0), meshVertices(0),
               meshVerticesMap(0), meshFaces(0), staticVertices(0),
               staticFaces(0), rigidVertices(0), rigidVerticesMap(0),
               rigidFaces(0), clothVertices(0), clothFaces(0){

      gridVertices     = new GridVertex<T>[n_grid_vertices];
      gridFaces        = new GridFace[n_grid_faces];
      gridElements     = new GridElement[n_grid_elements];

      meshVertices     = new GridVertex<T>[n_mesh_vertices];
      meshFaces        = new GridFace[n_mesh_faces];
      meshVerticesMap  = new int[n_mesh_vertices];

      staticVertices   = new GridVertex<T>[n_static_vertices];
      staticFaces      = new GridFace[n_static_faces];

      rigidVertices    = new GridVertex<T>[n_rigid_vertices];
      rigidVerticesMap = new int[n_rigid_vertices];
      rigidFaces       = new GridFace[n_rigid_faces];

      clothVertices    = new GridVertex<T>[n_cloth_vertices];
      clothFaces       = new GridFace[n_cloth_faces];
    }

#if 1
    /**************************************************************************/
    GridFile(int ngv, int nge, int ngf,
             int nmv, int nmf,
             int nsv, int nsf,
             int nrv, int nrf,
             int ncv, int ncf):n_grid_vertices(ngv), n_grid_elements(nge),
                               n_grid_faces(ngf), n_mesh_vertices(nmv),
                               n_mesh_faces(nmf), n_static_vertices(nsv),
                               n_static_faces(nsf),
                               n_rigid_vertices(nrv),
                               n_rigid_faces(nrf),
                               n_cloth_vertices(ncv),
                               n_cloth_faces(ncf){
      gridVertices    = new GridVertex<T>[n_grid_vertices];
      gridElements    = new GridElement[n_grid_elements];
      gridFaces       = new GridFace[n_grid_faces];

      meshVertices    = new GridVertex<T>[n_mesh_vertices];
      meshVerticesMap = new int[n_mesh_vertices];
      meshFaces       = new GridFace[n_mesh_faces];

      staticVertices  = new GridVertex<T>[n_static_vertices];
      staticFaces     = new GridFace[n_static_faces];

      rigidVertices   = new GridVertex<T>[n_rigid_vertices];
      rigidVerticesMap= new int[n_rigid_vertices];
      rigidFaces      = new GridFace[n_rigid_faces];

      clothVertices   = new GridVertex<T>[n_cloth_vertices];
      clothFaces      = new GridFace[n_cloth_faces];

    }
#endif

    /**************************************************************************/
    GridFile(const char* fileName){
      std::ifstream file(fileName, std::ios::in);

      if(!file.is_open()){
        throw new FileNotFoundException(__LINE__, __FILE__, fileName);
      }

      n_grid_vertices  = readInt(file);  /*Number of grid vertices*/
      n_grid_elements  = readInt(file);  /*Number of grid elements/tetrahedra*/
      n_grid_faces     = readInt(file);  /*Number of grid faces*/
      n_mesh_vertices  = readInt(file);  /*Number of embedded vertices*/
      n_mesh_faces     = readInt(file);  /*Number of embedded faces*/

      message("%d gridVertices", n_grid_vertices);
      message("%d gridElements", n_grid_elements);
      message("%d gridFaces", n_grid_faces);

      message("%d meshVertices", n_mesh_vertices);
      message("%d meshFaces", n_mesh_faces);

      n_static_vertices = 0;
      n_static_faces    = 0;
      n_rigid_vertices  = 0;
      n_rigid_faces     = 0;
      n_cloth_vertices  = 0;
      n_cloth_faces     = 0;

      gridVertices     = new GridVertex<T>[n_grid_vertices];
      gridElements     = new GridElement[n_grid_elements];
      gridFaces        = new GridFace[n_grid_faces];
      meshVertices     = new GridVertex<T>[n_mesh_vertices];
      meshVerticesMap  = new int[n_mesh_vertices];
      meshFaces        = new GridFace[n_mesh_faces];

      staticVertices    = 0;
      staticFaces       = 0;

      for(int i=0;i<n_grid_vertices;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> gridVertices[i];

#ifdef _DEBUG
        message("GridVertex[%d] %f, %f, %f, %d", i,
                gridVertices[i].v[0],
                gridVertices[i].v[1],
                gridVertices[i].v[2],
                gridVertices[i].mapping);
#endif
      }

      for(int i=0;i<n_grid_elements;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> gridElements[i];

#ifdef _DEBUG
        message("GridElement[%d] %d, %d, %d, %d", i,
                gridElements[i].v[0],
                gridElements[i].v[1],
                gridElements[i].v[2],
                gridElements[i].v[3]);
#endif
      }

      for(int i=0;i<n_grid_faces;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> gridFaces[i];

#ifdef _DEBUG
        message("GridFace[%d] %d, %d, %d | %d, %f, %f, %f, %f | %f, %f", i,
                gridFaces[i].v[0],
                gridFaces[i].v[1],
                gridFaces[i].v[2],
                gridFaces[i].material,
                gridFaces[i].diffuseColor[0],
                gridFaces[i].diffuseColor[1],
                gridFaces[i].diffuseColor[2],
                gridFaces[i].diffuseColor[3],
                gridFaces[i].kd,
                gridFaces[i].ks);
#endif
      }

      for(int i=0;i<n_mesh_vertices;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> meshVertices[i];
        meshVerticesMap[i] = meshVertices[i].mapping;

#ifdef _DEBUG
        message("MeshVertices[%d], %f, %f, %f, %d", i,
                meshVertices[i].v[0],
                meshVertices[i].v[1],
                meshVertices[i].v[2],
                meshVertices[i].mapping);
#endif
      }

      for(int i=0;i<n_mesh_faces;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> meshFaces[i];
#ifdef _DEBUG
        message("MeshFace[%d] %d, %d, %d | %d, %f, %f, %f, %f | %f, %f", i,
                meshFaces[i].v[0],
                meshFaces[i].v[1],
                meshFaces[i].v[2],
                meshFaces[i].material,
                meshFaces[i].diffuseColor[0],
                meshFaces[i].diffuseColor[1],
                meshFaces[i].diffuseColor[2],
                meshFaces[i].diffuseColor[3],
                meshFaces[i].kd,
                meshFaces[i].ks);
#endif
      }

      /*Load static geometry, static geometry does not move and or
        deform. However, it can collide with other non static
        objects.*/

      n_static_vertices  = readInt(file); /*Number of static vertices*/
      n_static_faces     = readInt(file); /*Number of static faces*/

      message("%d staticVertices", n_static_vertices);
      message("%d staticFaces", n_static_faces);

      staticVertices     = new GridVertex<T>[n_static_vertices];
      staticFaces        = new GridFace[n_static_faces];

      for(int i=0;i<n_static_vertices;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> staticVertices[i];
#ifdef _DEBUG
        message("StaticVertices[%d], %f, %f, %f, %d", i,
                staticVertices[i].v[0],
                staticVertices[i].v[1],
                staticVertices[i].v[2],
                staticVertices[i].mapping);

#endif
        /*Please note that in initial versions, static vertices do not
          have a mapping defined.*/
      }

      for(int i=0;i<n_static_faces;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> staticFaces[i];
#ifdef _DEBUG
        message("StaticFace[%d] %d, %d, %d | %d, %f, %f, %f, %f | %f, %f", i,
                staticFaces[i].v[0],
                staticFaces[i].v[1],
                staticFaces[i].v[2],
                staticFaces[i].material,
                staticFaces[i].diffuseColor[0],
                staticFaces[i].diffuseColor[1],
                staticFaces[i].diffuseColor[2],
                staticFaces[i].diffuseColor[3],
                staticFaces[i].kd,
                staticFaces[i].ks);
#endif
      }

      /*Load rigid geometry*/
      n_rigid_vertices  = readInt(file); /*Number of rigid vertices*/
      n_rigid_faces     = readInt(file); /*Number of rigid faces*/

      message("%d rigidVertices", n_rigid_vertices);
      message("%d rigidFaces", n_rigid_faces);

      rigidVertices     = new GridVertex<T>[n_rigid_vertices];
      rigidVerticesMap  = new int[n_rigid_vertices];
      rigidFaces        = new GridFace[n_rigid_faces];

      Tree<int> rigidElements;

      for(int i=0;i<n_rigid_vertices;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> rigidVertices[i];
        rigidVerticesMap[i] = rigidVertices[i].mapping;

        /*Extract all object ids*/
        rigidElements.uniqueInsert(rigidVerticesMap[i]);

#ifdef _DEBUG
        message("RigidVertices[%d], %f, %f, %f, %d", i,
                rigidVertices[i].v[0],
                rigidVertices[i].v[1],
                rigidVertices[i].v[2],
                rigidVertices[i].mapping);
#endif
      }

      /*The number of unique mapping ids of rigid vertices give the
        number of rigid objects in this scene.*/
      n_rigid_elements = rigidElements.size();

      message("%d rigid elements", n_rigid_elements);

      for(int i=0;i<n_rigid_faces;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> rigidFaces[i];
#ifdef _DEBUG
        message("RigidFace[%d] %d, %d, %d | %d, %f, %f, %f, %f | %f, %f", i,
                rigidFaces[i].v[0],
                rigidFaces[i].v[1],
                rigidFaces[i].v[2],
                rigidFaces[i].material,
                rigidFaces[i].diffuseColor[0],
                rigidFaces[i].diffuseColor[1],
                rigidFaces[i].diffuseColor[2],
                rigidFaces[i].diffuseColor[3],
                rigidFaces[i].kd,
                rigidFaces[i].ks);
#endif
      }

      /*Load cloth geometry*/
      n_cloth_vertices  = readInt(file); /*Number of cloth vertices*/
      n_cloth_faces     = readInt(file); /*Number of cloth faces*/

      message("%d clothVertices", n_cloth_vertices);
      message("%d clothFaces", n_cloth_faces);

      clothVertices     = new GridVertex<T>[n_cloth_vertices];
      clothFaces        = new GridFace[n_cloth_faces];

      for(int i=0;i<n_cloth_vertices;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> clothVertices[i];
#ifdef _DEBUG
        message("ClothVertices[%d], %f, %f, %f, %d", i,
                clothVertices[i].v[0],
                clothVertices[i].v[1],
                clothVertices[i].v[2],
                clothVertices[i].mapping);
#endif
      }

      for(int i=0;i<n_cloth_faces;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);
        ss >> clothFaces[i];
#ifdef _DEBUG
        message("ClothFace[%d] %d, %d, %d | %d, %f, %f, %f, %f | %f, %f", i,
                clothFaces[i].v[0],
                clothFaces[i].v[1],
                clothFaces[i].v[2],
                clothFaces[i].material,
                clothFaces[i].diffuseColor[0],
                clothFaces[i].diffuseColor[1],
                clothFaces[i].diffuseColor[2],
                clothFaces[i].diffuseColor[3],
                clothFaces[i].kd,
                clothFaces[i].ks);
#endif
      }

      int n_constraintDescriptors = readInt(file);
      message("%d constraint descriptors", n_constraintDescriptors);

      for(int i=0;i<n_constraintDescriptors;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);

        ConstraintDescriptor<T> d;
        int type;
        ss >> type;
        d.type = (typename ConstraintDescriptor<T>::ConstraintType) type;
        ss >> d.objectId[0];
        ss >> d.objectId[1];

        ss >> d.vertexId[0][0];
        ss >> d.vertexId[0][1];
        ss >> d.vertexId[0][2];

        ss >> d.vertexId[1][0];
        ss >> d.vertexId[1][1];
        ss >> d.vertexId[1][2];

        ss >> d.interpolated[0];
        ss >> d.interpolated[1];

        ss >> d.bary[0][0];
        ss >> d.bary[0][1];
        ss >> d.bary[0][2];
        ss >> d.bary[0][3];

        ss >> d.bary[1][0];
        ss >> d.bary[1][1];
        ss >> d.bary[1][2];
        ss >> d.bary[1][3];

        ss >> d.p[0][0];
        ss >> d.p[0][1];
        ss >> d.p[0][2];
        ss >> d.p[0][3];

        ss >> d.p[1][0];
        ss >> d.p[1][1];
        ss >> d.p[1][2];
        ss >> d.p[1][3];

        if(!ss.eof()){
          ss >> d.startFrame;
          ss >> d.endFrame;
        }

        constraintDescriptors.append(d);

        std::cout << d << std::endl;
      }

      int n_velocityDescriptors = readInt(file);

      message("%d velocity descriptors", n_velocityDescriptors);

      for(int i=0;i<n_velocityDescriptors;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);

        ExternalVelocityDescriptor<T> vel;
        ss >> vel.rigidObject;
        ss >> vel.angVelocity[0];
        ss >> vel.angVelocity[1];
        ss >> vel.angVelocity[2];
        ss >> vel.linVelocity[0];
        ss >> vel.linVelocity[1];
        ss >> vel.linVelocity[2];

        if(!ss.eof()){
          ss >> vel.startFrame;
          ss >> vel.endFrame;
        }

        velocityDescriptors.append(vel);

        std::cout << vel << std::endl;
      }

      int n_forceDescriptors = readInt(file);

      message("%d force descriptors", n_forceDescriptors);

      for(int i=0;i<n_forceDescriptors;i++){
        std::string line;
        getline(file, line);
        std::stringstream ss(line);

        ExternalForceDescriptor<T> force;
        ss >> force.rigidObject;
        ss >> force.angForce[0];
        ss >> force.angForce[1];
        ss >> force.angForce[2];
        ss >> force.linForce[0];
        ss >> force.linForce[1];
        ss >> force.linForce[2];

        if(!ss.eof()){
          ss >> force.startFrame;
          ss >> force.endFrame;
        }

        forceDescriptors.append(force);

        std::cout << force << std::endl;
      }
    }

    /**************************************************************************/
    ~GridFile(){
      delete[] gridVertices;
      delete[] gridElements;
      delete[] gridFaces;
      delete[] meshVertices;
      delete[] meshVerticesMap;
      delete[] meshFaces;

      delete[] staticVertices;
      delete[] staticFaces;

      delete[] rigidVertices;
      delete[] rigidVerticesMap;
      delete[] rigidFaces;

      delete[] clothVertices;
      delete[] clothFaces;
    }

    /**************************************************************************/
    GridFile(const GridFile& grid):n_grid_vertices(0), n_grid_elements(0),
                                   n_grid_faces(0), n_mesh_vertices(0),
                                   n_mesh_faces(0), n_static_vertices(0),
                                   n_static_faces(0),
                                   n_rigid_vertices(0), n_rigid_faces(0),
                                   n_rigid_elements(0),
                                   n_cloth_vertices(0), n_cloth_faces(0),
                                   gridVertices(0),
                                   gridElements(0), gridFaces(0),
                                   meshVertices(0), meshVerticesMap(0),
                                   meshFaces(0), staticVertices(0),
                                   staticFaces(0), rigidVertices(0),
                                   rigidVerticesMap(0), rigidFaces(0),
                                   clothVertices(0), clothFaces(0){
      *this = grid;
    }

    /**************************************************************************/
    GridFile& operator=(const GridFile& grid){
      if(this != &grid){
        n_grid_vertices   = grid.n_grid_vertices;
        n_grid_elements   = grid.n_grid_elements;
        n_grid_faces      = grid.n_grid_faces;
        n_mesh_vertices   = grid.n_mesh_vertices;
        n_mesh_faces      = grid.n_mesh_faces;
        n_static_vertices = grid.n_static_vertices;
        n_static_faces    = grid.n_static_faces;
        n_rigid_vertices  = grid.n_rigid_vertices;
        n_rigid_faces     = grid.n_rigid_faces;
        n_rigid_elements  = grid.n_rigid_elements;
        n_cloth_vertices  = grid.n_cloth_vertices;
        n_cloth_faces     = grid.n_cloth_faces;

        if(gridVertices != 0){
          delete[] gridVertices;
          delete[] gridElements;
          delete[] gridFaces;
          delete[] meshVertices;
          delete[] meshVerticesMap;
          delete[] meshFaces;
        }

        if(staticVertices != 0){
          delete[] staticVertices;
          delete[] staticFaces;
        }

        if(rigidVertices != 0){
          delete[] rigidVertices;
          delete[] rigidVerticesMap;
          delete[] rigidFaces;
        }

        if(clothVertices != 0){
          delete[] clothVertices;
          delete[] clothFaces;
        }

        PRINT(n_mesh_faces);
        PRINT(n_mesh_vertices);

        gridVertices     = new GridVertex<T>[n_grid_vertices];
        gridElements     = new GridElement[n_grid_elements];
        gridFaces        = new GridFace[n_grid_faces];
        meshVertices     = new GridVertex<T>[n_mesh_vertices];
        meshVerticesMap  = new int[n_mesh_vertices];
        meshFaces        = new GridFace[n_mesh_faces];

        staticVertices   = new GridVertex<T>[n_static_vertices];
        staticFaces      = new GridFace[n_static_faces];

        rigidVertices    = new GridVertex<T>[n_rigid_vertices];
        rigidVerticesMap = new int[n_rigid_vertices];
        rigidFaces       = new GridFace[n_rigid_faces];

        clothVertices    = new GridVertex<T>[n_cloth_vertices];
        clothFaces       = new GridFace[n_cloth_faces];

        for(int i=0;i<n_grid_vertices;i++){
          gridVertices[i] = grid.gridVertices[i];
        }

        for(int i=0;i<n_grid_elements;i++){
          gridElements[i] = grid.gridElements[i];
        }

        for(int i=0;i<n_grid_faces;i++){
          gridFaces[i] = grid.gridFaces[i];
        }

        for(int i=0;i<n_mesh_vertices;i++){
          meshVertices[i] = grid.meshVertices[i];
          meshVerticesMap[i] = grid.meshVerticesMap[i];
        }

        for(int i=0;i<n_mesh_faces;i++){
          meshFaces[i] = grid.meshFaces[i];
        }

        for(int i=0;i<n_static_vertices;i++){
          staticVertices[i] = grid.staticVertices[i];
        }

        for(int i=0;i<n_static_faces;i++){
          staticFaces[i] = grid.staticFaces[i];
        }

        for(int i=0;i<n_rigid_vertices;i++){
          rigidVertices[i]    = grid.rigidVertices[i];
          rigidVerticesMap[i] = grid.rigidVerticesMap[i];
        }

        for(int i=0;i<n_rigid_faces;i++){
          rigidFaces[i] = grid.rigidFaces[i];
        }

        for(int i=0;i<n_cloth_vertices;i++){
          clothVertices[i] = grid.clothVertices[i];
        }

        for(int i=0;i<n_cloth_faces;i++){
          clothFaces[i] = grid.clothFaces[i];
        }

        constraintDescriptors.clear();
        velocityDescriptors.clear();
        forceDescriptors.clear();

        DescriptorListIterator it = grid.constraintDescriptors.begin();

        while(it != grid.constraintDescriptors.end()){
          constraintDescriptors.append(*it++);
        }

        VelocityDescriptorListIterator fit = grid.velocityDescriptors.begin();

        while(fit != grid.velocityDescriptors.end()){
          velocityDescriptors.append(*fit++);
        }

        ForceDescriptorListIterator forceit = grid.forceDescriptors.begin();

        while(forceit != grid.forceDescriptors.end()){
          forceDescriptors.append(*forceit++);
        }
      }
      return *this;
    }

    /**************************************************************************/
    void loadStaticPly(const char* file, int operation = 0){
      DCTetraMesh<T>* staticModel = load_ply<T>(file, operation);
      setStaticMesh(staticModel);
      delete staticModel;
    }

    /**************************************************************************/
    void setStaticMesh(DCTetraMesh<T>* staticModel){
      //DCTetraMesh<T>* staticModel = load_ply<T>(file, operation);
      if(staticVertices != 0){
        delete[] staticVertices;
        delete[] staticFaces;
      }

      n_static_vertices = staticModel->getNVertices();
      n_static_faces    = staticModel->getNHalfFaces();

      staticVertices = new GridVertex<T>[n_static_vertices];
      staticFaces    = new GridFace[n_static_faces];

      for(int i=0;i<n_static_vertices;i++){
        staticVertices[i].v = staticModel->vertices[i].coord;
        staticVertices[i].v[3] = 1;
      }

      for(int i=0;i<n_static_faces;i++){
        staticModel->getTriangleIndices(i, staticFaces[i].v, false);
      }
    }

    /**************************************************************************/
    DCTetraMesh<T>* getMesh()const{
      DCTetraMesh<T>* mesh = new DCTetraMesh<T>();

      /*Add deformable mesh*/
      for(int i=0;i<n_grid_vertices;i++){
        addVertex(mesh,
                  gridVertices[i].v[0],
                  gridVertices[i].v[1],
                  gridVertices[i].v[2], i, Mesh::Deformable, 0, 0);
      }

      for(int i=0;i<n_grid_elements;i++){
        addTetrahedron(mesh,
                       gridElements[i].v[0],
                       gridElements[i].v[1],
                       gridElements[i].v[2],
                       gridElements[i].v[3], Mesh::Deformable);
      }

      for(int i=0;i<n_grid_faces;i++){
        addFace(mesh,
                gridFaces[i].v[0],
                gridFaces[i].v[1],
                gridFaces[i].v[2], Mesh::Deformable);
      }

      /*Add static mesh*/
      int offset = n_grid_vertices;

      for(int i=0;i<n_static_vertices;i++){
        addVertex(mesh,
                  staticVertices[i].v[0],
                  staticVertices[i].v[1],
                  staticVertices[i].v[2], i+offset, Mesh::Static,
                  n_grid_vertices * 3, offset);
      }

      for(int i=0;i<n_static_faces;i++){
        addFace(mesh,
                staticFaces[i].v[0] + offset,
                staticFaces[i].v[1] + offset,
                staticFaces[i].v[2] + offset, Mesh::Static);
      }

#if 1
      /*Add rigid mesh*/
      offset = n_grid_vertices + n_static_vertices;

      /*Set center of masses*/
      for(int i=0;i<n_rigid_elements;i++){
        mesh->addCenterOfMass(Vector4<T>(0,0,0,0), i);
      }

      for(int i=0;i<n_rigid_vertices;i++){
        addVertex(mesh,
                  rigidVertices[i].v[0],
                  rigidVertices[i].v[1],
                  rigidVertices[i].v[2], i+offset, Mesh::Rigid,
                  (n_grid_vertices + n_static_vertices)*3,
                  offset);

        int objectId = rigidVerticesMap[i];

        addRigidVertexMap(mesh, i+offset, objectId);

        mesh->vertices[i+offset].vel_index =
          (offset - n_static_vertices)*3 + objectId * 6;
      }

      //int faceOffset = n_grid_faces + n_static_faces;

      PRINT(n_rigid_faces);
      PRINT(offset);

      for(int i=0;i<n_rigid_faces;i++){
        addFace(mesh,
                rigidFaces[i].v[0] + offset,
                rigidFaces[i].v[1] + offset,
                rigidFaces[i].v[2] + offset, Mesh::Rigid);

        //int objectId = rigidVerticesMap[rigidFaces[i].v[0]];

        //addRigidFaceMap(mesh, i+faceOffset, objectId);
      }

      /*Add cloth mesh*/
      offset = n_grid_vertices + n_static_vertices + n_rigid_vertices;

      for(int i=0;i<n_cloth_vertices;i++){
        addVertex(mesh,
                  clothVertices[i].v[0],
                  clothVertices[i].v[1],
                  clothVertices[i].v[2], i+offset, Mesh::Cloth,
                  n_grid_vertices * 3 + n_static_vertices * 3 +
                  n_rigid_vertices * 6, offset);

        mesh->vertices[i+offset].vel_index =
          (offset - n_static_vertices) * 3 + n_rigid_elements * 6 + i * 3;
      }

      for(int i=0;i<n_cloth_faces;i++){
        addFace(mesh,
                clothFaces[i].v[0] + offset,
                clothFaces[i].v[1] + offset,
                clothFaces[i].v[2] + offset, Mesh::Cloth);
      }
#endif
      mesh->connectHalfEdges2();
      mesh->connectHalfEdges2();

      return mesh;
    }

    /**************************************************************************/
    void convertGridToRigid(){
      delete[] rigidVertices;
      delete[] rigidVerticesMap;
      delete[] rigidFaces;

      n_rigid_vertices = n_grid_vertices;
      n_rigid_faces    = n_grid_faces;
      n_rigid_elements = 1;

      rigidVertices    = new GridVertex<T>[n_rigid_vertices];
      rigidVerticesMap = new int[n_rigid_vertices];
      rigidFaces       = new GridFace[n_rigid_faces];

      for(int i=0;i<n_rigid_vertices;i++){
        rigidVertices[i] = gridVertices[i];
        rigidVerticesMap[i] = 0;
      }

      for(int i=0;i<n_rigid_faces;i++){
        rigidFaces[i] = gridFaces[i];
      }

      n_grid_vertices = 0;
      n_grid_faces = 0;
      n_grid_elements = 0;
      n_mesh_vertices = 0;
      n_mesh_faces = 0;

      delete[] gridVertices;
      delete[] gridFaces;
      delete[] meshVertices;
      delete[] meshVerticesMap;
      delete[] meshFaces;

      gridVertices     = new GridVertex<T>[n_grid_vertices];
      gridElements     = new GridElement[n_grid_elements];
      gridFaces        = new GridFace[n_grid_faces];
      meshVertices     = new GridVertex<T>[n_mesh_vertices];
      meshVerticesMap  = new int[n_mesh_vertices];
      meshFaces        = new GridFace[n_mesh_faces];
    }

    /**************************************************************************/
    /*Loads one ply file as one rigid object*/
    void loadRigidPly(const char* file, int operation = 0){
      DCTetraMesh<T>* rigid = load_ply<T>(file, operation);
      setRigidMesh(rigid);
      delete rigid;
    }

    /**************************************************************************/
    void setRigidMesh(DCTetraMesh<T>* rigid){
      if(rigidVertices != 0){
        delete[] rigidVertices;
        delete[] rigidVerticesMap;
        delete[] rigidFaces;
      }

      n_rigid_vertices = rigid->getNVertices();
      n_rigid_faces    = rigid->getNHalfFaces();
      n_rigid_elements = 1;

      rigidVertices    = new GridVertex<T>[n_rigid_vertices];
      rigidVerticesMap = new int[n_rigid_vertices];
      rigidFaces       = new GridFace[n_rigid_faces];

      for(int i=0;i<n_rigid_vertices;i++){
        rigidVertices[i].v = rigid->vertices[i].coord;
        rigidVertices[i].v[3] = 1;
        rigidVerticesMap[i] = 0;
      }

      for(int i=0;i<n_rigid_faces;i++){
        rigid->getTriangleIndices(i, rigidFaces[i].v, false);
      }
    }

    /**************************************************************************/
    /*Loads one ply file as one cloth object*/
    void loadClothPly(const char* file, int operation = 0){
      DCTetraMesh<T>* cloth = load_ply<T>(file, operation);
      if(clothVertices != 0){
        delete[] clothVertices;
        delete[] clothFaces;
      }

      n_cloth_vertices = cloth->getNVertices();
      n_cloth_faces    = cloth->getNHalfFaces();

      clothVertices    = new GridVertex<T>[n_cloth_vertices];
      clothFaces       = new GridFace[n_cloth_faces];

      for(int i=0;i<n_cloth_vertices;i++){
        clothVertices[i].v = cloth->vertices[i].coord;
        clothVertices[i].v[3] = 1;
      }

      for(int i=0;i<n_cloth_faces;i++){
        cloth->getTriangleIndices(i, clothFaces[i].v, false);
      }

      delete cloth;
    }

    /**************************************************************************/
    /*Transforms the model given a transformation matrix*/
    void transform(const Matrix44<T>& mat){
      for(int i=0;i<n_grid_vertices;i++){
        Vector4<T> t = mat * gridVertices[i].v;
        gridVertices[i].v = t;
      }

      for(int i=0;i<n_mesh_vertices;i++){
        Vector4<T> t = mat * meshVertices[i].v;
        meshVertices[i].v = t;
      }

      for(int i=0;i<n_static_vertices;i++){
        Vector4<T> t = mat * staticVertices[i].v;
        staticVertices[i].v = t;
      }

      for(int i=0;i<n_rigid_vertices;i++){
        Vector4<T> t = mat * rigidVertices[i].v;
        rigidVertices[i].v = t;
      }

      for(int i=0;i<n_cloth_vertices;i++){
        Vector4<T> t = mat * clothVertices[i].v;
        clothVertices[i].v = t;
      }
    }

    /**************************************************************************/
    int getRigidObjectFromVertex(int v)const{
      return rigidVerticesMap[v];
    }

    /**************************************************************************/
    int getNGridVertices()const{
      return n_grid_vertices;
    }

    /**************************************************************************/
    int getNMeshVertices()const{
      return n_mesh_vertices;
    }

    /**************************************************************************/
    int getNStaticVertices()const{
      return n_static_vertices;
    }

    /**************************************************************************/
    int getNRigidVertices()const{
      return n_rigid_vertices;
    }

    /**************************************************************************/
    int getNClothVertices()const{
      return n_cloth_vertices;
    }

    /**************************************************************************/
    int getNGridElements()const{
      return n_grid_elements;
    }

    /**************************************************************************/
    int getNRigidElements()const{
      return n_rigid_elements;
    }

    /**************************************************************************/
    int getNGridFaces()const{
      return n_grid_faces;
    }

    /**************************************************************************/
    int getNMeshFaces()const{
      return n_mesh_faces;
    }

    /**************************************************************************/
    int getNStaticFaces()const{
      return n_static_faces;
    }

    /**************************************************************************/
    int getNRigidFaces()const{
      return n_rigid_faces;
    }

    /**************************************************************************/
    int getNClothFaces()const{
      return n_cloth_faces;
    }

    /**************************************************************************/
    GridFile& operator+=(const GridFile& g){
      *this = *this + g;
      return *this;
    }

    /**************************************************************************/
    GridFile operator+(const GridFile& g)const{
      message("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
              n_grid_vertices, n_grid_elements,
              n_grid_faces, n_mesh_vertices,
              n_mesh_faces, n_static_vertices, n_static_faces,
              n_rigid_faces, n_rigid_faces, n_rigid_elements,
              n_cloth_vertices, n_cloth_faces);

      message("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
              g.n_grid_vertices, g.n_grid_elements,
              g.n_grid_faces, g.n_mesh_vertices,
              g.n_mesh_faces, g.n_static_vertices, g.n_static_faces,
              g.n_rigid_vertices, g.n_rigid_faces, g.n_rigid_elements,
              g.n_cloth_vertices, g.n_cloth_faces);

      GridFile r(n_grid_vertices   + g.n_grid_vertices,
                 n_grid_elements   + g.n_grid_elements,
                 n_grid_faces      + g.n_grid_faces,
                 n_mesh_vertices   + g.n_mesh_vertices,
                 n_mesh_faces      + g.n_mesh_faces,
                 n_static_vertices + g.n_static_vertices,
                 n_static_faces    + g.n_static_faces,
                 n_rigid_vertices  + g.n_rigid_vertices,
                 n_rigid_faces     + g.n_rigid_faces,
                 n_cloth_vertices  + g.n_cloth_vertices,
                 n_cloth_faces     + g.n_cloth_faces);

      //--Grid---------------------------------------------------

      for(int i=0;i<n_grid_vertices;i++){
        r.gridVertices[i] = gridVertices[i];
      }
      for(int i=0;i<g.n_grid_vertices;i++){
        r.gridVertices[i+n_grid_vertices] = g.gridVertices[i];
      }

      for(int i=0;i<n_grid_elements;i++){
        r.gridElements[i] = gridElements[i];
      }
      for(int i=0;i<g.n_grid_elements;i++){
        r.gridElements[i+n_grid_elements] = g.gridElements[i];
        r.gridElements[i+n_grid_elements].v[0] += n_grid_vertices;
        r.gridElements[i+n_grid_elements].v[1] += n_grid_vertices;
        r.gridElements[i+n_grid_elements].v[2] += n_grid_vertices;
        r.gridElements[i+n_grid_elements].v[3] += n_grid_vertices;
      }

      for(int i=0;i<n_grid_faces;i++){
        r.gridFaces[i] = gridFaces[i];
      }
      for(int i=0;i<g.n_grid_faces;i++){
        r.gridFaces[i+n_grid_faces] = g.gridFaces[i];
        r.gridFaces[i+n_grid_faces].v[0] += n_grid_vertices;
        r.gridFaces[i+n_grid_faces].v[1] += n_grid_vertices;
        r.gridFaces[i+n_grid_faces].v[2] += n_grid_vertices;
      }

      //--Mesh----------------------------------------------------

      for(int i=0;i<n_mesh_vertices;i++){
        r.meshVertices[i] = meshVertices[i];
        r.meshVerticesMap[i] = meshVerticesMap[i];
      }
      for(int i=0;i<g.n_mesh_vertices;i++){
        r.meshVertices[i+n_mesh_vertices] = g.meshVertices[i];
        r.meshVerticesMap[i+n_mesh_vertices] =
          g.meshVerticesMap[i] + n_grid_elements;
      }

      for(int i=0;i<n_mesh_faces;i++){
        r.meshFaces[i] = meshFaces[i];
      }
      for(int i=0;i<g.n_mesh_faces;i++){
        r.meshFaces[i+n_mesh_faces] = g.meshFaces[i];
        r.meshFaces[i+n_mesh_faces].v[0] += n_mesh_vertices;
        r.meshFaces[i+n_mesh_faces].v[1] += n_mesh_vertices;
        r.meshFaces[i+n_mesh_faces].v[2] += n_mesh_vertices;
      }

      //--Static---------------------------------------------------

      for(int i=0;i<n_static_vertices;i++){
        r.staticVertices[i] = staticVertices[i];
      }
      for(int i=0;i<g.n_static_vertices;i++){
        r.staticVertices[n_static_vertices+i] = g.staticVertices[i];
      }

      for(int i=0;i<n_static_faces;i++){
        r.staticFaces[i] = staticFaces[i];
      }
      for(int i=0;i<g.n_static_faces;i++){
        r.staticFaces[n_static_faces+i] = g.staticFaces[i];
        r.staticFaces[n_static_faces+i].v[0] += n_static_vertices;
        r.staticFaces[n_static_faces+i].v[1] += n_static_vertices;
        r.staticFaces[n_static_faces+i].v[2] += n_static_vertices;
      }

      //--Rigid---------------------------------------------------

      for(int i=0;i<n_rigid_vertices;i++){
        r.rigidVertices[i] = rigidVertices[i];

        r.rigidVerticesMap[i] = rigidVerticesMap[i];
      }

      for(int i=0;i<g.n_rigid_vertices;i++){
        r.rigidVertices[n_rigid_vertices+i] = g.rigidVertices[i];

        r.rigidVerticesMap[n_rigid_vertices+i] =
          g.rigidVerticesMap[i] + n_rigid_elements;
      }

      r.n_rigid_elements = n_rigid_elements + g.n_rigid_elements;

      for(int i=0;i<n_rigid_faces;i++){
        r.rigidFaces[i] = rigidFaces[i];
      }

      for(int i=0;i<g.n_rigid_faces;i++){
        r.rigidFaces[n_rigid_faces+i] = g.rigidFaces[i];
        r.rigidFaces[n_rigid_faces+i].v[0] += n_rigid_vertices;
        r.rigidFaces[n_rigid_faces+i].v[1] += n_rigid_vertices;
        r.rigidFaces[n_rigid_faces+i].v[2] += n_rigid_vertices;
      }

      //-Cloth--------------------------------------------------

      for(int i=0;i<n_cloth_vertices;i++){
        r.clothVertices[i] = clothVertices[i];
      }

      for(int i=0;i<g.n_cloth_vertices;i++){
        r.clothVertices[n_cloth_vertices+i] = g.clothVertices[i];
      }

      for(int i=0;i<n_cloth_faces;i++){
        r.clothFaces[i] = clothFaces[i];
      }

      for(int i=0;i<g.n_cloth_faces;i++){
        r.clothFaces[n_cloth_faces+i] = g.clothFaces[i];
        r.clothFaces[n_cloth_faces+i].v[0] += n_cloth_vertices;
        r.clothFaces[n_cloth_faces+i].v[1] += n_cloth_vertices;
        r.clothFaces[n_cloth_faces+i].v[2] += n_cloth_vertices;
      }

      return r;
    }

    /**************************************************************************/
    void load(const char* fileName){
      GridFile loaded(fileName);

      *this = loaded;
    }

    /**************************************************************************/
    void save(const char* fileName)const{
      std::ofstream outFile;
      outFile.open(fileName);
      outFile << n_grid_vertices << std::endl;
      outFile << n_grid_elements << std::endl;
      outFile << n_grid_faces    << std::endl;
      outFile << n_mesh_vertices << std::endl;
      outFile << n_mesh_faces    << std::endl;

      outFile << std::scientific;
      outFile.precision(10);

      for(int i=0;i<n_grid_vertices;i++){
        outFile << gridVertices[i] << std::endl;
      }

      for(int i=0;i<n_grid_elements;i++){
        outFile << gridElements[i] << std::endl;
      }

      for(int i=0;i<n_grid_faces;i++){
        outFile << gridFaces[i] << std::endl;
      }

      for(int i=0;i<n_mesh_vertices;i++){
        meshVertices[i].mapping = meshVerticesMap[i];
        outFile << meshVertices[i] << std::endl;
      }

      for(int i=0;i<n_mesh_faces;i++){
        outFile << meshFaces[i] << std::endl;
      }

      outFile << n_static_vertices << std::endl;
      outFile << n_static_faces    << std::endl;

      for(int i=0;i<n_static_vertices;i++){
        outFile << staticVertices[i] << std::endl;
      }

      for(int i=0;i<n_static_faces;i++){
        outFile << staticFaces[i] << std::endl;
      }

      outFile << n_rigid_vertices << std::endl;
      outFile << n_rigid_faces    << std::endl;

      for(int i=0;i<n_rigid_vertices;i++){
        rigidVertices[i].mapping = rigidVerticesMap[i];
        outFile << rigidVertices[i] << std::endl;
      }

      for(int i=0;i<n_rigid_faces;i++){
        outFile << rigidFaces[i] << std::endl;
      }

      outFile << n_cloth_vertices << std::endl;
      outFile << n_cloth_faces    << std::endl;

      for(int i=0;i<n_cloth_vertices;i++){
        outFile << clothVertices[i] << std::endl;
      }

      for(int i=0;i<n_cloth_faces;i++){
        outFile << clothFaces[i] << std::endl;
      }

      /*Constraints*/
      outFile << constraintDescriptors.size() << std::endl;

      DescriptorListIterator it = constraintDescriptors.begin();

      while(it != constraintDescriptors.end()){
        ConstraintDescriptor<T> d = *it++;
        outFile << (int)d.type << " "
                << d.objectId[0] << " "
                << d.objectId[1] << " "

                << d.vertexId[0][0] << " "
                << d.vertexId[0][1] << " "
                << d.vertexId[0][2] << " "
                << d.vertexId[1][0] << " "
                << d.vertexId[1][1] << " "
                << d.vertexId[1][2] << " "

                << d.interpolated[0] << " "
                << d.interpolated[1] << " "

                << d.bary[0][0] << " "
                << d.bary[0][1] << " "
                << d.bary[0][2] << " "
                << d.bary[0][3] << " "

                << d.bary[1][0] << " "
                << d.bary[1][1] << " "
                << d.bary[1][2] << " "
                << d.bary[1][3] << " "

                << d.p[0][0] << " "
                << d.p[0][1] << " "
                << d.p[0][2] << " "
                << d.p[0][3] << " "

                << d.p[1][0] << " "
                << d.p[1][1] << " "
                << d.p[1][2] << " "
                << d.p[1][3] << " "
                << d.startFrame << " "
                << d.endFrame << std::endl;
      }

      /*Velocities*/
      outFile << velocityDescriptors.size() << std::endl;

      VelocityDescriptorListIterator fit = velocityDescriptors.begin();

      while(fit != velocityDescriptors.end()){
        ExternalVelocityDescriptor<T> vel = *fit++;
        outFile << vel.rigidObject << " "
                << vel.angVelocity[0] << " "
                << vel.angVelocity[1] << " "
                << vel.angVelocity[2] << " "
                << vel.linVelocity[0] << " "
                << vel.linVelocity[1] << " "
                << vel.linVelocity[2] << " "
                << vel.startFrame << " "
                << vel.endFrame << std::endl;
      }

      /*Forces*/
      outFile << forceDescriptors.size() << std::endl;

      ForceDescriptorListIterator forceit = forceDescriptors.begin();

      while(forceit != forceDescriptors.end()){
        ExternalForceDescriptor<T> force = *forceit++;
        outFile << force.rigidObject << " "
                << force.angForce[0] << " "
                << force.angForce[1] << " "
                << force.angForce[2] << " "
                << force.linForce[0] << " "
                << force.linForce[1] << " "
                << force.linForce[2] << " "
                << force.startFrame << " "
                << force.endFrame << std::endl;
      }

      outFile.close();
    }

    /**************************************************************************/
    BBox<T> getGridBBox()const{
      BBox<T> box;
      for(int i=0;i<n_grid_vertices;i++){
        box.addPoint(gridVertices[i].v);
      }
      return box;
    }

    /**************************************************************************/
    BBox<T> getStaticBBox()const{
      BBox<T> box;
      for(int i=0;i<n_static_vertices;i++){
        box.addPoint(staticVertices[i].v);
      }
      return box;
    }

    /**************************************************************************/
    BBox<T> getRigidBBox()const{
      BBox<T> box;
      for(int i=0;i<n_rigid_vertices;i++){
        box.addPoint(rigidVertices[i].v);
      }
      return box;
    }

    /**************************************************************************/
    BBox<T> getClothBBox()const{
      BBox<T> box;
      for(int i=0;i<n_cloth_vertices;i++){
        box.addPoint(clothVertices[i].v);
      }
      return box;
    }

    /**************************************************************************/
    BBox<T> getBBox()const{
      BBox<T> box = getGridBBox();
      box.addBox(getStaticBBox());
      box.addBox(getRigidBBox());
      box.addBox(getClothBBox());
      return box;
    }

    /**************************************************************************/
    /*Moves center of bbox to origin*/
    void center(){
      BBox<T> box = getBBox();
      Vector4<T> c = box.center();
      transform(translationMatrix(-c));
    }

    /**************************************************************************/
    const GridVertex<T>& getGridVertex(int i)const{
      return gridVertices[i];
    }

    /**************************************************************************/
    const GridElement& getGridElement(int i)const{
      return gridElements[i];
    }

    /**************************************************************************/
    const GridFace& getGridFace(int i)const{
      return gridFaces[i];
    }

    /**************************************************************************/
    void setGridFace(int i, const GridFace& face){
      gridFaces[i] = face;
    }

    /**************************************************************************/
    const GridVertex<T>& getMeshVertex(int i)const{
      return meshVertices[i];
    }

    /**************************************************************************/
    int getMeshVertexMap(int i)const{
      return meshVerticesMap[i];
    }

    /**************************************************************************/
    const GridFace& getMeshFace(int i)const{
      return meshFaces[i];
    }

    /**************************************************************************/
    void setMeshFace(int i, const GridFace& face){
      meshFaces[i] = face;
    }

    /**************************************************************************/
    const GridVertex<T>& getStaticVertex(int i)const{
      return staticVertices[i];
    }

    /**************************************************************************/
    const GridFace& getStaticFace(int i)const{
      return staticFaces[i];
    }

    /**************************************************************************/
    void setStaticFace(int i, const GridFace& face){
      staticFaces[i] = face;
    }

    /**************************************************************************/
    const GridVertex<T>& getRigidVertex(int i)const{
      return rigidVertices[i];
    }

    /**************************************************************************/
    int getRigidVertexMap(int i)const{
      return rigidVerticesMap[i];
    }

    /**************************************************************************/
    const GridFace& getRigidFace(int i)const{
      return rigidFaces[i];
    }

    /**************************************************************************/
    void setRigidFace(int i, const GridFace& face){
      rigidFaces[i] = face;
    }

    /**************************************************************************/
    const GridVertex<T>& getClothVertex(int i)const{
      return clothVertices[i];
    }

    /**************************************************************************/
    const GridFace& getClothFace(int i)const{
      return clothFaces[i];
    }

    /**************************************************************************/
    void setClothFace(int i, const GridFace& face){
      clothFaces[i] = face;
    }

    /**************************************************************************/
    int getNConstraintDescriptors()const{
      return constraintDescriptors.size();
    }

    /**************************************************************************/
    ConstraintDescriptor<T>& getConstraintDescriptor(int i){
      return constraintDescriptors[i];
    }

    /**************************************************************************/
    void addConstraintDescriptor(ConstraintDescriptor<T> d){
      message("sf = %d, ef = %d", d.startFrame, d.endFrame);
      constraintDescriptors.append(d);
    }

    /**************************************************************************/
    void clearConstraintDescriptors(){
      constraintDescriptors.clear();
    }

    /**************************************************************************/
    int getNVelocityDescriptors()const{
      return velocityDescriptors.size();
    }

    /**************************************************************************/
    ExternalVelocityDescriptor<T>& getVelocityDescriptor(int i){
      return velocityDescriptors[i];
    }

    /**************************************************************************/
    void addVelocityDescriptor(ExternalVelocityDescriptor<T> d){
      velocityDescriptors.append(d);
    }

    /**************************************************************************/
    void clearVelocityDescriptors(){
      velocityDescriptors.clear();
    }

    /**************************************************************************/
    int getNForceDescriptors()const{
      return forceDescriptors.size();
    }

    /**************************************************************************/
    ExternalForceDescriptor<T>& getForceDescriptor(int i){
      return forceDescriptors[i];
    }

    /**************************************************************************/
    void addForceDescriptor(ExternalForceDescriptor<T> d){
      forceDescriptors.append(d);
    }

    /**************************************************************************/
    void clearForceDescriptors(){
      forceDescriptors.clear();
    }

  protected:
    /**************************************************************************/
    int readInt(std::ifstream& stream){
      std::string line;
      if(stream.good()){
        getline(stream, line);
        if(line.size() == 0){
          return 0;
        }
        std::stringstream ss(line);
        int r;
        ss >> r;
        return r;
      }
      return 0;
    }

    /**************************************************************************/
    int n_grid_vertices;
    int n_grid_elements;
    int n_grid_faces;
    int n_mesh_vertices;
    int n_mesh_faces;
    int n_static_vertices;
    int n_static_faces;
    int n_rigid_vertices;
    int n_rigid_faces;
    int n_rigid_elements;
    int n_cloth_vertices;
    int n_cloth_faces;

    /**************************************************************************/
    GridVertex<T>* gridVertices;
    GridElement*   gridElements;
    GridFace*      gridFaces;
    GridVertex<T>* meshVertices;
    int*           meshVerticesMap;
    GridFace*      meshFaces;

    GridVertex<T>* staticVertices;
    GridFace*      staticFaces;

    GridVertex<T>* rigidVertices;
    int*           rigidVerticesMap;
    GridFace*      rigidFaces;

    GridVertex<T>* clothVertices;
    GridFace*      clothFaces;

    /**************************************************************************/
    List<ConstraintDescriptor<T> > constraintDescriptors;
    List<ExternalVelocityDescriptor<T> > velocityDescriptors;
    List<ExternalForceDescriptor<T> > forceDescriptors;
  };
}

#endif/*GRIDFILE_HPP*/
