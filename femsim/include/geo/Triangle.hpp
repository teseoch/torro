/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "math/Vector3.hpp"
#include "math/Matrix22.hpp"
#include "math/Matrix33.hpp"

#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/Quaternion.hpp"

#include "math/Ray.hpp"
#include "core/Exception.hpp"
#include <stdio.h>
#include "math/RootFind.hpp"

namespace tsl{
#define EDGE_TOL 1E-7

#define DET_EPS 1e-12*0

  /**************************************************************************/
  template<class T>
  inline bool lineSegmentEdgeIntersection(const Vector4<T>& x0,
                                          const Vector4<T>& x1,
                                          const Vector4<T>& v0,
                                          const Vector4<T>& v1,
                                          const Vector4<T>& planeNormal,
                                          Vector4<T>& res,
                                          T* t = 0,
                                          Vector4<T>* bary = 0){
    Vector4<T> edge = v1 - v0;
    Vector4<T> tangent = cross(edge, planeNormal).normalized();

    T tt = dot(tangent, v0 - x0) / dot(tangent, x1 - x0);

    if(t){
      *t = tt;
    }

    res = x0 * ((T)1.0 - tt) + x1 * tt;

    Vector4<T> resv0 = res-v0;

    T b = dot(resv0, resv0) / dot(v1 - v0, v1 - v0);

    if(bary){
      (*bary)[0] = (T)1.0-b;
      (*bary)[1] = b;
    }

    if(tt > (T)0.0 && tt < (T)1.0){
      if(b > (T)0.0 && b < (T)1.0){
        return true;
      }
    }

    return false;
  }

  /**************************************************************************/
  template<class T>
  class Triangle{
  public:
    /**************************************************************************/
    Triangle();

    /**************************************************************************/
    Triangle(const Vector4<T>& a, const Vector4<T>& b, const Vector4<T>& c);

    /**************************************************************************/
    Triangle(const Triangle<T>& t);

    /**************************************************************************/
    void computeNormal();

    /**************************************************************************/
    Triangle<T>& operator=(const Triangle<T>& t);

    /**************************************************************************/
    void set(const Vector4<T>& a, const Vector4<T>& b, const Vector4<T>& c);

    /**************************************************************************/
    Vector4<T> v[3];
    Vector4<T> n;

    /**************************************************************************/
    /*Returns projection of p on the triangle*/
    Vector4<T> getProjection(const Vector4<T>& p)const;

    /**************************************************************************/
    /*Projects a vector on the plane of the triangle*/
    Vector4<T> getProjectedVector(const Vector4<T>& vec)const;

    /**************************************************************************/
    /*Projects a vector on the normal of the plane*/
    Vector4<T> getNormalProjectedVector(const Vector4<T>& vec)const;

    /**************************************************************************/
    T getPlaneDistance(const Vector4<T>& p)const;

    /**************************************************************************/
    Vector4<T> getBarycentricCoordinates(const Vector4<T>& p)const;

    /**************************************************************************/
    void getEdgeProjections(const Vector4<T>& p, Vector4<T>* ep)const;

    /**************************************************************************/
    /*Computes the clamped projections of p on all three edges of this
      triangle. If there is no projection for a particular edge, the
      closest vertex is is returned (clamping) for that edge*/
    void getClampedEdgeProjections(const Vector4<T>& p, Vector4<T>* ep)const;

    /**************************************************************************/
    T getArea()const;

    /**************************************************************************/
    Vector4<T> getCoordinates(const Vector4<T>& bary)const;

    /**************************************************************************/
    Vector4<T> clampWeights(const Vector4<T>& bary, T mn, T mx)const;

    /**************************************************************************/
    Vector4<T> edgeClampedBarycentricCoordinates(const Vector4<T>& p)const;

    /**************************************************************************/
    /*Creates an edge from a vertex of the triangle to vertex p, if p
      is outside the triangle, then there is one edge-edge crossing
      for which this function returns the barycentric coordinates.*/
    Vector4<T> edgeCrossedBarycentricCoordinates(const Vector4<T>& p)const;

    /**************************************************************************/
    bool barycentricInTriangle(const Vector4<T>& bary, T eps)const;

    /**************************************************************************/
    bool barycentricInTriangleBand(const Vector4<T>& bary, T eps)const;

    /**************************************************************************/
    bool barycentricInTriangleDist(const Vector4<T>& bary, T dist)const;

    /**************************************************************************/
    bool pointProjectsOnTriangle(const Vector4<T>& p, T eps=0,
                                 Vector4<T>* b=0)const;

    /**************************************************************************/
    T getSignedDistance2(const Vector4<T>& p, T* sgn)const;

    /**************************************************************************/
    T getSignedDistance(const Vector4<T>& p,
                        Vector4<T>*       bary = 0,
                        Vector4<T>*       projection = 0,
                        T*                ddd = 0)const;

    /**************************************************************************/
    bool intersects(const Ray<T>& ray, Vector4<T>& colPoint,
                    T* t = 0, Vector4<T>* bary = 0)const;

    /**************************************************************************/
    /*Checks for an intersection of a ray, defined by a point p and
      vector dir. If there is an intersection, the function returns
      true and res contains the intersection point and t such that res
      = p + dir * t*/
    bool rayIntersection(const Vector4<T>& p,
                         const Vector4<T>& dir,
                         Vector4<T>& res,
                         T* t = 0,
                         Vector4<T>* bary = 0,
                         T deps = 0,
                         T* detp = 0)const;

    /**************************************************************************/
    bool lineIntersection(const Vector4<T>& p,
                          const Vector4<T>& dir,
                          Vector4<T>& res,
                          T* t             = 0,
                          Vector4<T>* bary = 0,
                          T deps           = 0,
                          T* detp          = 0)const;

    /**************************************************************************/
    template<class Y>
    friend std::ostream& operator<<(std::ostream& os, const Triangle<Y>& t);

    /**************************************************************************/
    template<class Y>
    friend int intersection(const Triangle<Y>& p,
                            const Triangle<Y>& x,
                            const Vector4<Y>& vu,
                            const Vector4<Y>& v,
                            Vector4<Y>& res,
                            const Y eps,
                            Vector4<Y>* bary1,
                            Triangle<Y>* ppp,
                            Y beps,
                            Y normalFactor,
                            int method, Y* s,
                            bool forced);

    /**************************************************************************/
    template<class Y>
    friend int rigidIntersection(const Triangle<Y>& p,
                                 const Vector4<Y>& cp,
                                 const Quaternion<Y>& rp,
                                 const Triangle<Y>& t,
                                 const Vector4<Y>& ct,
                                 const Quaternion<Y>& rt,
                                 const Vector4<Y>& vu,
                                 const Vector4<Y>& cvu,
                                 const Quaternion<Y>& rvu,
                                 const Vector4<Y>& v,
                                 const Vector4<Y>& cv,
                                 const Quaternion<Y>& rv,
                                 Vector4<Y>& res,
                                 const Y eps,
                                 Vector4<Y>* bary1,
                                 Triangle<Y>* ppp,
                                 Y beps,
                                 Y normalFactor,
                                 int method,
                                 bool rigidFace,
                                 bool rigidVertex,
                                 Y* s,
                                 bool forced);
  };
}


#endif/*TRIANGLE_HPP*/
