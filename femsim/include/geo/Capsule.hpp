/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CAPSULE_HPP
#define CAPSULE_HPP

#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  /*Capsule for line intersection tests. The capsule is a cylinder
    with spherical ends. */
  template<class T>
  class Capsule{
  public:
    /**************************************************************************/
    Capsule();

    /**************************************************************************/
    Capsule(T r);

    /**************************************************************************/
    Capsule(T r, const Vector4<T>& start, const Vector4<T>& end);

    /**************************************************************************/
    Capsule(const Capsule<T>& capsule);

    /**************************************************************************/
    ~Capsule();

    /**************************************************************************/
    Capsule<T>& operator=(const Capsule<T>& caps);

    /**************************************************************************/
    /*Compute signed distance with point to infinite cylinder*/
    T signedDistance(const Vector4<T>& p, T* palpha = 0)const;

    /**************************************************************************/
    /*Compute signed distance with line to infinite cylinder*/
    T signedDistance(const Vector4<T>& a, const Vector4<T>& b,
                     T* palpha = 0, T* pbeta = 0, bool* pparallel = 0)const;

    /**************************************************************************/
    bool intersectionWithLine(const Vector4<T>& a, const Vector4<T>&b,
                              Vector4<T>* ca, Vector4<T>* cb,
                              T* palpha1 = 0, T* pbeta1 = 0,
                              T* palpha2 = 0, T* pbeta2 = 0,
                              bool* pparallel = 0)const;
  protected:
    /**************************************************************************/
    T radius;
    Vector4<T> start;
    Vector4<T> end;
  };
}

#endif/*CAPSULE_HPP*/
