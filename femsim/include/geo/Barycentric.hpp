/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef BARYCENTRIC_HPP
#define BARYCENTRIC_HPP

namespace tsl{
  /**************************************************************************/
  /*Returns the Barycentric coordinates of p within triangle p1, p2, p3,
    with v0 = p2-p1, v1 = p3-p1, v2 = p-p1 */
  template<class T>
  inline Vector4<T> getBarycentricCoordinatesFace(Vector4<T>& v0,
                                                  Vector4<T>& v1,
                                                  Vector4<T>& v2){
    //Vector4f v0 = p2-p1;
    //Vector4f v1 = p3-p1;
    //Vector4f v2 = p -p1;

    T dot00 = dot(v0, v0);
    T dot01 = dot(v0, v1);
    T dot02 = dot(v0, v2);
    T dot11 = dot(v1, v1);
    T dot12 = dot(v1, v2);

    T invDenom = (T)1.0 / (dot00 * dot11 - dot01 * dot01);
    T u        = (dot11 * dot02 - dot01 * dot12)*invDenom;
    T v        = (dot00 * dot12 - dot01 * dot02)*invDenom;

    return Vector4<T>((T)1.0-u-v, u, v, 0);
  }

  /**************************************************************************/
  /*v0 = p2-p1, v1 = p-p1*/
  template<class T>
  inline Vector4<T> getBarycentricCoordinatesEdge(Vector4<T>& v0,
                                                  Vector4<T>& v1){
    T dot00 = dot(v0, v0);
    T dot01 = dot(v0, v1);
    T u = (dot01)/(dot00);
    return Vector4<T>((T)1.0-u, u, 0, 0);
  }

  /**************************************************************************/
  template<class T>
  inline bool barycentricFaceMatch(const Vector4<T>& a, const Vector4<T>& b){
    char v1[3];
    char v2[3];
    int count = 0;
    for(int i=0;i<3;i++){
      if(a[i] <= (T)0.0){
        v1[i] = 1;
      }else if(a[i] <= (T)1.0){
        v1[i] = 2;
      }else{
        v1[i] = 3;
      }

      if(b[i] <= (T)0.0){
        v2[i] = 1;
      }else if(b[i] <= (T)1.0){
        v2[i] = 2;
      }else{
        v2[i] = 3;
      }
      if(v1[i] == v2[i]){
        count++;
      }
    }
    if(count == 3){
      return true;
    }
    return false;
  }
}

#endif/*BARYCENTRIC_HPP*/
