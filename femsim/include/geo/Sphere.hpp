/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Sphere{
  public:
    /**************************************************************************/
    Sphere();

    /**************************************************************************/
    Sphere(T r);

    /**************************************************************************/
    Sphere(T r, const Vector4<T>& c);

    /**************************************************************************/
    Sphere(const Sphere<T>& sphere);

    /**************************************************************************/
    ~Sphere();

    /**************************************************************************/
    Sphere<T>& operator=(const Sphere<T>& sphere);

    /**************************************************************************/
    T signedDistance(const Vector4<T>& p)const;

    /**************************************************************************/
    T signedDistance(const Vector4<T>& a, const Vector4<T>& b,
                     T* pbeta = 0)const;

    /**************************************************************************/
    bool pointInside(const Vector4<T>& p)const;

    /**************************************************************************/
    bool intersectionWithLine(const Vector4<T>& a, const Vector4<T>&b,
                              Vector4<T>* ca, Vector4<T>* cb,
                              T* pbeta1 = 0, T* pbeta2 = 0)const;
  protected:
    /**************************************************************************/
    T radius;
    Vector4<T> center;
  };
}

#endif/*SPHERE_HPP*/
