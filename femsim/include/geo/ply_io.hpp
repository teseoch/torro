/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef PLY_IO_HPP
#define PLY_IO_HPP

#include "datastructures/DCEList.hpp"

namespace tsl{

#define PLY_NONE      0
#define PLY_FILL_GAPS 1
#define PLY_SIMPLIFY  2
#define PLY_TESSELATE 4

  /**************************************************************************/
  template<class T>
  DCTetraMesh<T>*
  load_ply(const char* filename,
           int operation = PLY_FILL_GAPS|PLY_SIMPLIFY|PLY_TESSELATE);
  /**************************************************************************/
  template<class T>
  void
  save_ply(const char* filename, DCTetraMesh<T>* mesh);
}


#endif/*PLY_IO_HPP*/
