/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef EDGE_HPP
#define EDGE_HPP

#include "math/Vector4.hpp"
#include "math/Quaternion.hpp"
#include "math/RootFind.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  inline bool barycentricEdgeMatch(const Vector4<T>& a, const Vector4<T>& b){
    char v1[2];
    char v2[2];
    int count = 0;
    for(int i=0;i<2;i++){
      if(a[i] <= (T)0.0){
        v1[i] = 1;
      }else if(a[i] <= (T)1.0){
        v1[i] = 2;
      }else{
        v1[i] = 3;
      }

      if(b[i] <= (T)0.0){
        v2[i] = 1;
      }else if(b[i] <= (T)1.0){
        v2[i] = 2;
      }else{
        v2[i] = 3;
      }
      if(v1[i] == v2[i]){
        count++;
      }
    }
    if(count == 2){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  class Edge{
  public:
    /**************************************************************************/
    Edge();

    /**************************************************************************/
    Edge(const Vector4<T>& a, const Vector4<T>& b,
         const Vector4<T>& c, const Vector4<T>& d);

    /**************************************************************************/
    Edge(const Edge<T>& edge);

    /**************************************************************************/
    const Vector4<T>& vertex(int i)const{
      return v[i];
    }

    /**************************************************************************/
    const Vector4<T>& faceVertex(int i)const{
      return fv[i];
    }

    /**************************************************************************/
    const Vector4<T>& faceNormal(int i)const{
      return n[i];
    }

    /**************************************************************************/
    const Vector4<T>& faceTangent(int i)const{
      return t[i];
    }

    /**************************************************************************/
    int faceId(int i)const{
      return faceIds[i];
    }

    /**************************************************************************/
    int edgeId()const{
      return id;
    }

    /**************************************************************************/
    int faceVertexId(int i)const{
      return faceVertexIds[i];
    }

    /**************************************************************************/
    T getTangentDistance(int i)const{
      return tangentDistance[i];
    }

    /**************************************************************************/
    void setFaceId(int index, int _id){
      faceIds[index] = _id;
    }

    /**************************************************************************/
    void setEdgeId(int _id){
      id = _id;
    }

    /**************************************************************************/
    void setFaceVertexId(int index, int _id){
      faceVertexIds[index] = _id;
    }

    /**************************************************************************/
    T getFaceSignedDistance(int i, const Vector4<T>& p){
      return dot(p - v[0], n[i]);
    }

    /**************************************************************************/
    Edge<T>& operator=(const Edge<T>& edge);

    /**************************************************************************/
    void set(const Vector4<T>& a, const Vector4<T>& b,
             const Vector4<T>& c, const Vector4<T>& d);

    /**************************************************************************/
    bool isConvex()const;

    /**************************************************************************/
    bool isConcave()const;

    /**************************************************************************/
    bool isIndeterminate()const;

    /**************************************************************************/
    T getFaceArea(int f)const;

    /**************************************************************************/
    bool faceEdgeIntersection(const Edge<T>& edge, int faceId,
                              Vector4<T>& intersection,
                              bool& parallel, bool& degenerate, T eps,
                              bool debug = false)const;

    /**************************************************************************/
    bool edgesIntersect(const Edge<T>& edge, T eps,
                        bool forced = false, bool debug = false)const;

    /**************************************************************************/
    bool edgeIntersectsWith(const Edge<T>& edge, T eps,
                            bool forced = false, bool debug = false)const;

    /**************************************************************************/
    bool pointOutside(const Vector4<T>& p)const;

    /**************************************************************************/
    Vector4<T> getProjection(const Vector4<T>& p)const;

    /**************************************************************************/
    Vector4<T> getBarycentricCoordinates(const Vector4<T>& p)const;

    /**************************************************************************/
    T getLength()const;

    /**************************************************************************/
    Vector4<T> getCoordinates(const Vector4<T>& bary)const;

    /**************************************************************************/
    static Vector4<T> clampWeights(const Vector4<T>& bary, T mn, T mx);

    /**************************************************************************/
    bool barycentricOnEdge(const Vector4<T>& bary, T eps)const;

    /**************************************************************************/
    bool barycentricOnEdgeBand(const Vector4<T>& bary, T eps)const;

    /**************************************************************************/
    bool barycentricOnEdgeDist(const Vector4<T>& bary, T dist)const;

    /**************************************************************************/
    bool pointProjectsOnEdge(const Vector4<T>& p, T eps = (T)0.0)const;

    /**************************************************************************/
    T getPlaneDistance(const Vector4<T>& p, const Vector4<T>& n)const;

    /**************************************************************************/
    Vector4<T> getOutwardNormal(const Edge<T>& edge,
                                Vector4<T>* reference = 0,
                                int depth = 0, bool* potentialError = 0)const;

    /**************************************************************************/
    Vector4<T> getNormalToVertex(const Vector4<T>& v)const;

    /**************************************************************************/
    void infiniteProjections(const Edge<T>& edge,
                             Vector4<T>* p1, Vector4<T>* p2,
                             Vector4<T>* b1 = 0, Vector4<T>* b2 = 0,
                             bool* parallel = 0)const;

    /**************************************************************************/
    T getDistance(const Vector4<T>& p, Vector4<T>* c = 0);

    /**************************************************************************/
    T getDistance(const Edge<T>& e,
                  Vector4<T>* p1 = 0, Vector4<T>* p2 = 0,
                  Vector4<T>* b1 = 0, Vector4<T>* b2 = 0,
                  bool* degenerate = 0)const;

    /**************************************************************************/
    T getSignedDistance(const Edge<T>& e,
                        Vector4<T>* p1 = 0, Vector4<T>* p2 = 0,
                        Vector4<T>* b1 = 0, Vector4<T>* b2 = 0,
                        bool* degenerate = 0,
                        const Vector4<T>* reference = 0)const;

    /**************************************************************************/
    template<class Y>
    friend bool edgesIntersectedInTime(const Edge<Y>& e0,
                                       const Edge<Y>& eu,
                                       const Edge<Y>& p0,
                                       const Edge<Y>& pu);

    /**************************************************************************/
    template<class Y>
    friend std::ostream& operator<<(std::ostream& os, const Edge<Y>& e);

    /**************************************************************************/
    template<class Y>
    friend int intersection(const Edge<Y>& p,  const Edge<Y>& x,
                            const Edge<Y>& eu, const Edge<Y>& e,
                            Vector4<Y>& res, const Y eps,
                            Vector4<Y>* b1, Vector4<Y>* b2,
                            Edge<Y>* ppp, Edge<Y>* eee,
                            Y beps, int method, const Vector4<Y>* ref, Y* s);

    /**************************************************************************/
    template<class Y>
    friend int rigidIntersection(const Edge<Y>& p,
                                 const Vector4<Y>& comp,
                                 const Quaternion<Y>& rotp,
                                 const Edge<Y>& x,
                                 const Vector4<Y>& comx,
                                 const Quaternion<Y>& rotx,
                                 const Edge<Y>& eu,
                                 const Vector4<Y>& comeu,
                                 const Quaternion<Y>& roteu,
                                 const Edge<Y>& e,
                                 const Vector4<Y>& come,
                                 const Quaternion<Y>& rote,
                                 Vector4<Y>& res, const Y eps,
                                 Vector4<Y>* b1, Vector4<Y>* b2,
                                 Edge<Y>* ppp, Edge<Y>* eee,
                                 Y beps, int method, bool r1, bool r2,
                                 Y* s,
                                 const Vector4<Y>* reference);

    /**************************************************************************/
    template<class Y>
    friend int intersection(const Edge<Y>& p,  const Edge<Y>& x,
                            const Vector4<Y>& vu, const Vector4<Y>& v,
                            Vector4<Y>& res, const Y eps,
                            Vector4<Y>* b,
                            Edge<Y>* ppp, Vector4<Y>* vvv,
                            Y beps, int method, Y* s);

    /**************************************************************************/
    template<class Y>
    friend int rigidIntersection(const Edge<Y>& p,
                                 const Vector4<Y>& comp,
                                 const Quaternion<Y>& rotp,
                                 const Edge<Y>& x,
                                 const Vector4<Y>& comx,
                                 const Quaternion<Y>& rotx,
                                 const Vector4<Y>& vu,
                                 const Vector4<Y>& comvu,
                                 const Quaternion<Y>& rotvu,
                                 const Vector4<Y>& v,
                                 const Vector4<Y>& come,
                                 const Quaternion<Y>& rote,
                                 Vector4<Y>& res, const Y eps,
                                 Vector4<Y>* b,
                                 Edge<Y>* ppp, Vector4<Y>* vvv,
                                 Y beps, int method, bool re, bool rv,
                                 Y* s);

    /**************************************************************************/
    friend class Interpolator<T, Edge<T>, Edge<T> >;
    friend class Interpolator<T, Edge<T>, Vector4<T> >;

    /**************************************************************************/
    template<class Y>
    friend Edge<Y> interpolateEdges(const Edge<Y>& e1,
                                    const Edge<Y>& e2,
                                    Y d1, Y d2, Y iso);

    /**************************************************************************/
    template<class Y>
    friend bool edgeCollapsed(const Edge<Y>& e1, const Edge<Y>& e2,
                              bool debug);

  protected:
    /**************************************************************************/
    void computeNormalAndTangent();

    /**************************************************************************/
    Vector4<T> v[2];  /*Vertices of edge*/
    Vector4<T> n[2];  /*Normals of adjacent faces*/
    Vector4<T> t[2];  /*Tangent vectors (are orthogonal to normals)*/
    Vector4<T> fv[2]; /*Face vertices*/
    bool convex;
    bool validFaces[2];
    T faceArea[2];
    T tangentDistance[2];
    int faceIds[2];
    int faceVertexIds[2];
    int id;
  };

  /**************************************************************************/
  template<class T>
  bool edgeCollapsed(const Edge<T>& e1, const Edge<T>& e2, bool debug = false);

  /**************************************************************************/
  template<class T>
  inline Edge<T> interpolateEdges(const Edge<T>& e1, const Edge<T>& e2,
                                  T d1, T d2, T iso=(T)0.0){
    Edge<T> r;
    r.v[0] = linterp(iso, &e1.v[0], &e2.v[0], d1, d2);
    r.v[1] = linterp(iso, &e1.v[1], &e2.v[1], d1, d2);

    r.fv[0] = linterp(iso, &e1.fv[0], &e2.fv[0], d1, d2);
    r.fv[1] = linterp(iso, &e1.fv[1], &e2.fv[1], d1, d2);

    r.computeNormalAndTangent();

    r.setEdgeId(e1.edgeId());
    r.setFaceId(0, e1.faceId(0));
    r.setFaceId(1, e1.faceId(1));

    return r;
  }

  /**************************************************************************/
  template<class T>
  bool edgesIntersectedInTime(const Edge<T>& e0,
                              const Edge<T>& eu,
                              const Edge<T>& p0,
                              const Edge<T>& pu){
    return false;

  }
}

#endif/*EDGE_HPP*/
