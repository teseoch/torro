/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef BBOX_HPP
#define BBOX_HPP

#include "math/Math.hpp"
#include "math/Vector4.hpp"
#include "geo/Triangle.hpp"
#include "math/Ray.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class BBox{
  public:
    /**************************************************************************/
    /*Constructors*/
    BBox(){
      reset();
    }

    /**************************************************************************/
    BBox(const BBox<T>& box):vmin(box.vmin),
                             vmax(box.vmax),
                             cornersDefined(false){
    }

    /**************************************************************************/
    BBox(const Vector4<T>& a,
         const Vector4<T>& b):vmin(a),
                              vmax(b),
                              cornersDefined(false){
    }

    /**************************************************************************/
    ~BBox(){
    }

    /**************************************************************************/
    BBox& operator=(const BBox<T>& b){
      vmin = b.vmin;
      vmax = b.vmax;
      return *this;
    }

    /**************************************************************************/
    /*Resets the bounding box*/
    void reset(){
      vmin.set( FLOAT_MAX,  FLOAT_MAX,  FLOAT_MAX, 0);
      vmax.set(-FLOAT_MAX, -FLOAT_MAX, -FLOAT_MAX, 0);
      cornersDefined = false;
    }

    /**************************************************************************/
    /*Tests if point p is inside this box*/
    bool inside(const Vector4<T>& p) const{
      Vector4<T> mn = (p >= vmin);
      mn.m[3] = MultiplyIdentity(T());

      if(mn.isTrue()){
        Vector4<T> mx = (p <= vmax);
        mx.m[3] = MultiplyIdentity(T());

        if(mx.isTrue()){
          return true;
        }
      }
      return false;
    }

    /**************************************************************************/
    /*Adds a point such that the bounding box now also contains p*/
    void addPoint(const Vector4<T>& p){
      min(vmin, vmin, p);
      max(vmax, vmax, p);

      cornersDefined = false;
    }

    /**************************************************************************/
    /*Adds a box to this box such that it spans both volumes*/
    void addBox(const BBox<T>& box){
      min(vmin, vmin, box.vmin);
      max(vmax, vmax, box.vmax);

      cornersDefined = false;
    }

    /**************************************************************************/
    /*Adds an additional epsilon distance around the box*/
    void addEpsilon(T eps){
      for(int i=0;i<3;i++){
        vmin.m[i] -= eps;
        vmax.m[i] += eps;
      }

      cornersDefined = false;
    }

    /**************************************************************************/
    /*Computes the 8 vertices of this box*/
    void computeCorners()const{
      corners[0].set(vmin.m[0], vmin.m[1], vmin.m[2], 0);
      corners[1].set(vmin.m[0], vmin.m[1], vmax.m[2], 0);
      corners[2].set(vmin.m[0], vmax.m[1], vmin.m[2], 0);
      corners[3].set(vmin.m[0], vmax.m[1], vmax.m[2], 0);
      corners[4].set(vmax.m[0], vmin.m[1], vmin.m[2], 0);
      corners[5].set(vmax.m[0], vmin.m[1], vmax.m[2], 0);
      corners[6].set(vmax.m[0], vmax.m[1], vmin.m[2], 0);
      corners[7].set(vmax.m[0], vmax.m[1], vmax.m[2], 0);
      cornersDefined = true;
    }

    /**************************************************************************/
    /*Tests if point p is outside the box*/
    bool outside(const Vector4<T>& p)const{
      return !inside(p);
    }

    /**************************************************************************/
    /*Set the minimum of this box*/
    void setMin(const Vector4<T>& a){
      vmin = a;
      cornersDefined = false;
    }

    /**************************************************************************/
    /*Set the maximum of this box*/
    void setMax(const Vector4<T>& b){
      vmax = b;
      cornersDefined = false;
    }

    /**************************************************************************/
    /*Returns the minimum of this box*/
    const Vector4<T>& getMin()const{
      return vmin;
    }

    /**************************************************************************/
    /*Return the maximum of this box*/
    const Vector4<T>& getMax()const{
      return vmax;
    }

    /**************************************************************************/
    /*Returns the center of this box*/
    Vector4<T> center()const{
      return (vmin + vmax)/(T)2.0;
    }

    /**************************************************************************/
    /*Test for intersection with triangle*/
    bool intersection(const Vector4<T>& a,
                      const Vector4<T>& b,
                      const Vector4<T>& c)const{
      if(inside(a) || inside(b) || inside(c)){
        return true;
      }

      if(!cornersDefined){
        computeCorners();
      }

      /*It is possible that the triangle intersects the box*/
      Vector4<T> n = cross(a-c, b-c);
      bool planeIntersection = false;
      int sgn = 0;

      for(int i=0;i<8;i++){
        Vector4<T> vec = corners[i] - c;
        T proj = dot(vec, n);

        //if(fabs(proj) < 1E-5){
        //planeIntersection = true;
        //break;
        //}

        int curSign = (int)Sign(proj);
        if(sgn == 0){
          sgn = curSign;
        }else if(sgn != curSign){
          planeIntersection = true;
          break;
        }
      }

      if(planeIntersection){
        /*Plane box intersection, but is the box outside or inside the
          triangle?*/
        Triangle<T> tri(a, b, c);
        for(int i=0;i<8;i++){
          if(tri.pointProjectsOnTriangle(corners[i])){
            return true;
          }
        }
      }
      return false;
    }

    /**************************************************************************/
    /*Test for intersection with a line-segment a-b*/
    bool intersection(const Vector4<T>& a,
                      const Vector4<T>& b)const{
      if(inside(a) || inside(b)){
        /*At least one vertex is inside the box, so there is an
          intersection*/
        return true;
      }

      /*Use ray box intersection test*/

      error("Not implemented yet");
      return false;
    }

    /**************************************************************************/
    /*Test for intersection with another box*/
    bool intersection(const BBox<T>& b)const{
      if(b.vmax.m[0] < vmin.m[0]){
        return false;
      }else if(b.vmin.m[0] > vmax.m[0]){
        return false;
      }

      if(b.vmax.m[1] < vmin.m[1]){
        return false;
      }else if(b.vmin.m[1] > vmax.m[1]){
        return false;
      }

      if(b.vmax.m[2] < vmin.m[2]){
        return false;
      }else if(b.vmin.m[2] > vmax.m[2]){
        return false;
      }

      return true;
    }

    /**************************************************************************/
    /*Test if box is inside sphere*/
    bool inside(Vector4<T>& center, T radius)const{
      int inside_corners = 0;

      if(!cornersDefined){
        computeCorners();
      }

      for(int i=0;i<8;i++){
        if((corners[i]-center).length() <= radius){
          inside_corners++;
        }
      }

      if(inside_corners == 8){
        return true;
      }
      return false;
    }

    /**************************************************************************/
    /*Performs a faster bounding sphere test with another sphere*/
    bool intersectsSphere(const Vector4<T>& origin,
                          T radius)const{
      /*Compute radius of bounding sphere*/
      Vector4<T> boxCenter = center();
      Vector4<T> r = getMax() - boxCenter;
      T boxRadius = r.length();

      Vector4<T> distance = origin - boxCenter;

      if(distance.length() <= radius + boxRadius){
        /*Intersection of spheres*/
        return true;
      }
      return false;
    }

    /**************************************************************************/
    /*Box intersects with sphere*/
    bool intersects(Vector4<T>& center, T radius)const{
      bool pretest = intersectsSphere(center, radius);
      if(pretest == false){
        return false;
      }

      int inside_corners = 0;

      /*Check if sphere intersects the faces of the cube*/
      if((center[0] > vmin[0] - radius) && (center[0] < vmax[0] + radius)){
        if((center[1] > vmin[1] - radius) && (center[1] < vmax[1] + radius)){
          if((center[2] > vmin[2] - radius) && (center[2] < vmax[2] + radius)){
            return true;
          }
        }
      }

      for(int i=0;i<8;i++){
        if((corners[i]-center).length() <= radius){
          inside_corners++;
        }
      }
      if(inside_corners > 0 && inside_corners < 8){
        return true;
      }

      return false;
    }

    /**************************************************************************/
    /*Checks if a line segment intersects with this box. Does not test
      for some infinite line.*/
    bool intersects(const Ray<T>& ray)const{
      bool pretest = intersectsSphere(ray.getOrigin(),
                                      ray.getDirection().length());
      if(pretest == false){
        return false;
      }

      //Smith's method
      T tmin, tmax, tymin, tymax, tzmin, tzmax;
      T t0, t1;
      t0 = 0;
      t1 = 1;

      const Vector4<T>* bounds = &vmin;

      tmin =  (bounds[  ray.signs[0]][0] - ray.origin[0])*ray.inv_dir[0];
      tmax =  (bounds[1-ray.signs[0]][0] - ray.origin[0])*ray.inv_dir[0];
      tymin = (bounds[  ray.signs[1]][1] - ray.origin[1])*ray.inv_dir[1];
      tymax = (bounds[1-ray.signs[1]][1] - ray.origin[1])*ray.inv_dir[1];

      /*in case of a division by zero the result will be +- inf, but
        in case of 0/0, the result is NaN. Check this!*/

      if(isnan(tmin)){
        tmin = Sign(tmin) * (T)1.0/(T)0.0;
      }

      if(isnan(tmax)){
        tmax = Sign(tmax) * (T)1.0/(T)0.0;
      }

      if(isnan(tymin)){
        tymin = Sign(tymin) * (T)1.0/(T)0.0;
      }

      if(isnan(tymax)){
        tymax = Sign(tymax) * (T)1.0/(T)0.0;
      }

      if( (tmin > tymax) || (tymin > tmax)){
        return false;
      }

      if(tymin > tmin){
        tmin = tymin;
      }
      if(tymax < tmax){
        tmax = tymax;
      }

      tzmin = (bounds[  ray.signs[2]][2] - ray.origin[2])*ray.inv_dir[2];
      tzmax = (bounds[1-ray.signs[2]][2] - ray.origin[2])*ray.inv_dir[2];

      if(isnan(tzmin)){
        tzmin = Sign(tzmin) * (T)1.0/(T)0.0;
      }

      if(isnan(tzmax)){
        tzmax = Sign(tzmax) * (T)1.0/(T)0.0;
      }

      if( (tmin > tzmax) || (tzmin > tmax)){
        return false;
      }
      if(tzmin > tmin){
        tmin = tzmin;
      }
      if(tzmax < tmax){
        tmax = tzmax;
      }

      return ((tmin < t1) && (tmax > t0));
    }

    /**************************************************************************/
    /*Box contains a sphere*/
    bool contains(const Vector4<T>& center,
                  T radius)const{
      return inside(center);
    }

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const BBox<Y>& v);
  protected:
    Vector4<T> vmin;
    Vector4<T> vmax;
    mutable Vector4f corners[8];
    mutable bool cornersDefined;
  };

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const BBox<T>& b){
    os << "BBox " << std::endl;
    os << "min = " << std::endl;
    os << b.vmin << std::endl;

    os << "max = " << std::endl;
    os << b.vmax << std::endl;

    return os;
  }
}

#endif/*BBOX_HPP*/
