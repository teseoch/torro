/* Copyright (C) 2013-2019 by Mickeal Verschoor*/

#ifndef HYPERELASTICITY_FEM_HPP
#define HYPERELASTICITY_FEM_HPP

#include "FEMModel.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class HyperElasticityFEM : public FEMModel<T>{
  public:
    /**************************************************************************/
    HyperElasticityFEM(DCTetraMesh<T>* _mesh, int _nElements);

    /**************************************************************************/
    virtual ~HyperElasticityFEM();

    /**************************************************************************/
    virtual void computeElementStiffnessMatrixAndForce(MatrixT<12, 12, T>& K,
                                                       MatrixT<12, 1,  T>& rhs,
                                                       MatrixT<12, 1,  T>& ipos,
                                                       MatrixT<12, 1,  T>& cpos,
                                                       TetrahedronData<T>& tet,
                                                       int id,
                                                       T dt,
                                                       bool initial,
                                                       bool verbose);

    /**************************************************************************/
    void evaluateStrainEnergyDensityFunction(Matrix44<T>& F);

    /**************************************************************************/
    void computeSDerivatives(Vector4<T>& ds,
                             Matrix44<T>& dds,
                             Vector4<T>& q,
                             Matrix44<T>& dq,
                             Matrix44<T> (& ddq)[3],
                             const T s,
                             const Vector4<T>& sigma);

    /**************************************************************************/
    void computeHDerivatives(T& h,
                             Vector4<T>& dh,
                             Matrix44<T>& ddh,
                             const Vector4<T>& sigma,
                             const Vector4<T>& q,
                             const Matrix44<T>& dq,
                             const Matrix44<T> (& ddq)[3],
                             const Vector4<T>& u,
                             const Matrix44<T>& du,
                             const Matrix44<T> (&ddu) [3]);

    /**************************************************************************/
    void computeMDerivatives(T& m,
                             Vector4<T>& dm,
                             Matrix44<T>& ddm,
                             const Vector4<T>& sigma);

    /**************************************************************************/
    void computeUDerivatives(Vector4<T>& u,
                             Matrix44<T>& du,
                             Matrix44<T> (& ddu)[3],
                             const T m,
                             const Vector4<T>& dm,
                             const Matrix44<T>& ddm,
                             const Vector4<T>& sigma);

    /**************************************************************************/
    void computeHP(Matrix44<T>& H,
                   Matrix44<T>& P,
                   Matrix44<T>& F,
                   Matrix44<T>& MQ,
                   bool verbose = false);

    /**************************************************************************/
    void computeForceDerivative(Matrix44<T>& der,
                                Matrix44<T>& derRL,
                                Matrix44<T>& derRR,
                                Matrix44<T>& derDH,
                                Matrix44<T>& U,
                                Matrix44<T>& D,
                                Matrix44<T>& Q,
                                Matrix44<T>& V,
                                Matrix44<T>& VT,
                                Matrix44<T>& P,
                                Matrix44<T>& H,
                                Matrix44<T>& G,
                                int x, int y,
                                bool verbose);

  protected:
    Matrix44<T>* shapeFunctions;
  };
}

#endif/*HYPERELASTICITY_FEM_HPP*/
