/* Copyright (C) 2013-2019 by Mickeal Verschoor*/

#ifndef TETRAHEDRON_DATA_HPP
#define TETRAHEDRON_DATA_HPP

#include "math/Matrix44.hpp"
#include "math/Matrix.hpp"
#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class TetrahedronData{
  public:
    /**************************************************************************/
    TetrahedronData(){
      //stiffnessFactor = (T)1.0;
      //volumeForcesActive = false;
    }

    /**************************************************************************/
    Matrix44<T> initialPos;
    Matrix44<T> c;
    Matrix44<T> lastPos;
    Matrix44<T> lastRotation;
    MatrixT<12, 12, T> initialK;
    int indices[4];

    T volume;
    T initialVolume;
    T mass;
    T* pointers[144];
  };

}

#endif/*TETRAHEDRON_DATA_HPP*/
