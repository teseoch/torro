/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef OCTREE_HPP
#define OCTREE_HPP

#include "core/tsldefs.hpp"
#include "datastructures/List.hpp"
#include "datastructures/Tree.hpp"
#include "math/Vector4.hpp"
#include "geo/BBox.hpp"
#include "datastructures/OctreeInternals.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class Y>
  class Octree{
  public:
    /**************************************************************************/
    typedef OctreeInternals::OctreeNode<T, Y> Node;

    /**************************************************************************/
    typedef Octree<T, Y>                      self_type;

    /**************************************************************************/
    typedef typename List<Node*>::Iterator    NodeListIterator;

    /**************************************************************************/
    typedef typename List<T>::Iterator        ValueListIterator;

    /**************************************************************************/
    Octree(){
      root = new Node();
      root->clear();
      root->id = node_id;
      node_id++;
      BBox<Y> initial_box;
      initial_box.setMin(Vector4<Y>(-1,-1,-1,0));
      initial_box.setMax(Vector4<Y>( 1, 1, 1,0));
      root->bbox = initial_box;
      max_items = 10;
    }

    /**************************************************************************/
    Octree(const BBox<Y>& box){
      root = new Node();
      root->clear();
      root->id = node_id;
      node_id++;
      root->bbox = box;
      max_items = 10;
    }

    /**************************************************************************/
    Octree(const self_type&){
    }

    /**************************************************************************/
    self_type& operator=(const self_type&){
    }

    /**************************************************************************/
    virtual ~Octree(){
      delete root;
    }

    /**************************************************************************/
    void setMaxItems(int m){
      max_items = m;
    }

    /**************************************************************************/
    void insert(T& a){
      if(root->insert(a) == false){
        //message("root = %p", root);
        //std::cout << root->bbox << std::endl;
        //std::cout << getPosition<T, Y>(a) << " " << getId(a) << std::endl;
        //message("Inside %d", root->bbox.inside(getPosition<T, Y>(a)));
        //message("Outside %d", root->bbox.outside(getPosition<T, Y>(a)));
        int quadrant = root->quadrant(getPosition<T, Y>(a));
        int this_new_quadrant = 7-quadrant;
        Node* new_root = new Node();
        new_root->bbox = root->bbox;

        /*Extend bbox of root*/
        Vector4<Y> boxmin = root->bbox.getMin();
        Vector4<Y> boxmax = root->bbox.getMax();
        Vector4<Y> boxsize = boxmax - boxmin;

        Vector4<Y> newmax, newmin;

        switch(quadrant){
        case 0:
          newmax = boxmax;
          break;
        case 1:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          break;
        case 2:
          newmax = boxmax;
          newmax[1] += boxsize[1];
          break;
        case 3:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          newmax[1] += boxsize[1];
          break;
        case 4:
          newmax = boxmax;
          newmax[2] += boxsize[2];
          break;
        case 5:
          newmax = boxmax;
          newmax[0] += boxsize[0];
          newmax[2] += boxsize[2];
          break;
        case 6:
          newmax = boxmax;
          newmax[1] += boxsize[1];
          newmax[2] += boxsize[2];
          break;
        case 7:
          newmax = boxmax + boxsize;
        }
        newmin = newmax - boxsize*(Y)2.0;

        new_root->bbox.setMax(newmax);
        new_root->bbox.setMin(newmin);
        new_root->center = new_root->bbox.center();

        /*Split new root, which creates the childs and bounding boxes*/
        new_root->split(1,max_items,0,true);

        /*Replace one child with old root*/
        delete new_root->childs[this_new_quadrant];
        new_root->childs[this_new_quadrant] = root;
        root->parent = new_root;
        root = new_root;

        /*insert into new root recursively, since the new point might
          still be outside the newly created cells*/
        insert(a);
      }
    }

    /**************************************************************************/
    void remove(T& a){
      root->remove(a);
    }

    /**************************************************************************/
    void split(int depth, bool empty_split = false){
      root->split(depth, max_items, 0, empty_split);
    }

    /**************************************************************************/
    void merge(){
      root->merge();
    }

    /**************************************************************************/
    void computeNodeAverage(){
      root->computeNodeAverage();
    }

    /**************************************************************************/
    void balance(){
      List<Node*> leafNodes;
      root->getLeafNodes(leafNodes);

      NodeListIterator it = leafNodes.begin();
      NodeListIterator end = leafNodes.end();

      while(it != end){
        Node* curNode = *it;

        message("%p, end = %p, listSize = %d", curNode, end, leafNodes.size());

        if(curNode->hasToSplit()){
          if(curNode->childs){
            for(int i = 0;i<8;i++){
              Node* child = curNode->childs[i];
              message("append %p of node %p is root %d",
                      child, curNode, curNode->isRoot());
              leafNodes.append(child);
            }
          }

          /*Inspect neighbors*/
          Node* nb;
          for(int i=0;i<6;i++){
            nb = curNode->neighbor(i);
            if(nb){
              if(nb->hasToSplit()){
                leafNodes.append(nb);
              }
            }
          }
        }
        it++;
        leafNodes.removeAt(0);
      }
    }

    /**************************************************************************/
    void getLeafNodes(List<Node*>& list){
      root->getLeafNodes(list);
    }

    /**************************************************************************/
    void getBranchNodes(List<Node*>& list){
      root->getBranchNodes(list);
    }

    /**************************************************************************/
    int size(){
      return root->size();
    }

    /**************************************************************************/
    BBox<Y> getBox(){
      return root->getBox();
    }

#if 1
    /**************************************************************************/
    int getNeighboringElements(BBox<Y>& range, List<T>& el){
      List<Node*> nodes = getNeighborhood(range);

      NodeListIterator it = nodes.begin();

      List<T> elements;
      int n_found = 0;

      while(it != nodes.end()){
        Node* node = *it++;
        node->getAllElements(elements);
      }

      ValueListIterator eit = elements.begin();

      while(eit != elements.end()){
        T object = *eit++;
        if(range.inside(getPosition<T, Y>(object))){
          el.append(object);
          n_found++;
        }
      }
      return n_found;
    }
#endif

    /**************************************************************************/
    int getNeighboringElements(Vector4<Y>& center, Y radius, List<T>& el){
      List<Node*> nodes = getNeighborhood(center, radius);

      NodeListIterator it = nodes.begin();

      List<T> elements;
      int n_found = 0;

      while(it != nodes.end()){
        Node* node = *it++;
        node->getAllElements(elements);
      }

      //message("       Elements.size = %d", elements.size());
      //elements.unique();
      //message("Unique Elements.size = %d", elements.size());

      ValueListIterator eit = elements.begin();

      while(eit != elements.end()){
        T object = *eit++;
        if((getPosition<T, Y>(object) - center).length() < radius){
          el.append(object);
          n_found++;
        }
      }
      return n_found;
    }

    /**************************************************************************/
    List<Node*> getNeighborhood(Vector4<Y>& center, Y radius){
      return root->getNeighborhood(center, radius);
    }

#if 1
    /**************************************************************************/
    List<Node*> getNeighborhood(BBox<Y>& range){
      return root->getNeighborhood(range);
    }
#endif

    /**************************************************************************/
    void trace(const Vector4<Y>& a, const Vector4<Y>& b, List<T>& list){
    }

    /**************************************************************************/
    void computeBounds(){
      root->computeBounds();
    }
  protected:
    /**************************************************************************/
    Node* root;
    int max_items;
  };
}

#endif/*OCTREE_HPP*/
