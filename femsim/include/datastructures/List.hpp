/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef LIST_HPP
#define LIST_HPP

#include "core/tsldefs.hpp"
#include <ostream>
#include "math/Compare.hpp"
#include "core/Allocator.hpp"
#include "datastructures/ListInternals.hpp"

namespace tsl{
  /**************************************************************************/
  /*Double linked list used to store unordered elements*/
  template<typename T, class Alloc = Allocator<T> >
  class List{
  public:
    /**************************************************************************/
    typedef T                              value_type;

    /**************************************************************************/
    typedef List<T>                        self_type;

    /**************************************************************************/
    typedef ListInternals::ListNode<T>     Node;

    /**************************************************************************/
    typedef ListInternals::ListIterator<T> Iterator;

    /**************************************************************************/
    typedef typename Alloc::template rebind<Node>::other NodeAlloc;

    /**************************************************************************/
    List(){
      first = nodeAlloc.allocateAndConstruct(1);
      last  = nodeAlloc.allocateAndConstruct(1);

      first->next = last;
      last->prev  = first;
      sz = 0;
    }

    /**************************************************************************/
    ~List(){
      clear();

      nodeAlloc.destroyAndDeallocate(first);
      nodeAlloc.destroyAndDeallocate(last);
    }

    /**************************************************************************/
    List(const self_type& c){
      first = nodeAlloc.allocateAndConstruct(1);
      last  = nodeAlloc.allocateAndConstruct(1);

      first->next = last;
      last->prev  = first;
      sz = 0;

      Iterator it = c.begin();
      Iterator e  = c.end();
      while(it != e){
        append(*it++);
      }
    }

    /**************************************************************************/
    self_type& operator=(const self_type& c){
      if(this == &c){
        message("Self assignment");
      }else{
        clear();
        Iterator it = c.begin();
        Iterator e  = c.end();
        while(it != e){
          append(*it++);
        }
      }
      return *this;
    }

    /**************************************************************************/
    Iterator begin() const{
      Iterator i;
      i.ptr = first->next;
      return i;
    }

    /**************************************************************************/
    Iterator end() const{
      Iterator i;
      i.ptr = last;
      return i;
    }

    /**************************************************************************/
    void clear(){
      while(first->next != last){
        Node* next = first->next->next;

        nodeAlloc.destroyAndDeallocate(first->next);

        first->next = next;
        first->next->prev = first;
      }
      sz = 0;
    }

    /**************************************************************************/
    void append(const value_type& t){

      Node* n = nodeAlloc.allocateAndConstruct(1);

      Node* current = last->prev;
      current->next = n;
      n->prev = current;
      n->data = t;
      last->prev = n;
      n->next = last;
      sz++;
    }

    /**************************************************************************/
    void append(self_type& l){
      Iterator iter = l.begin();
      while(l.size() != 0){
        append(*iter);
        l.removeFromIterator(iter);
      }
    }

    /**************************************************************************/
    void appendAndKeep(const self_type& l){
      Iterator iter = l.begin();
      while(iter != l.end()){
        append(*iter);
        iter++;
      }
    }

    /**************************************************************************/
    void appendByValue(value_type t){
      Node* n = nodeAlloc.allocateAndConstruct(1);

      Node* current = last->prev;
      current->next = n;
      n->prev = current;
      n->data = t;
      last->prev = n;
      n->next = last;
      sz++;
    }

    /**************************************************************************/
    /*Removes the current node and advances to the next node*/
    /*If used in an iteration, do not increase the iterator after removal*/
    void removeFromIterator(Iterator& it){
      Node* curr = it.ptr;
      Node* prev = it.ptr->prev;
      Node* next = it.ptr->next;

      next->prev = prev;
      prev->next = next;

      nodeAlloc.destroyAndDeallocate(curr);

      sz--;
      it.ptr = next;
    }

    /**************************************************************************/
    void remove(value_type val){
      Iterator it = begin();
      while(it != end()){
        if(*it == val){
          removeFromIterator(it);
          return;
        }
        it++;
      }
    }

    /**************************************************************************/
    void removeAt(int i){
      Node* current = first->next;
      int index = 0;

      if(i>=sz)
        return;

      while(index != i){
        current = current->next;
        index++;
      }
      Node* prev = current->prev;
      Node* next = current->next;

      prev->next = next;
      next->prev = prev;

      delete current;
      sz--;
    }

    /**************************************************************************/
    int size()const{
      return sz;
    }

    /**************************************************************************/
    value_type& operator[](int idx) const{
      tslassert(idx >=0 && idx < sz);

      Node* current = first->next;
      int index = 0;
      while(index != idx){
        current = current->next;
        index++;
      }
      return current->data;
    }

    /**************************************************************************/
    template<class U, class AC>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const List<U, AC>& l);

  protected:
    /**************************************************************************/
    Node* first;
    Node* last;
    int sz;
    NodeAlloc nodeAlloc;
  };

  /**************************************************************************/
  template<typename U, class Alloc>
  inline std::ostream& operator<<(std::ostream& os, const List<U, Alloc>& l){
    typename List<U, Alloc>::Iterator it = l.begin();
    while(it != l.end()){
      os << *it++ << ',';
    }
    return os;
  }
}

#endif/*LIST_HPP*/
