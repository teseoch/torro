/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef ADJACENCYLIST_HPP
#define ADJACENCYLIST_HPP
#include "datastructures/Tree.hpp"
#include<ostream>

namespace tsl{

#define ADJV2
  //#undef ADJV2

  /**************************************************************************/
  /*The AdjacencyList keeps track of all connections within a graph.*/
  class AdjacencyList{
  public:
    /**************************************************************************/
    /*Creates an AdjacencyList for _size vertices with a maximim degree*/
    AdjacencyList(long _size, long _max_degree);

    /**************************************************************************/
    ~AdjacencyList();

    /**************************************************************************/
    /*Connects two vertices*/
    void connect(int a, int b);

    /**************************************************************************/
    /*Disconnects two vertices*/
    void disconnect(int a, int b);

    /**************************************************************************/
    /*Gets the degree of a vertex*/
    int getDegree(int a)const;

    /**************************************************************************/
    /*Gets the nth connected item to vertex a*/
    int getConnectedItem(int a, int idx)const;

    /**************************************************************************/
    /*Clears all connections with vertex a*/
    void clear(int a);

    /**************************************************************************/
    /*Read connections from file*/
    void readFromFile(std::ifstream& file);

    /**************************************************************************/
    /*Save graph to file*/
    void saveToFile(std::ofstream& file)const;

    /**************************************************************************/
    /*Gets the maximum degree of the graph*/
    long getMaxDegree()const{
      return max_degree;
    }

    /**************************************************************************/
    /*Returns the size of the graph (number of vertices)*/
    long getSize()const{
      return size;
    }

    /**************************************************************************/
    /*Remaps the graph*/
    void remap(Tree<int>& mapping);

    /**************************************************************************/
    /*Compresses the graph, i.e., removes unaccessed vertices*/
    void compress(Tree<int>& mapping, int new_size);

    /**************************************************************************/
    static const int undefinedIndex = -1;

    /**************************************************************************/
    /*Prints the graph*/
    void print();

  protected:
    /**************************************************************************/
    void extendSize();

    /**************************************************************************/
    void extendDegree();

    /**************************************************************************/
#ifdef ADJV2
    int** adj;
    int*  sizes;   /*sizes of arrays stored in adj*/
    int*  degrees; /*degrees of nodes*/
#else
    int* adj;
#endif
    long  size;
    long  max_degree;
  };
}


#endif/*ADJACENCYLIST_HPP*/
