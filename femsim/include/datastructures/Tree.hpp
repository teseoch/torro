/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef TREE_HPP
#define TREE_HPP
#include <iostream>
#include "core/tsldefs.hpp"
#include <stdlib.h>
#include "math/Math.hpp"
#include "math/Compare.hpp"
#include <string.h>
#include <datastructures/List.hpp>
#include <datastructures/Pair.hpp>
#include "core/types.hpp"
#include "core/Allocator.hpp"
#include "datastructures/TreeInternals.hpp"

/*AVL tree*/

namespace tsl{
  /**************************************************************************/
  /*Tree provides a container in which elements of type T are stored
    and ordered given their less and equal functions for type T. If no
    such function is provided, the default < and == operators are
    used. The underlying tree is an autobalancing AVL tree.*/
  template<class T, class Alloc = Allocator<T> >
  class Tree{
  public:
    /**************************************************************************/
    typedef T                                            value_type;

    /**************************************************************************/
    typedef TreeInternals::TreeNode<T>                   Node;

    /**************************************************************************/
    typedef TreeInternals::TreeIterator<T>               Iterator;

    /**************************************************************************/
    typedef Tree<T>                                      self_type;

    /**************************************************************************/
    typedef typename Alloc::template rebind<Node>::other NodeAlloc;

    /**************************************************************************/
    Tree(bool(*le)(const T&, const T&) = 0,
         bool(*eq)(const T&, const T&) = 0):root(0), beginNode(0),
                                            updateBegin(true),
                                            lastNode(0),
                                            updateLast(true),
                                            order(0), sz(0){
      l_less = le;
      l_equal = eq;
    }

    /**************************************************************************/
    Tree(const List<value_type>& list):root(0), beginNode(0),
                                       updateBegin(true),
                                       lastNode(0),
                                       updateLast(true),
                                       order(0), sz(0){
      l_less = 0;
      l_equal = 0;

      typename List<T>::Iterator lit = list.begin();
      while(lit != list.end()){
        insert(*lit++);
      }
    }

    /**************************************************************************/
    ~Tree(){
      if(root){
        destroyNode(root);
        nodeAlloc.destroyAndDeallocate(root);
      }
    }

    /**************************************************************************/
    static const int undefinedIndex = -1;

    /**************************************************************************/
    /*Implicitly advances the iterator to the next node*/
    void remove(Iterator& it){
      Node* node = it.ptr;

      if(node->l_child != 0 &&
         node->r_child != 0){
        /*Node will be reused and will contain the next logical value,
          no need to update the iterator*/
      }else{
        /*Node will be deleted, advance iterator to next*/
        it.next();
      }

      deleteNode2(node);
    }

    /**************************************************************************/
    /*Removes the node having value t.*/
    void remove(value_type& t){
      Node* node = findNode(t);
      if(node == 0){
        return;
      }
      deleteNode2(node);

      if(node == beginNode){
        updateBegin = true;
      }

      if(node == lastNode){
        updateLast = true;
      }
    }

    /**************************************************************************/
    int size()const{
      return sz;
    }

    /**************************************************************************/
    int getDepth(){
      return getDepth(root);
    }

    /**************************************************************************/
    void getBalance(Node* node)const{
      if(node){
        message("node = %p, depth = %d, balance = %d",
                node, node->depth, node->balance);

        getBalance(node->l_child);
        getBalance(node->r_child);
      }
    }

    /**************************************************************************/
    int getBalance()const{
      if(root){
        getBalance(root);
        return root->balance;
      }
      return 0;
    }

    /**************************************************************************/
    void uniqueInsert(value_type& t, int index = 0){
      int findex = findIndex(t);

      if(findex == undefinedIndex){
        insert(t, index);
      }
    }

    /**************************************************************************/
    void insertc(value_type t, int index = 0){
      T tt = t;
      insert(tt, index);
    }

    /**************************************************************************/
    /*AVL insert*/
    void insert(value_type& t, int index = 0){
      sz++;

      if(root == 0){
        //root = new Node(t, index);
        root = nodeAlloc.allocateAndConstruct(1, t, index);
        updateDepth(root, false);
        return;
      }
#if 1
      if(beginNode && (updateBegin == false)){
        /*beginNode is valid*/
        if(l_less){
          if(l_less(t, beginNode->data)){
            /*New value is smaller than begin, update beginnode*/
            updateBegin = true;
          }
        }else{
          if(Compare<T>::less(t, beginNode->data)){
            /*New value is smaller than begin, update beginnode*/
            updateBegin = true;
          }
        }
      }
      //message("lastNode = %p, updateLast = %d", lastNode, updateLast);
      if(lastNode && (updateLast == false)){
        //message("last node = valid");
        /*lastNode is valid*/
        if(l_less){
          if(!l_less(t, lastNode->data)){
            /*New value is larger than last, update lastnode*/
            updateLast = true;
            //message("last node must be updated");
          }
        }else{
          if(!Compare<T>::less(t, lastNode->data)){
            /*New value is larger than last, update lastnode*/
            updateLast = true;
            //message("last node must be updated");
          }
        }
      }
#endif
      Node* curr = root;

      if(l_less){
        while(curr){
          if(l_less(t, curr->data)){
            /*Traverse left*/
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              /*Left child does not exist, create*/
              curr->l_child = nodeAlloc.allocateAndConstruct(1, t, index);
              curr->l_child->parent = curr;
              updateDepth(curr->l_child, false);
              return;
            }
          }else{
            /*Traverse right*/
            if(curr->r_child){
              curr = curr->r_child;
            }else{
              curr->r_child = nodeAlloc.allocateAndConstruct(1, t, index);
              curr->r_child->parent = curr;
              updateDepth(curr->r_child, false);
              return;
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::less(t, curr->data)){
            /*Traverse left*/
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              /*Left child does not exist, create*/
              curr->l_child = nodeAlloc.allocateAndConstruct(1, t, index);
              curr->l_child->parent = curr;
              updateDepth(curr->l_child, false);
              return;
            }
          }else{
            /*Traverse right*/
            if(curr->r_child){
              curr = curr->r_child;
            }else{
              curr->r_child = nodeAlloc.allocateAndConstruct(1, t, index);
              curr->r_child->parent = curr;
              updateDepth(curr->r_child, false);
              return;
            }
          }
        }
      }
    }

    /**************************************************************************/
    Iterator begin(){
      if(updateBegin){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->l_child){
          curr = curr->l_child;
        }
        /*Current node is left most*/
        Iterator it(curr);

        updateBegin = false;

        beginNode = curr;
        return it;
      }else{
        return Iterator(beginNode);
      }
    }

    /**************************************************************************/
    Iterator last(){
      if(updateLast){
        /*Find right most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->r_child){
          curr = curr->r_child;
        }
        /*Current node is left most*/
        Iterator it(curr);

        updateLast = false;
        lastNode = curr;
        return it;
      }else{
        return Iterator(lastNode);
      }
    }

    /**************************************************************************/
    const Iterator begin()const{
      if(updateBegin){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->l_child){
          curr = curr->l_child;
        }
        /*Current node is left most*/
        beginNode = curr;
        updateBegin = false;
        const Iterator it(curr);
        return it;
      }else{
        const Iterator it(beginNode);
        return it;
      }
    }

    /**************************************************************************/
    const Iterator last()const{
      if(updateLast){
        /*Find left most item*/
        Node* curr = root;
        if(curr == 0){
          return end();
        }

        while(curr->r_child){
          curr = curr->r_child;
        }
        /*Current node is left most*/
        const Iterator it(curr);

        updateLast = false;
        lastNode = curr;
        return it;
      }else{
        const Iterator it(lastNode);
        return it;
      }
    }

    /**************************************************************************/
    Iterator end(){
      Iterator it(0);
      return it;
    }

    /**************************************************************************/
    const Iterator end()const{
      const Iterator it(0);
      return it;
    }

    /**************************************************************************/
    Iterator find(const value_type& t)const{
      Node* f = findNode(t);
      if(f==0){
        return end();
      }

      return Iterator(f);
    }

    /**************************************************************************/
    Iterator findClosestBefore(const value_type& t)const{
      Node* f = findClosestNodeBefore(t);
      if(f==0){
        return end();
      }

      return Iterator(f);
    }

    /**************************************************************************/
    int findClosestIndexBefore(const value_type& t)const{
      Node* f = findClosestNodeBefore(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }

    /**************************************************************************/
    int findClosestIndexAfter(const value_type& t)const{
      Node* f = findClosestNodeAfter(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }

    /**************************************************************************/
    int findIndex(const value_type& t)const{
      Node* f = findNode(t);
      if(f == 0)
        return undefinedIndex;
      else
        return f->index;
    }

    /**************************************************************************/
    void traverse(){
      ltraverse(root);
    }

    /**************************************************************************/
    void clear(){
      if(root){
        destroyNode(root);
        nodeAlloc.destroyAndDeallocate(root);
        root = 0;

        sz = 0;
        beginNode = 0;
        lastNode = 0;
        updateBegin = true;
        updateLast = true;
      }
    }

    /**************************************************************************/
    void consistent(){
      message("Consistency check");
      consistent(root, 0);
      message("End consistency check");
    }

    /**************************************************************************/
    template<class U, class AC>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Tree<U, AC>& t);

  public:
    /**************************************************************************/
    Tree(const self_type& t){
      *this = t;
    }

    /**************************************************************************/
    self_type& operator=(const self_type& t){
      if(this != &t){
        clear();
        Iterator it = t.begin();
        while(it != t.end()){
          T val = it.ptr->data;
          int index = it.ptr->index;
          insert(val, index);
          it++;
        }
      }
      return *this;
    }

  protected:
    /**************************************************************************/
    /*User defined compare functions*/
    bool(*l_less )(const value_type&, const value_type&);
    bool(*l_equal)(const value_type&, const value_type&);

    /**************************************************************************/
    int getDepth(Node* node){
      return node->depth;
    }

    /**************************************************************************/
    void consistent(Node* node, Node* parent){
      if(node != 0){
        tslassert(node->parent == parent);
        if(node->parent != parent){
          error("inconsistent tree");
        }
        consistent(node->l_child, node);
        consistent(node->r_child, node);
      }
    }

    /**************************************************************************/
    /*If cont == false, we can skip the traversal to the
      root. Otherwise, we must continue the traversal.*/
    void updateDepth(Node* node, bool deleting, bool cont = false){
      node->computeDepth();

      if(deleting && node->balance == 0){
        cont = true;
      }

      tslassert(node->balance >= -2 && node->balance <= 2);

      if(!cont){
        /*We can stop the traversal*/
        if(!deleting){
          if(node->l_child != 0 || node->r_child != 0){
            if(node->balance == 0){
              return;
            }
          }
        }else{
          if(node->l_child != 0 && node->r_child != 0){
            if(node->balance == 1 || node->balance == -1){
              return;
            }
          }
        }
      }

      if(node->balance == 2){
        /*Left subtree is deeper*/
        Node* P = node;
        Node* L = node->l_child;

        if(L->balance == -1){
          /*Left-right case*/
          rotL(L);
          rotR(P);
        }else if(L->balance == 1){
          /*Left-left case*/
          rotR(P);
        }else{
          if(deleting){
            rotR(P);
          }else{
            error();
          }
        }
        /*Update parent*/
        updateDepth(P, deleting, cont);
      }else if(node->balance == -2){
        /*Right subtree is deeper*/
        Node* P = node;
        Node* R = node->r_child;

        if(R->balance == -1){
          /*Right-tight case*/
          rotL(P);
        }else if(R->balance == 1){
          /*Left-right case*/
          rotR(R);
          rotL(P);
        }else{
          if(deleting){
            rotL(P);
          }else{
            error();
          }
        }
        /*Update parent*/
        updateDepth(P, deleting, cont);
      }else if(node->parent){
        /*Update parent*/
        updateDepth(node->parent, deleting, cont);
      }
    }

    /**************************************************************************/
    /*AVL rotation functions*/
    Node* rotL(Node* node){
      Node* right  = node->r_child;
      Node* parent = node->parent;
      Node* tmp;
      int dir = 0;
      if(parent){
        if(parent->l_child == node){
          dir = -1;
        }else{
          dir = 1;
        }
      }

      tmp = node->r_child->l_child;
      right->l_child = node;
      node->r_child = tmp;
      if(tmp){
        tmp->parent = node;
      }

      node->parent = right;
      right->parent = parent;

      /*Update parent*/
      if(dir == 0){
        /*Root*/
        root = right;
      }else if(dir == -1){
        parent->l_child = right;
      }else{
        parent->r_child = right;
      }

      /*Update depth/balance info*/
      if(node)node->computeDepth();
      if(right)right->computeDepth();
      if(parent)parent->computeDepth();

      return right;
    }

    /**************************************************************************/
    Node* rotR(Node* node){
      Node* left  = node->l_child;
      Node* parent = node->parent;
      Node* tmp;
      int dir = 0;
      if(parent){
        if(parent->l_child == node){
          dir = -1;
        }else{
          dir = 1;
        }
      }

      tmp = node->l_child->r_child;
      left->r_child = node;
      node->l_child = tmp;
      if(tmp){
        tmp->parent = node;
      }

      node->parent = left;
      left->parent = parent;

      /*Update parent*/
      if(dir == 0){
        /*Root*/
        root = left;
      }else if(dir == -1){
        parent->l_child = left;
      }else{
        parent->r_child = left;
      }

      /*Update depth/balance info*/
      if(node)node->computeDepth();
      if(left)left->computeDepth();
      if(parent)parent->computeDepth();

      return left;
    }

    /**************************************************************************/
    void deleteNode(Node* node){
      if(node == beginNode){
        updateBegin = true;
      }

      if(node->l_child == 0 && node->r_child == 0){
        if(node == root){
          root = 0;
        }else{
          /*Remove this node*/
          Node* parent = node->parent;
          if(parent->l_child == node){
            parent->l_child = 0;
          }
          if(parent->r_child == node){
            parent->r_child = 0;
          }
        }
        nodeAlloc.destroyAndDeallocate(node);

        return;
      }else if(node->l_child == 0 && node->r_child != 0){
        /*Left child is NULL. Replace current node with right child.*/
        Node* parent = node->parent;
        if(parent != 0){
          if(parent->l_child == node){
            parent->l_child = node->r_child;
            node->r_child->parent = parent;
          }else if(parent->r_child == node){
            parent->r_child = node->r_child;
            node->r_child->parent = parent;
          }
        }else{
          /*Node == Root*/
          root = node->r_child;
          root->parent = 0;
        }
        node->r_child = 0;

        nodeAlloc.destroyAndDeallocate(node);

        return;
      }else if(node->l_child != 0 && node->r_child == 0){
        /*Right child is NULL. Replace current node with left child.*/
        Node* parent = node->parent;
        if(parent != 0){
          if(parent->l_child == node){
            parent->l_child = node->l_child;
            node->l_child->parent = parent;
          }else if(parent->r_child == node){
            parent->r_child = node->l_child;
            node->l_child->parent = parent;
          }
        }else{
          /*Node == Root*/
          root = node->l_child;
          root->parent = 0;
        }
        node->l_child = 0;

        order = 1 - order;

        nodeAlloc.destroyAndDeallocate(node);

        return;
      }
    }

    /**************************************************************************/
    void deleteNode2(Node* node){
      Node* parent = node->parent;

      sz--;

      if(node->l_child == 0 && node->r_child == 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else if(node->l_child == 0 && node->r_child != 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else if(node->l_child != 0 && node->r_child == 0){
        deleteNode(node);
        if(parent)updateDepth(parent, true);
      }else{
        tslassert(node->l_child != 0 && node->r_child != 0);
        /*Both childs are not NULL*/
        Node* currentNode=0;

        if(order == 0){
          /*Find current node in-order successor*/
          currentNode = node->r_child;
          while(currentNode->l_child){
            currentNode = currentNode->l_child;
          }

          tslassert(currentNode->l_child == 0);
        }else{
          /*Find current node in-order predecessor*/
          currentNode = node->l_child;
          while(currentNode->r_child){
            currentNode = currentNode->r_child;
          }

          tslassert(currentNode->r_child == 0);
        }

        node->data = currentNode->data;
        node->index = currentNode->index;

        parent = currentNode->parent;

        deleteNode(currentNode);

        if(parent){
          updateDepth(parent, true);
        }
        //return;
      }
    }

    /**************************************************************************/
    Node* findClosestNodeBefore(const value_type& t)const{
      Node* curr = root;

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            Iterator it(curr);
            it.prev();
            return it.ptr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::equal(t, curr->data)){
            Iterator it(curr);
            it.prev();
            return it.ptr;
          }else{
            if(Compare<T>::less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                //it.prev();
                return it.ptr;
              }
            }
          }
        }
      }
      return 0;
    }

    /**************************************************************************/
    Node* findClosestNodeAfter(const value_type& t)const{
      Node* curr = root;

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            Iterator it(curr);
            it.next();
            return it.ptr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::equal(t, curr->data)){
            Iterator it(curr);
            it.next();
            return it.ptr;
          }else{
            if(Compare<T>::less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                Iterator it(curr);
                it.next();
                return it.ptr;
              }
            }
          }
        }
      }
      return 0;
    }

    /**************************************************************************/
    Node* findNode(const value_type& t)const{
      Node* curr = root;

      if(root == 0){
        return 0;
      }

      if(l_equal && l_less){
        while(curr){
          if(l_equal(t, curr->data)){
            return curr;
          }else{
            if(l_less(t, curr->data)){
              if(curr->l_child){
                curr = curr->l_child;
              }else{
                return 0;
              }
            }else{
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                return 0;
              }
            }
          }
        }
      }else{
        while(curr){
          if(Compare<T>::less(t, curr->data)){
            if(curr->l_child){
              curr = curr->l_child;
            }else{
              return 0;
            }
          }else{
            if(Compare<T>::less(curr->data, t)){
              if(curr->r_child){
                curr = curr->r_child;
              }else{
                return 0;
              }
            }else{
              return curr;
            }
          }
        }
      }
      return 0;
    }

    /**************************************************************************/
    void ltraverse(Node* subtree){
      if(subtree == 0){
        return;
      }

      ltraverse(subtree->l_child);
      message("[%d], %d || t = %p, p = %p, l = %p, r = %p, depth = %d, bal = %d",
              subtree->index, subtree->data, subtree,
              subtree->parent, subtree->l_child, subtree->r_child,
              subtree->depth, subtree->balance);
      ltraverse(subtree->r_child);
    }

    void destroyNode(Node* node){
        if(node->l_child){
          destroyNode(node->l_child);
          nodeAlloc.destroyAndDeallocate(node->l_child);
          node->l_child = 0;
        }

        if(node->r_child){
          destroyNode(node->r_child);
          nodeAlloc.destroyAndDeallocate(node->r_child);
          node->r_child = 0;
        }

        node->parent = 0;
    }

    /**************************************************************************/
    Node* root;
    mutable Node* beginNode;
    mutable bool updateBegin;

    mutable Node* lastNode;
    mutable bool updateLast;

#if 0
    Node* index;
    int last_direction;
#endif
    int order;
    int sz;

    NodeAlloc nodeAlloc;
  };

  /**************************************************************************/
  template<class U, class Alloc>
  inline std::ostream& operator<<(std::ostream& os, const Tree<U, Alloc>& t){
    typename Tree<U, Alloc>::Iterator it = t.begin();
    U last = *it;
    bool first = true;

    if(t.size() == 0){
      os << "(Empty)";
      return os;
    }

    while(it != t.end()){
      int index = it.getIndex();
      if(!first){
        if(!Compare<U>::less(last, *it)){
          std::cout << *it << "(" << index << ")" << std::endl;
          error("not ordered");
        }
      }
      int balance = it.ptr->balance;
      last = *it;
      first = false;
      os << *it++ << "(" << index << ", " << balance << ")" << ',';
    }
    return os;
  }
}

#endif/*TREE_HPP*/
