/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SURFACETREE_HPP
#define SURFACETREE_HPP

#include "datastructures/DCEList.hpp"
#include "math/Random.hpp"
#include "math/Vector.hpp"
#include "datastructures/Octree.hpp"
#include "datastructures/SurfaceTreeInternals.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Compare<SurfaceTreeInternals::SurfaceNode<T>*>{
  public:
    /**************************************************************************/
    typedef SurfaceTreeInternals::SurfaceNode<T>* SNP;
    static bool less(const SNP& a, const SNP& b){
      return a < b;
    }

    /**************************************************************************/
    static bool equal(const SNP& a, const SNP& b){
      return a == b;
    }
  };

  /**************************************************************************/
  template<class T>
  inline bool SurfaceNodeCompare<T>::snless(const SNP& a,
                                            const SNP& b){
    return Compare<Vector4<T> >::less(a->box.getMin(), b->box.getMin());
  }

  /**************************************************************************/
  template<class T>
  inline bool SurfaceNodeCompare<T>::snequal(const SNP& a,
                                             const SNP& b){
    return a == b;
    Vector4<T> va = a->box.center();
    Vector4<T> vb = b->box.center();
    if((va - vb).length() < (T)1E-6){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  inline bool SurfaceNodeCompare<T>::snless2(const SNP& a,
                                             const SNP& b){
    return a->area > b->area;
  }

  /**************************************************************************/
  template<class T>
  inline bool SurfaceNodeCompare<T>::snequal2(const SNP& a,
                                              const SNP& b){
    return a == b;
    Vector4<T> va = a->box.center();
    Vector4<T> vb = b->box.center();
    if((va - vb).length() < (T)1E-6){
      return true;
    }
    return false;
  }

  /**************************************************************************/
  /*Specialized getPosition function as used in Octree, operatates on
    nodes in the SurfaceTree*/
  template<>
  inline const Vector4<float> getPosition<SurfaceTreeInternals::SurfaceNode<float>*, float>(SurfaceTreeInternals::SurfaceNode<float>*& a){
    return a->box.center();
  }

  /**************************************************************************/
  template<>
  inline const Vector4<double> getPosition<SurfaceTreeInternals::SurfaceNode<double>*, double>(SurfaceTreeInternals::SurfaceNode<double>*& a){
    return a->box.center();
  }

  /**************************************************************************/
  /*SurfaceTree contains a BVH of a surface mesh*/
  template<class T, class A=char, class B=char, class C=char, class D=char>
  class SurfaceTree{
  public:
    /**************************************************************************/
    typedef SurfaceTreeInternals::SurfaceNode<T> Node;

  private:
    /**************************************************************************/
    typedef typename List<Node*>::Iterator NodePtrListIterator;
    typedef typename Tree<Node*>::Iterator NodePtrTreeIterator;
    typedef Octree<Node*, T>               NodeOctree;
  public:
    /**************************************************************************/
    SurfaceTree(DCTetraMesh<T, A, B, C, D>* m){
      mesh = m;
      leafNodes = root = 0;
      adjacentNodes = 0;
      nAdjacent = 0;
    }

    /**************************************************************************/
    virtual ~SurfaceTree(){
      if(leafNodes){
        delete [] leafNodes;
      }
      if(root){
        delete root;
      }
      if(adjacentNodes){
        delete[] adjacentNodes;
      }
      if(nAdjacent){
        delete[] nAdjacent;
      }
    }

    /**************************************************************************/
    /*Returns the BBox containing all surface elements.*/
    BBox<T> getBBox()const{
      if(mesh->getNHalfFaces() != 0){
        return root->box;
      }else{
        return BBox<T>();
      }
    }

    /**************************************************************************/
    /*@TODO: Update the structure of the tree above the level where
      complete objects are grouped. Due to the movement of the
      objects, the current structure can be inefficient.*/
    void update(){
      if(mesh->getNHalfFaces() != 0){
        mesh->computeDisplacement((Vector<T>*)0x0, (T)0.0);
        root->updateBBox(mesh);
        root->computeVisibility(mesh);
      }
    }

    /**************************************************************************/
    /*Updates the mesh and surface tree given a certain velocity*/
    void update(Vector<T>* velocity, T dt, bool keepCurrentBoxes = false){
      if(mesh->getNHalfFaces() != 0){
        mesh->computeDisplacement(velocity, dt);
        root->updateBBox(mesh, keepCurrentBoxes);
        root->computeVisibility(mesh);
      }
    }

    /**************************************************************************/
    bool projectionsValid(List<int>* invalidFaces){
      if(mesh->getNHalfFaces() != 0){
        return root->projectionsValid(mesh, invalidFaces);
      }else{
        return true;
      }
    }

    /**************************************************************************/
    bool volumesValid(List<int>* invalidFaces){
      if(mesh->getNHalfFaces() != 0){
        return root->volumesValid(mesh, invalidFaces);
      }else{
        return true;
      }
    }

    /**************************************************************************/
    int getDepth()const{
      if(root){
        return root->getDepth();
      }else{
        return 0;
      }
    }

    /**************************************************************************/
    int maxAdjacency()const{
      if(root){
        return root->maxAdjacency();
      }else{
        return 0;
      }
    }

    /**************************************************************************/
    /*Breadth-first collection of nodes*/
    void collectNodesBFS(List<Node*>* list){
      List<Node*> queue;
      if(root == 0){
        return;
      }
      queue.append(root);
      list->append(root);

      while(queue.size() != 0){
        NodePtrListIterator it = queue.begin();
        Node* node = *it;

        if(node->childs[0]){
          if(node->childs[0] == node->childs[1]){
            list->append(node->childs[0]);
            queue.append(node->childs[0]);
          }else{
            list->append(node->childs[0]);
            queue.append(node->childs[0]);
            list->append(node->childs[1]);
            queue.append(node->childs[1]);
          }
        }
        queue.removeFromIterator(it);
      }
    }

    /**************************************************************************/
    void computeNodeIds(){
      List<Node*> list;
      collectNodesBFS(&list);

      NodePtrListIterator it = list.begin();
      int currentId = 0;

      while(it != list.end()){
        Node* node = *it++;
        node->nodeId = currentId++;
      }
    }

    /**************************************************************************/
    void saveTree(std::ofstream& out){
      if(out.is_open() && mesh->getNHalfFaces() != 0){
        /*Compute node ids*/
        computeNodeIds();

        /*Write root id of this tree*/
        if(root){
          out << root->nodeId << std::endl;
        }else{
          out << -1 << std::endl;
        }

        List<Node*> list;
        collectNodesBFS(&list);

        /*Write number of nodes*/
        out << list.size() << std::endl;

        /*Write max adjacency*/
        out << maxAdjacency() << std::endl;

        /*Write nodes*/

        NodePtrListIterator it = list.begin();

        while(it != list.end()){
          Node* currentNode = *it++;

          /*Write face id, if non-leaf node id = -1*/
          if(currentNode->leaf == true){
            out << currentNode->face << "\t";
          }else{
            out << -1 << "\t";
          }

          /*Write parent, in case of no parent, -1*/
          if(currentNode->parent){
            out << currentNode->parent->nodeId << "\t";
          }else{
            out << -1 << "\t";
          }

          /*Write childs, if none-> -1*/
          if(currentNode->childs[0]){
            if(currentNode->childs[0] == currentNode->childs[1]){
              out << currentNode->childs[0]->nodeId << "\t" << -1 << "\t";
            }else{
              out << currentNode->childs[0]->nodeId << "\t"
                  << currentNode->childs[1]->nodeId << "\t";
            }
          }else{
            out << -1 << "\t" << -1 << "\t";
          }

          /*Write visibility*/
          //out << currentNode->visibility << "\t";

          /*Write level*/
          out << currentNode->level << std::endl;

          /*Write bbox min and max*/
          //Vector4f mn = currentNode->box.getMin();
          //Vector4f mx = currentNode->box.getMax();
          //out << mn[0] << "\t" << mn[1] << "\t" << mn[2] << "\t";
          //out << mx[0] << "\t" << mx[1] << "\t" << mx[2] << std::endl;
        }

        it = list.begin();

        /*Write adjacent faces*/
        while(it != list.end()){
          Node* currentNode = *it++;
          out << currentNode->adjacentFaces.size();

          NodePtrTreeIterator adit = currentNode->adjacentFaces.begin();
          Tree<int> sortedAdjacent;

          while(adit != currentNode->adjacentFaces.end()){
            Node* adjNode = *adit++;
            sortedAdjacent.insert(adjNode->nodeId);
          }

          Tree<int>::Iterator sadit = sortedAdjacent.begin();
          while(sadit != sortedAdjacent.end()){
            int nodeId = *sadit++;
            out << "\t" << nodeId;
          }
          out << std::endl;
        }
      }
    }

    /**************************************************************************/
    /*Traces a ray through the BVH and returns the id of the closest
      intersecting surface element in the mesh.*/
    int traceRay(const Ray<T>& ray,
                 Vector4<T>& col,
                 int self,
                 RayTraceMode mode,
                 T* distance = 0,
                 Vector4<T>* bary = 0)const{
      if(mesh->getNHalfFaces() != 0){
        int depth = 0;
        return root->traceRay(ray, col, self, mode, distance, bary, &depth);
      }
      return -1;
    }

    /**************************************************************************/
    /*Finds all intersections of line a-b and the triangles stored
      inside this tree. Line-triangle intersections that are outside
      the segment a-b are not detected.*/
    void traceLine(const Vector4<T>& a, const Vector4<T>& b, Tree<int>* tree){
      if(mesh->getNHalfFaces() != 0){
        root->traceLine(a, b, tree);
      }
    }

    /**************************************************************************/
    /*Returns true if both faces belong to the same object in the tree
      and thus share a common root node.*/
    bool facesShareSameRoot(int faceId1, int faceId2){
      Node* faceNode1 = &leafNodes[faceId1];
      Node* faceNode2 = &leafNodes[faceId2];

      return faceNode1->root == faceNode2->root;
    }

    /**************************************************************************/
    void findPotentialFaceCollisions(int faceId, List<int>& faces){
      if(mesh->getNHalfFaces() == 0){
        return;
      }

      Node* faceNode = &leafNodes[faceId];
      List<int> potentialFaces;

      /*Find collisions between query face and faces of root*/
      //root->findCollisions(faceNode, &potentialFaces);
      //root->findCollisionsR(faceNode, &potentialFaces, adjacentNodes, nAdjacent,
      //                    max_adj);

      root->findCollisionsR(faceNode, mesh, &potentialFaces);

      /*Find self-collisions between query face and its root*/
      //faceNode->root->findSelfCollisions(faceNode, &potentialFaces,
      //                                 adjacentNodes, nAdjacent, max_adj);

      //faceNode->root->findCollisionsR(faceNode, &potentialFaces,
      //                              adjacentNodes, nAdjacent, max_adj);

      if(potentialFaces.size() == 0){
        return;
      }

      int origin = mesh->halfEdges[mesh->halfFaces[faceId].half_edge].origin;

      Vector4<T> f1_origin = mesh->vertices[origin].coord;
      Vector4<T> f1_n = mesh->halfFaces[faceId].normal;

      Mesh::GeometryType type1 = mesh->vertices[origin].type;

      List<int>::Iterator it = potentialFaces.begin();

      while(it != potentialFaces.end()){
        /*Check normals of both faces. If both faces are facing each
          other, keep this combination, otherwise, skip.*/
        int face = *it++;

        Mesh::GeometryType type2;

        if(dot(f1_n, mesh->halfFaces[face].normal) >= 0){
          //continue;
        }

        /*If a vertex of face2 is behind face1, ignore face1*/
        int verticesBehindFace = 0;

        int currentEdge = mesh->halfFaces[face].half_edge;
        mesh->setCurrentEdge(currentEdge);

        for(int i=0;i<3;i++){
          //float sd = f1.getSignedDistance(f2.v[i]);
          //Using a rough approximation is fine here
          int vertex = mesh->getOriginVertex();

          type2 = mesh->vertices[vertex].type;

          T sd = dot(f1_n, mesh->vertices[vertex].coord - f1_origin);

          if(sd <= 0){
            verticesBehindFace++;
          }
          mesh->nextEdge();
        }

        if(verticesBehindFace == 3){
          //continue;
        }

        if(type1 == Mesh::Static && type2 == Mesh::Static){
          /*Do not check two static faces, they can never collide*/
          continue;
        }

        if(type1 == Mesh::Rigid && type2 == Mesh::Rigid){
          /*If they belong to the same rigid object, skip. No
            collision possible.*/
          int object1 = mesh->vertexObjectMap[origin];
          int object2 = mesh->vertexObjectMap[mesh->getOriginVertex()];
          if(object1 == object2){
            continue;
          }
        }

        faces.append(face);
      }
    }

    /**************************************************************************/
    void collectNeighboringFaces(const BBox<T>& query, List<int>& faces){
      if(mesh->getNHalfFaces() != 0){
        root->collectNeighboringFaces(query, faces);
      }
    }

    /**************************************************************************/
    void computeLevels(){
      if(mesh->getNHalfFaces() != 0){
        root->computeLevelsBU();
        root->computeLevelsTD(root->level);
      }
    }

    /**************************************************************************/
    /*An orphan node represents a single closed object not connected
      to other objects in this tree. In order to construct a full
      tree, these nodes should be merged together.*/
    void mergeOrphans(List<Node*>* nodes){
      /*Merge nodes with the largest overlap*/
      message("Merging individual objects");
      message("%d objects", nodes->size());

      NodePtrListIterator it = nodes->begin();

      if(nodes->size() == 1){
        root = *it;
        root->setRoot(root);
        return;
      }else{
        /*Set root information*/
        it = nodes->begin();

        while(it != nodes->end()){
          Node* objectRoot = *it++;
          objectRoot->setRoot(objectRoot);
        }

        it = nodes->begin();

        Tree<Node*>* t1 =
          new Tree<Node*>(&SurfaceNodeCompare<T>::snless,
                          &SurfaceNodeCompare<T>::snequal);
        Tree<Node*>* t2 =
          new Tree<Node*>(&SurfaceNodeCompare<T>::snless,
                          &SurfaceNodeCompare<T>::snequal);

        NodeOctree* octree = new NodeOctree();

        /*Fill octree*/
        while(it != nodes->end()){
          Node* n = *it++;
          octree->insert(n);
          t1->insert(n, 1);
        }
        octree->split(9999);

        int pass = 0;
        while(true){
          message("pass = %d\n\n\n", pass++);

          message("t1 size = %d", t1->size());

          if(t1->size() == 1){
            break;
          }
          NodePtrTreeIterator it2 = t1->begin();
          while(it2 != t1->end()){
            Node* node = *it2;

            List<Node*> neighborhood;

            Vector4<T> pos = node->box.center();
            Vector4<T> mn  = node->box.getMin();
            Vector4<T> mx  = node->box.getMax();

            T radius = (T)0.5*(mn-mx).length();

            std::cout << pos;
            std::cout << mn;
            std::cout << mx;

            PRINT(node->box);

            PRINT(radius);
            //message("node = %p, radius = %10.10e", radius);
            //std::cout << pos;

            while(neighborhood.size() == 0){
              octree->getNeighboringElements(pos, (T)radius, neighborhood);
              message("neighborhood.size = %d, radius = %10.10e",
                      neighborhood.size(), radius);
              if(neighborhood.size() == 1){
                neighborhood.clear();
                /*Only result is the query node*/
              }
              radius *= 1.5f;
            }

            NodePtrListIterator it3 = neighborhood.begin();
            Node* bestCandidate = 0;
            T volume = 100000000;
            message("n_neighbors = %d", neighborhood.size());

#if 1

            //float dist = (box.center() - node2->box.center()).length();

            Vector4<T> mn3 = node->box.getMin();
            Vector4<T> mx3 = node->box.getMax();
            Vector4<T> dd3 = mn3 - mx3;

            T origVol = Abs(dd3[0] * dd3[1] * dd3[2]);
#endif

            /*Minimize volume of new node*/
            while(it3 != neighborhood.end()){
              Node* node2 = *it3++;
              if(node2 == node){
                message("skip, node = node2");
                continue;
              }

              BBox<T> box = node->box;

              box.addPoint(node2->box.getMin());
              box.addPoint(node2->box.getMax());

              Vector4<T> mn2 = box.getMin();
              Vector4<T> mx2 = box.getMax();
              Vector4<T> dd = mn2 - mx2;
              T newVol = Abs(dd[0] * dd[1] * dd[2]);

              T factor = newVol/origVol;

              message("origVol = %10.10e, newVol = %10.10e", origVol, newVol);
              message("factor = %10.10e, oldFactor = %10.10e",
                      factor, volume);

              //factor*=dist;

              //factor = newVol;

              if(newVol <= volume){
                volume = newVol;
                bestCandidate = node2;
              }
            }

            tslassert(bestCandidate != 0);
            tslassert(bestCandidate != node);

            Node* parentNode = new Node();
            parentNode->parent = 0;
            parentNode->childs[0] = node;
            parentNode->childs[1] = bestCandidate;
            parentNode->area = 0;
            parentNode->box = node->box;
            parentNode->box.addPoint(bestCandidate->box.getMin());
            parentNode->box.addPoint(bestCandidate->box.getMax());
            parentNode->face = 0;
            parentNode->leaf = false;
            parentNode->level = Max(node->level,
                                    bestCandidate->level)+1;

            if(node == bestCandidate){
              parentNode->singleNode = false;
            }else{
              parentNode->singleNode = true;
            }

            parentNode->minFace =
              Min(node->minFace, bestCandidate->minFace);
            parentNode->maxFace =
              Max(node->maxFace, bestCandidate->maxFace);

            octree->remove(node);
            octree->remove(bestCandidate);

            t2->insert(parentNode, 1);
            t1->remove(it2);

            t1->remove(bestCandidate);
            it2 = t1->begin();
            message("size t1 after removal = %d", t1->size());
            message("size t2 after removal = %d", t2->size());
            message("oct size = %d", octree->size());

            if(t1->size() == 1){
              t2->insert(*it2);

              octree->remove(*it2);
              t1->remove(it2);
            }
          }
          swap(&t1, &t2);
          delete octree;
          octree = new NodeOctree();

          NodePtrTreeIterator it4 = t1->begin();
          int count = 0;
          while(it4 != t1->end()){
            Node* node = *it4++;
            octree->insert(node);
            count++;
          }

          octree->split(999);
        }

        root = *(t1->begin());
        delete octree;
        delete t1;
        delete t2;
        return;
      }
    }

    /**************************************************************************/
    int getCommonVisibility(Node* a, Node* b){
      int result = a->visibility & b->visibility;
      /*Not efficient, use popcount.*/
      int count = 0;
      for(int i=0;i<27;i++){
        int mask = 1<<i;
        if(result&mask){
          count++;
        }
      }
      return count;
    }

    /**************************************************************************/
    void debugPair(int face1, int face2){
      Node* node1 = &leafNodes[face1];
      Node* node2 = &leafNodes[face2];
      Node* commonRoot;

      List<Node*> path1;
      List<Node*> path2;

      /*Find common node*/
      {
        Node* root1 = node1;
        Node* root2 = node2;

        while(root1 != root2){
          if(root1->level == root2->level){
            path1.append(root1);
            path2.append(root2);

            root1 = root1->parent;
            root2 = root2->parent;

          }else if(root1->level > root2->level){
            root2 = root2->parent;
            path2.append(root2);
          }else{
            root1 = root1->parent;
            path1.append(root1);
          }
        }

        commonRoot = root1;
      }


      List<int> faces1;
      List<int> faces2;

      warning("test %d, %d", face1, face2);
      commonRoot->findDebugCollisions(node1, &faces1);
      warning("test %d, %d", face2, face1);
      commonRoot->findDebugCollisions(node2, &faces2);

      List<int>::Iterator it1 = faces1.begin();
      while(it1 != faces1.end()){
        warning("faces %d, %d", face1, *it1++);
      }

      List<int>::Iterator it2 = faces2.begin();
      while(it2 != faces2.end()){
        warning("faces %d, %d", face2, *it2++);
      }

    }

    /**************************************************************************/
    void constructTree(){
      PRINT_FUNCTION;
      if(mesh->getNHalfFaces() == 0){
        /*No mesh, nothing to construct*/
        return;
      }

      Tree<Node*>* faceTree1 = new Tree<Node*>();
      Tree<Node*>* faceTree2 = new Tree<Node*>();

      Vector4<T> eps((T)1e-6, (T)1e-6, (T)1e-6, 0);

      /*List for orphaned nodes, which are individual objects*/
      List<Node*> orphanNodes;

      /*Create leaf nodes. Each leafnode represents one face element*/
      leafNodes = new Node[mesh->getNHalfFaces()];

      message("V = %d, E = %d, F = %d", mesh->getNVertices(),
              mesh->getNHalfEdges(),
              mesh->getNHalfFaces());

      for(int i=0;i<mesh->getNHalfFaces();i++){
        /*Add current face node to search tree*/
        faceTree1->insertc(&(leafNodes[i]), i);

        leafNodes[i].face = i;
        Triangle<T> t;
        mesh->getTriangle(&t, i, false);
        leafNodes[i].averageNormal = t.n;
        leafNodes[i].area = t.getArea();
        leafNodes[i].leaf = true;
        leafNodes[i].id   = i;
        leafNodes[i].level = 0;
        leafNodes[i].type = FACE;
        leafNodes[i].tri = t;
        leafNodes[i].minFace = i;
        leafNodes[i].maxFace = i;
        leafNodes[i].singleNode = false;

        tslassert(leafNodes[i].childs[0] == 0);
        tslassert(leafNodes[i].childs[1] == 0);
      }

      message("Leafnodes created, construct tree");

      for(int i=0;i<mesh->getNHalfFaces();i++){
        /*Fill neighbor information*/
        int edge = mesh->halfFaces[i].half_edge;
        mesh->setCurrentEdge(edge);

        while(true){
          int twinFace = mesh->getTwinFace();
          int currentEdge = mesh->getCurrentEdge();

          /*Twin-face is a direct neighbor of face i*/
          Node* nd = &(leafNodes[twinFace]);
          leafNodes[i].neighboringFaces.uniqueInsert(nd, twinFace);

          /*Add current vertex to bounding box*/
          leafNodes[i].box.addPoint(mesh->vertices[mesh->getOriginVertex()].coord);
          leafNodes[i].interiorEdges.uniqueInsert(currentEdge, currentEdge);

          /*Collect all faces adjacent to originVertex of current
            edge*/

          List<int> adjacent;
          mesh->getSurroundingFacesOfVertex(mesh->vertices[mesh->getOriginVertex()], adjacent);

          List<int>::Iterator adjit = adjacent.begin();
          while(adjit != adjacent.end()){
            int adjFace = *adjit++;
            if(adjFace != i){
              Node* node = &(leafNodes[adjFace]);
              leafNodes[i].adjacentFaces.uniqueInsert(node, adjFace);
            }
          }
          mesh->setCurrentEdge(currentEdge);
          mesh->nextEdge();
          if(mesh->getCurrentEdge() == edge){
            break;
          }
        }
        leafNodes[i].computePerimeter(mesh);
        leafNodes[i].computeVisibility(mesh, false);
      }

      /*In the next level we have to merge sub-surfaces by searching
        all adjacent sub-surface. These are found by collecting all
        unique parents of surrounding nodes.

        Select a node from the temporary tree, find a surrounding
        sub-surface which has the smallest normal deviation. Create a
        new parent node, store in temporary tree, and remove the
        merged nodes from the temporary tree.*/
      int depth = 0;

      message("Construct tree");

      while(faceTree1->size() != 0){
        /*Remove non-mergable nodes*/
        NodePtrTreeIterator it = faceTree1->begin();

        it = faceTree1->begin();
        Node* first = *it++;

        /*Inspect neighboring sub-surfaces*/
        if(first->neighboringFaces.size() == 0){
          error("No neighbours left");
        }

        /*Select the best node to merge with this node*/
        NodePtrTreeIterator sit = first->neighboringFaces.begin();
        T dotp = (T)-1000;
        T smallest = (T)100000;
        //int minNeighbors = 10000;
        T smallestRatio = (T)10000;
        Node* bestCandidate = 0;
        Node* smallestCandidate = 0;
        Node* bestCommonCandidate = 0;
        T bestFactor = (T)-10000;

        Vector4<T> fmn = first->box.getMin();
        Vector4<T> fmx = first->box.getMax();
        fmx -= fmn;
        fmx += eps;

        T fvolume = (T)(fmx[0] * fmx[1] * fmx[2]);

        int bestInCommon = 0;
        int smallestInCommon = 0;
        T bestCommonFactor = -10000;

        while(sit != first->neighboringFaces.end()){
          Node* current = *sit++;
          T current_dot = dot(first->averageNormal,
                              current->averageNormal);
          T current_area = current->area + first->area;
          Vector4<T> mn = current->box.getMin();
          Vector4<T> mx = current->box.getMax();

          mx -= mn;
          mx += eps;

          BBox<T> result = current->box;
          result.addPoint(mn);
          result.addPoint(mx);

          Vector4<T> rmn = result.getMin();
          Vector4<T> rmx = result.getMax();
          rmx -= rmn;
          rmx += eps;


          T rvolume = (T)(rmx[0] * rmx[1] * rmx[2]);
          T  volume = (T)( mx[0] *  mx[1] *  mx[2]);

          T ratio =
            volume*(rvolume*current->area-fvolume*first->area)/current_dot;

          bool inSet = false;
          /*Neighboring must exist in faceTree1*/
          if(faceTree1->findIndex(current) == Tree<int>::undefinedIndex){
            inSet = false;
          }else{
            inSet = true;
          }

          if((current_dot > dotp) && inSet){
            dotp = current_dot;
            bestCandidate = current;
          }

          if((current_area < smallest) && inSet){
            smallest = current_area;
          }
          if((ratio < smallestRatio) && inSet){
            smallestRatio = ratio;
          }

          if(inSet){
            T currentFactor = Sqrt(first->area+current->area);
            T overlap = first->computeOverlap(current, mesh);
            T resPerimeter =
              (first->perimeter + current->perimeter) - (T)2.0*overlap;
            currentFactor/=resPerimeter;

            /*Check for vectors which are in common*/
            int common = getCommonVisibility(first, current);
            if(common != 0){
              if(common == bestInCommon){
                if(currentFactor > bestCommonFactor){
                  bestCommonFactor = currentFactor;
                  bestCommonCandidate = current;
                }
              }else if(common > bestInCommon){
                bestInCommon = common;
                bestCommonCandidate = current;
              }
            }

            if(currentFactor > bestFactor){
              bestFactor = currentFactor;
              smallestCandidate = current;
              smallestInCommon = common;
            }
          }
        }

        if(smallestCandidate == 0){

        }else{
          bestCandidate = smallestCandidate;
        }

        /*If there is a candidate with common visibility vectors, use
          that candidate*/
        if(bestCommonCandidate){
          if(smallestInCommon <= bestInCommon){
            bestCandidate = smallestCandidate;
          }else{
            bestCandidate = bestCommonCandidate;
          }
        }

        if(bestCandidate == 0){
          if(smallestCandidate){
            bestCandidate = smallestCandidate;
          }else if(bestCommonCandidate){
            bestCandidate = bestCommonCandidate;
          }
        }

        if(bestCandidate == 0){
          /*Move up one level*/
          /*Create dummy surface node*/
          Node* parentNode = new Node();
          parentNode->parent = 0;
          parentNode->childs[0] = first;
          parentNode->childs[1] = first;
          parentNode->area = first->area;
          parentNode->averageNormal = first->averageNormal;
          parentNode->box = first->box;
          parentNode->face = 0;
          parentNode->leaf = false;
          parentNode->level = depth+1;
          parentNode->id = 100;
          parentNode->addEdges(first->interiorEdges, mesh);
          parentNode->computePerimeter(mesh);
          parentNode->computeVisibility(mesh, false);
          parentNode->minFace = first->minFace;
          parentNode->maxFace = first->maxFace;
          parentNode->singleNode = true;

          first->parent = parentNode;
          faceTree1->remove(first);
          faceTree2->insert(parentNode);
        }else{
          Node* parentNode = new Node();
          parentNode->parent = 0;
          parentNode->childs[0] = first;
          parentNode->childs[1] = bestCandidate;
          parentNode->area = first->area + bestCandidate->area;

          parentNode->averageNormal =
            (first->averageNormal * first->area +
             bestCandidate->averageNormal * bestCandidate->area);

          if(parentNode->averageNormal.length() < 1e-6){
            /*If we can not normalize this vector, just put some dummy
              values*/
            parentNode->averageNormal.set(1,0,0,0);
          }else{
            parentNode->averageNormal.normalize();
          }

          parentNode->box = first->box;
          parentNode->box.addPoint(bestCandidate->box.getMin());
          parentNode->box.addPoint(bestCandidate->box.getMax());
          parentNode->face = 0;
          parentNode->leaf = false;
          parentNode->level = depth+1;
          parentNode->singleNode = false;

          parentNode->addEdges(first->interiorEdges, mesh);
          parentNode->addEdges(bestCandidate->interiorEdges, mesh);
          parentNode->computePerimeter(mesh);
          parentNode->computeVisibility(mesh, false);
          parentNode->minFace =
            Min(first->minFace, bestCandidate->minFace);
          parentNode->maxFace =
            Max(first->maxFace, bestCandidate->maxFace);

          first->parent = parentNode;
          bestCandidate->parent = parentNode;

          faceTree1->remove(first);
          faceTree1->remove(bestCandidate);
          faceTree2->insert(parentNode);
        }

        /*All nodes are merged in higher-level nodes. Update
          neighboring and adjacency information*/

        if(faceTree1->size() == 0){
          faceTree1->clear();

          /*FaceTree1 is empty now, compute neighboring sub-surfaces */
          NodePtrTreeIterator it = faceTree2->begin();

          /*Iterate over all parent nodes*/
          while(it!=faceTree2->end()){
            Node* current = *it;

            /*Collect from both childs the parents of the
              neighboring faces*/
            Tree<Node*> parentTree;
            Tree<Node*> parentAdjTree;

            for(int l=0;l<2;l++){
              Node* child = current->childs[l];
              NodePtrTreeIterator nit = child->neighboringFaces.begin();

              while(nit != child->neighboringFaces.end()){
                Node* currNeighbor = *nit++;

                if(currNeighbor->parent == current){
                  /*In order to prevent that in the next iteration
                    this node will be merged with itself.*/
                }else{
                  if(currNeighbor->parent == 0){
                    error("orphan node");
                  }

                  parentTree.uniqueInsert(currNeighbor->parent);
                }
              }

              nit = child->adjacentFaces.begin();
              while(nit != child->adjacentFaces.end()){
                Node* currNeighbor = *nit++;

                if(currNeighbor->parent == current){

                }else{
                  if(currNeighbor->parent == 0){
                    error("orphan node");
                  }

                  parentAdjTree.uniqueInsert(currNeighbor->parent);
                }
              }
            }

            NodePtrTreeIterator pit = parentTree.begin();

            while(pit != parentTree.end()){
              Node* neigh = *pit++;
              current->neighboringFaces.uniqueInsert(neigh, neigh->face);
            }

            pit = parentAdjTree.begin();
            while(pit != parentAdjTree.end()){
              Node* adj = *pit++;
              current->adjacentFaces.uniqueInsert(adj, adj->face);
            }

            if(current->neighboringFaces.size() == 0){
              orphanNodes.append(current);
              faceTree2->remove(it);
            }else{
              it++;
            }
          }

          /*Swap trees*/
          swap(&faceTree1, &faceTree2);

          depth++;
          if(faceTree1->size() == 1){
            root = *(faceTree1->begin());
            break;
          }
        }
      }
      delete faceTree1;
      delete faceTree2;

      if(orphanNodes.size() != 0){
        /*Merge orphan nodes. Each orphan node is one single object,
          not connected to others.*/
        mergeOrphans(&orphanNodes);
      }

      root->updateBBox(mesh);
      root->computeVisibility(mesh);
      computeLevels();
      compileAdjacency();
    }

    /**************************************************************************/
    /*Store adjacency info in a 2d array*/
    void compileAdjacency(){
      List<Node*> nodes;

      computeNodeIds();

      collectNodesBFS(&nodes);

      max_adj = maxAdjacency();

      if(adjacentNodes){
        delete[] adjacentNodes;
      }
      if(nAdjacent){
        delete[] nAdjacent;
      }

      adjacentNodes = new int[nodes.size() * max_adj];
      nAdjacent     = new int[nodes.size() * max_adj];

      NodePtrListIterator it = nodes.begin();
      while(it != nodes.end()){
        Node* node = *it++;

        int n_nodes = 0;
        NodePtrTreeIterator surface = node->adjacentFaces.begin();

        Tree<int> nodeIds;

        while(surface != node->adjacentFaces.end()){
          Node* neighbor = *surface++;
          nodeIds.insert(neighbor->nodeId);
          n_nodes++;
        }
        nAdjacent[node->nodeId] = n_nodes;

        Tree<int>::Iterator nodeIt = nodeIds.begin();

        int idx = 0;
        while(nodeIt != nodeIds.end()){
          int id = *nodeIt++;
          adjacentNodes[node->nodeId * max_adj + idx] = id;
          idx++;
        }
      }
    }

    /**************************************************************************/
    DCTetraMesh<T, A, B, C, D>* mesh;
    Node* root;
    Node* leafNodes;
    int* adjacentNodes;
    int* nAdjacent;
    int max_adj;
  };
}


#endif/*SURFACETREE_HPP*/
