/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DYNAMICARRAY_HPP
#define DYNAMICARRAY_HPP

#include "core/tsldefs.hpp"
#include "math/Math.hpp"
#include "datastructures/Tree.hpp"

#include <fstream>

namespace tsl{
  /**************************************************************************/
  /*A DynamicArray contains a number of acive and inactive elements,
    where all active elements are located on the first part of the
    array, all inactive elements are located on the end of the
    array.*/
  template<class T>
  class DynamicArray{
  public:
    /**************************************************************************/
    static const int undefinedIndex = -1;

    /**************************************************************************/
    DynamicArray(int _size = 1000, bool _heap = false):size(_size),
                                                       n_active(0),
                                                       data(0),
                                                       index(0),
                                                       reverse_index(0),
                                                       reverse_inactive(0),
                                                       heap_size(0),
                                                       heap(_heap),
                                                       sorted(true),
                                                       sorted_size(0),
                                                       swap_count(0){
      tslassert(size > 0);

      index = new int[size];
      tslassert(index != 0);

      reverse_index = new int[size];
      tslassert(reverse_index != 0);

      reverse_inactive = new int[size];
      tslassert(reverse_inactive != 0);

      data  = new T[size];
      tslassert(data != 0);
      //message("pointer data = %p, points to %p", (void*)&data, (void*)data);
      //message("array size = %d bytes", (sizeof(int) + sizeof(int) + sizeof(int) + sizeof(T)) * size);
      //message("size = %d, n_active = %d", size, n_active);

      for(int i=0;i<size;i++){
        index[i] = i;
        reverse_index[i] = undefinedIndex;
        reverse_inactive[i] = i;
      }
    }

    /**************************************************************************/
    DynamicArray(const DynamicArray<T>& d){
      index = 0;
      reverse_index = 0;
      reverse_inactive = 0;
      data = 0;

      *this = d;
    }

    /**************************************************************************/
    DynamicArray& operator=(const DynamicArray<T>& d){
      if(this == &d){
        return *this;
      }

      /*Delete old arrays*/
      if(index){
        delete [] index;
      }
      if(reverse_index){
        delete [] reverse_index;
      }
      if(reverse_inactive){
        delete [] reverse_inactive;
      }
      if(data){
        delete [] data;
      }

      size        = d.size;
      n_active    = d.n_active;
      heap_size   = d.heap_size;
      heap        = d.heap;
      sorted      = d.sorted;
      sorted_size = d.sorted_size;
      swap_count  = d.swap_count;

      index = new int[size];
      tslassert(index != 0);
      reverse_index = new int[size];
      tslassert(reverse_index != 0);
      reverse_inactive = new int[size];
      tslassert(reverse_inactive != 0);
      data = new T[size];
      tslassert(data != 0);

#if 1
      for(int i=0;i<size;i++){
        index[i]            = d.index[i];
        reverse_index[i]    = d.reverse_index[i];
        reverse_inactive[i] = d.reverse_inactive[i];
        data[i]             = d.data[i];
      }
#else
      memcpy(index, d.index, sizeof(int)*size);
      memcpy(reverse_index, d.reverse_index, sizeof(int)*size);
      memcpy(reverse_inactive, d.reverse_inactive, sizeof(int)*size);
      memcpy(data, d.data, sizeof(T)*size);
#endif
      return *this;
    }

    /**************************************************************************/
    virtual ~DynamicArray(){
      delete [] index;
      delete [] reverse_index;
      delete [] reverse_inactive;
      delete [] data;
    }

    /**************************************************************************/
    void print()const{
      return;
      message("this = %p, index = %p, reverse_index = %p, data = %p",
              this, index, reverse_index, data);
      for(int i=0;i<size;i++){
        message("index[%d] = %d, r_index[%d] = %d, rinactive[%d] = %d",
                i, index[i], i, reverse_index[i], i, reverse_inactive[i]);
      }
    }

    /**************************************************************************/
    void readFromFile(std::ifstream& file){
      /*Read size from file*/
      file.read((char*)&size, sizeof(int));
      /*Read n_active from file*/
      file.read((char*)&n_active, sizeof(int));
      /*Read heap_size from file*/
      file.read((char*)&heap_size, sizeof(int));
      /*Read heap from file*/
      file.read((char*)&heap, sizeof(bool));
      /*Read sorted from file*/
      file.read((char*)&sorted, sizeof(bool));
      /*Read heap_size from file*/
      file.read((char*)&sorted_size, sizeof(int));
      /*Read swap_count from file*/
      file.read((char*)&swap_count, sizeof(int));

      /*Free data*/
      if(index){
        delete[] index;
      }
      if(reverse_index){
        delete[] reverse_index;
      }
      if(reverse_inactive){
        delete[] reverse_inactive;
      }
      if(data){
        delete[] data;
      }

      tslassert(size > 0);
      /*Allocate data*/
      //message("size = %d", size);
      index = new int[size];
      tslassert(index != 0);
      reverse_index = new int[size];
      tslassert(reverse_index != 0);
      reverse_inactive = new int[size];
      tslassert(reverse_inactive != 0);
      data = new T[size];
      tslassert(data != 0);

      /*Read index from file*/
      file.read((char*)index, (int)sizeof(int)*size);
      /*Read reverse_index from file*/
      file.read((char*)reverse_index, (int)sizeof(int)*size);
      /*Read reverse_inactive from file*/
      file.read((char*)reverse_inactive, (int)sizeof(int)*size);
      /*Read data from file*/
      file.read((char*)data, (int)sizeof(T)*size);
    }

    /**************************************************************************/
    void saveToFile(std::ofstream& file)const{
      /*Write size to file*/
      file.write((char*)&size, sizeof(int));
      /*Write n_active to file*/
      file.write((char*)&n_active, sizeof(int));
      /*Write heap_size to file*/
      file.write((char*)&heap_size, sizeof(int));
      /*Write heap to file*/
      file.write((char*)&heap, sizeof(bool));
      /*Write sorted to file*/
      file.write((char*)&sorted, sizeof(bool));
      /*Write heap_size to file*/
      file.write((char*)&sorted_size, sizeof(int));
      /*Write swap_count to file*/
      file.write((char*)&swap_count, sizeof(int));

      /*Write index to file*/
      file.write((char*)index, (int)sizeof(int)*size);
      /*Write reverse_index to file*/
      file.write((char*)reverse_index, (int)sizeof(int)*size);
      /*Write reverse_inactive to file*/
      file.write((char*)reverse_inactive, (int)sizeof(int)*size);
      /*Write data to file*/
      file.write((char*)data, (int)sizeof(T)*size);
    }

    /**************************************************************************/
    void reset(){
      error();
      for(int i=0;i<size;i++){
        index[i] = i;
        reverse_index[i] = undefinedIndex;
        reverse_inactive[i] = i;
      }
      n_active = 0;
      sorted = true;
      heap_size = 0;
    }

    /**************************************************************************/
    const T& item(int i) const{
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      return data[i];
    }

    /**************************************************************************/
    T& item(int i){
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      return data[i];
    }

    /**************************************************************************/
    const T& operator[](int i) const{
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      return data[i];
    }

    /**************************************************************************/
    T& operator[](int i){
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      return data[i];
    }

    /**************************************************************************/
    const T& activeItem(int i, int* idx = 0) const{
      tslassert(i < size);
      tslassert(i>=0 && i < n_active);
      if(idx){
        *idx = index[i];
      }
      tslassert(index[i] < size);
      return data[index[i]];
    }

    /**************************************************************************/
    T& activeItem(int i, int* idx = 0){
      tslassert(i>=0 && i < n_active);
      tslassert(n_active <= size);
      if(idx){
        *idx = index[i];
      }
      return data[index[i]];
    }

    /**************************************************************************/
    int activeIndex(int i)const{
      tslassert(i>=0 && i < n_active);
      tslassert(n_active <= size);
      return index[i];
    }

    /**************************************************************************/
    const T& reverseItem(int i) const{
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      int idx = reverse_index[i];
      tslassert(idx >= 0 && idx < size);
      return data[idx];
    }

    /**************************************************************************/
    int reverseIndex(int i)const{
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      return reverse_index[i];
    }

    /**************************************************************************/
    T& reverseItem(int i){
      tslassert(i>=0 && i < size);
      tslassert(n_active <= size);
      int idx = reverse_index[i];
      tslassert(idx >= 0 && idx < size);
      return data[idx];
    }

    /**************************************************************************/
    int getFirstFreeItem(int idx){
      if(size == n_active + idx){
        extendStorage();
      }
      tslassert(n_active+idx <= size);
      return index[n_active + idx];
    }

    /**************************************************************************/
    /*Moves item i to end of first part*/
    void activate(int i){
      tslassert(n_active <= size);
      if(reverse_index[i] == undefinedIndex){
        int swp = index[n_active];
        index[n_active] = i;
        index[reverse_inactive[i]] = swp;

        int swp2 = reverse_inactive[i];
        reverse_inactive[i] = n_active;
        reverse_inactive[swp] = swp2;

        reverse_index[i] = n_active;

        n_active++;
      }else{
        error("Activating active element");
      }
    }

    /**************************************************************************/
    /*Moves item i to begin of second part*/
    void deactivate(int i){
      tslassert(n_active <= size);
      if(reverse_index[i] != undefinedIndex){
        n_active--;

        int rr1 = reverse_inactive[i];
        int rr2 = reverse_inactive[index[n_active]];

        reverse_inactive[i] = rr2;
        reverse_inactive[index[n_active]] = rr1;

        int rr = reverse_index[i];
        tslassert(rr <= n_active);

        int swp = index[rr];
        index[rr] = index[n_active];
        index[n_active] = swp;

        reverse_index[index[rr]] = rr;
        reverse_index[i] = undefinedIndex;
      }else{
        error("Deactivating inactive element");
      }
    }

    /**************************************************************************/
    bool isActive(int i) const{
      tslassert(i>=0 && i<size);
      tslassert(n_active <= size);
      return reverse_index[i] != undefinedIndex;
    }

    /**************************************************************************/
    int getNActive() const{
      tslassert(n_active <= size);
      return n_active;
    }

    /**************************************************************************/
    int getSize() const{
      tslassert(n_active <= size);
      return size;
    }

#if 0
    /**************************************************************************/
    void checkHeap(){
      for(int i=0;i<heap_size;i++){
        int indexl = 2*(i)+1;
        int indexr = 2*(i)+2;

        //message("%d, %d, %d", i, indexl, indexr);
        std::cout << data[index[i]] << std::endl;

        if(indexl<heap_size){
          std::cout << data[index[indexl]] << std::endl;
          tslassert(data[index[indexl]] <= data[index[i]]);
        }
        if(indexr<heap_size){
          std::cout << data[index[indexr]] << std::endl;
          tslassert(data[index[indexr]] <= data[index[i]]);
        }
      }
    }
#endif

    /**************************************************************************/
    int binarysearch(const T& t, int l, int r)const{
      int m = (l+r) / 2;
      if(l > r){
        return undefinedIndex;
      }
      if(data[index[m]] == t){
        return m;
      }
      if(l == r){
        return undefinedIndex;
      }
      if(t < data[index[m]]){
        return binarysearch(t, l, m-1);
      }else{
        return binarysearch(t, m+1, r);
      }
    }

    /**************************************************************************/
    int findIndex(const T& t) const{
      /*First search last added values, linear search.*/
      /*Since we assume that between 2 sort operations a few items are
        added that are close to t, this is faster than completely
        sorting the array and performing a binary search.*/
      int found_index;

      if(sorted_size != n_active){
        //message("Linear search, %d, %d", sorted_size, n_active);
        for(int i=sorted_size;i<n_active;i++){
          if(data[index[i]] == t){
            found_index =  reverse_index[i];
            if(found_index != undefinedIndex){
              return index[found_index];
            }else{
              return undefinedIndex;
            }
          }
        }
      }
      //message("Binary search");
      /*Perform a binary search on the sorted part of the array.*/
      found_index = binarysearch(t, 0, sorted_size -1);
      if(found_index != undefinedIndex){
        return index[found_index];
      }else{
        return undefinedIndex;
      }
    }

    /**************************************************************************/
    /*Moves all active elements to the beginning of the data array,
      the mapping is stored in mapping for updateing other data
      structures. After that, all arrays are reduced in size.*/
    bool compress(Tree<int>& mapping, bool force=false){
      int new_size = (int)Pow(2.0f,Ceil(Log((float)n_active)/Log(2.0f)));

      if(!force){
        if(new_size >= size){
          message("skip compression");
          return false;
        }
      }

      message("size = %d, n_active = %d, new_size = %d",
              size, n_active, new_size);

      int* new_index = new int[new_size];
      tslassert(new_index != 0);

      int* new_reverse_index = new int[new_size];
      tslassert(new_reverse_index != 0);

      int* new_reverse_inactive = new int[new_size];
      tslassert(new_reverse_inactive != 0);

      T* new_data  = new T[new_size];
      tslassert(new_data != 0);
      //message("pointer data = %p, points to %p", (void*)&new_data, (void*)new_data);
      //message("array size = %d bytes", (sizeof(int) + sizeof(int) + sizeof(int) + sizeof(T)) * new_size);
      //message("new_size = %d, n_active = %d", new_size, n_active);

      int n_new_active = 0;
      /*Reset structures*/
      for(int i=0;i<new_size;i++){
        new_index[i] = i;
        new_reverse_index[i] = undefinedIndex;
        new_reverse_inactive[i] = i;
      }

      for(int i=0;i<n_active;i++){
        int old_index = activeIndex(i);

        tslassert(mapping.findIndex(old_index) == undefinedIndex);

        mapping.insert(old_index, i);
        tslassert(i <= new_size);

        new_data[i] = data[old_index];

        /*Activate new item*/
#if 0
        int swp = new_index[n_new_active];
        new_index[n_new_active] = i;
        new_index[new_reverse_inactive[i]] = swp;

        int swp2 = new_reverse_inactive[i];
        new_reverse_inactive[i] = n_new_active;
        new_reverse_inactive[swp] = swp2;

        new_reverse_index[i] = n_new_active;
#else
        new_reverse_index[i] = n_new_active;
#endif

        n_new_active++;
      }

      /*Delete old data*/
      delete[] index;
      delete[] reverse_index;
      delete[] reverse_inactive;
      delete[] data;

      index = new_index;
      reverse_index = new_reverse_index;
      reverse_inactive = new_reverse_inactive;
      data = new_data;

      tslassert(n_active == n_new_active);

      size = new_size;

      return true;
    }

    /**************************************************************************/
    void sort(){
      //message("sort");
      swap_count = 0;
      if(heap_size == 0 && sorted_size == 0){
    /*No heap and no ordered data avaliable yet. Construct a heap.*/
    //message("No heap, not sorted.");
        for(int i=0;i<n_active;i++){
          heap_size++;
          fixup(i);
        }
        /*Heap restored.*/
      }else if(heap_size == 0 && sorted_size != 0){
        if(sorted_size == n_active){
          //message("Is completely sorted");
          /*Array is sorted, skip*/
          return;
        }else{
          //message("Array partially sorted, restore heap property.");
          /*Array is partially sorted, restore heap property by
            reversing array.*/

          for(int i=0;i<sorted_size/2;i++){
            swapIndices(i, sorted_size-1-i);
          }
          //message("end reverse");
          sorted = false;
          heap_size = sorted_size;
          sorted_size = 0;
          /*Heap restored for ordered data.*/

          /*Restore heap for last part*/
          for(int i=heap_size;i<n_active;i++){
            heap_size++;
            fixup(i);
          }

          /*Heap restored*/
        }
      }else if(heap_size == n_active){
        /*Array has heap property*/
        if(n_active != 0)
          error("The array cant have the heap property, since it is only restored in the sort function");
      }else{
        /*Restore heap for last part*/
        for(int i=heap_size;i<n_active;i++){
          heap_size++;
          fixup(i);
        }
        /*Heap restored*/
      }
      //message("heap restored!  heap_size = %d, n_active = %d", heap_size, n_active);

      tslassert(heap_size == n_active);

      while(heap_size > 0){
        swapIndices(0,heap_size - 1);
        heap_size--;
        fixdown(0);
      }
      sorted = true;
      sorted_size = n_active;
      tslassert(heap_size == 0);
      //message("swap ops = %d", swap_count);
    }

  protected:
    /**************************************************************************/
    int size;
    int n_active;

    /**************************************************************************/
    void extendStorage(){
      message("Extend storage, size = %d, sizeof(T) = %d, n_active = %d",
              size, sizeof(T), n_active);
      tslassert(n_active <= size);

      T* tmp_data = new T[size*2];

      /*Extend active element data*/
      int* tmp_index = new int[size*2];
      tslassert(tmp_index != 0);

      int* tmp_reverse_inactive = new int[size*2];
      tslassert(tmp_reverse_inactive != 0);

      int* tmp_reverse_index = new int[size*2];
      tslassert(tmp_reverse_index != 0);

      for(int i=0;i<size*2;i++){
        if(i<size){
          tmp_index[i] = index[i];
          tmp_reverse_inactive[i] = reverse_inactive[i];
          tmp_reverse_index[i] = reverse_index[i];
          tmp_data[i] = data[i];
        }else{
          tmp_index[i] = i;
          tmp_reverse_inactive[i] = i;
          tmp_reverse_index[i] = undefinedIndex;
        }
      }

      //message("array size = %d bytes", (sizeof(int) + sizeof(int) + sizeof(int) + sizeof(T)) * size * 2);
      //message("size = %d, n_active = %d", size*2, n_active);

      delete [] data;
      data = tmp_data;
      delete [] index;
      index = tmp_index;
      delete [] reverse_inactive;
      reverse_inactive = tmp_reverse_inactive;
      delete [] reverse_index;
      reverse_index = tmp_reverse_index;

      size *= 2;
    }

  public:
    /**************************************************************************/
    T*   data;
    int* index;
    int* reverse_index;
    int* reverse_inactive;

  private:
    //DynamicArray(const DynamicArray& a);
    //DynamicArray& operator=(const DynamicArray& a);

  public:

    /**************************************************************************/
    void swapIndices(int i, int j){
      tslassert(n_active <= size);
      int swp = index[i];
      index[i] = index[j];
      index[j] = swp;
      tslassert(swp < size);

      int rswp = reverse_index[index[i]];
      reverse_index[index[i]] = reverse_index[index[j]];
      reverse_index[index[j]] = rswp;

      int rswpi = reverse_inactive[index[i]];
      reverse_inactive[index[i]] = reverse_inactive[index[j]];
      reverse_inactive[index[j]] = rswpi;
      swap_count++;
    }

    /**************************************************************************/
    void fixdown(int k){
      int j;
      while(2*k + 1 < heap_size){
        j = 2*k + 1;
        if(j < heap_size-1 && data[index[j]] < data[index[j+1]]){
          j++;
        }
        if( !(data[index[k]] < data[index[j]])){
          break;
        }

        swapIndices(k, j);
        k = j;
      }
    }

    /**************************************************************************/
    void fixup(int k){
      while(k > 0 && data[index[(k-1)/2]] < data[index[k]]){
        swapIndices(k, (k-1)/2);
        k=(k-1)/2;
      }
    }

    /**************************************************************************/
    int  heap_size;
    bool heap;
    bool sorted;
    int  sorted_size;
    int  swap_count;
  };
}

#endif/*DYNAMICARRAY_HPP*/
