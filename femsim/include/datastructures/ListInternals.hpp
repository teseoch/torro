/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef LISTINTERNALS_HPP
#define LISTINTERNALS_HPP

namespace tsl{
  namespace ListInternals{
    /**************************************************************************/
    template<typename T>
    struct ListNode{
      /************************************************************************/
      typedef T           value_type;

      /************************************************************************/
      typedef ListNode<T> self_type;

      /************************************************************************/
      ListNode():next(0),prev(0){
      }

      /************************************************************************/
      self_type* next;
      self_type* prev;
      value_type  data;
    protected:
      /************************************************************************/
      ListNode(const self_type& l);

      /************************************************************************/
      ListNode& operator=(const self_type& l);
    };

    /**************************************************************************/
    template<typename T>
    struct ListIterator{
    public:
      /************************************************************************/
      typedef T                        value_type;

      /************************************************************************/
      typedef ListNode<value_type>*    nodePtr;

      /************************************************************************/
      typedef ListIterator<value_type> self_type;

      /************************************************************************/
      nodePtr ptr;

      /************************************************************************/
      /*Dereference operator*/
      value_type& operator*(){
        return ptr->data;
      }

      /************************************************************************/
      /*Pointer operator*/
      value_type* operator->(){
        return &(ptr->data);
      }

      /************************************************************************/
      /*Post increment*/
      self_type operator++(int i){
        self_type it;
        it.ptr = ptr;
        ptr = ptr->next;
        return it;
      }

      /************************************************************************/
      /*Post decrement*/
      self_type operator--(int i){
        self_type it;
        it.ptr = ptr;
        ptr = ptr->prev;
        return it;
      }

      /************************************************************************/
      /*Pre increment*/
      self_type& operator++(){
        ptr = ptr->next;
        return *this;
      }

      /************************************************************************/
      /*Pre decrement*/
      self_type& operator--(){
        ptr = ptr->prev;
        return *this;
      }

      /************************************************************************/
      /*Equality*/
      bool operator==(const self_type& i)const{
        return ptr == i.ptr;
      }

      /************************************************************************/
      /*Inequality*/
      bool operator!=(const self_type& i)const{
        return ptr != i.ptr;
      }

      /************************************************************************/
      /*Next item*/
      self_type next(){
        self_type i;
        i.ptr = ptr->next;
        return i;
      }

      /************************************************************************/
      /*Previous item*/
      self_type prev(){
        self_type i;
        i.ptr = ptr->prev;
        return i;
      }
    };
  }
}

#endif/*LISTINTERNALS_HPP*/
