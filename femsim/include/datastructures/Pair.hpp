/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef PAIR_HPP
#define PAIR_HPP

namespace tsl{
  /**************************************************************************/
  /*Template class for storing a pair of elements*/
  template<class T, class U>
  class Pair{
  public:
    /**************************************************************************/
    Pair(){
    }

    /**************************************************************************/
    Pair(T _lhs, U _rhs):lhs(_lhs), rhs(_rhs){
    }

    /**************************************************************************/
    Pair(const Pair<T, U>& p){
      lhs = p.lhs;
      rhs = p.rhs;
    }

    /**************************************************************************/
    void operator=(const Pair<T, U>& p){
      lhs = p.lhs;
      rhs = p.rhs;
    }

    /**************************************************************************/
    T getFirst()const{
      return lhs;
    }

    /**************************************************************************/
    T getSecond()const{
      return rhs;
    }
  protected:
    /**************************************************************************/
    T lhs;
    U rhs;
  };
}

#endif/*PAIR_HPP*/
