/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SURFACETREEINTERNALS_HPP
#define SURFACETREEINTERNALS_HPP

namespace tsl{

#define BBOXEPS 1e-3
  /**************************************************************************/
  enum SurfaceType{VERTEX, FACE, EDGE};

  /**************************************************************************/
  enum RayTraceMode{BackFace, FrontFace, FrontAndBackFace};

  /**************************************************************************/
  namespace SurfaceTreeInternals{
    template<class T>
    class SurfaceNode;
  }

  /**************************************************************************/
  template<class T>
  class SurfaceNodeCompare{
  public:
    /**************************************************************************/
    typedef SurfaceTreeInternals::SurfaceNode<T>* SNP;

    /**************************************************************************/
    static bool snless(const SNP& a, const SNP& b);

    /**************************************************************************/
    static bool snequal(const SNP& a, const SNP& b);

    /**************************************************************************/
    static bool snless2(const SNP& a, const SNP& b);

    /**************************************************************************/
    static bool snequal2(const SNP& a, const SNP& b);
  };

  /**************************************************************************/
  inline int binarySearch(int* A, int key, int imin, int imax){
    while(imax >= imin){
      int imid = imin + ((imax - imin) / 2);

      if(A[imid] < key){
        imin = imid + 1;
      }else if(A[imid] > key){
        imax = imid - 1;
      }else{
        return imid;
      }
    }

    return -1;
  }

  namespace SurfaceTreeInternals{
    /**************************************************************************/
    template<class T>
    class SurfaceNode{
    public:
      /************************************************************************/
      typedef SurfaceNode<T> self_type;

      /************************************************************************/
      SurfaceNode():neighboringFaces(&SurfaceNodeCompare<T>::snless2,
                                     &SurfaceNodeCompare<T>::snequal2){
        parent = childs[0] = childs[1] = 0;
        leaf = false;
        face = 0;
        id = 0;
        color[0] = genRand<float>();
        color[1] = genRand<float>();
        color[2] = genRand<float>();
        perimeter = 0;
        level = 0;
        visible = false;
        visibility = 0;
        visibilityExt = 0;
        //adjacentFaceIndices = 0;
        root = 0;

        /*Range of faces*/
        minFace = 0;
        maxFace = 0;
      }

      /************************************************************************/
      virtual ~SurfaceNode(){
        if(parent){
          if(parent->childs[0] == this){
            parent->childs[0] = 0;
          }

          if(parent->childs[1] == this){
            parent->childs[1] = 0;
          }
        }

        if(!leaf){
          if(childs[0] == childs[1]){
            if(childs[0]){
              delete childs[0];
            }
          }else{
            if(childs[0]){
              delete childs[0];
            }
            if(childs[1]){
              delete childs[1];
            }
          }
        }
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      void addEdges(Tree<int>& tree,
                    DCTetraMesh<T, A, B, C, D>* mesh){
        Tree<int>::Iterator it = tree.begin();

        while(it != tree.end()){
          int edge = *it;
          /*Check if its twinedge exists*/
          int twin = mesh->halfEdges[edge].twin;
          int index = interiorEdges.findIndex(twin);

          if(index != Tree<int>::undefinedIndex){
            /*Twin edge exists, so both sides of the edge are present,
              which implies that both surfaces are glued on this
              edge. Hence this edge will be an interior edge and can be
              removed.*/
            interiorEdges.remove(twin);
          }else{
            interiorEdges.uniqueInsert(edge, edge);
          }
          it++;
        }
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      void computeVisibility(DCTetraMesh<T, A, B, C, D>* mesh,
                             bool recursive = true){
        //bool hasVector = false;
        visibility = 0;
        if(leaf){
          /*Perform the actual test using the geometry*/
          //Triangle t = mesh->getTriangle(id);
          for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              for(int k=0;k<3;k++){
                if((i == 1) && (j == 1) && (k == 1)){
                  /*Skip, center*/
                }else{
                  Vector4<T> testVector(1-i, 1-j, 1-k, 0);
                  if(dot(testVector, tri.n) >= 0){
                    visibility |= 1 << (i*9+j*3+k);
                  }
                  if(dot(testVector, triExt.n) >= 0){
                    visibilityExt |= 1 << (i*9+j*3+k);
                  }
                }
              }
            }
          }
        }else{
          if(recursive){
            if(childs[0] == childs[1]){
              childs[0]->computeVisibility(mesh);
            }else{
              childs[0]->computeVisibility(mesh);
              childs[1]->computeVisibility(mesh);
            }
          }

          /*And-ing the visibility values*/
          visibility = childs[0]->visibility & childs[1]->visibility;
          visibilityExt = childs[0]->visibilityExt & childs[1]->visibilityExt;
        }

        if(visibility){
          visible = true;
        }
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      void computePerimeter(DCTetraMesh<T, A, B, C, D>* mesh){
        Tree<int>::Iterator it = interiorEdges.begin();
        perimeter = 0;

        while(it != interiorEdges.end()){
          int edge = *it++;

          mesh->setCurrentEdge(edge);

          Vector4<T> ea = mesh->vertices[mesh->getOriginVertex()].coord;
          Vector4<T> eb = mesh->vertices[mesh->getTwinOriginVertex()].coord;

          perimeter += (ea-eb).length();
        }
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      T computeOverlap(self_type* node, DCTetraMesh<T, A, B, C, D>* mesh){
        /*Overlapping edges in this surface, have twin edges in the other*/
        Tree<int> overlappingEdges;
        //Tree<int> twinEdges;
        Tree<int>::Iterator it = interiorEdges.begin();

        while(it != interiorEdges.end()){
          int edge = *it++;
          mesh->setCurrentEdge(edge);

          int twinEdge = mesh->getTwinEdge();
          int index = node->interiorEdges.findIndex(twinEdge);

          if(index != Tree<int>::undefinedIndex){
            overlappingEdges.uniqueInsert(twinEdge, twinEdge);
          }
        }
        T overlap = 0;

        it = overlappingEdges.begin();

        while(it != overlappingEdges.end()){
          int edge = *it++;
          mesh->setCurrentEdge(edge);
          Vector4<T> ea = mesh->vertices[mesh->getOriginVertex()].coord;
          Vector4<T> eb = mesh->vertices[mesh->getTwinOriginVertex()].coord;

          overlap += (ea - eb).length();
        }
        return overlap;
      }

      /************************************************************************/
      /*Check*/
      template<class A, class B, class C, class D>
      bool projectionsValid(DCTetraMesh<T, A, B, C, D>* mesh,
                            List<int>* invalidFaces){
        if(leaf == true){
          /*Update extended face*/
          mesh->getDisplacedTriangle(&triExt, face);

          for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              for(int k=0;k<3;k++){
                if((i == 1) && (j == 1) && (k == 1)){
                  /*Skip, center*/
                }else{
                  Vector4f testVector(1-i, 1-j, 1-k, 0);

                  if(dot(testVector, triExt.n) >= 0){
                    visibilityExt |= 1 << (i*9+j*3+k);
                  }
                }
              }
            }
          }
        }else{
          if(childs[0] == childs[1]){
            /*Nothing to check*/
            bool valid = childs[0]->projectionsValid(mesh, invalidFaces);
            visibilityExt = childs[0]->visibilityExt;
          }else{
            int originalVisibilityExt = visibilityExt;
            bool valid1 = childs[0]->projectionsValid(mesh, invalidFaces);
            bool valid2 = childs[1]->projectionsValid(mesh, invalidFaces);

            visibilityExt = childs[0]->visibilityExt & childs[1]->visibilityExt;

            if(!visibility){
              /*Current node could have self collision, no need for
                further inspection*/
            }else{

            }
          }
        }
        return false;
      }

      /************************************************************************/
      /*This function is called at convergence. If an invalid face is
        detected here, there is no need to report it.*/
      template<class A, class B, class C, class D>
      bool volumesValid(DCTetraMesh<T, A, B, C, D>* mesh,
                        List<int>* invalidFaces){
        if(leaf == true){
          /*Check if displaced triangle is contained by this bounding box*/
          try{
            mesh->getDisplacedTriangle(&triExt, face, false);
          }catch(Exception* e){
            warning("Singular face %d %s", face, e->getError().c_str());
            delete e;

            /*Since this face is singular we don't care.*/
            return true;
          }

          if(box.inside(triExt.v[0]) &&
             box.inside(triExt.v[1]) &&
             box.inside(triExt.v[2])){
            /*All vertices of triangle are bound by the volume*/
            return true;
          }else{
            message("Face %d not in box", face);
            std::cout << triExt << std::endl;
            std::cout << box << std::endl;
            invalidFaces->append(face);
            return false;
          }
        }else{
          if(childs[0] == childs[1]){
            return childs[0]->volumesValid(mesh, invalidFaces);
          }else{
            return (childs[0]->volumesValid(mesh, invalidFaces) &&
                    childs[1]->volumesValid(mesh, invalidFaces));
          }
        }
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      void updateBBox(DCTetraMesh<T, A, B, C, D>* mesh,
                      bool keepCurrentBoxes = false){
        if(leaf == true){
          if(keepCurrentBoxes){

          }else{
            box.reset();
          }

          /*Update triangle*/
          try{
            mesh->getTriangle(&tri, face, false);
            mesh->getDisplacedTriangleExt(&triExt, face, false);
          }catch(Exception* e){
            warning("Singular face found %s", e->getError().c_str());
            delete e;
          }

          /*However, the vertices are still correct, so we can create a
            correct bounding box.*/
          box.addPoint(tri.v[0]);
          box.addPoint(tri.v[1]);
          box.addPoint(tri.v[2]);

          box.addPoint(triExt.v[0]);
          box.addPoint(triExt.v[1]);
          box.addPoint(triExt.v[2]);

          box.addEpsilon((T)BBOXEPS);
        }else{
          if(childs[0] == childs[1]){
            childs[0]->updateBBox(mesh, keepCurrentBoxes);
            box = childs[0]->box;
          }else{
            childs[0]->updateBBox(mesh, keepCurrentBoxes);
            childs[1]->updateBBox(mesh, keepCurrentBoxes);
            box = childs[0]->box;
            box.addBox(childs[1]->box);
          }
        }
      }

      /************************************************************************/
      /*Bottom up*/
      void computeLevelsBU(){
        if(leaf){
          level = 0;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->computeLevelsBU();
            level = childs[0]->level + 1;
          }else{
            childs[0]->computeLevelsBU();
            childs[1]->computeLevelsBU();
            level = Max(childs[0]->level, childs[1]->level) + 1;
          }
        }
      }

      /************************************************************************/
      /*Top down*/
      void computeLevelsTD(int parentLevel){
        if(leaf){
          level = parentLevel;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->computeLevelsTD(parentLevel-1);
            level = parentLevel;
          }else{
            childs[0]->computeLevelsTD(parentLevel-1);
            childs[1]->computeLevelsTD(parentLevel-1);
            level = parentLevel;
          }
        }
      }

      /************************************************************************/
      void setRoot(self_type* node){
        root = node;
        if(leaf){
          return;
        }else{
          if(childs[0] == childs[1]){
            childs[0]->setRoot(node);
          }else{
            childs[0]->setRoot(node);
            childs[1]->setRoot(node);
          }
        }
      }

      /************************************************************************/
      int getDepth()const{
        if(leaf){
          return 1;
        }else{
          return 1+MAX(childs[0]->getDepth(), childs[1]->getDepth());
        }
      }

      /************************************************************************/
      int maxAdjacency()const{
        int adjSize = adjacentFaces.size();
        if(!leaf){
          if(childs[0]){
            if(childs[0] == childs[1]){
              int mx0 = childs[0]->maxAdjacency();
              return adjSize>mx0?adjSize:mx0;
            }else{
              int mx0 = childs[0]->maxAdjacency();
              int mx1 = childs[1]->maxAdjacency();
              int mx3 = mx0>mx1?mx0:mx1;
              return adjSize>mx3?adjSize:mx3;
            }
          }
        }else{
          return adjSize;
        }
        return 0;
      }

      /************************************************************************/
      void printSpaces(int dp){
        for(int i=0;i<dp*4;i++){
          std::cout << ' ';
        }
      }

      /************************************************************************/
      void printVisibility(int dp){
        printSpaces(dp);
        for(int i=0;i<27;i++){
          int mask = 1<<i;
          int val  = (visibility & mask)>>i;
          std::cout << val;
        }
        std::cout << std::endl;
      }

      /************************************************************************/
      bool checkAdjacency(self_type* query,
                          int* adjacentNodes,
                          int* nAdjacent, int max_adj){
        int id = nodeId;
        int qid = query->nodeId;
        int n = nAdjacent[nodeId];

        int res = binarySearch(adjacentNodes + id * max_adj, qid, 0, n);

        if(res == -1){
          return false;
        }
        return true;
      }

      /************************************************************************/
      bool findSelfCollisions(self_type* query,
                              List<int>* list,
                              int* adjacentNodes,
                              int* nAdjacent, int max_adj){
        self_type* stack[1000];
        int top = 0;
        bool collision = false;

        stack[top++] = this;

        while(top != 0){
          self_type* stackTop = stack[top-1];

          if(stackTop == query){
            /*Skip*/
            top--;
            continue;
          }

          bool boxIntersection = false;
          bool leafAdjCheck = false;

          if(stackTop->singleNode){
            boxIntersection = true;
          }else{
            boxIntersection = stackTop->box.intersection(query->box);
          }

          if(boxIntersection){
            /*Possible collision*/

            /*Check if this node is a parent of query node*/
            self_type* child = query;

            /*Check if query and current node share the same root. If
              so, check for self collisions. Otherwise, there are no
              self collisions, skip self collision check.*/
            if(!stackTop->singleNode){
              /*Traverse the tree to the root until the current node has
                no parent*/
              bool skip = false;
              while(!skip){
                if(child == stackTop){
                  /*The query node is a part of the sub-surface
                    represented by this node. If visibility has one bit
                    enabled, the subsurface is free of self
                    collisions. Hence this node does not collide with
                    the query node. */
                  if(stackTop->visibility){
                    /*There exists a vector defining a plane on which
                      this node and it children can project without
                      self-intersection*/

                    /*If there exists such a vector AND the query node
                      is a deep child of this node, there is no (self)
                      intersection*/
                    skip = true;
                  }
                  break;
                }
                if(child == query->root){
                  /*We have reached the root of the object associated
                    with the query node*/
                  break;
                }

                if(stackTop->level == child->level){
                  /*We have reached a node in the chain to the root
                    which is at the same depth as this node. Check if
                    that node is an adjacent node of the current node.*/

                  if(stackTop->visibility & child->visibility){
                    /*If there exists a vector defining a plane on which
                      both adjacent subtrees can project, there is no
                      (self) collision.*/

                    leafAdjCheck = true;

                    if(stackTop->checkAdjacency(child, adjacentNodes,
                                                nAdjacent, max_adj)){
                      skip = true;
                    }
                  }else{
                    /*Current node and this node are not adjacent.*/
                  }
                  /*Current node and child node are not adjacent or
                    adjacent but not collision free projection. Continue
                    with recursive collision check*/
                  break;
                }
                child = child->parent;
              }
              if(skip){
                top--;
                continue;
              }
            }

            if(stackTop->leaf){
              bool adjacentFaces = false;

              /*Query node and this node share the same root. Check if the
                nodes are adjacent. If so, there is no collision
                possible.*/
              if(leafAdjCheck){
                adjacentFaces =
                  stackTop->checkAdjacency(child, adjacentNodes,
                                           nAdjacent, max_adj);
              }

              if(adjacentFaces){
                top--;
              }else{
                list->append(stackTop->id);
                collision = true;
                top--;
              }
            }else{
              if(stackTop->childs[0] == stackTop->childs[1]){
                self_type* c1 = stackTop->childs[0];
                top--;
                stack[top++] = c1;
              }else{
                self_type* c1 = stackTop->childs[0];
                self_type* c2 = stackTop->childs[1];
                top--;
                stack[top++] = c1;
                stack[top++] = c2;
              }
            }
          }else{
            top--;
          }
        }
        return collision;
      }

      /************************************************************************/
      bool findCollisions(self_type* query,
                          List<int>* list){
        self_type* stack[1000];
        int top = 0;
        bool collision = false;

        stack[top++] = this;

        while(top != 0){
          self_type* stackTop = stack[top-1];

          if(stackTop == query){
            /*Skip*/
            top--;
            continue;
          }

          if(stackTop->root == query->root && query->root != 0){
            top--;
            continue;
          }

          bool boxIntersection = false;

          if(stackTop->singleNode){
            boxIntersection = true;
          }else{
            boxIntersection = stackTop->box.intersection(query->box);
          }

          if(boxIntersection){
            if(stackTop->leaf){
              list->append(stackTop->id);
              collision = true;
              top--;
            }else{
              if(stackTop->childs[0] == stackTop->childs[1]){
                self_type* c1 = stackTop->childs[0];
                top--;
                stack[top++] = c1;
              }else{
                self_type* c1 = stackTop->childs[0];
                self_type* c2 = stackTop->childs[1];
                top--;
                stack[top++] = c1;
                stack[top++] = c2;
              }
            }
          }else{
            top--;
          }
        }
        return collision;
      }

      /************************************************************************/
      bool checkAdjacencyR(self_type* query){
        int index = adjacentFaces.findIndex(query);
        if(index == Tree<int>::undefinedIndex){
          return false;
        }
        return true;
      }

      /************************************************************************/
      template<class A, class B, class C, class D>
      bool findCollisionsR(self_type* query,
                           const DCTetraMesh<T, A, B, C, D>* mesh,
                           List<int>* list = 0, bool skipCheck = false){
        bool dump = false;

        if(root == query->root){
          /*Current node has same root as query node. If query node is
            of type static or rigid, we can stop here since rigid and
            static objects do not have self collisions.*/
          if(Mesh::isRigidOrStatic(mesh->halfFaces[query->face].type)){
            return false;
          }
        }

        if(query->face == 2910 ||
           query->face == 2906){
          //dump = true;
        }

        //findcall++;
        if(query == this){
          return false;
        }

        bool boxIntersection = false;
        if(skipCheck){
          boxIntersection = true;
        }else{
          boxIntersection = box.intersection(query->box);
        }

        if(boxIntersection){
          /*Possible collision*/


          /*Check if this node is a parent of query node*/
          self_type* child = query;

          /*Check if query and current node share the same root. If so,
            check for self collisions. Otherwise, there are no self
            collisions, skip self collision check.*/
          if(!skipCheck && query->root == root){
            /*Traverse the tree to the root until the current node has
              no parent*/
            while(true){
              if(child == this){
                /*The query node is a part of the sub-surface
                  represented by this node. If visibility has one bit
                  enabled, the subsurface is free of self
                  collisions. Hence this node does not collide with the
                  query node. */
                if(visibility && visibilityExt){
                  /*There exists a vector defining a plane on which this
                    node and its children can project without
                    self-intersection*/

                  /*If there exists such a vector AND the query node is
                    a deep child of this node, there is no (self)
                    intersection*/
                  ///return false;
                }
                break;
              }
              if(child == 0){
                /*We have reached the root of the object associated with
                  the query node*/
                break;
              }

              if(level == child->level){
                /*We have reached a node in the chain to the root which
                  is at the same depth as this node. Check if that node
                  is an adjacent node of the current node.*/
                /*TODO: figure out what to do here*/

                if(/*visibility & child->visibility &
                     visibilityExt & child->visibilityExt*/false){
                  /*If there exists a vector defining a plane on which
                    both adjacent subtrees can project, there is no
                    (self) collision.*/

                  if(checkAdjacencyR(child)){
                    return false;
                  }
                }else{
                  /*Current node and this node are not adjacent.*/
                }
                /*Current node and child node are not adjacent or
                  adjacent but not collision free projection. Continue
                  with recursive collision check*/
                break;
              }
              child = child->parent;
            }
          }

          if(leaf){
            bool valid = true;
            bool adjacentFaces = false;

            if(query->root == root){
              if(dump){
                warning("face %d and face %d share same root",
                        face, query->face);
              }
              /*Query node and this node share the same root. Check if
                the nodes are adjacent. If so, there is no collision
                possible.*/
              adjacentFaces = false;//checkAdjacencyR(query);
              if(adjacentFaces){
                if(dump){
                  warning("adjacent, skip");
                }
                return false;
              }else{
                if(dump){
                  warning("not adjacent");
                }
              }
            }

            if(valid && !adjacentFaces){
              //collision = true;
              //query->collision = true;

              if(dump){
                warning("adding %d, %d", face, query->face);
              }

              if(list){
                //message("collision %d - %d", query->id, id);
                list->append(id);
              }
              return true;
            }else{
              return false;
            }
          }else{
            if(childs[0] == childs[1]){
              return childs[0]->findCollisionsR(query, mesh, list, singleNode);
            }else{
              bool c1 = childs[0]->findCollisionsR(query, mesh, list);
              bool c2 = childs[1]->findCollisionsR(query, mesh, list);
              return c1 || c2;
            }
          }
        }
        return false;
      }

      /************************************************************************/
      bool findDebugCollisions(self_type* query,
                               List<int>* list = 0,
                               bool skipCheck = false){
        warning("findDebugCollisions, level = %d, ids %d, %d",
                level, id, query->id);
        if(query == this){
          warning("query == this");
          return false;
        }

        bool boxIntersection = false;
        if(skipCheck){
          boxIntersection = true;
        }else{
          boxIntersection = box.intersection(query->box);
        }

        if(boxIntersection){
          /*Possible collision*/

          warning("bounding box intersection");

          /*Check if this node is a parent of query node*/
          self_type* child = query;

          /*Check if query and current node share the same root. If so,
            check for self collisions. Otherwise, there are no self
            collisions, skip self collision check.*/
          if(!skipCheck && query->root == root){
            warning("Both query and this node have the same root");
            /*Traverse the tree to the root until the current node has
              no parent*/
            while(true){
              warning("traverse up levels :: %d, %d",
                      this->level, child->level);
              if(child == this){
                warning("query and subject node share this node");
                /*The query node is a part of the sub-surface
                  represented by this node. If visibility has one bit
                  enabled, the subsurface is free of self
                  collisions. Hence this node does not collide with the
                  query node. */

                warning("vis %d, %d", visibility, visibilityExt);
                if(visibility && visibilityExt){
                  /*There exists a vector defining a plane on which this
                    node and its children can project without
                    self-intersection*/

                  /*If there exists such a vector AND the query node is
                    a deep child of this node, there is no (self)
                    intersection*/
                  warning("Shared node is completely visible, no collisions");
                  return false;
                }
                break;
              }
              if(child == 0){
                /*We have reached the root of the object associated with
                  the query node*/
                warning("root reached, break");
                break;
              }

              if(level == child->level){
                /*We have reached a node in the chain to the root which
                  is at the same depth as this node. Check if that node
                  is an adjacent node of the current node.*/

                warning("parent nodes found on the same level, %d", level);
                warning("vis bits this  %d %d", visibility, visibilityExt);
                warning("vis bits child %d %d", child->visibility,
                        child->visibilityExt);

                warning("%d", visibility & child->visibility);
                warning("%d", visibilityExt & child->visibilityExt);

                if(/*visibility & child->visibility &
                     visibilityExt & child->visibilityExt*/false){
                  /*If there exists a direction from which we can see
                    all faces in the normal and extended adjacent
                    subtrees, there cant be a (self) collision*/

                  if(checkAdjacencyR(child)){
                    warning("Adjacent nodes");
                    return false;
                  }
                }else{
                  /*Current node and this node are not adjacent.*/
                }
                /*Current node and child node are not adjacent or
                  adjacent but not collision free projection. Continue
                  with recursive collision check*/
                break;
              }
              child = child->parent;
            }
          }

          warning("Root reached");

          if(leaf){
            warning("this node is leaf");
            bool valid = true;
            bool adjacentFaces = false;

            if(query->root == root){
              warning("Nodes share same root");
              /*Query node and this node share the same root. Check if
                the nodes are adjacent. If so, there is no collision
                possible.*/
              adjacentFaces = checkAdjacencyR(query);
              if(adjacentFaces){
                warning("adjacent, skip");
                return false;
              }else{
                warning("not adjacent");
              }
            }

            if(valid && !adjacentFaces){
              //collision = true;
              //query->collision = true;

              warning("valid and not adjacent");

              if(list){
                //message("collision %d - %d", query->id, id);
                list->append(id);
              }
              return true;
            }else{
              return false;
            }
          }else{
            if(childs[0] == childs[1]){
              return childs[0]->findDebugCollisions(query, list,
                                                    singleNode);
            }else{
              bool c1 = childs[0]->findDebugCollisions(query, list);
              bool c2 = childs[1]->findDebugCollisions(query, list);
              return c1 || c2;
            }
          }
        }else{
          warning("No bounding box intersections");
        }
        return false;
      }

      /************************************************************************/
      void collectNeighboringFaces(const BBox<T>& query, List<int>& faces){
        if(box.intersection(query)){
          if(leaf){
            faces.append(id);
          }else{
            if(childs[0] == childs[1]){
              childs[0]->collectNeighboringFaces(query, faces);
            }else{
              childs[0]->collectNeighboringFaces(query, faces);
              childs[1]->collectNeighboringFaces(query, faces);
            }
          }
        }
      }

      /************************************************************************/
      void traceLine(const Vector4<T>& a, const Vector4<T>& b,
                     Tree<int>* faces){
        Vector4<T> col;
        Vector4<T> bary;

        Ray<T> ray;

        Vector4<T> dir = b-a;
        ray.setOrigin(a);
        ray.setDirection(dir);

        if(box.intersects(ray)){
          if(leaf){
            T t = -1000;

            if(tri.lineIntersection(a, dir, col, &t, &bary)){
              faces->uniqueInsert(face);
              if(t == -1000){
                error("T not set while intersecting");
              }
            }
          }else{
            /*Trace childs*/
            if(childs[0] == childs[1]){
              childs[0]->traceLine(a, b, faces);
            }else{
              childs[0]->traceLine(a, b, faces);
              childs[1]->traceLine(a, b, faces);
            }
          }
        }else{
          /*No box intersection*/
        }
      }

      /************************************************************************/
      /*When both back and front faces are searched, it is possible that
        a ray exactly hits a shared edge of two faces. When used in a
        raytracing setting, the next ray starts from the intersection
        point of the previous ray. When both back and front faces are
        searched, this method will return the neighboring face, since
        that is the closest, but is in principle not correct.*/
      int traceRay(const Ray<T>& ray,
                   Vector4<T>& col,
                   int self,
                   RayTraceMode mode,
                   T* distance = 0,
                   Vector4<T>* bary = 0,
                   int* depth = 0)const{
        T epsilon = (T)0.0;

        if(mode != FrontAndBackFace){
          /*When only front or backfaces are traced, we can move the
            startingpoint of a ray slightly backwards. This ensures a
            proper ray-face intersection test in case of touching
            objects.*/
          epsilon = (T)1e-5;
        }

        if(box.intersects(ray)){
          if(leaf){
            if(face == self){
              return -1;
            }

            //Check ray triangle intersection
            T t;
            //float nd = -1;//tri.n * ray.dir;
            T nd = dot(tri.n, ray.getDirection());

            bool check = false;
            if(mode == FrontAndBackFace && Abs(nd) > 0.0f){
              check = true;
            }else if(mode == FrontFace && nd < 0.0f){
              check = true;
            }else if(mode == BackFace && nd > 0.0f){
              check = true;
            }

            if(check){
              //          if(nd < 0){
              if(tri.intersects(ray, col, &t, bary)){
                if(t <= -epsilon){
                  return -1;
                }
                if(distance){
                  //Vector4f dir = ray.getDirection();
                  //dir.normalize();
                  *distance = t;//-(ray.getOrigin() - col)*dir;
                }

                return face;
              }else{
                return -1;
              }
            }else{
              return -1;
            }
          }else{
            T d1, d2;
            Vector4<T> b1, b2;
            Vector4<T> c1, c2;
            int depth1 = *depth+1;
            int depth2 = *depth+1;
            d1 = d2 = 1e+10;
            int t1, t2;
            t1 = t2 = -1;

            if(childs[0] == childs[1]){
              t1 = childs[0]->traceRay(ray, c1, self, mode, &d1, &b1, &depth1);
            }else{
              t1 = childs[0]->traceRay(ray, c1, self, mode, &d1, &b1, &depth1);
              t2 = childs[1]->traceRay(ray, c2, self, mode, &d2, &b2, &depth2);
            }

            if(t1 == -1 && t2 == -1){
              //ok
            }else{
              tslassert(t1 != t2);
            }

            if(childs[0] == childs[1]){
              if(distance){
                *distance = d1;
              }
              if(bary){
                *bary = b1;
              }
              col = c1;
              return t1;
            }else{
              if(Abs(d1) < Abs(d2)){
                if(distance){
                  *distance = d1;
                }
                if(bary){
                  *bary = b1;
                }
                col = c1;
                return t1;
              }else{
                if(distance){
                  *distance = d2;
                }
                if(bary){
                  *bary = b2;
                }
                col = c2;
                return t2;
              }
            }
          }
        }

        if(distance){
          *distance = 1e+10;
        }
        return -1;
      }
      /************************************************************************/
      //protected:
      self_type* root; /*Root of the object != root of tree*/
      self_type* parent;
      self_type* childs[2];

      Vector4<T> averageNormal;
      T area;
      int face;
      int minFace;
      int maxFace;
      BBox<T> box;
      bool leaf;
      //List<SurfaceNode*> neighboringFaces;
      Tree<self_type*> neighboringFaces;
      Tree<self_type*> adjacentFaces;
      //SurfaceNode*       adjacentFaceIndices;
      Tree<int> interiorEdges;
      int id;
      int level;
      Vector4f color;
      SurfaceType type;

      int visibility;
      int visibilityExt;
      bool visible;

      T perimeter;

      Triangle<T> tri;
      Triangle<T> triExt;

      int nodeId;  /*Used to compute a unique identifier*/
      bool singleNode;
    };
  }
}

#endif/*SURFACETREEINTERNALS_HPP*/
