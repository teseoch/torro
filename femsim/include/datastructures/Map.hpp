/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MAP_HPP
#define MAP_HPP

#include "datastructures/Tree.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class U>
  class MapPair{
  public:
    /**************************************************************************/
    MapPair(){
    }

    /**************************************************************************/
    MapPair(T _key, U _data):key(_key), data(_data){
    }

    /**************************************************************************/
    MapPair(const MapPair<T, U>& p){
      key = p.key;
      data = p.data;
    }

    /**************************************************************************/
    void operator=(const MapPair<T, U>& p){
      key = p.key;
      data = p.data;
    }

    /**************************************************************************/
    inline const T getKey()const{
      return key;
    }

    /**************************************************************************/
    inline T getKey(){
      return key;
    }

    /**************************************************************************/
    inline const U& getData()const{
      return data;
    }

    /**************************************************************************/
    inline U& getData(){
      return data;
    }

    /**************************************************************************/
    template<class V, class W>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const MapPair<V, W>& m);
  protected:
    /**************************************************************************/
    T key;
    U data;
  };

  /**************************************************************************/
  template<class T, class U>
  class Compare<MapPair<T, U> >{
  public:
    /**************************************************************************/
    static bool less(const MapPair<T, U>& a, const MapPair<T, U>& b){
      return Compare<T>::less(a.getKey(), b.getKey());
    }

    /**************************************************************************/
    static bool equal(const MapPair<T, U>& a, const MapPair<T, U>& b){
      return Compare<T>::equal(a.getKey(), b.getKey());
    }
  };

  /**************************************************************************/
  /*Map based on Tree datastructure*/
  template<class T, class U, class Alloc = Allocator<T> >
  class Map{
  public:
    /**************************************************************************/
    typedef T key_type;

    /**************************************************************************/
    typedef U data_type;

    /**************************************************************************/
    typedef typename Tree<MapPair<key_type, data_type>,
                          Alloc>::Iterator Iterator;

    /**************************************************************************/
    Map(){
    }

    /**************************************************************************/
    Iterator begin(){
      return tree.begin();
    }

    /**************************************************************************/
    Iterator end(){
      return tree.end();
    }

    /**************************************************************************/
    void insert(key_type& key, const data_type& val){
      MapPair<key_type, data_type> pair(key, val);
      tree.insert(pair);
    }

    /**************************************************************************/
    void insert(key_type& key, data_type& val){
      MapPair<key_type, data_type> pair(key, val);
      tree.insert(pair);
    }

    /**************************************************************************/
    Iterator find(const key_type& key)const{
      MapPair<key_type, data_type> pair(key, U());
      return tree.find(pair);
    }

    /**************************************************************************/
    void clear(){
      tree.clear();
    }

    /**************************************************************************/
    int size()const{
      return tree.size();
    }

    /**************************************************************************/
    void remove(Iterator& it){
      tree.remove(it);
    }

    /**************************************************************************/
    template<class V, class W, class AC>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Map<V, W, AC>& m);
  protected:
    /**************************************************************************/
    Tree<MapPair<key_type, data_type>, Alloc> tree;
  };

  /**************************************************************************/
  template<class T, class U>
  inline std::ostream& operator<<(std::ostream& os, const MapPair<T, U>& t){
    os << "[" << t.key << "] -> " << t.data << ", ";
    return os;
  }

  /**************************************************************************/
  template<class T, class U, class Alloc>
  inline std::ostream& operator<<(std::ostream& os, const Map<T, U, Alloc>& t){
    os << t.tree;
    return os;
  }
}

#endif/*MAP_HPP*/
