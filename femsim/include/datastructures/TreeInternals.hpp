/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef TREEINTERNALS_HPP
#define TREEINTERNALS_HPP

namespace tsl{
  /*Internal node and iterator classes for the Tree class*/
  namespace TreeInternals{
    /**************************************************************************/
    template<typename T>
    struct TreeNode{
    public:
      /************************************************************************/
      typedef T                        value_type;

      /************************************************************************/
      typedef TreeNode<T>              self_type;

      /************************************************************************/
      TreeNode(value_type& t, int idx):parent(0),
                                       l_child(0),
                                       r_child(0),
                                       data(t),
                                       index(idx),
                                       balance(0),
                                       depth(0){
      }

      /************************************************************************/
      ~TreeNode(){
      }

      /************************************************************************/
      void computeDepth(){
        int left_depth = 0;
        int right_depth = 0;

        if(l_child){
          left_depth = l_child->depth;
        }
        if(r_child){
          right_depth = r_child->depth;
        }

        depth = 1+Max(left_depth, right_depth);
        balance = left_depth - right_depth;
      }

      /************************************************************************/
      self_type* parent;
      self_type* l_child;
      self_type* r_child;
      value_type data;

      int index;
      int balance;
      int depth;
    };

    /************************************************************************/
    template<typename T>
    struct TreeIterator{
    public:
      /************************************************************************/
      typedef T               value_type;

      /************************************************************************/
      typedef TreeNode<T>     Node;

      /************************************************************************/
      typedef TreeIterator<T> self_type;

      /************************************************************************/
      TreeIterator(Node* p):ptr(p){
      }

      /************************************************************************/
      int getIndex(){
        return ptr->index;
      }

      /************************************************************************/
      value_type& operator*(){
        return ptr->data;
      }

      /************************************************************************/
      value_type* operator->(){
        return &(ptr->data);
      }

      /************************************************************************/
      /*Post increment*/
      self_type operator++(int i){
        self_type it(ptr);
        next();
        return it;
      }

      /************************************************************************/
      /*Post decrement*/
      self_type operator--(int i){
        self_type it(ptr);
        prev();
        return it;
      }

      /************************************************************************/
      /*Pre increment*/
      self_type& operator++(){
        next();
        return *this;
      }

      /************************************************************************/
      /*Pre decrement*/
      self_type& operator--(){
        prev();
        return *this;
      }

      /************************************************************************/
      /*Equality*/
      bool operator==(const self_type& i)const{
        return ptr == i.ptr;
      }

      /************************************************************************/
      /*Equality*/
      bool operator!=(const self_type& i)const{
        return ptr != i.ptr;
      }

      /************************************************************************/
      //protected:
      Node* ptr;

      /************************************************************************/
      void next(){
        if(ptr->r_child){
          /*Find left most child*/
          ptr = ptr->r_child;
          while(ptr->l_child){
            ptr = ptr->l_child;
          }
          /*Arrived at left most child*/
        }else{
          /*Go to parent node if this node is a left child of the parent*/
          if(ptr->parent){
            if(ptr->parent->l_child == ptr){
              ptr = ptr->parent;
            }else if(ptr->parent->r_child == ptr){
              /*Find first parent node for which
                the current node is a left child*/
              while(ptr->parent){
                if(ptr->parent->r_child == ptr){
                  ptr = ptr->parent;
                }else if(ptr->parent->l_child == ptr){
                  ptr = ptr->parent;
                  return;
                }
              }
              ptr = 0;
            }
          }else{
            ptr = 0;
          }
        }
      }

      /************************************************************************/
      void prev(){
        //error();
        //message("pointer = %p", ptr);
        if(ptr->l_child){
          /*Find right most child*/
          ptr = ptr->l_child;
          while(ptr->r_child){
            ptr = ptr->r_child;
          }
          /*Arrived at left most child*/
        }else{
          /*Go to parent node if this node is a right child of the parent*/
          if(ptr->parent){
            if(ptr->parent->r_child == ptr){
              ptr = ptr->parent;
            }else if(ptr->parent->l_child == ptr){
              /*Find first parent node for which
                the current lode is a left child*/
              while(ptr->parent){
                if(ptr->parent->l_child == ptr){
                  ptr = ptr->parent;
                }else if(ptr->parent->r_child == ptr){
                  ptr = ptr->parent;
                  return;
                }
              }
              ptr = 0;
            }
          }else{
            ptr = 0;
          }
        }
      }
    };
  }
}

#endif/*TREEINTERNALS_HPP*/
