/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DCELIST_HPP
#define DCELIST_HPP

#include <iostream>
#include <fstream>

#include "datastructures/List.hpp"
#include "datastructures/Tree.hpp"
#include "math/Quaternion.hpp"
#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/Vector.hpp"
#include "string.h"
#include <limits.h>
#include <map>
#include <list>
#include <stdio.h>
#include "geo/BBox.hpp"
#include "geo/Triangle.hpp"
#include "geo/Edge.hpp"
#include "geo/OrientedVertex.hpp"

namespace tsl{
#define DEFAULT_SIZE 100

  //#define DIST_C (T)3.0
#define DIST_C (T)2.0

  //#define NI -1

  /**************************************************************************/
  /*Tetrahedron -> 4 faces*/
  /*Face        -> 1 half-edge*/
  /*Half-edge   -> 1 origin vertices & 1 faces & 1 twin & 1 next half-edge*/
  /*Vertex      -> 1 halfedge*/

  /**************************************************************************/
  namespace Mesh{
    /**************************************************************************/
    static const int UndefinedIndex = -1;

    /**************************************************************************/
    enum GeometryType{
      Deformable,  /*Velocity is defined by translations only*/
      Rigid,       /*Velocity is defined by translation and angular velocity,
                     Geometry should have a center of mass defined*/
      Static,      /*Velocity is always zero*/
      Cloth,       /*Special case of deformable geometry*/
      Undefined    /*Undefined type of geometry*/
    };

    /**************************************************************************/
    inline bool isRigidOrStatic(GeometryType& type){
      if(type == Rigid || type == Static){
        return true;
      }
      return false;
    }

    /**************************************************************************/
    inline bool isRigid(GeometryType& type){
      if(type == Rigid){
        return true;
      }
      return false;
    }

    /**************************************************************************/
    inline bool isStatic(GeometryType& type){
      if(type == Static){
        return true;
      }
      return false;
    }

    /**************************************************************************/
    inline bool isNotStatic(GeometryType& type){
      return !isStatic(type);
    }
  }

  /**************************************************************************/
  template<class T, class VD=char>
  class DCELVertex{
  public:
    /**************************************************************************/
    DCELVertex():coord(0,0,0,0),
                 displCoord(0,0,0,0),
                 displCoordExt(0,0,0,0),
                 leaving(Mesh::UndefinedIndex),
                 normal(0,0,0,0),
                 displNormal(0,0,0,0),
                 data(),
                 id(0),
                 vel_index(0),
                 type(Mesh::Undefined){
    }

    /**************************************************************************/
    DCELVertex(const Vector4<T>& c, int l):coord(c),
                                           displCoord(c),
                                           displCoordExt(c),
                                           leaving(l),
                                           normal(0,0,0,0),
                                           displNormal(0,0,0,0),
                                           data(),
                                           id(0),
                                           vel_index(0),
                                           type(Mesh::Undefined){
    }

    /**************************************************************************/
    Vector4<T> coord;         /*Position of this vertex*/
    Vector4<T> displCoord;    /*Displaced position of this vertex*/
    Vector4<T> displCoordExt; /*Extended displaced position of this vertex*/
    int        leaving;       /*A half edge that leaves from this vertex*/
    Vector4<T> normal;        /*Vertex normal, used for smoothening*/
    Vector4<T> displNormal;   /*Displaced vertex normal, at displaced vertex*/
    VD         data;          /*User defined data*/
    int        id;            /*Unique id*/
    int        vel_index;     /*Index in velocity vector*/
    Mesh::GeometryType type;  /*Type of geometry*/

    /**************************************************************************/
    template <class U, class V>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const DCELVertex<U, V>& v);

    /**************************************************************************/
    void print()const{
      message("Vertex:: id = %d, leaving = %d, vel_index = %d",
              id, leaving, vel_index);
      std::cout << coord  << displCoord << displCoordExt;
      std::cout << normal << displNormal;
    }

    /**************************************************************************/
    template<class VD2>
    DCELVertex<T, VD>& operator=(const DCELVertex<T, VD2>& v){
      if((void*)this != (void*)&v){
        coord         = v.coord;
        displCoord    = v.displCoord;
        displCoordExt = v.displCoordExt;
        leaving       = v.leaving;
        normal        = v.normal;
        displNormal   = v.displNormal;
        id            = v.id;
        vel_index     = v.vel_index;
        type          = v.type;
        /*Copy vertex specific data*/
        data          = v.data;
      }
      return *this;
    }

    /**************************************************************************/
    DCELVertex<T, VD>& operator=(const DCELVertex<T, VD>& v){
      if((void*)this != (void*)&v){
        coord         = v.coord;
        displCoord    = v.displCoord;
        displCoordExt = v.displCoordExt;
        leaving       = v.leaving;
        normal        = v.normal;
        displNormal   = v.displNormal;
        id            = v.id;
        vel_index     = v.vel_index;
        type          = v.type;
        /*Copy vertex specific data*/
        data          = v.data;
      }
      return *this;
    }

    /**************************************************************************/
    template<class VD2>
    DCELVertex(const DCELVertex<T, VD2>& v){
      *this = v;
    }

    /**************************************************************************/
    bool operator<(const DCELVertex<T, VD>& b)const{
      return coord < b.coord;
    }
  };

  /**************************************************************************/
  template<class U, class V>
  inline std::ostream& operator<<(std::ostream& os,
                                  const DCELVertex<U, V>& v){
    v.print();
    return os;
  }

  /**************************************************************************/
  template<class ED=char>
  class DCELHalfEdge{
  public:
    /**************************************************************************/
    DCELHalfEdge():origin(Mesh::UndefinedIndex),
                   half_face(Mesh::UndefinedIndex),
                   twin(Mesh::UndefinedIndex),
                   next(Mesh::UndefinedIndex),
                   next_twin(Mesh::UndefinedIndex),
                   visited(false),
                   data(),
                   type(Mesh::Undefined){
    }

    /**************************************************************************/
    DCELHalfEdge(int o, int hf, int t, int n, int nt):origin(o),
                                                      half_face(hf),
                                                      twin(t),
                                                      next(n),
                                                      next_twin(nt),
                                                      visited(false),
                                                      data(),
                                                      type(Mesh::Undefined){
    }

    /**************************************************************************/
    int origin;     /*A pointer to the origin vertex of this half edge*/
    int half_face;  /*A pointer to the half face left of this half edge*/
    int twin;       /*A pointer to the half edge right of this edge*/
    int next;       /*A pointer to the next half edge*/
    int next_twin;  /*A pointer to the next twin*/
    bool visited;   /*Temporary value*/
    ED data;        /*User defined data*/
    Mesh::GeometryType type; /*Geometry type*/

    /**************************************************************************/
    template<class ED2>
    DCELHalfEdge& operator=(const DCELHalfEdge<ED2>& e){
      if((void*)this != (void*)&e){
        origin    = e.origin;
        half_face = e.half_face;
        twin      = e.twin;
        next      = e.next;
        next_twin = e.next_twin;
        type      = e.type;
        visited   = e.visited;
        /*Copy edge specific data*/
        data      = e.data;
      }
      return *this;
    }

    /**************************************************************************/
    DCELHalfEdge& operator=(const DCELHalfEdge& e){
      if((void*)this != (void*)&e){
        origin    = e.origin;
        half_face = e.half_face;
        twin      = e.twin;
        next      = e.next;
        next_twin = e.next_twin;
        type      = e.type;
        visited   = e.visited;
        /*Copy edge specific data*/
        data      = e.data;
      }
      return *this;
    }

    /**************************************************************************/
    template<class ED2>
    DCELHalfEdge(const DCELHalfEdge<ED2>& e){
      *this = e;
    }

    /**************************************************************************/
    void print()const{
      message("HalfEdge:: origin = %d, half_face = %d, twin = %d, next = %d",
              origin, half_face, twin, next);
    }
  };

  /**************************************************************************/
  template<class T, class FD=char>
  class DCELHalfFace{
  public:
    /**************************************************************************/
    DCELHalfFace():half_edge(Mesh::UndefinedIndex),
                   twin(Mesh::UndefinedIndex),
                   next(Mesh::UndefinedIndex),
                   normal(0,0,0,0),
                   displNormal(0,0,0,0),
                   visited(false),
                   data(),
                   type(Mesh::Undefined){
    }

    /**************************************************************************/
    DCELHalfFace(int he, int t, int n):half_edge(he),
                                       twin(t),
                                       next(n),
                                       normal(0,0,0,0),
                                       displNormal(0,0,0,0),
                                       visited(false),
                                       data(),
                                       type(Mesh::Undefined){
    }

    /**************************************************************************/
    int half_edge;          /*A pointer to a half edge object that has
                              this face as a face object */
    int twin;               /*A pointer to the twin face of this face*/
    int next;               /*A pointer to the next face of a tetrahedron*/
    Vector4<T> normal;      /*Normal of the face*/
    Vector4<T> displNormal; /*Displaced normal of the face*/
    bool visited;           /*Temporary value*/
    FD data;                /*User defined data*/
    Mesh::GeometryType type;/*Geometry type*/

    /**************************************************************************/
    template<class FD2>
    DCELHalfFace<T, FD>& operator=(const DCELHalfFace<T, FD2>& f){
      if((void*)this != (void*)&f){
        half_edge   = f.half_edge;
        twin        = f.twin;
        next        = f.next;
        normal      = f.normal;
        displNormal = f.displNormal;
        type        = f.type;
        visited     = f.visited;
        /*Copy face specific data*/
        data        = FD();//f.data;
      }
      return *this;
    }

    /**************************************************************************/
    DCELHalfFace<T, FD>& operator=(const DCELHalfFace<T, FD>& f){
      if((void*)this != (void*)&f){
        half_edge   = f.half_edge;
        twin        = f.twin;
        next        = f.next;
        normal      = f.normal;
        displNormal = f.displNormal;
        type        = f.type;
        visited     = f.visited;
        /*Copy face specific data*/
        data        = f.data;
      }
      return *this;
    }

    /**************************************************************************/
    template<class FD2>
    DCELHalfFace(const DCELHalfFace<T, FD2>& f){
      *this = f;
    }

    /**************************************************************************/
    void print()const{
      message("HalfFace %p:: half_edge = %d, twin = %d", this, half_edge, twin);
    }
  };

  /**************************************************************************/
  template<class TD=char>
  class DCELTet{
  public:
    /**************************************************************************/
    DCELTet():half_face(Mesh::UndefinedIndex),
              data(),
              v1(Mesh::Undefined),
              v2(Mesh::Undefined),
              v3(Mesh::Undefined),
              v4(Mesh::Undefined),
              type(Mesh::Undefined){
    }

    /**************************************************************************/
    DCELTet(int hf):half_face(hf),
                    data(),
                    v1(Mesh::Undefined),
                    v2(Mesh::Undefined),
                    v3(Mesh::Undefined),
                    v4(Mesh::Undefined),
                    type(Mesh::Undefined){
    }

    int half_face;  /*A pointer to a half face object*/
    TD data;        /*User defined data*/
    int v1;         /*Indices to vertices 1, 2, 3, 4*/
    int v2;
    int v3;
    int v4;
    Mesh::GeometryType type; /*Geometry type*/

    /**************************************************************************/
    template<class TD2>
    DCELTet& operator=(const DCELTet<TD2>& t){
      if((void*)this != (void*)&t){
        half_face = t.half_face;
        v1        = t.v1;
        v2        = t.v2;
        v3        = t.v3;
        v4        = t.v4;
        type      = t.type;
        /*Copy tet specific data*/
        data      = t.data;
      }
      return *this;
    }

    /**************************************************************************/
    DCELTet& operator=(const DCELTet& t){
      if(this != &t){
        half_face = t.half_face;
        v1        = t.v1;
        v2        = t.v2;
        v3        = t.v3;
        v4        = t.v4;
        type      = t.type;
        /*Copy tet specific data*/
        data      = t.data;
      }
      return *this;
    }

    /**************************************************************************/
    template<class TD2>
    DCELTet(const DCELTet<TD2>& t){
      *this = t;
    }

    /**************************************************************************/
    void getIndices(int indices[4])const{
      indices[0] = v1;
      indices[1] = v2;
      indices[2] = v3;
      indices[3] = v4;
    }
  };

  /**************************************************************************/
  template<class T, class VD=char, class ED=char, class FD=char, class TD=char>
  class DCTetraMesh{
  public:
    /**************************************************************************/
    DCTetraMesh(){
      init(DEFAULT_SIZE);
    }

    /**************************************************************************/
    DCTetraMesh(int s){
      init(s);
    }

    /**************************************************************************/
    void init(int size){
      tetrahedra      = 0;
      halfFaces       = 0;
      halfEdges       = 0;
      vertices        = 0;

      n_tetrahedra    = 0;
      n_halfFaces     = 0;
      n_halfEdges     = 0;
      n_vertices      = 0;
      n_rigidObjects  = 0;

      size_tetrahedra = 0;
      size_halfFaces  = 0;
      size_halfEdges  = 0;
      size_vertices   = 0;

      currentEdge     = Mesh::UndefinedIndex;
      currentFace     = Mesh::UndefinedIndex;
      currentTet      = Mesh::UndefinedIndex;

      size_tetrahedra = size;
      size_halfFaces  = 4 * size;
      size_halfEdges  = 4 * 3 * size;
      size_vertices   = 4 * 3 * 3 * size;
      size_map        = size_vertices;

      tetrahedra = new DCELTet<TD>[size_tetrahedra];
      halfFaces  = new DCELHalfFace<T, FD>[size_halfFaces];
      halfEdges  = new DCELHalfEdge<ED>[size_halfEdges];
      vertices   = new DCELVertex<T, VD>[size_vertices];

      vertexObjectMap   = new int[size_vertices];
      centerOfMass      = new Vector4<T>[size_map];
      centerOfMassDispl = new Vector4<T>[size_map];
      orientations      = new Quaternion<T>[size_map];
      orientationsDispl = new Quaternion<T>[size_map];

      for(int i=0;i<size_vertices;i++){
        vertexObjectMap[i] = Mesh::UndefinedIndex;
      }

#ifdef USE_THREADS
      pthread_mutex_init(&accessMutex, 0);
#endif
      message("END %s", __FUNCTION__);
    }

    /**************************************************************************/
    virtual ~DCTetraMesh(){
      delete[] tetrahedra;
      delete[] halfFaces;
      delete[] halfEdges;
      delete[] vertices;

      delete[] vertexObjectMap;
      delete[] centerOfMass;
      delete[] centerOfMassDispl;
      delete[] orientations;
      delete[] orientationsDispl;

#ifdef USE_THREADS
      pthread_mutex_destroy(&accessMutex);
#endif
      message("END %s", __FUNCTION__);
    }

    /**************************************************************************/
    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh(const DCTetraMesh<VD2, ED2, FD2, TD2>& m){
      init(DEFAULT_SIZE);
      *this = m;
    }

    /**************************************************************************/
    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh(const DCTetraMesh<VD2, ED2, FD2, TD2>* m){
      init(DEFAULT_SIZE);
      merge(m);
    }

    /**************************************************************************/
    template<class VD2, class ED2, class FD2, class TD2>
    DCTetraMesh& operator=(const DCTetraMesh<VD2, ED2, FD2, TD2>& m){
      if((void*)&m != (void*)this){
        n_vertices   = 0;
        n_halfEdges  = 0;
        n_halfFaces  = 0;
        n_tetrahedra = 0;

        merge(&m);
      }
      return *this;
    }

    /**************************************************************************/
    void transform(Matrix44<T> mat){
      for(int i=0;i<n_vertices;i++){
        /*Make homogene coordinates*/
        vertices[i].coord[3] = (T)1.0;

        Vector4<T> res = mat * vertices[i].coord;

        vertices[i].coord = res;
        vertices[i].coord[3] = (T)0.0;
      }

      for(int i=0;i<n_rigidObjects;i++){
        /*Make homogene coordinates*/
        centerOfMass[i][3] = (T)1.0;

        Vector4<T> res = mat * centerOfMass[i];

        centerOfMass[i] = res;
        centerOfMass[i][3] = (T)0.0;
      }
    }

    /**************************************************************************/
    template<class VD2, class ED2, class FD2, class TD2>
    void merge(const DCTetraMesh<T, VD2, ED2, FD2, TD2>* m){
      int old_n_vertices      = n_vertices;
      int old_n_halfEdges     = n_halfEdges;
      int old_n_halfFaces     = n_halfFaces;
      int old_n_rigid_objects = n_rigidObjects;

      for(int i=0;i<m->n_vertices;i++){
        DCELVertex<T, VD> v = m->vertices[i];
        if(v.leaving != Mesh::UndefinedIndex) v.leaving += old_n_halfEdges;

        v.id += old_n_vertices;
        addVertex(v);

        if(m->vertexObjectMap[i] != Mesh::UndefinedIndex){
          vertexObjectMap[n_vertices-1] = (m->vertexObjectMap[i] +
                                           old_n_rigid_objects);
        }
      }

      for(int i=0;i<m->n_halfEdges;i++){
        DCELHalfEdge<ED> e = m->halfEdges[i];
        if(e.origin    != Mesh::UndefinedIndex) e.origin    += old_n_vertices;
        if(e.half_face != Mesh::UndefinedIndex) e.half_face += old_n_halfFaces;
        if(e.twin      != Mesh::UndefinedIndex) e.twin      += old_n_halfEdges;
        if(e.next      != Mesh::UndefinedIndex) e.next      += old_n_halfEdges;
        if(e.next_twin != Mesh::UndefinedIndex) e.next_twin += old_n_halfEdges;
        addHalfEdge(e);
      }

      for(int i=0;i<m->n_halfFaces;i++){
        DCELHalfFace<T, FD> f = m->halfFaces[i];
        if(f.half_edge != Mesh::UndefinedIndex) f.half_edge += old_n_halfEdges;
        if(f.twin      != Mesh::UndefinedIndex) f.twin      += old_n_halfFaces;
        if(f.next      != Mesh::UndefinedIndex) f.next      += old_n_halfFaces;
        addHalfFace(f);
      }

      for(int i=0;i<m->n_tetrahedra;i++){
        DCELTet<TD> t = m->tetrahedra[i];
        if(t.half_face != Mesh::UndefinedIndex) t.half_face += old_n_halfFaces;
        addTetrahedron(t);
      }

      /*Merge rigid data*/
      for(int i=0;i<m->n_rigidObjects;i++){
        int objectId = n_rigidObjects;
        addCenterOfMass(m->centerOfMass[i], 0);

        centerOfMass[objectId]      = m->centerOfMass[i];
        centerOfMassDispl[objectId] = m->centerOfMassDispl[i];
        orientations[objectId]      = m->orientations[i];
        orientationsDispl[objectId] = m->orientationsDispl[i];
      }
    }

    /**************************************************************************/
    void dumpVertices(){
      for(int i=0;i<n_vertices;i++){
        vertices[i].print();
      }
    }

  private:
    /**************************************************************************/
    void findAllConnectedFaces(int face,
                               Tree<int>* vertexFace,
                               Tree<int>* faceVertex,
                               List<int>* currentFaces,
                               Tree<int>* faces,
                               bool* visitedFaces,
                               bool* visitedVertices)const{
      if(visitedFaces[face] == true){
        return;
      }

      int lastCurrentEdge = currentEdge;

      visitedFaces[face] = true;
      faces->remove(face);

      currentFaces->append(face);

      //Visit all connected vertices
      setCurrentEdge(halfFaces[face].half_edge);

      for(int i=0;i<3;i++){
        int vertex = getOriginVertex();
        if(visitedVertices[vertex] == false){
          visitedVertices[vertex] = true;

          Tree<int> connectedFaces;
          getConnectedFacesOfVertex(vertex, connectedFaces);

          /*Iterate over all connected faces*/
          Tree<int>::Iterator fit = connectedFaces.begin();
          while(fit != connectedFaces.end()){
            int cface = *fit++;
            findAllConnectedFaces(cface, vertexFace, faceVertex, currentFaces,
                                  faces, visitedFaces, visitedVertices);
          }
        }
        nextEdge();
      }

      currentEdge = lastCurrentEdge;
    }

  public:
    /**************************************************************************/
    List<List<int> > getAllConnectedFaces()const{
      Tree<int>* vertexFaceTrees = new Tree<int>[n_vertices];
      Tree<int>* faceVertexTrees = new Tree<int>[n_halfFaces];
      Tree<int>  faces;

      bool* visitedVertices = new bool[n_vertices];
      bool* visitedFaces    = new bool[n_halfFaces];

      for(int i=0;i<n_vertices;i++){
        visitedVertices[i] = false;
      }

      for(int i=0;i<n_halfFaces;i++){
        setCurrentEdge(halfFaces[i].half_edge);

        for(int j=0;j<3;j++){
          int originVertex = getOriginVertex();
          vertexFaceTrees[originVertex].uniqueInsert(i);
          faceVertexTrees[i].uniqueInsert(originVertex);
          nextEdge();
        }
        faces.uniqueInsert(i);
        visitedFaces[i] = false;
      }

      int n_surfaces = 0;
      List<List<int> > surfaces;

      while(faces.size() != 0){
        Tree<int>::Iterator fit = faces.begin();
        int face = *fit++;

        List<int> currentFaces;

        findAllConnectedFaces(face, vertexFaceTrees, faceVertexTrees,
                              &currentFaces, &faces,
                              visitedFaces, visitedVertices);

        surfaces.append(currentFaces);
        n_surfaces++;
      }

      delete[] vertexFaceTrees;
      delete[] faceVertexTrees;

      delete[] visitedVertices;
      delete[] visitedFaces;

      return surfaces;
    }

    /**************************************************************************/
    void connectHalfEdges2(){
      std::map<int, std::list<int> > edgeMap;

      /*Store all half edges in a map, according to its origin.*/
      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;

        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          /*Get next vertex of this edge. That vertex is the origin
            vertex of the edge we are searching fo.*/
          int origin = getOriginVertex();
          int next_origin = getNextOriginVertex();

          std::list<int>::iterator it = edgeMap[next_origin].begin();

          /*Loop over all edges with 'next_origin' as origin vertex*/
          while(it != edgeMap[next_origin].end()){
            currentEdge = *it++;
            if(getNextOriginVertex() == origin){
              halfEdges[i].twin = currentEdge;
              halfEdges[currentEdge].twin = i;
              it = edgeMap[next_origin].end();
            }
          }
        }
      }

      bool inconsistent = false;

      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          message("Unconnected edge");
          halfEdges[i].print();
          inconsistent = true;
        }
      }

      if(inconsistent){
        error("Input mesh not consistent. Not able to connect half edges. Perhaps one triangle has an improper winding.");
      }
    }

    /**************************************************************************/
    /*If a polygon model is loaded, the half edges are mostly not
      connected. (the twins). This operation finds and connects
      them.*/
    void connectHalfEdges(){
      std::map<int, std::list<int> > edgeMap;

      /*Store all half edges in a map, according to its origin.*/
      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      /*Connect half-edges by searching for half-edges with the same
        origin as its next half-edge. This collection should contain
        one or none half-edge with a next origin which is the same as
        the origin of the initial half-edge.*/

      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;

        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          int origin = getOriginVertex();
          int next_origin = getNextOriginVertex();

          std::list<int>::iterator it = edgeMap[next_origin].begin();

          while(it != edgeMap[next_origin].end()){
            currentEdge = *it++;
            if(getNextOriginVertex() == origin){
              /*Found twin*/
              halfEdges[i].twin = currentEdge;
              halfEdges[currentEdge].twin = i;
              it = edgeMap[next_origin].end();
            }
          }
        }
        currentEdge = i;
      }

      /*Find half edges without a twin*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          /*Create a new half-edge*/
          DCELHalfEdge<ED> hedge;
          int hedge_index = addHalfEdge(hedge);

          currentEdge = i;
          halfEdges[hedge_index].origin    = getNextOriginVertex();
          halfEdges[hedge_index].half_face = Mesh::UndefinedIndex;
          halfEdges[hedge_index].twin      = i;
          halfEdges[hedge_index].next_twin = Mesh::UndefinedIndex;

          edgeMap[halfEdges[hedge_index].origin].push_back(hedge_index);

          halfEdges[i].twin = hedge_index;
        }
      }

      /*Check next pointers for new half-edges*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].next == Mesh::UndefinedIndex){
          currentEdge = i;
          int origin = getTwinOriginVertex();
          tslassert(getTwinEdge() != Mesh::UndefinedIndex);

          std::list<int>::iterator it = edgeMap[origin].begin();

          int count = 0;
          while(it != edgeMap[origin].end()){
            int edge = *it++;

            if(halfEdges[edge].half_face == Mesh::UndefinedIndex){
              count++;

              halfEdges[i].next = edge;
            }
          }
          tslassert(count == 1);
        }
      }

      /*Check previous pointers*/
      for(int i=0;i<n_halfEdges;i++){
        int next = halfEdges[i].next;
        currentEdge = next;
        tslassert(prevEdge() == i);
      }

      /*Check faces*/
      for(int i=0;i<n_halfEdges;i++){
        currentEdge = i;
        tslassert(getFace() != getTwinFace());
      }
    }

    /**************************************************************************/
    void computeFaceNormal(int f){
      int lastEdge = currentEdge;

      int edge = halfFaces[f].half_edge;
      currentEdge = edge;

      int vert[3];
      vert[0] = getOriginVertex(); nextEdge();
      vert[1] = getOriginVertex(); nextEdge();
      vert[2] = getOriginVertex();

      Vector4<T> coords[3];
      coords[0] = vertices[vert[0]].coord;
      coords[1] = vertices[vert[1]].coord;
      coords[2] = vertices[vert[2]].coord;

      coords[1] -= coords[0];
      coords[2] -= coords[0];

      Vector4<T> normal = cross(coords[1], coords[2]).normalized();

      halfFaces[f].normal = normal;

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void computeFaceNormalDispl(int f){
      int lastEdge = currentEdge;

      int edge = halfFaces[f].half_edge;
      currentEdge = edge;

      int vert[3];
      vert[0] = getOriginVertex(); nextEdge();
      vert[1] = getOriginVertex(); nextEdge();
      vert[2] = getOriginVertex();

      Vector4<T> coords[3];
      coords[0] = vertices[vert[0]].displCoord;
      coords[1] = vertices[vert[1]].displCoord;
      coords[2] = vertices[vert[2]].displCoord;

      coords[1] -= coords[0];
      coords[2] -= coords[0];

      Vector4<T> normal = cross(coords[1], coords[2]).normalized();

      halfFaces[f].displNormal = normal;

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void computeVertexNormal(int v){
      int lastEdge = currentEdge;

      int edge = vertices[v].leaving;
      currentEdge = edge;
      int faceCount = 0;

      Vector4<T> normal;

      while(true){
        int face = halfEdges[edge].half_face;

        if(face != Mesh::UndefinedIndex){
          normal += halfFaces[face].normal;
          faceCount++;
        }

        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }

      normal.normalize();

      vertices[v].normal = normal;

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void computeVertexNormalDispl(int v){
      int lastEdge = currentEdge;

      int edge = vertices[v].leaving;
      currentEdge = edge;
      int faceCount = 0;

      Vector4<T> normal;

      while(true){
        int face = halfEdges[edge].half_face;

        if(face != Mesh::UndefinedIndex){
          normal += halfFaces[face].displNormal;
          faceCount++;
        }

        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }

      normal.normalize();

      vertices[v].displNormal = normal;

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getEdgeIndices(int i, int* indices){
      int lastEdge = currentEdge;
      int edge = i;
      currentEdge = edge;
      indices[0]  = getOriginVertex();
      indices[1]  = getTwinOriginVertex();
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void computeDisplacement(Vector<T>* v, T dt){
      if(v == 0){
        for(int i=0;i<n_vertices;i++){
          vertices[i].displCoord    = vertices[i].coord;
          vertices[i].displCoordExt = vertices[i].coord;
        }
      }else{
        int n_static = 0;
        int n_deformable = 0;
        int n_cloth = 0;
        int n_rigid = 0;

        for(int i=0;i<n_vertices;i++){
          int index = vertices[i].vel_index;

          if(vertices[i].type == Mesh::Deformable){
            n_deformable++;
          }

          if(vertices[i].type == Mesh::Cloth){
            n_cloth++;
          }

          if(vertices[i].type == Mesh::Rigid){
            n_rigid++;
          }

          if(vertices[i].type == Mesh::Static){
            n_static++;
          }

          if(vertices[i].type == Mesh::Deformable ||
             vertices[i].type == Mesh::Cloth){
            /*Velocity defined*/
            Vector4<T> vel(*v, index, 3);

            if(IsNan(vel)){
              warning("NAN velocity");
              std::cout << *v ;
              std::cout << vel << std::endl;
              error("NAN");
            }

            vertices[i].displCoord    = vertices[i].coord + vel * dt;
            vertices[i].displCoordExt = vertices[i].coord + vel * dt*(T)DIST_C;
          }else if(vertices[i].type == Mesh::Static){
            /*Static vertex -> no displacement*/
            vertices[i].displCoord    = vertices[i].coord;
            vertices[i].displCoordExt = vertices[i].coord;
          }else if(vertices[i].type == Mesh::Rigid){
            //error("displacing rigid vertex");
            int rigidObject = vertexObjectMap[i];

            Vector4<T> com  = centerOfMass[rigidObject];
            Vector4<T> axis = vertices[i].coord - com;

            /*Load linear velocity*/
            Vector4<T> vel_lin(*v, index, 3);

            if(IsNan(vel_lin)){
              warning("NAN velocity");
              std::cout << vel_lin << std::endl;
              error("NAN");
            }

            centerOfMassDispl[rigidObject] = com + dt * vel_lin;

            /*Load angular velocity*/
            Vector4<T> vel_ang(*v, index + 3, 3);

            Quaternion<T> ang_vel_q(vel_ang[0], vel_ang[1], vel_ang[2], (T)0.0);
            Quaternion<T> qz;

            qz = (qz + (dt/(T)2.0) * (ang_vel_q * qz)).normalized();

            orientationsDispl[rigidObject] = qz;

            axis = qz * axis;

            Vector4<T> comExt = com + dt * vel_lin * (T)DIST_C;

            Quaternion<T> ang_vel_q_ext(vel_ang[0] * (T)DIST_C,
                                        vel_ang[1] * (T)DIST_C,
                                        vel_ang[2] * (T)DIST_C, (T)0.0);
            qz.setIdentity();

            qz = (qz + (dt/(T)2.0) * (ang_vel_q_ext * qz)).normalized();

            Vector4<T> axis_ext = vertices[i].coord - com;
            axis_ext = qz * axis_ext;

            vertices[i].displCoord    = axis + centerOfMassDispl[rigidObject];
            vertices[i].displCoordExt = axis_ext + comExt;
          }else if(vertices[i].type == Mesh::Undefined){
            error("Displacing undefined vertex type");
          }else{
            error("Displacing undefined vertex type");
          }
        }
      }
    }

    /**************************************************************************/
    /*Displace the mesh using the displaced vertices*/
    void acceptDisplacement(){
      for(int i=0;i<n_vertices;i++){
        vertices[i].coord = vertices[i].displCoord;
      }

      for(int i=0;i<n_rigidObjects;i++){
        centerOfMass[i] = centerOfMassDispl[i];
        orientationsDispl[i].setIdentity();
        orientations[i].setIdentity();
      }
    }

    /**************************************************************************/
    void computeFaceNormals(){
      for(int i=0;i<n_halfFaces;i++){
        computeFaceNormal(i);
      }
    }

    /**************************************************************************/
    void computeFaceNormalsDispl(){
      for(int i=0;i<n_halfFaces;i++){
        computeFaceNormalDispl(i);
      }
    }

    /**************************************************************************/
    void getDisplacedEdge(Edge<T>* e, int edge, bool backFace){
      int lastEdge = currentEdge;
      currentEdge = edge;

      e->setEdgeId(edge);

      if(backFace){
        e->setFaceId(0, getTwinFace());
        e->setFaceId(1, getFace());
      }else{
        e->setFaceId(0, getFace());
        e->setFaceId(1, getTwinFace());
      }

      Vector4<T> pos[4];

      int indices[4];
      indices[0] = getOriginVertex();
      indices[1] = getTwinOriginVertex();

      /*Find tangent indices*/
      nextEdge();
      nextEdge();
      indices[2] = getOriginVertex();

      currentEdge = edge;
      twinEdge();
      nextEdge();
      nextEdge();
      indices[3] = getOriginVertex();

      pos[0] = vertices[indices[0]].displCoord;
      pos[1] = vertices[indices[1]].displCoord;
      pos[2] = vertices[indices[2]].displCoord;
      pos[3] = vertices[indices[3]].displCoord;

      if(backFace){
        Vector4<T> tmp = pos[2];
        pos[2] = pos[3];
        pos[3] = tmp;

        int itmp = indices[2];
        indices[2] = indices[3];
        indices[3] = itmp;
      }

      e->setFaceVertexId(0, indices[2]);
      e->setFaceVertexId(1, indices[3]);

      e->set(pos[0], pos[1], pos[2], pos[3]);
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getEdge(Edge<T>* e, int edge, bool backFace){
      int lastEdge = currentEdge;
      currentEdge = edge;

      e->setEdgeId(edge);

      if(backFace){
        e->setFaceId(0, getTwinFace());
        e->setFaceId(1, getFace());
      }else{
        e->setFaceId(0, getFace());
        e->setFaceId(1, getTwinFace());
      }

      Vector4<T> pos[4];

      pos[0] = vertices[getOriginVertex()].coord;
      pos[1] = vertices[getTwinOriginVertex()].coord;
      nextEdge();
      nextEdge();
      pos[2] = vertices[getOriginVertex()].coord;
      int v1 = getOriginVertex();

      setCurrentEdge(edge);

      twinEdge();
      nextEdge();
      nextEdge();
      pos[3] = vertices[getOriginVertex()].coord;
      int v2 = getOriginVertex();

      if(backFace){
        Vector4<T> tmp = pos[2];
        pos[2] = pos[3];
        pos[3] = tmp;

        int itmp = v2;
        v2 = v1;
        v1 = itmp;
      }

      e->setFaceVertexId(0, v1);
      e->setFaceVertexId(1, v2);

      e->set(pos[0], pos[1], pos[2], pos[3]);
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getTriangleIndices(int i, int* indices, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[i].half_edge;
      currentEdge = edge;

      if(backFace){
        for(int i=0;i<3;i++){
          indices[i] = getOriginVertex();
          prevEdge();
        }
      }else{
        for(int i=0;i<3;i++){
          indices[i] = getOriginVertex();
          nextEdge();
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getDisplacedTriangle(Triangle<T>* t, int id, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      int indices[3];
      getTriangleIndices(id, indices, backFace);

      for(int i=0;i<3;i++){
        t->v[i] = vertices[indices[i]].displCoord;
      }

      currentEdge = lastEdge;
      t->computeNormal();
    }

    /**************************************************************************/
    void getDisplacedTriangleExt(Triangle<T>* t, int id, bool backFace){
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      int indices[3];
      getTriangleIndices(id, indices, backFace);

      for(int i=0;i<3;i++){
        t->v[i] = vertices[indices[i]].displCoordExt;
      }

      currentEdge = lastEdge;
      t->computeNormal();
    }

    /**************************************************************************/
    template<typename E>
    void getEntity(E* e, int id, bool backFace)const{
      error("Default getEntity called");
    }

    /**************************************************************************/
    /*Converts a halfFace to a triangle with vertices and a
      normal. Usefull for intersection computations*/
    void getTriangle(Triangle<T>* t, int id, bool backFace)const{
#ifdef USE_THREADS
      pthread_mutex_lock(&accessMutex);
#endif
      int lastEdge = currentEdge;
      int edge = halfFaces[id].half_edge;
      currentEdge = edge;

      if(backFace){
        for(int i=0;i<3;i++){
          t->v[i] = vertices[getOriginVertex()].coord;
          prevEdge();
        }
      }else{
        for(int i=0;i<3;i++){
          t->v[i] = vertices[getOriginVertex()].coord;
          nextEdge();
        }
      }

      currentEdge = lastEdge;

#ifdef USE_THREADS
      pthread_mutex_unlock(&accessMutex);
#endif

      /*Compute Normal can throw an exception, so do it after the lock
        in order to prevent a deadlock.*/
      t->computeNormal();
    }

    /**************************************************************************/
    void getEdgeIndicesOfTriangle(int f, int* indices)const{
      int lastEdge = currentEdge;

      currentEdge = halfFaces[f].half_edge;

      for(int i=0;i<3;i++){
        int edge = getCurrentEdge();
        int twin = getTwinEdge();

        if(edge < twin){
          indices[i] = edge;
        }else{
          indices[i] = twin;
        }

        nextEdge();
      }

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getOrientedVertex(OrientedVertex<T>* ov, int id, bool backFace){
      if(vertices[id].leaving == Mesh::UndefinedIndex){
        /*Internal vertex -> skip*/
        return;
      }

      int lastEdge = currentEdge;

      currentEdge = vertices[id].leaving;
      int edge = currentEdge;

      List<Vector4<T> > surroundingVertices;
      List<Vector4<T> > edgeNormals;
      List<int>         edgeIds;
      List<int>         vIds;

      if(backFace){
        while(true){
          computeFaceNormal(getFace());
          computeFaceNormal(getTwinFace());

          Vector4<T> edgeNormal =
            -(halfFaces[getFace()].normal +
              halfFaces[getTwinFace()].normal).normalized();

          twinEdge();

          int vertexId = getOriginVertex();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          surroundingVertices.append(vertices[getOriginVertex()].coord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }else{
        while(true){
          computeFaceNormal(getFace());
          computeFaceNormal(getTwinFace());

          Vector4<T> edgeNormal =
            (halfFaces[getFace()].normal +
             halfFaces[getTwinFace()].normal).normalized();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          nextEdge();

          int vertexId = getOriginVertex();

          surroundingVertices.append(vertices[getOriginVertex()].coord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();
          twinEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }
      currentEdge = lastEdge;

      computeVertexNormal(id);

      ov->set(vertices[id].coord, surroundingVertices, edgeNormals,
              edgeIds, vIds);
    }

    /**************************************************************************/
    void getDisplacedOrientedVertex(OrientedVertex<T>* ov, int id,
                                    bool backFace){
      if(vertices[id].leaving == Mesh::UndefinedIndex){
        /*Internal vertex -> skip*/
        return;
      }

      int lastEdge = currentEdge;

      currentEdge = vertices[id].leaving;
      int edge = currentEdge;

      List<Vector4<T> > surroundingVertices;
      List<Vector4<T> > edgeNormals;
      List<int>         edgeIds;
      List<int>         vIds;

      if(backFace){
        while(true){
          computeFaceNormalDispl(getFace());
          computeFaceNormalDispl(getTwinFace());

          Vector4<T> edgeNormal =
            -(halfFaces[getFace()].normal +
              halfFaces[getTwinFace()].normal).normalized();

          twinEdge();

          int vertexId = getOriginVertex();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          surroundingVertices.append(vertices[getOriginVertex()].displCoord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }else{
        while(true){
          computeFaceNormalDispl(getFace());
          computeFaceNormalDispl(getTwinFace());

          Vector4<T> edgeNormal =
            (halfFaces[getFace()].normal +
             halfFaces[getTwinFace()].normal).normalized();

          int edgeId =
            getTwinEdge() < getCurrentEdge() ? getTwinEdge() : getCurrentEdge();

          nextEdge();

          int vertexId = getOriginVertex();

          surroundingVertices.append(vertices[getOriginVertex()].displCoord);
          edgeNormals.append(edgeNormal);
          edgeIds.append(edgeId);
          vIds.append(vertexId);

          nextEdge();
          twinEdge();

          if(currentEdge == edge){
            break;
          }
        }
      }

      ov->set(vertices[id].displCoord, surroundingVertices,
              edgeNormals, edgeIds, vIds);

      currentEdge = lastEdge;
    }

    /**************************************************************************/
    int addCenterOfMass(const Vector4<T>& com, int id){
      int r = n_rigidObjects;

      if(n_rigidObjects == size_map){
        extendRigidObjects();
      }

      centerOfMass[n_rigidObjects] = com;
      orientations[n_rigidObjects].setIdentity();
      n_rigidObjects++;

      return r;
    }

    /**************************************************************************/
    int addVertex(DCELVertex<T, VD>& v){
      int r = n_vertices;

      if(n_vertices == size_vertices){
        extendVertices();
      }

      vertices[n_vertices] = v;
      n_vertices++;

      return r;
    }

    /**************************************************************************/
    int addHalfEdge(DCELHalfEdge<ED>& f){
      int r = n_halfEdges;

      if(n_halfEdges == size_halfEdges){
        extendHalfEdges();
      }

      halfEdges[n_halfEdges] = f;
      n_halfEdges++;

      return r;
    }

    /**************************************************************************/
    int addHalfFace(DCELHalfFace<T, FD>& f){
      int r = n_halfFaces;

      if(n_halfFaces == size_halfFaces){
        extendHalfFaces();
      }

      halfFaces[n_halfFaces] = f;
      n_halfFaces++;

      return r;
    }

    /**************************************************************************/
    int addTetrahedron(DCELTet<TD>& t){
      int r = n_tetrahedra;

      if(n_tetrahedra == size_tetrahedra){
        extendTetrahedra();
      }

      tetrahedra[n_tetrahedra] = t;
      n_tetrahedra++;

      return r;
    }

    /**************************************************************************/
    void removeObjectFromFace(int faceId){
      Tree<int> faceIds;
      Tree<int> vertexIds;
      Tree<int> edgeIds;

      List<int> faceStack;
      Tree<int> stackTree;

      faceStack.append(faceId);
      stackTree.uniqueInsert(faceId);

      while(faceStack.size() != 0){
        int cFace = *faceStack.begin();
        faceIds.uniqueInsert(cFace);

        setCurrentEdge(halfFaces[cFace].half_edge);

        for(int i=0;i<3;i++){
          int vertex = getOriginVertex();
          int edge = getCurrentEdge();
          edgeIds.uniqueInsert(edge);
          vertexIds.uniqueInsert(vertex);

          Tree<int>::Iterator it = stackTree.find(getTwinFace());

          if(it == stackTree.end()){
            /*Face not encountered before, add to stack*/
            int twinFace = getTwinFace();
            faceStack.append(twinFace);
            stackTree.uniqueInsert(twinFace);
          }else{
            /*Face encountered before, is on the stack and will be
              removed later*/
          }
          nextEdge();
        }
        faceStack.removeAt(0);
      }

      /*Invalidate removed geometry*/
      Tree<int>::Iterator it = faceIds.begin();
      while(it != faceIds.end()){
        int f = *it++;

        halfFaces[f] = DCELHalfFace<T, FD>();
      }

      it = vertexIds.begin();
      while(it != vertexIds.end()){
        int v = *it++;

        vertices[v] = DCELVertex<T, VD>();

        vertexObjectMap[v] = Mesh::UndefinedIndex;
      }

      it = edgeIds.begin();
      while(it != edgeIds.end()){
        int e = *it++;

        halfEdges[e] = DCELHalfEdge<ED>();
      }

      DCELCompact();
    }

    /**************************************************************************/
    void setInitial(){
      DCELVertex<T, VD> v[4] = {
#if 0
        DCELVertex<T, VD>(Vector4<T>(0,0,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(2,0,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(1,2,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(1,1,2,0), 1)
#else
#if 1
        DCELVertex<T, VD>(Vector4<T>(-20,-2,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(20,-2,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(0,40,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(-20,-2,-2,0), 1)
#else

        DCELVertex<T, VD>(Vector4<T>(-0.25,-0.5,0,0), 0),
        DCELVertex<T, VD>(Vector4<T>(0.25,-0.5,0,0), 2),
        DCELVertex<T, VD>(Vector4<T>(0,0.5,0,0), 6),
        DCELVertex<T, VD>(Vector4<T>(-0.25,-0.25,-2,0), 1)
#endif
#endif
      };

      addVertex(v[0]);
      addVertex(v[1]);
      addVertex(v[2]);
      addVertex(v[3]);

      /*HalfFaces, clockwise orientation.*/
      /*face 0: 0, 3, 1 n: 1 he: 0*/
      /*face 1: 1, 3, 2 n: 2 he: 3*/
      /*face 2: 2, 3, 0 n: 3 he: 6*/
      /*face 3: 1, 2, 0 n: 0 he: 9*/
      /*face twin: -1*/
      DCELHalfFace<T, FD> f[4] = {
        DCELHalfFace<T, FD>(0, Mesh::UndefinedIndex, 1),
        DCELHalfFace<T, FD>(3, Mesh::UndefinedIndex, 2),
        DCELHalfFace<T, FD>(6, Mesh::UndefinedIndex, 3),
        DCELHalfFace<T, FD>(9, Mesh::UndefinedIndex, 0)
      };

      addHalfFace(f[0]);
      addHalfFace(f[1]);
      addHalfFace(f[2]);
      addHalfFace(f[3]);

      /*edge 0:  0, 3 or: 0, hf 0, n: 1, nt:*/
      /*edge 1:  3, 1 or: 3, hf 0, n: 2*/
      /*edge 2:  1, 0 or: 1, hf 0, n: 0*/
      /*edge 3:  1, 3 or: 1, hf 1, n: 4*/
      /*edge 4:  3, 2 or: 3, hf 1, n: 5*/
      /*edge 5:  2, 1 or: 2, hf 1, n: 3*/
      /*edge 6:  2, 3 or: 2, hf 2, n: 7*/
      /*edge 7:  3, 0 or: 3, hf 2, n: 8*/
      /*edge 8:  0, 2 or: 0, hf 2, n: 6*/
      /*edge 9:  1, 2 or: 1, hf 3, n: 10*/
      /*edge 10: 2, 0 or: 2, hf 3, n: 11*/
      /*edge 11: 0, 1 or: 0, hf 3, n: 9*/
      DCELHalfEdge<ED> e[12] = {
        DCELHalfEdge<ED>(0, 0, Mesh::UndefinedIndex, 1,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 0, Mesh::UndefinedIndex, 2,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(1, 0, Mesh::UndefinedIndex, 0,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(1, 1, Mesh::UndefinedIndex, 4,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 1, Mesh::UndefinedIndex, 5,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(2, 1, Mesh::UndefinedIndex, 3,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(2, 2, Mesh::UndefinedIndex, 7,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(3, 2, Mesh::UndefinedIndex, 8,  Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(0, 2, Mesh::UndefinedIndex, 6,  Mesh::UndefinedIndex),

        DCELHalfEdge<ED>(1, 3, Mesh::UndefinedIndex, 10, Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(2, 3, Mesh::UndefinedIndex, 11, Mesh::UndefinedIndex),
        DCELHalfEdge<ED>(0, 3, Mesh::UndefinedIndex, 9,  Mesh::UndefinedIndex)
      };

      addHalfEdge(e[0]);
      addHalfEdge(e[1]);
      addHalfEdge(e[2]);
      addHalfEdge(e[3]);
      addHalfEdge(e[4]);
      addHalfEdge(e[5]);
      addHalfEdge(e[6]);
      addHalfEdge(e[7]);
      addHalfEdge(e[8]);
      addHalfEdge(e[9]);
      addHalfEdge(e[10]);
      addHalfEdge(e[11]);

      DCELTet<TD> t[1] = {
        DCELTet<TD>(0)
      };

      addTetrahedron(t[0]);

      connectHalfEdges();
    }

  public:
    /**************************************************************************/
    //DCTetraMesh(const DCTetraMesh&);
    //DCTetraMesh& operator=(const DCTetraMesh&);
    DCELTet<TD>*         tetrahedra;
    DCELHalfFace<T, FD>* halfFaces;
    DCELHalfEdge<ED>*    halfEdges;
    DCELVertex<T, VD>*   vertices;
    Vector4<T>*          centerOfMass;
    Vector4<T>*          centerOfMassDispl;
    Quaternion<T>*       orientations;
    Quaternion<T>*       orientationsDispl;
    int*                 vertexObjectMap;

    int n_tetrahedra;
    int n_halfFaces;
    int n_halfEdges;
    int n_vertices;
    int n_rigidObjects;

    int size_tetrahedra;
    int size_halfFaces;
    int size_halfEdges;
    int size_vertices;
    int size_map;

    mutable int currentEdge;
    mutable int currentFace;
    mutable int currentTet;

#ifdef USE_THREADS
    mutable pthread_mutex_t accessMutex;
#endif

    /**************************************************************************/
    int getNVertices()const{
      return n_vertices;
    }

    /**************************************************************************/
    int getNHalfEdges()const{
      return n_halfEdges;
    }

    /**************************************************************************/
    int getNHalfFaces()const{
      return n_halfFaces;
    }

    /**************************************************************************/
    int getNTetrahedra()const{
      return n_tetrahedra;
    }

    /**************************************************************************/
    void getTetrahedronCoords(const int tet, Matrix44<T>& coords)const{
      int indices[4];
      tetrahedra[tet].getIndices(indices);
      for(int i=0;i<4;i++){
        coords[i] = vertices[indices[i]].coord;
        coords[i][3] = (T)1.0;
      }
    }

    /**************************************************************************/
    void stats(){
      message("%d vertices", n_vertices);
      message("%d halfEdges", n_halfEdges);
      message("%d halfFaces", n_halfFaces);
      message("%d tetrahedra", n_tetrahedra);
    }

    /**************************************************************************/
    void extendRigidObjects(){
      Vector4<T>* tmp     = new Vector4<T>[size_map*2];
      Vector4<T>* tmp3    = new Vector4<T>[size_map*2];
      Quaternion<T>* tmp2 = new Quaternion<T>[size_map*2];
      Quaternion<T>* tmp4 = new Quaternion<T>[size_map*2];

      for(int i=0;i<n_rigidObjects;i++){
        tmp[i]  = centerOfMass[i];
        tmp3[i] = centerOfMassDispl[i];
        tmp2[i] = orientations[i];
        tmp4[i] = orientationsDispl[i];
      }

      //memcpy(tmp, tetrahedra, sizeof(DCELTet<TD>)*size_tetrahedra);
      size_map *= 2;

      delete[] centerOfMass;
      delete[] centerOfMassDispl;
      delete[] orientations;
      delete[] orientationsDispl;

      centerOfMass      = tmp;
      centerOfMassDispl = tmp3;
      orientations      = tmp2;
      orientationsDispl = tmp4;
    }

    /**************************************************************************/
    void extendTetrahedra(){
      DCELTet<TD>* tmp  = new DCELTet<TD>[size_tetrahedra*2];
      for(int i=0;i<n_tetrahedra;i++){
        tmp[i]  = tetrahedra[i];
      }
      size_tetrahedra *= 2;
      delete[] tetrahedra;
      tetrahedra   = tmp;
    }

    /**************************************************************************/
    void extendHalfFaces(){
      DCELHalfFace<T, FD>* tmp  = new DCELHalfFace<T, FD>[size_halfFaces*2];
      for(int i=0;i<n_halfFaces;i++){
        tmp[i]  = halfFaces[i];
      }
      size_halfFaces *= 2;
      delete[] halfFaces;
      halfFaces     = tmp;
    }

    /**************************************************************************/
    void extendHalfEdges(){
      DCELHalfEdge<ED>* tmp  = new DCELHalfEdge<ED>[size_halfEdges*2];
      for(int i=0;i<n_halfEdges;i++){
        tmp[i] = halfEdges[i];
      }
      size_halfEdges *= 2;
      delete[] halfEdges;
      halfEdges     = tmp;
    }

    /**************************************************************************/
    void extendVertices(){
      DCELVertex<T, VD>* tmp  = new DCELVertex<T, VD>[size_vertices*2];
      int*               tmp2 = new int[size_vertices*2];

      for(int i=0;i<size_vertices*2;i++){
        tmp[i]  = DCELVertex<T, VD>();
        tmp2[i] = -1;
      }

      for(int i=0;i<n_vertices;i++){
        tmp[i]  = vertices[i];
        tmp2[i] = vertexObjectMap[i];
      }

      size_vertices *= 2;

      delete[] vertices;
      delete[] vertexObjectMap;

      vertices        = tmp;
      vertexObjectMap = tmp2;
    }

    /**************************************************************************/
    inline void setCurrentEdge(int e)const{
      currentEdge = e;
    }

    /**************************************************************************/
    inline int getCurrentEdge()const{
      return currentEdge;
    }

    /**************************************************************************/
    inline int nextEdge()const{
      tslassert(currentEdge != Mesh::UndefinedIndex);
      currentEdge =  halfEdges[currentEdge].next;
      tslassert(currentEdge != Mesh::UndefinedIndex);
      return currentEdge;
    }

    /**************************************************************************/
    inline int twinEdge()const{
      tslassert(currentEdge != Mesh::UndefinedIndex);
      currentEdge = halfEdges[currentEdge].twin;
      tslassert(currentEdge != Mesh::UndefinedIndex);
      return currentEdge;
    }

    /**************************************************************************/
    inline int prevEdge()const{
      int cedge = currentEdge;
      nextEdge();
      int lastEdge = currentEdge;
      while(currentEdge != cedge){
        lastEdge = currentEdge;
        nextEdge();
      }
      currentEdge = lastEdge;
      return lastEdge;
    }

    /**************************************************************************/
    inline int getOriginVertex()const{
      return halfEdges[currentEdge].origin;
    }

    /**************************************************************************/
    inline int getNextEdge()const{
      return halfEdges[currentEdge].next;
    }

    /**************************************************************************/
    inline int getTwinEdge()const{
      return halfEdges[currentEdge].twin;
    }

    /**************************************************************************/
    inline int getTwinNextEdge()const{
      return halfEdges[getNextEdge()].twin;
    }

    /**************************************************************************/
    inline int getPrevEdge()const{
      int prev;
      int last = currentEdge;
      prev = prevEdge();
      currentEdge = last;
      return prev;
    }

    /**************************************************************************/
    inline int getPrevTwinEdge()const{
      return halfEdges[getPrevEdge()].twin;
    }

    /**************************************************************************/
    inline int getNextTwinEdge()const{
      return halfEdges[getNextEdge()].twin;
    }

    /**************************************************************************/
    inline int getFace()const{
      return halfEdges[currentEdge].half_face;
    }

    /**************************************************************************/
    inline int getTwinFace()const{
      if(getTwinEdge() == Mesh::UndefinedIndex){
        return Mesh::UndefinedIndex;
      }
      return halfEdges[getTwinEdge()].half_face;
    }

    /**************************************************************************/
    inline int getTwinOriginVertex()const{
      if(halfEdges[currentEdge].twin == Mesh::UndefinedIndex){
        return Mesh::UndefinedIndex;
      }
      return halfEdges[halfEdges[currentEdge].twin].origin;
    }

    /**************************************************************************/
    inline int getTwinTwinOriginVertex()const{
      return halfEdges[halfEdges[getTwinEdge()].twin].origin;
    }

    /**************************************************************************/
    inline int getNextOriginVertex()const{
      return halfEdges[getNextEdge()].origin;
    }

    /**************************************************************************/
    inline int getPrevOriginVertex()const{
      return halfEdges[getPrevEdge()].origin;
    }

    /**************************************************************************/
    inline Vector4<T>& getOriginCoord()const{
      return vertices[getOriginVertex()].coord;
    }

    /**************************************************************************/
    void getConnectedEdgesOfVertex(int vertex, Tree<int>& edges){
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        edges.uniqueInsert(currentEdge, currentEdge);
        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getSurroundingEdgesOfVertex(DCELVertex<T, VD>& v, List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        list.append(currentEdge);
        twinEdge();
        nextEdge();
        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getConnectedFacesOfVertex(int vertex, Tree<int>& faces)const{
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          int uface = face;
          faces.insert(uface, uface);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getConnectedFacesOfVertex(int vertex, List<int>& faces)const{
      int lastEdge = currentEdge;
      currentEdge  = vertices[vertex].leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          int uface = face;
          faces.append(uface);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getSurroundingFacesOfVertex(DCELVertex<T, VD>& v, List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        int face = halfEdges[currentEdge].half_face;
        if(face != Mesh::UndefinedIndex){
          list.append(face);
        }
        twinEdge();
        nextEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void getSurroundingVerticesOfVertex(DCELVertex<T, VD>& v, List<int>& list){
      int lastEdge = currentEdge;
      currentEdge  = v.leaving;
      int edge     = currentEdge;

      while(true){
        nextEdge();

        list.append(getOriginVertex());

        nextEdge();
        twinEdge();

        if(currentEdge == edge){
          break;
        }
      }
      currentEdge = lastEdge;
    }

    /**************************************************************************/
    void DCELComputeFaceArea(int f);

    /**************************************************************************/
    void DCELGetShortestEdgeOfFace(int f, int* e);

    /**************************************************************************/
    void DCELCheckFaceConsistency(int f);

    /**************************************************************************/
    void DCELCheckEdgeVertexConnectivity(int e, int v);

    /**************************************************************************/
    bool DCELCheckVertexVertexConnectivity(int v1,int v2);

    /**************************************************************************/
    void DCELDisplaySpanningVerticesOfEdge(int e);

    /**************************************************************************/
    void DCELGetConnectedVerticesOfVertex(int v, int* verts);

    /**************************************************************************/
    /*v3-v2 = edge for which all connected vertices must be in front*/
    /*v1-v2 = edge to collapse*/
    bool allVerticesInFront(int v3, int v2, int v1, List<int>& connected){
      /*Check projections*/

      /*Compute tangent vector*/
      Vector4<T> ea = (vertices[v1].coord - vertices[v2].coord);
      Vector4<T> eb = (vertices[v3].coord - vertices[v2].coord);

      /*Compute face normal*/
      Vector4<T> normal = cross(ea, eb).normalized();

      Vector4<T> edgeNormal = cross(eb, normal).normalized();
      /*Tangent vector, normal to edge*/

      bool allInFront = true;
      /*Check v3 with list1*/
      List<int>::Iterator it = connected.begin();
      while(it != connected.end()){
        int eee = *it++;

        if(eee == v2 || eee == v3){
          continue;
        }

        /*An edge between v1 and v1 is not allowed.*/
        tslassert(eee != v1);

        Vector4<T> vec = (vertices[v1].coord - vertices[v3].coord).normalized();

        tslassert(dot(vec, edgeNormal) >= 0);

        vec = vertices[eee].coord - vertices[v3].coord;

        if(dot(vec, edgeNormal) <= 1E-4){
          allInFront = false;
        }
      }
      return allInFront;
    }

    /**************************************************************************/
    void normalize(Vector4<T>& center, T& scale){
      BBox<T> box = getBBox();
      Vector4<T> diff = box.getMax() - box.getMin();
      scale = Max(diff[0], Max(diff[1], diff[2]));

      center = diff/(T)2.0 + box.getMin();

      scale += (T)1E-4;

      for(int i=0;i<n_vertices;i++){
        vertices[i].coord = (T)2.0*(vertices[i].coord - center)/scale;
      }
    }

    /**************************************************************************/
    BBox<T> getBBox(){
      BBox<T> box;

      for(int i=0;i<n_vertices;i++){
        Vector4<T> v = vertices[i].coord;

        box.addPoint(v);
      }

      return box;
    }

    /**************************************************************************/
    void DCELCompact(){
      /*Compact faces*/
      int currentFaceIndex = 0;
      for(int i=0;i<n_halfFaces;i++){
        if(halfFaces[i].half_edge == Mesh::UndefinedIndex){
          continue;
        }

        halfFaces[currentFaceIndex] = halfFaces[i];
        currentEdge = halfFaces[currentFaceIndex].half_edge;

        int edge = currentEdge;
        halfEdges[currentEdge].half_face = currentFaceIndex;
        nextEdge();
        while(currentEdge != edge){
          halfEdges[currentEdge].half_face = currentFaceIndex;
          nextEdge();
        }
        currentFaceIndex++;
      }

      n_halfFaces = currentFaceIndex;

      int currentEdgeIndex = 0;
      /*Compact edges*/
      for(int i=0;i<n_halfEdges;i++){
        if(halfEdges[i].twin == Mesh::UndefinedIndex){
          continue;
        }
        currentEdge = i;
        int prevEdge = getPrevEdge();

        /*Copy*/
        halfEdges[currentEdgeIndex] = halfEdges[i];

        /*half_edge of face*/
        if(halfEdges[currentEdgeIndex].half_face != Mesh::UndefinedIndex){
          halfFaces[halfEdges[currentEdgeIndex].half_face].half_edge =
            currentEdgeIndex;
        }

        /*Twin*/
        halfEdges[halfEdges[currentEdgeIndex].twin].twin = currentEdgeIndex;
        /*Next of previous edge*/
        halfEdges[prevEdge].next = currentEdgeIndex;

        /*Leaving of origin vertex*/
        vertices[halfEdges[currentEdgeIndex].origin].leaving = currentEdgeIndex;
        currentEdgeIndex++;
      }

      n_halfEdges = currentEdgeIndex;

      /*Compact vertices*/
      std::map<int, std::list<int> > edgeMap;

      for(int i=0;i<n_halfEdges;i++){
        edgeMap[halfEdges[i].origin].push_back(i);
      }

      int currentVertexIndex = 0;
      for(int i=0;i<n_vertices;i++){
        if(edgeMap[i].size() == 0){
          continue;
        }

        vertices[currentVertexIndex] = vertices[i];
        vertices[currentVertexIndex].id = currentVertexIndex;

        vertexObjectMap[currentVertexIndex] = vertexObjectMap[i];

        std::list<int>::iterator it = edgeMap[i].begin();

        while(it != edgeMap[i].end()){
          int edge = *it++;

          halfEdges[edge].origin = currentVertexIndex;
        }
        currentVertexIndex++;
      }

      n_vertices = currentVertexIndex;
    }

    /**************************************************************************/
    void dumpFace(int f){
      setCurrentEdge(halfFaces[f].half_edge);
      int startEdge = currentEdge;

      std::cout << "face[" << f << "] = ";

      while(true){
        std::cout << getOriginVertex() << ", ";
        nextEdge();
        if(currentEdge == startEdge){
          std::cout << std::endl;
          return;
        }
      }
    }

    /**************************************************************************/
    /*Computes the centers of mass for each rigid object*/
    void computeCentersOfMass(){
      for(int i=0;i<n_rigidObjects;i++){
        centerOfMass[i].set((T)0.0, (T)0.0, (T)0.0, (T)0.0);

        T totalVolume = (T)0.0;

        for(int j=0;j<n_halfFaces;j++){
          setCurrentEdge(halfFaces[j].half_edge);
          int v = getOriginVertex();

          if(vertexObjectMap[v] == i){
            /*Vertex of face j belongs to object i*/

            Vector4<T> va = vertices[v].coord;
            nextEdge();
            Vector4<T> vb = vertices[getOriginVertex()].coord;
            nextEdge();
            Vector4<T> vc = vertices[getOriginVertex()].coord;

            Vector4<T> vd((T)0.0, (T)0.0, (T)0.0, (T)0.0);

            nextEdge();

            if(v != getOriginVertex()){
              error("vertex not equal to begin vertex");
            }

            Matrix44d tet(va, vb, vc, vd);
            tet[0][3] = tet[1][3] = tet[2][3] = tet[3][3] = (T)1.0;

            double volume = tet.det() / (T)6.0;
            totalVolume += (T)volume;

            centerOfMass[i] += (T)volume*(va + vb + vc + vd) * (T)0.25;
          }
        }
        centerOfMass[i] /= (T)totalVolume;
      }
    }

    /**************************************************************************/
    void saveMesh(std::ofstream& out){
      if(out.is_open()){
        out.precision(10);
        out << std::scientific;

        /*Write n vertices*/
        out << getNVertices() << std::endl;

        /*Write n edges*/
        out << getNHalfEdges() << std::endl;

        /*Write n faces*/
        out << getNHalfFaces() << std::endl;

        for(int i=0;i<getNVertices();i++){
          out << vertices[i].coord[0] << "\t" <<
            vertices[i].coord[1] << "\t" <<
            vertices[i].coord[2] << std::endl;
        }

        for(int i=0;i<getNHalfEdges();i++){
          int twin = halfEdges[i].twin;
          out<< halfEdges[i].origin << "\t" << halfEdges[twin].origin <<
            std::endl;
        }

        for(int i=0;i<getNHalfFaces();i++){
          setCurrentEdge(halfFaces[i].half_edge);

          out << getOriginVertex();
          nextEdge();
          out << "\t" << getOriginVertex();
          nextEdge();
          out << "\t" << getOriginVertex();

          setCurrentEdge(halfFaces[i].half_edge);

          int cedge = getCurrentEdge();
          int tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge);

          nextEdge();

          cedge = getCurrentEdge();
          tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge);

          nextEdge();

          cedge = getCurrentEdge();
          tedge = getTwinEdge();

          out << "\t" << (cedge<tedge?cedge:tedge) << std::endl;
        }
      }
    }

    /**************************************************************************/
    template<class U, class V, class W, class X>
    friend void convertMesh(DCTetraMesh<U, V, W, X>, DCTetraMesh<U>* s);
  };

  /**************************************************************************/
  template<class T, class U, class V, class W,
           class TT, class UU, class VV, class WW>
  void convertMesh(DCTetraMesh<T, U, V, W>* r,
                   DCTetraMesh<TT, UU, VV, WW>* s){
#if 0
    for(int i=0;i<s->getNVertices();i++){
      DCELVertex<T> v;
      v.coord   = s->vertices[i].coord;
      v.leaving = s->vertices[i].leaving;
      v.normal  = s->vertices[i].normal;
      v.id      = s->vertices[i].id;
      r->addVertex(v);
    }

    for(int i=0;i<s->getNHalfEdges();i++){
      DCELHalfEdge<U> e;
      e.origin    = s->halfEdges[i].origin;
      e.half_face = s->halfEdges[i].half_face;
      e.twin      = s->halfEdges[i].twin;
      e.next      = s->halfEdges[i].next;
      e.next_twin = s->halfEdges[i].next_twin;
      r->addHalfEdge(e);
    }

    for(int i=0;i<s->getNHalfFaces();i++){
      DCELHalfFace<T, V> f;
      f.half_edge = s->halfFaces[i].half_edge;
      f.twin      = s->halfFaces[i].twin;
      f.next      = s->halfFaces[i].next;
      f.normal    = s->halfFaces[i].normal;
      r->addHalfFace(f);
    }

    for(int i=0;i<s->getNTetrahedra();i++){
      DCELTet<W> t;
      t.half_face = s->tetrahedra[i].half_face;
      r->addTetrahedron(t);
    }
    r->currentEdge = 0;
    r->currentFace = 0;
    r->currentTet  = 0;
#else
    *r = *s;
#endif
  }
}

#endif/*DCELIST_HPP*/
