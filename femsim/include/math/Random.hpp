/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef RANDOM_HPP
#define RANDOM_HPP

#include "math/Math.hpp"
#include "core/tsldefs.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Random{
  public:
    /**************************************************************************/
    Random();

    /**************************************************************************/
    ~Random();

    /**************************************************************************/
    void seed(ulong seed);

    /**************************************************************************/
    T genRand();
  protected:
    /**************************************************************************/
    long unsigned int genRandInt();
    /**************************************************************************/
    ulong* mt;
    int mti;
  };

  /**************************************************************************/
  static Random<float> randomFloat;
  static Random<double> randomDouble;

  template<class T>
  T genRand();
}

#endif/*RANDOM_HPP*/
