/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef RAY_HPP
#define RAY_HPP

#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Ray{
  public:
    /**************************************************************************/
    Ray(){
    }

    /**************************************************************************/
    Ray(const Vector4<T>& a,
        const Vector4<T>& b){
      origin = a;
      setDirection(b-a);
    }

    /**************************************************************************/
    void setOrigin(const Vector4<T>& a){
      origin = a;
    }

    /**************************************************************************/
    void setDirection(const Vector4<T>& a){
      dir = a;
      for(int i=0;i<4;i++){
        inv_dir[i] = (T)1.0/dir[i];
        signs[i] = (inv_dir[i] < 0);
      }
    }

    /**************************************************************************/
    template<class TT>
    friend class BBox;

    /**************************************************************************/
    const Vector4<T>& getOrigin()const{
      return origin;
    }

    /**************************************************************************/
    const Vector4<T>& getDirection()const{
      return dir;
    }

  protected:
    /**************************************************************************/
    Vector4<T> origin;
    Vector4<T> dir;
    Vector4<T> inv_dir;
    int signs[4];
  };
}


#endif/*RAY_HPP*/
