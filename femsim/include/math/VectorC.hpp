/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef VECTORC_HPP
#define VECTORC_HPP

#include "math/Vector.hpp"
#include "math/SpMatrixC.hpp"
#include "datastructures/Tree.hpp"
#include "math/constraints/Constraint.hpp"

namespace tsl{

  template<class T>
  class VectorC : public Vector<T>{
  public:
    /**************************************************************************/
    VectorC(int size=0):Vector<T>(size){
      constrVec = new Vector<T>(1); /*Default size*/
    }

    /**************************************************************************/
    VectorC(const VectorC<T>& v):Vector<T>(v){
      constrVec = new Vector<T>(*v.constrVec);
    }

    /**************************************************************************/
    virtual ~VectorC(){
      if(constrVec){
        delete constrVec;
      }
    }

    /**************************************************************************/
    /*Assignment operators*/
    VectorC<T>& operator=(const VectorC<T>& v){
      if(this == &v){
        /*Self assignment*/
        return *this;
      }else{
        Vector<T>::operator=(v);
        (*constrVec) = (*v.constrVec);
      }
      return *this;
    }

    /**************************************************************************/
    T getAbsMax()const{
      return Max(Vector<T>::getAbsMax(), constrVec->getAbsMax());
    }

    /**************************************************************************/
    T getOAbsMax()const{
      return Vector<T>::getAbsMax();
    }

    /**************************************************************************/
    T getCAbsMax()const{
      return constrVec->getAbsMax();
    }

    /**************************************************************************/
    T oSum() const{
      return Vector<T>::sum();
    }

    /**************************************************************************/
    T cSum() const{
      return constrVec->sum();
    }

    /**************************************************************************/
    T sum() const{
      return oSum() + cSum();
    }

    /**************************************************************************/
    T sump(const VectorRange r)const{
      error("Not implemented for enriched vectors");
      return Vector<T>::sump(r);
    }

    /**************************************************************************/
    T& operator[](int i){
      if(i < this->getSize()){
        return Vector<T>::operator[](i);
      }

      int index = i - this->getSize();

      return (*constrVec)[index];
    }

    /**************************************************************************/
    const T operator[](int i)const{
      if(i < this->getSize()){
        return Vector<T>::operator[](i);
      }

      int index = i - this->getSize();

      return (*constrVec)[index];
    }

    /**************************************************************************/
    void clear(VectorOpMode mode = Normal){
      if(mode == Normal || mode == NoConstraints){
        Vector<T>::clear();
      }

      if(mode == Normal || mode == Constraints){
        constrVec->clear();
      }
    }

    /**************************************************************************/
    void truncate(){
      for(int i=0;i<constrVec->getSize();i++){
        if((*constrVec)[i] < 0){
          (*constrVec)[i]  = 0;
        }
      }
    }

    /**************************************************************************/
    void reset(){
      constrVec->clear();
    }

    /**************************************************************************/
    T* getExtendedData(){
      return constrVec->getData();
    }

    /**************************************************************************/
    const T* getExtendedData() const{
      return constrVec->getData();
    }

    /**************************************************************************/
    const Vector<T>& getExtendedVector()const{
      return *constrVec;
    }

    /**************************************************************************/
    Vector<T>& getExtendedVector(){
      return *constrVec;
    }

    /**************************************************************************/
    /*
    VectorC<T>& operator=(const T f){
      Vector<T>::operator=(f);
      (*constrVec) = f;

      return *this;
      }*/

    /**************************************************************************/
    VectorC<T>& operator*=(T n){
      VectorC<T>::mulf(*this, *this, n);
      return *this;
    }

    /**************************************************************************/
    VectorC<T>& operator/=(T n){
      VectorC<T>::mulf(*this, *this, (T)1.0/n);
      return *this;
    }

    /**************************************************************************/
    VectorC<T>& operator+=(const VectorC<T>& v){
      VectorC<T>::add(*this, *this, v);
      return *this;
    }

    /**************************************************************************/
    VectorC<T>& operator-=(const VectorC<T>& v){
      VectorC<T>::sub(*this, *this, v);
      return *this;
    }

    /**************************************************************************/
    VectorC<T>& operator*=(const VectorC<T>& v){
      VectorC<T>::mul(*this, *this, v);
      return *this;
    }

    /**************************************************************************/
    VectorC<T>& operator/=(const VectorC<T>& v){
      VectorC<T>::div(*this, *this, v);
      return *this;
    }

    /**************************************************************************/
    /*Unary*/
    VectorC<T> operator+() const{
      VectorC<T> r(*this);
      return r;
    }

    /**************************************************************************/
    VectorC<T> operator-() const{
      VectorC<T> r(*this);
      r *= -1.0f;
      return r;
    }

    /**************************************************************************/
    void printConstraints(std::ostream& os)const;

    /**************************************************************************/
    /*Vector vector*/
    VectorC<T> operator+(const VectorC<T>& v) const{
      tslassert(this->getSize() == v.getSize());
      VectorC<T> r(*this);
      r += v;
      return r;
    }

    /**************************************************************************/
    VectorC<T> operator-(const VectorC<T>& v) const{
      tslassert(this->getSize() == v.getSize());
      VectorC<T> r(*this);
      r -= v;
      return r;
    }

    /**************************************************************************/
    template<class U>
    friend VectorC<U> operator*(const VectorC<U>& a,
                                U n);

    /**************************************************************************/
    template<class U>
    friend VectorC<U> operator*(U n,
                                const VectorC<U>& a);

    /**************************************************************************/
    template<class U>
    friend VectorC<U> operator/(const VectorC<U>& a,
                                U n);

    /**************************************************************************/
    /*Dot product*/
    T operator*(const VectorC<T>& v) const{
      T a = Vector<T>::operator*(v);
      T b = (*constrVec) * (*v.constrVec);
      return a + b;
    }

    /**************************************************************************/
    /*Vector matrix multiplication*/
    template<int M, class U>
    friend VectorC<U> operator*(const SpMatrixC<M, U>& m,
                                const VectorC<U>& v);

    /**************************************************************************/
    template<int M, class U>
    friend void spmvc(VectorC<U>& r,
                      const SpMatrixC<M, U>& m,
                      const VectorC<U>& v,
                      const MatrixMulMode mode,
                      const VectorC<U>* mask);

    /**************************************************************************/
    template<int M, class U>
    friend void spmv_partialc(VectorC<U>& r,
                              const SpMatrixC<M, U>& m,
                              const VectorC<U>& v,
                              const MatrixRange mr,
                              const MatrixMulMode mode,
                              const VectorC<U>* mask);

    /**************************************************************************/
    template<class U>
    friend std::ostream& operator<<(std::ostream& os,
                                    const VectorC<U>& v);

    /**************************************************************************/
    static void sub  (VectorC<T>& r,
                      const VectorC<T>& a,
                      const VectorC<T>& b,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void add  (VectorC<T>& r,
                      const VectorC<T>& a,
                      const VectorC<T>& b,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void madd (VectorC<T>& r,
                      const VectorC<T>& a,
                      const VectorC<T>& b,
                      const VectorC<T>& c,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mfadd(VectorC<T>& r,
                      T f,
                      const VectorC<T>& b,
                      const VectorC<T>& c,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mfadd2(VectorC<T>& r,
                       T f,
                       const VectorC<T>& b,
                       const VectorC<T>& c,
                       const VectorC<T>& d,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mul  (VectorC<T>& r,
                      const VectorC<T>& a,
                      const VectorC<T>& b,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mulf (VectorC<T>& r,
                      const VectorC<T>& a,
                      T f,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void div  (VectorC<T>& r,
                      const VectorC<T>& a,
                      const VectorC<T>& b,
                      const VectorOpMode mode = Normal);

    /**************************************************************************/
    /*Partial operations*/
    static void subp  (VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void addp  (VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void maddp (VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorC<T>& c,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mfaddp(VectorC<T>& r, T f,
                       const VectorC<T>& b,
                       const VectorC<T>& c,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mfadd2p(VectorC<T>& r, T f,
                        const VectorC<T>& b,
                        const VectorC<T>& c,
                        const VectorC<T>& d,
                        const VectorRange rg,
                        const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mulp  (VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    static void mulfp (VectorC<T>& r,
                       const VectorC<T>& a,
                       T f,
                       const VectorRange rg,
                       const VectorOpMode mode = Normal);

    /**************************************************************************/
    void extendConstraints(int csize){
      if(csize <= constrVec->getSize()){
        return;
      }

      Vector<T>* tmp = new Vector<T>(csize);
      tmp->clear();

      memcpy(tmp->getData(), constrVec->getData(),
             sizeof(T)*(unsigned long int)constrVec->getSize());

      delete constrVec;

      constrVec = tmp;
    }

    Vector<T>* constrVec;
  };

#define VECTOR_C_CV
  /**************************************************************************/
  template<class T>
  VectorC<T> operator*(const VectorC<T>& a,
                       T n){
    VectorC<T> r = a;
    r *= n;
    return r;
  }

  /**************************************************************************/
  template<class T>
  VectorC<T> operator*(T n,
                       const VectorC<T>& a){
    VectorC<T> r = a;
    r *= n;
    return r;
  }

  /**************************************************************************/
  template<class T>
  VectorC<T> operator/(const VectorC<T>& a,
                       T n){
    VectorC<T> r = a;
    error("Called?? original function did multiplication");
    r /= n;
    return r;
  }

#if 1
  /**************************************************************************/
  template<int N, class T>
  VectorC<T> operator*(const SpMatrixC<N, T>& m,
                       const VectorC<T>& v){
    if(!m.isFinalized()){
      error("Matrix is not finalized");
    }

    VectorC<T> r(v);
    r *= 0;

    spmvc(r, m, v);

    return r;
  }
#endif

  /**************************************************************************/
  template<class T>
  std::ostream& operator<<(std::ostream& stream,
                           const VectorC<T>& v){
    stream << "vector size = " << v.getPaddedSize() << " " << v.getSize() << "," << v.constrVec->getSize() << std::endl;
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    for(int i=0;i<v.getSize();i++){
      stream << i << "\t" << v.data[i] << "\n";
    }

    stream << "------------ Constraint part ------------" << std::endl;

    for(int i=0;i<v.constrVec->getSize();i++){
      stream << i+v.getSize() << "\t" << (*v.constrVec)[i] << "\n";
    }

    stream << std::endl;
    stream.flags(origflags);
    return stream;
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::printConstraints(std::ostream& stream)const{
    stream << "vector size = " << this->getPaddedSize() << " " << this->getSize() << "," << this->constrVec->getSize() << std::endl;
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    //for(int i=0;i<v.getSize();i++){
    //stream << i << "\t" << v.data[i] << "\n";
    //}

    stream << "------------ Constraint part ------------" << std::endl;

    for(int i=0;i<constrVec->getSize();i++){
      stream << i+this->getSize() << "\t" << (*constrVec)[i] << "\n";
    }

    stream << std::endl;
    stream.flags(origflags);
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::sub(VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::sub(r, a, b);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::sub(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::add(VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::add(r, a, b);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::add(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::madd (VectorC<T>& r,
                         const VectorC<T>& a,
                         const VectorC<T>& b,
                         const VectorC<T>& c,
                         const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::madd(r, a, b, c);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::madd(*r.constrVec, *a.constrVec, *b.constrVec, *c.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mfadd(VectorC<T>& r,
                         T f,
                         const VectorC<T>& b,
                         const VectorC<T>& c,
                         const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mfadd(r, f, b, c);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mfadd(*r.constrVec, f, *b.constrVec, *c.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mfadd2(VectorC<T>& r,
                          T f,
                          const VectorC<T>& b,
                          const VectorC<T>& c,
                          const VectorC<T>& d,
                          const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mfadd2(r, f, b, c, d);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mfadd2(*r.constrVec, f, *b.constrVec,
                        *c.constrVec, *d.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mul(VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mul(r, a, b);
    }
    if(mode == Normal || mode == Constraints){
      Vector<T>::mul(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mulf(VectorC<T>& r,
                        const VectorC<T>& a,
                        T f,
                        const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::mulf(r, a, f);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::mulf(*r.constrVec, *a.constrVec, f);
    }
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::div(VectorC<T>& r,
                       const VectorC<T>& a,
                       const VectorC<T>& b,
                       const VectorOpMode mode){
#ifdef VECTOR_C_CV
    if(mode != Normal){
      if(mode == Constraints){
        r.clear(NoConstraints);
      }else{
        r.clear(Constraints);
      }
    }
#endif

    if(mode == Normal || mode == NoConstraints){
      Vector<T>::div(r, a, b);
    }

    if(mode == Normal || mode == Constraints){
      Vector<T>::div(*r.constrVec, *a.constrVec, *b.constrVec);
    }
  }

  /**************************************************************************/
  /*Partial operations*/
  template<class T>
  void VectorC<T>::subp(VectorC<T>& r,
                        const VectorC<T>& a,
                        const VectorC<T>& b,
                        const VectorRange rg,
                        const VectorOpMode mode){
    Vector<T>::subp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::addp(VectorC<T>& r,
                        const VectorC<T>& a,
                        const VectorC<T>& b,
                        const VectorRange rg,
                        const VectorOpMode mode){
    Vector<T>::addp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::maddp(VectorC<T>& r,
                         const VectorC<T>& a,
                         const VectorC<T>& b,
                         const VectorC<T>& c,
                         const VectorRange rg,
                         const VectorOpMode mode){
    Vector<T>::maddp(r, a, b, c, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mfaddp(VectorC<T>& r,
                          T f,
                          const VectorC<T>& b,
                          const VectorC<T>& c,
                          const VectorRange rg,
                          const VectorOpMode mode){
    Vector<T>::mfaddp(r, f, b, c, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mfadd2p(VectorC<T>& r,
                           T f,
                           const VectorC<T>& b,
                           const VectorC<T>& c,
                           const VectorC<T>& d,
                           const VectorRange rg,
                           const VectorOpMode mode){
    Vector<T>::mfadd2p(r, f, b, c, d, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mulp(VectorC<T>& r,
                        const VectorC<T>& a,
                        const VectorC<T>& b,
                        const VectorRange rg,
                        const VectorOpMode mode){
    Vector<T>::mulp(r, a, b, rg);
    error("No parallel support for enriched vectors");
  }

  /**************************************************************************/
  template<class T>
  void VectorC<T>::mulfp(VectorC<T>& r,
                         const VectorC<T>& a,
                         T f,
                         const VectorRange rg,
                         const VectorOpMode mode){
    Vector<T>::mulfp(r, a, f, rg);
    error("No parallel support for enriched vectors");
  }
}

#endif/*VECTORC_HPP*/
