/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SPMATRIXC_HPP
#define SPMATRIXC_HPP

#include "math/constraints/AbstractMatrixConstraint.hpp"
#include "math/constraints/Constraint.hpp"
#include "math/SpMatrix.hpp"
#include "math/Vector.hpp"
#include "datastructures/Tree.hpp"
#include "datastructures/DynamicArray.hpp"
#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"
#include "math/VectorC.hpp"
#include "math/Compare.hpp"

#include "core/BenchmarkTimer.hpp"
#include "core/Timer.hpp"
//#include "math/matrix_loaders.hpp"

/*An extension of the sparse matrix in which constraints can be
  updated dynamically*/

namespace tsl{
  /**************************************************************************/
  template<int N, class T=float>
  class SpMatrixC : public SpMatrix<N, T>{
  public:
    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolve;

    /**************************************************************************/
    template<int M, class TT, class CC>
    friend class IECLinSolveCR;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolveGS;

    /**************************************************************************/
    template<class TT>
    friend class VectorC;

    /**************************************************************************/
    template<class TT, class CC>
    friend class ConstraintCandidate;

    /**************************************************************************/
    template<class TT, class CC>
    friend class ConstraintCandidateEdge;

    /**************************************************************************/
    template<class TT, class CC>
    friend class ConstraintSet;

    /**************************************************************************/
    template<class TT, class CC>
    friend class ConstraintSetEdge;

    /**************************************************************************/
    /*Constructors*/
    SpMatrixC():SpMatrix<N, T>(), status(Individual), constraints(10){
      activeConstraints = 0;
    }

    /**************************************************************************/
    SpMatrixC(int width, int height):SpMatrix<N, T>(width, height),
      status(Individual), constraints(10){
      activeConstraints = 0;
    }

    /**************************************************************************/
    SpMatrixC(const SpMatrix<N, T>& m):SpMatrix<N,T>(m),
      status(Individual), constraints(10){
      activeConstraints = 0;
    }

    /**************************************************************************/
    /*Copy constructor*/
    SpMatrixC(const SpMatrixC<N, T>& m):SpMatrix<N,T>(m),
      status(Individual), constraints(m.constraints){
      if(&m != this){
        activeConstraints = m.activeConstraints;
      }
    }

    /**************************************************************************/
    /*Destructor*/
    virtual ~SpMatrixC(){
      /*Delete constraints*/
      for(int i=0; i<constraints.getNActive();i++){
        int idx = constraints.activeIndex(i);
        delete constraints[idx];
        constraints[idx] = 0;
      }
    }

    /**************************************************************************/
    /*Assignment operators*/
    SpMatrixC<N, T> operator=(const SpMatrixC<N, T>& m){
      if(this == &m){
        /*Self assignment*/
      }else{
        SpMatrix<N, T>::operator=(m);
        constraints = m.constraints;
      }
      return *this;
    }

    /**************************************************************************/
    SpMatrixC<N, T> operator=(const SpMatrix<N, T>& m){
      if(this == &m){
        /*Self assignment*/
        return *this;
      }else{
        SpMatrix<N, T>::operator=(m);
        constraints.reset();
      }
      return *this;
    }

    /**************************************************************************/
    int addConstraint(AbstractMatrixConstraint<N, T>* c/*, int column*/){
      //c->index = column;
      PRINT_FUNCTION;

      START_DEBUG;
#if 0
      message("dump constraints");

      for(int i=0;i<constraints.getNActive();i++){
        int index = constraints.activeIndex(i);

        message("[%d] = %p", i, constraints[index]);
      }
#endif
      constraints.print();

      message("this = %p, constraint = %p, array = %p", this, c, &constraints);
      message("pre activate size = %d", constraints.getNActive());
      END_DEBUG;

      c->row_id = constraints.getFirstFreeItem(0);

      c->status = Active;
      constraints[c->row_id] = c;
      constraints.activate(c->row_id);

      c->row_id_given = constraints.reverseIndex(c->row_id);

      activateConstraint(c->row_id);

      START_DEBUG;
      constraints.print();

      message("post activate size = %d", constraints.getNActive());

      message("row_id = %d", c->row_id);
      END_DEBUG;

      c->init(this);

      //int r = constraints.activeIndex(c->row_id);

#if 0
      message("%d constraints", constraints.getNActive());
      message("r = %d", c->row_id);


      getchar();
#endif
      START_DEBUG;
      c->print(std::cout);
      message("end matrix add constraint, row_id = %d -- %d", c->row_id,
              constraints.reverseIndex(c->row_id));
      END_DEBUG;

      return c->row_id;
    }

    /**************************************************************************/
    void updateConstraint(int id,
                          T vc,
                          int column){
      //int idx = constraints.activeIndex(id);
      error("used?");
      //constraints[id].c = vc;
      //constraints[id].index = column;
    }

    /**************************************************************************/
    bool isActiveConstraint(int i){
      //int idx = constraints.activeIndex(i);
      if(constraints[i]->status == Active){
        return true;
      }
      return false;
    }

    /**************************************************************************/
    T getConstraintValue(int i){
      error("used??");
      //int index = constraints.activeIndex(i);
      //return constraints[index]->c;
      return 0.0;
    }

    /**************************************************************************/
    int getConstraintIndex(int i,
                           int j){
      error("used???");
      return -1;
      //int index = constraints.activeIndex(i);
      //return constraints[index]->index[j];
    }

    /**************************************************************************/
    void removeConstraint(int id){
      DBG(message("removing constraint %d", id));
      DBG(constraints.print());
      deactivateConstraint(id);
      //id = constraints.reverseIndex(id);
      constraints[id] = 0;
      constraints.deactivate(id);
      DBG(constraints.print());
    }

    /**************************************************************************/
    void activateConstraint(int id){
      //int index = constraints.reverseIndex(id);
      constraints[id]->status = Active;
      activeConstraints->uniqueInsert(id, id);
    }

    /**************************************************************************/
    void deactivateConstraint(int id){
      //int index = constraints.reverseIndex(id);
      //constraints[index]->status = Inactive;
      constraints[id]->status = Inactive;
      activeConstraints->remove(id);
    }

    /**************************************************************************/
    int getNConstraints() const{
      return constraints.getNActive();
    }

    /**************************************************************************/
    void deactivateAllConstraints(){
      status = NoneActive;
    }

    /**************************************************************************/
    void activateAllConstraints(){
      status = AllActive;
    }

    /**************************************************************************/
    void individualActivation(){
      status = Individual;
    }

    /**************************************************************************/
    void reset(){
      //SpMatrix<N, T>::clear();
      constraints.reset();
    }

    /**************************************************************************/
    void listConstraints(){
#if 0
      PRINT_FUNCTION;
      for(int i=0;i<constraints.getNActive();i++){
        int idx = constraints.activeIndex(i);
        bool active = false;
        if(constraints[idx]->status == Active){
          active = true;
        }
        message("Constraint %d, active = %d", i, active);
        std::cout << constraints[idx];
      }
#endif
    }

    /**************************************************************************/
    void extractConstraintMatrices(SpMatrix<N, T>& L,
                                   SpMatrix<N, T>& LT,
                                   Tree<int>& columns,
                                   Vector<T>* rhs = 0){

      int nConstraints = getNConstraints();

      for(int i=0;i<nConstraints;i++){
        AbstractMatrixConstraint<N, T>* c = getActiveConstraint(i);
        c->extractValues(&L, &LT, &columns, i, rhs);
      }
    }

    /**************************************************************************/
    /*Overloaded functions*/
    ConstraintMatrixStatus getStatus()const{
      return status;
    }

    /**************************************************************************/
    template<int M, class TT>
    friend void spmvc(VectorC<TT>& r,
                      const SpMatrixC<M, TT>& m,
                      const VectorC<TT>& v,
                      const MatrixMulMode mode,
                      const VectorC<TT>* mask);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmvc_t(VectorC<TT>& r,
                        const SpMatrixC<M, TT>& m,
                        const VectorC<TT>& v,
                        const MatrixMulMode mode,
                        const VectorC<TT>* mask);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv_parallelc(VectorC<TT>& r,
                               const SpMatrixC<M, TT>& m,
                               const VectorC<TT>& v,
                               const MatrixMulMode mode,
                               const VectorC<TT>* mask);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv_partialc(VectorC<TT>& r,
                              const SpMatrixC<M, TT>& m,
                              const VectorC<TT>& v,
                              const MatrixRange rg,
                              const MatrixMulMode mode,
                              const VectorC<TT>* mask);

    /**************************************************************************/
    /*Conversion routines*/
    template<int M, int MM, class TPA, class TPB>
    friend void convertMatrix(SpMatrixC<M, TPA> & a,
                              const SpMatrix<MM, TPB>& b);

    /**************************************************************************/
    template<int M, int MM, class TPA, class TPB>
    friend void convertMatrix(SpMatrix<M, TPA> & a,
                              const SpMatrixC<MM, TPB>& b);

    /**************************************************************************/
    template<int M, class TT>
    friend bool save_matrix_matlab(const char* filename,
                                   const SpMatrixC<M, TT>* const mat);

    /**************************************************************************/
    template<int M, class TT>
    friend void save_matrix_market_exchange(const char* filename,
                                            const SpMatrixC<M, TT>* const mat);

    /**************************************************************************/
    //protected:
    AbstractMatrixConstraint<N, T>* getConstraint(int index){
      //int index = constraints.activeIndex(i);
      return constraints[index];
    }

    /**************************************************************************/
    const AbstractMatrixConstraint<N, T>* getConstraint(int index)const{
      //int index = constraints.activeIndex(i);
      return constraints[index];
    }

    /**************************************************************************/
    AbstractMatrixConstraint<N, T>* getActiveConstraint(int index){
      index = constraints.activeIndex(index);
      return constraints[index];
    }

    /**************************************************************************/
    const AbstractMatrixConstraint<N, T>* getActiveConstraint(int index)const{
      index = constraints.activeIndex(index);
      return constraints[index];
    }
    ConstraintMatrixStatus status;
    DynamicArray<AbstractMatrixConstraint<N, T>* > constraints;
    Tree<int>* activeConstraints;

    /*Used to store intermedite values from the constraints*/
  };

  /**************************************************************************/
  template<int M, class TT>
  void spmvc(VectorC<TT>& r,
             const SpMatrixC<M, TT>& m,
             const VectorC<TT>& v,
             const MatrixMulMode mode = Full,
             const VectorC<TT>* mask = 0);

  /**************************************************************************/
  template<int M, class TT>
  void spmvc_t(VectorC<TT>& r,
               const SpMatrixC<M, TT>& m,
               const VectorC<TT>& v,
               const MatrixMulMode mode = Full,
               const VectorC<TT>* mask = 0);

  /**************************************************************************/
  template<int M, class TT>
  void spmv_parallelc(VectorC<TT>& r,
                      const SpMatrixC<M, TT>& m,
                      const VectorC<TT>& v,
                      const MatrixMulMode mode = Full,
                      const VectorC<TT>* mask = 0);

  /**************************************************************************/
  template<int M, class TT>
  void spmv_partialc(VectorC<TT>& r,
                     const SpMatrixC<M, TT>& m,
                     const VectorC<TT>& v,
                     const MatrixRange rg,
                     const MatrixMulMode mode = Full,
                     const VectorC<TT>* mask = 0);

  /**************************************************************************/
  extern BenchmarkTimer* spmv_timer;

  /**************************************************************************/
  template<int N, class T>
  void conventionalSpmv(VectorC<T>& r,
                        const SpMatrixC<N, T>& m,
                        const VectorC<T>& v){
    if(m.n_blocks != 0){
      spmv_timer->start("spmv_conventional");

      spmv(r, m, v);

      spmv_timer->stop("spmv_conventional");
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void constraintSpmv(VectorC<T>& r,
                      const SpMatrixC<N, T>& m,
                      const VectorC<T>& v,
                      const MatrixMulMode mode,
                      const VectorC<T>* mask=0){
    int nConstraints = m.getNConstraints();
    ConstraintMatrixStatus status = m.getStatus();

    if(nConstraints == 0){
      return;
    }

    /*Call pre-multiply*/
    const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(0);

    spmv_timer->start("spmv_constr_pre");
    c->preMultiply(r, v, &m, false);
    spmv_timer->stop("spmv_constr_pre");

    spmv_timer->start("spmv_constr");

    for(int i=0; i<nConstraints; i++){
      const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(i);

      bool process = false;

      if(status == AllActive ||
         mode == FullActive ||
         mode == ConstraintsFullActive){
        process = true;
      }else if(status == Individual){
        if(c->status == Active){
          process = true;
        }else{
          process = false;
        }
      }

      if(process){
        if(c->getType() == rowConstraint){
          if(mode == ConstraintsCol        ||
             mode == ConstraintsFull       ||
             mode == ConstraintsFullActive ||
             mode == Full                  ||
             mode == FullActive){
            /*Multiply as column constraint*/
            c->multiplyTransposed(r, v);
          }

          if(mode == ConstraintsRow        ||
             mode == ConstraintsFull       ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as row constraint*/
            c->multiply(r, v);
          }
        }else if(c->getType() == blockConstraint){
          if(mode == Full || mode == FullActive){
            c->multiply(r, v);
          }
        }
      }
    }
    spmv_timer->stop("spmv_constr");
  }

  /**************************************************************************/
  template<int N, class T>
  void spmvc(VectorC<T>& r,
             const SpMatrixC<N, T>& m,
             const VectorC<T>& v,
             const MatrixMulMode mode,
             const VectorC<T>* mask){
    tslassert(m.finalized == true);

    r.clear();

    if(mode == Conventional || mode == Full || mode == FullActive){
      conventionalSpmv(r, m, v);
    }

    if(mode != Conventional || mode == ConstraintsFullActive ||
       mode == ConstraintsRow || mode == ConstraintsCol ||
       mode == ConstraintsFull){
      constraintSpmv(r, m, v, mode);
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void conventionalSpmvT(VectorC<T>& r,
                         const SpMatrixC<N, T>& m,
                         const VectorC<T>& v){
    if(m.n_blocks != 0){
      spmv_timer->start("spmv_conventional");

      spmv_t(r, m, v);

      spmv_timer->stop("spmv_conventional");
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void constraintSpmvT(VectorC<T>& r,
                       const SpMatrixC<N, T>& m,
                       const VectorC<T>& v,
                       const MatrixMulMode mode){
    int nConstraints = m.getNConstraints();
    ConstraintMatrixStatus status = m.getStatus();

    if(nConstraints == 0){
      return;
    }

    /*Call pre-multiply*/
    const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(0);

    spmv_timer->start("spmv_constr_pre");
    c->preMultiply(r, v, &m, true); /**/
    spmv_timer->stop("spmv_constr_pre");

    spmv_timer->start("spmv_constr");

    for(int i=0;i<nConstraints; i++){
      const AbstractMatrixConstraint<N, T>* c = m.getActiveConstraint(i);
      bool process = false;

      if(status == AllActive ||
         mode == FullActive ||
         mode == ConstraintsFullActive){
        process = true;
      }else if(status == Individual){
        if(c->status == Active){
          process = true;
        }else{
          process = false;
        }
      }
      if(process){
        if(c->getType() == rowConstraint){
          if(mode == ConstraintsCol  ||
             mode == ConstraintsFull ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as column constraint*/
            c->multiply(r, v);
          }

          if(mode == ConstraintsRow  ||
             mode == ConstraintsFull ||
             mode == ConstraintsFullActive ||
             mode == Full ||
             mode == FullActive){
            /*Multiply as row constraint*/
            c->multiplyTransposed(r, v);
          }
        }else if(c->getType() == blockConstraint){
          if(mode == Full ||
             mode == FullActive){
            c->multiplyTransposed(r, v);
          }
        }
      }
    }
    spmv_timer->stop("spmv_constr");
  }

  /**************************************************************************/
  template<int N, class T>
  void spmvc_t(VectorC<T>& r,
               const SpMatrixC<N, T>& m,
               const VectorC<T>& v,
               const MatrixMulMode mode,
               const VectorC<T>* mask){
    tslassert(m.finalized == true);

    r.clear();

    if(mode == Conventional || mode == Full || mode == FullActive){
      conventionalSpmvT(r, m, v);
    }


    if(mode != Conventional || mode == ConstraintsFullActive ||
       mode == ConstraintsRow || mode == ConstraintsCol ||
       mode == ConstraintsFull){
      constraintSpmvT(r, m, v, mode);
    }
  }
}


#endif/*SPMATRIXC_HPP*/
