/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MATRIX22F_HPP
#define MATRIX22F_HPP

#include <string.h>
#include "math/Vector2.hpp"
#include "math/Matrix44.hpp"


namespace tsl{
  /**************************************************************************/
  template<class T>
  class Matrix22{
  public:
    /**************************************************************************/
    Matrix22(){
      vector4_load_zero(m);
    }

    /**************************************************************************/
    Matrix22(T a,
             T b,
             T c,
             T d){
      vector4_load(m, a, b, c, d);
    }

    /**************************************************************************/
    Matrix22(const Matrix44<T>& mat){
      vector4_load(m,
                   mat[0][0], mat[0][1],
                   mat[1][0], mat[1][1]);
    }

    /**************************************************************************/
    Matrix22(const T a[4]){
      vector4_load(m, a);
    }

    /**************************************************************************/
    T det()const{
      return (m[0] * m[3]) - (m[1] * m[2]);
    }

    /**************************************************************************/
    static Matrix22<T> identity(){
      Matrix22<T> r;
      r.m[0] = MultiplyIdentity(T());
      r.m[3] = MultiplyIdentity(T());
      return r;
    }

    /**************************************************************************/
    Matrix22<T> inverse(T* determinant=0)const{
      Matrix22<T> r;

      T d = det();
      if(determinant)*determinant = d;

      d = MultiplyIdentity(T())/d;

      r.m[0] =  m[3] * d;
      r.m[1] = -m[1] * d;
      r.m[2] = -m[2] * d;
      r.m[3] =  m[0] * d;
      return r;
    }

    /**************************************************************************/
    Matrix22<T> transpose()const{
      return Matrix22<T>(m[0], m[2], m[1], m[3]);
    }

    /**************************************************************************/
    Matrix22<T>& operator+=(const Matrix22<T>& w){
      vector4_add(m, m, w.m);
      return *this;
    }

    /**************************************************************************/
    Matrix22<T>& operator-=(const Matrix22<T>& w){
      vector4_sub(m, m, w.m);
      return *this;
    }

    /**************************************************************************/
    Matrix22<T> operator+()const{
      return *this;
    }

    /**************************************************************************/
    Matrix22<T> operator+(const Matrix22<T>& w)const{
      Matrix22<T> r = *this;
      r += w;
      return r;
    }

    /**************************************************************************/
    Matrix22<T> operator-(const Matrix22<T>& w)const{
      Matrix22<T> r = *this;
      r -= w;
      return r;
    }

    /**************************************************************************/
    Vector2<T> operator*(const Vector2<T>& x)const{
      return Vector2<T>(m[0] * x.m[0] + m[1] * x.m[1],
                        m[2] * x.m[0] + m[3] * x.m[1]);
    }

#if 0
    /**************************************************************************/
    /*Multiply with vector4, ignoring the last two components of the
      vector*/
    Vector2<T> operator*(const Vector4<T>& x)const{
      error("called?");
      return Vector2<T>(m[0] * x.m[0] + m[1] * x.m[1],
                        m[2] * x.m[0] + m[3] * x.m[1]);
    }
#endif

    /**************************************************************************/
    Matrix22<T> operator*(const Matrix22<T>& a)const{
      return Matrix22<T>(m[0]*a.m[0] + m[1]*a.m[2],
                         m[0]*a.m[1] + m[1]*a.m[3],
                         m[2]*a.m[0] + m[3]*a.m[2],
                         m[2]*a.m[1] + m[3]*a.m[3]);
    }

    /**************************************************************************/
    Matrix22<T> operator*(const T x){
      Matrix22<T> r;
      vector4_muls(r.m, m, x);
      return r;
    }

    /**************************************************************************/
    Matrix22<T>& operator*=(const T x){
      vector4_muls(m, m, x);
      return *this;
    }

    /**************************************************************************/
    T* operator[](int i){
      return &(m[i*2]);
    }

    /**************************************************************************/
    const T* operator[](int i)const{
      return &(m[i*2]);
    }

    /**************************************************************************/
    template<class Y>
    friend Matrix22<Y> operator*(const Y x,
                                 const Matrix22<Y>& m);

    /**************************************************************************/
    template<class Y>
    friend Matrix22<Y> operator*(const Matrix22<Y>& m,
                                 const Y x);

#ifndef __ANDROID__
    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Matrix22<Y>& m);
#endif
  public:
#ifdef _WIN32
	__declspec(align(16)) T m[4];
#else
    T m[4] __attribute__ ((aligned (16)));
#endif
  };

  /**************************************************************************/
  typedef Matrix22<float> Matrix22f;
  typedef Matrix22<double> Matrix22d;

  /**************************************************************************/
  template<class T>
  inline Matrix22<T> MultiplyIdentity(const Matrix22<T>& ){
    return Matrix22<T>::identity();
  }

  /**************************************************************************/
  template<class T>
  inline Matrix22<T> SumIdentity(const Matrix22<T>& ){
    return Matrix22<T>();
  }

  /**************************************************************************/
  template<class T>
  inline Matrix22<T> operator*(const T x,
                               const Matrix22<T>& m){
    Matrix22<T> r = m;
    r *= x;
    return r;
  }

  /**************************************************************************/
  template<class T>
  inline Matrix22<T> operator*(const Matrix22<T>& m,
                               const T x){
    Matrix22<T> r = m;
    r *= x;
    return r;
  }

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const Matrix22<T>& d){
    for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
        if(IsNan(d[i][j])){
          return true;
        }
      }
    }
    return false;
  }

#ifndef __ANDROID__
  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os, const Matrix22<T>& m){
    os << std::endl << m.m[0] << "\t" << m.m[1] << std::endl;
    os << m.m[2] << "\t" << m.m[3] << std::endl;
    return os;
  }
#endif
}

#endif/*MATRIX*/
