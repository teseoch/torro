/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SSE2_MATRIX_INTRINSICS
#define SSE2_MATRIX_INTRINSICS

#include "core/tsldefs.hpp"

#include <xmmintrin.h>

#ifdef _WIN32
#include <emmintrin.h>
#define _mm_set_pd1 _mm_set1_pd
#endif

#include <string.h>

#include "math/default/default_matrix_intrinsics.hpp"

namespace tsl{
  namespace x86_sse2{
    /**************************************************************************/
    template<class T>
    inline void matrix44_vmadd(T ra[4], T rb[4],
                               T rc[4], T rd[4],
                               const T a1[4], const T b1[4],
                               const T c1[4], const T d1[4],
                               const T a2[4], const T b2[4],
                               const T c2[4], const T d2[4],
                               const T x[4]){
      default_proc::matrix44_vmadd(ra, rb, rc, rd,
                                   a1, b1, c1, d1,
                                   a2, b2, c2, d2, x);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_vmadd(float ra[4], float rb[4],
                               float rc[4], float rd[4],
                               const float a1[4], const float b1[4],
                               const float c1[4], const float d1[4],
                               const float a2[4], const float b2[4],
                               const float c2[4], const float d2[4],
                               const float x[4]){
#if 1
      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(a1, 16));
      tslassert(alignment(b1, 16));
      tslassert(alignment(c1, 16));
      tslassert(alignment(d1, 16));
      tslassert(alignment(a2, 16));
      tslassert(alignment(b2, 16));
      tslassert(alignment(c2, 16));
      tslassert(alignment(d2, 16));
      tslassert(alignment(x,  16));

      __m128 XMM0 = _mm_load_ps(a1);
      __m128 XMM1 = _mm_load_ps(b1);
      __m128 XMM2 = _mm_load_ps(c1);
      __m128 XMM3 = _mm_load_ps(d1);

      __m128 XMM4 = _mm_load_ps(a2);
      __m128 XMM5 = _mm_load_ps(b2);
      __m128 XMM6 = _mm_load_ps(c2);
      __m128 XMM7 = _mm_load_ps(d2);

      __m128 XMMX = _mm_load_ps(x);

      _mm_store_ps(ra, _mm_add_ps(XMM0, _mm_mul_ps(XMM4, XMMX)));
      _mm_store_ps(rb, _mm_add_ps(XMM1, _mm_mul_ps(XMM5, XMMX)));
      _mm_store_ps(rc, _mm_add_ps(XMM2, _mm_mul_ps(XMM6, XMMX)));
      _mm_store_ps(rd, _mm_add_ps(XMM3, _mm_mul_ps(XMM7, XMMX)));
#else
      default_proc::matrix44_vmadd(ra, rb, rc, rd,
                                   a1, b1, c1, d1,
                                   a2, b2, c2, d2, x);

#endif

    }

    /**************************************************************************/
    template<>
    inline void matrix44_vmadd(double ra[4], double rb[4],
                               double rc[4], double rd[4],
                               const double a1[4], const double b1[4],
                               const double c1[4], const double d1[4],
                               const double a2[4], const double b2[4],
                               const double c2[4], const double d2[4],
                               const double x[4]){
#if 1
      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(a1, 16));
      tslassert(alignment(b1, 16));
      tslassert(alignment(c1, 16));
      tslassert(alignment(d1, 16));
      tslassert(alignment(a2, 16));
      tslassert(alignment(b2, 16));
      tslassert(alignment(c2, 16));
      tslassert(alignment(d2, 16));
      tslassert(alignment(x,  16));

      __m128d XMM00 = _mm_load_pd(a1+0);
      __m128d XMM01 = _mm_load_pd(a1+2);
      __m128d XMM10 = _mm_load_pd(b1+0);
      __m128d XMM11 = _mm_load_pd(b1+2);
      __m128d XMM20 = _mm_load_pd(c1+0);
      __m128d XMM21 = _mm_load_pd(c1+2);
      __m128d XMM30 = _mm_load_pd(d1+0);
      __m128d XMM31 = _mm_load_pd(d1+2);

      __m128d XMM40 = _mm_load_pd(a2+0);
      __m128d XMM41 = _mm_load_pd(a2+2);
      __m128d XMM50 = _mm_load_pd(b2+0);
      __m128d XMM51 = _mm_load_pd(b2+2);
      __m128d XMM60 = _mm_load_pd(c2+0);
      __m128d XMM61 = _mm_load_pd(c2+2);
      __m128d XMM70 = _mm_load_pd(d2+0);
      __m128d XMM71 = _mm_load_pd(d2+2);

      __m128d XMMX0 = _mm_load_pd(x+0);
      __m128d XMMX1 = _mm_load_pd(x+2);

      _mm_store_pd(ra+0, _mm_add_pd(XMM00, _mm_mul_pd(XMM40, XMMX0)));
      _mm_store_pd(ra+2, _mm_add_pd(XMM01, _mm_mul_pd(XMM41, XMMX1)));
      _mm_store_pd(rb+0, _mm_add_pd(XMM10, _mm_mul_pd(XMM50, XMMX0)));
      _mm_store_pd(rb+2, _mm_add_pd(XMM11, _mm_mul_pd(XMM51, XMMX1)));
      _mm_store_pd(rc+0, _mm_add_pd(XMM20, _mm_mul_pd(XMM60, XMMX0)));
      _mm_store_pd(rc+2, _mm_add_pd(XMM21, _mm_mul_pd(XMM61, XMMX1)));
      _mm_store_pd(rd+0, _mm_add_pd(XMM30, _mm_mul_pd(XMM70, XMMX0)));
      _mm_store_pd(rd+2, _mm_add_pd(XMM31, _mm_mul_pd(XMM71, XMMX1)));
#else
      default_proc::matrix44_vmadd(ra, rb, rc, rd,
                                   a1, b1, c1, d1,
                                   a2, b2, c2, d2, x);
#endif
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_mul_vector4(T r[4],
                                     const T a[4], const T b[4],
                                     const T c[4], const T d[4],
                                     const T x[4]){
      default_proc::matrix44_mul_vector4(r,
                                         a, b, c, d,
                                         x);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_mul_vector4(float r[4],
                                     const float a[4], const float b[4],
                                     const float c[4], const float d[4],
                                     const float x[4]){


      //default_proc::matrix44_mul_vector4(r, a, b, c, d, x);
#if 1
      __m128 XMM0 = _mm_set_ps(d[0], c[0], b[0], a[0]);
      __m128 XMM1 = _mm_set_ps(d[1], c[1], b[1], a[1]);
      __m128 XMM2 = _mm_set_ps(d[2], c[2], b[2], a[2]);
      __m128 XMM3 = _mm_set_ps(d[3], c[3], b[3], a[3]);

      __m128 XMMX0 = _mm_set_ps1(x[0]);
      __m128 XMMX1 = _mm_set_ps1(x[1]);
      __m128 XMMX2 = _mm_set_ps1(x[2]);
      __m128 XMMX3 = _mm_set_ps1(x[3]);

      XMM0 = _mm_mul_ps(XMM0, XMMX0);
      XMM1 = _mm_mul_ps(XMM1, XMMX1);
      XMM2 = _mm_mul_ps(XMM2, XMMX2);
      XMM3 = _mm_mul_ps(XMM3, XMMX3);

      _mm_store_ps(r, _mm_add_ps(_mm_add_ps(XMM0, XMM1),
                                 _mm_add_ps(XMM2, XMM3)));

#endif
#if 0
      __m128 XMM0 = _mm_load_ps(a);
      __m128 XMM1 = _mm_load_ps(b);
      __m128 XMM2 = _mm_load_ps(c);
      __m128 XMM3 = _mm_load_ps(d);

      __m128 XMM4 = _mm_load_ps(x);

      XMM0 = _mm_mul_ps(XMM0, XMM4); //a b c d
      XMM1 = _mm_mul_ps(XMM1, XMM4); //e f g h
      XMM2 = _mm_mul_ps(XMM2, XMM4); //i j k l
      XMM3 = _mm_mul_ps(XMM3, XMM4); //m n o p

      /*Transpose result*/
      XMM4 = _mm_shuffle_ps(XMM0, XMM1, _MM_SHUFFLE(2,0,2,0));       //aceg
      __m128 XMM5 = _mm_shuffle_ps(XMM1, XMM0, _MM_SHUFFLE(3,1,1,3));//hfbd
      __m128 XMM6 = _mm_shuffle_ps(XMM3, XMM2, _MM_SHUFFLE(0,2,2,0));//moki
      __m128 XMM7 = _mm_shuffle_ps(XMM2, XMM3, _MM_SHUFFLE(3,1,3,1));//jlnp

      __m128 XMM8 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(0,3,2,0)); //aeim
      __m128 XMM9 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(2,0,1,2)); //bfjn
      __m128 XMM10 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(1,2,3,1));//cgko
      __m128 XMM11 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(3,1,0,3));//dhlp

      XMM0 = _mm_add_ps(XMM8, XMM9);   //a+b e+f i+j m+n
      XMM1 = _mm_add_ps(XMM10, XMM11); //c+d g+h k+l o+p

      XMM0 = _mm_add_ps(XMM0, XMM1);   /*(a+b)+(c+d)
                                         (e+f)+(g+h)
                                         (i+j)+(k+l)
                                         (m+n)+(o+p)*/

      _mm_store_ps(r, XMM0);
#endif
    }

    /**************************************************************************/
    template<>
    inline void matrix44_mul_vector4(double r[4],
                                     const double a[4], const double b[4],
                                     const double c[4], const double d[4],
                                     const double x[4]){
      default_proc::matrix44_mul_vector4(r, a, b, c, d, x);
#if 0
      //return;
      __m128d XMM00 = _mm_set_pd(b[0], a[0]);
      __m128d XMM01 = _mm_set_pd(d[0], c[0]);

      __m128d XMM10 = _mm_set_pd(b[1], a[1]);
      __m128d XMM11 = _mm_set_pd(d[1], c[1]);

      __m128d XMM20 = _mm_set_pd(b[2], a[2]);
      __m128d XMM21 = _mm_set_pd(d[2], c[2]);

      __m128d XMM30 = _mm_set_pd(b[3], a[3]);
      __m128d XMM31 = _mm_set_pd(d[3], c[3]);

      __m128d XMMX0 = _mm_set_pd1(x[0]);
      __m128d XMMX1 = _mm_set_pd1(x[1]);
      __m128d XMMX2 = _mm_set_pd1(x[2]);
      __m128d XMMX3 = _mm_set_pd1(x[3]);

      XMM00 = _mm_mul_pd(XMM00, XMMX0);
      XMM01 = _mm_mul_pd(XMM01, XMMX0);

      XMM10 = _mm_mul_pd(XMM10, XMMX1);
      XMM11 = _mm_mul_pd(XMM11, XMMX1);

      XMM20 = _mm_mul_pd(XMM20, XMMX2);
      XMM21 = _mm_mul_pd(XMM21, XMMX2);

      XMM30 = _mm_mul_pd(XMM30, XMMX3);
      XMM31 = _mm_mul_pd(XMM31, XMMX3);

      _mm_store_pd(r+0, _mm_add_pd(_mm_add_pd(XMM00, XMM10),
                                   _mm_add_pd(XMM20, XMM30)));

      _mm_store_pd(r+2, _mm_add_pd(_mm_add_pd(XMM01, XMM11),
                                   _mm_add_pd(XMM21, XMM31)));
#endif
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_mul_matrix44(T ra[4], T rb[4],
                                      T rc[4], T rd[4],
                                      const T aa[4], const T ab[4],
                                      const T ac[4], const T ad[4],
                                      const T ba[4], const T bb[4],
                                      const T bc[4], const T bd[4]){
      default_proc::matrix44_mul_matrix44(ra, rb, rc, rd,
                                          aa, ab, ac, ad,
                                          ba, bb, bc, bd);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_mul_matrix44(float ra[4], float rb[4],
                                      float rc[4], float rd[4],
                                      const float aa[4], const float ab[4],
                                      const float ac[4], const float ad[4],
                                      const float ba[4], const float bb[4],
                                      const float bc[4], const float bd[4]){
      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(aa, 16));
      tslassert(alignment(ab, 16));
      tslassert(alignment(ac, 16));
      tslassert(alignment(ad, 16));
      tslassert(alignment(ba, 16));
      tslassert(alignment(bb, 16));
      tslassert(alignment(bc, 16));
      tslassert(alignment(bd, 16));

      /*Transpose current matrix*/
      __m128 XMM0 = _mm_load_ps(aa); //abcd
      __m128 XMM1 = _mm_load_ps(ab); //efgh
      __m128 XMM2 = _mm_load_ps(ac); //ijkl
      __m128 XMM3 = _mm_load_ps(ad); //mnop

      __m128 XMM4 = _mm_shuffle_ps(XMM0, XMM1, _MM_SHUFFLE(2,0,2,0));
      __m128 XMM5 = _mm_shuffle_ps(XMM1, XMM0, _MM_SHUFFLE(3,1,1,3));
      __m128 XMM6 = _mm_shuffle_ps(XMM3, XMM2, _MM_SHUFFLE(0,2,2,0));
      __m128 XMM7 = _mm_shuffle_ps(XMM2, XMM3, _MM_SHUFFLE(3,1,3,1));

      XMM0 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(0,3,2,0)); //aeim
      XMM1 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(2,0,1,2)); //bfjn
      XMM2 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(1,2,3,1)); //cgko
      XMM3 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(3,1,0,3)); //dhlp

      /*Load first column of matrix w*/
      __m128 XMM8  = _mm_set_ps1(ba[0]);
      __m128 XMM9  = _mm_set_ps1(bb[0]);
      __m128 XMM10 = _mm_set_ps1(bc[0]);
      __m128 XMM11 = _mm_set_ps1(bd[0]);

      XMM4 = _mm_mul_ps(XMM8, XMM0);
      XMM5 = _mm_mul_ps(XMM9, XMM1);
      XMM6 = _mm_mul_ps(XMM10, XMM2);
      XMM7 = _mm_mul_ps(XMM11, XMM3);
      __m128 XMM12 = _mm_add_ps(_mm_add_ps(XMM4, XMM5), _mm_add_ps(XMM6, XMM7));

      /*Second colum*/
      XMM8  = _mm_set_ps1(ba[1]);
      XMM9  = _mm_set_ps1(bb[1]);
      XMM10 = _mm_set_ps1(bc[1]);
      XMM11 = _mm_set_ps1(bd[1]);

      XMM4 = _mm_mul_ps(XMM8, XMM0);
      XMM5 = _mm_mul_ps(XMM9, XMM1);
      XMM6 = _mm_mul_ps(XMM10, XMM2);
      XMM7 = _mm_mul_ps(XMM11, XMM3);
      __m128 XMM13 = _mm_add_ps(_mm_add_ps(XMM4, XMM5), _mm_add_ps(XMM6, XMM7));

      /*Third colum*/
      XMM8  = _mm_set_ps1(ba[2]);
      XMM9  = _mm_set_ps1(bb[2]);
      XMM10 = _mm_set_ps1(bc[2]);
      XMM11 = _mm_set_ps1(bd[2]);

      XMM4 = _mm_mul_ps(XMM8, XMM0);
      XMM5 = _mm_mul_ps(XMM9, XMM1);
      XMM6 = _mm_mul_ps(XMM10, XMM2);
      XMM7 = _mm_mul_ps(XMM11, XMM3);
      __m128 XMM14 = _mm_add_ps(_mm_add_ps(XMM4, XMM5), _mm_add_ps(XMM6, XMM7));

      /*Fourth colum*/
      XMM8  = _mm_set_ps1(ba[3]);
      XMM9  = _mm_set_ps1(bb[3]);
      XMM10 = _mm_set_ps1(bc[3]);
      XMM11 = _mm_set_ps1(bd[3]);

      XMM4 = _mm_mul_ps(XMM8, XMM0);
      XMM5 = _mm_mul_ps(XMM9, XMM1);
      XMM6 = _mm_mul_ps(XMM10, XMM2);
      XMM7 = _mm_mul_ps(XMM11, XMM3);
      __m128 XMM15 = _mm_add_ps(_mm_add_ps(XMM4, XMM5), _mm_add_ps(XMM6, XMM7));

      /*Transpose result*/
      XMM4 = _mm_shuffle_ps(XMM12, XMM13, _MM_SHUFFLE(2,0,2,0));
      XMM5 = _mm_shuffle_ps(XMM13, XMM12, _MM_SHUFFLE(3,1,1,3));
      XMM6 = _mm_shuffle_ps(XMM15, XMM14, _MM_SHUFFLE(0,2,2,0));
      XMM7 = _mm_shuffle_ps(XMM14, XMM15, _MM_SHUFFLE(3,1,3,1));

      XMM0 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(0,3,2,0));
      XMM1 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(2,0,1,2));
      XMM2 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(1,2,3,1));
      XMM3 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(3,1,0,3));

      _mm_store_ps(ra, XMM0);
      _mm_store_ps(rb, XMM1);
      _mm_store_ps(rc, XMM2);
      _mm_store_ps(rd, XMM3);
    }

    /**************************************************************************/
    template<class T>
    inline T matrix44_determinant(const T a[4], const T b[4],
                                  const T c[4], const T d[4]){
      return default_proc::matrix44_determinant(a, b, c, d);
    }

    /**************************************************************************/
    template<>
    inline float matrix44_determinant(const float a[4], const float b[4],
                                      const float c[4], const float d[4]){
      tslassert(alignment(a, 16));
      tslassert(alignment(b, 16));
      tslassert(alignment(c, 16));
      tslassert(alignment(d, 16));

      __m128 ABCD = _mm_load_ps(a);
      __m128 EFGH = _mm_load_ps(b);
      __m128 IJKL = _mm_load_ps(c);
      __m128 MNOP = _mm_load_ps(d);

      __m128 kljk = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(2,1,3,2));
      __m128 popn = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(1,3,2,3));
      __m128 lklj = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(1,3,2,3));
      __m128 opno = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(2,1,3,2));

      __m128 lili = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(0,3,0,3));
      __m128 npmo = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(2,0,3,1));
      __m128 jlik = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(2,0,3,1));
      __m128 pmpm = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(0,3,0,3));

      __m128 jkij = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(1,0,2,1));
      __m128 omnm = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(0,1,0,2));
      __m128 kiji = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(0,1,0,2));
      __m128 nomn = _mm_shuffle_ps(MNOP, MNOP, _MM_SHUFFLE(1,0,2,1));

      __m128 c1 = _mm_sub_ps(_mm_mul_ps(kljk, popn), _mm_mul_ps(lklj, opno));
      __m128 c2 = _mm_sub_ps(_mm_mul_ps(lili, npmo), _mm_mul_ps(jlik, pmpm));
      __m128 c3 = _mm_sub_ps(_mm_mul_ps(jkij, omnm), _mm_mul_ps(kiji, nomn));

      __m128 feee = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(0,0,0,1));
      __m128 ggff = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(1,1,2,2));
      __m128 hhhg = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(2,3,3,3));

      c1 = _mm_mul_ps(c1, feee);
      c2 = _mm_mul_ps(c2, ggff);
      c3 = _mm_mul_ps(c3, hhhg);

      c1 = _mm_add_ps(c1, _mm_add_ps(c2, c3));
      c1 = _mm_mul_ps(ABCD, c1);

      float r[4];
      _mm_store_ps(r, c1);

      return (r[0] + r[1]) + (r[2] + r[3]);


#if 0

      /*Compute inner determinants*/
      __m128 DDDC = _mm_shuffle_ps(ABCD, ABCD, _MM_SHUFFLE(2, 3, 3, 3));
      __m128 HHHG = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(2, 3, 3, 3));
      __m128 GGFF = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(1, 1, 2, 2));
      __m128 CCBB = _mm_shuffle_ps(ABCD, ABCD, _MM_SHUFFLE(1, 1, 2, 2));
      __m128 FEEE = _mm_shuffle_ps(EFGH, EFGH, _MM_SHUFFLE(0, 0, 0, 1));
      __m128 BAAA = _mm_shuffle_ps(ABCD, ABCD, _MM_SHUFFLE(0, 0, 0, 1));

      __m128 id1 = _mm_sub_ps(_mm_mul_ps(DDDC, GGFF), _mm_mul_ps(CCBB, HHHG));
      __m128 id2 = _mm_sub_ps(_mm_mul_ps(DDDC, FEEE), _mm_mul_ps(BAAA, HHHG));
      __m128 id3 = _mm_sub_ps(_mm_mul_ps(CCBB, FEEE), _mm_mul_ps(BAAA, GGFF));

      __m128 JIII = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(0, 0, 0, 1));
      __m128 KKJJ = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(1, 1, 2, 2));
      __m128 LLLK = _mm_shuffle_ps(IJKL, IJKL, _MM_SHUFFLE(2, 3, 3, 3));

      id1 = _mm_mul_ps(JIII, id1);
      id2 = _mm_mul_ps(KKJJ, id2);
      id3 = _mm_mul_ps(LLLK, id3);

      id2 = _mm_sub_ps(id2, id3);

      float r[4];
      _mm_store_ps(r, _mm_mul_ps(MNOP, _mm_sub_ps(id1, id2)));

      return (r[0] - r[1]) + (r[2] - r[3]);
#endif
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_transpose(T ra[4], T rb[4],
                                   T rc[4], T rd[4],
                                   const T a[4], const T b[4],
                                   const T c[4], const T d[4]){
      default_proc::matrix44_transpose(ra, rb, rc, rd,
                                       a, b, c, d);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_transpose(float ra[4], float rb[4],
                                   float rc[4], float rd[4],
                                   const float a[4], const float b[4],
                                   const float c[4], const float d[4]){
      //default_proc::matrix44_transpose(ra, rb, rc, rd, a, b, c, d);
      //return;
      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(a, 16));
      tslassert(alignment(b, 16));
      tslassert(alignment(c, 16));
      tslassert(alignment(d, 16));

      __m128 XMM0 = _mm_load_ps(a);
      __m128 XMM1 = _mm_load_ps(b);
      __m128 XMM2 = _mm_load_ps(c);
      __m128 XMM3 = _mm_load_ps(d);

      __m128 XMM4 = _mm_shuffle_ps(XMM0, XMM1, _MM_SHUFFLE(2,0,2,0));
      __m128 XMM5 = _mm_shuffle_ps(XMM1, XMM0, _MM_SHUFFLE(3,1,1,3));
      __m128 XMM6 = _mm_shuffle_ps(XMM3, XMM2, _MM_SHUFFLE(0,2,2,0));
      __m128 XMM7 = _mm_shuffle_ps(XMM2, XMM3, _MM_SHUFFLE(3,1,3,1));

      __m128 XMM8 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(0,3,2,0));
      __m128 XMM9 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(2,0,1,2));
      __m128 XMM10 = _mm_shuffle_ps(XMM4, XMM6, _MM_SHUFFLE(1,2,3,1));
      __m128 XMM11 = _mm_shuffle_ps(XMM5, XMM7, _MM_SHUFFLE(3,1,0,3));

      _mm_store_ps(ra, XMM8);
      _mm_store_ps(rb, XMM9);
      _mm_store_ps(rc, XMM10);
      _mm_store_ps(rd, XMM11);
    }

    static const __m128 mones = {-1.0f, -1.0f, -1.0f, -1.0f};

    /**************************************************************************/
    template<class T>
    inline void matrix44_adjugate(T ra[4], T rb[4],
                                  T rc[4], T rd[4],
                                  const T a[4], const T b[4],
                                  const T c[4], const T d[4]){
      default_proc::matrix44_adjugate(ra, rb, rc, rd,
                                      a, b, c, d);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_adjugate(float ra[4], float rb[4],
                                  float rc[4], float rd[4],
                                  const float a[4], const float b[4],
                                  const float c[4], const float d[4]){

      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(a, 16));
      tslassert(alignment(b, 16));
      tslassert(alignment(c, 16));
      tslassert(alignment(d, 16));

      __m128 ABCD = _mm_load_ps(a);
      __m128 EFGH = _mm_load_ps(b);
      __m128 IJKL = _mm_load_ps(c);
      __m128 MNOP = _mm_load_ps(d);

      __m128 aceg = _mm_shuffle_ps(ABCD, EFGH, _MM_SHUFFLE(2,0,2,0));/*ACEG*/
      __m128 bdfh = _mm_shuffle_ps(ABCD, EFGH, _MM_SHUFFLE(3,1,3,1));/*BDFH*/
      __m128 ikmo = _mm_shuffle_ps(IJKL, MNOP, _MM_SHUFFLE(2,0,2,0));/*IKMO*/
      __m128 jlnp = _mm_shuffle_ps(IJKL, MNOP, _MM_SHUFFLE(3,1,3,1));/*JLNP*/

      /* A*E* + I*M* *//*EAMI*/
      __m128 col1 = _mm_shuffle_ps(aceg, ikmo, _MM_SHUFFLE(0,2,0,2));

      /* B*F* + J*N* *//*FBNJ*/
      __m128 col2 = _mm_shuffle_ps(bdfh, jlnp, _MM_SHUFFLE(0,2,0,2));

      /* *C*G + *K*O *//*GCOK*/
      __m128 col3 = _mm_shuffle_ps(aceg, ikmo, _MM_SHUFFLE(1,3,1,3));

      /* *D*H + *L*P *//*HDPL*/
      __m128 col4 = _mm_shuffle_ps(bdfh, jlnp, _MM_SHUFFLE(1,3,1,3));


      __m128 klcd = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,2,3,2));
      __m128 pohg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,3,2,3));
      __m128 lkdc = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,3,2,3));
      __m128 opgh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,2,3,2));

      __m128 ljdb = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,3,1,3));
      __m128 npfh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,1,3,1));
      __m128 jlbd = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,1,3,1));
      __m128 pnhf = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,3,1,3));

      __m128 jkbc = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,1,2,1));
      __m128 ongf = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,2,1,2));
      __m128 kjcb = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,2,1,2));
      __m128 nofg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,1,2,1));

      __m128 r1 = _mm_sub_ps(_mm_mul_ps(klcd, pohg), _mm_mul_ps(lkdc, opgh));
      __m128 r2 = _mm_sub_ps(_mm_mul_ps(ljdb, npfh), _mm_mul_ps(jlbd, pnhf));
      __m128 r3 = _mm_sub_ps(_mm_mul_ps(jkbc, ongf), _mm_mul_ps(kjcb, nofg));

      __m128 r1_2 = _mm_mul_ps(r1, col2);
      __m128 r2_2 = _mm_mul_ps(r2, col3);
      __m128 r3_2 = _mm_mul_ps(r3, col4);

      __m128 ra1 = _mm_add_ps(r1_2, _mm_add_ps(r2_2, r3_2));

      _mm_store_ps(ra, ra1);

      //---------------------------------------------------------------

      __m128 ilad = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,0,3,0));
      __m128 pmhe = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,3,0,3));
      __m128 lida = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,3,0,3));
      __m128 mpeh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,0,3,0));

      __m128 kica = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,2,0,2));
      __m128 moeg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,0,2,0));
      __m128 ikac = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,0,2,0));
      __m128 omge = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,2,0,2));

#if 1
      __m128 r4 = _mm_mul_ps(r1, mones);
#else
      __m128 r4 = _mm_sub_ps(_mm_mul_ps(lkdc, opgh), _mm_mul_ps(klcd, pohg));
#endif
      __m128 r5 = _mm_sub_ps(_mm_mul_ps(ilad, pmhe), _mm_mul_ps(lida, mpeh));
      __m128 r6 = _mm_sub_ps(_mm_mul_ps(kica, moeg), _mm_mul_ps(ikac, omge));

      __m128 r4_2 = _mm_mul_ps(r4, col1);
      __m128 r5_2 = _mm_mul_ps(r5, col3);
      __m128 r6_2 = _mm_mul_ps(r6, col4);

      __m128 rb1 = _mm_add_ps(r4_2, _mm_add_ps(r5_2, r6_2));

      _mm_store_ps(rb, rb1);

      //--------------------------------------------------------------

      __m128 ijab = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,0,1,0));
      __m128 nmfe = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,1,0,1));
      __m128 jiba = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,1,0,1));
      __m128 mnef = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,0,1,0));

#if 1
      __m128 r7 = _mm_mul_ps(r2, mones);
      __m128 r8 = _mm_mul_ps(r5, mones);
#else
      __m128 r7 = _mm_sub_ps(_mm_mul_ps(jlbd, pnhf), _mm_mul_ps(ljdb, npfh));
      __m128 r8 = _mm_sub_ps(_mm_mul_ps(lida, mpeh), _mm_mul_ps(ilad, pmhe));
#endif
      __m128 r9 = _mm_sub_ps(_mm_mul_ps(ijab, nmfe), _mm_mul_ps(jiba, mnef));

      __m128 r7_2 = _mm_mul_ps(r7, col1);
      __m128 r8_2 = _mm_mul_ps(r8, col2);
      __m128 r9_2 = _mm_mul_ps(r9, col4);

      __m128 rc1 = _mm_add_ps(r8_2, _mm_add_ps(r7_2, r9_2));

      _mm_store_ps(rc, rc1);

      //-------------------------------------------------------------

#if 1
      __m128 r10 = _mm_mul_ps(r3, mones);
      __m128 r11 = _mm_mul_ps(r6, mones);
      __m128 r12 = _mm_mul_ps(r9, mones);
#else
      __m128 r10 = _mm_sub_ps(_mm_mul_ps(kjcb, nofg), _mm_mul_ps(jkbc, ongf));
      __m128 r11 = _mm_sub_ps(_mm_mul_ps(ikac, omge), _mm_mul_ps(kica, moeg));
      __m128 r12 = _mm_sub_ps(_mm_mul_ps(jiba, mnef), _mm_mul_ps(ijab, nmfe));
#endif

      __m128 r10_2 = _mm_mul_ps(r10, col1);
      __m128 r11_2 = _mm_mul_ps(r11, col2);
      __m128 r12_2 = _mm_mul_ps(r12, col3);

      __m128 rd1 = _mm_add_ps(r11_2, _mm_add_ps(r10_2, r12_2));

      _mm_store_ps(rd, rd1);
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_inverse(T* det,
                                 T ra[4], T rb[4],
                                 T rc[4], T rd[4],
                                 const T a[4], const T b[4],
                                 const T c[4], const T d[4]){
      default_proc::matrix44_inverse(det,
                                     ra, rb, rc, rd,
                                     a, b, c, d);
    }

    /**************************************************************************/
    template<>
    inline void matrix44_inverse(float* det,
                                 float ra[4], float rb[4],
                                 float rc[4], float rd[4],
                                 const float a[4], const float b[4],
                                 const float c[4], const float d[4]){

      tslassert(alignment(ra, 16));
      tslassert(alignment(rb, 16));
      tslassert(alignment(rc, 16));
      tslassert(alignment(rd, 16));
      tslassert(alignment(a, 16));
      tslassert(alignment(b, 16));
      tslassert(alignment(c, 16));
      tslassert(alignment(d, 16));

      __m128 ABCD = _mm_load_ps(a);
      __m128 EFGH = _mm_load_ps(b);
      __m128 IJKL = _mm_load_ps(c);
      __m128 MNOP = _mm_load_ps(d);

      __m128 aceg = _mm_shuffle_ps(ABCD, EFGH, _MM_SHUFFLE(2,0,2,0));/*ACEG*/
      __m128 bdfh = _mm_shuffle_ps(ABCD, EFGH, _MM_SHUFFLE(3,1,3,1));/*BDFH*/
      __m128 ikmo = _mm_shuffle_ps(IJKL, MNOP, _MM_SHUFFLE(2,0,2,0));/*IKMO*/
      __m128 jlnp = _mm_shuffle_ps(IJKL, MNOP, _MM_SHUFFLE(3,1,3,1));/*JLNP*/

      /* A*E* + I*M* *//*EAMI*/
      __m128 col1 = _mm_shuffle_ps(aceg, ikmo, _MM_SHUFFLE(0,2,0,2));

      /* B*F* + J*N* *//*FBNJ*/
      __m128 col2 = _mm_shuffle_ps(bdfh, jlnp, _MM_SHUFFLE(0,2,0,2));

      /* *C*G + *K*O *//*GCOK*/
      __m128 col3 = _mm_shuffle_ps(aceg, ikmo, _MM_SHUFFLE(1,3,1,3));

      /* *D*H + *L*P *//*HDPL*/
      __m128 col4 = _mm_shuffle_ps(bdfh, jlnp, _MM_SHUFFLE(1,3,1,3));


      __m128 klcd = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,2,3,2));
      __m128 pohg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,3,2,3));
      __m128 lkdc = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,3,2,3));
      __m128 opgh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,2,3,2));

      __m128 ljdb = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,3,1,3));
      __m128 npfh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,1,3,1));
      __m128 jlbd = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,1,3,1));
      __m128 pnhf = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,3,1,3));

      __m128 jkbc = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,1,2,1));
      __m128 ongf = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,2,1,2));
      __m128 kjcb = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,2,1,2));
      __m128 nofg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,1,2,1));

      __m128 r1 = _mm_sub_ps(_mm_mul_ps(klcd, pohg), _mm_mul_ps(lkdc, opgh));
      __m128 r2 = _mm_sub_ps(_mm_mul_ps(ljdb, npfh), _mm_mul_ps(jlbd, pnhf));
      __m128 r3 = _mm_sub_ps(_mm_mul_ps(jkbc, ongf), _mm_mul_ps(kjcb, nofg));

      __m128 r1_2 = _mm_mul_ps(r1, col2);
      __m128 r2_2 = _mm_mul_ps(r2, col3);
      __m128 r3_2 = _mm_mul_ps(r3, col4);

      __m128 ra1 = _mm_add_ps(r1_2, _mm_add_ps(r2_2, r3_2));

      _mm_store_ps(ra, ra1);

      //---------------------------------------------------------------

      __m128 ilad = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(3,0,3,0));
      __m128 pmhe = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,3,0,3));
      __m128 lida = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,3,0,3));
      __m128 mpeh = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(3,0,3,0));

      __m128 kica = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,2,0,2));
      __m128 moeg = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(2,0,2,0));
      __m128 ikac = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(2,0,2,0));
      __m128 omge = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,2,0,2));

#if 1
      __m128 r4 = _mm_mul_ps(r1, mones);
#else
      __m128 r4 = _mm_sub_ps(_mm_mul_ps(lkdc, opgh), _mm_mul_ps(klcd, pohg));
#endif
      __m128 r5 = _mm_sub_ps(_mm_mul_ps(ilad, pmhe), _mm_mul_ps(lida, mpeh));
      __m128 r6 = _mm_sub_ps(_mm_mul_ps(kica, moeg), _mm_mul_ps(ikac, omge));

      __m128 r4_2 = _mm_mul_ps(r4, col1);
      __m128 r5_2 = _mm_mul_ps(r5, col3);
      __m128 r6_2 = _mm_mul_ps(r6, col4);

      __m128 rb1 = _mm_add_ps(r4_2, _mm_add_ps(r5_2, r6_2));

      _mm_store_ps(rb, rb1);

      //--------------------------------------------------------------

      __m128 ijab = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(1,0,1,0));
      __m128 nmfe = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(0,1,0,1));
      __m128 jiba = _mm_shuffle_ps(IJKL, ABCD, _MM_SHUFFLE(0,1,0,1));
      __m128 mnef = _mm_shuffle_ps(MNOP, EFGH, _MM_SHUFFLE(1,0,1,0));

#if 1
      __m128 r7 = _mm_mul_ps(r2, mones);
      __m128 r8 = _mm_mul_ps(r5, mones);
#else
      __m128 r7 = _mm_sub_ps(_mm_mul_ps(jlbd, pnhf), _mm_mul_ps(ljdb, npfh));
      __m128 r8 = _mm_sub_ps(_mm_mul_ps(lida, mpeh), _mm_mul_ps(ilad, pmhe));
#endif
      __m128 r9 = _mm_sub_ps(_mm_mul_ps(ijab, nmfe), _mm_mul_ps(jiba, mnef));

      __m128 r7_2 = _mm_mul_ps(r7, col1);
      __m128 r8_2 = _mm_mul_ps(r8, col2);
      __m128 r9_2 = _mm_mul_ps(r9, col4);

      __m128 rc1 = _mm_add_ps(r8_2, _mm_add_ps(r7_2, r9_2));

      _mm_store_ps(rc, rc1);

      //-------------------------------------------------------------

#if 1
      __m128 r10 = _mm_mul_ps(r3, mones);
      __m128 r11 = _mm_mul_ps(r6, mones);
      __m128 r12 = _mm_mul_ps(r9, mones);
#else
      __m128 r10 = _mm_sub_ps(_mm_mul_ps(kjcb, nofg), _mm_mul_ps(jkbc, ongf));
      __m128 r11 = _mm_sub_ps(_mm_mul_ps(ikac, omge), _mm_mul_ps(kica, moeg));
      __m128 r12 = _mm_sub_ps(_mm_mul_ps(jiba, mnef), _mm_mul_ps(ijab, nmfe));
#endif

      __m128 r10_2 = _mm_mul_ps(r10, col1);
      __m128 r11_2 = _mm_mul_ps(r11, col2);
      __m128 r12_2 = _mm_mul_ps(r12, col3);

      __m128 rd1 = _mm_add_ps(r11_2, _mm_add_ps(r10_2, r12_2));

      _mm_store_ps(rd, rd1);

      *det = ((a[0]*ra[0] +
               a[1]*rb[0]) +
              (a[2]*rc[0] +
               a[3]*rd[0]));

      float det2 = 1.0f/ *det;

      __m128 xmmdet = _mm_set_ps1(det2);

      ra1 = _mm_mul_ps(ra1, xmmdet);
      rb1 = _mm_mul_ps(rb1, xmmdet);
      rc1 = _mm_mul_ps(rc1, xmmdet);
      rd1 = _mm_mul_ps(rd1, xmmdet);

      _mm_store_ps(ra, ra1);
      _mm_store_ps(rb, rb1);
      _mm_store_ps(rc, rc1);
      _mm_store_ps(rd, rd1);
    }
  }
}
#endif/*SSE2_MATRIX_INTRINSICS*/
