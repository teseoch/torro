/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

namespace tsl{
  /**************************************************************************/
  template<class T, class U, int N>
  class RunningAverage{
  public:
    /**************************************************************************/
    RunningAverage(){
      lastSample = -1;
      nSamples = 0;
      nTotalSamples = 0;
    }

    /**************************************************************************/
    void addSample(const T& val){
      if(IsNan(val)){
        error("input is NAN");
      }

      lastSample++;
      lastSample = lastSample % N;
      samples[lastSample] = val;

      if(nSamples < N){
        nSamples++;
      }

      cumulative = (val + cumulative *
                    (U)nTotalSamples) / (U)(nTotalSamples + 1);
      nTotalSamples ++;
    }

    /**************************************************************************/
    void clear(){
      nSamples = 0;
      nTotalSamples = 0;
      cumulative = T();
      lastSample = -1;
    }

    /**************************************************************************/
    T getAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }

      for(int i=0;i<nSamples;i++){
        sum += samples[i];
      }

      return (U)(1.0 / nSamples) * sum;
    }

    /**************************************************************************/
    T getCumulativeAverage()const{
      return cumulative;
    }

    /**************************************************************************/
    T getWeightedAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }

      //error("called?, check j+5");

      int sumWeights = 0;

      for(int i=0, j=lastSample;i<nSamples;i++, j--){
        sum += (U)(j+5) * samples[i];
        sumWeights += (j+5);

        if(j <= 0){
          j += nSamples;
        }
      }

      return (U)(1.0 / sumWeights) * sum;
    }

    /**************************************************************************/
    T getReverseWeightedAverage()const{
      T sum = T();
      if(nSamples == 0){
        return sum;
      }
      error("called?");

      int sumWeights = 0;

      for(int i=0, j=lastSample;i<nSamples;i++, j--){
        sum += (U)(nSamples - j + 5) * samples[i];
        sumWeights += (nSamples - j + 5);
        if(j <= 0){
          j += nSamples;
        }
      }

      return (U)(1.0 / sumWeights) * sum;
    }

  protected:
    /**************************************************************************/
    T samples[(uint)(N)];
    T cumulative;
    int lastSample;
    int nSamples;
    int nTotalSamples;
  };
}
