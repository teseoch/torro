/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SVD_HPP
#define SVD_HPP

#include "math/Matrix44.hpp"

namespace tsl{

  /**************************************************************************/
  /*Computes the Singular Value Decomposition of A as A=U*D*V^T*/
  template<class T, int d>
  void SVD(const Matrix44<T>& A,
           Matrix44<T>& U,
           Matrix44<T>& D,
           Matrix44<T>& V);

  /**************************************************************************/
  /*Computes the derivative dA/dA_{x,y} =
    (dU/dA_{x,y}) * D * V^T + U * (dD/dA_{x,y}) * V^T + U * D * (dV^T/dA_{x,y}),
    with dA_{i,j}/dA_{x,y} = 1 for i=x and j=y, else 0.
  */
  template<class T, int d>
  void SVDDerivative(Matrix44<T>* dU,
                     Matrix44<T>* dD,
                     Matrix44<T>* dV,
                     Matrix44<T>*  U,
                     Matrix44<T>*  D,
                     Matrix44<T>* VT,
                     int x,
                     int y,
                     bool verbose = false);

  /**************************************************************************/
  template<class T>
  void SVDCorrection(Matrix44<T>& G,
                     Matrix44<T>& U,
                     Matrix44<T>& D,
                     Matrix44<T>& V,
                     Matrix44<T>& VT){

    int invertedIndex = 2;

    /*Pure rotation of the deformation*/
    Matrix44<T> GR = U * V.transpose();

    Matrix44<T> ID = Matrix44<T>::identity();

    if(G.det() < 0.0){
      /*Correct SVD*/
      /*Negate smallest component of D and ID, where ID is the
        Identity matrix, used to correct GR*/
      D[2][2] *= -(T)1.0;
      ID[2][2] *= (T)1.0;
    }

    if(V.det() < 0.0){
      V[0][invertedIndex] *= -(T)1.0;
      V[1][invertedIndex] *= -(T)1.0;
      V[2][invertedIndex] *= -(T)1.0;
      V[3][invertedIndex] *= -(T)1.0;

      /*Recompute U*/
      U = GR * V * ID;
    }

    if(U.det3() < 0.0){
      U[0][invertedIndex] *= (T)-1.0;
      U[1][invertedIndex] *= (T)-1.0;
      U[2][invertedIndex] *= (T)-1.0;
      U[3][invertedIndex] *= (T)-1.0;
    }

    for(int i=0;i<3;i++){
      if(U[i].length() < 0.9){
        U[i].normalize();
        error("not orthonormal U");
      }
    }

    if(U.det3() < 0.0){
      U[0][invertedIndex] *= (T)-1.0;
      U[1][invertedIndex] *= (T)-1.0;
      U[2][invertedIndex] *= (T)-1.0;
      U[3][invertedIndex] *= (T)-1.0;
    }

    VT = V.transpose();

    if(Abs(V.det() - (T)1.0) > 1e-6 ||
       Abs(U.det() - (T)1.0) > 1e-6){
      error("Not orthonormal U OR V");
    }
  }
}

#endif/*SVD_HPP*/
