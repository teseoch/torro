/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef IECLINSOLVE_CR_HPP
#define IECLINSOLVE_CR_HPP

#include <limits.h>
#include "math/IECPrecLinSolve.hpp"
//#include "math/constraints/ContactBlockConstraint.hpp"
#include "core/Exception.hpp"
#include "util/CSVExporter.hpp"
#include "collision/ConstraintTol.hpp"

#include "collision/constraints/ContactConstraint2.hpp"

namespace tsl{

  double scf = 1.0;

  static int tstep = 0;
  extern BenchmarkTimer* spmv_timer;

  /**************************************************************************/
  /*In order to instantiate this solver, one must create a subclass
    which implements the virtual functions provided by the
    IECEvaluator interface. That interface provides additional 'rules'
    for constraint evaluations.*/
  template<int N, class T, class CC>
  class IECLinSolveCR : public IECPrecLinSolve<N, T>,
                        public IECEvaluator<T>{
  public:
    /**************************************************************************/
    IECLinSolveCR(int d):IECPrecLinSolve<N,T>(d, UsesFullPreconditioner<CC>()){
      /*Create and register vectors*/
      ru   = new VectorC<T>(*(this->x));
      rc   = new VectorC<T>(*(this->x));
      p    = new VectorC<T>(*(this->x));
      Ap   = new VectorC<T>(*(this->x));
      Aup  = new VectorC<T>(*(this->x));
      Acp  = new VectorC<T>(*(this->x));
      Cr   = new VectorC<T>(*(this->x));
      ACr  = new VectorC<T>(*(this->x));
      AuCr = new VectorC<T>(*(this->x));
      AcCr = new VectorC<T>(*(this->x));
      CAp1 = new VectorC<T>(*(this->x));
      scratch1 = new VectorC<T>(*(this->x));
      scratch2 = new VectorC<T>(*(this->x));
      scratchP = new VectorC<T>(*(this->x));

      pACAp  = (T)0.0;
      beta   = (T)0.0;
      alpha  = (T)0.0;
      alpha1 = (T)0.0;

      spmv_timer = &this->timer;

      this->vectors.append(ru);
      this->vectors.append(rc);
      this->vectors.append(p);
      this->vectors.append(Ap);
      this->vectors.append(Aup);
      this->vectors.append(Acp);
      this->vectors.append(Cr);
      this->vectors.append(ACr);
      this->vectors.append(AuCr);
      this->vectors.append(AcCr);
      this->vectors.append(CAp1);
      this->vectors.append(scratch1);
      this->vectors.append(scratch2);
      this->vectors.append(scratchP);
    }

    /**************************************************************************/
    virtual ~IECLinSolveCR(){
      delete ru;
      delete rc;
      delete p;
      delete Ap;
      delete Aup;
      delete Acp;
      delete Cr;
      delete ACr;
      delete AuCr;
      delete AcCr;
      delete CAp1;
      delete scratch1;
      delete scratch2;
      delete scratchP;
    }

    /**************************************************************************/
    virtual void disableAllConstraints(){
    }

    /**************************************************************************/
    virtual bool checkConstraints(bool friction,
                                  EvalStats& stats){
      return false;
    }

    /**************************************************************************/
    virtual void recomputeResidual(VectorC<T>* s1,
                                   VectorC<T>* s2,
                                   bool useAccepted = false,
                                   bool recomputeru = false){
      /*rc = Ac * x*/
      spmvc(*s1, *this->mat, *this->x, ConstraintsFull);

      /*Reset inactive constraint values in b*/
      {
        this->timer.start("vector");

        VectorC<T>::mulf(*s2, *this->b, 1, Constraints);
        this->resetInactive(*s2);

        /*rc = b - Ac*x*/
        VectorC<T>::sub(*rc, *s2, *s1);

        if(useAccepted){
          /*Adding additional RHS for accepted friction state*/
          VectorC<T>::add(*rc, *rc, *this->ab2);
        }else{
          /*Adding additional RHS for friction state*/
          VectorC<T>::add(*rc, *rc, *this->b2);
        }

        this->timer.stop("vector");
      }

      if(recomputeru){
        /*For accuracy it is better to also recompute ru, especially
          in situations where it takes a long time before converge. RU
          might get very small while rc is larger. At some point ru
          just contains some noise. If ru is not reset, this noise
          'can' be amplified due to the multiplications in the search
          process. In some cases, this can result in a non-converging
          sequence. By resetting also ru, it always resembles the real
          unconstrained error.*/

        /*Compute r, ru, rc (r = ru+rc), r = b - Ax*/
        spmvc(*s1, *this->mat, *this->x, Conventional);
        /*Compute ru = bu - Axu*/
        {
          this->timer.start("vector");
          VectorC<T>::sub(*ru, *this->b, *s1, NoConstraints);
          /*r = ru + rc*/
          VectorC<T>::add(*this->r, *ru, *rc);
          this->timer.stop("vector");
        }
      }else{
        this->timer.start("vector");
        /*r = ru + rc*/
        VectorC<T>::add(*this->r, *ru, *rc);
        this->timer.stop("vector");
      }
    }

    /**************************************************************************/
    /*Evaluates (Ax - b) and (de)activates the constraints when
      needed. Returns true in case of a change.*/
    virtual bool evaluateConstraints(const EvalType& etype, EvalStats& stats){
      bool change = false;
      bool active = false;

      /*Consider all constraints*/
      for(int i=0;i<this->mat->getNConstraints();i++){
        AbstractRowConstraint<N, T>* c =
          (AbstractRowConstraint<N, T>*)this->mat->getActiveConstraint(i);

        //c->setBB2Norm(this->bb2norm);
        c->setBB2Norm((T)1.0);
        c->setBB2NormCheck(this->bb2norm);

        bool changed = c->evaluate(*this->getx(),
                                   *this->getb(),
                                   0, etype, stats, &active,
                                   this->getb2());
        if(changed){
          change = true;
        }
      }

      if(change){
        syncConstraints();
        clean_iterations = 0;
      }
      return change;
    }

    /**************************************************************************/
    /*Reset search, residual and all associated vectors*/
    virtual void resetSearchVector(bool updatePreconditioner,
                                   bool useGradient = false,
                                   bool recomputeru = false){
      changed_iterations++;

      /*Recompute preconditioner if constraints have changed*/
      if(updatePreconditioner){
        this->timer.start("prec_update");
        this->recomputePreconditioner();
        this->timer.stop("prec_update");
      }

      /*Re-evaluate r, ru, rc first*/
      recomputeResidual(scratch1, scratch2, true, recomputeru);

      /*Cr = C * r*/
      this->applyPreconditioner(Cr,  this->r, RightPreconditioning, false);

      if(useGradient){
        /*Experimental!!*/
        /*ACr*/
        spmvc(*scratch1, *this->mat, *Cr);
        /*CACr*/
        this->applyPreconditioner(p,  scratch1, RightPreconditioning, false);
      }else{
        /*p  = Cr*/
        this->timer.start("vector");
        *p = *Cr;
        this->timer.stop("vector");
      }

      /*Compute Apu, Apc*/
      spmvc(*Aup, *this->mat, *p, Conventional);
      spmvc(*Acp, *this->mat, *p, ConstraintsFull);

      {
        this->timer.start("vector");
        VectorC<T>::add(*Ap, *Aup, *Acp);

        /*ACr = Ap*/
        *ACr  = *Ap;
        *AuCr = *Aup;
        *AcCr = *Acp;
        this->timer.stop("vector");
      }
    }

    /**************************************************************************/
    /*Tests if the method has converged*/
    bool isConverged(int i,
                     T rr,
                     T rr2,
                     T rr3,
                     T rr4){
      message("| r|  = %10.10e", rr);
      message("|Ar|  = %10.10e", rr2);
      message("|rc|  = %10.10e", rr3);
      message("|Arc| = %10.10e", rr4);

      if((rr3 < DISTANCE_TOL*1.0/*this->bb2norm*//(T)1.0) ||
         (rr4 < DISTANCE_TOL*0.0/*this->bb2norm*//(T)1.0)){
        //return false;
      }else{
        return false;
      }

      /*rr3 < DISTANCE_TOL -> constraints are satisfied. Norm rr3 must
        be invariant from the number of constraints, i.e., it needs
        the maximum constraint error of all constraints.

        Norm rr4 is not used anymore*/

      if(rr  < (this->eps * this->bb2norm + this->eps) ||
         rr2 < (this->eps * this->bb2norm + this->eps)/(T)1.0){
        /*Method converged, store statistics*/

        this->timer.stop("solve");

        warning("%d iterations", i);
        warning("| r|  = %10.10e", rr);
        warning("|Ar|  = %10.10e", rr2);
        warning("|rc|  = %10.10e", rr3);
        warning("|Arc| = %10.10e", rr4);

        recomputeResidual(scratch1, scratch2, true);

        T res = this->getResidualNorm(scratch1);
        warning("|Ax-ab| = %10.10e", res);
        message("|Ax-ab| = %10.10e", res);

        recomputeResidual(scratch1, scratch2, false);

        res = this->getResidualNorm(scratch1);
        warning("|Ax-ab| = %10.10e", res);
        message("|Ax-ab| = %10.10e", res);

        if(this->exporter){
          /*Export statistics*/
          this->exporter->setValue("n_iterations",
                                   i);
          this->exporter->setValue("n_clean_iterations",
                                   clean_iterations);
          this->exporter->setValue("n_changed_iterations",
                                   changed_iterations);
          this->exporter->setValue("n_constraints",
                                   this->mat->getNConstraints());
          this->exporter->setValue("n_update_iterations",
                                   n_updated);
          this->exporter->setValue("n_changed_eval_iterations",
                                   n_changed_iter);
          this->exporter->setValue("delassus_operator",
                                   this->timer.getTotalTimeUSec("delassus"));
          this->exporter->setValue("n_delassus_operator",
                                   this->timer.getTotalCalls("delassus"));
          this->exporter->setValue("solverTime",
                                   this->timer.getTotalTimeUSec("solve"));
          this->exporter->setValue("preconditionerTime",
                                   this->timer.getTotalTimeUSec("applyPreconditioner"));
          this->exporter->setValue("preSolverTime",
                                   this->timer.getTotalTimeUSec("solve_pre"));
          this->exporter->setValue("n_preconditionerCalls",
                                   this->timer.getTotalCalls("applyPreconditioner"));
          this->exporter->setValue("evaluationTime",
                                   this->timer.getTotalTimeUSec("evalConstraints"));
          this->exporter->setValue("n_evalCalls",
                                   this->timer.getTotalCalls("evalConstraints"));
          this->exporter->setValue("spmvTime",
                                   this->timer.getTotalTimeUSec("spmv"));
          this->exporter->setValue("spmvCalls",
                                   this->timer.getTotalCalls("spmv"));
          this->exporter->setValue("evaluationTime2",
                                   this->timer.getTotalTimeUSec("evalConstraints2"));
          this->exporter->setValue("n_evalCalls2",
                                   this->timer.getTotalCalls("evalConstraints2"));
          this->exporter->setValue("evaluationTime3",
                                   this->timer.getTotalTimeUSec("evalConstraints3"));
          this->exporter->setValue("n_evalCalls3",
                                   this->timer.getTotalCalls("evalConstraints3"));
          this->exporter->setValue("updateTime",
                                   this->timer.getTotalTimeUSec("updateConstraints"));
          this->exporter->setValue("n_updateCalls",
                                   this->timer.getTotalCalls("updateConstraints"));
          this->exporter->setValue("bNorm",
                                   this->bb2norm);
          this->exporter->setValue("spmv_conventional",
                                   this->timer.getTotalTimeUSec("spmv_conventional"));
          this->exporter->setValue("n_spmv_conventional",
                                   this->timer.getTotalCalls("spmv_conventional"));
          this->exporter->setValue("spmv_pre",
                                   this->timer.getTotalTimeUSec("spmv_constr_pre"));
          this->exporter->setValue("n_spmv_pre",
                                   this->timer.getTotalCalls("spmv_constr_pre"));
          this->exporter->setValue("spmv_constraints",
                                   this->timer.getTotalTimeUSec("spmv_constr"));
          this->exporter->setValue("n_spmv_constraints",
                                   this->timer.getTotalCalls("spmv_constr"));
          this->exporter->setValue("precond_vec",
                                   this->timer.getTotalTimeUSec("precond_vec"));
          this->exporter->setValue("n_precond_vec",
                                   this->timer.getTotalCalls("precond_vec"));
          this->exporter->setValue("check_time",
                                   this->timer.getTotalTimeUSec("checkConstraints"));
          this->exporter->setValue("n_check",
                                   this->timer.getTotalCalls("checkConstraints"));
          this->exporter->setValue("vector",
                                   this->timer.getTotalTimeUSec("vector"));
          this->exporter->setValue("n_vector",
                                   this->timer.getTotalCalls("vector"));
          this->exporter->setValue("vector_red",
                                   this->timer.getTotalTimeUSec("vector_red"));
          this->exporter->setValue("n_vector_red",
                                   this->timer.getTotalCalls("vector_red"));
          this->exporter->setValue("prec_update",
                                   this->timer.getTotalTimeUSec("prec_update"));
          this->exporter->setValue("n_prec_update",
                                   this->timer.getTotalCalls("prec_update"));
          this->exporter->setValue("prec_scaling_update",
                                   this->timer.getTotalTimeUSec("prec_scaling_update"));
          this->exporter->setValue("n_prec_scaling_update",
                                   this->timer.getTotalCalls("prec_scaling_update"));

          /*Compute FBNorm*/
          scratch1->clear();
          scratch2->clear();
          scratchP->clear();

          for(int i=0;i<this->mat->getNConstraints();i++){
            AbstractRowConstraint<N, T>* c =
              (AbstractRowConstraint<N, T>*)this->mat->getActiveConstraint(i);

            c->computeFBFunction(*scratch1, *scratch2,
                                 *scratchP, *this->C2, *this->x, *this->b, *this->b2);
          }

          this->exporter->setValue("FBValueNP", scratch1->getAbsMax());
          this->exporter->setValue("FBValueSF", scratch2->getAbsMax());
          this->exporter->setValue("FBValueKF", scratchP->getAbsMax());
        }

        tstep++;

        return true;
      }
      return false;
    }

    /**************************************************************************/
    /*Solve the constrained optimization problem*/
    virtual void solve(int steps = 10000, T _eps = (T)1e-6){
      tslassert(this->mat->getWidth() == this->b->getSize());
      tslassert(this->mat->getWidth() == this->mat->getHeight());
      tslassert(this->mat->getWidth() == this->x->getSize());

      steps = 100000;

      iter = 0;
      n_updated = 0;
      n_changed_iter = 0;

      constraintSetModified = false;

      spmv_timer = &this->timer;

      EvalStats firstStats;

      message("matrix height = %d", this->mat->getHeight());
      message("matrix n_elements = %d", this->mat->getNElements());
      message("matrix average row fill = %10.10e", this->mat->getAverageRow());
      message("%d constraints", this->mat->getNConstraints());

      this->eps = _eps;

      this->timer.resetTimers();

      this->timer.start("solve");

      /*-----------------------------------------------------------*/

      /*Performs an update of all (potential) constraints that are
        known at this point and updates vector b*/
      {
        this->timer.start("updateConstraints");
        this->updateConstraints();
        this->timer.stop("updateConstraints");
      }

      /*-----------------------------------------------------------*/
      /*Compute norm of b, used for relative error.*/
      this->computeBNorm(scratch1);

      /*Used in stats -> number of iterations that triggered a reset*/
      changed_iterations = 0;

      /*Used in stats -> number of iterations without a reset prior to
        convergence*/
      clean_iterations = 0;

      /*Finalize matrices*/
      this->mat->finalize();

      /*Clear kinetic friction*/
      this->b2->clear();

      /*Update diagonal preconditioner*/
      this->computePreconditioner();
      this->C2->finalize();

      /*-----------------------------------------------------------*/

      /*-----------------------------------------------------------*/
      {
#if 0
        /*If enabled, constraints are evaluated, meaning: a full
          collision detection is performed using the current
          velocity. However, this evaluation can be skipped at the
          start of the method such that the method continues with the
          current set of correct constraints. */
        this->timer.start("evalConstraints2");
        EvalStats stats;
        this->evaluateStart(stats);
        this->timer.stop("evalConstraints2");
#endif
      }
      /*-----------------------------------------------------------*/

      /*-----------------------------------------------------------*/
      /*Update kinetic friction values b2, and accept these (ab2)*/
      {
        this->timer.start("evalConstraints3");
        this->evaluateAccept();
        this->timer.stop("evalConstraints3");
      }
      /*-----------------------------------------------------------*/

      /*-----------------------------------------------------------*/

      /*Compute r, ru, rc (r = ru + rc), r = b - Ax*/
      spmvc(*scratch1, *this->mat, *this->x, Conventional);

      {
        /*Compute ru = bu - Axu*/
        this->timer.start("vector");
        VectorC<T>::sub(*ru, *this->b, *scratch1, NoConstraints);
        this->timer.stop("vector");
      }

      /*Compute the preconditioner scaling*/
      {
        this->timer.start("prec_scaling_update");
        this->computePreconditionerScaling();
        this->timer.stop("prec_scaling_update");
      }

      /*Compute r, p and related vectors, and update preconditioner*/
      resetSearchVector(true);
      changed_iterations = 0;

      bool converged   = false; /*If converged, check validity afterwards*/
      bool lastChanged = false; /*If constraints are changed,
                                  lastChanged is set to true,
                                  preventing that in the next
                                  iteration another constraint
                                  evaluation is performed*/

      int localIter = -1;
      int reset_iter = 0;
      int penalty = 0;      /*If penalty > 0 -> no convergence check
                              is performed. When constraints are
                              changed, penalty is reset to 3, meaning,
                              the next 3 iterations the method can not
                              stop.*/

      /*Note: penalty and lastChanged can be removed due to updated
        schema for constraint evaluation, this prevents two successive
        iterations to execute a constraint evaluation while the
        residual is large.*/

      T residual   = this->getResidualNorm(scratch1);
      T residualC  = this->getResidualNormC(scratch1);
      T residual2  = (T)0.0;
      T residualC2 = (T)0.0;

      T residualNorm = residual;

      /*-----------------------------------------------------------*/

      bool lastFreeze = false;

      int stepIter = 0;

      bool lastFine = false;

      int nReset = 0;

      for(int i=0;i<steps*10;i++){
        EvalStats stats;/*Constraint evaluation statistics for this iteration*/
        if(this->iterCallBack){
          /*Call the callback function.*/
          this->iterCallBack();
        }

        clean_iterations++;
        reset_iter++;
        localIter++;
        stepIter++;

        /*Compute |ru| |r| and |rc|*/
        T residualU = this->getResidualNormUnconstrained(scratch1);
        residual  = this->getResidualNorm(scratch1);
        residualC = this->getResidualNormC(scratch1);

        /*Compute |b + b2|*/
        this->computeBB2Norm(scratch1);

        {
          this->timer.start("vector");
          VectorC<T>::mul(*scratch1, *ACr, *ACr);
          this->timer.stop("vector");
        }

        {
          this->timer.start("vector_red");
          residual2 = Sqrt(Abs(scratch1->oSum())) + ACr->getCAbsMax();
          residualC2 = ACr->getCAbsMax();
          this->timer.stop("vector_red");
        }

#if 1
        message("%d relative error = %10.10e, res = %10.10e, bnorm = %10.10e, res^2 = %10.10e, eps = %10.10e",i ,
                residual / this->bb2norm, residual, this->bb2norm,
                residual * residual, this->eps);

        message("%d relative error = %10.10e, res = %10.10e, bnorm = %10.10e, res^2 = %10.10e, eps = %10.10e",i,
                residual2 / this->bb2norm, residual2, this->bb2norm,
                residual2 * residual2, this->eps);

        message("residual factor = %10.10e", residual / residual2);

        message("residualU = %10.10e", residualU);

        VectorC<T>::mul(*scratch1, *this->x, *this->x);

        T xunorm = Sqrt(scratch1->oSum());
        T xcnorm = Sqrt(scratch1->cSum());

        VectorC<T>::add(*scratch1, *this->b, *this->b2);
        VectorC<T>::mul(*scratch1, *scratch1, *scratch1);

        T bnorm = Sqrt(scratch1->sum());

        VectorC<T>::mul(*scratch1, *this->r, *this->r);

        message("residual C = %10.10e, norm xu = %10.10e, norm xc = %10.10e, bnorm = %10.10e",
                residualC, xunorm, xcnorm, bnorm);
        message("residual v = %10.10e, l = %10.10e, t = %10.10e",
                Sqrt(scratch1->oSum()),
                Sqrt(scratch1->getCAbsMax()),
                Sqrt(scratch1->sum()));

        message("residual c2 = %10.10e", residualC2);
#endif

        bool converged2 = converged;

        message("lastFreeze = %d", lastFreeze);
        message("penalty    = %d", penalty);

        if(!lastChanged && penalty <= 0){
          if((residual  < (this->eps*this->bb2norm + this->eps) ||
              residual2 < (this->eps*this->bb2norm + this->eps)) &&
             (residualC <  DISTANCE_TOL ||
              residualC2 < DISTANCE_TOL*0.0) &&
             residualU < (this->eps*this->bb2norm + this->eps)){
            converged = true;
          }

          if(converged2){
            converged = true;
          }

          /*Check constraints if converged*/
          bool changed  = false;
          bool changed1 = false;

          message("converged  = %d", converged);

          if(converged && !lastFreeze && penalty <= 0){
            /*Perform a full evaluation*/

            warning("Converged -> final check @ %d - %d - %d",
                    i, localIter, iter);
            warning("%10.10e, %10.10e", residual, residual2);
            warning("%10.10e, %10.10e", residualC, residualC2);

            bool changed2 = false;
            {
              this->timer.start("evalConstraints2");
              /*evaluateAtConvergence requires a clean b2*/
              this->b2->clear();
              changed1 = this->evaluateAtConvergence(stats);

              if(changed1){
                n_changed_iter++;
                penalty = 3;
                message("constraints changed at convergence");
              }else{
                /*No constraints have changed, perform a final
                  evaluation that triggers additional collision
                  checks using the updated geometry*/
                message("no constraints changed at convergence");

                /*evaluateAtConvergenceFinal requires a clean b2*/
                this->b2->clear();
                changed1 = this->evaluateAtConvergenceFinal(stats);

                if(changed1){
                  message("constraints changed at convergence final");
                  penalty = 3;
                }else{
                  message("no constraints changed at convergence final");
                }
              }
              this->timer.stop("evalConstraints2");
            }

            /*-----------------------------------------------------------*/

            if(!changed1){
              message("converged & not changed -> check constraints");

              {
                this->timer.start("checkConstraints");

                /*Check and possibly update all existing constraints*/
                changed2 = checkConstraints(false, stats) || changed1;

                if(changed1){
                  this->evaluateAccept();
                }

                /*Check constraints might affect b2 or not. To be sure
                  we reset b2 by running an empty evaluate
                  constraints*/
                this->b2->clear();
                EvalType dummy;
                this->evaluateConstraints(dummy, stats);
                this->timer.stop("checkConstraints");
              }

              if(changed2){
                message("Constraints checked and UPDATED!!\n\n");
                changed     = true;
                lastChanged = true;
                n_updated++;
                penalty = 3;
                /*Recompute ru*/
              }
            }else{
              changed = true;
              lastChanged = true;
              penalty = 3;

              message("Something has changed");
              /*-----------------------------------------------------------*/
              { //friction in b2 -> ab2
                this->timer.start("evalConstraints3");
                this->evaluateAccept();
                this->timer.stop("evalConstraints3");
              }
              /*-----------------------------------------------------------*/
            }

            if(!changed){
              if(isConverged(i, residual, residual2, residualC, residualC2)){
                /*No constraints have changed/updated and the method
                  has converged -> x and the constraint state are
                  final*/
                return;
              }
            }
          }

          if(changed){
            /*Some constraints are changed, added or removed,
              re-evaluate residual*/

            if(constraintSetModified){
              /*Do this only if constraints are added or removed*/

              constraintSetModified = false;
              message("constraint set modified, update scaling");
              {
                this->timer.start("prec_scaling_update");
                this->computePreconditionerScaling();
                this->timer.stop("prec_scaling_update");
              }
            }

            /*Also reset unconstrained residual*/
            resetSearchVector(true, false, true);

            residual  = this->getResidualNorm(scratch1);
            residualC = this->getResidualNormC(scratch1);

            {
              this->timer.start("vector");
              VectorC<T>::mul(*scratch1, *ACr, *ACr);
              residual2 = (Sqrt(scratch1->oSum()) + ACr->getCAbsMax());
              this->timer.stop("vector");
            }

            message("Residual after reset %10.10e, %10.10e",
                    residual, residualC);

            localIter = 1;

            if(penalty == 0){
              penalty = 1;
            }

            lastChanged = true;
          }
        }

        converged = false;

        /*Compute CAp*/
        this->applyPreconditioner(CAp1, Ap, RightPreconditioning, false);

        /*Compute rCACr*/
        {
          this->timer.start("vector");
          VectorC<T>::mul(*scratch2, *Cr, *ACr);
          this->timer.stop("vector");
        }

        /*Compute alpha1*/
        {
          this->timer.start("vector_red");
          alpha1 = scratch2->sum();
          this->timer.stop("vector_red");
        }

        /*Compute pACAp*/
        {
          this->timer.start("vector");
          VectorC<T>::mul(*scratch2, *Ap, *CAp1);
          this->timer.stop("vector");
        }

        /*Compute pACAp*/
        {
          this->timer.start("vector_red");
          pACAp = scratch2->sum();
          this->timer.stop("vector_red");
        }

#if 1
        if(residual2 <= 1e-13){
          /*Hard rest, should not happen, if residual2 becomes very
            small, the method has computed a least squares solution
            which is sub optimal. Usually this is a sing of
            constraints acting in opposite directions. Such
            constraints can be created by errors in the collision
            detection.*/
          *this->x *= (T)0.9999;

          if(HasFrictionCone<CC>()){
            resetSearchVector(false);
          }else{
            resetSearchVector(true);
          }
          warning("hard reset");
          getchar();
          continue;
        }
#endif
        /*Compute alpha*/
        alpha = alpha1/pACAp;

        /*Compute
          x  = x + alpha * p
          ru = ru - alpha * Apu
          rc = rc - alpha * Apc
          r  = ru + rc
          Cr  = Cr - alpha * CAp*/
        {
          this->timer.start("vector");
          VectorC<T>::mfadd(*this->x, alpha, *p, *this->x);
          VectorC<T>::mfadd(*ru, -alpha, *Aup, *ru);
          VectorC<T>::mfadd(*rc, -alpha, *Acp, *rc);
          VectorC<T>::add(*this->r, *ru, *rc);
          VectorC<T>::mfadd(*Cr, -alpha, *CAp1, *Cr);
          this->timer.stop("vector");
        }

        residualNorm = this->getResidualNorm(scratch1);

        message("Alpha = %10.10e, %10.10e/%10.10e", alpha, alpha1, pACAp);

        /*If alpha becomes larger than 1, skip the evaluation of the
          constraints*/
        bool freeze = false;

        if( Abs(alpha) > (T)1.0 || (alpha < 0.0)){
          freeze = true;
          warning("FREEZE, alpha = %10.10e", alpha);
        }

#if 1
        /*If we are close to the solution, allow constraint iterations
          also directly after something has changed.*/
        if(residualNorm < 1e-3){
          lastChanged = false;
        }
#endif
        /*Evaluate constraints given updated x*/
        bool changed   = false;

        message("penalty = %d, lastChanged = %d", penalty, lastChanged);
        if(/*penalty <= 0 &&*/ !lastChanged && !freeze){
          {
            this->timer.start("evalConstraints");
            changed = this->evaluateRegular(localIter, residual,
                                            residualC, &lastFine, stats);
            this->timer.stop("evalConstraints");
          }

          if(changed){
            /*Friction b2 -> ab2*/
            this->timer.start("evalConstraints3");
            this->evaluateAccept();
            this->timer.stop("evalConstraints3");
          }
          lastChanged = changed;
        }else{
          lastChanged = false;
        }

        penalty--;

        message("changed @ %d, %d", i, changed);

        /*End constraint evaluation*/

        converged = false;

        if(changed){
          /*Constraints changed, recompute residual and update preconditioner*/
          nReset++;

          if(HasFrictionCone<CC>()){
            resetSearchVector(false, false, false);
          }else{
            resetSearchVector(true, false, false);
          }
        }else{
          if(Abs(alpha) < 1e-9){
            /*If alpha is small, the update on x is also small*/
            warning("alpha = %10.10e", alpha);
            resetSearchVector(false, true);
            continue;
          }

          /*Compute AuCr and AcCr*/
          spmvc(*AuCr, *this->mat, *Cr, Conventional);
          spmvc(*AcCr, *this->mat, *Cr, ConstraintsFull);

          {
            this->timer.start("vector");
            VectorC<T>::add(*ACr, *AuCr, *AcCr);

            /*Compute beta = -r_{i+1}CACr_{i+1}/r_{i}CACr_{i}*/

            /*Compute rCACr*/
            VectorC<T>::mul(*scratch2, *ACr, *Cr);
            this->timer.stop("vector");

            this->timer.start("vector_red");
            beta = -scratch2->sum() / alpha1;
            this->timer.stop("vector_red");
          }

          message("Beta = %10.10e/%10.10e = %10.10e",
                  beta*alpha1, alpha1, beta);

          /*Compute p, Aup, Acp, Ap*/
          {
            this->timer.start("vector");
            /*p   = Cr - beta * p */
            VectorC<T>::mfadd(*p, -beta, *p, *Cr);

            /*Aup = AuCr - beta * Aup */
            VectorC<T>::mfadd(*Aup, -beta, *Aup, *AuCr);

            /*Acp = AcCr - beta * Acp */
            VectorC<T>::mfadd(*Acp, -beta, *Acp, *AcCr);

            /*Ap = Aup + Acp*/
            VectorC<T>::add(*Ap,  *Aup,  *Acp);
            this->timer.stop("vector");
          }
        }
        iter++;
      }
      throw new SolutionNotFoundException(__LINE__, __FILE__,
                                          "Number of iterations exceeded.");
    }

  protected:
    /**************************************************************************/
    VectorC<T>* ru;   /*Unconstrained residual*/
    VectorC<T>* rc;   /*Constraint residual*/
    VectorC<T>* p;    /*Search vector*/
    VectorC<T>* Ap;   /*A * p = (Au + Ac) * p*/
    VectorC<T>* Aup;  /*Au * p*/
    VectorC<T>* Acp;  /*Ac * p*/
    VectorC<T>* Cr;   /*Preconditioned residual*/
    VectorC<T>* ACr;  /*A * C * r = (Au + Ac) * C * r*/
    VectorC<T>* AuCr; /*Au * C * r*/
    VectorC<T>* AcCr; /*Ac * C * r*/
    VectorC<T>* CAp1; /*C * A * p_i*/

    VectorC<T>* scratch1;
    VectorC<T>* scratch2;
    VectorC<T>* scratchP;

    T pACAp;
    T alpha;
    T alpha1;
    T beta;

    int clean_iterations;
    int changed_iterations;

    int iter;
    int n_updated;
    int n_changed_iter;
  protected:
    /**************************************************************************/
    void syncConstraints(){
    }

    bool constraintSetModified;
  };
}

#endif/*IECLINSOLVE_CR_HPP*/
