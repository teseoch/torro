/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINT_HPP
#define CONSTRAINT_HPP

/*Old way of compiling different methods. New approach uses a template
  and a constraint type specific for a solver/method*/

#define METHOD_CR
//#undef  METHOD_CR
#define METHOD_GS
#undef  METHOD_GS
#define METHOD_SP
#undef  METHOD_SP

#if not defined METHOD_CR && not defined METHOD_GS && not defined METHOD_SP
#error "No method defined"
#else
#if defined METHOD_CR && defined METHOD_GS
#error "Multiple methods enabled! Not allowed"
#endif
#if defined METHOD_CR && defined METHOD_SP
#error "Multiple methods enabled! Not allowed"
#endif
#if defined METHOD_GS && defined METHOD_SP
#error "Multiple methods enabled! Not allowed"
#endif
#endif

#if defined METHOD_CR
#define FRICTION                  /*Friction*/
#define DEACTIVATE_CONSTRAINT     /*Constraints are allowed to deactivate*/
#define FRICTION_APPROX
#define WEIGHT_CLAMPING
#define WEIGHT_MIN 0.0
#define WEIGHT_MAX 1.0
#define FRICTION_CONE
//#undef FRICTION_CONE
#define PREC_FULL
#endif

#if defined METHOD_GS
#define FRICTION
#define ACTIVE_CONSTRAINTS        /**/
#define DEACTIVATE_CONSTRAINT
//#define WEIGHT_CLAMPING
//#define WEIGHT_MIN 0.0
//#define WEIGHT_MAX 1.0
#endif

#if defined METHOD_SP
#define FRICTION                  /*Friction*/
#define DEACTIVATE_CONSTRAINT     /*Constraints are allowed to deactivate*/
#define FRICTION_APPROX
#define WEIGHT_CLAMPING
#define WEIGHT_MIN 0.0
#define WEIGHT_MAX 1.0
//#define FRICTION_CONE
#define PREC_DIAG
#endif


/*Check macros*/
#if defined(FRICTION_APPROX) && defined(ACTIVE_CONSTRAINTS)
#error "Two multiplier methods enabled, not allowed. (f)"
#endif


namespace tsl{
  /**************************************************************************/
  template<class T>
  inline T pos(T n){
#if 1
    if(n > 0.0){
      return n;
    }
    return 0;
#else
    return fabs(n);
#endif
  }

  /**************************************************************************/
  template<class T>
  inline T neg(T n){
#if 1
    if(n < 0.0){
      return n;
    }
    return 0;
#else
    return fabs(n);
#endif
  }
}

#endif/*CONSTRAINT_HPP*/
