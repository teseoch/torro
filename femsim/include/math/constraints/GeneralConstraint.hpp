/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef GENERALCONSTRAINT_HPP
#define GENERALCONSTRAINT_HPP

#include "math/constraints/AbstractBlockConstraint.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "collision/ConstraintTol.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Compare<ConstraintElement<T> >{
  public:
    /**************************************************************************/
    static bool less(const ConstraintElement<T>& a,
                     const ConstraintElement<T>& b){
      return a.col < b.col;
    }

    /**************************************************************************/
    static bool equal(const ConstraintElement<T>& a,
                      const ConstraintElement<T>& b){
      return a.col == b.col;
    }
  };

  /**************************************************************************/
  enum ConstraintType{Equality, InequalityLessThan, InequalityGreaterThan};

#define GENERAL_CONSTRAINT_SUBTYPE 2392

  /**************************************************************************/
  template<int N, class T>
  class GeneralConstraint;

  /**************************************************************************/
  template<int N, class T>
  class SpMatrix;

  /**************************************************************************/
  template<int N, class T>
  class GeneralBlockConstraint : public AbstractBlockConstraint<N, T>{
  public:
    /**************************************************************************/
    typedef ConstraintElement<T>             Element;
    typedef typename Tree<Element>::Iterator ElementTreeIterator;

    /**************************************************************************/
    GeneralBlockConstraint(bool full):fullPreconditioner(full){
      this->offset = 3;
      index[0] = Constraint::undefined;
      this->subType = GENERAL_CONSTRAINT_SUBTYPE;
    }

    /**************************************************************************/
    virtual ~GeneralBlockConstraint(){
    }

    /**************************************************************************/
    void collectElements(List<Element>& elements)const{
    }

    /**************************************************************************/
    int getProjectionType(int l)const{
      return 0;
    }

    /**************************************************************************/
    virtual void multiplyAHSHA(VectorC<T>& r,
                               const VectorC<T>& x,
                               const SpMatrixC<N, T>* mat,
                               bool transposed = false,
                               const VectorC<T>* mask = 0)const{
      if(fullPreconditioner){
        if(mat->getStatus() == Individual){
          if(this->rowConstraint->status == Inactive){
            return;
          }
        }else if(mat->getStatus() == AllActive){
          //Just multiply
        }else if(mat->getStatus() == NoneActive){
          return;
        }else{
          error("Unimplemented mode");
        }

        const T* xdata  = x.getData();
        //T*       rdatax = r.getExtendedData();
        T*       rdata  = r.getData();


        //return;

        GeneralConstraint<N, T>* constraint =
          (GeneralConstraint<N, T>*)this->rowConstraint;

        //int cindex = this->row_id;
        //int offset = this->getOffset();

        T lr = 0;

        ElementTreeIterator it = constraint->elements.begin();
        while(it != constraint->elements.end()){
          int col = (*it).col;

          T lx = xdata[col];

          //lr += /*Sqr(S) **/ (*it).SHA * ((*it).D)* lx;
          lr += (*it).SHA * lx;

          DBG(message("1 x = %10.15e, SHA = %10.15e, r = %10.15e, sum = %10.15e",
                      lx, (*it).SHA, (*it).SHA*lx, lr));

          it++;
        }

        //rdatax[cindex * offset] = lr;

        /*AHT*/
        it = constraint->elements.begin();
        while(it != constraint->elements.end()){
          int col = (*it).col;

          //T res = ((*it).D) * (*it).HA * lr;
          T res = (*it).HA * lr;

          rdata[col] += -res;

          DBG(message("2 [%d] x = %10.15e, HA = %10.15e, r = %10.15e, sum = %10.15e",
                      col, lr, (*it).HA, (*it).HA*lr, rdata[col]));



          it++;
        }
      }
    }

    /**************************************************************************/
    void generalMultiply(VectorC<T>& r,
                         const VectorC<T>& x,
                         bool transposed = false)const{
      if(this->matrix->getStatus() == Individual){
        if(this->rowConstraint->status == Inactive){
          return;
        }
      }else if(this->matrix->getStatus() == AllActive){
        //Just multiply
      }else if(this->matrix->getStatus() == NoneActive){
        return;
      }else{
        error("Unimplemented mode");
      }

      int cindex = this->row_id;
      int offset = this->getOffset();
      int size = x.getSize();

      T* rdata  = 0;
      T* rdatax = 0;
      GeneralConstraint<N, T>* constraint = 0;

      if(fullPreconditioner){
        rdata  = r.getData();
        constraint = (GeneralConstraint<N, T>*)this->rowConstraint;
      }

      if(fullPreconditioner){
        rdatax = r.getExtendedData();
        const T* xdata  = x.getData();
        T lr = 0;
        ElementTreeIterator it = constraint->elements.begin();

        while(it != constraint->elements.end()){
          int col = (*it).col;

          T lx = xdata[col];

          lr += (*it).SHA * lx;

          it++;
        }

        rdatax[cindex * offset] = lr;
      }

      if(fullPreconditioner){
        rdatax[cindex * offset] += -S * x[size + cindex * offset];
      }else{
        r[size + cindex * offset] = S * x[size + cindex * offset];
      }

      //r[size + cindex * offset] += (T)1000000000.0 * x[size + cindex * offset];

      /*Multiply AHS*/
      if(fullPreconditioner){
        ElementTreeIterator it = constraint->elements.begin();
        while(it != constraint->elements.end()){
          int col = (*it).col;
          rdata[col] += (*it).SHA * x[size + cindex * offset];
          it++;
        }
      }
    }

    /**************************************************************************/
    /*Performs a multiplication r = Q x*/
    void multiply(VectorC<T>& r, const VectorC<T>& x)const{
      generalMultiply(r, x, false);
    }

    /**************************************************************************/
    /*Performs a multiplication r = Q^T x*/
    void multiplyTransposed(VectorC<T>& r, const VectorC<T>& x)const{
      generalMultiply(r, x, true);
    }

    /**************************************************************************/
    void setIndex(int i,
                  int idx){
      tslassert(i==0);
      index[i] = idx;
    }

    /**************************************************************************/
    int getIndex(int i)const{
      tslassert(i==0);
      return index[i];
    }

    /**************************************************************************/
    void print(std::ostream& os)const{
      os << "Block constraint" << std::endl;
    }

    /**************************************************************************/
    ConstraintStatus getStatus()const{
      return this->status;
    }

    /**************************************************************************/
    void destroy(VectorC<T>&b,
                 VectorC<T>& x){
      //tslassert(this->indexMap.size() == 0);
      //tslassert(this->indices.size() == 0);
    }

    /**************************************************************************/
    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns,
                       int row,
                       Vector<T>* rhs)const{
    }

    //protected:
    int index[1];
    T S;
    const bool fullPreconditioner;
  };

  /**************************************************************************/
  template<int N, class T>
  class GeneralConstraint : public AbstractRowConstraint<N, T>{
  public:
    /**************************************************************************/
    typedef ConstraintElement<T>             Element;
    typedef typename Tree<Element>::Iterator ElementTreeIterator;

    /**************************************************************************/
    GeneralConstraint(bool full, ConstraintType type = Equality):
      fullPreconditioner(full){
      this->offset = 3;
      c = 0;
      ctype = type;
      this->status = Active;
      this->row_id = -1;
      scale = (T)1.0;
      cache = (T)0.0;
      counter = 0;
      nForced = 0;
      this->subType = GENERAL_CONSTRAINT_SUBTYPE;
    }

    /**************************************************************************/
    virtual ~GeneralConstraint(){
    }

    /**************************************************************************/
    virtual void init(SpMatrixC<N, T>* owner){
      this->matrix = owner;
      counter = 0;
      nForced = 0;
    }

    /**************************************************************************/
    int getProjectionType(int l)const{
      if(ctype == Equality){
        return 0;
      }

      if(ctype == InequalityGreaterThan){
        return 1;
      }

      if(ctype == InequalityLessThan){
        return -1;
      }

      return 0;
    }

    /**************************************************************************/
    /*Performs a multiplication r = Hx*/
    void multiply(VectorC<T>& r,
                  const VectorC<T>& x)const{
      if(this->matrix->getStatus() == Individual){
        if(this->status == Inactive){
          return;
        }
      }else if(this->matrix->getStatus() == AllActive){
        //Just multiply
      }else if(this->matrix->getStatus() == NoneActive){
        return;
      }else{
        error("Unimplemented mode");
      }

      T rv = 0;

      int cindex = this->row_id;
      int offset = this->getOffset();
      int size   = x.getSize();

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        T xv = x[column];
        rv += (*it).value * xv;

        it++;
      }

      r[size + cindex * offset] = rv;
    }

    /**************************************************************************/
    /*Performs a multiplication r = (H^T)x*/
    void multiplyTransposed(VectorC<T>& r,
                            const VectorC<T>& x)const{
      if(this->matrix->getStatus() == Individual){
        if(this->status == Inactive){
          return;
        }
      }else if(this->matrix->getStatus() == AllActive){
        //Just multiply
      }else if(this->matrix->getStatus() == NoneActive){
        return;
      }else{
        error("Unimplemented mode");
      }

      int cindex = this->row_id;
      int size = x.getSize();
      int offset = this->getOffset();

      T xv = x[size + cindex * offset];

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int row = (*it).col;
        r[row] += (*it).value * xv;
        it++;
      }
    }

    /**************************************************************************/
    void preMultiply(VectorC<T>& r,
                     const VectorC<T>& x,
                     const SpMatrixC<N, T>* mat,
                     bool transposed,
                     const VectorC<T>* mask)const{
    }

    /**************************************************************************/
    void resetMultipliers(VectorC<T>& x,
                          Vector4<T>& c)const{
      int cindex = this->row_id;
      int size = x.getSize();
      int offset = this->getOffset();

      x[size + cindex * offset] = 0;
    }

    /**************************************************************************/
    void cacheMultipliers(Vector4<T>& c,
                          VectorC<T>& x)const{
      int cindex = this->row_id;
      int size = x.getSize();
      int offset = this->getOffset();

      c[0] = x[size + cindex * offset];
    }

    /**************************************************************************/
    T& getConstraintValue(int i){
      if(i==0){
        return c;
      }else{
        zero = 0;
        return zero;
      }
    }

    /**************************************************************************/
    const T& getConstraintValue(int i)const{
      if(i==0){
        return c;
      }else{
        zero = 0;
        return zero;
      }
    }

    /**************************************************************************/
    void computePreconditioner(VectorC<T>& C)const{
      this->preconditioner = &C;

      if(this->status == Inactive){
        //return;
      }

      PRINT_FUNCTION;

      int cindex = this->row_id;
      int size = C.getSize();
      int offset = this->getOffset();

      C[size + cindex * offset] = 0;
    }

    /**************************************************************************/
    void computePreconditionerMatrix(VectorC<T>& DD,
                                     SpMatrixC<N, T>& C,
                                     const SpMatrixC<N, T>& B,
                                     AdjacencyList* l,
                                     VectorC<T>* scaling = 0)const{
      GeneralBlockConstraint<N, T>* c =
        (GeneralBlockConstraint<N, T>*)C.getConstraint(this->row_id);

      //message("block constraint @ %d = %p", this->row_id, c);

      if(this->status == Inactive){
        //return;
      }

      /*Compute S*/
      c->S = 0;
      long double sum = 0.0;

      T maxScale = 1.0;
      ElementTreeIterator it = elements.begin();
      PRINT(elements.size());

      while(it != elements.end()){

        PRINT(sizeof(c->S));
        PRINT(sizeof((*it).SHA));
        PRINT(sizeof((*it).HA));


        int column = (*it).col;
        long double d = (DD[column]);
        //double ddd = DD[column];
        //message("dd = %10.10e @%d", ddd, column);
        PRINT(d);
        long double val = (*it).value;
        PRINT(val);
        sum += Sqr(val) * d;// * val;
        maxScale = Max(maxScale, (*scaling)[column]);
        it++;
      }

      T* xdata = scaling->getExtendedData();
      T factor = xdata[this->row_id * this->getOffset()];

      if(factor == (T)0.0){
        factor = (T)1.0;
      }else if(factor != (T)1.0){
        //warning("Scale factor = %10.10e", factor);
      }

      factor = (T)1.0;

      if(fullPreconditioner){
        c->S = (T)((long double)1.0/sum) / (T)factor;
      }else{
        c->S = (T)((long double)1.0/sum) / (T)factor;
      }

      PRINT(c->S);

      if(IsNan(c->S)){
        error("Singular general preconditioner");
      }

      it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        T d = DD[column];
        (*it).D = d;
        (*it).HA = ((*it).value) * ((d));
        (*it).SHA = (c->S) * (*it).HA;
        it++;
      }
    }

    /**************************************************************************/
    AbstractBlockConstraint<N,T>* constructBlockConstraint()const{
      AbstractBlockConstraint<N, T>* cc =
        new GeneralBlockConstraint<N, T>(fullPreconditioner);
      cc->status = this->status;
      //cc->rowConstraint = this;
      return cc;
    }

    /**************************************************************************/
    RowConstraintMultiplier<N,T>* constructConstraintMultiplier()const{
      PRINT_FUNCTION;
      return 0;//new RowContactConstraintMultiplier<N, T>();
    }

    /**************************************************************************/
    void showValues(VectorC<T>& x){
      int size = x.getSize();
      int row = this->row_id;
      int offset = this->getOffset();

      T m = x[size + row * offset];

      T r = 0;

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        T xv = x[column];
        r += (*it).value * xv;
        it++;
      }

      r -= c;

      START_DEBUG;
      warning("General constraint %d, %p, m = %10.10e, c = %10.10e, jv-c = %10.10e",
              this->row_id, this, m, c, r);
      warning("Status = %d, counter = %d", this->status == Active,
              this->counter);
      END_DEBUG;
    }

    /**************************************************************************/
    bool forceEvaluate(VectorC<T>& x,
                       VectorC<T>& b,
                       T meps,
                       //int operation,
                       const EvalType& etype,
                       EvalStats& stats,
                       bool* active = 0,
                       VectorC<T>* kb = 0){
      if(active){
        *active = true;
      }


      int size = x.getSize();
      int row = this->row_id;
      int offset = this->getOffset();

      T m = x[size + row * offset];

      DBG(warning("force general constraint, row: %d, c: %10.10e, m: %10.10e, status = %d",
                  this->row_id, c, m, this->status == Active));

      if(ctype == Equality){
        return false;
      }


      START_DEBUG;

      message("Inequality");
      END_DEBUG;
      /*Load multiplier*/


      /*Evaluate*/
      T r = 0;

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        T xv = x[column];
        r += (*it).value * xv;
        it++;
      }

      r -= c;

      bool print = false;

      if(this->status == Active){
        if(ctype == InequalityGreaterThan){
          if(m < 1e-7*0 /*&& r >= -JV_EPS*0*/){
            DBG(warning("general constraint forced deactivated"));
            cache = x[size + row * offset];
            x[size + row * offset] = 0;

            print = true;
            this->status = Inactive;
          }
        }else if(ctype == InequalityLessThan){
          if(m < 1e-7*0 /*&& r <= JV_EPS*0*/){
            DBG(warning("general constraint forced deactivated"));
            cache = x[size + row * offset];
            x[size + row * offset] = 0;

            print = true;
            this->status = Inactive;
          }
        }
      }else{
        /*INACTIVE*/
        if(ctype == InequalityGreaterThan){
          if(r <= -JV_EPS){
            DBG(warning("general constraint forced activated"));
            this->status = Active;
            nForced++;
            counter = -nForced;
            print = true;
            x[size + row * offset] = cache;
          }
        }else if(ctype == InequalityLessThan){
          if(r >= JV_EPS){
            DBG(warning("general constraint forced activated"));
            this->status = Active;
            nForced++;
            counter = -nForced;
            print = true;
            x[size + row * offset] = cache;
          }
        }
      }

      if(print){
        DBG(warning("forced evaluate jv %d : %10.10e, jv-c: %10.10e, m: %10.10e, status = %d",
                    this->row_id, r + c, r, m,
                    this->status == Active));
      }

      if(print){
        return print;
      }

      return false;
    }

    /**************************************************************************/
    /*Evaluates Hx > c and (de)activates the constraint accordingly,
      returns true if a constraint has been changed*/
    bool evaluate(VectorC<T>& x,
                  VectorC<T>& b,
                  T meps,
                  //int operation,
                  const EvalType& etype,
                  EvalStats& stats,
                  bool* active = 0,
                  VectorC<T>* kb = 0){
      if(active){
        *active = true;
      }

      if(etype.forcePenetrationCheck() ||
         etype.forceVolumeCheck()){
        return forceEvaluate(x, b, meps, etype, stats, active, kb);
      }

      int size = x.getSize();
      int row = this->row_id;
      int offset = this->getOffset();

      T m = x[size + row * offset];

      DBG(warning("evaluate general constraint, row: %d, c: %10.10e, m: %10.10e, status = %d",
               this->row_id, c, m, this->status == Active));



      if(ctype == Equality){
        /*Keep active*/
        //message("Equality");

        T r = 0;

        ElementTreeIterator it = elements.begin();

        if(IsNan(c)){
          error("c is nan");
        }

        while(it != elements.end()){
          int column = (*it).col;
          T xv = x[column];
          r += (*it).value * xv;
          it++;
        }

        r -= c;

        //(warning("jv-c = %10.10e %d", r, this->row_id));
        //warning("m    = %10.10e %d", m, this->row_id);
        return false;
      }else{
        //message("Inequality");
        /*Load multiplier*/


        /*Evaluate*/
        T r = 0;

        ElementTreeIterator it = elements.begin();

        while(it != elements.end()){
          int column = (*it).col;
          T xv = x[column];
          r += (*it).value * xv;
          it++;
        }

        r -= c;

        //(warning("jv-c = %10.10e %d", r, this->row_id));
        //warning("m    = %10.10e %d", m, this->row_id);
        bool violation = false;
        bool changed = false;

        if(ctype == InequalityGreaterThan){
          if(r >= -JV_EPS && m <= 1e-7*0){
            violation = false;
          }else{
            violation = true;
          }
        }else if(ctype == InequalityLessThan){
          if(r <= JV_EPS && m <= 1e-7*0){
            violation = false;
          }else{
            violation = true;
          }
        }

        //message("violation = %d", violation);

        if(!violation){
          /*Constraint should be nonactive*/
          if(this->status == Active){
            this->status = Inactive;
            //reset multiplier
            cache = x[size + row * offset];
            x[size + row * offset] = 0;
            //x[size + row * offset] = 0;

            DBG(warning("general constraint deactivated"));
            changed = true;
            //counter++;
          }
        }else{
          /*Constraint should be active*/
          if(this->status == Inactive && counter < 250000){
            counter++;
            this->status = Active;
            DBG(warning("general constraint activated"));
            changed = true;
            x[size + row * offset] = cache;
          }
        }

        if(changed){
          DBG(warning("general evaluate jv %d, %p : %10.10e, jv-c: %10.10e, m: %10.10e, status = %d",
                      this->row_id, this, r + c, r, m,
                      this->status == Active));
        }

        if(changed){
          return true;
        }
      }

      return false;
    }

    /**************************************************************************/
    bool project(VectorC<T>& x,
                 VectorC<T>& b,
                 T meps,
                 //int operation,
                 const EvalType& etype,
                 bool* active = 0,
                 VectorC<T>* kb = 0){
      if(ctype == Equality){
        return false;
      }else{
        int size = x.getSize();
        int row = this->row_id;
        int offset = this->getOffset();

        T m = x[size + row * offset];

        if(ctype == InequalityLessThan){
          if(m < (T)0.0){
            x[size + row * offset] = (T)0.0;
          }
        }else if(ctype == InequalityGreaterThan){
          if(m > (T)0.0){
            x[size + row * offset] = (T)0.0;
          }
        }

        return false;
      }
    }

#if 0
    /**************************************************************************/
    void computeResidual(VectorC<T>& r,
                         const VectorC<T>& x){
      if(this->status == Inactive){
        //return;
      }

      T rv = 0;

      int cindex = this->row_id;
      int offset = this->getOffset();
      int size   = x.getSize();

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        T xv = x[column];
        rv += (*it).value * xv;

        it++;
      }

      r[size + cindex * offset] = rv - c;
    }
#endif

    /**************************************************************************/
    void setValue(int column,
                  T value){
      Element target;
      target.col = column;

      if(IsNan(value)){
        error("NAN value");
      }

      ElementTreeIterator it = elements.find(target);

      if(it == elements.end()){
        /*Element does not exist*/
        target.value = value;
        elements.insert(target);
        DBG(message("Adding value @ %d - %10.10e, row %d",
                    column, value, this->row_id));
        this->cSize++;
      }else{
        (*it).value = value;
        DBG(message("Setting value @ %d - %10.10e, row %d",
                    column, value, this->row_id));
      }
    }

    /**************************************************************************/
    void setIndex(int i, int idx){
      /////
    }

    /**************************************************************************/
    int getIndex(int i)const{
      int index = 0;
      ElementTreeIterator it = elements.begin();
      while(it != elements.end()){
        if(index == i){
          return it->col/3;
        }
        index++;
        it++;
      }

      return 0;
    }

    /**************************************************************************/
    int getIndividualSize()const{
      return this->getSize();
    }

    /**************************************************************************/
    Element getValue(int row,
                     int col)const{
      if(row == 0){
        int idx = 0;
        ElementTreeIterator it = elements.begin();
        while(it != elements.end()){
          if(idx == col){
            if(it->col < 0){
              break;
            }
            return *it;
          }
          it++;
          idx++;
        }
      }
      Element ret;
      ret.col = -1;
      ret.value = 0;
      return ret;
    }

    /**************************************************************************/
    void print(std::ostream& os)const{
      os << "subType = " << this->getSubType() << ", " << GENERAL_CONSTRAINT_SUBTYPE << std::endl;
      os << "GeneralConstraint " << this->row_id << " c = " <<  getConstraintValue(0) << std::endl;

      if(this->status == Active){
        os << "Active" << std::endl;
      }else{
        os << "Inactive" << std::endl;
      }

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;
        //T xv = x[column];
        //rv += (*it).value * xv;
        os << "Column " << column << "  |  " << (*it).value << std::endl;
        it++;
      }
    }

    /**************************************************************************/
    ConstraintStatus getStatus()const{
      return this->status;
    }

    /**************************************************************************/
    void destroy(VectorC<T>& b,
                 VectorC<T>& x){
    }

    /**************************************************************************/
    void collectElements(List<Element>& el)const{
      int row = this->matrix->constraints.reverseIndex(this->row_id);
      int offset = 3;

      ElementTreeIterator it = elements.begin();

      while(it != elements.end()){
        int column = (*it).col;

        Element element;
        element.row   = this->matrix->getHeight() + row * offset;
        element.col   = column;
        element.value = (*it).value;
        el.append(element);

        it++;
      }
    }

    /**************************************************************************/
    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns,
                       int row,
                       Vector<T>* rhs)const{
      //int row = this->row_id;
      int offset = 3;
      //int size = this->getSize();

      ElementTreeIterator it = elements.begin();

      //int row = this->matrix->constraints.reverseIndex(this->row_id);

      while(it != elements.end()){

        int column = (*it).col;
        T value = (*it).value;
        (*L) [row*offset][column] = value;
        (*LT)[column][row*offset] = value;
        columns->uniqueInsert(column, column);

        it++;
      }

      if(rhs){
        (*rhs)[row * offset] = c;
      }
    }

    /**************************************************************************/
    void computeFBFunction(VectorC<T>& np,
                           VectorC<T>& sf,
                           VectorC<T>& kf,
                           SpMatrixC<N, T>& C,
                           const VectorC<T>& x,
                           const VectorC<T>& b,
                           const VectorC<T>& b2){
    }


    Tree<Element> elements;

    T c; /*rhs of constraints*/

    mutable T scale;

    ConstraintType ctype;

    mutable T zero;
    int counter;
    int nForced;
    T cache;
    const bool fullPreconditioner;
  };
}


#endif/*GENERALCONSTRAINT_HPP*/
