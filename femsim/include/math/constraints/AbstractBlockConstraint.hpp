/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef ABSTRACTBLOCKCONSTRAINT_HPP
#define ABSTRACTBLOCKCONSTRAINT_HPP

#include "math/constraints/AbstractMatrixConstraint.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  class AbstractRowConstraint;

  /**************************************************************************/
  template<int N, class T>
  class ContactConstraint;

  /**************************************************************************/
  template<int N, class T>
  class GeneralConstraint;

  /**************************************************************************/
  template<int N, class T>
  class AbstractBlockConstraint : public AbstractMatrixConstraint<N, T>{
  public:
    /**************************************************************************/
    friend class ContactConstraint<N, T>;
    friend class GeneralConstraint<N, T>;

    /**************************************************************************/
    AbstractBlockConstraint(){
      rowConstraint = 0;
      this->type = blockConstraint;
    }

    /**************************************************************************/
    virtual ~AbstractBlockConstraint(){
    }

    /**************************************************************************/
    virtual void multiplyAHSHA(VectorC<T>& r,
                               const VectorC<T>& x,
                               const SpMatrixC<N, T>* mat,
                               bool transposed,
                               const VectorC<T>* mask)const = 0;

    /**************************************************************************/
    virtual void preMultiply(VectorC<T>& r,
                             const VectorC<T>& x,
                             const SpMatrixC<N, T>* mat,
                             bool transposed,
                             const VectorC<T>* mask)const{
      int nConstraints = mat->getNConstraints();

      for(int idx = 0; idx < nConstraints; idx++){
        AbstractBlockConstraint<N, T>* cc =
          (AbstractBlockConstraint<N, T>*)mat->getActiveConstraint(idx);
        cc->multiplyAHSHA(r, x, mat, transposed, mask);
      }
    }

    /**************************************************************************/
    /*Performs a multiplication r = Q x*/
    virtual void multiply(VectorC<T>& r,
                          const VectorC<T>& x)const = 0;

    /**************************************************************************/
    /*Performs a multiplication r = Q^T x*/
    virtual void multiplyTransposed(VectorC<T>& r,
                                    const VectorC<T>& x)const = 0;

    /**************************************************************************/
    void setRowConstraint(AbstractRowConstraint<N, T>* c){
      rowConstraint = c;
    }

    /**************************************************************************/
    int getIndividualSize()const{
      error("called?");
      return this->getSize();
    }
  protected:
    /*Associated row constraint*/
    AbstractRowConstraint<N, T>* rowConstraint;
  };
}

#endif/*ABSTRACTBLOCKCONSTRAINT_HPP*/
