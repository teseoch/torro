/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef QUATERNION_HPP
#define QUATERNION_HPP

#include "math/Vector4.hpp"
#include "math/Matrix44.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Quaternion{
  public:
    //x, y, z, w
#ifdef _WIN32
	__declspec(align(16)) T m[4];
#else
    T m[4] __attribute__ ((aligned (16)));
#endif

    /**************************************************************************/
    Quaternion(bool){
      /*No initialization*/
    }

    /**************************************************************************/
    Quaternion(){
      /*Initialize with 0,0,0,1 */
      set(SumIdentity(T()),
          SumIdentity(T()),
          SumIdentity(T()),
          MultiplyIdentity(T()));
    }

    /**************************************************************************/
    /*Axis angle initialization*/
    Quaternion(const Vector4<T>& v,
               T angle){
      set(v, angle);
    }

    /**************************************************************************/
    Quaternion(const Vector4<T>& v){
      tslassert(v.m[3] == 0);
      vector4_load(m, v.m);
      m[3] = 0;
    }

    /**************************************************************************/
    /*Euler angles in radians*/
    Quaternion(T x, T y, T z){
      set(x, y, z);
      error("Called?");
    }

    /**************************************************************************/
    Quaternion(const Quaternion<T>& q){
      vector4_load(m, q.m);
    }

    /**************************************************************************/
    Quaternion(const T q[]){
      vector4_load(m, q);
    }

    /**************************************************************************/
    Quaternion(T x,
               T y,
               T z,
               T w){
      vector4_load(m, x, y, z, w);
    }

    /**************************************************************************/
    T& operator[](int i){
#ifdef N_DEBUG
      return m[i];
#else
      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Quaternion::operator[] out of bounds %d", i);
      }
      return m[0];
#endif
    }

    /**************************************************************************/
    const T& operator[](int i)const{
#ifdef N_DEBUG
      return m[i];
#else
      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Quaternion::operator[] out of bounds %d", i);
      }
      return m[0];
#endif
    }

    /**************************************************************************/
    Quaternion<T>& operator=(const Quaternion<T>& q){
      vector4_load(m, q.m);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& operator=(const T f){
      error("assigning scalar to quaternion");
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& operator=(const T q[]){
      error("assigning array to quaternion");
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& set(const Quaternion<T>& q){
      vector4_load(m, q.m);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& set(const T f){
      error("assigning scalar to quaternion");
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& set(const T q[]){
      error("assigning array to quaternion");
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& set(T x,
                       T y,
                       T z,
                       T w){
      vector4_load(m, x, y, z, w);
      return *this;
    }

    /**************************************************************************/
    /*Angle in radians*/
    Quaternion<T>& set(const Vector4<T> v,
                       T angle){
      T c = Cos(angle/(T)2.0);
      T s = Sin(angle/(T)2.0);

      tslassert(v.m[3] == 0);

      Vector4<T> vv = v.normalized() * s;

      vector4_load(m, vv.m);
      m[3] = c;
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& setIdentity(){
      set(SumIdentity(T()),
          SumIdentity(T()),
          SumIdentity(T()),
          MultiplyIdentity(T()));
      return *this;
    }

    /**************************************************************************/
    void normalize(){
      *this /= norm();
    }

    /**************************************************************************/
    Quaternion<T> normalized()const{
      return *this / norm();
    }

    /**************************************************************************/
    Quaternion<T>& normalizeVec(){
      T nrm = Sqrt(Sqr(m[0]) + Sqr(m[1]) + Sqr(m[2]));
      m[0] /= nrm;
      m[1] /= nrm;
      m[2] /= nrm;

      return *this;
    }

    /**************************************************************************/
    /*Angles in radians*/
    Quaternion<T>& set(T x,
                       T y,
                       T z){
      T sY = Sin(y*(T)0.5);
      T cY = Cos(y*(T)0.5);
      T sZ = Sin(z*(T)0.5);
      T cZ = Cos(z*(T)0.5);
      T sX = Sin(x*(T)0.5);
      T cX = Cos(x*(T)0.5);

      set(sY * sZ * cX + cY * cZ * sX,
          sY * cZ * cX + cY * sZ * sX,
          cY * sZ * cX - sY * cZ * sX,
          cY * cZ * cX - sY * sZ * sX);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& operator+=(const Quaternion<T>& q){
      vector4_add(m, m, q.m);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& operator-=(const Quaternion<T>& q){
      vector4_sub(m, m, q.m);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T> operator+()const{
      return *this;
    }

    /**************************************************************************/
    Quaternion<T> operator-()const{
      Quaternion<T> r(false);
      vector4_muls(r.m, m, -(T)1.0);
      return r;
    }

    /**************************************************************************/
    Quaternion<T> operator+(const Quaternion<T>& q)const{
      Quaternion<T> r(false);
      vector4_add(r.m, m, q.m);
      return r;
    }

    /**************************************************************************/
    Quaternion<T> operator-(const Quaternion<T>& q)const{
      Quaternion<T> r(false);
      vector4_sub(r.m, m, q.m);
      return r;
    }

    /**************************************************************************/
    Quaternion<T>& operator*=(T f){
      vector4_muls(m, m, f);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T>& operator/=(T f){
      vector4_divs(m, m, f);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T> operator*(T f)const{
      Quaternion<T> r(false);
      vector4_muls(r.m, m, f);
      return r;
    }

    /**************************************************************************/
    Vector4<T> operator*(const Vector4<T>& v)const{
      Quaternion qv(v);
      qv = *this * qv * inverse();

      Vector4<T> rv;

      vector4_load(rv.m, qv.m);
      rv[3] = 0;

      return rv;
    }

    /**************************************************************************/
    Quaternion<T> operator/(T f)const{
      Quaternion<T> r(false);
      vector4_divs(r.m, m, f);
      return r;
    }

    /**************************************************************************/
    T norm()const{
      return Sqrt(vector4_dot(m, m));
    }

    /**************************************************************************/
    T norm2()const{
      return vector4_dot(m, m);
    }

    /**************************************************************************/
    Quaternion<T> conjugate()const{
      Quaternion<T> r(false);
      T inv[4] = {-(T)1.0, -(T)1.0, -(T)1.0, 1.0};
      vector4_mul(r.m, m, inv);
      return r;
    }

    /**************************************************************************/
    Quaternion<T> log()const{
      Quaternion<T> r;

      T a = ArcCos(m[3]);
      T sa = Sin(a);

      r[3] = 0;

      if(sa > 0){
        r[0] = m[0] * a / sa;
        r[1] = m[1] * a / sa;
        r[2] = m[2] * a / sa;
      }else{
        r *= 0;
      }
      return r;
    }

    /**************************************************************************/
    Quaternion<T> exp()const{
      Quaternion<T> r;
      T a = Sqrt(Sqr(m[0]) + Sqr(m[1]) + Sqr(m[2]));
      T sa = Sin(a);
      T ca = Cos(a);

      r[3] = ca;

      if(a > 0){
        r[0] = sa * m[0] / a;
        r[1] = sa * m[1] / a;
        r[2] = sa * m[2] / a;
      }else{
        r[0] = 0;
        r[1] = 0;
        r[2] = 0;
      }
      return r;
    }

    /**************************************************************************/
    template<class TT>
    friend Quaternion<TT> operator*(TT f,
                                    const Quaternion<TT>& q);

    /**************************************************************************/
    Quaternion<T> operator*(const Quaternion<T>& q)const{
      Vector4<T> vp(  m[0],   m[1],   m[2], 0);
      Vector4<T> vq(q.m[0], q.m[1], q.m[2], 0);

      Vector4<T> cr = m[3]*vq + q.m[3]*vp + cross(vp, vq);

      Quaternion<T> r(cr.m[0], cr.m[1], cr.m[2],
                      m[3] * q.m[3] - dot(vp, vq));

      return r;
    }

    /**************************************************************************/
    Matrix44<T> toRotMatrix() const{
      Matrix44<T> r;
      r[0][0] = (T)1.0 - (T)2.0 * (m[1] * m[1] + m[2] * m[2]);
      r[0][1] =          (T)2.0 * (m[0] * m[1] - m[2] * m[3]);
      r[0][2] =          (T)2.0 * (m[0] * m[2] + m[1] * m[3]);
      r[0][3] = 0;

      r[1][0] =          (T)2.0 * (m[0] * m[1] + m[2] * m[3]);
      r[1][1] = (T)1.0 - (T)2.0 * (m[0] * m[0] + m[2] * m[2]);
      r[1][2] =          (T)2.0 * (m[2] * m[1] - m[0] * m[3]);
      r[1][3] = 0;

      r[2][0] =          (T)2.0 * (m[0] * m[2] - m[1] * m[3]);
      r[2][1] =          (T)2.0 * (m[1] * m[2] + m[0] * m[3]);
      r[2][2] = (T)1.0 - (T)2.0 * (m[0] * m[0] + m[1] * m[1]);
      r[2][3] = 0;

      r[3][0] = r[1][3] = r[2][3] = 0;
      r[3][3] = (T)1;
      return r;
    }

    /**************************************************************************/
    Quaternion<T>& eye(){
      set(0,0,0,1);
      return *this;
    }

    /**************************************************************************/
    Quaternion<T> unitInverse()const{
      return conjugate();
    }

    /**************************************************************************/
    Quaternion<T> inverse()const{
      Quaternion<T> r = conjugate();

      T nrm2 = norm2();
      vector4_divs(r.m, r.m, nrm2);

      return r;
    }

    /**************************************************************************/
    Vector4<T> getRotationAxis()const{
      T s = Sqrt(Max((T)1.0 - Sqr(m[3]), (T)0.0));

      if(s >= (T)1e-9){
        return Vector4<T>(m[0] / s, m[1] / s, m[2] / s, 0.0);
      }else{
        /*No rotation, just return a default axis*/
        return Vector4<T>((T)1.0, (T)0.0, (T)0.0, (T)0.0);
      }
    }

    /**************************************************************************/
    void toAxisAngle(Vector4<T>& axis,
                     T& angle)const{
      angle = (T)2.0 * ArcCos(m[3]);
      axis  = getRotationAxis();
    }

    /**************************************************************************/
    template<class TT>
    friend Quaternion<TT> slerp(const Quaternion<TT>& a,
                                const Quaternion<TT>& b,
                                TT t);

#ifndef __ANDROID__
    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Quaternion<Y>& q);
#endif
  };

  /**************************************************************************/
  template<class T>
  Quaternion<T> operator*(T f, const Quaternion<T>& q){
    Quaternion<T> r;

    vector4_muls(r.m, q.m, f);

    return r;
  }

  /**************************************************************************/
  template<class T>
  Quaternion<T> slerp(const Quaternion<T>& a,
                      const Quaternion<T>& b, T t){
    Quaternion<T> aa = a.normalized();
    Quaternion<T> bb = b.normalized();

    T dot = vector4_dot(aa.m, bb.m);

    if(dot < SumIdentity(T())){
      dot = dot * -MultiplyIdentity(T());
      bb = -bb;
    }else{
      //c = bb;
    }

    if(dot < MultiplyIdentity(T()) - MultiplyIdentity(T()) * (T)1e-6){
      T angle = ArcCos(dot);
      T angle_t = angle * t;
      T sin_angle = Sin(angle);
      T sin_angle_t = Sin(angle_t);

      T s1 = sin_angle_t / sin_angle;
      T s0 = Cos(angle_t) - dot * s1;

      return ((aa * s0) + (bb * s1)).normalized();
    }
    return (aa + (bb - aa) * t).normalized();
  }

#ifndef __ANDROID__
  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const Quaternion<T>& q){
    os << std::scientific;
    os.precision(10);
    os << "" << q.m[0] << '\t' << q.m[1] << '\t' << q.m[2] << " |\t" << q.m[3] << std::endl;
    return os;
  }
#endif
}

#endif/*QUATERNION_HPP*/
