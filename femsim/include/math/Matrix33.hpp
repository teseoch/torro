/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MATRIX33_HPP
#define MATRIX33_HPP

#include "math/Vector3.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Matrix33{
  public:
    /**************************************************************************/
    static const Matrix33<T> Zero;

    /**************************************************************************/
    static const Matrix33<T> Identity;

    /**************************************************************************/
    Matrix33(){
      memset(m, 0, 9*sizeof(T));
    }

    /**************************************************************************/
    Matrix33(T a, T b, T c,
             T d, T e, T f,
             T g, T h, T i){
      m[0] = a; m[1] = b; m[2] = c;
      m[3] = d; m[4] = e; m[5] = f;
      m[6] = g; m[7] = h; m[8] = i;
    }

    /**************************************************************************/
    void set(T a, T b, T c,
             T d, T e, T f,
             T g, T h, T i){
      m[0] = a; m[1] = b; m[2] = c;
      m[3] = d; m[4] = e; m[5] = f;
      m[6] = g; m[7] = h; m[8] = i;
    }

    /**************************************************************************/
    T det()const{
      return
        m[0] * (m[4] * m[8] - m[7] * m[5]) -
        m[1] * (m[3] * m[8] - m[6] * m[5]) +
        m[2] * (m[3] * m[7] - m[6] * m[4]);
    }

    /**************************************************************************/
    Matrix33<T> inverse(T* dd = 0)const{
      Matrix33<T> r;
      /*Compute adjoint*/
      r.m[0] = +(m[4] * m[8] - m[5] * m[7]);
      r.m[1] = -(m[1] * m[8] - m[2] * m[7]);
      r.m[2] = +(m[1] * m[5] - m[2] * m[4]);

      r.m[3] = -(m[3] * m[8] - m[5] * m[6]);
      r.m[4] = +(m[0] * m[8] - m[2] * m[6]);
      r.m[5] = -(m[0] * m[5] - m[2] * m[3]);

      r.m[6] = +(m[3] * m[7] - m[4] * m[6]);
      r.m[7] = -(m[0] * m[7] - m[1] * m[6]);
      r.m[8] = +(m[0] * m[4] - m[1] * m[3]);

      /*Compute 1/determinant given adjoint*/
      T d = (m[0] * r.m[0] + m[1] * r.m[3] + m[2] * r.m[6]);
      if(dd){
        *dd = d;
      }
      d = ((T)1.0)/d;
      /*Compute inverse*/
      for(int i=0;i<9;i++){
        r.m[i] *= d;
      }

      return r;
    }

    /**************************************************************************/
    Vector3<T> operator*(const Vector3<T>& x)const{
      Vector3<T> r;
      r.m[0] = m[0] * x.m[0] + m[1] * x.m[1] + m[2] * x.m[2];
      r.m[1] = m[3] * x.m[0] + m[4] * x.m[1] + m[5] * x.m[2];
      r.m[2] = m[6] * x.m[0] + m[7] * x.m[1] + m[8] * x.m[2];
      return r;
    }

    /**************************************************************************/
    Matrix33<T> operator+(const Matrix33<T>& w) const{
      return Matrix33<T>(m[0]+w.m[0], m[1]+w.m[1], m[2]+w.m[2],
                         m[3]+w.m[3], m[4]+w.m[4], m[5]+w.m[5],
                         m[6]+w.m[6], m[7]+w.m[7], m[8]+w.m[8]);
    }

    /**************************************************************************/
    Matrix33<T> operator-(const Matrix33<T>& w)const{
      return Matrix33<T>(m[0]-w.m[0], m[1]-w.m[1], m[2]-w.m[2],
                         m[3]-w.m[3], m[4]-w.m[4], m[5]-w.m[5],
                         m[6]-w.m[6], m[7]-w.m[7], m[8]-w.m[8]);
    }

    /**************************************************************************/
    Matrix33<T> operator*(const Matrix33<T>& w) const{
      T x, y, z;
      Matrix33<T> r;

      x = m[0]; y = m[1]; z = m[2];
      r.m[0] = x*w.m[0] + y*w.m[3] + z*w.m[6];
      r.m[1] = x*w.m[1] + y*w.m[4] + z*w.m[7];
      r.m[2] = x*w.m[2] + y*w.m[5] + z*w.m[8];

      x = m[3]; y = m[4]; z = m[5];
      r.m[3] = x*w.m[0] + y*w.m[3] + z*w.m[6];
      r.m[4] = x*w.m[1] + y*w.m[4] + z*w.m[7];
      r.m[5] = x*w.m[2] + y*w.m[5] + z*w.m[8];

      x = m[6]; y = m[7]; z = m[8];
      r.m[6] = x*w.m[0] + y*w.m[3] + z*w.m[6];
      r.m[7] = x*w.m[1] + y*w.m[4] + z*w.m[7];
      r.m[8] = x*w.m[2] + y*w.m[5] + z*w.m[8];

      return r;
    }

    /**************************************************************************/
    Matrix33<T> transpose() const{
      return Matrix33<T>(m[0], m[3], m[6],
                         m[1], m[4], m[7],
                         m[2], m[5], m[8]);
    }

    /**************************************************************************/
    Matrix33<T>& eye(){
      m[0] = (T)1; m[1] = (T)0; m[2] = (T)0;
      m[3] = (T)0; m[4] = (T)1; m[5] = (T)0;
      m[6] = (T)0; m[7] = (T)0; m[8] = (T)1;
      return *this;
    }

    /**************************************************************************/
    const T& operator[](int i) const{
      return m[i];
    }

    /**************************************************************************/
    T& operator[](int i){
      return m[i];
    }

#ifndef __ANDROID__
    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Matrix33<Y>& m);
#endif
  public:
    T m[9];
  };

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const Matrix33<T>& d){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        if(IsNan(d[i][j])){
          return true;
        }
      }
    }
    return false;
  }

  typedef Matrix33<float>  Matrix33f;
  typedef Matrix33<double> Matrix33d;


#ifndef __ANDROID__
  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os, const Matrix33<T>& m){
    os << std::endl << m.m[0] << "\t" << m.m[1] << "\t" << m.m[2] << std::endl;
    os << m.m[3] << "\t" << m.m[4] << "\t" << m.m[5] << std::endl;
    os << m.m[6] << "\t" << m.m[7] << "\t" << m.m[8] << std::endl << std::endl;
    return os;
  }
#endif
}

#endif/*MATRIX_HPP*/
