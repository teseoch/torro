/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef IECLINSOLVE_HPP
#define IECLINSOLVE_HPP

#include "math/SpMatrixC.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "datastructures/AdjacencyList.hpp"
#include "core/BenchmarkTimer.hpp"

namespace tsl{
  class CSVExporter;

  /**************************************************************************/
  enum PreconditionerApplication{LeftPreconditioning,
                                 RightPreconditioning};

  /**************************************************************************/
  /*Common interface for constraint evaluation. The actual
    implementation of these functions depend on the actual problem to
    solve.*/
  template<class T>
  class IECEvaluator{
  public:
    /**************************************************************************/
    virtual void evaluateStart(EvalStats&) = 0;

    /**************************************************************************/
    virtual bool evaluateAtConvergence(EvalStats&) = 0;

    /**************************************************************************/
    virtual bool evaluateAtConvergenceFinal(EvalStats&) = 0;

    /**************************************************************************/
    virtual bool evaluateRegular(int iter, T r1, T r2,
                                 bool* fine, EvalStats&) = 0;

    /**************************************************************************/
    virtual void evaluateAccept() = 0;
  };

  /**************************************************************************/
  /*Base class for constrained optimization solvers*/
  template<int N, class T>
  class IECLinSolve{
  public:
    /**************************************************************************/
    typedef typename List<VectorC<T> * >::Iterator VectorCIterator;

    /**************************************************************************/
    IECLinSolve(int d):dim(d){
      /*Create basic vectors*/
      mat = new SpMatrixC<N, T>(dim, dim);
      b   = new VectorC<T>(dim);
      x   = new VectorC<T>(dim);
      x1  = new VectorC<T>(dim);
      b2  = new VectorC<T>(dim);
      ab2 = new VectorC<T>(dim);
      r   = new VectorC<T>(dim);

      /*Register basic vectors*/
      vectors.append(b);
      vectors.append(x);
      vectors.append(x1);
      vectors.append(b2);
      vectors.append(ab2);
      vectors.append(r);

      /*Initialize a dummy, will be removed when the constraints are
        distributed.*/
      constraintConnectivity = new AdjacencyList(d,1);
      activeConstraints      = new Tree<int>();

      mat->activeConstraints = activeConstraints;

      b  ->clear();
      b2 ->clear();
      ab2->clear();
      x  ->clear();
      x1 ->clear();
      r  ->clear();

      exporter = 0;
      eps      = (T)1E-6;
      mnorm    = (T)1E-6;

      externalAllocatedMat = false;
      externalAllocatedb   = false;
      externalAllocatedb2  = false;
      externalAllocatedx   = false;

      iterCallBack = 0;

      firstIteration = false;
    }

    /**************************************************************************/
    virtual ~IECLinSolve(){
      if(!externalAllocatedMat){
        delete mat;
      }
      if(!externalAllocatedb){
        delete b;
      }
      if(!externalAllocatedb2){
        delete b2;
      }

      delete ab2;

      if(!externalAllocatedx){
        delete x;
      }

      delete x1;
      delete r;
      delete constraintConnectivity;
      delete activeConstraints;
    }

    /**************************************************************************/
    SpMatrixC<N, T>* getMatrix(){
      return mat;
    }

    /**************************************************************************/
    VectorC<T>* getb(){
      return b;
    }

    /**************************************************************************/
    VectorC<T>* getb2(){
      return b2;
    }

    /**************************************************************************/
    VectorC<T>* getx(){
      return x;
    }

    /**************************************************************************/
    VectorC<T>* getx1(){
      return x1;
    }

    /**************************************************************************/
    /*Register callback function to be called at each iteration,
      usefull for debugging.*/
    void setIterationCallBack(void(*cb)(void)){
      iterCallBack = cb;
    }

    /**************************************************************************/
    /*Override b, b2, x vectors and matrix with external allocated ones*/
    virtual void setb(VectorC<T>* vec){
      tslassert(vec->getSize() == b->getSize());
      if(!externalAllocatedb){
        vectors.remove(b);
        delete b;
      }
      b = vec;
      externalAllocatedb = true;
    }

    /**************************************************************************/
    virtual void setb2(VectorC<T>* vec){
      tslassert(vec->getSize() == b2->getSize());
      if(!externalAllocatedb2){
        vectors.remove(b2);
        delete b2;
      }
      b2 = vec;
      externalAllocatedb2 = true;
    }

    /**************************************************************************/
    virtual void setx(VectorC<T>* vec){
      tslassert(vec->getSize() == x->getSize());
      if(!externalAllocatedb){
        vectors.remove(x);
        delete x;
      }
      x = vec;
      externalAllocatedx = true;
    }

    /**************************************************************************/
    virtual void setMatrix(SpMatrixC<N, T>* m){
      tslassert(m->getWidth()  == mat->getWidth());
      tslassert(m->getHeight() == mat->getHeight());
      if(!externalAllocatedMat){
        delete mat;
      }

      mat = m;
      externalAllocatedMat = true;
    }

    /**************************************************************************/
    int getDim()const{
      return dim;
    }

    /**************************************************************************/
    T getEpsilon()const{
      return eps;
    }

    /**************************************************************************/
    T getRelativeEpsilon()const{
      return eps * bnorm + eps;
    }

    /**************************************************************************/
    /*Computes the norm of all multipliers involved*/
    void computeMultiplierNorm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *x, *x, Constraints);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      mnorm = Sqrt(tmp->cSum());
      this->timer.stop("vector_red");
    }

    /**************************************************************************/
    /*Computes norm of b*/
    void computeBNorm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *this->b, *this->b);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      bnorm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    /**************************************************************************/
    /*Computes norm of b + b2*/
    void computeBB2Norm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::add(*tmp, *this->b, *this->b2);
      VectorC<T>::mul(*tmp, *tmp, *tmp);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      bb2norm = Sqrt(tmp->oSum());
      this->timer.stop("vector_red");
    }

    /**************************************************************************/
    /*Computes norm of b2*/
    void computeB2Norm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *this->b2, *this->b2);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      b2norm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    /**************************************************************************/
    /*Computes norm of ab2*/
    void computeAB2Norm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *this->ab2, *this->ab2);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      ab2norm = Sqrt(tmp->sum());
      this->timer.stop("vector_red");
    }

    /**************************************************************************/
    /*Computes norm of residual r*/
    virtual T getResidualNorm(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *this->r, *this->r);
      this->timer.stop("vector");

      /*Total norm is 2 norm of unconstrained part and infinity norm
        of constraint part.*/
      this->timer.start("vector_red");
      T ret = Sqrt(tmp->oSum()) + this->r->getCAbsMax();
      this->timer.stop("vector_red");

      return ret;
    }

    /**************************************************************************/
    /*Computes norm of r (withouth considering the constraints)*/
    virtual T getResidualNormUnconstrained(VectorC<T>* tmp){
      this->timer.start("vector");
      VectorC<T>::mul(*tmp, *this->r, *this->r);
      this->timer.stop("vector");

      this->timer.start("vector_red");
      T ret =  Sqrt(tmp->oSum());
      this->timer.stop("vector_red");

      return ret;
    }

    /**************************************************************************/
    /*Computes infinity norm of constraint error*/
    virtual T getResidualNormC(VectorC<T>* tmp){
      this->timer.start("vector");
      T res = this->r->getCAbsMax();
      this->timer.stop("vector");

      return res;
    }

    /**************************************************************************/
    T getMultiplierNorm()const{
      return mnorm;
    }

    /**************************************************************************/
    void setExporter(CSVExporter* e){
      exporter = e;
    }

    /**************************************************************************/
    /*Checks if all constraint are resolved properly*/
    virtual bool checkConstraints(bool friction, EvalStats& stats) = 0;

    /**************************************************************************/
    /*Evaluates all constraints as specified by etype*/
    virtual bool evaluateConstraints(const EvalType& etype,
                                     EvalStats& stats) = 0;

    /**************************************************************************/
    /*Disables all constraints*/
    virtual void disableAllConstraints() = 0;

    /**************************************************************************/
    /*Solves the constrained optimization problem*/
    virtual void solve(int steps = 10000, T eps = (T)1e-6)=0;

    /**************************************************************************/
    /*Clears all values in vec associated with an inactive
      constraint*/
    virtual void resetInactive(VectorC<T>& vec){
      int size = vec.getSize();

      for(int i=0;i<this->mat->constraints.getNActive();i++){
        int idx = this->mat->constraints.activeIndex(i);

        if(this->mat->constraints[idx]->getStatus() != Active){
          int offset = this->mat->constraints[idx]->getOffset();
          int row_id = this->mat->constraints[idx]->row_id;

          for(int j=0;j<offset;j++){
            vec[size + row_id * offset + j] = 0;
          }
        }
      }
    }


    /**************************************************************************/
    /*Is called when constraints are changed. It copies the constraint
      values from the constraints into vector b. In overriden
      functions, updateConstraints can also perform a domain specific
      update of the constraints by adding or removing
      constraints. After that is done, this function should be
      called.*/
    virtual void updateConstraints(){
      int size = this->b->getSize();

      for(int i=0;i<this->mat->getNConstraints();i++){
        int idx = this->mat->constraints.activeIndex(i);

        AbstractRowConstraint<N, T>* c =
          (AbstractRowConstraint<N, T>*)this->mat->constraints[idx];

        for(int j=0;j<c->getOffset();j++){
          T val = c->getConstraintValue(j);
          (*this->b)[size + c->row_id * c->getOffset() + j] = val;
        }
      }
    }

    /**************************************************************************/
    /*Add constraint to matrix and vectors*/
    virtual int addConstraint(AbstractRowConstraint<N, T>* c){
      int idx = this->mat->addConstraint(c);

      if(c->getSize() == 0){
        error("Empty constraint added, so we can't maintain a connectivity graph of all constraints.");
      }

      this->mat->activeConstraints = activeConstraints;

      int updatedSize = this->mat->constraints.getSize();

      /*Initialize constraint*/
      c->init(this->mat);

      /*Update the size of all vectors used by this method (stored in
        vectors) and reset the values associated to the new
        constraint, and updates the values stored in vector b.*/
      int offset = c->getOffset();
      int row_id = c->row_id;
      int size   = this->b->getSize();

      VectorCIterator it = this->vectors.begin();

      while(it != this->vectors.end()){
        VectorC<T>* vv = *it++;

        /*Update size of vectors if needed*/
        vv->extendConstraints(updatedSize * c->getOffset());

        for(int j=0;j<offset;j++){
          if(vv == this->b){
            (*vv)[size + row_id * offset + j] = c->getConstraintValue(j);
          }else{
            (*vv)[size + row_id * offset + j] = 0;
          }
        }
      }

      /*Store connectivity information. Store only unique indices*/
      Tree<int> collection;
      for(int i=0;i<c->getSize();i++){
        if(c->getIndex(i) != Constraint::undefined){
          int index = c->getIndex(i);
          collection.uniqueInsert(index);
        }
      }

      /*Add connections*/
      Tree<int>::Iterator cit = collection.begin();
      while(cit != collection.end()){
        constraintConnectivity->connect(*cit++, idx);
      }

      return idx;
    }

    /**************************************************************************/
    /*Removes a constraint with a particular id*/
    virtual void removeConstraint(int id){
      AbstractRowConstraint<N, T>* c =
        (AbstractRowConstraint<N, T>*)this->mat->getConstraint(id);

      /*Get connected constraint ids*/
      Tree<int> collection;
      for(int i=0;i<c->getSize();i++){
        if(c->getIndex(i) != Constraint::undefined){
          int index = c->getIndex(i);
          collection.uniqueInsert(index);
        }
      }

      /*Disconnect*/
      Tree<int>::Iterator cit = collection.begin();
      while(cit != collection.end()){
        constraintConnectivity->disconnect(*cit++, id);
      }

      int row_id = c->row_id;
      int offset = c->getOffset();
      int size   = this->b->getSize();

      /*Remove constraint from matrix*/
      c->destroy(*this->b, *this->x);

      this->mat->removeConstraint(id);

      /*Set values associated with this constraint to 0 for all
        registered vectors*/
      VectorCIterator it = this->vectors.begin();
      while(it != this->vectors.end()){
        VectorC<T>* curVec = *it++;

        for(int j=0;j<offset;j++){
          (*curVec)[size + row_id * offset + j] = 0;
        }
      }
    }

    /**************************************************************************/
    List<VectorC<T>*> getVectorList() const{
      return vectors;
    }

    /**************************************************************************/
    AdjacencyList* getConnectivityList() const{
      return constraintConnectivity;
    }

    /**************************************************************************/
    void setFirstIteration(bool first){
      firstIteration = first;
    }

    /**************************************************************************/
    BenchmarkTimer* getTimer(){
      return &timer;
    }

  protected:
    /**************************************************************************/
    int dim;
    SpMatrixC<N, T>* mat;
    VectorC<T>* b;
    VectorC<T>* b2;
    VectorC<T>* ab2;
    VectorC<T>* x;
    VectorC<T>* x1;
    VectorC<T>* r;

    List<VectorC<T>*> vectors;         /*Contains all vectors present
                                         in the numerical method. When
                                         the addition of a constraint
                                         requires additional storage,
                                         these vectors are extended.*/

    AdjacencyList* constraintConnectivity; /*Stores information of
                                             constraints applied to
                                             the same node.*/

    Tree<int>* activeConstraints;  /*A tree containing indices of all
                                     active constraints, used by all
                                     associated vectors and matrices
                                     in order to find active
                                     constraints fast.*/

    CSVExporter* exporter;
    BenchmarkTimer timer;

    T eps;
    T mnorm;
    T bnorm;
    T b2norm;
    T bb2norm;
    T ab2norm;

    bool externalAllocatedMat;
    bool externalAllocatedb;
    bool externalAllocatedb2;
    bool externalAllocatedx;

    bool firstIteration;

    /*Called at the beginning of each iteration in order to interrupt
      the process in order to check things out*/
    void (*iterCallBack)(void);
  };
}

#endif/*IECLINSOLVE_HPP*/
