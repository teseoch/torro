/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTOPERATIONS_HPP
#define CONSTRAINTOPERATIONS_HPP

namespace tsl{
  /**************************************************************************/
  namespace Constraint{
    static const int undefined = -1;
  }

  /**************************************************************************/
  enum VectorOpMode{
    Normal,
    Constraints,
    NoConstraints
  };

  /**************************************************************************/
  enum MatrixMulMode{
    Conventional,    /*Performs only a multiplication using the
                       conventional matrix*/

    ConstraintsRow,  /*Performs only a multiplication with the row
                       constraints*/

    ConstraintsCol,  /*Performs only a multiplication with the column
                       constraints*/

    ConstraintsFull, /*Performs a multiplication only with the
                       constraints*/

    ConstraintsFullActive,
                     /*Performs a multiplication only with the
                       constraints and all constraints are considered
                       active*/

    Full,            /*Performs a multiplication with both constraints
                        and the conventional Matrix*/

    FullActive       /*Performs a multiplication with both constraints
                       and the conventional matrix, and all
                       constraints are considered active*/

  };

  /**************************************************************************/
  enum ConstraintStatus{
    Active,

    Inactive
  };

  /**************************************************************************/
  enum ConstraintFrictionStatus{
    Static,

    Kinetic
  };

  /**************************************************************************/
  enum ConstraintMatrixStatus{
    NoneActive, /*Used for unconstrained multiplication*/

    AllActive,  /*Used to evaluate all constraints*/

    Individual  /*Status determined from individual constraint status*/
    /*Add status for deactivating conventional matrix part*/
  };
}

#endif/*CONSTRAINTOPERATIONS_HPP*/
