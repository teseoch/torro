/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SPMATRIXBLOCK_HPP
#define SPMATRIXBLOCK_HPP

#if defined SSE2
#include "math/x86-sse/sse_spmatrix_intrinsics.hpp"
using namespace tsl::x86_sse2;
#elif defined NEON
#include "math/arm-neon/neon_spmatrix_intrinsics.hpp"
using namespace tsl::arm_neon;
#else
#include "math/default/default_spmatrix_intrinsics.hpp"
using namespace tsl::default_proc;
#endif

#include <ostream>
#include <iostream>
#include <string.h>

namespace tsl{
  /**************************************************************************/
  template <int N=DEFAULT_BLOCK_SIZE, class T=float>
  class SpMatrixBlock{
  public:

#if (defined SSE2)
#ifdef _WIN32
	__declspec(align(16)) T m[(uint)(N*N)];
#else
    T m[(uint)(N*N)] __attribute__((aligned(16)));
#endif
#else
    T m[(uint)(N*N)];
#endif

#ifdef SSE2
    /**************************************************************************/
    SpMatrixBlock(){
      tslassert(alignment(m, 16));   /*Check alignment*/
    }
#endif

    /**************************************************************************/
    void mul   (T f);

    /**************************************************************************/
    void div   (T f);

    /**************************************************************************/
    void cmpeq (T f);

    /**************************************************************************/
    void cmpneq(T f);

    /**************************************************************************/
    void cmplt (T f);

    /**************************************************************************/
    void cmple (T f);

    /**************************************************************************/
    void cmpgt (T f);

    /**************************************************************************/
    void cmpge (T f);

    /**************************************************************************/
    void mul   (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void div   (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmpeq (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmpneq(const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmplt (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmple (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmpgt (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void cmpge (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void add   (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void sub   (const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void addPos(const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void addNeg(const SpMatrixBlock<N, T>* const b);

    /**************************************************************************/
    void clear();

    /**************************************************************************/
    void set(T f);

    /**************************************************************************/
    /*reduceAllRows to a singular value using the sum operator*/
    void rowSumReduce();

    /**************************************************************************/
    void columnSumReduce();

    /**************************************************************************/
    void vectorMul   (const T* const vec);

    /**************************************************************************/
    void vectorMulAdd(const SpMatrixBlock<N, T>*const mat,
                      const T* const vec);

    /**************************************************************************/
    void vectorMulAddP(const SpMatrixBlock<N, T>*const mat,
                       const T* const vec);

    /**************************************************************************/
    void vectorMulAddN(const SpMatrixBlock<N, T>*const mat,
                       const T* const vec);

    /**************************************************************************/
    void vectorMulTranspose   (const T* const vec);

    /**************************************************************************/
    void vectorMulAddTranspose(const SpMatrixBlock<N, T>*const mat,
                               const T* const vec);

    /**************************************************************************/
    void vectorMulAddJacobi(const SpMatrixBlock<N, T>*const mat,
                            const T* const vec);

    /**************************************************************************/
    void matrixMulAdd(const SpMatrixBlock<N, T>* const A,
                      const SpMatrixBlock<N, T>* const B);

    /**************************************************************************/
    SpMatrixBlock<N, T>& operator=(const SpMatrixBlock<N, T>& mat);

    /**************************************************************************/
    template<int M, class TT>
    friend std::ostream& operator<<(std::ostream& os,
                                    const SpMatrixBlock<M, TT>& b);
  };

  /**************************************************************************/
  template<int N, class T>
  std::ostream& operator<<(std::ostream& os,
                           const SpMatrixBlock<N, T>& b){
    int index = 0;
    for(int h=0;h<N;h++){
      for(int w=0;w<N;w++){
        os << b.m[index++] << "\t";
      }
      os << std::endl;
    }
    return os;
  }

  /**************************************************************************/
  template<int N, class T>
  inline SpMatrixBlock<N, T>& SpMatrixBlock<N, T>::
  operator=(const SpMatrixBlock<N, T>& mat){
    spmatrix_block_load<N, T>(m, mat.m);
    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::mul(T f){
    spmatrix_block_muls<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::div(T f){
    spmatrix_block_divs<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpeq(T f){
    spmatrix_block_cmpeq<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpneq(T f){
    spmatrix_block_cmpneq<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmplt(T f){
    spmatrix_block_cmplt<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmple(T f){
    spmatrix_block_cmple<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpgt(T f){
    spmatrix_block_cmpgt<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpge(T f){
    spmatrix_block_cmpge<N, T>(m, m, f);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::mul(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_mul<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::div(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_div<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpeq(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmpeq<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpneq(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmpneq<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmplt(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmplt<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmple(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmple<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpgt(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmpgt<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::cmpge(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_cmpge<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::rowSumReduce(){
    spmatrix_block_row_sum_reduce<N, T>(m, m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::columnSumReduce(){
    spmatrix_block_column_sum_reduce<N, T>(m, m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::vectorMul(const T* const vec){
    spmatrix_block_vector_mul<N, T>(m, m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::
  vectorMulAdd(const SpMatrixBlock<N, T>*const mat,
               const T* const vec){
    spmatrix_block_vmadd<N, T>(m, m, mat->m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::
  vectorMulAddP(const SpMatrixBlock<N, T>*const mat,
                const T* const vec){
    spmatrix_block_vmadd_p<N, T>(m, m, mat->m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::
  vectorMulAddN(const SpMatrixBlock<N, T>*const mat,
                const T* const vec){
    spmatrix_block_vmadd_n<N, T>(m, m, mat->m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::vectorMulTranspose(const T* const vec){
    spmatrix_block_vector_mul_transpose<N, T>(m, m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::
  vectorMulAddTranspose(const SpMatrixBlock<N, T>*const mat,
                        const T* const vec){
    spmatrix_block_vmadd_transpose<N, T>(m, m, mat->m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::
  vectorMulAddJacobi(const SpMatrixBlock<N, T>*const mat,
                     const T* const vec){
    spmatrix_block_vmadd_jacobi<N, T>(m, m, mat->m, vec);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::matrixMulAdd(const SpMatrixBlock<N, T>*
                                                const A,
                                                const SpMatrixBlock<N, T>*
                                                const B){
    spmatrix_block_mmadd<N, T>(m, m, A->m, B->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::add(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_add<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::addPos(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_add_pos<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::addNeg(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_add_neg<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::sub(const SpMatrixBlock<N, T>* const b){
    spmatrix_block_sub<N, T>(m, m, b->m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::clear(){
    spmatrix_block_clear<N, T>(m);
  }

  /**************************************************************************/
  template<int N, class T>
  inline void SpMatrixBlock<N, T>::set(T f){
    spmatrix_block_set<N, T>(m, f);
  }
}

#endif/*SPMATRIXBLOCK_HPP*/
