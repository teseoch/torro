/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DEFAULT_MATRIX_INTRINSICS_HPP
#define DEFAULT_MATRIX_INTRINSICS_HPP

#include <string.h>

#include "math/default/default_vector_intrinsics.hpp"

namespace tsl{
  namespace default_proc{
    /**************************************************************************/
    template<class T>
    inline void matrix44_vmadd(T ra[4], T rb[4], T rc[4], T rd[4],
                               const T a1[4], const T b1[4],
                               const T c1[4], const T d1[4],
                               const T a2[4], const T b2[4],
                               const T c2[4], const T d2[4],
                               const T x[4]){
      ra[0] = a1[0] + a2[0] * x[0];
      ra[1] = a1[1] + a2[1] * x[1];
      ra[2] = a1[2] + a2[2] * x[2];
      ra[3] = a1[3] + a2[3] * x[3];

      rb[0] = b1[0] + b2[0] * x[0];
      rb[1] = b1[1] + b2[1] * x[1];
      rb[2] = b1[2] + b2[2] * x[2];
      rb[3] = b1[3] + b2[3] * x[3];

      rc[0] = c1[0] + c2[0] * x[0];
      rc[1] = c1[1] + c2[1] * x[1];
      rc[2] = c1[2] + c2[2] * x[2];
      rc[3] = c1[3] + c2[3] * x[3];

      rd[0] = d1[0] + d2[0] * x[0];
      rd[1] = d1[1] + d2[1] * x[1];
      rd[2] = d1[2] + d2[2] * x[2];
      rd[3] = d1[3] + d2[3] * x[3];
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_mul_vector4(T r[4],
                                     const T a[4], const T b[4],
                                     const T c[4], const T d[4],
                                     const T x[4]){
      r[0] = vector4_dot(a, x);
      r[1] = vector4_dot(b, x);
      r[2] = vector4_dot(c, x);
      r[3] = vector4_dot(d, x);
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_mul_matrix44(T ra[4], T rb[4],
                                      T rc[4], T rd[4],
                                      const T aa[4], const T ab[4],
                                      const T ac[4], const T ad[4],
                                      const T ba[4], const T bb[4],
                                      const T bc[4], const T bd[4]){
      T x, y, z, ww;

      x = aa[0]; y=aa[1]; z=aa[2]; ww=aa[3];
      ra[0] = x*ba[0] + y*bb[0] + z*bc[0] + ww*bd[0];
      ra[1] = x*ba[1] + y*bb[1] + z*bc[1] + ww*bd[1];
      ra[2] = x*ba[2] + y*bb[2] + z*bc[2] + ww*bd[2];
      ra[3] = x*ba[3] + y*bb[3] + z*bc[3] + ww*bd[3];

      x=ab[0]; y=ab[1]; z=ab[2]; ww=ab[3];
      rb[0] = x*ba[0] + y*bb[0] + z*bc[0] + ww*bd[0];
      rb[1] = x*ba[1] + y*bb[1] + z*bc[1] + ww*bd[1];
      rb[2] = x*ba[2] + y*bb[2] + z*bc[2] + ww*bd[2];
      rb[3] = x*ba[3] + y*bb[3] + z*bc[3] + ww*bd[3];

      x=ac[0]; y=ac[1]; z=ac[2]; ww=ac[3];
      rc[0] = x*ba[0] + y*bb[0] + z*bc[0] + ww*bd[0];
      rc[1] = x*ba[1] + y*bb[1] + z*bc[1] + ww*bd[1];
      rc[2] = x*ba[2] + y*bb[2] + z*bc[2] + ww*bd[2];
      rc[3] = x*ba[3] + y*bb[3] + z*bc[3] + ww*bd[3];

      x=ad[0]; y=ad[1]; z=ad[2]; ww=ad[3];
      rd[0] = x*ba[0] + y*bb[0] + z*bc[0] + ww*bd[0];
      rd[1] = x*ba[1] + y*bb[1] + z*bc[1] + ww*bd[1];
      rd[2] = x*ba[2] + y*bb[2] + z*bc[2] + ww*bd[2];
      rd[3] = x*ba[3] + y*bb[3] + z*bc[3] + ww*bd[3];
    }

#if 1
#define MMA a[0]
#define MMB a[1]
#define MMC a[2]
#define MMD a[3]
#define MME b[0]
#define MMF b[1]
#define MMG b[2]
#define MMH b[3]
#define MMI c[0]
#define MMJ c[1]
#define MMK c[2]
#define MML c[3]
#define MMM d[0]
#define MMN d[1]
#define MMO d[2]
#define MMP d[3]
#endif

    /**************************************************************************/
    template<class T>
    inline T matrix44_determinant(const T a[4],
                                  const T b[4],
                                  const T c[4],
                                  const T d[4]){
      T kplo = MMK*MMP - MML*MMO;
      T lnjp = MML*MMN - MMJ*MMP;
      T jokn = MMJ*MMO - MMK*MMN;

      T ra0 = MMF*( kplo) + MMG*( lnjp) + MMH*( jokn);

      T iplm = MMI*MMP - MML*MMM;
      T kmio = MMK*MMM - MMI*MMO;

      T rb0 = MME*(-kplo) + MMG*( iplm) + MMH*( kmio);

      T injm = MMI*MMN - MMJ*MMM;

      T rc0 = MME*(-lnjp) + MMF*(-iplm) + MMH*( injm);

      T rd0 = MME*(-jokn) + MMF*(-kmio) + MMG*(-injm);

      return ((a[0]*ra0 + a[1]*rb0) + (a[2]*rc0 + a[3]*rd0));
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_transpose(T ra[4], T rb[4],
                                   T rc[4], T rd[4],
                                   const T a[4], const T b[4],
                                   const T c[4], const T d[4]){
      ra[0] = a[0];
      ra[1] = b[0];
      ra[2] = c[0];
      ra[3] = d[0];

      rb[0] = a[1];
      rb[1] = b[1];
      rb[2] = c[1];
      rb[3] = d[1];

      rc[0] = a[2];
      rc[1] = b[2];
      rc[2] = c[2];
      rc[3] = d[2];

      rd[0] = a[3];
      rd[1] = b[3];
      rd[2] = c[3];
      rd[3] = d[3];
    }

#ifndef DET2
#define DET2(a00, a01,                              \
             a10, a11) ((a00)*(a11) - (a10)*(a01))
#endif
#ifndef DET3
#define DET3(a00, a01, a02,                                     \
             a10, a11, a12,                                     \
             a20, a21, a22) ((a00)*DET2(a11, a12, a21, a22) -	\
                             (a10)*DET2(a01, a02, a21, a22) +	\
                             (a20)*DET2(a01, a02, a11, a12))
#endif

    /**************************************************************************/
    template<class T>
    inline void matrix44_inverse(T* det,
                                 T ra[4], T rb[4],
                                 T rc[4], T rd[4],
                                 const T a[4], const T b[4],
                                 const T c[4], const T d[4]){

      T kplo = MMK*MMP - MML*MMO;
      T lnjp = MML*MMN - MMJ*MMP;
      T jokn = MMJ*MMO - MMK*MMN;

      ra[0] = MMF*( kplo) + MMG*( lnjp) + MMH*( jokn);
      ra[1] = MMB*(-kplo) + MMC*(-lnjp) + MMD*(-jokn);

      T chdg = MMC*MMH - MMD*MMG;
      T dfbh = MMD*MMF - MMB*MMH;
      T bgcf = MMB*MMG - MMC*MMF;

      ra[2] = MMN*( chdg) + MMO*( dfbh) + MMP*( bgcf);
      ra[3] = MMJ*(-chdg) + MMK*(-dfbh) + MML*(-bgcf);

      T iplm = MMI*MMP - MML*MMM;
      T kmio = MMK*MMM - MMI*MMO;

      rb[0] = MME*(-kplo) + MMG*( iplm) + MMH*( kmio);
      rb[1] = MMA*( kplo) + MMC*(-iplm) + MMD*(-kmio);

      T ahde = MMA*MMH - MMD*MME;
      T ceag = MMC*MME - MMA*MMG;

      rb[2] = MMM*(-chdg) + MMO*( ahde) + MMP*( ceag);
      rb[3] = MMI*( chdg) + MMK*(-ahde) + MML*(-ceag);

      T injm = MMI*MMN - MMJ*MMM;

      rc[0] = MME*(-lnjp) + MMF*(-iplm) + MMH*( injm);
      rc[1] = MMA*( lnjp) + MMB*( iplm) + MMD*(-injm);

      T afbe = MMA*MMF - MMB*MME;

      rc[2] = MMM*(-dfbh) + MMN*(-ahde) + MMP*( afbe);
      rc[3] = MMI*( dfbh) + MMJ*( ahde) + MML*(-afbe);


      rd[0] = MME*(-jokn) + MMF*(-kmio) + MMG*(-injm);
      rd[1] = MMA*( jokn) + MMB*( kmio) + MMC*( injm);

      rd[2] = MMM*(-bgcf) + MMN*(-ceag) + MMO*(-afbe);
      rd[3] = MMI*( bgcf) + MMJ*( ceag) + MMK*( afbe);

      *det = ((a[0]*ra[0] +
               a[1]*rb[0]) +
              (a[2]*rc[0] +
               a[3]*rd[0]));

      T det2 = MultiplyIdentity(T()) / *det;

      vector4_muls(ra, ra, det2);
      vector4_muls(rb, rb, det2);
      vector4_muls(rc, rc, det2);
      vector4_muls(rd, rd, det2);
    }

    /**************************************************************************/
    template<class T>
    inline void matrix44_adjugate(T ra[4], T rb[4],
                                  T rc[4], T rd[4],
                                  const T a[4], const T b[4],
                                  const T c[4], const T d[4]){

      T kplo = MMK*MMP - MML*MMO;
      T lnjp = MML*MMN - MMJ*MMP;
      T jokn = MMJ*MMO - MMK*MMN;

      ra[0] = MMF*( kplo) + MMG*( lnjp) + MMH*( jokn);
      ra[1] = MMB*(-kplo) + MMC*(-lnjp) + MMD*(-jokn);

      T chdg = MMC*MMH - MMD*MMG;
      T dfbh = MMD*MMF - MMB*MMH;
      T bgcf = MMB*MMG - MMC*MMF;

      ra[2] = MMN*( chdg) + MMO*( dfbh) + MMP*( bgcf);
      ra[3] = MMJ*(-chdg) + MMK*(-dfbh) + MML*(-bgcf);

      T iplm = MMI*MMP - MML*MMM;
      T kmio = MMK*MMM - MMI*MMO;

      rb[0] = MME*(-kplo) + MMG*( iplm) + MMH*( kmio);
      rb[1] = MMA*( kplo) + MMC*(-iplm) + MMD*(-kmio);

      T ahde = MMA*MMH - MMD*MME;
      T ceag = MMC*MME - MMA*MMG;

      rb[2] = MMM*(-chdg) + MMO*( ahde) + MMP*( ceag);
      rb[3] = MMI*( chdg) + MMK*(-ahde) + MML*(-ceag);

      T injm = MMI*MMN - MMJ*MMM;

      rc[0] = MME*(-lnjp) + MMF*(-iplm) + MMH*( injm);
      rc[1] = MMA*( lnjp) + MMB*( iplm) + MMD*(-injm);

      T afbe = MMA*MMF - MMB*MME;

      rc[2] = MMM*(-dfbh) + MMN*(-ahde) + MMP*( afbe);
      rc[3] = MMI*( dfbh) + MMJ*( ahde) + MML*(-afbe);


      rd[0] = MME*(-jokn) + MMF*(-kmio) + MMG*(-injm);
      rd[1] = MMA*( jokn) + MMB*( kmio) + MMC*( injm);

      rd[2] = MMM*(-bgcf) + MMN*(-ceag) + MMO*(-afbe);
      rd[3] = MMI*( bgcf) + MMJ*( ceag) + MMK*( afbe);
    }
  }
}
#endif/*DEFAULT_MATRIX_INTRINSICS_HPP*/
