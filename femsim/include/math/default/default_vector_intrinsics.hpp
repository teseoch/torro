/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DEFAULT_VECTOR_INTRINSICS_HPP
#define DEFAULT_VECTOR_INTRINSICS_HPP

#include <string.h>

#ifdef __GNUG__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
#endif

namespace tsl{
  namespace default_proc{
    /**************************************************************************/
    template<class T>
    inline void vector4_load_zero(T r[4]){
      r[0] = r[1] = r[2] = r[3] = SumIdentity(T());
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_load(T r[4],
                             const T a[4]){
      r[0] = a[0];
      r[1] = a[1];
      r[2] = a[2];
      r[3] = a[3];
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_load(T r[4],
                             const T& a,
                             const T& b,
                             const T& c,
                             const T& d){
      r[0] = a;
      r[1] = b;
      r[2] = c;
      r[3] = d;
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_load(T r[4],
                             const T& a){
      r[0] = a;
      r[1] = a;
      r[2] = a;
      r[3] = a;
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_muls(T r[4],
                             const T a[4],
                             const T& n){
      r[0] = a[0] * n;
      r[1] = a[1] * n;
      r[2] = a[2] * n;
      r[3] = a[3] * n;
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_mul(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = a[0] * b[0];
      r[1] = a[1] * b[1];
      r[2] = a[2] * b[2];
      r[3] = a[3] * b[3];
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_divs(T r[4],
                             const T a[4],
                             const T& n){
      r[0] = a[0] / n;
      r[1] = a[1] / n;
      r[2] = a[2] / n;
      r[3] = a[3] / n;
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_div(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = a[0] / b[0];
      r[1] = a[1] / b[1];
      r[2] = a[2] / b[2];
      r[3] = a[3] / b[3];
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_add(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = a[0] + b[0];
      r[1] = a[1] + b[1];
      r[2] = a[2] + b[2];
      r[3] = a[3] + b[3];
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_sub(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = a[0] - b[0];
      r[1] = a[1] - b[1];
      r[2] = a[2] - b[2];
      r[3] = a[3] - b[3];
    }

    /**************************************************************************/
    /*dot and sum are computed in the same order as in the sse2 equivalent*/
    template<class T>
    inline T vector4_dot(const T a[4],
                         const T b[4]){
      return ( ((a[0]*b[0]) + (a[2]*b[2])) +
               ((a[1]*b[1]) + (a[3]*b[3]))   );
    }

    /**************************************************************************/
    template<class T>
    inline T vector4_sum(const T a[4]){
      return (a[0] + a[2]) + (a[1] + a[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_cross(T r[4],
                              const T a[4],
                              const T b[4]){
      r[0] = a[1]*b[2] - a[2]*b[1];
      r[1] = a[2]*b[0] - a[0]*b[2];
      r[2] = a[0]*b[1] - a[1]*b[0];
      r[3] = SumIdentity(T());
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_eq(T r[4],
                           const T a[4],
                           const T b[4]){
      r[0] = (a[0] == b[0]);
      r[1] = (a[1] == b[1]);
      r[2] = (a[2] == b[2]);
      r[3] = (a[3] == b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_neq(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = (a[0] != b[0]);
      r[1] = (a[1] != b[1]);
      r[2] = (a[2] != b[2]);
      r[3] = (a[3] != b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_lt(T r[4],
                           const T a[4],
                           const T b[4]){
      r[0] = (a[0] < b[0]);
      r[1] = (a[1] < b[1]);
      r[2] = (a[2] < b[2]);
      r[3] = (a[3] < b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_le(T r[4],
                           const T a[4],
                           const T b[4]){
      r[0] = (a[0] <= b[0]);
      r[1] = (a[1] <= b[1]);
      r[2] = (a[2] <= b[2]);
      r[3] = (a[3] <= b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_gt(T r[4],
                           const T a[4],
                           const T b[4]){
      r[0] = (a[0] > b[0]);
      r[1] = (a[1] > b[1]);
      r[2] = (a[2] > b[2]);
      r[3] = (a[3] > b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_ge(T r[4],
                           const T a[4],
                           const T b[4]){
      r[0] = (a[0] >= b[0]);
      r[1] = (a[1] >= b[1]);
      r[2] = (a[2] >= b[2]);
      r[3] = (a[3] >= b[3]);
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_max(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = (a[0] > b[0])?a[0]:b[0];
      r[1] = (a[1] > b[1])?a[1]:b[1];
      r[2] = (a[2] > b[2])?a[2]:b[2];
      r[3] = (a[3] > b[3])?a[3]:b[3];
    }

    /**************************************************************************/
    template<class T>
    inline void vector4_min(T r[4],
                            const T a[4],
                            const T b[4]){
      r[0] = (a[0] < b[0])?a[0]:b[0];
      r[1] = (a[1] < b[1])?a[1]:b[1];
      r[2] = (a[2] < b[2])?a[2]:b[2];
      r[3] = (a[3] < b[3])?a[3]:b[3];
    }

    /**************************************************************************/
    template<class T>
    inline T vector4_reduce_max(const T a[4]){
      T r1 = a[0] > a[1] ? a[0] : a[1];
      T r2 = a[2] > a[3] ? a[2] : a[3];
      return r1 > r2 ? r1 : r2;
    }

    /**************************************************************************/
    template<class T>
    inline T vector4_reduce_min(const T a[4]){
      T r1 = a[0] < a[1] ? a[0] : a[1];
      T r2 = a[2] < a[3] ? a[2] : a[3];
      return r1 < r2 ? r1 : r2;
    }
  }
}

#ifdef __GNUG__
#pragma GCC diagnostic pop
#endif

#endif//DEFAULT_VECTOR_INTRINSICS_HPP
