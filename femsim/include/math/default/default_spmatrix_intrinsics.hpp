/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DEFAULT_SPMATRIX_INTRINSICS
#define DEFAULT_SPMATRIX_INTRINSICS

#include <string.h>

#ifdef __GNUG__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
#endif

namespace tsl{
  namespace default_proc{
    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_load(T r[N*N],
                                    const T a[N*N]){
      memcpy((void*)r, (void*)a, sizeof(T) * N * N);
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_muls(T r[N*N],
                                    const T a[N*N],
                                    T f){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j]*f;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_divs(T r[N*N],
                                    const T a[N*N],
                                    T f){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j]/f;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_adds(T r[N*N],
                                    const T a[N*N],
                                    T f){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j]+f;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_subs(T r[N*N],
                                    const T a[N*N],
                                    T f){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j]-f;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpeq(T r[N*N],
                                     const T a[N*N],
                                     T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] == f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpneq(T r[N*N],
                                      const T a[N*N],
                                      T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] != f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmplt(T r[N*N],
                                     const T a[N*N],
                                     T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] < f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmple(T r[N*N],
                                     const T a[N*N],
                                     T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] <= f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpgt(T r[N*N],
                                     const T a[N*N],
                                     T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] > f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpge(T r[N*N],
                                     const T a[N*N],
                                     T f){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] >= f);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_mul(T r[N*N],
                                   const T a[N*N],
                                   const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j] * b[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_div(T r[N*N],
                                   const T a[N*N],
                                   const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j] / b[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpeq(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] == b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpneq(T r[N*N],
                                      const T a[N*N],
                                      const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] != b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmplt(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] < b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmple(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] <= b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpgt(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] > b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_cmpge(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = (a[j] >= b[j]);
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_row_sum_reduce(T r[N*N],
                                              const T a[N*N]){
      for(uint i=0;i<N;i++){
        T sum = SumIdentity(T());
        for(uint j=0;j<N;j++){
          sum += a[i*N + j];
          r[i*N + j] = SumIdentity(T());
        }

        r[i*N] = sum;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_column_sum_reduce(T r[N*N],
                                                 const T a[N*N]){
      for(uint j=0;j<N;j++){
        T sum = SumIdentity(T());
        for(uint i=0;i<N;i++){
          sum += a[i*N + j];
          r[i*N + j] = SumIdentity(T());
        }
        r[N*(N-1)+j] = sum;
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vector_mul(T r[N*N],
                                          const T a[N*N],
                                          const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          r[j*N + i] = a[j*N + i] * v[i];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vector_mul_transpose(T r[N*N],
                                                    const T a[N*N],
                                                    const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          r[j*N + i] = a[j*N + i] * v[j];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vmadd(T r[N*N],
                                     const T a[N*N],
                                     const T b[N*N],
                                     const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          r[j*N + i] = a[j*N + i] + b[j*N + i] * v[i];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vmadd_p(T r[N*N],
                                       const T a[N*N],
                                       const T b[N*N],
                                       const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          T res = b[j*N + i] * v[i];
          res = res>SumIdentity(T())?res:SumIdentity(T());
          r[j*N + i] = a[j*N + i] + res;
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vmadd_n(T r[N*N],
                                       const T a[N*N],
                                       const T b[N*N],
                                       const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          T res = b[j*N + i] * v[i];
          res = res<SumIdentity(T())?res:SumIdentity(T());
          r[j*N + i] = a[j*N + i] + res;
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vmadd_transpose(T r[N*N],
                                               const T a[N*N],
                                               const T b[N*N],
                                               const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          r[j*N + i] = a[j*N + i] + b[j*N + i] * v[j];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_vmadd_jacobi(T r[N*N],
                                            const T a[N*N],
                                            const T b[N*N],
                                            const T v[N]){
      for(uint j=0;j<N;j++){
        for(uint i=0;i<N;i++){
          if(i == j){
            r[j*N + i] = a[j*N + i];
          }else{
            r[j*N + i] = a[j*N + i] + b[j*N + i] * v[i];
          }
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_mmadd(T r[N*N],
                                     const T p[N*N],
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint i=0;i<N;i++){
        for(uint j=0;j<N;j++){
          T sum = SumIdentity(T());
          for(uint k=0;k<N;k++){
            sum += a[i*N + k] * b[k*N + j];
          }
          r[i*N+j] = sum + p[i*N+j];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_add(T r[N*N],
                                   const T a[N*N],
                                   const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j] + b[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_add_pos(T r[N*N],
                                       const T a[N*N],
                                       const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        if(b[j] > SumIdentity(T())){
          r[j] = a[j] + b[j];
        }else{
          r[j] = a[j];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_add_neg(T r[N*N],
                                       const T a[N*N],
                                       const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        if(b[j] < SumIdentity(T())){
          r[j] = a[j] + b[j];
        }else{
          r[j] = a[j];
        }
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_sub(T r[N*N],
                                   const T a[N*N],
                                   const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j] - b[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_madd(T r[N*N],
                                    const T a[N*N],
                                    const T b[N*N],
                                    const T c[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = a[j] * b[j] + c[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_msadd(T r[N*N],
                                     T f,
                                     const T a[N*N],
                                     const T b[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = f * a[j] + b[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_msadd2(T r[N*N],
                                      T f,
                                      const T a[N*N],
                                      const T b[N*N],
                                      const T d[N*N]){
      for(uint j=0;j<N*N;j++){
        r[j] = f * a[j] + b[j] + d[j];
      }
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_clear(T r[N*N]){
      memset((void*)r, 0, sizeof(T)*N*N);
    }

    /**************************************************************************/
    template<uint N, class T>
    inline void spmatrix_block_set(T r[N*N],
                                   T f){
      for(uint j=0;j<N*N;j++){
        r[j] = f;
      }
    }
  }
}

#ifdef __GNUG__
#pragma GCC diagnostic pop
#endif

#endif/*DEFAULT_SPMATRIX_INTRINSICS*/
