/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "math/Math.hpp"
#include "math/Matrix44.hpp"
#include "math/Vector.hpp"
#include "math/SpMatrixBlock.hpp"

namespace tsl{

  /**************************************************************************/
  /*Templatized matrix implementation: can only be used iff the
    dimensions of the matrix are known at compile time. Due to this,
    the code is better optimized by the compiler, resulting in faster
    execution.*/
  template<int N, int M, class T=float>
  class MatrixT{
  public:
    /**************************************************************************/
    MatrixT(){
      clear();
    }

    /**************************************************************************/
    MatrixT(bool b){
      /*No initialization*/
    }

    /**************************************************************************/
    /*Copy constructor*/
    MatrixT(const MatrixT<N, M, T>& m){
      memcpy((void*)data, (void*)m.data, sizeof(T) * N * M);
    }

    /**************************************************************************/
    /*Assignment operator*/
    MatrixT<N, M, T>& operator=(const MatrixT<N, M, T>& m){
      if(this != &m){
        memcpy((void*)data, (void*)m.data, sizeof(T) * N * M);
      }
      return *this;
    }

    /**************************************************************************/
    void clear(){
      memset((void*)data, 0, sizeof(T) * N * M);
    }

    /**************************************************************************/
    MatrixT<N, M, T>& operator=(T f){
      for(int i=0;i<N*M;i++){
        data[i] = f;
      }
      return *this;
    }

    /**************************************************************************/
    T* operator[](int i){
      return &(data[i * M]);
    }

    /**************************************************************************/
    const T* operator[](int i)const{
      return &(data[i * M]);
    }

    /**************************************************************************/
    T& element(int x,
               int y){
      return data[x * M + y];
    }

    /**************************************************************************/
    const T& element(int x,
                     int y)const{
      return data[x * M + y];
    }

    /**************************************************************************/
    T& elementv(int x){
      return data[x * M];
    }

    /**************************************************************************/
    const T& elementv(int x)const{
      return data[x * M];
    }

    /**************************************************************************/
    /*Needed since multiplication operator technically operates on
      different classes*/
    template<int NN, int MM, class TT>
    friend class MatrixT;

    /**************************************************************************/
    template<int P>
    MatrixT<N, P, T> operator*(const MatrixT<M, P, T>& m)const{
      MatrixT<N, P, T> mat(false);

      for(int i=0;i<P;i++){
        for(int j=0;j<N;j++){
          T r = (T)0.0;

          for(int k=0;k<M;k++){
            r += data[j*M + k] * m.data[k*P+i];
          }

          mat.data[j*P+i] = r;
        }
      }

      return mat;
    }

    /**************************************************************************/
    MatrixT<N, M, T> mulBlockR(Matrix44<T>& bl,
                               int bs){
      MatrixT<N, M, T> mat(false);

      for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
          T r = SumIdentity(T());

          for(int k=i/bs;k<(i+1/bs);k++){
            r += data[j * M + k] * bl[k % bs][i % bs];
          }

          mat.data[j * M + i] = r;
        }
      }

      return mat;
    }

    /**************************************************************************/
    MatrixT<N, M, T> mulBlockL(Matrix44<T>& bl,
                               int bs){
      MatrixT<N, M, T> mat(false);

      for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
          T r = SumIdentity(T());

          for(int k=j/bs;k<(j+1/bs);k++){
            r += bl[j % bs][k % bs] * data[k * M + i];
          }

          mat.data[j * M + i] = r;
        }
      }

      return mat;
    }

    /**************************************************************************/
    /*Matrix vector multiplication*/
    Vector<T> operator*(const Vector<T>& v)const{
      Vector<T> r(M);

      for(int j=0;j<N;j++){
        T val = SumIdentity(T());

        for(int i=0;i<M;i++){
          val += data[j * N + i] * v[i];
        }

        r[j] = val;
      }
      return r;
    }

    /**************************************************************************/
    MatrixT<M, N, T> transpose() const{
      MatrixT<M, N, T> tr;

      for(int j=0;j<N;j++){
        for(int i=0;i<M;i++){
          tr.data[i * N + j] = data[j * M + i];
        }
      }
      return tr;
    }

    /**************************************************************************/
    MatrixT<N, M, T>& operator*=(const T n){
      for(int i=0;i<N*M;i++){
        data[i] *= n;
      }
      return *this;
    }

    /**************************************************************************/
    MatrixT<N, M, T>& operator/=(const T n){
      for(int i=0;i<N*M;i++){
        data[i] /= n;
      }
      return *this;
    }

    /**************************************************************************/
    MatrixT<N, M, T>& operator-=(const MatrixT<N, M, T>& m){
      for(int i=0;i<N*M;i++){
        data[i] -= m.data[i];
      }
      return *this;
    }

    /**************************************************************************/
    MatrixT<N, M, T>& operator+=(const MatrixT<N, M, T>& m){
      for(int i=0;i<N*M;i++){
        data[i] += m.data[i];
      }

      return *this;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator-(const MatrixT<N, M, T>& m)const{
      MatrixT<N, M, T> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] - m.data[i];
      }
      return r;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator+(const MatrixT<N, M, T>& m)const{
      MatrixT<N, M, T> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] + m.data[i];
      }
      return r;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator+() const{
      return *this;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator-() const{
      MatrixT<N, M, T> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = -data[i];
      }
      return r;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator/(const T n)const{
      MatrixT<N, M, T> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] / n;
      }
      return r;
    }

    /**************************************************************************/
    MatrixT<N, M, T> operator*(const T n)const{
      MatrixT<N, M, T> r(false);
      for(int i=0;i<N*M;i++){
        r.data[i] = data[i] * n;
      }
      return r;
    }

    /**************************************************************************/
    template<int NN, int MM, class TT>
    friend std::ostream& operator<<(std::ostream& os,
                                    const MatrixT<NN, MM, TT>& v);

    protected:
    /**************************************************************************/
    /*Storage*/
#ifdef _WIN32
	__declspec(align(16)) T data[(uint)(N*M)];
#else
    T data[(uint)(N*M)] __attribute__ ((aligned (16)));
#endif
  };

  /**************************************************************************/
  template<int N, int M, class T>
  std::ostream& operator<<(std::ostream& os,
                           const MatrixT<N, M, T>& v){
    os << "MatrixT(" << N << ", " << M << ")" << std::endl;
    for(int i=0;i<N;i++){
      for(int j=0;j<M;j++){
        std::cout << v[i][j] << "\t";
      }
      std::cout << std::endl;
    }
    return os;
  }

  /**************************************************************************/
  template<int N, int M, class T>
  inline bool IsNan(const MatrixT<N, M, T>& mat){
    for(int i=0;i<N;i++){
      for(int j=0;j<M;j++){
        if(IsNan(mat[i][j])){
          return true;
        }
      }
    }
    return false;
  }

#if 0
  /**************************************************************************/
  template<int N, int M, class T>
  void eig(const MatrixT<N, M, T>& A, MatrixT<N, M, T>& vectors,
           MatrixT<N, 1, T>& eigenvalues){
    MatrixT<N, M, T> AA = A;
    MatrixT<N, M, T> vecr;

    MatrixT<N, 1, T> val;

    for(int i=0;i<N;i++){
      val[i][0] = rayleighQuotientIteration(AA, vecr[i], i);
      vecr[i] /= vecr[i].length();

      /*Wielandt deflation*/
    }
  }
#endif

  /*Non templatized matrix implementation: can be used if the
    dimensions of the matrix are unknown at compile time. The code is
    almost identical to the templatized version, but is less optimized
    by the compiler*/

  /**************************************************************************/
  template<class T=float, int B=2>
  class Matrix{
  public:
    /**************************************************************************/
    class RowProxy{
    public:
      /************************************************************************/
      T& operator[](int col){
        tslassert(col >= 0 && col < mat->height);
        return mat->data[(row/B)*mat->blocks_w + col/B].m[(row%B) * B + col%B];
      }

      /************************************************************************/
      const T& operator[](int col)const{
        tslassert(col >= 0 && col < mat->height);
        return mat->data[(row/B)*mat->blocks_w + col/B].m[(row%B) * B + col%B];
      }

      Matrix<T, B>* mat;
      int row;
    };

    /**************************************************************************/
    Matrix(int h,
           int w){
      blocks_w = (w / B) + ((w % B) > 0 ? 1 : 0);
      blocks_h = (h / B) + ((h % B) > 0 ? 1 : 0);

      vsize = ((w / B) + ((w % B) > 0 ? 1 : 0))*B;

      width  = w;
      height = h;

      data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

      alignedMalloc((void**)&vr, 16, (ulong)blocks_w*B*sizeof(T));

      tslassert(alignment(vr, 16));

      clear();
    }

    /**************************************************************************/
    /*Copy constructor*/
    Matrix(const Matrix<T, B>& m){
      blocks_w = m.blocks_w;
      blocks_h = m.blocks_h;

      width = m.width;
      height = m.height;

      vsize = m.vsize;

      data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

      alignedMalloc((void**)&vr, 16, (ulong)blocks_w*B*sizeof(T));

      tslassert(alignment(vr, 16));

      memcpy((void*)data, (void*)m.data, sizeof(SpMatrixBlock<B, T>) *
             (uint)(blocks_w * blocks_h));
    }

    /**************************************************************************/
    ~Matrix(){
      delete[] data;
      alignedFree(vr);
    }

    /**************************************************************************/
    /*Assignment operator*/
    Matrix<T, B>& operator=(const Matrix<T, B>& m){
      message("assignment operator");
      if(this != &m){

        delete[] data;

        alignedFree(vr);

        blocks_w = m.blocks_w;
        blocks_h = m.blocks_h;

        width = m.width;
        height = m.height;

        vsize = m.vsize;

        data = new SpMatrixBlock<B, T>[blocks_w * blocks_h];

        alignedMalloc((void**)&vr, 16, blocks_w*B*sizeof(T));

        tslassert(alignment(vr, 16));

        memcpy((void*)data, (void*)m.data, sizeof(SpMatrixBlock<B, T>) * blocks_w * blocks_h);
      }
      return *this;
    }

    /**************************************************************************/
    void clear(){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].clear();
        }
      }
    }

    /**************************************************************************/
    Matrix<T, B>& operator=(T f){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].set(f);
        }
      }
      return *this;
    }

    /**************************************************************************/
    RowProxy operator[](int i){
      RowProxy r;
      r.mat = this;
      r.row = i;
      return r;
    }

    /**************************************************************************/
    const RowProxy operator[](int i)const{
      RowProxy r;
      r.mat = (Matrix<T, B>*)this;
      r.row = i;
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> operator*(const Matrix<T, B>& m){
      Matrix<T, B> mat(height, m.width);

      for(int i=0;i<m.width;i++){

        for(int j=0;j<width;j++){
          vr[j] = m.data[(j/B)*m.blocks_w + (i/B)].m[(j%B)*B+i%B];
        }

        /*Vector loaded*/

        /*Multiply blocks with vector*/

        /*Block-rows*/
        for(int j=0;j<blocks_h;j++){
          SpMatrixBlock<B, T> block;
          block.clear();

          for(int k=0;k<blocks_w;k++){
            block.vectorMulAdd(&(data[j*blocks_w + k]), vr+k*B);
          }

          block.rowSumReduce();

          /*Store data*/

          for(int k=0;k<B;k++){
            mat.data[j*m.blocks_w + i/B].m[k*B+i%B] =
              block.m[k*B];
          }
        }
      }
      return mat;
    }

    /**************************************************************************/
    /*Matrix vector multiplication*/
    Vector<T> operator*(const Vector<T>& v)const{
      tslassert(v.getSize() == width);
      Vector<T> r(height);

      const T* vdata = v.getData();

      for(int i=0;i<blocks_h;i++){
        SpMatrixBlock<B, T> block;
        block.clear();

        for(int j=0;j<blocks_w;j++){
          block.vectorMulAdd(&(data[i*blocks_w + j]), vdata+j*B);
        }

        block.rowSumReduce();

        /*Store data*/

        for(int k=0;k<B;k++){
          r[i*B+k] = block.m[k*B];
        }
      }
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> transpose() const{
      Matrix<T, B> tr(width, height);

      for(int i=0;i<height;i++){
        for(int j=0;j<width;j++){
          tr[j][i] = (*this)[i][j];
        }
      }
      return tr;
    }

    /**************************************************************************/
    Matrix<T, B>& operator*=(const T n){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].mul(n);
        }
      }
      return *this;
    }

    /**************************************************************************/
    Matrix<T, B>& operator/=(const T n){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].div(n);
        }
      }
      return *this;
    }

    /**************************************************************************/
    Matrix<T, B>& operator-=(const Matrix<T, B>& m){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].sub(&(m.data[i*blocks_w+j]));
        }
      }
      return *this;
    }

    /**************************************************************************/
    Matrix<T, B>& operator+=(const Matrix<T, B>& m){
      for(int i=0;i<blocks_h;i++){
        for(int j=0;j<blocks_w;j++){
          data[i*blocks_w+j].add(&(m.data[i*blocks_w+j]));
        }
      }
      return *this;
    }

    /**************************************************************************/
    Matrix<T, B> operator-(const Matrix<T, B>& m)const{
      Matrix<T, B> r(*this);
      r -= m;
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> operator+(const Matrix<T, B>& m)const{
      Matrix<T, B> r(*this);
      r += m;
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> operator+() const{
      return *this;
    }

    /**************************************************************************/
    Matrix<T, B> operator-() const{
      Matrix<T, B> r(*this);
      r *= (T)-1.0;
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> operator/(const T n)const{
      Matrix<T, B> r(*this);
      r /= n;
      return r;
    }

    /**************************************************************************/
    Matrix<T, B> operator*(const T n)const{
      Matrix<T, B> r(*this);
      r *= n;
      return r;
    }

    /**************************************************************************/
    template<class TT, int BB>
    friend std::ostream& operator<<(std::ostream& os,
                                    const Matrix<TT, BB>& v);

  protected:
    /**************************************************************************/
    int blocks_w;
    int blocks_h;

    int width;
    int height;

    int vsize;

    /*Storage*/
    SpMatrixBlock<B, T>* data;
    T* vr;
  };

  /**************************************************************************/
  template<class T, int B>
  std::ostream& operator<<(std::ostream& os,
                           const Matrix<T, B>& v){
    os << "Matrix(" << v.height << ", " << v.width << ")" << std::endl;
    for(int i=0;i<v.height;i++){
      for(int j=0;j<v.width;j++){
        std::cout << v[i][j] << "\t";
      }
      std::cout << std::endl;
    }
    return os;
  }
}

#endif/*MATRIX_HPP*/
