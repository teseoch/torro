/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef ROTATIONS_HPP
#define ROTATIONS_HPP

#include "math/Matrix44.hpp"

namespace tsl{
  /*Right-handed systems*/

  /**************************************************************************/
  template<class T>
  Matrix44<T> rotationMatrixX(T theta){
    Matrix44<T> m;
    m.eye();
    m[1][1] =  Cos(theta);
    m[1][2] =  Sin(theta);
    m[2][1] = -m[1][2];
    m[2][2] =  m[1][1];
    return m;
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> rotationMatrixY(T theta){
    Matrix44<T> m;
    m.eye();
    m[0][0] =  Cos(theta);
    m[2][0] =  Sin(theta);
    m[0][2] = -m[2][0];
    m[2][2] =  m[0][0];
    return m;
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> rotationMatrixZ(T theta){
    Matrix44<T> m;
    m.eye();
    m[0][0] =  Cos(theta);
    m[0][1] =  Sin(theta);
    m[1][0] = -m[0][1];
    m[1][1] =  m[0][0];

    return m;
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> translationMatrix(const Vector4<T>& t){
    Matrix44<T> r;
    r.eye();

    r[0][3] = t[0];
    r[1][3] = t[1];
    r[2][3] = t[2];

    return r;
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> scaleMatrix(const Vector4<T>& s){
    Matrix44<T> r;
    r[0][0] = s[0];
    r[1][1] = s[1];
    r[2][2] = s[2];
    r[3][3] = (T)1.0;

    return r;
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> scaleMatrix(T s){
    return scaleMatrix(Vector4<T>(s, s, s, 0));
  }

  /**************************************************************************/
  template<class T>
  Matrix44<T> rotationMatrixAxis(const Vector4<T>& axs,
                                 T theta){
    Matrix44<T> RX;
    Matrix44<T> RY;
    Matrix44<T> RZ;
    Vector4<T> axis(axs);
    RX.eye();
    RY.eye();
    RZ.eye();

    if(axs.length() < 1E-6){
      return RZ;
    }

    axis.normalize();

    T a = axis[0];
    T b = axis[1];
    T c = axis[2];
    T d = Sqrt(b*b + c*c);

    if(Abs(d) > 1E-6){
      RX[1][1] =  c/d;
      RX[1][2] = -b/d;
      RX[2][1] =  b/d;
      RX[2][2] =  c/d;
    }

    RY[0][0] =  d;
    RY[0][2] = -a;
    RY[2][0] =  a;
    RY[2][2] =  d;

    /*
      RZ[0][0] =  Cos(theta);
      RZ[0][1] = -Sin(theta);
      RZ[1][0] =  Sin(theta);
      RZ[1][1] =  Cos(theta);
    */
    RZ = rotationMatrixZ(theta);

    return RX.inverse() * RY.inverse() * RZ * RY * RX;
  }

  /**************************************************************************/
  template<class T>
  inline Vector4<T> rotateVectorAxis(const Vector4<T>& v,
                                     const Vector4<T>& axis,
                                     T theta){
    /*Rotates v around axis by theta radians using Rodrigues' rotation
      formula */

    Vector4<T> r;
    T costheta = Cos(theta);
    T sintheta = Sin(theta);
    Vector4<T> axis_cross_vector = cross(axis, v);
    return v*costheta + axis_cross_vector * sintheta +
      axis*(dot(axis, v)*((T)1.0 - costheta));
  }

  /**************************************************************************/
  template<class T>
  inline void orthogonalMatrix(Matrix44<T>& matrix,
                               T l, T r,
                               T b, T t,
                               T n, T f){
    matrix.clear();

    matrix[0][0] = (T)2.0/(r-l);
    matrix[0][3] = -(r+l)/(r-l);

    matrix[1][1] = (T)2.0/(t-b);
    matrix[1][3] = -(t+b)/(t-b);

    matrix[2][2] = (T)-2.0/(f-n);
    matrix[2][3] = -(f+n)/(f-n);

    matrix[3][3] = (T)1.0;
  }

  /**************************************************************************/
  template<class T>
  inline void frustumMatrix(Matrix44<T>& matrix,
                            T l, T r,
                            T b, T t,
                            T n, T f){
    matrix.clear();

    matrix[0][0] = (T)2.0*n/(r-l);
    matrix[0][2] = (r+l)/(r-l);

    matrix[1][1] = (T)2.0*n/(t-b);
    matrix[1][2] = (t+b)/(t-b);

    matrix[2][2] = -(f+n)/(f-n);
    matrix[2][3] = (T)-2.0*f*n/(f-n);
    matrix[3][2] = (T)-1.0;
  }
}

#endif/*ROTATIONS_HPP*/
