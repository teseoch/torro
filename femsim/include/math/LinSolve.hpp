/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef LINSOLVE_HPP
#define LINSOLVE_HPP

#include "math/SpMatrix.hpp"
#include "math/Vector.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  class LinOperator{
  public:
    /**************************************************************************/
    virtual void performOperator(Vector<T>& result,
                                 const Vector<T>& arg) = 0;
  };

  /**************************************************************************/
  template<int N, class T>
  class LinSolve{
  public:
    LinSolve(int d){
      dim = d;
      mat = new SpMatrix<N, T>(dim, dim);

      b   = new Vector<T>(dim);
      x   = new Vector<T>(dim);

      /*Reset result vector*/
      //Vector<T>::mulf(*x, *x, 0);

      externalAllocatedMat = false;
      externalAllocatedb   = false;
      externalAllocatedx   = false;

      iterations = 0;

      projector = 0;

      lastError = (T)0.0;

      linOperator = 0;
    }

    /**************************************************************************/
    virtual ~LinSolve(){
      if(!externalAllocatedMat)
        delete mat;
      if(!externalAllocatedb)
        delete b;
      if(!externalAllocatedx)
        delete x;
    }

    /**************************************************************************/
    /*Fuctions for obtaining pointers to A, x, and b*/
    SpMatrix<N, T>* getMatrix(){
      return mat;
    }

    /**************************************************************************/
    Vector<T>* getb(){
      return b;
    }

    /**************************************************************************/
    Vector<T>* getx(){
      return x;
    }

    /**************************************************************************/
    int getDim()const{
      return dim;
    }

    /**************************************************************************/
    int getIterations(){
      return iterations;
    }

    /**************************************************************************/
    virtual void clear(){
      b->clear();
      x->clear();
    }

    /**************************************************************************/
    /*Functions for replacing A, x, b by versions allocated elsewhere.
      Not, if created elsewhere, they should be deleted elsewehere*/
    virtual void setb(Vector<T>* vec){
      tslassert(vec->getSize() == b->getSize());
      if(!externalAllocatedb){
        delete b;
      }
      b = vec;
      externalAllocatedb = true;
    }

    /**************************************************************************/
    virtual void setx(Vector<T>* vec){
      tslassert(vec->getSize() == x->getSize());
      if(!externalAllocatedx){
        delete x;
      }
      x = vec;
      externalAllocatedx = true;
    }

    /**************************************************************************/
    void setProjector(const Vector<T>* p){
      projector = p;
    }

    /**************************************************************************/
    T getLastError()const{
      return lastError;
    }

    /**************************************************************************/
    virtual void setMatrix(SpMatrix<N, T>* m){
      tslassert(m->getWidth() == mat->getWidth());
      tslassert(m->getHeight() == mat->getHeight());
      if(!externalAllocatedMat){
        delete mat;
      }
      mat = m;
      externalAllocatedMat = true;
    }

    /**************************************************************************/
    virtual void preSolve() = 0;

    /**************************************************************************/
    virtual void solve(int steps = 100000,
                       T tolerance = (T)1E-6) = 0;

    /**************************************************************************/
    void setLinOperator(LinOperator<N, T>* op){
      linOperator = op;
    }

  protected:
    /**************************************************************************/
    int dim;
    SpMatrix<N, T>* mat;
    Vector<T>* b;
    Vector<T>* x;
    const Vector<T>* projector;

    bool externalAllocatedMat;
    bool externalAllocatedb;
    bool externalAllocatedx;
    int iterations;

    T lastError;

    LinOperator<N, T>* linOperator;
  };
}

#endif/*LINSOLVE*/
