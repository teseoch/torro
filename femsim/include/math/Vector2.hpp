/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef VECTOR2_HPP
#define VECTOR2_HPP

#include "math/Vector4.hpp"

namespace tsl{

  /**************************************************************************/
  template<class T>
  class Vector2{
  public:
    /**************************************************************************/
    Vector2(){
      vector4_load_zero(m);
    }

    /**************************************************************************/
    Vector2(T a,
            T b){
      vector4_load(m, a, b, T(), T());
    }

    /**************************************************************************/
    /*Trim Vector4 to Vector2*/
    Vector2(const Vector4<T>& v){
      vector4_load(m, v.m[0], v.m[1], T(), T());
    }

    /**************************************************************************/
    Vector2<T>& operator=(const Vector4<T>& v){
      vector4_load(m, v.m[0], v.m[1], T(), T());
      return *this;
    }

    /**************************************************************************/
    T& operator[](int i){
      return m[i];
    }

    /**************************************************************************/
    const T& operator[](int i)const{
      return m[i];
    }

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Vector2<Y>& v);

  public:
#ifdef _WIN32
	__declspec(align(16)) T m[4];
#else
    T m[4] __attribute__ ((aligned (16)));
#endif
  };

#ifndef __ANDROID__
  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const Vector2<T>& v){
    os << std::scientific;
    os.precision(10);
    os << v.m[0] << '\t' << v.m[1] << std::endl;
    return os;
  }
#endif

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const Vector2<T>& v){
    return IsNan(v[0]) || IsNan(v[1]);
  }

  typedef Vector2<float> Vector2f;
  typedef Vector2<double> Vector2d;

}

#endif/*VECTOR2_HPP*/
