/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef VECTOR_HPP
#define VECTOR_HPP

#ifdef SSE2
#include <xmmintrin.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include <vector>

#include "core/tsldefs.hpp"
#include "math/SpMatrix.hpp"
#include <math.h>
#include <ostream>
#include <string.h>
#include <algorithm>

#ifdef USE_THREADS
#include "core/Thread.hpp"
#endif

#if defined SSE2
#include "math/x86-sse/sse_spmatrix_intrinsics.hpp"
using namespace tsl::x86_sse2;
#elif defined NEON
#include "math/arm-neon/neon_spmatrix_intrinsics.hpp"
using namespace tsl::arm_neon;
#else
#include "math/default/default_spmatrix_intrinsics.hpp"
using namespace tsl::default_proc;
#endif



#define RANGE_ASSERT_A                          \
  tslassert((r.size == a.size));                \
  tslassert(rg.startBlock % 16 == 0);			\
  tslassert(rg.endBlock   % 16 == 0);

#define RANGE_ASSERT_AB                         \
  RANGE_ASSERT_A                                \
  tslassert((a.size == b.size));

namespace tsl{

  //#define VEC_BLOCK_SIZE SPM_BLK_DIM

  class BlockDiagMatrix;

#ifdef USE_THREADS
  class Thread;
#endif

  template<class T=float>
  class Vector{
  public:
    /**************************************************************************/
    template<int M, class TT>
    friend class SpMatrix;

    /**************************************************************************/
    friend class BlockDiagMatrix;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelCGTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelCGCudaTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelSPMVCudaTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class CSpMatrix;

    /**************************************************************************/
    template< class TT>
    friend class CVector;

    /**************************************************************************/
    template< class TT>
    friend class VectorC;

    /**************************************************************************/
    Vector(int size=0);

    /**************************************************************************/
    Vector(const Vector& v);

    /**************************************************************************/
    virtual ~Vector();

    /**************************************************************************/
    /*Single threaded sum*/
    T sum()const;

    /**************************************************************************/
#ifdef USE_THREADS
    /*Multithreaded sum*/
    T sum(T* sharedBuffer, const Thread* caller,
          const VectorRange* rg)const;
#endif

    /**************************************************************************/
    /*Partial sum (used in parallel version)*/
    T sump(const VectorRange r)const;

    /**************************************************************************/
    T& operator[](int i){
      tslassert(i < size);
      return data[i];
    }

    /**************************************************************************/
    const T& operator[](int i)const{
      tslassert(i < size);
      return data[i];
    }

    /**************************************************************************/
    void clear(){
      memset((void*)data, 0, sizeof(T)*(uint)size);
    }

    /**************************************************************************/
    T* getData(){
      return data;
    }

    /**************************************************************************/
    const T* getData()const{
      return data;
    }

    /**************************************************************************/
    void randomShuffle();

    /**************************************************************************/
    Vector<T>& operator=(const Vector<T>& v);

    /**************************************************************************/
    Vector<T>& operator=(const T f){
      for(int i = 0; i < origSize; i++){
        data[i] = f;
      }
      return *this;
    }

    /**************************************************************************/
    T getAbsMax()const{
      T max = (T)0.0;

      for(int i=0;i<origSize;i++){
        max = Max(Abs(data[i]), max);
      }

      return max;
    }

    /**************************************************************************/
    //Vector<T>& set(const T*, int size);

    /**************************************************************************/
    Vector<T>& operator*=(T n){
      Vector<T>::mulf(*this, *this, (T)n);
      return *this;
    }

    /**************************************************************************/
    Vector<T>& operator/=(T n){
      Vector<T>::mulf(*this, *this, (T)((T)1.0/n));
      return *this;
    }

    /**************************************************************************/
    Vector<T>& operator+=(T n){
      Vector<T>::adds(*this, *this, (T)n);
      return *this;
    }

    /**************************************************************************/
    Vector<T>& operator-=(T n){
      Vector<T>::subs(*this, *this, (T)n);
      return *this;
    }

    /**************************************************************************/
    Vector<T>& operator+=(const Vector<T>& v);

    /**************************************************************************/
    Vector<T>& operator-=(const Vector<T>& v);

    /**************************************************************************/
    Vector<T>& operator*=(const Vector<T>& v);

    /**************************************************************************/
    Vector<T>& operator/=(const Vector<T>& v);

    /**************************************************************************/
    /*Conversion*/
    operator T*(){return (T*)data;}

    /**************************************************************************/
    operator const T*()const {return (T*)data;}

    /**************************************************************************/
    /*Unary*/
    Vector<T> operator+() const{
      Vector<T> r(*this);
      return r;
    }

    /**************************************************************************/
    Vector<T> operator-() const{
      Vector<T> r(*this);
      r *= -1.0f;
      return r;
    }

    /**************************************************************************/
    /*Vector vector*/
    Vector<T> operator+(const Vector<T>& v) const{
      tslassert(size == v.size);
      Vector<T> r(*this);
      r += v;
      return r;
    }

    /**************************************************************************/
    Vector<T> operator-(const Vector<T>& v) const{
      tslassert(size == v.size);
      Vector<T> r(*this);
      r -= v;
      return r;
    }

    /**************************************************************************/
    /*Vector scalar*/
    Vector<T> operator+(T f)const{
      Vector<T> r(*this);
      r += (T)f;
      return r;
    }

    /**************************************************************************/
    Vector<T> operator-(T f)const{
      Vector<T> r(*this);
      r -= (T)f;
      return r;
    }

    /**************************************************************************/
    template<class U>
    friend Vector<U> operator*(const Vector<U>& a,
                               U n);

    /**************************************************************************/
    template<class U>
    friend Vector<U> operator*(U n,
                               const Vector<U>& a);

    /**************************************************************************/
    template<class U>
    friend Vector<U> operator/(const Vector<U>& a,
                               U n);

    /**************************************************************************/
    /*Dot product*/
    T operator*(const Vector<T>& v) const;

    /**************************************************************************/
    /*Vector matrix multiplication*/
    template<int M, class U>
    friend Vector<U> operator*(const SpMatrix<M, U>& m,
                               const Vector<U>& v);

    /**************************************************************************/
    template<int M, class U>
    friend void spmv(Vector<U>& r,
                     const SpMatrix<M, U>& m,
                     const Vector<U>& v);

    /**************************************************************************/
    template<int M, class U>
    friend void spmv_t(Vector<U>& r, const SpMatrix<M, U>& m,
                       const Vector<U>& v);

    /**************************************************************************/
    template<class U>
    friend void spmv(Vector<U>& r,
                     const BlockDiagMatrix& m,
                     const Vector<U>& v);

    /**************************************************************************/
    template<int M, class U>
    friend void spmv_partial(Vector<U>& r,
                             const SpMatrix<M, U>& m,
                             const Vector<U>& v,
                             const MatrixRange mr);

    /**************************************************************************/
    template<int M, class U>
    friend void multiplyJacobi(Vector<U>& r,
                               const SpMatrix<M, U>& m,
                               const Vector<U>& v);

    /**************************************************************************/
    template<int M, class U>
    friend void multiplyGaussSeidel(Vector<U>& r,
                                    const SpMatrix<M, U>& m,
                                    const Vector<U>& v,
                                    const Vector<U>* b);

    /**************************************************************************/
    /*Vector length*/
    T length2()const;

    /**************************************************************************/
    T length()const{
      return Sqrt(length2());
    }

    /**************************************************************************/
    T norm()const{
      return length();
    }

    /**************************************************************************/
    T infiniteNorm(int* index = 0){
      int largestIndex = -1;
      T largestValue = 0.0;

      for(int i=0;i<size;i++){
        if(Abs((*this)[i]) > largestValue){
          largestValue = Abs((*this)[i]);
          largestIndex = i;
        }
      }

      if(index){
        *index = largestIndex;
      }
      return largestValue;
    }

    /**************************************************************************/
    template<class U>
    friend std::ostream& operator<<(std::ostream& os,
                                    const Vector<U>& v);

    /**************************************************************************/
    int getSize()const{return origSize;}

    /**************************************************************************/
    int getPaddedSize()const{return size;}

    //  protected:
    /*Functions which should be used in iterative methods because
      these functions do not allocate memory themself. When executing
      iterative methods, a lot of vector calculations are
      performed. Since most standard operators will create new
      objects, with a large size of allocated memory, and return these
      temporary vectors as result. Eventually this result is assigned
      to another vector object. In normal situations this is generally
      not a problem, however, in iterative methods we suggest to reuse
      allocated memory and avoid the use of unneeded memory
      allocations and memory copies. */
    /*
      float sumReduction(float* tmpStorage)const;
      float sumReductionPartial(float* tmpStorage,
      const VectorRange r)const;
    */


    /**************************************************************************/
    static void sub  (Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b);

    /**************************************************************************/
    static void add  (Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b);

    /**************************************************************************/
    static void subs (Vector<T>& r,
                      const Vector<T>& a,
                      T f);

    /**************************************************************************/
    static void adds (Vector<T>& r,
                      const Vector<T>& a,
                      T f);

    /**************************************************************************/
    static void madd (Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b,
                      const Vector<T>& c);

    /**************************************************************************/
    static void mfadd(Vector<T>& r,
                      T f,
                      const Vector<T>& b,
                      const Vector<T>& c);

    /**************************************************************************/
    static void mfadd2(Vector<T>& r,
                       T f,
                       const Vector<T>& b,
                       const Vector<T>& c,
                       const Vector<T>& d);

    /**************************************************************************/
    static void mul  (Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b);

    /**************************************************************************/
    static void mulf (Vector<T>& r,
                      const Vector<T>& a,
                      T f);

    /**************************************************************************/
    static void div  (Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b);

    /**************************************************************************/
    /*Partial operations*/
    static void subp  (Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg);

    /**************************************************************************/
    static void addp  (Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg);

    /**************************************************************************/
    static void subsp (Vector<T>& r,
                       const Vector<T>& a,
                       T f,
                       const VectorRange rg);

    /**************************************************************************/
    static void addsp (Vector<T>& r,
                       const Vector<T>& a,
                       T f,
                       const VectorRange rg);

    /**************************************************************************/
    static void maddp (Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const Vector<T>& c,
                       const VectorRange rg);

    /**************************************************************************/
    static void mfaddp(Vector<T>& r,
                       T f,
                       const Vector<T>& b,
                       const Vector<T>& c,
                       const VectorRange rg);

    /**************************************************************************/
    static void mfadd2p(Vector<T>& r,
                        T f,
                        const Vector<T>& b,
                        const Vector<T>& c,
                        const Vector<T>& d,
                        const VectorRange rg);

    /**************************************************************************/
    static void mulp  (Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg);

    /**************************************************************************/
    static void mulfp (Vector<T>& r,
                       const Vector<T>& a,
                       T f,
                       const VectorRange rg);
    /**************************************************************************/
    /**************************************************************************/
    /**************************************************************************/
  protected:
    int size;
    int origSize;
    T* data;                   /*Data points to memory containing a
                                 multiple of 16 elements*/
    mutable T* tmp_buffer_pos; /*Used for positve values in reductions*/
    mutable T* tmp_buffer_neg; /*Used for negative values reductions*/

    /**************************************************************************/
    virtual void copy(const Vector<T>* src);
  };

  /**************************************************************************/
  template<class U>
  extern std::ostream& operator<<(std::ostream& os,
                                  const Vector<U>& v);

  /**************************************************************************/
  template<class T>
  Vector<T> operator*(const Vector<T>& a,
                      T n){
    Vector<T> r = a;
    r *= (T)n;
    return r;
  }

  /**************************************************************************/
  template<class T>
  Vector<T> operator*(T n,
                      const Vector<T>& a){
    Vector<T> r = a;
    r *= (T)n;
    return r;
  }

  /**************************************************************************/
  template<class T>
  Vector<T> operator/(const Vector<T>& a,
                      T n){
    Vector<T> r = a;
    r /= (T)n;
    return r;
  }

  /**************************************************************************/
  template<class T>
  Vector<T> operator/(T n,
                      const Vector<T>& a){
    Vector<T> r;
    error("not implemented yet");
    return r;
  }

  /**************************************************************************/
  /*The size of a vector is always a multiple of 16*/
  template<class T>
  Vector<T>::Vector(int s){
    origSize = s;
    if(s % 16 == 0){
      size = s;
    }else{
      size = ((s/16)+1)*16;
    }

    if(size==0){
      data = 0;
      tmp_buffer_pos = 0;
      tmp_buffer_neg = 0;
      return;
    }

    alignedMalloc((void**)&data, 16, (uint)size*sizeof(T));
    alignedMalloc((void**)&tmp_buffer_pos, 16, (uint)size*sizeof(T));
    alignedMalloc((void**)&tmp_buffer_neg, 16, (uint)size*sizeof(T));

    tslassert(alignment(data, 16));
    tslassert(alignment(tmp_buffer_pos, 16));
    tslassert(alignment(tmp_buffer_neg, 16));

    /*Initialize extra space to zero since the extra space is being
      used in almost all operations*/

    memset((void*)data, 0, sizeof(T)*(uint)(size));

    /*Note, we only have to initialize the extra data here. When a
      vector is being copied, we copy the complete vector, including
      the extra padded data*/
  }

  /**************************************************************************/
  template<class T>
  Vector<T>::Vector(const Vector<T>& v){
    origSize = v.origSize;
    size = v.size;

    alignedMalloc((void**)&data, 16, (uint)size*sizeof(T));
    alignedMalloc((void**)&tmp_buffer_pos, 16, (uint)size*sizeof(T));
    alignedMalloc((void**)&tmp_buffer_neg, 16, (uint)size*sizeof(T));

    tslassert(alignment(data, 16));
    tslassert(alignment(tmp_buffer_pos, 16));
    tslassert(alignment(tmp_buffer_neg, 16));

    memcpy((void*)data, (void*)v.data, sizeof(T)*(uint)size);
  }

  /**************************************************************************/
  template<class T>
  Vector<T>::~Vector(){
    if(data){
      alignedFree(data);
    }
    if(tmp_buffer_pos){
      alignedFree(tmp_buffer_pos);
      alignedFree(tmp_buffer_neg);
    }
  }

  /**************************************************************************/
  template<class T>
  Vector<T>& Vector<T>::operator=(const Vector<T>& v){
    if(this == &v){
      /*Self assignment*/
      return *this;
    }
    copy(&v);
    return *this;
  }

  /**************************************************************************/
  template<class T>
  void Vector<T>::copy(const Vector<T>* src){
    if(origSize == src->origSize && size == src->size){
      /*No need to update storage*/
    }else{
      origSize = src->origSize;
      size = src->size;
      if(data){
        alignedFree(data);
      }

      if(tmp_buffer_pos){
        alignedFree(tmp_buffer_pos);
        alignedFree(tmp_buffer_neg);

        tmp_buffer_pos = 0;
        tmp_buffer_neg = 0;
      }

      alignedMalloc((void**)&data, 16, (uint)size*sizeof(T));
      alignedMalloc((void**)&tmp_buffer_pos, 16, (uint)size*sizeof(T));
      alignedMalloc((void**)&tmp_buffer_neg, 16, (uint)size*sizeof(T));

      tslassert(alignment(data, 16));
      tslassert(alignment(tmp_buffer_pos, 16));
      tslassert(alignment(tmp_buffer_neg, 16));
    }
    memcpy((void*)data, (void*)src->data, sizeof(T)*(uint)size);
  }

  /**************************************************************************/
  template<int N, class T>
  Vector<T> operator*(const SpMatrix<N, T>& m,
                      const Vector<T>& v){
    if(!m.isFinalized()){
      //error("Matrix is not finalized");
    }

    Vector<T> r(m.origHeight);
    spmv<N>(r, m, v);

    return r;
  }

  /**************************************************************************/
  template<class T>
  std::ostream& operator<<(std::ostream& stream,
                           const Vector<T>& v){
    stream << "vector size = " << v.size << " " << v.origSize << std::endl;
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    for(int i=0;i<v.origSize;i++){
      stream << i << "\t" << v.data[i] << "\n";
    }
    stream << std::endl;
    stream.flags(origflags);
    return stream;
  }

  /**************************************************************************/
  template<class T>
  void Vector<T>::randomShuffle(){
    std::random_shuffle(data, data+origSize);
  }

  /**************************************************************************/
  inline int computeStepsVec(int size){
    /*Find a value, smaller than size, which is a power of 2*/
    if(size <= 1){
      return 0;
    }
    int steps = 1;
    while(steps*2<size){
      steps*=2;
    }
    return steps;
  }

  /**************************************************************************/
  template<class T>
  T Vector<T>::sum()const{
    if(origSize == 0){
      return 0;
    }

    VectorRange range;
    range.startBlock = 0;
    range.endBlock = size;
    return sump(range);
  }

#ifdef USE_THREADS
  /**************************************************************************/
  /*Parallel sum reduction*/
  template<class T>
  T Vector<T>::sum(T* sharedReductions,
                   const Thread* caller,
                   const VectorRange* rg)const{
    /*Compute partial sum and store result and error in shared memory*/
    sharedReductions[caller->getId()] = sump(rg[caller->getId()]);
    caller->sync();

    /*Copy shared data to local memory and each thread computes the
      total sum*/
    T result = 0;
    T c = 0;

    for(int i=0;i<caller->getLastId();i++){
      T y = sharedReductions[i] - c;
      T t = result + y;
      c = ((t - result) - sharedReductions[i]) + c;
      result = t;
    }

    return result;
  }
#endif

  /**************************************************************************/
  template<class T>
  inline T Vector<T>::sump(const VectorRange rg)const{
    if(rg.startBlock == rg.endBlock){
      /*Range is zero.*/
      return (T)0;
    }

    bool first = true;
    int steps = computeStepsVec(rg.endBlock - rg.startBlock);

    while(steps >= 1){
      for(int j=0;j<steps;j++){
        int idx1 = j + rg.startBlock;
        int idx2 = j + rg.startBlock + steps;

        if(first){
          if(idx2 < rg.endBlock){
            if(data[idx1] > 0){
              tmp_buffer_pos[idx1] = data[idx1];
              tmp_buffer_neg[idx1] = 0;
            }else{
              tmp_buffer_neg[idx1] = data[idx1];
              tmp_buffer_pos[idx1] = 0;
            }

            if(data[idx2] > 0){
              tmp_buffer_pos[idx1] += data[idx2];
            }else{
              tmp_buffer_neg[idx1] += data[idx2];
            }
          }else{
            if(data[idx1] > 0){
              tmp_buffer_pos[idx1] = data[idx1];
              tmp_buffer_neg[idx1] = 0;
            }else{
              tmp_buffer_neg[idx1] = data[idx1];
              tmp_buffer_pos[idx1] = 0;
            }
          }
        }else{
          if(idx2 < rg.endBlock){
            tmp_buffer_pos[idx1] += tmp_buffer_pos[idx2];
            tmp_buffer_neg[idx1] += tmp_buffer_neg[idx2];
          }
        }
      }
      steps /= 2;
      first = false;
    }
    return tmp_buffer_pos[rg.startBlock] + tmp_buffer_neg[rg.startBlock];
  }


#if 1
#ifdef SSE2
  /**************************************************************************/
  template<>
  inline float Vector<float>::sump(const VectorRange rg)const{
    if(rg.startBlock == rg.endBlock){
      /*Range is zero.*/
      return 0;
    }

    __m128 XMMZero = _mm_set_ps1(0.0f);

    tslassert(rg.startBlock%16 == 0);
    tslassert(rg.endBlock%16 == 0);

    int steps = computeStepsVec(rg.endBlock-rg.startBlock);
    bool first = true;

    while(steps >= 4){
      for(int j=0;j<steps/4;j++){
        int idx1 = rg.startBlock + j*4;
        int idx2 = rg.startBlock + j*4 + steps;

        if(first){
          if(idx2 < rg.endBlock){
            __m128 XMM1 = _mm_load_ps(data + idx1);
            __m128 XMM2 = _mm_load_ps(data + idx2);

            __m128 XMMPosMask = _mm_cmpgt_ps(XMM1, XMMZero);
            __m128 XMMNegMask = _mm_cmple_ps(XMM1, XMMZero);

            __m128 XMM1P = _mm_and_ps(XMMPosMask, XMM1);
            __m128 XMM1N = _mm_and_ps(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_ps(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_ps(XMM2, XMMZero);

            __m128 XMM2P = _mm_and_ps(XMMPosMask, XMM2);
            __m128 XMM2N = _mm_and_ps(XMMNegMask, XMM2);


            _mm_store_ps(tmp_buffer_pos + idx1, _mm_add_ps(XMM1P, XMM2P));
            _mm_store_ps(tmp_buffer_neg + idx1, _mm_add_ps(XMM1N, XMM2N));
          }else{
            __m128 XMM1 = _mm_load_ps(data + idx1);

            __m128 XMMPosMask = _mm_cmpgt_ps(XMM1, XMMZero);
            __m128 XMMNegMask = _mm_cmple_ps(XMM1, XMMZero);

            __m128 XMM1P = _mm_and_ps(XMMPosMask, XMM1);
            __m128 XMM1N = _mm_and_ps(XMMNegMask, XMM1);

            _mm_store_ps(tmp_buffer_pos + idx1, XMM1P);
            _mm_store_ps(tmp_buffer_neg + idx1, XMM1N);
          }
        }else{
          if(idx2 < rg.endBlock){
            __m128 XMM1P = _mm_load_ps(tmp_buffer_pos + idx1);
            __m128 XMM1N = _mm_load_ps(tmp_buffer_neg + idx1);
            __m128 XMM2P = _mm_load_ps(tmp_buffer_pos + idx2);
            __m128 XMM2N = _mm_load_ps(tmp_buffer_neg + idx2);

            _mm_store_ps(tmp_buffer_pos + idx1, _mm_add_ps(XMM1P, XMM2P));
            _mm_store_ps(tmp_buffer_neg + idx1, _mm_add_ps(XMM1N, XMM2N));
          }
        }
      }
      steps /= 2;
      first = false;
    }
    return ( ((tmp_buffer_pos[rg.startBlock+0] +
               tmp_buffer_pos[rg.startBlock+2]) +
              (tmp_buffer_pos[rg.startBlock+1] +
               tmp_buffer_pos[rg.startBlock+3])) +

             ((tmp_buffer_neg[rg.startBlock+0] +
               tmp_buffer_neg[rg.startBlock+2]) +
              (tmp_buffer_neg[rg.startBlock+1] +
               tmp_buffer_neg[rg.startBlock+3])) );
  }
#endif
#endif

#if 1
#ifdef SSE2
  /**************************************************************************/
  template<>
  inline double Vector<double>::sump(const VectorRange rg)const{
    if(rg.startBlock == rg.endBlock){
      /*Range is zero.*/
      return 0;
    }

    tslassert(rg.startBlock%16 == 0);
    tslassert(rg.endBlock%16 == 0);

    int steps = computeStepsVec(rg.endBlock - rg.startBlock);
    bool first = true;

    __m128d XMMZero = _mm_set_pd1(0.0);

    while(steps >= 4){
      for(int j=0;j<steps/4;j++){
        int idx1 = rg.startBlock + j*4;
        int idx2 = rg.startBlock + j*4 + steps;

        if(first){
          if(idx2 < rg.endBlock){
            __m128d XMM1  = _mm_load_pd(data + idx1 + 0);
            __m128d XMM12 = _mm_load_pd(data + idx1 + 2);
            __m128d XMM2  = _mm_load_pd(data + idx2 + 0);
            __m128d XMM22 = _mm_load_pd(data + idx2 + 2);

            __m128d XMMPosMask = _mm_cmpgt_pd(XMM1, XMMZero);
            __m128d XMMNegMask = _mm_cmple_pd(XMM1, XMMZero);

            __m128d XMM1P = _mm_and_pd(XMMPosMask, XMM1);
            __m128d XMM1N = _mm_and_pd(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_pd(XMM12, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM12, XMMZero);

            __m128d XMM12P = _mm_and_pd(XMMPosMask, XMM12);
            __m128d XMM12N = _mm_and_pd(XMMNegMask, XMM12);

            XMMPosMask = _mm_cmpgt_pd(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM2, XMMZero);

            __m128d XMM2P = _mm_and_pd(XMMPosMask, XMM2);
            __m128d XMM2N = _mm_and_pd(XMMNegMask, XMM2);

            XMMPosMask = _mm_cmpgt_pd(XMM22, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM22, XMMZero);

            __m128d XMM22P = _mm_and_pd(XMMPosMask, XMM22);
            __m128d XMM22N = _mm_and_pd(XMMNegMask, XMM22);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, _mm_add_pd(XMM1P,  XMM2P));
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, _mm_add_pd(XMM12P, XMM22P));

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, _mm_add_pd(XMM1N,  XMM2N));
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, _mm_add_pd(XMM12N, XMM22N));
          }else{
            __m128d XMM1 = _mm_load_pd(data + idx1 + 0);
            __m128d XMM2 = _mm_load_pd(data + idx1 + 2);

            __m128d XMMPosMask = _mm_cmpgt_pd(XMM1, XMMZero);
            __m128d XMMNegMask = _mm_cmple_pd(XMM1, XMMZero);

            __m128d XMM1P = _mm_and_pd(XMMPosMask, XMM1);
            __m128d XMM1N = _mm_and_pd(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_pd(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM2, XMMZero);

            __m128d XMM2P = _mm_and_pd(XMMPosMask, XMM2);
            __m128d XMM2N = _mm_and_pd(XMMNegMask, XMM2);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, XMM1P);
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, XMM2P);

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, XMM1N);
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, XMM2N);
          }
        }else{
          if(idx2 < rg.endBlock){
            __m128d XMM1  = _mm_load_pd(tmp_buffer_pos + idx1 + 0);
            __m128d XMM12 = _mm_load_pd(tmp_buffer_pos + idx1 + 2);
            __m128d XMM2  = _mm_load_pd(tmp_buffer_pos + idx2 + 0);
            __m128d XMM22 = _mm_load_pd(tmp_buffer_pos + idx2 + 2);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, _mm_add_pd(XMM1,  XMM2));
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, _mm_add_pd(XMM12, XMM22));

            XMM1  = _mm_load_pd(tmp_buffer_neg + idx1 + 0);
            XMM12 = _mm_load_pd(tmp_buffer_neg + idx1 + 2);
            XMM2  = _mm_load_pd(tmp_buffer_neg + idx2 + 0);
            XMM22 = _mm_load_pd(tmp_buffer_neg + idx2 + 2);

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, _mm_add_pd(XMM1,  XMM2));
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, _mm_add_pd(XMM12, XMM22));
          }
        }
      }
      steps /= 2;
      first = false;
    }

    return ( ((tmp_buffer_pos[rg.startBlock+0] +
               tmp_buffer_pos[rg.startBlock+2]) +
              (tmp_buffer_pos[rg.startBlock+1] +
               tmp_buffer_pos[rg.startBlock+3])) +

             ((tmp_buffer_neg[rg.startBlock+0] +
               tmp_buffer_neg[rg.startBlock+2]) +
              (tmp_buffer_neg[rg.startBlock+1] +
               tmp_buffer_neg[rg.startBlock+3])) );
  }
#endif
#endif

  /**************************************************************************/
  template<class T>
  inline T Vector<T>::operator*(const Vector<T>& v) const{
    tslassert(size == v.size);

    if(size == 0){
      /*Range is zero.*/
      return 0;
    }

    bool first = true;
    int steps = computeStepsVec(size);

    while(steps >= 1){
      for(int j=0;j<steps;j++){
        int idx1 = j;
        int idx2 = j + steps;

        if(first){
          T v1 = data[idx1] * v.data[idx1];
          T v2 = data[idx2] * v.data[idx2];

          if(idx2 < size){
            if(v1 > 0){
              tmp_buffer_pos[idx1] = v1;
              tmp_buffer_neg[idx2] = 0;
            }else{
              tmp_buffer_pos[idx1] = 0;
              tmp_buffer_neg[idx2] = v1;
            }

            if(v2 > 0){
              tmp_buffer_pos[idx1] += v1;
            }else{
              tmp_buffer_neg[idx2] += v1;
            }
          }else{
            T val = data[idx1]*v.data[idx1];
            if(val > 0){
              tmp_buffer_pos[idx1] = val;
              tmp_buffer_neg[idx1] = 0;
            }else{
              tmp_buffer_pos[idx1] = 0;
              tmp_buffer_neg[idx1] = val;
            }
          }
        }else{
          if(idx2 < size){
            tmp_buffer_pos[idx1] += tmp_buffer_pos[idx2];
            tmp_buffer_neg[idx1] += tmp_buffer_neg[idx2];
          }
        }
      }
      steps/=2;
      first = false;
    }
    return tmp_buffer_pos[0] + tmp_buffer_neg[0];
  }

#if 1
#ifdef SSE2
  /**************************************************************************/
  template<>
  inline float Vector<float>::operator*(const Vector<float>& v)const{
    if(size == 0){
      /*Range is zero.*/
      return 0;
    }

    tslassert(size == v.size);

    int steps = computeStepsVec(size);
    bool first = true;

    __m128 XMMZero = _mm_set_ps1(0.0f);

    while(steps >= 4){
      for(int j=0;j<steps/4;j++){
        int idx1 = j*4;
        int idx2 = j*4 + steps;

        if(first){
          if(idx2 < size){
            __m128 XMM1 = _mm_load_ps(data + idx1);
            __m128 XMM2 = _mm_load_ps(data + idx2);

            __m128 XMM3 = _mm_load_ps(v.data + idx1);
            __m128 XMM4 = _mm_load_ps(v.data + idx2);

            XMM1 = _mm_mul_ps(XMM1, XMM3);
            XMM2 = _mm_mul_ps(XMM2, XMM4);

            __m128 XMMPosMask = _mm_cmpgt_ps(XMM1, XMMZero);
            __m128 XMMNegMask = _mm_cmple_ps(XMM1, XMMZero);

            __m128 XMM1P = _mm_and_ps(XMMPosMask, XMM1);
            __m128 XMM1N = _mm_and_ps(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_ps(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_ps(XMM2, XMMZero);

            __m128 XMM2P = _mm_and_ps(XMMPosMask, XMM2);
            __m128 XMM2N = _mm_and_ps(XMMNegMask, XMM2);


            _mm_store_ps(tmp_buffer_pos + idx1, _mm_add_ps(XMM1P, XMM2P));
            _mm_store_ps(tmp_buffer_neg + idx1, _mm_add_ps(XMM1N, XMM2N));
          }else{
            __m128 XMM1 = _mm_load_ps(data + idx1);
            __m128 XMM2 = _mm_load_ps(v.data + idx1);

            XMM1 = _mm_mul_ps(XMM1, XMM2);

            __m128 XMMPosMask = _mm_cmpgt_ps(XMM1, XMMZero);
            __m128 XMMNegMask = _mm_cmple_ps(XMM1, XMMZero);

            __m128 XMM1P = _mm_and_ps(XMMPosMask, XMM1);
            __m128 XMM1N = _mm_and_ps(XMMNegMask, XMM1);

            _mm_store_ps(tmp_buffer_pos + idx1, XMM1P);
            _mm_store_ps(tmp_buffer_neg + idx1, XMM1N);
          }
        }else{
          if(idx2 < size){
            __m128 XMM1 = _mm_load_ps(tmp_buffer_pos + idx1);
            __m128 XMM2 = _mm_load_ps(tmp_buffer_pos + idx2);

            _mm_store_ps(tmp_buffer_pos + idx1, _mm_add_ps(XMM1, XMM2));

            XMM1 = _mm_load_ps(tmp_buffer_neg + idx1);
            XMM2 = _mm_load_ps(tmp_buffer_neg + idx2);

            _mm_store_ps(tmp_buffer_neg + idx1, _mm_add_ps(XMM1, XMM2));
          }
        }
      }
      steps/=2;
      first = false;
    }
    return ( ((tmp_buffer_pos[0] +
               tmp_buffer_pos[2]) +
              (tmp_buffer_pos[1] +
               tmp_buffer_pos[3])) +

             ((tmp_buffer_neg[0] +
               tmp_buffer_neg[2]) +
              (tmp_buffer_neg[1] +
               tmp_buffer_neg[3])) );
  }

  /**************************************************************************/
  template<>
  inline double Vector<double>::operator*(const Vector<double>& v)const{
    if(size == 0){
      /*Range is zero.*/
      return 0;
    }

    tslassert(size == v.size);

    int steps = computeStepsVec(size);
    bool first = true;

    __m128d XMMZero = _mm_set_pd1(0.0);

    while(steps >= 4){
      for(int j=0;j<steps/4;j++){
        int idx1 = j*4;
        int idx2 = j*4 + steps;

        if(first){
          if(idx2 < size){
            __m128d XMM1  = _mm_load_pd(data + idx1 + 0);
            __m128d XMM12 = _mm_load_pd(data + idx1 + 2);
            __m128d XMM2  = _mm_load_pd(data + idx2 + 0);
            __m128d XMM22 = _mm_load_pd(data + idx2 + 2);


            __m128d XMM3  = _mm_load_pd(v.data + idx1 + 0);
            __m128d XMM32 = _mm_load_pd(v.data + idx1 + 2);
            __m128d XMM4  = _mm_load_pd(v.data + idx2 + 0);
            __m128d XMM42 = _mm_load_pd(v.data + idx2 + 2);

            XMM1  = _mm_mul_pd(XMM1,  XMM3);
            XMM12 = _mm_mul_pd(XMM12, XMM32);

            XMM2  = _mm_mul_pd(XMM2,  XMM4);
            XMM22 = _mm_mul_pd(XMM12, XMM42);

            __m128d XMMPosMask = _mm_cmpgt_pd(XMM1, XMMZero);
            __m128d XMMNegMask = _mm_cmple_pd(XMM1, XMMZero);

            __m128d XMM1P = _mm_and_pd(XMMPosMask, XMM1);
            __m128d XMM1N = _mm_and_pd(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_pd(XMM12, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM12, XMMZero);

            __m128d XMM12P = _mm_and_pd(XMMPosMask, XMM12);
            __m128d XMM12N = _mm_and_pd(XMMNegMask, XMM12);

            XMMPosMask = _mm_cmpgt_pd(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM2, XMMZero);

            __m128d XMM2P = _mm_and_pd(XMMPosMask, XMM2);
            __m128d XMM2N = _mm_and_pd(XMMNegMask, XMM2);

            XMMPosMask = _mm_cmpgt_pd(XMM22, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM22, XMMZero);

            __m128d XMM22P = _mm_and_pd(XMMPosMask, XMM22);
            __m128d XMM22N = _mm_and_pd(XMMNegMask, XMM22);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, _mm_add_pd(XMM1P, XMM2P));
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, _mm_add_pd(XMM12P, XMM22P));

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, _mm_add_pd(XMM1N, XMM2N));
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, _mm_add_pd(XMM12N, XMM22N));
          }else{
            __m128d XMM1 = _mm_load_pd(data + idx1 + 0);
            __m128d XMM2 = _mm_load_pd(data + idx1 + 2);

            __m128d XMM3 = _mm_load_pd(v.data + idx1 + 0);
            __m128d XMM4 = _mm_load_pd(v.data + idx1 + 2);

            XMM1 = _mm_mul_pd(XMM1, XMM3);
            XMM2 = _mm_mul_pd(XMM2, XMM4);

            __m128d XMMPosMask = _mm_cmpgt_pd(XMM1, XMMZero);
            __m128d XMMNegMask = _mm_cmple_pd(XMM1, XMMZero);

            __m128d XMM1P = _mm_and_pd(XMMPosMask, XMM1);
            __m128d XMM1N = _mm_and_pd(XMMNegMask, XMM1);

            XMMPosMask = _mm_cmpgt_pd(XMM2, XMMZero);
            XMMNegMask = _mm_cmple_pd(XMM2, XMMZero);

            __m128d XMM2P = _mm_and_pd(XMMPosMask, XMM2);
            __m128d XMM2N = _mm_and_pd(XMMNegMask, XMM2);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, XMM1P);
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, XMM2P);

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, XMM1N);
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, XMM2N);
          }
        }else{
          if(idx2 < size){
            __m128d XMM1  = _mm_load_pd(tmp_buffer_pos + idx1 + 0);
            __m128d XMM12 = _mm_load_pd(tmp_buffer_pos + idx1 + 2);
            __m128d XMM2  = _mm_load_pd(tmp_buffer_pos + idx2 + 0);
            __m128d XMM22 = _mm_load_pd(tmp_buffer_pos + idx2 + 2);

            _mm_store_pd(tmp_buffer_pos + idx1 + 0, _mm_add_pd(XMM1, XMM2));
            _mm_store_pd(tmp_buffer_pos + idx1 + 2, _mm_add_pd(XMM12, XMM22));

            XMM1  = _mm_load_pd(tmp_buffer_neg + idx1 + 0);
            XMM12 = _mm_load_pd(tmp_buffer_neg + idx1 + 2);
            XMM2  = _mm_load_pd(tmp_buffer_neg + idx2 + 0);
            XMM22 = _mm_load_pd(tmp_buffer_neg + idx2 + 2);

            _mm_store_pd(tmp_buffer_neg + idx1 + 0, _mm_add_pd(XMM1, XMM2));
            _mm_store_pd(tmp_buffer_neg + idx1 + 2, _mm_add_pd(XMM12, XMM22));
          }
        }
      }
      steps/=2;
      first = false;
    }

    return ( ((tmp_buffer_pos[0] +
               tmp_buffer_pos[2]) +
              (tmp_buffer_pos[1] +
               tmp_buffer_pos[3])) +

             ((tmp_buffer_neg[0] +
               tmp_buffer_neg[2]) +
              (tmp_buffer_neg[1] +
               tmp_buffer_neg[3])) );
  }
#endif
#endif

  /**************************************************************************/
  /*r = a - b*/
  template<class T>
  void Vector<T>::sub(Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_sub<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
    }
  }

  /**************************************************************************/
  template<class T>
  void Vector<T>::subs(Vector<T>& r,
                       const Vector<T>& a,
                       T f){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_subs<4, T>(r.data+i*16, a.data+i*16, f);
    }
  }

  /**************************************************************************/
  /*r = a + b*/
  template<class T>
  void Vector<T>::add(Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_add<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
    }
  }

  /**************************************************************************/
  template<class T>
  void Vector<T>::adds(Vector<T>& r,
                       const Vector<T>& a,
                       T f){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_adds<4, T>(r.data+i*16, a.data+i*16, f);
    }
  }

  /**************************************************************************/
  /*r = a * b + c*/
  template<class T>
  void Vector<T>::madd(Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const Vector<T>& c){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_madd<4, T>(r.data+i*16, a.data+i*16, b.data+i*16,
                                c.data+i*16);
    }
  }

  /**************************************************************************/
  /*r = f * b + c*/
  template<class T>
  void Vector<T>::mfadd(Vector<T>& r,
                        T f,
                        const Vector<T>& b,
                        const Vector<T>& c){
    for(int i=0;i<b.size/16;i++){
      spmatrix_block_msadd<4, T>(r.data+i*16, f, b.data+i*16, c.data+i*16);
    }
  }

  /**************************************************************************/
  /*r = f * b + c + d*/
  template<class T>
  void Vector<T>::mfadd2(Vector<T>& r,
                         T f,
                         const Vector<T>& b,
                         const Vector<T>& c,
                         const Vector<T>& d){
    for(int i=0;i<b.size/16;i++){
      spmatrix_block_msadd2<4, T>(r.data+i*16, f, b.data+i*16, c.data+i*16,
                                  d.data+i*16);
    }
  }

  /**************************************************************************/
  /*r = a .* b*/
  template<class T>
  void Vector<T>::mul(Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_mul<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
    }
  }

  /**************************************************************************/
  /*r = a ./ b*/
  template<class T>
  void Vector<T>::div(Vector<T>& r,
                      const Vector<T>& a,
                      const Vector<T>& b){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_div<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
    }
  }

  /**************************************************************************/
  /*r = a * f*/
  template<class T>
  void Vector<T>::mulf(Vector<T>& r,
                       const Vector<T>& a,
                       T f){
    for(int i=0;i<a.size/16;i++){
      spmatrix_block_muls<4, T>(r.data+i*16, a.data+i*16, f);
    }
  }

  /*Partial functions*/

  /**************************************************************************/
  /*r = a - b*/
  template<class T>
  void Vector<T>::subp(Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16; i<rg.endBlock/16; i++){
      if(i < a.size/16){
        spmatrix_block_sub<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
      }
    }
  }

  /**************************************************************************/
  template<class T>
  void Vector<T>::subsp(Vector<T>& r,
                        const Vector<T>& a,
                        T f,
                        const VectorRange rg){
    RANGE_ASSERT_A;

    for(int i=rg.startBlock/16; i<rg.endBlock/16; i++){
      if(i < a.size/16){
        spmatrix_block_subs<4, T>(r.data+i*16, a.data+i*16, f);
      }
    }
  }

  /**************************************************************************/
  /*r = a + b*/
  template<class T>
  void Vector<T>::addp(Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_add<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
      }
    }
  }

  /**************************************************************************/
  /*r = a + f*/
  template<class T>
  void Vector<T>::addsp(Vector<T>& r,
                        const Vector<T>& a,
                        T f,
                        const VectorRange rg){
    RANGE_ASSERT_A;

    for(int i=rg.startBlock/16; i<rg.endBlock/16; i++){
      if(i < a.size/16){
        spmatrix_block_adds<4, T>(r.data+i*16, a.data+i*16, f);
      }
    }
  }

  /**************************************************************************/
  /*r = a * b + c*/
  template<class T>
  void Vector<T>::maddp(Vector<T>& r,
                        const Vector<T>& a,
                        const Vector<T>& b,
                        const Vector<T>& c,
                        const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_madd<4, T>(r.data+i*16, a.data+i*16, b.data+i*16,
                                  c.data+i*16);
      }
    }
  }

  /**************************************************************************/
  /*r = f * a + b*/
  template<class T>
  void Vector<T>::mfaddp(Vector<T>& r, T f,
                         const Vector<T>& a,
                         const Vector<T>& b,
                         const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_msadd<4, T>(r.data+i*16, f, a.data+i*16, b.data+i*16);
      }
    }
  }

  /**************************************************************************/
  /*r = f * a + b + d*/
  template<class T>
  void Vector<T>::mfadd2p(Vector<T>& r, T f,
                          const Vector<T>& a,
                          const Vector<T>& b,
                          const Vector<T>& d,
                          const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_msadd2<4, T>(r.data+i*16, f, a.data+i*16, b.data+i*16,
                                    d.data+i*16);
      }
    }
  }

  /**************************************************************************/
  /*r = a .* b*/
  template<class T>
  void Vector<T>::mulp(Vector<T>& r,
                       const Vector<T>& a,
                       const Vector<T>& b,
                       const VectorRange rg){
    RANGE_ASSERT_AB;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_mul<4, T>(r.data+i*16, a.data+i*16, b.data+i*16);
      }
    }
  }

  /**************************************************************************/
  /*r = a * f*/
  template<class T>
  void Vector<T>::mulfp(Vector<T>& r,
                        const Vector<T>& a,
                        T f,
                        const VectorRange rg){
    RANGE_ASSERT_A;

    for(int i=rg.startBlock/16;i<rg.endBlock/16;i++){
      if(i<a.size/16){
        spmatrix_block_muls<4, T>(r.data+i*16, a.data+i*16, f);
      }
    }
  }

  /**************************************************************************/
  template<class T>
  Vector<T>& Vector<T>::operator+=(const Vector<T>& v){
    Vector<T>::add(*this, *this, v);
    return *this;
  }

  /**************************************************************************/
  template<class T>
  Vector<T>& Vector<T>::operator-=(const Vector<T>& v){
    Vector<T>::sub(*this, *this, v);
    return *this;
  }

  /**************************************************************************/
  template<class T>
  Vector<T>& Vector<T>::operator*=(const Vector<T>& v){
    Vector<T>::mul(*this, *this, v);
    return *this;
  }

  /**************************************************************************/
  template<class T>
  Vector<T>& Vector<T>::operator/=(const Vector<T>& v){
    Vector<T>::div(*this, *this, v);
    return *this;
  }

  /**************************************************************************/
  template<class T>
  T Vector<T>::length2() const{
    if(origSize == 0){
      return 0;
    }

    int steps = computeStepsVec(size);
    bool first = true;

    while(steps >= 1){
      for(int j=0;j<steps;j++){
        int idx1 = j;
        int idx2 = j + steps;

        if(first){
          /*Result is always positive!*/
          if(idx2 < size){
            tmp_buffer_pos[idx1] =
              data[idx1] * data[idx1] + data[idx2] * data[idx2];
          }else{
            tmp_buffer_pos[idx1] =
              data[idx1] * data[idx1];
          }
        }else{
          if(idx2 < size){
            tmp_buffer_pos[idx1] += tmp_buffer_pos[idx2];
          }
        }
      }
      steps/=2;
      first = false;
    }

    return tmp_buffer_pos[0];
  }

#if 0
#ifdef SSE2
  /**************************************************************************/
  template<>
  float Vector<float>::length2() const{
    tslassert(size%16 == 0);

    int steps = computeStepsVec(size);
    bool first = true;

    while(steps >= 4){
      for(int j=0;j<steps/4;j++){
        int idx1 = j*4;
        int idx2 = j*4 + steps;

        if(first){
          if(idx2 < size){
            __m128 XMM1 = _mm_load_ps(data + idx1);
            __m128 XMM2 = _mm_load_ps(data + idx2);
            XMM1 = _mm_mul_ps(XMM1, XMM1);
            XMM2 = _mm_mul_ps(XMM2, XMM2);

            XMM1 = _mm_add_ps(XMM1, XMM2);

            _mm_store_ps(tmp_buffer + idx1, XMM1);
          }else{
            __m128 XMM1 = _mm_load_ps(data + idx1);
            XMM1 = _mm_mul_ps(XMM1, XMM1);
            _mm_store_ps(tmp_buffer + idx1, XMM1);
          }
        }else{
          if(idx2 < size){
            __m128 XMM1 = _mm_load_ps(tmp_buffer + idx1);
            __m128 XMM2 = _mm_load_ps(tmp_buffer + idx2);
            XMM1 = _mm_add_ps(XMM1, XMM2);
            _mm_store_ps(tmp_buffer + idx1, XMM1);
          }
        }
      }
      steps/=2;
      first = false;
    }

    return ((tmp_buffer[0] + tmp_buffer[2]) +
            (tmp_buffer[1] + tmp_buffer[3]));
  }
#endif
#endif
}

#endif/*VECTOR_HPP*/
