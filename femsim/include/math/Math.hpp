/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MATH_HPP
#define MATH_HPP
#include <math.h>
#include "core/tsldefs.hpp"
#include "core/Exception.hpp"
#include "core/types.hpp"
#include <iostream>

/**************************************************************************/
#ifndef PI
#define PI 3.14159265358979323846
#endif

/**************************************************************************/
#ifndef H_PI
#define H_PI (PI/2.0)
#endif

/**************************************************************************/
#ifndef D_PI
#define D_PI (PI*2.0)
#endif

/**************************************************************************/
#ifndef EULER
#define EULER 2.71828183
#endif

/**************************************************************************/
#ifndef PHI
#define PHI 1.6180339887498948482
#endif

/**************************************************************************/
#ifndef INV_PHI
#define INV_PHI 0.6180339887498948482
#endif

#include <limits>

#ifdef _WIN32
#define isnan(x) _isnan((x))
#endif



//#define FLOAT_MAX  1e+100
//#define FLOAT_MIN -1e+100
#define FLOAT_MAX std::numeric_limits<float>::max()
#define FLOAT_MIN std::numeric_limits<float>::min()

namespace tsl{
#ifdef __APPLE__
  typedef unsigned long ulong;
#endif
  /**************************************************************************/
  template<class T>
  class Math{
  public:
    static const T Pi = (T)3.14159265358979323846;
    static const T DPi;
    static const T HPi;
    static const T E = (T)2.7182818284590452353602874713527;
    static const T Max;
    static const T Min;
    static const T Phi = (T)1.6180339887498948482;
    static const T InvPhi = (T)0.6180339887498948482;
  };

  /**************************************************************************/
  template<class T>
  const T Math<T>::DPi = Math<T>::Pi * (T)2.0;

  /**************************************************************************/
  template<class T>
  const T Math<T>::HPi = Math<T>::Pi * (T)0.5;

  /**************************************************************************/
  template<class T>
  const T Math<T>::Max = std::numeric_limits<T>::max();

  /**************************************************************************/
  template<class T>
  const T Math<T>::Min = std::numeric_limits<T>::min();

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const T& v){
    if(IsFloatType<T>()){
      const unsigned long u = *(unsigned long*)&v;
      return (u&0x7F800000) == 0x7F800000 && (u&0x7FFFFF);
    }else if(IsDoubleType<T>()){
      const unsigned long long u = *(unsigned long long*)&v;
      return (u&0x7FF0000000000000ULL) == 0x7FF0000000000000ULL && (u&0xFFFFFFFFFFFFFULL);
    }else{
      error("IsNan applied on non a non floating point value");
      return false;
    }
  }

  /**************************************************************************/
  template<class T>
  static inline T Nan(){
    return (T)0.0/(T)0.0;
  }

  /**************************************************************************/
  template<class T>
  inline T MultiplyIdentity(const T&){
    error("calling default identity function");
    return T();
  }

  /**************************************************************************/
  template<>
  inline float MultiplyIdentity(const float&){
    return 1.0f;
  }

  /**************************************************************************/
  template<>
  inline double MultiplyIdentity(const double&){
    return 1.0;
  }

  /**************************************************************************/
  template<>
  inline long double MultiplyIdentity(const long double&){
    return (long double)1.0;
  }

  /**************************************************************************/
  template<class T>
  inline T SumIdentity(const T&){
    error("calling default identity function");
    return T();
  }

  /**************************************************************************/
  template<>
  inline float SumIdentity(const float&){
    return 0.0f;
  }

  /**************************************************************************/
  template<>
  inline double SumIdentity(const double&){
    return 0.0;
  }

  /**************************************************************************/
  template<>
  inline long double SumIdentity(const long double&){
    return (long double)0.0;
  }

  /**************************************************************************/
  template<class T>
  inline const T& Min(const T& a,
                      const T& b){
    return (a > b) ? b : a;
  }

  /**************************************************************************/
  template<class T>
  inline const T& Max(const T& a,
                      const T& b){
    return (a > b) ? a : b;
  }

  /**************************************************************************/
  template<class T>
  inline const T& Max(const T& a,
                      const T& b,
                      const T& c){
    return Max(a, Max(b, c));
  }

  /**************************************************************************/
  template<class T>
  inline const T& Min(const T& a,
                      const T& b,
                      const T& c){
    return Min(a, Min(b, c));
  }

  /**************************************************************************/
  template<class T>
  inline const T& Min(const T& a,
                      const T& b,
                      const T& c,
                      const T& d){
    return Min(Min(a, b), Min(c, d));
  }

  /**************************************************************************/
  template<class T>
  inline const T& Max(const T& a,
                      const T& b,
                      const T& c,
                      const T& d){
    return Max(Max(a, b), Max(c, d));
  }

  /**************************************************************************/
  template<class T>
  inline T Pos(const T& a){
    return Max(a, (T)0.0);
  }

  /**************************************************************************/
  template<class T>
  inline T Neg(const T& a){
    return Min(a, (T)0.0);
  }

  /**************************************************************************/
  template<class T>
  inline const T& Clamp(const T& a,
                        const T& min,
                        const T& max){
    if(a < min){
      return min;
    }else if(a > max){
      return max;
    }
    return a;
  }

  /**************************************************************************/
  template<class T>
  inline T Clamp01(const T& a){
    return Clamp(a, (T)0.0, (T)1.0);
  }

  /**************************************************************************/
  template<class T>
  inline T Sqr(const T& f){
    if(IsRealType<T>()){
      return f * f;
    }else{
      error("Sqr applied on non a non floating point value");
      return (T)0;
    }
  }

  /**************************************************************************/
  template<class T>
  inline T Sqrt(const T& v){
    error("Sqrt applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Sqrt(const float& v){
    return sqrtf(v);
  }

  /**************************************************************************/
  template<>
  inline double Sqrt(const double& v){
    return sqrt(v);
  }

  /**************************************************************************/
  template<>
  inline long double Sqrt(const long double& v){
    return sqrtl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Abs(const T& v){
    error("Abs applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Abs(const float& v){
    return fabsf(v);
  }

  /**************************************************************************/
  template<>
  inline double Abs(const double& v){
    return fabs(v);
  }

  /**************************************************************************/
  template<>
  inline long double Abs(const long double& v){
    return fabsl(v);
  }

#if 0
  template<class T>
  inline bool Equal(const T& a, const T& b, const T& eps = (T)1e-6){
    if(Abs(a-b) < ((Abs(a) < Abs(b)) ? Abs(b) : Abs(a)) * eps){
      return true;
    }
    return false;
  }

  template<class T>
  inline bool NotEqual(const T& a, const T& b, const T& eps = (T)1e-6){
    return !Equal(a, b, eps);
  }
#endif

  /**************************************************************************/
  template<class T>
  inline T Mod(const T& v, const T& b){
    error("Mod applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Mod(const float& v, const float& b){
    return fmodf(v, b);
  }

  /**************************************************************************/
  template<>
  inline double Mod(const double& v, const double& b){
    return fmod(v, b);
  }

  /**************************************************************************/
  template<>
  inline long double Mod(const long double& v, const long double& b){
    return fmodl(v, b);
  }

  /**************************************************************************/
  template<class T>
  inline T Sign(const T& a, const T& b){
    return (b >= SumIdentity(T())) ? Abs(a) : -Abs(a);
  }

  /**************************************************************************/
  template<class T>
  inline T Sign(const T& n){
    return Sign(MultiplyIdentity(T()), n);
  }

  /**************************************************************************/
  template<class T>
  inline T Log(const T& v){
    error("Log applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Log(const float& v){
    return logf(v);
  }

  /**************************************************************************/
  template<>
  inline double Log(const double& v){
    return log(v);
  }

  /**************************************************************************/
  template<>
  inline long double Log(const long double& v){
    return logl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Ceil(const T& v){
    error("Ceil applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Ceil(const float& v){
    return ceilf(v);
  }

  /**************************************************************************/
  template<>
  inline double Ceil(const double& v){
    return ceil(v);
  }

  /**************************************************************************/
  template<>
  inline long double Ceil(const long double& v){
    return ceill(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Floor(const T& v){
    error("Floor applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Floor(const float& v){
    return floorf(v);
  }

  /**************************************************************************/
  template<>
  inline double Floor(const double& v){
    return floor(v);
  }

  /**************************************************************************/
  template<>
  inline long double Floor(const long double& v){
    return floorl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Exp(const T& v){
    error("Exp applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Exp(const float& v){
    return expf(v);
  }

  /**************************************************************************/
  template<>
  inline double Exp(const double& v){
    return exp(v);
  }

  /**************************************************************************/
  template<>
  inline long double Exp(const long double& v){
    return expl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Round(const T& v){
    error("Round applied on a non floating point type");
    return (T)0.0;
  }

  /**************************************************************************/
  template<>
  inline float Round(const float& v){
    return roundf(v);
  }

  /**************************************************************************/
  template<>
  inline double Round(const double& v){
    return round(v);
  }

  /**************************************************************************/
  template<>
  inline long double Round(const long double& v){
    return roundl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Tan(const T& v){
    error("Tan applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Tan(const float& v){
    return tanf(v);
  }

  /**************************************************************************/
  template<>
  inline double Tan(const double& v){
    return tan(v);
  }

  /**************************************************************************/
  template<>
  inline long double Tan(const long double& v){
    return tanl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T CoTan(const T& v){
    return (T)1.0/Tan(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Atan(const T& v){
    error("ATan applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Atan(const float& v){
    return atanf(v);
  }

  /**************************************************************************/
  template<>
  inline double Atan(const double& v){
    return atan(v);
  }

  /**************************************************************************/
  template<>
  inline long double Atan(const long double& v){
    return atanl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Atan2(const T& y,
                 const T& x){
    error("Atan2 applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Atan2(const float& y,
                     const float& x){
    return atan2f(y, x);
  }

  /**************************************************************************/
  template<>
  inline double Atan2(const double& y,
                      const double& x){
    return atan2(y, x);
  }

  /**************************************************************************/
  template<>
  inline long double Atan2(const long double& y,
                           const long double& x){
    return atan2l(y, x);
  }

  /**************************************************************************/
  /*Custom Atan2 func*/
  template<class T>
  inline T Atan22(const T& y,
                  const T& x){
    if(y != (T)0.0){
      return (T)2.0 * Atan( (Sqrt(Sqr(x) + Sqr(y)) - x) / y);
    }else if(x > (T)0.0){
      return (T)0.0;
    }else if(x < (T)0.0){
      return (T)PI;
    }else{
      error("Atan22 undefined for (0,0)");
      return (T)0.0;
    }
  }

  /**************************************************************************/
  template<class T>
  inline T Sin(const T& v){
    error("Sin applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Sin(const float& v){
    return sinf(v);
  }

  /**************************************************************************/
  template<>
  inline double Sin(const double& v){
    return sin(v);
  }

  /**************************************************************************/
  template<>
  inline long double Sin(const long double& v){
    return sinl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T ArcSin(const T& v){
    error("ArcSin applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float ArcSin(const float& v){
    tslassert(v >= -(float)1.0);
    tslassert(v <=  (float)1.0);
    return asinf(v);
  }

  /**************************************************************************/
  template<>
  inline double ArcSin(const double& v){
    tslassert(v >= -(double)1.0);
    tslassert(v <=  (double)1.0);
    return asin(v);
  }

  /**************************************************************************/
  template<>
  inline long double ArcSin(const long double& v){
    tslassert(v >= -(long double)1.0);
    tslassert(v <=  (long double)1.0);
    return asinl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T Cos(const T& v){
    error("Cos applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Cos(const float& v){
    return cosf(v);
  }

  /**************************************************************************/
  template<>
  inline double Cos(const double& v){
    return cos(v);
  }

  /**************************************************************************/
  template<>
  inline long double Cos(const long double& v){
    return cosl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T ArcCos(const T& v){
    error("Acos applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float ArcCos(const float& v){
    tslassert(v >= -(float)1.0);
    tslassert(v <=  (float)1.0);
    return acosf(v);
  }

  /**************************************************************************/
  template<>
  inline double ArcCos(const double& v){
    tslassert(v >= -(double)1.0);
    tslassert(v <=  (double)1.0);
    return acos(v);
  }

  /**************************************************************************/
  template<>
  inline long double ArcCos(const long double& v){
    tslassert(v >= -(long double)1.0);
    tslassert(v <=  (long double)1.0);
    return acosl(v);
  }

  /**************************************************************************/
  template<class T>
  inline T SafeArcCos(T v){
    return ArcCos(Clamp(v, (T)-1.0, (T)1.0));
  }

  /**************************************************************************/
  template<class T>
  inline T RadiansToDegrees(T rad){
    return rad * (T)360.0 / (T)D_PI;
  }

  /**************************************************************************/
  template<class T>
  inline T DegreesToRadians(T deg){
    return deg * (T)D_PI / (T)360.0;
  }

  /**************************************************************************/
  template<class T>
  inline T Log2(const T& n){
    return Log(n) / Log((T)2.0);
  }

  /**************************************************************************/
  template<class T>
  inline T Pow(const T& v, const T& p){
    error("Pow applied on non a non floating point value");
    return (T)0;
  }

  /**************************************************************************/
  template<>
  inline float Pow(const float& v, const float& p){
    return powf(v, p);
  }

  /**************************************************************************/
  template<>
  inline double Pow(const double& v, const double& p){
    return pow(v, p);
  }

  /**************************************************************************/
  template<>
  inline long double Pow(const long double& v, const long double& p){
    return powl(v, p);
  }

  /**************************************************************************/
  template<class T>
  inline T Coth(const T& v){
    T t = Pow<T>((T)EULER, v);
    return (t + (T)1.0/t) / (t - (T)1.0 / t);
  }

  /**************************************************************************/
  template<class T>
  inline T kroneckerDelta(int i,
                          int j){
    return (i == j)?(T)1.0:(T)0.0;
  }

  /**************************************************************************/
  template<class T>
  T pythagoras(T a,
               T b){
    T at = Abs(a);
    T bt = Abs(b);
    T ct, result;

    if(at > bt){
      ct = bt / at;
      result = at * Sqrt((T)1.0 + ct * ct);
    }else if(bt > (T)0.0){
      ct = at / bt;
      result = bt * Sqrt((T)1.0 + ct * ct);
    }else{
      return (T)0.0;
    }

    return result;
  }

  /**************************************************************************/
  template<class T>
  inline void quicksort(T* data,
                        int start,
                        int end){
    if(start == end){
      return;
    }
    T midValue = data[(start + end)/2];

    int left = start;
    int right = end;

    do{
      while(data[left] < midValue){
        left++;
      }

      while(midValue < data[right]){
        right--;
      }

      if(left <= right){
        /*Swap*/
        T valueLeft = data[left];
        data[left] = data[right];
        data[right] = valueLeft;
        left++;
        right--;
      }
    }while(left <= right);
    if(start < right){
      /*Sort left*/
      quicksort(data, start, right);
    }

    if(left < end){
      /*Sort right*/
      quicksort(data, left, end);
    }
  }

  /**************************************************************************/
  template<class T>
  inline T aitkenD2(T pn,
                    T pn1,
                    T pn2){
    T denom = pn2 - (T)2.0 * pn1 + pn;
    if(Abs(denom) < (T)1e-16){
      /*second derivative is zero*/
      return pn;
    }

    return pn - Sqr(pn1 - pn)/denom;
  }

  /**************************************************************************/
  template<class T, class Y>
  inline Y linterp(T iso,
                   Y* d1,
                   Y* d2,
                   T val1,
                   T val2,
                   T tol = (T)1E-40){
    Y *op1, *op2;
    T opv1, opv2;

    if(val1 > val2){
      op1  = d1;
      op2  = d2;
      opv1 = val1;
      opv2 = val2;
    }else{
      op1  = d2;
      op2  = d1;
      opv1 = val2;
      opv2 = val1;
    }

    T mu;

    if(Abs(iso - opv1) < tol){
      return *op1;
    }
    if(Abs(iso - opv2) < tol){
      return *op2;
    }
    if(Abs(opv1 - opv2) < tol){
      return *op1;
    }

    mu = (iso - opv1)/(opv2 - opv1);

    return *op1 + mu * (*op2 - *op1);
  }

  /**************************************************************************/
  template<class T>
  void machine_epsilon(){
    T mach_eps = (T)1.0;
    do{
      mach_eps /= (T)2.0;
    }while((T)(1.0 + (mach_eps/(T)2.0)) != (T)1.0);
    message("Machine epsilon = %10.10e", mach_eps/(T)2.0);
  }

  /**************************************************************************/
  struct _VectorRange{
    int startBlock;
    int endBlock;
    int range;
  };

  typedef struct _VectorRange VectorRange;

  /**************************************************************************/
  struct _MatrixRange{
    int startRow;
    int endRow;
    int range;
  };

  typedef struct _MatrixRange MatrixRange;
}

#endif/*MATH_HPP*/
