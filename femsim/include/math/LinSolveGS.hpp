/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef LINSOLVEGS_HPP
#define LINSOLVEGS_HPP

#include "math/LinSolve.hpp"
#include "math/Matrix44.hpp"

//#define TOL 1e-16
#define GSTOL 1e-6

#define ITP float

namespace tsl{
  /**************************************************************************/
  /*GausSeidel solver*/
  template<int N, class T>
  class LinSolveGS : public LinSolve<N, T>{
  public:
    /**************************************************************************/
    LinSolveGS(int d):LinSolve<N, T>(d){
      message("LinSolve::LinSolveGS");
      /*Gauss-Seidel does not require additional storage*/
      x0 = new Vector<T>(this->dim);
      r = new Vector<T>(this->dim);

      r2 = new Vector<T>(this->dim);

      x0->clear();
      this->x->clear();
      this->b->clear();
    }

    /**************************************************************************/
    virtual ~LinSolveGS(){
      delete x0;
      delete r;
      delete r2;
    }

    /**************************************************************************/
    virtual void preSolve(){
    }

    /**************************************************************************/
    virtual void solve(int steps = 10000,
                       T eps=(T)1e-6){
      tslassert(this->mat->getWidth() == this->b->getSize());
      tslassert(this->mat->getWidth() == this->mat->getHeight());
      tslassert(this->mat->getWidth() == this->x->getSize());

      this->lastError = (T)0.0;

      this->mat->finalize();

      this->iterations = 0;

      for(int i=0;i<steps;i++){
        *x0 = *this->x;
        this->iterations++;

        for(int j=0;j<this->mat->height/N;j++){
          /*For each blockrow*/

          for(int m=0;m<N;m++){
            if(j*N+m >= this->mat->getHeight()){
              continue;
            }

            /*For each row in blockrow*/
            T divider = (T)0.0;
            T sum = (T)0.0;

            for(int k=0;k<this->mat->row_lengths[j];k++){
              int block_index = this->mat->block_indices[j][k];
              int block_col   = this->mat->col_indices[j][k] * N;

              for(int l=0;l<N;l++){
                int l_row = m;
                int l_col = l;
                int row = j*N + l_row;
                int col = block_col + l_col;

                T val = this->mat->blocks[block_index].m[l_row*N+l];

                if(val != 0){
                  if(row == col){
                    divider = val;
                  }else if(col < row){
                    sum += -val * (*this->x)[col];
                  }else{
                    sum += -val * (*x0)[col];
                  }
                }
              }
            }

            if(divider != (T)0.0){
              sum += (*this->b)[j*N+m];
              sum /= divider;

#if 1 //PROJECTION
              if(this->projector != 0){
                int index = j*N+m;

                int projectionType = (int)(*this->projector)[index];

                /*Multiplier should be negative*/
                if(projectionType == -1){
                  if(sum > -1e-5*0){
                    sum = 0;
                  }
                }

                /*Multiplier should be positive*/
                if(projectionType == 1){
                  if(sum < 1e-5*0){
                    sum = 0;
                  }
                }

                /*Friction correction*/
                if(projectionType > 10){
                  /*Friction*/
                  T mu = (T)(projectionType - 1000)/(T)1000.0;
                  if(index%3 != 0){
                    //friction component
                    int normal_index = index/3;
                    normal_index *= 3;
                    tslassert(normal_index %3 == 0);

                    T fwmax = mu * Pos((*this->x)[normal_index]);
                    //message("fwmax = %10.10e, gamma = %10.10e",
                    //      fwmax, sum);
                    if(Abs(sum) > fwmax){
                      sum = fwmax * Sign(sum);
                    }
                    //message("Corrected = %10.10e", sum);
                  }
                }

                if(projectionType == 0){
                  /*No projection, equality constraint*/
                }
              }
#endif
              (*this->x)[j*N+m] = sum;
            }else{
              (*this->x)[j*N+m] = SumIdentity(T());
            }
          }
        }

#if 0
        for(int j=0;j<this->mat->getHeight();j++){
          if((*this->x)[j] < 0){
            (*this->x)[j] = 0;
          }
        }
#endif
        //Vector<T>::sub(*r, *this->x, *x0);
        //Vector<T>::mul(*r, *r, *r);

        //this->lastError = Sqrt(residual2);
        Vector<T>::sub(*r, *this->x, *x0);

        for(int j=0;j<this->mat->getHeight();j++){
          (*r)[j] *= (*this->mat)[j][j];
#if 1
          if((*this->x)[j] < SumIdentity(T())){
            //(*r)[j] = 0;
          }
#else
          if((*this->x)[j] < SumIdentity(T())){
            //(*r)[j] = 0;
          }
#endif
        }
        Vector<T>::mul(*r, *r, *r);

        T residual = r->sum();

        this->lastError = Sqrt(residual);
        //std::cout << *r;
        //message("GS::Residual[%d] = %10.10e, %10.10e, %10.10e", i, residual, Sqrt(residual), Sqrt(residual));
        if(Sqrt(Abs(residual)) < eps){
          //message("GS::Residual[%d] = %10.10e, %10.10e, %10.10e", i, residual, Sqrt(residual), Sqrt(residual2));
          //message("Success");
          return;
        }
      }
    }

  protected:
    Vector<T>* x0;
    Vector<T>* r;
    Vector<T>* r2;
  };

  /**************************************************************************/
  /*Block-GausSeidel solver*/
  template<int N, class T>
  class LinSolveBGS : public LinSolve<N, T>{
  public:
    /**************************************************************************/
    LinSolveBGS(int d, int bs):LinSolve<N, T>(d){
      message("LinSolve::LinSolveBGS");
      block_size = bs;
      if(bs == 0 || bs > 4){
        error("Invalid block-size for Block-Gauss-Seidel solver");
      }
      /*Block-Gauss-Seidel does not require additional storage*/
      x0 = new Vector<T>(this->dim);
      r = new Vector<T>(this->dim);

      r2 = new Vector<T>(this->dim);

      x0->clear();
      this->x->clear();
      this->b->clear();
    }

    /**************************************************************************/
    virtual ~LinSolveBGS(){
      delete x0;
      delete r;
      delete r2;
    }

    /**************************************************************************/
    virtual void preSolve(){
    }

    /**************************************************************************/
    virtual void solve(int steps = 10000){
      tslassert(this->mat->getWidth() == this->b->getSize());
      tslassert(this->mat->getWidth() == this->mat->getHeight());
      tslassert(this->mat->getWidth() == this->x->getSize());

      this->mat->finalize();

      for(int i=0;i<steps;i++){
        *x0 = *this->x;

        int block_rows = this->mat->height / block_size;
        if(this->mat->height % block_size != 0){
          block_rows++;
        }

        for(int j=0;j<block_rows;j++){
          /*Local block and matrices*/
          Matrix44<T> block_mat;
          Vector4<T>  block_vec;
          block_mat.eye();

          for(int k=0;k<block_size;k++){
            int current_row = j*block_size + k;
            int current_block_row = current_row / N;

            if(current_row >= this->mat->getHeight()){
              continue;
            }

            /*Collect data from blockrows*/
            for(int l=0;l<this->mat->row_lengths[current_block_row];l++){
              int block_index = this->mat->block_indices[current_block_row][l];
              int block_col   = this->mat->col_indices[current_block_row][l]*N;

              for(int m=0;m<N;m++){
                int l_row = current_row%N;
                int l_col = m;
                int row = current_row;
                int col = block_col + l_col;

                T val = this->mat->blocks[block_index].m[l_row*N+l_col];

                if(val != 0){
                  if((row/block_size) == (col/block_size)){
                    block_mat[k][col%block_size] = val;
                  }else if((col/block_size) < (row/block_size)){
                    //sum += -val * (*this->x)[col];
                    block_vec[(int)k] += -val * (*this->x)[col];
                  }else{
                    //sum += -val * (*x0)[col];
                    block_vec[(int)k] += -val * (*x0)[col];
                  }
                }
              }
            }
            block_vec[k] += (*this->b)[current_row];
          }
          if(j*block_size < this->mat->getHeight()){
            block_mat = block_mat.inverse();

            Vector4<T> rr = block_mat * block_vec;

            for(int k=0;k<block_size;k++){
              if(j*block_size + k < this->mat->getHeight()){
                message("x[%d] = %f", j*block_size+k, rr[k]);
                if(rr[k]<0){
                  rr[k] = 0;
                }
                (*this->x)[j*block_size + k] = rr[k];
              }
            }
          }
        }

        std::cout << *this->x;

        Vector<T>::sub(*r, *this->x, *x0);
        Vector<T>::mul(*r, *r, *r);

        T residual = r->sum();

        if(Sqrt(Abs(residual)) < 1E-6){
          message("Residual[%d] = %10.10e, %10.10e",
                  i, residual, Sqrt(residual));
          message("Success");
          //message("b = ");
          //std::cout << *this->b;

          message("x = ");
          std::cout << *this->x;

          //message("matrix");
          //std::cout << *this->mat;

          //this->mat->finalize();
          //spmv(*r, *this->mat, *this->x);
          //message("b recomputed = ");
          //std::cout << *this->r;
          return;
        }
      }
    }

  protected:
    /**************************************************************************/
    Vector<T>* x0;
    Vector<T>* r;
    Vector<T>* r2;

    int block_size;
  };
}

#endif/*LINSOLVEGS_HPP*/
