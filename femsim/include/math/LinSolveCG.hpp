/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef LINSOLVECG_HPP
#define LINSOLVECG_HPP

#include "math/LinSolve.hpp"
#include "core/Exception.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  class LinSolveCG : public LinSolve<N, T>{
  public:
    /**************************************************************************/
    LinSolveCG(int d):LinSolve<N, T>(d){
      r = new Vector<T>(this->dim);
      C = new Vector<T>(this->dim);
      scratch1 = new Vector<T>(this->dim);
      scratch2 = new Vector<T>(this->dim);

      w = new Vector<T>(this->dim);
      v = new Vector<T>(this->dim);
      u = new Vector<T>(this->dim);
    }

    /**************************************************************************/
    virtual ~LinSolveCG(){
      delete r;
      delete C;
      delete scratch1;
      delete scratch2;
      delete w;
      delete v;
      delete u;
    }

    /**************************************************************************/
    virtual void clear(){
      LinSolve<N, T>::clear();
      r->clear();
      C->clear();
      scratch1->clear();
      scratch2->clear();
      w->clear();
      v->clear();
      u->clear();
    }

    /**************************************************************************/
    virtual void preSolve(){
      this->mat->finalize();
      Vector<T>::mul(*scratch1, *this->b, *this->b);
      bnorm = Sqrt(scratch1->sum());
    }

    /**************************************************************************/
    virtual void solve(int steps = 100000,
                       T tolerance = (T)1e-6){
      tslassert(this->mat->getWidth() == this->b->getSize());
      tslassert(this->mat->getWidth() == this->mat->getHeight());
      tslassert(this->mat->getWidth() == this->x->getSize());

      this->iterations = 0;

      if(this->linOperator == 0){
        for(int i=0;i<this->mat->getHeight();i++){
          (*C)[i] = (T)1.0/Sqrt(Abs((*this->mat)[i][i]));
        }
      }else{
        for(int i=0;i<this->mat->getHeight();i++){
          (*C)[i] = (T)1.0;
        }
      }

      /*r = Ax*/
      if(this->linOperator == 0){
        spmv(*r, *(this->mat), *(this->x));
      }else{
        this->linOperator->performOperator(*r, *(this->x));
      }

      /*r = b - r*/;
      Vector<T>::sub(*scratch1, *(this->b), *r);
      *r = *scratch1;

      /*w = C * r*/
      Vector<T>::mul(*w, *C, *r);

      /*v = C * w*/
      Vector<T>::mul(*v, *C, *w);

      /*s1 = w * w*/
      Vector<T>::mul(*scratch1, *w, *w);
      T alpha;

      /*alpha = sum(s1)*/
      alpha = scratch1->sum();

      int k=0;

      while(k<steps){
        /*s1 = v * v*/
        this->iterations++;
        Vector<T>::mul(*scratch1, *v, *v);
        T residual;

        residual = scratch1->sum();
        message("r = %10.10e, %d", residual, this->linOperator == 0);
        if(Sqrt(Abs(residual)) < (tolerance/*bnorm*/ /*+ tolerance*/)){
          warning("CG::Success1 in %d iterations, %10.10e, %10.10e",
                  k, residual, Sqrt(residual));
          return;
        }

        /*u = A*v*/
        if(this->linOperator == 0){
          spmv(*u, *(this->mat), *v);
        }else{
          this->linOperator->performOperator(*u, *v);
        }

        /*s1 = v * u*/
        Vector<T>::mul(*scratch1, *v, *u);
        T divider;

        /*divider = sum(s1)*/
        divider = scratch1->sum();

        T t = alpha/divider;

        /*x = x + t*v*/
        /*r = r - t*u*/
        /*w = C * r*/
        Vector<T>::mfadd(*(this->x),  t, *v, *(this->x));
        Vector<T>::mfadd(*this->r, -t, *u, *r);
        Vector<T>::mul  (*w, *C, *r);

        /*s1 = w*w*/
        Vector<T>::mul(*scratch1, *w, *w);

        /*beta = sum(s1)*/
        T beta = scratch1->sum();
#if 0
        if(beta < (tolerance*bnorm + tolerance)){
          T rl = r->length2();
          if(Sqrt(rl)<(/*tolerance*bnorm + */tolerance)){
            warning("CG::Success2 in %d iterations, %10.10e, %10.10e", k, rl,
                    Sqrt(Abs(rl)));
            return;
          }
        }
#endif

        T s = beta/alpha;

        /*s1 = C * w*/
        Vector<T>::mul(*scratch1, *C, *w);
        /*v = s1 + s * v*/
        Vector<T>::mfadd(*v, s, *v, *scratch1);

        alpha = beta;
        k++;
      }
      message("Unsuccesfull");
      throw new SolutionNotFoundException(__LINE__, __FILE__,
                                          "Number of iterations exceeded.");

    }
  protected:
    /**************************************************************************/
    Vector<T>* r;
    Vector<T>* C;
    Vector<T>* scratch1;
    Vector<T>* scratch2;
    Vector<T>* w;
    Vector<T>* v;
    Vector<T>* u;
    T bnorm;
  };
}

#endif/*LINSOLVECG*/
