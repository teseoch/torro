/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef VECTOR3_HPP
#define VECTOR3_HPP

#include "math/Vector4.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Vector3{
  public:
    /**************************************************************************/
    Vector3(){
      memset(m, 0, 3*sizeof(T));
    }

    /**************************************************************************/
    Vector3(const Vector4<T>& v){
      m[0] = v.m[0];
      m[1] = v.m[1];
      m[2] = v.m[2];
    }

    /**************************************************************************/
    Vector3(T a,
            T b,
            T c){
      m[0] = a;
      m[1] = b;
      m[2] = c;
    }

    /**************************************************************************/
    T& operator[](int i){
      return m[i];
    }

    /**************************************************************************/
    const T& operator[](int i)const{
      return m[i];
    }

    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Vector3<Y>& v);
  public:
    T m[3];
  };

  /**************************************************************************/
  template<class T>
  inline std::ostream& operator<<(std::ostream& os,
                                  const Vector3<T>& v){
    os << v.m[0] << '\t' << v.m[1] << '\t' << v.m[2] << std::endl;
    return os;
  }

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const Vector3<T>& v){
    return IsNan(v[0]) || IsNan(v[1]) || IsNan(v[2]);
  }

  typedef Vector3<float> Vector3f;
  typedef Vector3<double> Vector3d;

}

#endif/*VECTOR3_HPP*/
