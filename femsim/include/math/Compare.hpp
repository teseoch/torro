/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef COMPARE_HPP
#define COMPARE_HPP

#include "core/types.hpp"

namespace tsl{
  /**************************************************************************/
  /*Default compare class which generates an error if pointers are
    compared. Since the value of pointers depend on time and other
    descisions made by the operating system, a comparison will result
    in non-deterministic behaviour of the program.

    For object types that do not have the < and == operators defined,
    a specialized Compare should be provided.
  */
  template<class T>
  class Compare{
  public:
    /**************************************************************************/
    Compare(){
     if(IsPointerType<T>() == true){
        error("Instancing a default compare function which just compares pointers. If you want to do this, please instantiate a Compare function for the pointervalues of type T. When used like this, the resulting program becomes non-deterministic.");
      }
    }

    /**************************************************************************/
    static bool less(const T& a,
                     const T& b){
      return a < b;
    }

    /**************************************************************************/
    static bool equal(const T& a,
                      const T& b){
      return a == b;
    }
  };
}

#endif/*COMPARE_HPP*/
