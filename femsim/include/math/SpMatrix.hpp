/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SPMATRIX_HPP
#define SPMATRIX_HPP

#include "core/tsldefs.hpp"
#include "math/Math.hpp"
#include "math/SpMatrixBlock.hpp"
#include "datastructures/Tree.hpp"
#include "datastructures/List.hpp"
#include "math/SpMatrixReorder.hpp"

#include <ostream>
#include <map>
#include <stdio.h>

#ifdef CUDA
/*Number of blocks processed simultaneously*/
#define SIM_BLOCKS  2

/*A block row is divided in a number of segment of size SEG_LENGTH*/
#define SEG_LENGTH  8

#define THREADS 16
#endif

namespace tsl{
  /**************************************************************************/
  template<class T>
  class Vector;

  /**************************************************************************/
  typedef struct _reverse_index reverse_index_t;
  struct _reverse_index{
    int blockRow;
    int blockCol;
  };

  /**************************************************************************/
  template<int N, class T>
  class SpMatrix;

  /**************************************************************************/
  template<int N, class T>
  class Cholesky;

#ifdef USE_THREADS
  /**************************************************************************/
  class ThreadPool;

  /**************************************************************************/
  template<int N, class T>
  class ParallelSPMVTask;
#endif

  /**************************************************************************/
  template<int N, class T>
  class SpMatrixTuple{
  public:
    T val;
    int row;
    int col;
  };

  /**************************************************************************/
  template<int N, class T>
  class Compare<SpMatrixTuple<N, T> >{
  public:
    /**************************************************************************/
    static bool less(const SpMatrixTuple<N, T>& a,
                     const SpMatrixTuple<N, T>& b){
      if(a.row/N < b.row/N){
        return true;
      }else if(a.row/N == b.row/N){
        if(a.col/N < b.col/N){
          return true;
        }
      }
      return false;
    }

    /**************************************************************************/
    static bool equal(const SpMatrixTuple<N, T>& a,
                      const SpMatrixTuple<N, T>& b){
      if(a.row/N == b.row/N && a.row/N == b.row/N){
        return true;
      }
      return false;
    }
  };

  /**************************************************************************/
  template<int N, class T>
  class SpRowProxy{
  public:
    /**************************************************************************/
    SpRowProxy(int r,
               const SpMatrix<N, T>* const cm);

    /**************************************************************************/
    SpRowProxy(int r,
               SpMatrix<N, T>* m);

    /**************************************************************************/
    T& operator[](int c);

    /**************************************************************************/
    T operator[](int c) const;

  protected:
    /**************************************************************************/
    template <int, class>
    friend class SpMatrix;

    /**************************************************************************/
    SpRowProxy(){
    }

    SpMatrix<N, T>* matrix;
    const SpMatrix<N, T>* cmatrix;
    int row;
  };

  /**************************************************************************/
  template<int N, class T>
  class SpMatrix{
  public:
    /**************************************************************************/
    friend SpMatrix<N, T>* reorderRCM<N, T>(const SpMatrix<N, T>*);

    /**************************************************************************/
    friend SpMatrix<N, T>* reorderKING<N, T>(const SpMatrix<N, T>*,
                                             int* map,
                                             int*rmap);

    /**************************************************************************/
    friend SpMatrix<N, T>* reorderMMD<N, T>(const SpMatrix<N, T>*);

    /**************************************************************************/
    friend SpMatrix<N, T>* reorderMy<N, T>(const SpMatrix<N, T>*);

    /**************************************************************************/
    friend class Cholesky<N, T>;

    /**************************************************************************/
    /*Constructors*/
    SpMatrix();

    /**************************************************************************/
    SpMatrix(int w,
             int h);

    /**************************************************************************/
    /*Copy constructor*/
    SpMatrix(const SpMatrix<N, T>& m);

    /**************************************************************************/
    /*Destructor*/
    virtual ~SpMatrix();

    /**************************************************************************/
    /*Assignment operator*/
    SpMatrix<N, T>& operator=(const SpMatrix<N, T>& m);

    /**************************************************************************/
    /*Enables direct manipulation of the data items. If the element
      does not exists then this function will create the element for
      you!*/
    SpRowProxy<N, T> operator[](int i){
      tslassert(i<origHeight);
      return SpRowProxy<N, T>(i, this);
    }

    /**************************************************************************/
    /*If an element does not exist, this function will not create the
      memory block but returns the zero element*/
    const SpRowProxy<N, T> operator[](int i) const{
      tslassert(i<origHeight);
      const SpMatrix<N, T>* cmatrix = (const SpMatrix<N, T>*)this;
      return SpRowProxy<N, T>(i, cmatrix);
    }

    /**************************************************************************/
    T getValue(int row,
               int col)const;

    /**************************************************************************/
    void setElements(List<SpMatrixTuple<N, T> >& elements);

    /**************************************************************************/
    void setElements(Tree<SpMatrixTuple<N, T> >& elements);

    /*Sparse matrix sparse matrix multiplication*/
#if 1
    /**************************************************************************/
	/*Not available yet*/
    SpMatrix<N, T>  operator* (const SpMatrix<N, T>& m)const;

    /**************************************************************************/
    SpMatrix<N, T>& operator*=(const SpMatrix<N, T>& m);

    /**************************************************************************/
    static void multiply(SpMatrix<N, T>* r,
                         const SpMatrix<N, T>* A,
                         const SpMatrix<N, T>* B);
#endif

    /**************************************************************************/
    /*Add similar sparse matrices together*/
    SpMatrix<N, T>  operator+ (const SpMatrix<N, T>& m) const;

    /**************************************************************************/
    SpMatrix<N, T>& operator+=(const SpMatrix<N, T>& m);

    /**************************************************************************/
    SpMatrix<N, T>  operator- (const SpMatrix<N, T>& m) const;

    /**************************************************************************/
    SpMatrix<N, T>& operator-=(const SpMatrix<N, T>& m);

    /**************************************************************************/
    /*Multiply/divide each non zero value*/
    SpMatrix<N, T>  operator* (T f) const;

    /**************************************************************************/
    SpMatrix<N, T>& operator*=(T f);

    /**************************************************************************/
    SpMatrix<N, T>  operator/ (T f) const;

    /**************************************************************************/
    SpMatrix<N, T>& operator/=(T f);

    /**************************************************************************/
    /*Helper function for full multiplication*/
    template<int M, class TT>
    friend TT multiplyRows(SpMatrix<M, TT>& A,
                           int rowA,
                           SpMatrix<M, TT>& B,
                           int rowB);

    /*Test binary relations. The result of an (in)equality test is a
      binary matrix.*/

    /**************************************************************************/
    /*Binary equality tests*/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator==(const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator!=(const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator==(TT n,
                                      const SpMatrix<M, TT>& m);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator!=(TT n,
                                      const SpMatrix<M, TT>& m);

    /**************************************************************************/
    /*Binary inequality tests*/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator< (const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator<=(const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator> (const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator>=(const SpMatrix<M, TT>& m,
                                      TT n);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator< (TT n,
                                      const SpMatrix<M, TT>& m);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator<=(TT n,
                                      const SpMatrix<M, TT>& m);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator> (TT n,
                                      const SpMatrix<M, TT>& m);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator>=(TT n, const SpMatrix<M, TT>& m);

    /**************************************************************************/
    /*Set one singe value and increment the number of stored
      elements*/
    void set(int row,
             int col,
             T value){
      (*this)[row][col] = value;
      n_elements++;
    }

    /**************************************************************************/
    T infiniteNorm();

    /**************************************************************************/
    void clear();

    /**************************************************************************/
    void clearStructure();

    /**************************************************************************/
    void analyse()const;

    /**************************************************************************/
    int getNElements()const;

    /**************************************************************************/
    int getNBlocks()const;

    /**************************************************************************/
    int getShortestRow()const;

    /**************************************************************************/
    int getLongestRow()const;

    /**************************************************************************/
    float getAverageRow()const;

    /**************************************************************************/
    float getAverageBlockFill()const;


    /**************************************************************************/
    SpMatrix<N, T> getTranspose()const;

    /**************************************************************************/
    void getTranspose(SpMatrix<N, T>& out)const;

    /*Candidate for removal, use LinSolve classes instead*/
#if 0
    Vector<T>* solveSystemCG(const Vector<T>* b);
    void       solveSystemCG(const Vector<T>* b, Vector<T>* x);
    Vector<T>* solveSystemBICGSTAB(const Vector<T>* b);
    void       solveSystemBICGSTAB(const Vector<T>* b, Vector<T>* x);
    Vector<T>* solveSystemBICGStabl(const Vector<T>* b, int l);
    void       solveSystemBICGStabl(const Vector<T>* b, int l, Vector<T>* x,
                                    int iter=1000);
    Vector<T>* solveSystemCGParallel(const Vector<T>* b, ThreadPool* pool);
    Vector<T>* solveSystemBICGSTABParallel(const Vector<T>* b,
                                           ThreadPool* pool);
#endif
#ifndef NO_CUDA
    Vector<T>* solveSystemCGCuda(const Vector<T>* b, ThreadPool* pool);
#endif

    /**************************************************************************/
    /*Multiplies matrix M = (D+U+L) with v as D^{-1}(U+L)v*/
    template<int M, class TT>
    friend void multiplyJacobi(Vector<TT>& r,
                               const SpMatrix<M, TT>& m,
                               const Vector<TT>& v);

    /**************************************************************************/
    template<int M, class TT>
    friend void multiplyGaussSeidel(Vector<TT>& r,
                                    const SpMatrix<M, TT>& m,
                                    const Vector<TT>& v,
                                    const Vector<TT>* b);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv(Vector<TT>& r,
                     const SpMatrix<M, TT>& m,
                     const Vector<TT>& v);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv_t(Vector<TT>& r,
                       const SpMatrix<M, TT>& m,
                       const Vector<TT>& v);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv_parallel(Vector<TT>& r,
                              const SpMatrix<M, TT>& m,
                              const Vector<TT>& v);

    /**************************************************************************/
    template<int M, class TT>
    friend void spmv_partial(Vector<TT>& r,
                             const SpMatrix<M, TT>& m,
                             const Vector<TT>& v,
                             const MatrixRange rg);

    /**************************************************************************/
    template<int M, class TT>
    friend Vector<TT> operator*(const SpMatrix<M, TT>&m,
                                const Vector<TT>&v);

    /**************************************************************************/
    template<int M, class TT>
    friend SpMatrix<M, TT> operator*(const SpMatrix<M, TT>&m,
                                     const SpMatrix<M, TT>&v);

    /**************************************************************************/
    template<int M, int MM, class TPA, class TPB>
    friend void convertMatrix(SpMatrix<M, TPA> & a,
                              const SpMatrix<MM, TPB>& b);

    /**************************************************************************/
    int getWidth()const {return origWidth;}

    /**************************************************************************/
    int getHeight()const {return origHeight;}

    /**************************************************************************/
    int getOrigWidth()const {return origWidth;}

    /**************************************************************************/
    int getOrigHeight()const {return origHeight;}

    /**************************************************************************/
    void printRow(int row)const;

    /**************************************************************************/
    template<int M, class TT>
    friend std::ostream& operator<<(std::ostream& stream,
                                    const SpMatrix<M, TT>& m);

    /**************************************************************************/
    void finalize(){
      if(!finalized){
        reorderBlocks();

        /*Allocate new temp buffer for spmv_t*/
        //if(tBlocks){
        //  delete[]tBlocks;
        //}

        //tBlocks = new SpMatrixBlock<N, T>[width/N];

        finalized = true;
      }
    }

    /**************************************************************************/
    bool isFinalized()const{
      return finalized;
    }

    /**************************************************************************/
    void unFinalize(){
      finalized = false;
    }

    //void finalizeParallel(ThreadPool* pool);

    /**************************************************************************/
    void computeBlockDistribution(MatrixRange* mRange,
                                  VectorRange* vRange,
                                  int* n_blocks,
                                  int n_segments) const;

#if 0
    /**************************************************************************/
    void setBandwidth(int bw){
      bandwidth = bw;
    }

    /**************************************************************************/
    int getBandwidth(){
      return bandwidth;
    }
#endif

    /**************************************************************************/
    SpMatrix<N, T> getSubMatrix(int width,
                                int height)const{
      SpMatrix<N, T> ret(width, height);

      for(int i=0;i<height/N;i++){
        for(int j=0;j<row_lengths[i];j++){
          int block_index = block_indices[i][j];
          int col_index   = col_indices[i][j] * N;
          int idx = 0;
          for(int k=0;k<N;k++){
            for(int l=0;l<N;l++){
              T bval = blocks[block_index].m[idx++];
              if(bval != (T)0.0){
                if(i*N + k < height && col_index + l < width){
                  ret[i*N + k][col_index + l] = (T)bval;
                }
              }
            }
          }
        }
      }
      return ret;
    }

  protected:
    /**************************************************************************/
    template<int M, class TT>
    friend class LinSolveGS;

    /**************************************************************************/
    template<int M, class TT>
    friend class LinSolveBGS;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolveGS;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolveBGS;

    /**************************************************************************/
    template<int M, class TT>
    friend class SpRowProxy;

    /**************************************************************************/
    template<class TT>
    friend class Vector;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelCGCudaTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelSPMVCudaTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelCGTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class ParallelSPMVTask;

    /**************************************************************************/
    template<int M, class TT>
    friend class CSpMatrix;

    /**************************************************************************/
    template<int M, class TT>
    friend bool save_matrix_matlab(const char* filename,
                                   const SpMatrix<M, TT>* const mat);

    /**************************************************************************/
    template<int M, class TT>
    friend void save_matrix_market_exchange(const char* filename,
                                            const SpMatrix<M, TT>* const mat);

    int width;
    int height;
    int origWidth;
    int origHeight;

  public:
    //std::map<int, int>* block_map; /*Block lookup map*/
    Tree<int>* block_map;
    int** col_indices;      /*Indices to blocks for each row*/
    int** row_indices;
    int** block_indices;    /*Indices to blocks for each row*/
    int** block_indices_col;
    int* comp_col_indices;  /*Compressed column indices multiplied with N*/
    int* comp_row_indices;  /*Compressed row indices multiplied with N*/
    int* row_lengths;       /*Stores for each row the length*/
    int* col_lengths;
    int* allocated_length_row;  /*Stores for each row the allocated length */
    int* allocated_length_col;  /*Stores for each col the allocated length */
    SpMatrixBlock<N, T>* blocks; /*Blocks*/
    int n_allocated_blocks; /*Stores the number of allocated blocks*/
    int n_blocks;           /*Number of stored blocks*/
    int n_elements;         /*Number of elements*/
  protected:
    /**************************************************************************/
    void reorderBlocks();    /*Reorder blocks such that all blocks are
                               stored in a cache coherent order*/

    /**************************************************************************/
    void grow_row(int row); /*Extends the storage of a row*/

    /**************************************************************************/
    void grow_col(int col); /*Extends the storage of a col*/

    /**************************************************************************/
    void grow_blocks();      /*Extends the storafe of for the blocks*/

    /**************************************************************************/
    SpRowProxy<N, T> proxy;  /*Proxy object for [][] operator*/

    /**************************************************************************/
    //ParallelSPMVTask<N, T>*task;/*Task descriptor*/

    /**************************************************************************/
    //ThreadPool* pool;        /*Pool associated with task descriptor*/

    /**************************************************************************/
    bool finalized;          /*True if the matrix is finalized. If the
                               matrix is altered after finalization,
                               this value becomes false*/

    /**************************************************************************/
    SpMatrixBlock<N, T>* tBlocks; /*Temporary buffer for storing the
                                    intermediate results for a
                                    transposed multiplication*/
#if 0
    /**************************************************************************/
    CSVExporter* exporter;   /*Exporter*/

    /**************************************************************************/
    int bandwidth;
#endif
  };

#ifdef CUDA
  void cuda_sum();
#endif

  /**************************************************************************/
  template<int N, class T>
  SpRowProxy<N, T>::SpRowProxy(int r,
                               const SpMatrix<N, T>* const cm){
    row = r;
    cmatrix = cm;
    matrix = 0;
  }

  /**************************************************************************/
  template<int N, class T>
  SpRowProxy<N, T>::SpRowProxy(int r,
                               SpMatrix<N, T>* m){
    row = r;
    matrix = m;
    cmatrix = 0;
  }

  /**************************************************************************/
  template<int N, class T>
  T& SpRowProxy<N, T>::operator[](int c){
    int col = c;
    bool block_exists = false;
    int block_index;
    int block_row_col;
    int block_row_index = row/N;
    int block_col_index = col/N;

    if(!(block_col_index < matrix->width  / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(!(block_row_index < matrix->height / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    tslassert(col < matrix->origWidth);

    tslassert(block_col_index < matrix->width  / N);
    tslassert(block_row_index < matrix->height / N);

    /*Find target block*/
    if(matrix->getWidth() == 1){
      if(matrix->row_lengths[block_row_index] == 0){
        block_exists = false;
      }else{
        block_exists = true;
        block_row_col = 0;
      }
    }else{
      int idx = matrix->block_map[block_row_index].findIndex(block_col_index);
      if(idx == Tree<int>::undefinedIndex){
        /*Block not found*/
        block_exists = false;
      }else{
        /*Block found*/
        block_exists = true;
        block_row_col = idx;
      }
    }

    if(!block_exists){
      /*Create a new block*/
      if(matrix->n_blocks == matrix->n_allocated_blocks){
        matrix->grow_blocks();
      }
#if 0
      if(!matrix->row_lengths[block_row_index] <=
         matrix->allocated_length_row[block_row_index]){
        printf("Block row index = %d, row_lengths = %d, allocated = %d\n",
               block_row_index, matrix->row_lengths[block_row_index],
               matrix->allocated_length_row[block_row_index]);
      }
#endif
      tslassert(matrix->row_lengths[block_row_index] <=
                matrix->allocated_length_row[block_row_index]);

      if(matrix->row_lengths[block_row_index] ==
         matrix->allocated_length_row[block_row_index]){
        matrix->grow_row(block_row_index);
      }

      if(matrix->col_lengths[block_col_index] ==
         matrix->allocated_length_col[block_col_index]){
        matrix->grow_col(block_col_index);
      }

      matrix->block_indices[block_row_index]
        [matrix->row_lengths[block_row_index]] = matrix->n_blocks;

      matrix->block_indices_col[block_col_index]
        [matrix->col_lengths[block_col_index]] = matrix->n_blocks;

      matrix->col_indices[block_row_index]
        [matrix->row_lengths[block_row_index]] = block_col_index;

      matrix->row_indices[block_col_index]
        [matrix->col_lengths[block_col_index]] = block_row_index;

      block_index = matrix->n_blocks;

      matrix->block_map[block_row_index].
        insert(block_col_index, matrix->row_lengths[block_row_index]);

      matrix->row_lengths[block_row_index]++;
      matrix->col_lengths[block_col_index]++;
      matrix->comp_col_indices[block_index] = block_col_index * N;
      matrix->comp_row_indices[block_index] = block_row_index * N;

      /*Reset block*/
      matrix->blocks[matrix->n_blocks].clear();
      matrix->n_blocks++;

      /*Structure of matrix has been altered, finalization is needed.*/
      matrix->finalized = false;
    }else{
      block_index = matrix->block_indices[block_row_index][block_row_col];

      /*Structure has not been altered, no finalization is needed yet.*/
    }

    int block_row = row%N;
    int block_col = col%N;

    return matrix->blocks[block_index].m[block_row*N + block_col];
  }

  /**************************************************************************/
  template<int N, class T>
  T SpRowProxy<N, T>::operator[](int c) const{
    return cmatrix->getValue(row, c);
  }

  /**************************************************************************/
  template<int N, class T>
  T SpMatrix<N, T>::getValue(int row,
                             int col)const{
    bool block_exists = false;
    int block_index;
    int block_row_col;
    int block_row_index = row/N;
    int block_col_index = col/N;

    tslassert(col < getWidth());

    tslassert(block_col_index < width  / N);
    tslassert(block_row_index < height / N);

    if(!(block_col_index < width / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(!(block_row_index < height / N)){
      message("Index = %d, %d", row, col);
      error("Index out of bounds");
    }

    if(getWidth() == 1){
      if(row_lengths[block_row_index] == 0){
        return SumIdentity(T());
      }

      block_exists = true;
      block_row_col = 0;
    }else{
      int idx = block_map[block_row_index].findIndex(block_col_index);

      if(idx == Tree<int>::undefinedIndex){
        /*Block not found*/
        block_exists = false;
      }else{
        /*Block found*/
        block_exists = true;
        block_row_col = idx;
      }
    }

    if(!block_exists){
      /*Block does not exist and since this is a const object we can
        not create new blocks*/
      return SumIdentity(T());
    }else{
      block_index = block_indices[block_row_index][block_row_col];
    }

    int block_row = row%N;
    int block_col = col%N;

    return blocks[block_index].m[block_row*N + block_col];
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::setElements(List<SpMatrixTuple<N, T> >& elements){
    clearStructure();
    elements.sort();

    int lastBlockRow = -1;
    int lastBlockCol = -1;

    typename List<SpMatrixTuple<N, T> >::Iterator it = elements.begin();

    while(it != elements.end()){
      int blockRow = it->row/N;
      int blockCol = it->col/N;

      if(blockRow == lastBlockRow && blockCol == lastBlockCol){
        /*Add to last blocks*/
      }else{
        /*Use new block*/
        if(n_blocks == n_allocated_blocks){
          grow_blocks();
        }

        if(row_lengths[blockRow] == allocated_length_row[blockRow]){
          grow_row(blockRow);
        }

        if(col_lengths[blockCol] == allocated_length_col[blockCol]){
          grow_col(blockCol);
        }

        lastBlockRow = blockRow;
        lastBlockCol = blockCol;

        row_lengths[blockRow]++;
        col_lengths[blockCol]++;

        n_blocks++;

        comp_col_indices[n_blocks-1] = blockCol * N;
        comp_row_indices[n_blocks-1] = blockRow * N;

        col_indices[blockRow][row_lengths[blockRow]-1] = blockCol;
        row_indices[blockCol][col_lengths[blockCol]-1] = blockRow;

        block_indices    [blockRow][row_lengths[blockRow]-1] = n_blocks-1;
        block_indices_col[blockCol][col_lengths[blockCol]-1] = n_blocks-1;


        block_map[blockRow].insert(blockCol, row_lengths[blockRow]-1);
      }

      int localRow = it->row % N;
      int localCol = it->col % N;

      blocks[n_blocks-1].m[localRow*N+localCol] += it->val;

      it++;
    }
    /*Input list is ordered*/
    finalized = true;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::setElements(Tree<SpMatrixTuple<N, T> >& elements){
    clearStructure();

    int lastBlockRow = -1;
    int lastBlockCol = -1;

    typename Tree<SpMatrixTuple<N, T> >::Iterator it = elements.begin();

    while(it != elements.end()){
      int blockRow = it->row/N;
      int blockCol = it->col/N;

      if(blockRow == lastBlockRow && blockCol == lastBlockCol){
        /*Add to last blocks*/
      }else{
        /*Use new block*/
        if(n_blocks == n_allocated_blocks){
          grow_blocks();
        }

        if(row_lengths[blockRow] == allocated_length_row[blockRow]){
          grow_row(blockRow);
        }

        if(col_lengths[blockCol] == allocated_length_col[blockCol]){
          grow_col(blockCol);
        }

        lastBlockRow = blockRow;
        lastBlockCol = blockCol;

        row_lengths[blockRow]++;
        col_lengths[blockCol]++;

        n_blocks++;

        comp_col_indices[n_blocks-1] = blockCol * N;
        comp_row_indices[n_blocks-1] = blockRow * N;

        col_indices[blockRow][row_lengths[blockRow]-1] = blockCol;
        row_indices[blockCol][col_lengths[blockCol]-1] = blockRow;

        block_indices    [blockRow][row_lengths[blockRow]-1] = n_blocks-1;
        block_indices_col[blockCol][col_lengths[blockCol]-1] = n_blocks-1;


        block_map[blockRow].insert(blockCol, row_lengths[blockRow]-1);
      }

      int localRow = it->row % N;
      int localCol = it->col % N;

      blocks[n_blocks-1].m[localRow*N+localCol] += it->val;

      it++;
    }

    finalized = true;
  }

  /**************************************************************************/
  template<int N, class T>
  void spmv(Vector<T>& r,
            const SpMatrix<N, T>& m,
            const Vector<T>& v);

  /**************************************************************************/
  template<int N, class T>
  void spmv_t(Vector<T>& r,
              const SpMatrix<N, T>& m,
              const Vector<T>& v);

  /**************************************************************************/
  template<int N, class T>
  void spmv_partial(Vector<T>& r,
                    const SpMatrix<N, T>& m,
                    const Vector<T>& v,
                    const MatrixRange rg);

  /**************************************************************************/
  template<int N, class T>
  void spmv_parallel(Vector<T>& r,
                     const SpMatrix<N, T>& m,
                     const Vector<T>& v);

  /**************************************************************************/
  /*Performs a multiplication x^{k+1}' = D^{-1}(L+U)x^{k} and is used
    in Jacobi's method as x^{k+1} = D^{-1}b - x^{k+1}'
  */
  template<int N, class T>
  void multiplyJacobi(Vector<T>& r,
                      const SpMatrix<N, T>& m,
                      const Vector<T>& v);

  /**************************************************************************/
  /*Performs one Gauss-Seidel step*/
  template<int N, class T>
  void multiplyGaussSeidel(Vector<T>& r,
                           const SpMatrix<N, T>& m,
                           const Vector<T>& v,
                           const Vector<T>* b = 0);

  /**************************************************************************/
  template<int N, int M, class TPA, class TPB>
  void convertMatrix(SpMatrix<N, TPA> & a,
                     const SpMatrix<M, TPB>& b){
    /*Create new empty matrix*/
    SpMatrix<N, TPA> na(b.getWidth(), b.getHeight());

    /*Assign empty matrix*/
    a = na;
    for(int i=0;i<b.height/M;i++){
      for(int j=0;j<b.row_lengths[i];j++){
        int block_index = b.block_indices[i][j];
        int col_index   = b.col_indices[i][j] * M;
        int idx = 0;
        for(int k=0;k<M;k++){
          for(int l=0;l<M;l++){
            TPB bval = b.blocks[block_index].m[idx++];
            if(bval != (TPB)0.0){
              a[i*M + k][col_index + l] = (TPA)bval;
            }
          }
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>::SpMatrix():width(0),
                             height(0),
                             origWidth(0),
                             origHeight(0),
                             block_map(0),
                             col_indices(0),
                             row_indices(0),
                             block_indices(0),
                             block_indices_col(0),
                             comp_col_indices(0),
                             comp_row_indices(0),
                             row_lengths(0),
                             col_lengths(0),
                             allocated_length_row(0),
                             allocated_length_col(0),
                             blocks(0),
                             n_allocated_blocks(0),
                             n_blocks(0),
                             n_elements(0),
                             finalized(false),
                             tBlocks(0){
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>::SpMatrix(int w,
                           int h):block_map(0),
                                  col_indices(0),
                                  row_indices(0),
                                  block_indices(0),
                                  block_indices_col(0),
                                  comp_col_indices(0),
                                  comp_row_indices(0),
                                  row_lengths(0),
                                  col_lengths(0),
                                  allocated_length_row(0),
                                  allocated_length_col(0),
                                  blocks(0),
                                  n_allocated_blocks(0),
                                  n_blocks(0),
                                  n_elements(0),
                                  finalized(false),
                                  tBlocks(0){
    origWidth = w;
    origHeight = h;

    if(h%16 == 0){
      height = h;
    }else{
      height = ((h/16)+1)*16;
    }

    if(w%16 == 0){
      width = w;
    }else{
      width = ((w/16)+1)*16;
    }

    /*Allocate data for rows*/
    if((height/N)%32 == 0){
      row_lengths          = new int[height/N];
      allocated_length_row = new int[height/N];
      col_indices          = new int*[height/N];
      block_indices        = new int*[height/N];
      block_map            = new Tree<int>[height/N];
    }else{
      int extendedHeight = (height/N)/32;
      extendedHeight++;
      extendedHeight *= 32;
      row_lengths          = new int[extendedHeight];
      allocated_length_row = new int[extendedHeight];
      col_indices          = new int*[extendedHeight];
      block_indices        = new int*[extendedHeight];
      block_map            = new Tree<int>[extendedHeight];
    }

    /*Allocate data for cols*/
    if((width/N)%32 == 0){
      col_lengths          = new int[width/N];
      allocated_length_col = new int[width/N];
      row_indices          = new int*[width/N];
      block_indices_col    = new int*[width/N];
    }else{
      int extendedWidth = (width/N)/32;
      extendedWidth++;
      extendedWidth *= 32;
      col_lengths          = new int[extendedWidth];
      allocated_length_col = new int[extendedWidth];
      row_indices          = new int*[extendedWidth];
      block_indices_col    = new int*[extendedWidth];
    }

    for(int i=0;i<height/N;i++){
      allocated_length_row[i] = 1;
      row_lengths[i]      = 0;
      col_indices[i]      = new int[1];
      block_indices[i]    = new int[1];
    }

    for(int i=0;i<width/N;i++){
      allocated_length_col[i] = 1;
      col_lengths[i]       = 0;
      row_indices[i]       = new int[1];
      block_indices_col[i] = new int[1];
    }

    /*Allocate blocks*/
    n_allocated_blocks = 16;
    n_blocks = 0;
    n_elements = 0;

    alignedMalloc((void**)&blocks, 16,
                  (uint)n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));

    proxy.matrix = this;

    comp_col_indices = new int[n_allocated_blocks];
    comp_row_indices = new int[n_allocated_blocks];
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>::SpMatrix(const SpMatrix<N, T>& m):width(0),
                                                    height(0),
                                                    origWidth(0),
                                                    origHeight(0),
                                                    block_map(0),
                                                    col_indices(0),
                                                    row_indices(0),
                                                    block_indices(0),
                                                    block_indices_col(0),
                                                    comp_col_indices(0),
                                                    comp_row_indices(0),
                                                    row_lengths(0),
                                                    col_lengths(0),
                                                    allocated_length_row(0),
                                                    allocated_length_col(0),
                                                    blocks(0),
                                                    n_allocated_blocks(0),
                                                    n_blocks(0),
                                                    n_elements(0){
    *this = m;
  }

  /**************************************************************************/
  /*Destructor*/
  template<int N, class T>
  SpMatrix<N, T>::~SpMatrix(){
    alignedFree(blocks);

    for(int i=0;i<height/N;i++){
      delete [] col_indices[i];
      delete [] block_indices[i];
    }

    for(int i=0;i<width/N;i++){
      delete [] row_indices[i];
      delete [] block_indices_col[i];
    }

    delete [] block_map;

    delete [] col_indices;
    delete [] row_indices;
    delete [] block_indices;
    delete [] block_indices_col;
    delete [] allocated_length_row;
    delete [] allocated_length_col;
    delete [] row_lengths;
    delete [] col_lengths;

    delete[] comp_col_indices;
    delete[] comp_row_indices;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::clear(){
    for(int i=0;i<n_blocks;i++){
      blocks[i].clear();
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::clearStructure(){
    if(n_blocks < (width + height)/N){
      /*Instead of looping over large arrays, just loop over the
      blocks and reset the individual components*/

      for(int blockIndex = 0;blockIndex<n_blocks;blockIndex++){
        int row = comp_row_indices[blockIndex]/N;
        int col = comp_col_indices[blockIndex]/N;

        tslassert(row != -1);
        tslassert(col != -1);

        row_lengths[row] = 0;
        col_lengths[col] = 0;

        comp_row_indices[blockIndex] = -1;
        comp_col_indices[blockIndex] = -1;

        block_map[row].clear();
      }
    }else{
      /*Reset pointers*/
      for(int i=0;i<height/N;i++){
        row_lengths[i] = 0;
        block_map[i].clear();
      }

      for(int i=0;i<width/N;i++){
        col_lengths[i] = 0;
      }
    }

    n_blocks = 0;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator=(const SpMatrix<N, T>& m){
    if(this == &m){
      /*Self assignment*/
      return *this;
    }

    if(height == m.height &&
       width  == m.width &&
       origWidth  == m.origWidth &&
       origHeight == m.origHeight){
      /*Matrices have same shape*/

      clearStructure();

      if(n_blocks < (m.width + m.height) * 4){
        if(m.n_blocks >= n_allocated_blocks){
          grow_blocks();
        }

        for(int blockIndex = 0;blockIndex < m.n_blocks;blockIndex++){
          int row = m.comp_row_indices[blockIndex];
          int col = m.comp_col_indices[blockIndex];

          tslassert(row != -1);
          tslassert(col != -1);

          comp_row_indices[blockIndex] = row;
          comp_col_indices[blockIndex] = col;

          for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
              T value = m.blocks[blockIndex].m[i*N+j];
              if(Abs(value) != (T)0.0){
                (*this)[row + i][col + j] = m.blocks[blockIndex].m[i * N + j];
              }
            }
          }
        }

        proxy.matrix       = this;
        proxy.cmatrix      = this;
        finalized = m.finalized;

        return *this;
      }

      //Rows
      for(int i=0;i<height/N;i++){
        if(allocated_length_row[i] >= m.row_lengths[i]){
          /*Does fit*/
          row_lengths[i] = m.row_lengths[i];

          memcpy(col_indices[i], m.col_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
          memcpy(block_indices[i], m.block_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
        }else{
          /*Does not fit*/
          row_lengths[i] = m.row_lengths[i];
          delete[] col_indices[i];
          delete[] block_indices[i];

          allocated_length_row[i] = m.allocated_length_row[i];
          col_indices[i] = new int[allocated_length_row[i]];
          block_indices[i] = new int[allocated_length_row[i]];

          memcpy(col_indices[i], m.col_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
          memcpy(block_indices[i], m.block_indices[i],
                 sizeof(int)*(uint)row_lengths[i]);
        }
        block_map[i] = m.block_map[i];
      }

      //Cols
      for(int i=0;i<width/N;i++){
        if(allocated_length_col[i] >= m.col_lengths[i]){
          /*Does fit*/
          col_lengths[i] = m.col_lengths[i];

          memcpy(row_indices[i], m.row_indices[i],
                 sizeof(int)*(uint)col_lengths[i]);
          memcpy(block_indices_col[i], m.block_indices_col[i],
                 sizeof(int)*(uint)col_lengths[i]);
        }else{
          /*Does not fit*/
          col_lengths[i] = m.col_lengths[i];
          delete[] row_indices[i];
          delete[] block_indices_col[i];

          allocated_length_col[i] = m.allocated_length_col[i];
          row_indices[i] = new int[allocated_length_col[i]];
          block_indices_col[i] = new int[allocated_length_col[i]];

          memcpy(row_indices[i], m.row_indices[i],
                 sizeof(int)*(uint)col_lengths[i]);
          memcpy(block_indices_col[i], m.block_indices_col[i],
                 sizeof(int)*(uint)col_lengths[i]);
        }
        block_map[i] = m.block_map[i];
      }

      if(n_allocated_blocks >= m.n_blocks){
        /*Does fit*/
        n_blocks = m.n_blocks;

        /*Copy blocks*/
        for(int k=0;k<n_blocks;k++){
          blocks[k] = m.blocks[k];
        }

        memcpy(comp_col_indices, m.comp_col_indices,
               sizeof(int)*(long unsigned int)n_blocks);

        memcpy(comp_row_indices, m.comp_row_indices,
               sizeof(int)*(long unsigned int)n_blocks);
      }else{
        /*Does not fit*/
        alignedFree(blocks);
        delete[] comp_col_indices;
        delete[] comp_row_indices;

        n_allocated_blocks = m.n_allocated_blocks;
        n_blocks = m.n_blocks;

        /*Allocate blocks*/
        alignedMalloc((void**)&blocks, 16,
                      (uint)n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));
        comp_col_indices = new int[n_allocated_blocks];
        comp_row_indices = new int[n_allocated_blocks];

        /*Copy blocks*/
        for(int k=0;k<n_blocks;k++){
          blocks[k] = m.blocks[k];
        }

        memcpy(comp_col_indices, m.comp_col_indices,
               sizeof(int)*(long unsigned int)n_blocks);

        memcpy(comp_row_indices, m.comp_row_indices,
               sizeof(int)*(long unsigned int)n_blocks);
      }

      proxy.matrix       = this;
      proxy.cmatrix      = this;

      return *this;
    }

    /*Not equal, reshape*/

    for(int i=0;i<height/N;i++){
      delete [] col_indices[i];
      delete [] block_indices[i];
    }

    for(int i=0;i<width/N;i++){
      delete [] row_indices[i];
      delete [] block_indices_col[i];
    }

    delete [] block_map;
    delete [] col_indices;
    delete [] row_indices;
    delete [] block_indices;
    delete [] block_indices_col;
    delete [] allocated_length_row;
    delete [] allocated_length_col;
    delete [] row_lengths;
    delete [] col_lengths;

    height             = m.height;
    width              = m.width;
    origHeight         = m.origHeight;
    origWidth          = m.origWidth;
    n_blocks           = m.n_blocks;
    n_elements         = m.n_elements;

    proxy.matrix       = this;
    proxy.cmatrix      = this;

    /*Allocate new arrays row*/
    if((height/N)%32 == 0){
      row_lengths          = new int[height/N];
      allocated_length_row = new int[height/N];
      col_indices          = new int*[height/N];
      block_indices        = new int*[height/N];
      block_map            = new Tree<int>[height/N];
    }else{
      int extendedHeight = (height/N)/32;
      extendedHeight++;
      extendedHeight*=32;
      row_lengths          = new int[extendedHeight];
      allocated_length_row = new int[extendedHeight];
      col_indices          = new int*[extendedHeight];
      block_indices        = new int*[extendedHeight];
      block_map            = new Tree<int>[extendedHeight];
    }

    /*Allocate new arrays col*/
    if((width/N)%32 == 0){
      col_lengths          = new int[width/N];
      allocated_length_col = new int[width/N];
      row_indices          = new int*[width/N];
      block_indices_col    = new int*[width/N];
      //block_map        = new std::map<int, int>[height/N];
      //block_map        = new Tree<int>[height/N];
    }else{
      int extendedWidth = (width/N)/32;
      extendedWidth++;
      extendedWidth*=32;
      col_lengths          = new int[extendedWidth];
      allocated_length_col = new int[extendedWidth];
      row_indices          = new int*[extendedWidth];
      block_indices_col    = new int*[extendedWidth];
      //block_map        = new std::map<int, int>[extendedHeight];
      //block_map        = new Tree<int>[extendedHeight];
    }

    /*Copy data*/
    memcpy(row_lengths, m.row_lengths, sizeof(int)*(uint)height/N);
    memcpy(col_lengths, m.col_lengths, sizeof(int)*(uint)width/N);

    memcpy(allocated_length_row, m.allocated_length_row,
           sizeof(int)*(uint)height/N);
    memcpy(allocated_length_col, m.allocated_length_col,
           sizeof(int)*(uint)width/N);

    for(int i=0;i<height/N;i++){
      col_indices[i]   = new int[allocated_length_row[i]];
      block_indices[i] = new int[allocated_length_row[i]];

      memcpy(col_indices[i], m.col_indices[i],
             sizeof(int)*(uint)row_lengths[i]);
      memcpy(block_indices[i], m.block_indices[i],
             sizeof(int)*(uint)row_lengths[i]);

      block_map[i] = m.block_map[i];
    }

    for(int i=0;i<width/N;i++){
      row_indices[i]       = new int[allocated_length_col[i]];
      block_indices_col[i] = new int[allocated_length_col[i]];

      memcpy(row_indices[i], m.row_indices[i],
             sizeof(int)*(uint)col_lengths[i]);
      memcpy(block_indices_col[i], m.block_indices_col[i],
             sizeof(int)*(uint)col_lengths[i]);
    }

    if(m.n_blocks >= n_allocated_blocks){
      /*Free blocks*/
      alignedFree(blocks);
      delete [] comp_col_indices;
      delete [] comp_row_indices;

      /*Allocate blocks*/
      alignedMalloc((void**)&blocks, 16,
                    (uint)m.n_allocated_blocks * sizeof(SpMatrixBlock<N, T>));

      comp_col_indices = new int[m.n_allocated_blocks];
      comp_row_indices = new int[m.n_allocated_blocks];

      n_allocated_blocks = m.n_allocated_blocks;
    }

    /*Copy blocks*/
    for(int k=0;k<n_blocks;k++){
      blocks[k] = m.blocks[k];
    }

    memcpy(comp_col_indices, m.comp_col_indices,
           sizeof(int)*(uint)n_blocks);

    memcpy(comp_row_indices, m.comp_row_indices,
           sizeof(int)*(uint)n_blocks);

    finalized = m.finalized;

    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator+(const SpMatrix<N, T>& m) const{
    SpMatrix<N, T> m2 = *this;

    m2 += m;
    return m2;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator+=(const SpMatrix<N, T>& m){
    tslassert(origWidth == m.origWidth);
    tslassert(origHeight == m.origHeight);

    for(int i=0;i<m.height/N;i++){
      for(int j=0;j<m.row_lengths[i];j++){
        int block_index = m.block_indices[i][j];
        int block_col   = m.col_indices[i][j] * N;

        for(int k=0;k<N*N;k++){
          int l_row = k/N;
          int l_col = k%N;
          int row = i*N + l_row;
          int col = block_col + l_col;

          T val = m.blocks[block_index].m[k];

          if(val != (T)0.0){
            (*this)[row][col] += val;
          }
        }
      }
    }
    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator-(const SpMatrix<N, T>& m) const{
    tslassert(origWidth == m.origWidth);
    tslassert(origHeight == m.origHeight);
    SpMatrix<N, T> m2 = *this;

    m2 -= m;
    return m2;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator-=(const SpMatrix<N, T>& m){
    tslassert(origWidth == m.origWidth);
    tslassert(origHeight == m.origHeight);

    for(int i=0;i<m.height/N;i++){
      for(int j=0;j<m.row_lengths[i];j++){
        int block_index = m.block_indices[i][j];
        int block_col   = m.col_indices[i][j] * N;

        for(int k=0;k<N*N;k++){
          int l_row = k/N;
          int l_col = k%N;
          int row = i*N + l_row;
          int col = block_col + l_col;

          T val = m.blocks[block_index].m[k];

          if(val != (T)0.0){
            (*this)[row][col] -= val;
          }
        }
      }
    }
    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator*(const SpMatrix<N, T>& B)const{
    SpMatrix<N, T> r;
    multiply(&r, this, &B);
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::multiply(SpMatrix<N, T>* r,
                                const SpMatrix<N, T>* A,
                                const SpMatrix<N, T>* B){
    int rWidth = B->getWidth();
    int rHeight = A->getHeight();

    if(r->getWidth() == rWidth &&
       r->getHeight() == rHeight){
      /*Same shape*/
      r->clearStructure();
    }else{
      /*Different shape, create new one*/
      *r = SpMatrix<N, T>(rWidth, rHeight);
    }

    if(!A->finalized || !B->finalized){
      /*This function requires finalized (ordered) matrices*/
      error("matrices not finalized");
    }

    if(A->getWidth() != B->getHeight()){
      error("matrix dimensions do not agree");
    }

    if(true){
      Tree<int> touchedBlocks;
      std::map<int, SpMatrixBlock<N, T> > blockMap;

      for(int blockIndex = 0;blockIndex < A->n_blocks;blockIndex++){
        int row = A->comp_row_indices[blockIndex]/N;
        int col = A->comp_col_indices[blockIndex]/N;

        /*Loop over row in B*/
        for(int i=0;i<B->row_lengths[col];i++){
          int colB = B->col_indices[col][i];
          int bBlockIndex = B->block_indices[col][i];

          int index = touchedBlocks.findIndex(colB);

          SpMatrixBlock<N, T>* tmp = 0;
          if(index == Tree<int>::undefinedIndex){
            touchedBlocks.insert(colB, colB);
            tmp = &(blockMap[colB]);
            tmp->clear();
          }else{
            tmp = &(blockMap[colB]);
          }

          tmp->matrixMulAdd(&(A->blocks[blockIndex]),
                            &(B->blocks[bBlockIndex]));

        }

        if(col == A->col_indices[row][A->row_lengths[row]-1]){
          /*End of row, write row in result matrix*/
          Tree<int>::Iterator it = touchedBlocks.begin();
          while(it != touchedBlocks.end()){

            int rCol = *it++;
            SpMatrixBlock<N, T>* tmp = &(blockMap[rCol]);
            /*Store block*/
            for(int k=0;k<N;k++){
              for(int l=0;l<N;l++){
                if(row*N+k < rHeight && rCol*N+l < rWidth){
                  (*r)[row*N+k][rCol*N+l] = tmp->m[k*N+l];//result.m[k*N+l];
                }
              }
            }
            //tmp->clear();

          }
          touchedBlocks.clear();
        }
      }
    }

    /*Blocks are already added in order*/
    r->finalized = true;
    //r->finalize();
    //return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator*(T f) const{
    SpMatrix<N, T> m2 = *this;

    for(int i=0;i<m2.n_blocks;i++){
      m2.blocks[i].mul(f);
    }
    return m2;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator*=(T f){
    for(int i=0;i<n_blocks;i++){
      blocks[i].mul(f);
    }
    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::operator/(T f) const{
    SpMatrix<N, T> m2 = *this;

    for(int i=0;i<m2.n_blocks;i++){
      m2.blocks[i].div(f);
    }
    return m2;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>& SpMatrix<N, T>::operator/=(T f){
    for(int i=0;i<n_blocks;i++){
      blocks[i].div(f);
    }
    return *this;
  }

  /**************************************************************************/
  template<int N, class T>
  T multiplyRows(SpMatrix<N, T>& A,
                 int rowA,
                 SpMatrix<N, T>& B,
                 int rowB){
    T sum = (T)0.0;

    A.finalize();
    B.finalize();

    if(A.getWidth() != B.getWidth()){
      error("dimensions not equal");
    }

    int blockRowA = rowA/N;
    int blockRowB = rowB/N;
    int subRowA   = rowA%N;
    int subRowB   = rowB%N;

    int rowLengthA = A.row_lengths[blockRowA];
    int rowLengthB = B.row_lengths[blockRowB];

    int blockPtrA = 0;
    int blockPtrB = 0;

    while(true){
      if(blockPtrA >= rowLengthA ||
         blockPtrB >= rowLengthB){
        /*One row has reached its end*/
        break;
      }

      int colA = A.col_indices[blockRowA][blockPtrA];
      int colB = B.col_indices[blockRowB][blockPtrB];

      if(colA < colB){
        blockPtrA++;
      }else if(colA == colB){
        /*Compute*/
        int blockIdxA = A.block_indices[blockRowA][blockPtrA];
        int blockIdxB = B.block_indices[blockRowB][blockPtrB];

        for(int i=0;i<N;i++){
          sum += (A.blocks[blockIdxA].m[subRowA*N+i] *
                  B.blocks[blockIdxB].m[subRowB*N+i]);

        }

        blockPtrA++;
        blockPtrB++;
      }else{
        blockPtrB++;
      }
    }
    return sum;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator==(const SpMatrix<N, T>& m,
                            T f){
    SpMatrix<N, T> r(m);

    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpeq(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator!=(const SpMatrix<N, T>& m,
                            T f){
    SpMatrix<N, T> r(m);
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpneq(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator==(T f,
                            const SpMatrix<N, T>& m){
    return m==f;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator!=(T f,
                            const SpMatrix<N, T>& m){
    return m!=f;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator<(const SpMatrix<N, T>& m,
                           T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmplt(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator<=(const SpMatrix<N, T>& m,
                            T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmple(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator>(const SpMatrix<N, T>& m,
                           T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpgt(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator>=(const SpMatrix<N, T>& m,
                            T f){
    SpMatrix<N, T> r(m);
    /*Just inspect all the blocks*/
    for(int i=0;i<r.n_blocks;i++){
      r.blocks[i].cmpge(f);
    }
    return r;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator<(T f,
                           const SpMatrix<N, T>& m){
    return m>f;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator<=(T f,
                            const SpMatrix<N, T>& m){
    return m>=f;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator>(T f,
                           const SpMatrix<N, T>& m){
    return m<f;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> operator>=(T f,
                            const SpMatrix<N, T>& m){
    return m<=f;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::printRow(int row)const{
    T mul = 200.0;
    T sum = 0;
    T sum2 = 0;
    int block_row = row/N;
    int local_row = row%N;

    for(int i=0;i<row_lengths[block_row];i++){
      int block_index = block_indices[block_row][i];
      int block_col   = col_indices[block_row][i] * N;

      for(int j=0;j<N;j++){
        message("(%d, %d) = %20.20e, %20.20e",
                row, block_col + j,
                blocks[block_index].m[local_row*N + j],
                blocks[block_index].m[local_row*N + j]*mul);

        sum  += blocks[block_index].m[local_row*N + j] * mul;
        sum2 += blocks[block_index].m[local_row*N + j];
      }
    }

    message("sum = %10.10e", sum);
    message("sum2 = %10.10e, %10.20e", sum2, sum2*200);
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::analyse()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;

    for(int i=0;i<height/N;i++){
      if(row_lengths[i] != 0){
        minrow = Min(minrow, row_lengths[i]);
        maxrow = Max(maxrow, row_lengths[i]);
        totalrow += row_lengths[i];
      }
    }

    message("%d stored elements", n_elements);
    message("%d minimal number of blocks", n_elements/(N*N));
    message("%d allocated blocks", n_blocks);
    message("Average block fill = %f", (float)n_elements/(float)n_blocks);

    message("Shortest row = %d", minrow);
    message("Longest  row = %d", maxrow);
    message("Average  row = %f", (float)totalrow/(float)(height/N));
  }

  /**************************************************************************/
  template<int N, class T>
  int SpMatrix<N, T>::getNElements()const{
    return n_elements;
  }

  /**************************************************************************/
  template<int N, class T>
  int SpMatrix<N, T>::getNBlocks()const{
    return n_blocks;
  }

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T> SpMatrix<N, T>::getTranspose()const{
    SpMatrix<N, T> ret(origHeight, origWidth);

    getTranspose(ret);
    ret.finalize();
    return ret;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::getTranspose(SpMatrix<N, T>& out)const{
    out.clearStructure();

    for(int block_index=0;block_index<n_blocks;block_index++){
      int origRow = comp_row_indices[block_index];
      int origCol = comp_col_indices[block_index];

      for(int k=0;k<N;k++){
        for(int l=0;l<N;l++){
          T val = blocks[block_index].m[k*N+l];
          if(Abs(val) != 0.0){
            out[origCol + l][origRow + k] = val;
          }
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  T SpMatrix<N, T>::infiniteNorm(){
    int n_rows = height/N;

    int idx = 0;

    T norm = 0.0;

    for(int i=0;i<n_rows;i++){
      T res[(uint)(N)];
      for(int j=0;j<N;j++){
        res[j] = 0.0;
      }

      int row_length = row_lengths[i];
      for(int j=0;j<row_length;j++, idx++){
        for(int k=0;k<N;k++){
          for(int l=0;l<N;l++){
            res[k] += Abs(blocks[idx].m[k*N+l]);
          }
        }
      }

      for(int j=0;j<N;j++){
        norm = Max(norm, res[j]);
      }
    }
    return norm;
  }

  /**************************************************************************/
  template<int N, class T>
  int SpMatrix<N, T>::getShortestRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = Min(minrow, row_lengths[i]);
      maxrow = Max(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return minrow;
  }

  /**************************************************************************/
  template<int N, class T>
  int SpMatrix<N, T>::getLongestRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = Min(minrow, row_lengths[i]);
      maxrow = Max(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return maxrow;
  }

  /**************************************************************************/
  template<int N, class T>
  float SpMatrix<N, T>::getAverageRow()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = Min(minrow, row_lengths[i]);
      maxrow = Max(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return (float)totalrow/(float)(height/N);
  }

  /**************************************************************************/
  template<int N, class T>
  float SpMatrix<N, T>::getAverageBlockFill()const{
    int minrow = 100000;
    int maxrow = 0;
    int totalrow = 0;
    for(int i=0;i<height/N;i++){
      minrow = Min(minrow, row_lengths[i]);
      maxrow = Max(maxrow, row_lengths[i]);
      totalrow += row_lengths[i];
    }
    return (float)n_elements/(float)n_blocks;
  }

  /**************************************************************************/
  /*****Protected functions*******************/
  template<int N, class T>
  void SpMatrix<N, T>::grow_row(int row){
    int* tmp_blk_index = block_indices[row];
    int* tmp_col_index = col_indices[row];

    col_indices[row]   = new int[allocated_length_row[row]*2];
    block_indices[row] = new int[allocated_length_row[row]*2];

    memcpy(col_indices[row], tmp_col_index,
           sizeof(int)*(uint)allocated_length_row[row]);

    memcpy(block_indices[row], tmp_blk_index,
           sizeof(int)*(uint)allocated_length_row[row]);

    allocated_length_row[row] *=2;

    delete [] tmp_blk_index;
    delete [] tmp_col_index;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::grow_col(int col){
    int* tmp_blk_index = block_indices_col[col];
    int* tmp_row_index = row_indices[col];

    row_indices[col]   = new int[allocated_length_col[col]*2];
    block_indices_col[col] = new int[allocated_length_col[col]*2];

    memcpy(row_indices[col], tmp_row_index,
           sizeof(int)*(uint)allocated_length_col[col]);

    memcpy(block_indices_col[col], tmp_blk_index,
           sizeof(int)*(uint)allocated_length_col[col]);

    allocated_length_col[col] *=2;

    delete [] tmp_blk_index;
    delete [] tmp_row_index;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::grow_blocks(){
    SpMatrixBlock<N, T>* tmp = blocks;
    int* tmp_comp_col = comp_col_indices;
    int* tmp_comp_row = comp_row_indices;

    alignedMalloc((void**)&blocks, 16,
                  (uint)n_allocated_blocks*2*sizeof(SpMatrixBlock<N, T>));

    comp_col_indices = new int[n_allocated_blocks * 2];
    comp_row_indices = new int[n_allocated_blocks * 2];

    for(int k=0;k<n_allocated_blocks;k++){
      blocks[k] = tmp[k];
    }

    memcpy(comp_col_indices, tmp_comp_col,
           sizeof(int)*(uint)n_allocated_blocks);
    memcpy(comp_row_indices, tmp_comp_row,
           sizeof(int)*(uint)n_allocated_blocks);

    n_allocated_blocks *= 2;

    alignedFree(tmp);

    delete[] tmp_comp_col;
    delete[] tmp_comp_row;
  }

  /**************************************************************************/
  template<int N, class T>
  std::ostream& operator<<(std::ostream& stream,
                           const SpMatrix<N, T>& m){
    std::ios_base::fmtflags origflags = stream.flags();
    stream.setf(std::ios_base::scientific);

    for(int row=0;row<m.origHeight;row++){
      for(int col=0;col<m.origWidth;col++){
        int block_index;
        int block_row;
        int block_col;
        bool found = false;

        int idx = m.block_map[row/N].findIndex(col/N);
        if(idx != Tree<int>::undefinedIndex){
          found = true;
          block_index = m.block_indices[row/N][idx];
          block_row = row%N;
          block_col = col%N;
        }

        if(found){
          stream << m.blocks[block_index].m[block_row*N+block_col] << "\t";
        }else{
          stream << T() << "\t";
        }
      }
      stream<<std::endl;
    }
    stream.flags(origflags);
    return stream;
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::reorderBlocks(){
    /*Order the blocks such that they are ordered in memory in the way
      the are accessed. */

    /*First build a reverse index such that we know where each block
      is located. This information is needed to swap the blocks
      properly*/

    reverse_index_t* reverseIndices = new reverse_index_t[n_blocks];

    int blockIndex = 0;

    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++){
        int block_index = block_indices[i][j];

        reverseIndices[block_index].blockRow = i;
        reverseIndices[block_index].blockCol = j;
      }
    }

    blockIndex = 0;

    /*Swap blocks*/
    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++){
        int tmpBlockIndex = block_indices[i][j];

        if(blockIndex != tmpBlockIndex){
          /*Swap blocks and adapt indices*/

          reverse_index_t rev = reverseIndices[blockIndex];

          SpMatrixBlock<N, T> tmpBlock1 = blocks[tmpBlockIndex];
          SpMatrixBlock<N, T> tmpBlock2 = blocks[blockIndex];
          tslassert(rev.blockRow != -1);
          tslassert(rev.blockCol != -1);

          block_indices[i][j]    = blockIndex;
          blocks[blockIndex]     = tmpBlock1;
          blocks[tmpBlockIndex]  = tmpBlock2;
          block_indices[rev.blockRow][rev.blockCol] = tmpBlockIndex;

          /*Update reverse pointers*/
          reverseIndices[blockIndex].blockRow = -1;
          reverseIndices[blockIndex].blockCol = -1;
          reverseIndices[tmpBlockIndex].blockRow = rev.blockRow;
          reverseIndices[tmpBlockIndex].blockCol = rev.blockCol;
        }
        blockIndex++;
      }
    }

    /*Each block is now moved to its row, but the row itself is
      unsorted*/

    for(int i=0;i<height/N;i++){
      //message("sort row %d", i);

      Tree<int>::Iterator it = block_map[i].begin();
      Tree<int> colMap;

      for(int j=0;j<row_lengths[i];j++){
        int col = col_indices[i][j];
        int block_index = block_indices[i][j];

        colMap.insert(col, block_index);
      }

      SpMatrixBlock<N, T>* tmpBlocks =
        new SpMatrixBlock<N, T>[row_lengths[i]];

      it = colMap.begin();

      for(int j=0;j<row_lengths[i];j++){
        int newColumn = *it++;
        int index = colMap.findIndex(newColumn);

        tmpBlocks[j] = blocks[index];

        col_indices[i][j] = newColumn;
      }

      block_map[i].clear();

      for(int j=0;j<row_lengths[i];j++){
        blocks[block_indices[i][j]] = tmpBlocks[j];

        block_map[i].insert(col_indices[i][j], j);
      }

      delete[] tmpBlocks;
    }

    delete [] reverseIndices;

    int idx = 0;
    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++, idx++){
        comp_col_indices[idx] = col_indices[i][j] * N;
        comp_row_indices[idx] = i * N;
      }
    }

    for(int i=0;i<width/N;i++){
      col_lengths[i] = 0;
    }

    idx = 0;
    for(int i=0;i<height/N;i++){
      for(int j=0;j<row_lengths[i];j++, idx++){
        int row = comp_row_indices[idx]/N;
        int col = comp_col_indices[idx]/N;
        row_indices[col][col_lengths[col]] = row;
        block_indices_col[col][col_lengths[col]] = idx;
        col_lengths[col]++;
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void spmv(Vector<T>& r,
            const SpMatrix<N, T>& m,
            const Vector<T>& v){
    MatrixRange range;

    if(v.getSize() != m.getWidth()){
      error("Dimensions of vector (%d) does not match width of matrix (%d)",
            v.getSize(), m.getWidth());
    }

    range.startRow = 0;
    range.endRow   = m.height/N;
    range.range    = m.height/N;

    spmv_partial(r, m, v, range);
  }

  /**************************************************************************/
  /*Transposed multiplication*/
  template<int N, class T>
  void spmv_t(Vector<T>& r,
              const SpMatrix<N, T>& m,
              const Vector<T>& v){
    tslassert(m.getHeight() == v.getSize());
    tslassert(r.getSize()  == m.getWidth());
    tslassert(m.finalized  == true);

    warning("spmv_t does not take sign into account");

    error("tBlocks are about to be removed");

    /*Clear tmp_data*/
    for(int i=0;i<m.width/N;i++){
      m.tBlocks[i].clear();
    }

    int idx = 0;
    /*Number of block rows*/
    int n_rows = m.height/N;

    const T* data = v.data;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];
      for(int j=0;j<row_length;j++, idx++){
        int blockIndex = m.comp_col_indices[idx]/N;
        m.tBlocks[blockIndex].vectorMulAddTranspose(&(m.blocks[idx]),
                                                    &(data[i*N]));
      }
    }

    /*Column reduce*/
    for(int i=0;i<m.width/N;i++){
      m.tBlocks[i].columnSumReduce();
    }

    for(int i=0;i<m.width/N;i++){
      for(int j=0;j<N;j++){
        r.data[i*N+j] = m.tBlocks[i].m[N*(N-1) + j];
      }
    }
  }

  /**************************************************************************/
  int computeSteps(int size);
#if 0
  {
    /*Find a value, smaller than size, which is a power of 2*/
    int steps = size/2;
    int pwr = 1;
    while(steps >1){
      steps/=2;
      pwr++;
    }
    return 1<<pwr;
  }
#endif

  /*Experimental mode for testing numerical
    instabilities. SPLIT_MATRIX -> performs 2 multiplications per
    matrix block. Positive results are stored in positive blocks,
    negative in negative blocks. Finally the result of the positive
    and negative block are added giving the final result. This should
    increase numerical stability, i.e., reduce loss of significance in
    the intermediate computations.*/
#define SPLIT_MATRIX
#undef SPLIT_MATRIX

  /**************************************************************************/
  /*Performs a partial sparse matrix vector
    multiplication given by MatrixRange */
  template<int N, class T>
  void spmv_partial(Vector<T>& r,
                    const SpMatrix<N, T>& m,
                    const Vector<T>& v,
                    const MatrixRange mr){
    tslassert(m.getWidth() == v.getSize());
    tslassert(r.getSize() == m.getHeight());
    tslassert(m.finalized == true);

    if(mr.range == 0)
      return;
    if(mr.startRow == mr.endRow){
      return;
    }

    const T* data = v.data;


#ifdef SPLIT_MATRIX
    SpMatrixBlock<N, T> tmpBlockPos;
    SpMatrixBlock<N, T> tmpBlockNeg;
#else
    SpMatrixBlock<N, T> tmpBlock;
#endif

    for(int i=mr.startRow;i<mr.endRow;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        bool processed = false;

        int idx = m.block_indices[i][0];
#ifdef SPLIT_MATRIX
        tmpBlockPos.clear();
        tmpBlockNeg.clear();
#else
        tmpBlock.clear();
#endif

        for(int j=0;j<row_length;j++,idx++){
          int columnIndex = m.comp_col_indices[idx];
          bool skip = true;
          for(int k=0;k<N;k++){
            if(data[columnIndex+k] != T()){
              skip = false;
              break;
            }
          }

          if(!skip){
#ifdef SPLIT_MATRIX
            tmpBlockPos.vectorMulAddP(&(m.blocks[idx]),
                                      &(data[columnIndex]));
            tmpBlockNeg.vectorMulAddN(&(m.blocks[idx]),
                                      &(data[columnIndex]));
#else
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[columnIndex]));
#endif
            processed = true;
          }
        }
#ifdef SPLIT_MATRIX
        if(processed){
          tmpBlockPos.rowSumReduce();
          tmpBlockNeg.rowSumReduce();
          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlockPos.m[j*N] + tmpBlockNeg.m[j*N];
          }
          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlockPos.m[j*N] + tmpBlockNeg.m[j*N];
          }
        }
#else
        if(processed){
          tmpBlock.rowSumReduce();

          for(int j=0;j<N;j++){
            r.data[i*N + j] = tmpBlock.m[j*N];
          }
        }
#endif
      }
    }
  }

#if 0
  /**************************************************************************/
  template<int N, class T>
  void spmv_parallel(Vector<T>& r,
                     const SpMatrix<N, T>& m,
                     const Vector<T>& v){
    tslassert(m.finalized == true);
    m.task->setVectors(&r, &v);
    m.pool->assignTask(m.task);
  }

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::finalizeParallel(ThreadPool* _pool){
    pool = _pool;
    if(task)
      delete task;

    finalize();

    task = new ParallelSPMVTask<N, T>(pool, this);
  }
#endif

  /**************************************************************************/
  template<int N, class T>
  void SpMatrix<N, T>::computeBlockDistribution(MatrixRange* mRange,
                                                VectorRange* vRange,
                                                int* n_blks,
                                                int n_segments)const{
    tslassert(blocks != 0);
    /*Compute distribution of the blocks over the segments. The
      corresponding vector dimension per segment is a multiple of 16,
      because the partial vector and matrix functions operate with
      chunks of 16 floats*/

	int n_combined_rows = 16/N;
    int blocks_per_segment = (int)ceil((float)n_blocks/(float)n_segments);

    int currentBlockRow = 0;
    int currentRow = 0;

    int totalBlocks = 0;
    for(int i=0;i<n_segments;i++){
      mRange[i].startRow = currentRow;
      currentBlockRow = 0;
      while(currentBlockRow < blocks_per_segment){
        int stop = 0;
        int k = 0;
        for(;k<n_combined_rows;k++){
          if(currentRow + k < height/N){
            currentBlockRow += row_lengths[currentRow + k];
          }else{
            stop = 1;
            break;
          }
        }
        currentRow += k;
        if(stop){
          break;
        }
      }

      mRange[i].endRow = currentRow;
      n_blks[i] = currentBlockRow;
      totalBlocks += currentBlockRow;
    }
    tslassert(n_blocks == totalBlocks);

    for(int i=0;i<n_segments;i++){
      vRange[i].startBlock = mRange[i].startRow * N;
      vRange[i].endBlock   = mRange[i].endRow * N;
      vRange[i].range = vRange[i].endBlock - vRange[i].startBlock;
      mRange[i].range = mRange[i].endRow - mRange[i].startRow;
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void multiplyJacobi(Vector<T>& r,
                      const SpMatrix<N, T>& m,
                      const Vector<T>& v){
    tslassert(m.getWidth() == v.getSize());
    tslassert(r.getSize()  == m.getHeight());
    tslassert(m.finalized);

    int idx = 0;
    int n_rows = m.height/N;

    const T* data = v.data;

    SpMatrixBlock<N, T> tmpBlock;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        tmpBlock.clear();

        int diagBlockIndex = -1;

        for(int j=0;j<row_length;j++, idx++){
          int col_index = m.comp_col_indices[idx]/N;
          if(col_index == i){
            //Diagonal block
            tmpBlock.vectorMulAddJacobi(&(m.blocks[idx]),
                                        &(data[col_index*N]));
            diagBlockIndex = idx;
          }else{
            //Off-diagonal block
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[col_index*N]));
          }
        }

        tslassert(diagBlockIndex != -1);

        tmpBlock.rowSumReduce();

        /*Store and divide by diagonal*/
        for(int j=0;j<N;j++){
          r.data[i*N + j] = tmpBlock.m[j*N] / m.blocks[diagBlockIndex].m[j*N+j];
        }
      }
    }
  }

  /**************************************************************************/
  template<int N, class T>
  void multiplyGaussSeidel(Vector<T>& r,
                           const SpMatrix<N, T>& m,
                           const Vector<T>& v,
                           const Vector<T>* b){
    tslassert(m.getWidth() == v.getSize());
    tslassert(r.getSize()  == m.getHeight());
    tslassert(m.finalized);

    int idx = 0;
    int n_rows = m.height/N;

    int height = m.getHeight();

    const T* data = v.data;
    T* rdata = r.data;

    SpMatrixBlock<N, T> tmpBlock;

    for(int i=0;i<n_rows;i++){
      int row_length = m.row_lengths[i];

      if(row_length > 0){
        tmpBlock.clear();

        int diagBlockIndex = -1;

        for(int j=0;j<row_length;j++, idx++){
          int col_index = m.comp_col_indices[idx] / N;
          if(col_index == i){
            //Diagonal block
            /*Skip diagonal block for now, and is performed as last*/
            diagBlockIndex = idx;
          }else if(col_index < i){
            //Off-diagonal block, use vector r containing updated
            //values
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(rdata[col_index*N]));
          }else{
            //Off-diagonal block, use vector v
            tmpBlock.vectorMulAdd(&(m.blocks[idx]),
                                  &(data[col_index*N]));

          }
        }

        tslassert(diagBlockIndex != -1);

        tmpBlock.rowSumReduce();

        /*Store*/
        for(int j=0;j<N;j++){
          r.data[i*N + j] = -tmpBlock.m[j*N];

          if(b){
            r.data[i*N + j] += b->data[i*N + j];
          }
        }

        /*Perform multiplication of diagonal block*/
        int col_index = m.comp_col_indices[diagBlockIndex] / N;
        for(int j=0;j<N;j++){
          T diagElement = 0;

          for(int k=0;k<N;k++){
            if(j==k){
              /*Diagonal element*/
              diagElement = m.blocks[diagBlockIndex].m[j*N+k];
            }else if(j<k){
              /*Use vector v*/
              r.data[i*N+j] -= m.blocks[diagBlockIndex].m[j*N+k] *
                data[col_index*N + k];
            }else{
              /*Use updated k*/
              r.data[i*N+j] -= m.blocks[diagBlockIndex].m[j*N+k] *
                rdata[col_index*N + k];
            }
          }

          if(i*N + j < height){
            r.data[i*N + j] /= diagElement;
          }else{
            r.data[i*N + j] = (T)0.0;
          }
        }
      }
    }
  }
}

#endif/*SPMATRIX_HPP*/
