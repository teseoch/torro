/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef SPMATRIX_REORDER_HPP
#define SPMATRIX_REORDER_HPP

//#include "math/SpMatrix.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  class SpMatrix;

  /**************************************************************************/
  /*Reversed Cuthill-Mckee method*/
  template<int N, class T>
  SpMatrix<N, T>* reorderRCM(const SpMatrix<N, T>*);

  /**************************************************************************/
  /*King's method*/
  template<int N, class T>
  SpMatrix<N, T>* reorderKING(const SpMatrix<N, T>*,
                              int* map = 0,
                              int* reverseMap = 0);

  /**************************************************************************/
  /*Modified Minimum Degree method*/
  template<int N, class T>
  SpMatrix<N, T>* reorderMMD(const SpMatrix<N, T>*);

  /**************************************************************************/
  template<int N, class T>
  SpMatrix<N, T>* reorderMy(const SpMatrix<N, T>*);
}


#endif/*SPMATRIX_REORDER_HPP*/
