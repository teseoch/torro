/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef ROOTFIND_HPP
#define ROOTFIND_HPP

#include "math/Math.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class X, class Y>
  class Interpolator{
  public:
    /**************************************************************************/
    void init(){
    }

    /**************************************************************************/
    T evaluate(T){
      return 0.0;
    }

    /**************************************************************************/
    void getResult(T a,
                   X* b,
                   Y* c){
    }

    /**************************************************************************/
    void setComputeDerivative(bool d,
                              T dx){
    }
  };

  /**************************************************************************/
  template<class T, class X, class Y>
  T secant(Interpolator<T, X, Y>* func,
           T x1,
           T x2,
           T tol){
    func->init();

    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);

    for(int i=0;i<100;i++){
      T derivative = (fx2 - fx1)/(x2 - x1);

      T xt = x2 - (fx2)/(derivative);

      T fxt = func->evaluate(xt);

      if(Abs(derivative) < tol){
        return x2;
      }

      x1 = x2;
      fx1 = fx2;

      x2 = xt;
      fx2 = fxt;

      if(Abs(fxt) < tol){
        return xt;
      }
      if(Abs(x1 - x2) < tol){
        return xt;
      }
    }
    return x2;
  }

  /**************************************************************************/
  template<class T, class X, class Y>
  T falsePosition(Interpolator<T, X, Y>* func,
                  T x1,
                  T x2,
                  T tol){
    func->init();

    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);
#if 0
    if(fx1 > 0){
      /*Swap such that fx1, x1 are the negative part of the function*/
      T fxt = fx1;
      T xt = x1;
      fx1 = fx2;
      x1 = x2;
      fx2 = fxt;
      x2 = xt;
    }
#endif
    int side = 0;

    for(int i=0;i<100;i++){
      T xn = (fx1 * x2 - fx2 * x1)/(fx1 - fx2);

      if(Abs(x2 - xn) < tol || Abs(x1 - x2) < tol){
        return xn;
      }

      T fxn = func->evaluate(xn);

      if(Abs(fxn) < tol){
        return xn;
      }

      //x2 = xn;
      //fx2 = fxn;

#if 0
      if(fx1*fxn > 0){
        x1 = xn;
        fx1 = fxn;
      }else{
        x2 = xn;
        fx2 = fxn;
      }
#else
      if(fxn * fx2 > 0.0){
        x2 = xn;
        fx2 = fxn;
        if(side == -1){
          fx1 /= (T)2.0;
        }
        side = -1;
      }else if(fx1 * fxn > 0.0){
        x1 = xn;
        fx1 = fxn;
        if(side == 1){
          fx2 /= (T)2.0;
        }
        side = 1;
      }else{
        return xn;
      }
#endif
    }
    return x2;
  }

  /**************************************************************************/
  template<class T>
  class DoubleRootResult{
  public:
    DoubleRootResult(){
      roots[0] = roots[1] = 0.0;
      derivatives[0] = derivatives[1] = 0.0;
      defined[0] = defined[1] = false;
      functionValues[0] = functionValues[1] = functionValues[2] = (T)0.0;
      values[0] = values[1] = values[2] = (T)0.0;
    }

    T roots[2];
    T derivatives[2];
    bool defined[2];
    T functionValues[3];
    T values[3];
  };

  /**************************************************************************/
  template<class T, class X, class Y>
  DoubleRootResult<T> doubleRootFinder(Interpolator<T, X, Y>* func,
                                       T x1,
                                       T x2,
                                       T tol){
    func->init();

    DoubleRootResult<T> result;

    /*Check if the root is bracketed*/
    T fx10 = func->evaluate(x1);
    T fx20 = func->evaluate(x2);

    DBG(message("fx10 = %10.10e", fx10));
    DBG(message("fx20 = %10.10e", fx20));

    if(Sign(fx10) != Sign(fx20)){
      result.roots[0] = brentDekker(func, x1, x2, tol);
      result.defined[0] = true;
      result.functionValues[0] = fx10;
      result.functionValues[1] = fx20;
      result.values[0] = x1;
      result.values[1] = x2;
      return result;
    }

    /*Same signs, approximate derivative at end and beginning*/
    T fx11 = func->evaluate(x1 + (T)1e-4);
    T fx21 = func->evaluate(x2 + (T)1e-4);

    DBG(message("fx11 = %10.10e", fx11));
    DBG(message("fx21 = %10.10e", fx21));

    T dfx1 = (fx11 - fx10)/(T)1e-4;
    T dfx2 = (fx21 - fx20)/(T)1e-4;

    DBG(message("Equal sign, derivatives = %10.10e, %10.10e", dfx1, dfx2));
    result.derivatives[0] = dfx1;
    result.derivatives[1] = dfx2;

    if(Sign(dfx1) != Sign(dfx2)){
      /*There is some maximum in between*/
      T mid = (x1 * dfx1 - x2 * dfx2)/(dfx1 - dfx2);
      DBG(message("  mid = %10.10e", mid));
      T fxmid = func->evaluate(mid);

      START_DEBUG;
      message("f 0   = %10.10e", fx10);
      message("f mid = %10.10e", fxmid);
      message("f 1   = %10.10e", fx20);
      message("x1 = %10.10e, mid = %10.10e, x2 = %10.10e\n\n", x1, mid, x2);
      END_DEBUG;

      if(Sign(fx10) != Sign(fxmid)){
        DBG(message("double root with different signed derivatives"));
        result.roots[0] = brentDekker(func, x1, mid, tol);
        result.defined[0] = true;

        result.roots[1] = brentDekker(func, mid, x2, tol);
        result.defined[1] = true;

        result.functionValues[0] = fx10;
        result.functionValues[1] = fxmid;
        result.functionValues[2] = fx20;
        result.values[0] = x1;
        result.values[1] = mid;
        result.values[2] = x2;

        DBG(message("root1 = %10.10e", result.roots[0]));
        DBG(message("root2 = %10.10e", result.roots[1]));
      }else{
        DBG(message("double root with equal signed derivatives"));

        func->setComputeDerivative(true, (T)1e-6);

        T extrema = brentDekker(func, x1, x2, tol);

        DBG(message("Extrema at %10.10e", extrema));

        func->setComputeDerivative(false, (T)0.0);

        fxmid = func->evaluate(extrema);
        DBG(message("fx1 = %10.10e", fx10));
        DBG(message("fxm = %10.10e", fxmid));
        DBG(message("fx2 = %10.10e", fx20));

        if(Sign(fxmid) == Sign(fx10)){
          warning("sign at extrema and boundary is the same.");
          //result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
          result.roots[0] = muller(func, x1, extrema, x2, tol);
          result.defined[0] = true;

          result.functionValues[0] = fx10;
          result.functionValues[1] = fxmid;
          result.functionValues[2] = fx20;
          result.values[0] = x1;
          result.values[1] = extrema;
          result.values[2] = x2;

          if(result.roots[0] >= x1 && result.roots[0] <= x2){
            //ok
            (warning("After extrema test:: muller computed %10.10e", result.roots[0]));
          }else{
            result.defined[0] = false;
            (warning("After extrema test:: muller gave out of bounds result %10.10e", result.roots[0]));
            throw new MathException(__LINE__, __FILE__, "No root found, extrema has same sign as fx1 and fx2, muller failed");
          }
        }

        result.roots[0] = brentDekker(func, x1, extrema, tol);
        result.defined[0] = true;
        DBG(message("Root 1 = %10.10e", result.roots[0]));

        result.roots[1] = brentDekker(func, extrema, x2, tol);
        result.defined[1] = true;
        DBG(message("Root 2 = %10.10e", result.roots[1]));

        result.functionValues[0] = fx10;
        result.functionValues[1] = fxmid;
        result.functionValues[2] = fx20;
        result.values[0] = x1;
        result.values[1] = extrema;
        result.values[2] = x2;

        DBG(message("root1 = %10.10e", result.roots[0]));
        DBG(message("root2 = %10.10e", result.roots[1]));
#if 0
        result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
        result.defined[0] = true;

        result.functionValues[0] = fx10;
        result.functionValues[1] = fx20;
        result.values[0] = x1;
        result.values[1] = x2;

        if(result.roots[0] >= x1 && result.roots[0] <= x2){
          //ok
          (warning("muller computed %10.10e", result.roots[0]));
        }else{
          result.defined[0] = false;
          (warning("muller gave out of bounds result %10.10e", result.roots[0]));
          throw new MathException(__LINE__, __FILE__, "No root found");
        }
#endif
      }
    }else{
      /*Derivatives have equal sign, assume monotonically
        (de/in)creasing function, no root, but check to be sure*/
#if 0
      result.roots[0] = muller(func, x1, (T)(x1+x2)/(T)2.0, x2, tol);
      result.defined[0] = true;

      result.functionValues[0] = fx10;
      result.functionValues[1] = fx20;
      result.values[0] = x1;
      result.values[1] = x2;

      if(result.roots[0] >= x1 && result.roots[0] <= x2){
        //ok
        warning("muller computed %10.10e", result.roots[0]);
      }else{
        result.defined[0] = false;
        warning("muller gave out of bounds result %10.10e", result.roots[0]);
        throw new MathException(__LINE__, __FILE__, "No root found");
      }
#endif
      //throw new MathException(__LINE__, __FILE__, "No root found, sd and derivatives have same sign");
    }

    return result;
  }

  /**************************************************************************/
  template<class T, class X, class Y>
  T bisection(Interpolator<T, X, Y>* func,
              T x1,
              T x2,
              T tol){
    T dx, f, fmid, xmid, rtb;

    func->init();

    f = func->evaluate(x1);
    fmid = func->evaluate(x2);

    if(f*fmid >= (T)0.0){
      error("Root must be bracketed");
    }

    rtb = f < (T)0.0 ? (dx = x2-x1, x1) : (dx=x1-x2, x2);

    for(int i = 0;i<100;i++){
      fmid = func->evaluate(xmid = rtb + (dx*=(T)0.5));
      if(fmid <= (T)0.0){
        rtb = xmid;
      }

      if(Abs(dx) < tol || fmid == (T)0.0){
        return rtb;
      }
    }
    error("Too many bisection iterations");
    return (T)0.0;
  }

  /**************************************************************************/
  /*Also supports complex values of T*/
  template<class T, class X, class Y>
  T muller(Interpolator<T, X, Y>* func,
           T x0,
           T x1,
           T x2,
           T tol){
    int iter = 0;
    T h1 = x1 - x0;
    T h2 = x2 - x1;

    func->init();

    T fx0 = func->evaluate(x0);
    T fx1 = func->evaluate(x1);
    T fx2 = func->evaluate(x2);

    T delta1 = (fx1 - fx0)/h1;
    T delta2 = (fx2 - fx1)/h2;

    T d = (delta2 - delta1)/(h2 + h1);
    iter = 3;

    while(iter < 100){
      T b = delta2 + h2 * d;

      T arg = Sqr(b) - fx2 * d * (T)4.0;

      T D = Sqrt(arg);

      if(IsNan(D)){
        return D;
      }

      T E = (T)0.0;

      if(Abs(b - D) < Abs(b + D)){
        E = b + D;
      }else{
        E = b - D;
      }

      T h = fx2 * (T)(-2.0) / E;
      T x = x2 + h;

      if(Abs(h) < Abs(tol)){
        return x;
      }

      x0 = x1;
      fx0 = fx1;
      x1 = x2;
      fx1 = fx2;
      x2 = x;

      if(Abs(x2 - x1) < Abs(tol)){
        return x;
      }

      fx2 = func->evaluate(x2);

      DBG(message("iter %d", iter);
          std::cout << x2 << " | " << fx2 << std::endl);

      h1 = x1 - x0;
      h2 = x2 - x1;
      delta1 = (fx1 - fx0)/h1;
      delta2 = (fx2 - fx1)/h2;

      d = (delta2 - delta1)/(h2 + h1);

      if(Abs(delta2 - delta1) < Abs(tol)){
        return x;
      }
      iter++;
    }
    return x2;
  }

  /**************************************************************************/
  template<class T, class X, class Y>
  T brentDekker(Interpolator<T, X, Y>* func,
                T x1,
                T x2,
                T tol){
    int iter = 0;
    T a = x1;
    T b = x2;
    T c = x2;
    T d = 0, e = 0, min1, min2;

    T meps = (T)5e-8;

    func->init();

    T fa = func->evaluate(a);
    T fb = func->evaluate(b);
    T fc, p, q, r, s, tol1, xm;

#if 0
    if(Abs(fa) < meps){
      return a;
    }

    if(Abs(fb) < meps){
      return b;
    }
#endif

    if( (fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)){
      warning("Root is not bracketed, %10.10e, %10.10e, %10.10e, %10.10e",
              fa, fb, a, b);
      //error("Root is not bracketed, %10.10e, %10.10e", fa, fb);
      throw new MathException(__LINE__, __FILE__, "Root not bracketed");
    }

    fc = fb;

    for(iter=0;iter<100;iter++){
      if( (fb > 0.0 && fc > 0.0) ||
          (fb < 0.0 && fc < 0.0)){
        c  = a;
        fc = fa;
        e  = d = b - a;
      }

      if(Abs(fc) < Abs(fb)){
        a = b;
        b = c;
        c = a;
        fa = fb;
        fb = fc;
        fc = fa;
      }

      tol1 = (T)2.0 * meps * Abs(b) + (T)0.5*tol;
      xm   = (T)0.5 * (c-b);

      if(Abs(xm) <= tol1 || fb == (T)0.0){
        DBG(message("xm = %10.10e, %10.10e, %10.10e, %10.10e", xm, a, b, c));
        return b;
      }

      if(Abs(e) >= tol1 && Abs(fa) > Abs(fb)){
        /*Inverse quadratic interpolation*/
        s = fb/fa;

        if(a == c){
          p = (T)2.0*xm*s;
          q = (T)1.0 - s;
        }else{
          q = fa/fc;
          r = fb/fc;
          p = s*( (T)2.0*xm*q*(q-r) - (b-a)*(r-(T)1.0) );
          q = (q-(T)1.0)*(r-(T)1.0)*(s-(T)1.0);
        }

        if(p > 0.0){
          q = -q;
        }

        p = Abs(p);

        min1 = (T)3.0*xm*q - Abs(tol1*q);
        min2 = Abs(e*q);

        if((T)2.0*p < (min1 < min2 ? min1 : min2)){
          /*Accept interpolation*/
          e = d;
          d = p/q;
        }else{
          /*Interpolation failed, use bisection*/
          d = xm;
          e = d;
        }
      }else{
        /*Bounds decreasing too slowly, use bisection*/
        d = xm;
        e = d;
      }

      a = b;
      fa = fb;
      if( Abs(d) > tol1){
        b += d;
      }else{
        b += Sign(tol1, xm);
      }
      fb = func->evaluate(b);
    }

    error("Maximum numbers of iterations");
    return 0.0;
  }
}

#endif/*ROOTFIND_HPP*/
