/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef MATRIX44_H
#define MATRIX44_H

#ifndef __ANDROID__
#include <iostream>
#endif

#include <string.h>
#include "math/Vector4.hpp"
#include "math/Math.hpp"

#include <stdio.h>

#if defined SSE2
#include "math/x86-sse/sse_matrix_intrinsics.hpp"
using namespace tsl::x86_sse2;
#elif defined NEON
#include "math/arm-neon/neon_matrix_intrinsics.hpp"
using namespace tsl::arm_neon;
#else
#include "math/default/default_matrix_intrinsics.hpp"
using namespace tsl::default_proc;
#endif


namespace tsl{
  /**************************************************************************/
  template<class T>
  class Matrix22;

  /**************************************************************************/
  template<class T>
  class Matrix33;

  /**************************************************************************/
  template<class T>
  class Vector3;

  /**************************************************************************/
  template<class T>
  class Vector2;

  /**************************************************************************/
  template<class T>
  class Matrix44
  {
    /**************************************************************************/
    friend class Vector4<T>;

    /**************************************************************************/
    friend class Vector3<T>;

    /**************************************************************************/
    friend class Vector2<T>;

    /**************************************************************************/
    friend class Matrix33<T>;

    /**************************************************************************/
    friend class Matrix22<T>;
  protected:
    /**************************************************************************/
    Vector4<T> m[4]; /*Rows*/
  public:

    /**************************************************************************/
    static const Matrix44<T> identity(){
      Matrix44<T> r;
      r.m[0][0] = r.m[1][1] = r.m[2][2] = r.m[3][3] = MultiplyIdentity(T());
      return r;
    }

    /**************************************************************************/
    Matrix44(){
      /*Initializes with zeros due to default constructor of Vector4*/
    }

    /**************************************************************************/
    Matrix44(const Matrix44<T>& o){
      m[0] = o[0];
      m[1] = o[1];
      m[2] = o[2];
      m[3] = o[3];
    }

    /**************************************************************************/
    Matrix44(const T f){
      error("called?");
      m[0] = f;
      m[1] = f;
      m[2] = f;
      m[3] = f;
    }

    /**************************************************************************/
    Matrix44(const Matrix22<T>& mat){
      m[0][0] = mat.m[0];
      m[0][1] = mat.m[1];
      m[1][0] = mat.m[2];
      m[1][1] = mat.m[3];
    }

    /**************************************************************************/
    Matrix44(const T a00, const T a01, const T a02, const T a03,
             const T a10, const T a11, const T a12, const T a13,
             const T a20, const T a21, const T a22, const T a23,
             const T a30, const T a31, const T a32, const T a33){
      m[0].set(a00, a01, a02, a03);
      m[1].set(a10, a11, a12, a13);
      m[2].set(a20, a21, a22, a23);
      m[3].set(a30, a31, a32, a33);
    }

    /**************************************************************************/
    Matrix44(const T a00, const T a01, const T a02,
             const T a10, const T a11, const T a12,
             const T a20, const T a21, const T a22){
      m[0].set(a00, a01, a02, (T)0.0);
      m[1].set(a10, a11, a12, (T)0.0);
      m[2].set(a20, a21, a22, (T)0.0);
      m[3].set((T)0.0, (T)0.0, (T)0.0, (T)0.0);
    }

    /**************************************************************************/
    Matrix44(const Vector4<T>& a,
             const Vector4<T>& b,
             const Vector4<T>& c,
             const Vector4<T>& d){
      m[0] = a;
      m[1] = b;
      m[2] = c;
      m[3] = d;
    }

    /**************************************************************************/
    Matrix44(const T* const data){
      m[0].set(data + 0);
      m[1].set(data + 4);
      m[2].set(data + 8);
      m[3].set(data + 12);
    }

    /**************************************************************************/
    Matrix44<T>& operator=(const Matrix44<T>& o){
      m[0] = o[0];
      m[1] = o[1];
      m[2] = o[2];
      m[3] = o[3];
      return *this;
    }

#if 0
    /**************************************************************************/
    Matrix44<T>& operator=(T f){
      error("called?");
      m[0] = f;
      m[1] = f;
      m[2] = f;
      m[3] = f;
      return *this;
    }
#endif

    /**************************************************************************/
    Matrix44<T>& set(const Matrix44<T>& o){
      m[0] = o.m[0];
      m[1] = o.m[1];
      m[2] = o.m[2];
      m[3] = o.m[3];
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& set(T f){
      m[0].set(f);
      m[1].set(f);
      m[2].set(f);
      m[3].set(f);
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& set(T a00, T a01, T a02, T a03,
                     T a10, T a11, T a12, T a13,
                     T a20, T a21, T a22, T a23,
                     T a30, T a31, T a32, T a33){
      m[0].set(a00, a01, a02, a03);
      m[1].set(a10, a11, a12, a13);
      m[2].set(a20, a21, a22, a23);
      m[3].set(a30, a31, a32, a33);
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& set(const Vector4<T>& a,
                     const Vector4<T>& b,
                     const Vector4<T>& c,
                     const Vector4<T>& d){
      m[0] = a;
      m[1] = b;
      m[2] = c;
      m[3] = d;
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& operator+=(const Matrix44<T>& w){
      m[0] += w.m[0];
      m[1] += w.m[1];
      m[2] += w.m[2];
      m[3] += w.m[3];
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& operator-=(const Matrix44<T>& w){
      m[0] -= w.m[0];
      m[1] -= w.m[1];
      m[2] -= w.m[2];
      m[3] -= w.m[3];
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& operator*=(const Matrix44<T>& w){
      if(this == &w){
        Matrix44<T> ww = w;
        matrix44_mul_matrix44(m[0].m, m[1].m, m[2].m, m[3].m,
                              m[0].m, m[1].m, m[2].m, m[3].m,
                              ww[0].m, ww[1].m, ww[2].m, ww[3].m);
      }else{
        matrix44_mul_matrix44(m[0].m, m[1].m, m[2].m, m[3].m,
                              m[0].m, m[1].m, m[2].m, m[3].m,
                              w[0].m, w[1].m, w[2].m, w[3].m);
      }

      return *this;
    }

#if 0
    /**************************************************************************/
    Matrix44<T>& multiplyAdd(const Matrix44<T>& w, const Vector4<T>& v){
      /*this += w * v*/
      matrix44_vmadd(m[0], m[1], m[2], m[3],
                     m[0], m[1], m[2], m[3],
                     w.m[0], w.m[1], w.m[2], w.m[3], v.m);
      return *this;
    }
#endif

    /**************************************************************************/
    Matrix44<T>& operator*=(T f){
      m[0] *= f;
      m[1] *= f;
      m[2] *= f;
      m[3] *= f;
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& operator/=(T f){
      m[0] /= f;
      m[1] /= f;
      m[2] /= f;
      m[3] /= f;
      return *this;
    }

    /**************************************************************************/
    Vector4<T>& operator[](int i){
#ifdef N_DEBUG
      return m[i];
#else
      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Matrix44::operator[] out of bounds %d", i);
      }
      return m[0];
#endif
    }

    /**************************************************************************/
    const Vector4<T>& operator[](int i)const {
#ifdef N_DEBUG
      return m[i];
#else
      if(i>=0 && i<4){
        return m[i];
      }else{
        error("Matrix44::operator[] out of bounds %d", i);
      }
      return m[0];
#endif
    }

    /**************************************************************************/
    Matrix44<T> operator-() const{
      return (*this)*(-MultiplyIdentity(T()));
    }

    /**************************************************************************/
    Matrix44<T> operator+() const{
      return *this;
    }

    /**************************************************************************/
    Matrix44<T> operator+(const Matrix44<T>& w) const{
      Matrix44<T> r = *this;
      r += w;
      return r;
    }

    /**************************************************************************/
    Matrix44<T> operator-(const Matrix44<T>& w) const{
      Matrix44<T> r = *this;
      r -= w;
      return r;
    }

    /**************************************************************************/
    Matrix44<T> operator*(const Matrix44<T>& w) const{
      Matrix44<T> r;
      matrix44_mul_matrix44(r[0].m, r[1].m, r[2].m, r[3].m,
                            m[0].m, m[1].m, m[2].m, m[3].m,
                            w[0].m, w[1].m, w[2].m, w[3].m);
      return r;
    }

    /**************************************************************************/
    Vector4<T> operator*(const Vector4<T>& v)const;

    /**************************************************************************/
    Matrix44<T> operator*(const T x){
      Matrix44<T> r(m[0] * x,
                    m[1] * x,
                    m[2] * x,
                    m[3] * x);
      return r;
    }

    /**************************************************************************/
    template<class Y>
    friend Matrix44<Y> operator*(const Y x, const Matrix44<Y>& m);

    /**************************************************************************/
    template<class Y>
    friend Matrix44<Y> operator*(const Matrix44<Y>& m, const Y x);

    /**************************************************************************/
    template<class Y>
    friend Matrix44<Y> operator/(const Y x, const Matrix44<Y>& m);

    /**************************************************************************/
    template<class Y>
    friend Matrix44<Y> operator/(const Matrix44<Y>& m, const Y x);

    /**************************************************************************/
    Matrix44<T>& clear(){
      m[0].set(SumIdentity(T()));
      m[1].set(SumIdentity(T()));
      m[2].set(SumIdentity(T()));
      m[3].set(SumIdentity(T()));
      return *this;
    }

    /**************************************************************************/
    Matrix44<T>& eye(){
      *this = Matrix44<T>::identity();
      return *this;
    }

    /**************************************************************************/
    T det() const{
      return matrix44_determinant(m[0].m, m[1].m, m[2].m, m[3].m);
    }

    /**************************************************************************/
#define DET2(a00, a01,                              \
             a10, a11) ((a00)*(a11) - (a10)*(a01))

#define DET3(a00, a01, a02,                                     \
             a10, a11, a12,                                     \
             a20, a21, a22) ((a00)*DET2(a11, a12, a21, a22) -	\
                             (a10)*DET2(a01, a02, a21, a22) +	\
                             (a20)*DET2(a01, a02, a11, a12))

#define MAD4 m[0][0]
#define MBD4 m[0][1]
#define MCD4 m[0][2]
#define MDD4 m[0][3]
#define MED4 m[1][0]
#define MFD4 m[1][1]
#define MGD4 m[1][2]
#define MHD4 m[1][3]
#define MID4 m[2][0]
#define MJD4 m[2][1]
#define MKD4 m[2][2]
#define MLD4 m[2][3]
#define MMD4 m[3][0]
#define MND4 m[3][1]
#define MOD4 m[3][2]
#define MPD4 m[3][3]

    /**************************************************************************/
    T det3() const{
#ifdef SSE2
      return MAD4 * (MFD4 * MKD4 - MGD4 * MJD4) - MED4 * (MBD4 * MKD4 - MCD4 * MJD4) + MID4 * (MBD4 * MGD4 - MCD4 * MFD4);
#else
      return DET3(MAD4, MBD4, MCD4,
                  MED4, MFD4, MGD4,
                  MID4, MJD4, MKD4);
#endif
    }

    /**************************************************************************/
    T detDiag()const{
      return m[0][0] * m[1][1] * m[2][2] * m[3][3];
    }

    /**************************************************************************/
    /*p norm of a matrix with p=2*/
    T norm2()const{
      Vector4<T> I(MultiplyIdentity(T()), MultiplyIdentity(T()),
                   MultiplyIdentity(T()), MultiplyIdentity(T()));
      Vector4<T> r = (*this)*I;
      return r.length()/(T)2.0;
    }

    /**************************************************************************/
    Matrix44<T> transpose()const{
      Matrix44<T> r;
      matrix44_transpose(r[0].m, r[1].m, r[2].m, r[3].m,
                         m[0].m, m[1].m, m[2].m, m[3].m);
      return r;
    }

    /**************************************************************************/
    Matrix44<T> transpose3()const{
      Matrix44<T> r;
      r.m[0][0] = m[0][0];
      r.m[1][1] = m[1][1];
      r.m[2][2] = m[2][2];
      r.m[3][3] = m[3][3];

      r.m[0][1] = m[1][0];
      r.m[0][2] = m[2][0];

      r.m[1][0] = m[0][1];
      r.m[1][2] = m[2][1];

      r.m[2][0] = m[0][2];
      r.m[2][1] = m[1][2];

      return r;
    }

    /**************************************************************************/
    Matrix44<T> inverse(T* det = 0)const{
      Matrix44<T> res;
      T det2;
      matrix44_inverse(&det2, res[0].m, res[1].m, res[2].m, res[3].m,
                       m[0].m, m[1].m, m[2].m, m[3].m);
      if(det){
        *det = det2;
      }
      return res;
    }

    /**************************************************************************/
    /*Treat 4x4 matrix as 3x3*/
    Matrix44<T> inverse3x3(T* det = 0)const{
      Matrix44<T> r;
      /*Compute adjoint*/
      r[0][0] = +(m[1][1] * m[2][2] - m[1][2] * m[2][1]);
      r[0][1] = -(m[0][1] * m[2][2] - m[0][2] * m[2][1]);
      r[0][2] = +(m[0][1] * m[1][2] - m[0][2] * m[1][1]);

      r[1][0] = -(m[1][0] * m[2][2] - m[1][2] * m[2][0]);
      r[1][1] = +(m[0][0] * m[2][2] - m[0][2] * m[2][0]);
      r[1][2] = -(m[0][0] * m[1][2] - m[0][2] * m[1][0]);

      r[2][0] = +(m[1][0] * m[2][1] - m[1][1] * m[2][0]);
      r[2][1] = -(m[0][0] * m[2][1] - m[0][1] * m[2][0]);
      r[2][2] = +(m[0][0] * m[1][1] - m[0][1] * m[1][0]);

      r[3][3] = (T)1.0;

      /*Compute 1/determinant given adjoint*/
      T d = (m[0][0] * r.m[0][0] + m[0][1] * r.m[1][0] + m[0][2] * r.m[2][0]);

      if(det){
        *det = d;
      }

      d = ((T)1.0)/d;

      /*Compute inverse*/
      for(int i=0;i<9;i++){
        r.m[i] *= d;
      }

      return r;
    }

    /**************************************************************************/
    /*Treat as diagonal matrix*/
    Matrix44<T> inverseDiag()const{
      Matrix44<T> r;
      r.m[0][0] = (T)1.0/m[0][0];
      r.m[1][1] = (T)1.0/m[1][1];
      r.m[2][2] = (T)1.0/m[2][2];
      r.m[3][3] = (T)1.0/m[3][3];
      return r;
    }

    /**************************************************************************/
    Matrix44<T> adjugate()const{
      Matrix44<T> res;
      matrix44_adjugate(res[0].m, res[1].m, res[2].m, res[3].m,
                        m[0].m, m[1].m, m[2].m, m[3].m);
      return res;
    }

    /**************************************************************************/
    Matrix44<T> sqr()const{
      Matrix44<T> r = *this;

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          r[i][j] = Sqr(r[i][j]);
        }
      }
      return r;
    }

    /**************************************************************************/
    Matrix44<T> sqr3x3()const{
      Matrix44<T> r = *this;

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          r[i][j] = Sqr(r[i][j]);
        }
      }

      r[0][3] = (T)0.0;
      r[1][3] = (T)0.0;
      r[2][3] = (T)0.0;

      r[3] *= (T)0.0;

      r[3][3] = (T)1.0;

      return r;
    }

    /**************************************************************************/
    Matrix44<T> sqrt()const{
      Matrix44<T> y = *this;
      Matrix44<T> z = identity();

      for(int i=0;i<1000;i++){
        Matrix44<T> y1 = ((T)0.5)*(y + z.inverse());
        Matrix44<T> z1 = ((T)0.5)*(z + y.inverse());

        Matrix44<T> res = (y1-y).sqr();

        y = y1;
        z = z1;

        T norm = res.norm2();

        if(norm < 1E-10){
          return y;
        }
      }
      return y;
    }

    /**************************************************************************/
    Matrix44<T> sqrt3x3()const{
      Matrix44<T> y = *this;
      Matrix44<T> z = identity();

      for(int i=0;i<1000;i++){
        Matrix44<T> y1 = ((T)0.5)*(y + z.inverse3x3());
        Matrix44<T> z1 = ((T)0.5)*(z + y.inverse3x3());

        Matrix44<T> res = (y1-y).sqr3x3();

        y = y1;
        z = z1;

        T norm = res.norm2();

        if(norm < 1E-10){
          return y;
        }
      }
      return y;
    }

    /**************************************************************************/
    Vector4<T> getSkewVector()const{
      return Vector4<T>(m[2][1], m[0][2], m[1][0], 0.0);
    }

    /**************************************************************************/
    T trace()const{
      return m[0][0] + m[1][1] + m[2][2] + m[3][3];
    }

    /**************************************************************************/
    Vector4<T> eigenValues(Matrix44<T>* ev = 0)const;

    /**************************************************************************/
    Vector4<T> singularValues(Matrix44<T>* rU = 0,
                              Matrix44<T>* rV = 0)const;

#ifndef __ANDROID__
    /**************************************************************************/
    template<class Y>
    friend inline std::ostream& operator<<(std::ostream& os,
                                           const Matrix44<Y>& m);
#endif
  };

  /**************************************************************************/
  typedef Matrix44<float>  Matrix44f;
  typedef Matrix44<double> Matrix44d;

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> operator*(const T x, const Matrix44<T>& m){
    Matrix44<T> r = m;
    r *= x;
    return r;
  }

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> operator*(const Matrix44<T>& m, const T x){
    Matrix44<T> r = m;
    r *= x;
    return r;
  }

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> operator/(const Matrix44<T>& m, const T x){
    Matrix44<T> r = m;
    r /= x;
    return r;
  }

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> operator/(const T x, const Matrix44<T>& m){
    error("Error scalar divided by matrix");
    return Matrix44<T>();
  }

  /**************************************************************************/
  template<class T>
  inline Vector4<T> Matrix44<T>::operator*(const Vector4<T>& v)const{
#ifdef _WIN32
    __declspec(align(16)) T rr[4];
#else
    T rr[4] __attribute__ ((aligned (16)));
#endif
    matrix44_mul_vector4(rr, m[0].m, m[1].m, m[2].m, m[3].m, v.m);
    return Vector4<T>(rr[0], rr[1], rr[2], rr[3]);
  }

  /**************************************************************************/
  template<class T>
  void QR(Matrix44<T>& Q, Matrix44<T>& R, const Matrix44<T>& A, int dim);

  /**************************************************************************/
  template<class T>
  inline bool IsNan(const Matrix44<T>& d){
    for(int i=0;i<4;i++){
      for(int j=0;j<4;j++){
        if(IsNan(d[i][j])){
          return true;
        }
      }
    }
    return false;
  }

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> MultiplyIdentity(const Matrix44<T>&){
    return Matrix44<T>::identity();
  }

  /**************************************************************************/
  template<class T>
  inline Matrix44<T> SumIdentity(const Matrix44<T>&){
    Matrix44<T> r;
    r.clear();
    return r;
  }

#ifndef __ANDROID__
  /**************************************************************************/
  template<class Y>
  inline std::ostream& operator<<(std::ostream& os, const Matrix44<Y>& m){
    os << m.m[0] << m.m[1] << m.m[2] << m.m[3] << std::endl;
    return os;
  }
#endif
}

#endif/*MATRIX44_H*/
