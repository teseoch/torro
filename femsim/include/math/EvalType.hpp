/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef EVALTYPE_HPP
#define EVALTYPE_HPP

namespace tsl{

  /**************************************************************************/
  class EvalStats{
  public:
    /**************************************************************************/
    EvalStats(){
      n_kinetic_friction_update = 0;
      n_kinetic_friction_vector_update = 0;
      n_friction_check = 0;
      n_penetration_check = 0;
      n_volume_check = 0;
      n_penetration_force = 0;
      n_volume_force = 0;
      n_friction_force = 0;
      n_geometry_check = 0;
    }

    int n_kinetic_friction_update;
    int n_kinetic_friction_vector_update;
    int n_friction_check;
    int n_penetration_check;
    int n_volume_check;
    int n_penetration_force;
    int n_volume_force;
    int n_friction_force;
    int n_geometry_check;
  };

  /**************************************************************************/
  /*Class used to describe the type of constraint evaluation*/
  class EvalType{
  public:
    EvalType():evaluateGeometry(false),
               evaluatePenetration(false),
               evaluateFriction(false),
               evaluateHistory(false),
               disableConstraints(true),
               evaluateAllGeometry(false),
               updateKineticFriction(false),
               updateKineticVector(false),
               acceptFriction(false),
               forceFriction(false),
               forcePenetration(false),
               forceDuplicateRemoval(false),
               evaluateVolume(false),
               forceVolume(false){
    }

    /**************************************************************************/
    void reset(){
      evaluateGeometry = false;
      evaluatePenetration = false;
      evaluateFriction = false;
      evaluateHistory = false;
      disableConstraints = true;
      evaluateAllGeometry = false;
      evaluateHotPriorityGeometry = false;
      updateKineticFriction = false;
      updateKineticVector = false;
      acceptFriction = false;
      forceFriction = false;
      forcePenetration = false;
      forceDuplicateRemoval = false;
      evaluateVolume = false;
      forceVolume = false;
    }

    /**************************************************************************/
    void enableGeometryCheck(){
      evaluateGeometry = true;
    }

    /**************************************************************************/
    void enablePenetrationCheck(){
      evaluatePenetration = true;
    }

    /**************************************************************************/
    void enableFrictionCheck(){
      evaluateFriction = true;
    }

    /**************************************************************************/
    void enableHistoryUpdate(){
      evaluateHistory = true;
    }

    /**************************************************************************/
    void disableDisable(){
      disableConstraints = false;
    }

    /**************************************************************************/
    void enableEvaluateAllGeometry(){
      evaluateAllGeometry = true;
    }

    /**************************************************************************/
    void enableEvaluatePriorityGeometry(){
      evaluateAllGeometry = false;
    }

    /**************************************************************************/
    /*For checking only hot potential constraints*/
    void enableEvaluateHotPriorityGeometry(){
      enableEvaluatePriorityGeometry();
      evaluateHotPriorityGeometry = true;
    }

    /**************************************************************************/
    void enableKineticFrictionUpdate(){
      updateKineticFriction = true;
    }

    /**************************************************************************/
    void enableKineticVectorUpdate(){
      updateKineticVector = true;
    }

    /**************************************************************************/
    void enableAcceptFriction(){
      acceptFriction = true;
    }

    /**************************************************************************/
    void enableForceFriction(){
      forceFriction = true;
    }

    /**************************************************************************/
    void enableForcePenetration(){
      forcePenetration = true;
    }

    /**************************************************************************/
    void enableForceDuplicateRemoval(){
      forceDuplicateRemoval = true;
    }

    /**************************************************************************/
    void enableForceVolume(){
      forceVolume = true;
    }

    /**************************************************************************/
    void enableVolumeCheck(){
      evaluateVolume = true;
    }

    /**************************************************************************/
    //Performs an evaluation of the geometry in order to find new
    //non-penetration constraints
    bool geometryCheck()const{
      return evaluateGeometry;
    }

    /**************************************************************************/
    //Checks all existing constraints for non-penetration, friction or
    //for a history update.
    bool existingCheck()const{
      return (evaluatePenetration || evaluateFriction ||
              evaluateHistory || updateKineticFriction || evaluateVolume ||
              forcePenetration || forceFriction || acceptFriction ||
              forceVolume);
    }

    /**************************************************************************/
    //Performs a non-penetration check
    bool penetrationCheck()const{
      return evaluatePenetration;
    }

    /**************************************************************************/
    bool volumeCheck()const{
      return evaluateVolume;
    }

    /**************************************************************************/
    //Performs a friction check
    bool frictionCheck()const{
      return evaluateFriction;
    }

    /**************************************************************************/
    //Performs a history update
    bool historyUpdate()const{
      return evaluateHistory;
    }

    /**************************************************************************/
    bool disablingAllowed()const{
      return disableConstraints;
    }

    /**************************************************************************/
    bool priorityGeometryCheck()const{
      return !evaluateAllGeometry;
    }

    /**************************************************************************/
    bool hotPriorityGeometryCheck()const{
      return evaluateHotPriorityGeometry;
    }

    /**************************************************************************/
    bool updateKineticFrictionCheck()const{
      return updateKineticFriction;
    }

    /**************************************************************************/
    bool updateKineticVectorCheck()const{
      return updateKineticVector;
    }

    /**************************************************************************/
    bool acceptFrictionCheck()const{
      return acceptFriction;
    }

    /**************************************************************************/
    bool forceFrictionCheck()const{
      return forceFriction;
    }

    /**************************************************************************/
    bool forcePenetrationCheck()const{
      return forcePenetration;
    }

    /**************************************************************************/
    bool forceDuplicateRemovalCheck()const{
      return forceDuplicateRemoval;
    }

    /**************************************************************************/
    bool forceVolumeCheck()const{
      return forceVolume;
    }

  protected:
    /**************************************************************************/
    bool evaluateGeometry;
    bool evaluatePenetration;
    bool evaluateFriction;
    bool evaluateHistory;
    bool disableConstraints;
    bool evaluateAllGeometry; //Instead of prioritized
    bool evaluateHotPriorityGeometry; //Evaluates geometry of just
    //disabled constraints.
    bool updateKineticFriction;
    bool updateKineticVector;
    bool acceptFriction;
    bool forceFriction;
    bool forcePenetration;
    bool forceDuplicateRemoval;

    bool evaluateVolume;
    bool forceVolume;
  };
}

#endif/*EVALTYPE_HPP*/
