/* Copyright (C) 2013-2019 by Mickeal Verschoor*/

#ifndef COROTATIONAL_FEM_HPP
#define COROTATIONAL_FEM_HPP

#include "FEMModel.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class CorotationalFEM : public FEMModel<T>{
  public:
    /**************************************************************************/
    CorotationalFEM(DCTetraMesh<T>* _mesh, int n);

    /**************************************************************************/
    virtual ~CorotationalFEM();

    /**************************************************************************/
    virtual void computeElementStiffnessMatrixAndForce(MatrixT<12, 12, T>& K,
                                                       MatrixT<12, 1, T>& rhs,
                                                       MatrixT<12, 1, T>& ipos,
                                                       MatrixT<12, 1, T>& cpos,
                                                       TetrahedronData<T>& tet,
                                                       int id, T dt,
                                                       bool initial,
                                                       bool verbose);

    /**************************************************************************/
    void computePartialDerivatives(MatrixT<3, 4, T>& par, int id);

    /**************************************************************************/
    MatrixT<6, 6, T> computeMaterialMatrix(T lE, T lMu);

    /**************************************************************************/
    void extractRotationMatrix(Matrix44<T>* u,
                               Matrix44<T>* m,
                               Matrix44<T>* a,
                               Matrix44<T>* ap,
                               bool debug);

    /**************************************************************************/
    void convert44To1212Matrix(MatrixT<12, 12, T>* res,
                               const Matrix44<T>* mat);

    /**************************************************************************/
    MatrixT<12, 12, T> multiplyBlockR(const MatrixT<12, 12, T>& mat,
                                      Matrix44<T>& r);

    /**************************************************************************/
    MatrixT<12, 12, T> multiplyBlockL(const MatrixT<12, 12, T>& mat,
                                      Matrix44<T>& r);

    /**************************************************************************/
    void computeRotationDerivative(const Matrix44<T>* r,
                                   const Matrix44<T>* s,
                                   const Matrix44<T>* pos,
                                   const Matrix44<T>* ipos,
                                   MatrixT<12, 12, T>* initialK,
                                   MatrixT<12, 12, T>* derK,
                                   MatrixT<12, 12, T>* derRHS);
  };
}

#endif/*COROTATIONAL_FEM_HPP*/
