/* Copyright (C) 2013-2019 by Mickeal Verschoor*/

#ifndef FEMMODEL_HPP
#define FEMMODEL_HPP

#include "TetrahedronData.hpp"
#include "math/Matrix.hpp"
#include "datastructures/DCEList.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T>
  class FEMModel{
  public:
    /**************************************************************************/
    FEMModel(DCTetraMesh<T>* _mesh, int _nElements):mesh(_mesh),
                                                    nElements(_nElements){
    }

    /**************************************************************************/
    virtual ~FEMModel(){
    }

    /**************************************************************************/
    virtual void computeElementStiffnessMatrixAndForce(MatrixT<12, 12, T>& K,
                                                       MatrixT<12, 1, T>& rhs,
                                                       MatrixT<12, 1, T>& ipos,
                                                       MatrixT<12, 1, T>& cpos,
                                                       TetrahedronData<T>& tet,
                                                       int id, T dt,
                                                       bool initial,
                                                       bool verbose) = 0;

    /**************************************************************************/
    void setMu(T _mu){
      mu = _mu;
    }

    /**************************************************************************/
    void setE(T _E){
      E = _E;
    }

    /**************************************************************************/
    DCTetraMesh<T>* mesh;
    int nElements;
    T E;
    T mu;
  };
}

#endif/*FEMMODEL_HPP*/
