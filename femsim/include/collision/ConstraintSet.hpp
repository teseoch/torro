/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTSET_HPP
#define CONSTRAINTSET_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Triangle.hpp"
#include "math/EvalType.hpp"

#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateFace;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSet:
    public AbstractConstraintSet<T, OrientedVertex<T>, Triangle<T>, CC>{
  public:
    /**************************************************************************/
    typedef ConstraintCandidateFace<T, CC> CandidateType;

    /**************************************************************************/
    typedef AbstractConstraintCandidate<T, OrientedVertex<T>,
                                        Triangle<T>, CC> Candidate;

    /**************************************************************************/
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;

    /**************************************************************************/
    typedef typename Map<int, Candidate*>::Iterator CandidateMapIterator;

    /**************************************************************************/
    ConstraintSet();

    /**************************************************************************/
    virtual ~ConstraintSet();

    /**************************************************************************/
    virtual bool evaluate(OrientedVertex<T>& p,
                          Vector4<T>& com,
                          Quaternion<T>& rot,
                          const EvalType& etype,
                          EvalStats& stats);

    /**************************************************************************/
    /*Check the validity of the solution, i.e., constrained vertices
      can not penetrate the constraining faces.

      However, if the current solution does not project on the current
      triangle, there must be an other triangle that does so. In such
      a case, don't check the validity since it is not valid by
      definition.*/
    virtual bool checkAndUpdate(OrientedVertex<T>& p,
                                Vector4<T>& com,
                                Quaternion<T>& rot,
                                EvalStats& stats,
                                bool friction,
                                bool force,
                                T bnorm,
                                bool* allPositive);

    /**************************************************************************/
    virtual void incrementalUpdate(bool onlyForced = false);

    /**************************************************************************/
    virtual void update();

    /**************************************************************************/
    bool enableSlidedConstraint(int faceId,
                                Vector4<T> gap);

    /**************************************************************************/
    void showFaceCandidateState(int face);
  };
}

#endif/*CONSTRAINTSET_HPP*/
