/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTSETTRIANGLEEDGE_HPP
#define CONSTRAINTSETTRIANGLEEDGE_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Edge.hpp"
#include "math/EvalType.hpp"

#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateTriangleEdge;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetTriangleEdge:
    public AbstractConstraintSet<T, Edge<T>, Vector4<T>, CC>{
  public:
    /**************************************************************************/
    typedef ConstraintCandidateTriangleEdge<T, CC> CandidateType;

    /**************************************************************************/
    typedef AbstractConstraintCandidate<T, Edge<T>, Vector4<T>, CC> Candidate;

    /**************************************************************************/
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;

    /**************************************************************************/
    typedef typename Map<int, Candidate*>::Iterator CandidateMapIterator;

    /**************************************************************************/
    ConstraintSetTriangleEdge();

    /**************************************************************************/
    virtual ~ConstraintSetTriangleEdge();

    /**************************************************************************/
    virtual bool evaluate(Edge<T>& p,
                          Vector4<T>& com,
                          Quaternion<T>& rot,
                          const EvalType& etype,
                          EvalStats& stats);

    /**************************************************************************/
    virtual bool checkAndUpdate(Edge<T>& p,
                                Vector4<T>& com,
                                Quaternion<T>& rot,
                                EvalStats& stats,
                                bool friction,
                                bool force,
                                T bnorm,
                                bool* allPositive);

    /**************************************************************************/
    virtual bool updateInitial(Edge<T>& p,
                               Vector4<T>& com,
                               Quaternion<T>& rot,
                               EvalStats& stats,
                               bool friction,
                               bool force, T bnorm);

    /**************************************************************************/
    virtual void incrementalUpdate(bool onlyForced = false){
      error("Incremental update for TriangleEdge does not exist! Should not be called");
    }

    /**************************************************************************/
    virtual void update();

    /**************************************************************************/
    void showVertexCandidateState(int vertex);

  };
}

#endif/*CONSTRAINTSETTRIANGLEEDGE_HPP*/
