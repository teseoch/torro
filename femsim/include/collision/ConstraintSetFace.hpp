/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTSETFACE_HPP
#define CONSTRAINTSETFACE_HPP

#include "geo/BBox.hpp"
#include "datastructures/List.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetFaces{
  public:
    /**************************************************************************/
    ConstraintSetFaces();

    /**************************************************************************/
    virtual ~ConstraintSetFaces();

    /**************************************************************************/
    void setContext(CollisionContext<T, CC>* _ctx){
      ctx = _ctx;
    }

    /**************************************************************************/
    void update();

    /**************************************************************************/
    int faceId;
    List<int> candidates;
    CollisionContext<T, CC>* ctx;
  };
}

#endif/*CONSTRAINTSETFACE_HPP*/
