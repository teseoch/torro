/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef IECGEO_HPP
#define IECGEO_HPP

#include "collision/CollisionContext.hpp"
#include <unistd.h>

namespace tsl{

  /**************************************************************************/
  /*Provides functions for updating the collision context*/
  template<class T, class CC>
  class IECGeo{
  public:
    /**************************************************************************/
    IECGeo(CollisionContext<T, CC>* c):colContext(c){
    }

    /**************************************************************************/
    virtual void disableAllCandidates(){
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        if(colContext->edgeSets[i].getNCandidates() > 0){
          colContext->edgeSets[i].deactivateAll();
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->backEdgeSets[i].getNCandidates() > 0){
          colContext->backEdgeSets[i].deactivateAll();
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].getNCandidates() > 0){
          colContext->triangleEdgeSets[i].deactivateAll();
        }
      }

      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].getNCandidates() > 0){
          colContext->sets[i].deactivateAll();
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].getNCandidates() > 0){
          colContext->backSets[i].deactivateAll();
        }
      }
    }

    /**************************************************************************/
    virtual bool checkBoundingVolumes(){
      bool changed = false;
      colContext->mesh->computeDisplacement(colContext->solver->getx(),
                                            colContext->getDT());

      bool geometryInvalid = false;
      List<int> invalidFaces;
      List<int> invalidVertices;
      List<int> invalidEdges;

      if(!colContext->stree->volumesValid(&invalidFaces)){
        /*Update the bounding volumes*/
        warning("bounding volumes of triangles not bounding");
        geometryInvalid = true;
        colContext->stree->update(colContext->solver->getx(),
                                  colContext->getDT(), true);
      }

      /*Check individual vertex-face / edge-edge combination*/
      /*Update collision detection structures*/
      colContext->checkBBoxesVertex(&invalidVertices);
      colContext->checkBBoxesEdge(&invalidEdges);

      if(invalidVertices.size() != 0 || invalidEdges.size() != 0){
        /*Some vertices and/or edges moved outside their bounding
          volume, update them.*/
        geometryInvalid = true;
      }

      bool hotlistedPotentials = false;

      {
        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          if(colContext->sets[i].getForcedPotentialsSize() > 0){
            hotlistedPotentials = true;
            break;
          }
        }

        for(int i=0;i<n_vertices;i++){
          if(colContext->backSets[i].getForcedPotentialsSize() > 0){
            hotlistedPotentials = true;
            break;
          }
        }
      }

      if(geometryInvalid || hotlistedPotentials){
        warning("Invalid bboxes detected");
        if(hotlistedPotentials){
          warning("hotlisted potentials detected");
        }

        List<int>::Iterator it = invalidFaces.begin();
        while(it != invalidFaces.end()){
          int face = *it++;
          warning("Face %d out of box", face);

          Triangle<T> triangle;
          bool degenerate = false;

          try{
            colContext->mesh->getTriangle(&triangle, face, false);
          }catch(Exception* e){
            degenerate = true;
            delete e;
          }

          std::cout << triangle << std::endl;

          if(degenerate){
            warning("Degenerate face");
          }
        }

        it = invalidEdges.begin();
        while(it != invalidEdges.end()){
          warning("Edge %d out of box", *it++);
        }

        it = invalidVertices.begin();
        while(it != invalidVertices.end()){
          warning("Vertex %d out of box", *it++);
        }

        /*Update per edge and vertex their bounding boxes.*/
        colContext->updateBBoxes(true);

        int n_faces = colContext->mesh->getNHalfFaces();
        for(int i=0;i<n_faces;i++){
          colContext->faceFaceSets[i].update();
        }

        int n_edges = colContext->mesh->getNHalfEdges();
        for(int i=0;i<n_edges;i++){
          int twin = colContext->mesh->halfEdges[i].twin;
          if(i < twin){
            colContext->edgeSets[i].incrementalUpdate();
          }
        }

        for(int i=0;i<n_edges;i++){
          int twin = colContext->mesh->halfEdges[i].twin;
          if(i < twin){
            colContext->backEdgeSets[i].incrementalUpdate();
          }
        }

        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          colContext->sets[i].incrementalUpdate();
        }

        for(int i=0;i<n_vertices;i++){
          colContext->backSets[i].incrementalUpdate();
        }

        changed = true;
      }

      return changed;
    }

    /**************************************************************************/
    virtual bool checkGeometry(bool friction, T bnorm, EvalStats& stats){
      bool changed = false;
      bool allPositive = true;
#if 1
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        if(colContext->edgeSets[i].getNCandidates() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular edge %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com;
          Quaternion<T> rot;

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->edgeSets[i].checkAndUpdate(ne, com, rot,
                                                    stats,
                                                    friction, false,
                                                    bnorm, &allPositive)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->backEdgeSets[i].getNCandidates() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular edge %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com;
          Quaternion<T> rot;

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backEdgeSets[i].checkAndUpdate(ne, com, rot,
                                                        stats,
                                                        friction, false,
                                                        bnorm, &allPositive)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].getNCandidates() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular edge %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com(0,0,0,0);
          Quaternion<T> rot(0,0,0,1);

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->triangleEdgeSets[i].checkAndUpdate(ne, com, rot,
                                                            stats,
                                                            friction, false,
                                                            bnorm,
                                                            &allPositive)){
            changed = true;
          }
        }
      }
#endif
#if 1
      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].getNCandidates() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

          Vector4<T> com;
          Quaternion<T> rot;

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->sets[i].checkAndUpdate(p, com, rot,
                                                stats,
                                                friction, false,
                                                bnorm, &allPositive)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].getNCandidates() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

          Vector4<T> com;
          Quaternion<T> rot;

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backSets[i].checkAndUpdate(p, com, rot,
                                                    stats,
                                                    friction, false,
                                                    bnorm, &allPositive)){
            changed = true;
          }
        }
      }
#endif

      if(!changed){
        message("Update initial states");

        /*Update initial states*/
        int n_edges = colContext->mesh->getNHalfEdges();
        for(int i=0;i<n_edges;i++){
          if(colContext->edgeSets[i].getNCandidates() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, false);
            }catch(Exception* e){
              warning("Singular edge %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com;
            Quaternion<T> rot;

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->edgeSets[i].updateInitial(ne, com, rot,
                                                     stats,
                                                     friction, false,
                                                     bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_edges;i++){
          if(colContext->backEdgeSets[i].getNCandidates() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, true);
            }catch(Exception* e){
              warning("Singular edge %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com;
            Quaternion<T> rot;

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->backEdgeSets[i].updateInitial(ne, com, rot,
                                                         stats,
                                                         friction, false,
                                                         bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_edges;i++){
          if(colContext->triangleEdgeSets[i].getNCandidates() > 0){
            Edge<T> ne;
            try{
              colContext->mesh->getDisplacedEdge(&ne, i, false);
            }catch(Exception* e){
              warning("Singular edge %s", e->getError().c_str());
              delete e;
              continue;
            }

            Vector4<T> com;
            Quaternion<T> rot;

            colContext->mesh->setCurrentEdge(i);
            int vertex = colContext->mesh->getOriginVertex();

            if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[vertex];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->triangleEdgeSets[i].updateInitial(ne, com, rot,
                                                             stats,
                                                             friction, false,
                                                             bnorm)){
              changed = true;
            }
          }
        }

        int n_vertices = colContext->mesh->getNVertices();
        for(int i=0;i<n_vertices;i++){
          if(colContext->sets[i].getNCandidates() > 0){
            OrientedVertex<T> p;
            colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

            Vector4<T> com;
            Quaternion<T> rot;

            if(colContext->mesh->vertices[i].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[i];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->sets[i].updateInitial(p, com, rot,
                                                 stats,
                                                 friction, false,
                                                 bnorm)){
              changed = true;
            }
          }
        }

        for(int i=0;i<n_vertices;i++){
          if(colContext->backSets[i].getNCandidates() > 0){
            OrientedVertex<T> p;
            colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

            Vector4<T> com;
            Quaternion<T> rot;

            if(colContext->mesh->vertices[i].type == Mesh::Rigid){
              int objectId = colContext->mesh->vertexObjectMap[i];
              com = colContext->mesh->centerOfMassDispl[objectId];
              rot = colContext->mesh->orientationsDispl[objectId];
            }

            if(colContext->backSets[i].updateInitial(p, com, rot,
                                                     stats,
                                                     friction, false,
                                                     bnorm)){
              changed = true;
            }
          }
        }

        if(changed){
          message("Evaluate geometry for updated initial states");
          //EvalType etype;
          //etype.enableGeometryCheck();
          //etype.enablePenetrationCheck();
          //etype.enableFrictionCheck();
          //evaluateCandidates(etype, stats);
        }
      }

      return changed;
    }

    /**************************************************************************/
    virtual void updateCandidateLists(){
      /*Update all face normals*/
      colContext->mesh->computeFaceNormals();

      /*Update BBoxes (with extension due to velocity) of each vertex
        and edge. Face boxes are stored in tree.*/
      colContext->updateBBoxes();

      /*Performs raw collision detection for each triangle and find a
        set of potentially colliding / nearby triangles.*/
      for(int i=0;i<colContext->mesh->getNHalfFaces();i++){
        colContext->faceFaceSets[i].update();
      }

#if 1
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        int twin = colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          colContext->edgeSets[i].update();
        }
      }

      for(int i=0;i<n_edges;i++){
        int twin = colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          colContext->backEdgeSets[i].update();
        }
      }

      for(int i=0;i<n_edges;i++){
        int twin = colContext->mesh->halfEdges[i].twin;
        if(i<twin){
          colContext->triangleEdgeSets[i].update();
        }
      }
#endif

#if 1
      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        colContext->sets[i].update();
      }

      for(int i=0;i<n_vertices;i++){
        colContext->backSets[i].update();
      }
#endif
    }

    /**************************************************************************/
    void saveIntermediate(){
      while(true){
        bool error = false;
        char buffer[256];

        /*Save single precision file*/
        sprintf(buffer, "out_intermediate.tet2");
        FILE* file = fopen(buffer, "w");

        for(int i=0;i<colContext->mesh->getNVertices();i++){
          if(colContext->mesh->vertices[i].type == Mesh::Static){
            //Skip static vertices
            continue;
          }

          float xx, yy, zz;
          zz = (float)(3.0*(colContext->mesh->vertices[i].displCoord[0]));
          xx = (float)(3.0*(colContext->mesh->vertices[i].displCoord[1]));
          yy = (float)(3.0*(colContext->mesh->vertices[i].displCoord[2]));

          char* pp;
          pp = (char*)&xx;

          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&yy;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }

          pp = (char*)&zz;
          for(uint j=0;j<sizeof(float);j++){
            fputc(pp[j], file);
            if(ferror(file)){
              error = true;
            }
          }
        }

        fclose(file);

        if(!error){
          return;
        }else{
          warning("Could not properly write states to a file, disk full?");
          warning("Hit enter to retry, after making some space");
          getchar();
          sleep(10000000);
        }
      }
    }

    /**************************************************************************/
    virtual bool evaluateCandidates(const EvalType& etype, EvalStats& stats){
      bool changed = false;

      /*Update displacement information*/

      colContext->mesh->computeDisplacement(colContext->solver->getx(),
                                            colContext->getDT());

      /*Save current state. Usefull for debugging.*/
      saveIntermediate();

      /*Check for collisions on the candidate pairs*/

      /*Here it is important that the edges are checked first since
        they can be flagged due to a collapse. If an edge is flagged,
        the faces using the collapsed edge are not evaluated, except
        when they are forced to resolve a collapse*/
#if 1
      int n_edges = colContext->mesh->getNHalfEdges();
      for(int i=0;i<n_edges;i++){
        if(true){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, false);
          }catch(Exception* e){
            warning("Singular edge, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com;
          Quaternion<T> rot;

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->edgeSets[i].evaluate(ne, com, rot,
                                              etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(true){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular edge, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com;
          Quaternion<T> rot;

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backEdgeSets[i].evaluate(ne, com, rot,
                                                  etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_edges;i++){
        if(colContext->triangleEdgeSets[i].getNCandidates() > 0){
          Edge<T> ne;
          try{
            colContext->mesh->getDisplacedEdge(&ne, i, true);
          }catch(Exception* e){
            warning("Singular edge, %s", e->getError().c_str());
            delete e;
            continue;
          }

          Vector4<T> com;
          Quaternion<T> rot;

          colContext->mesh->setCurrentEdge(i);
          int vertex = colContext->mesh->getOriginVertex();

          if(colContext->mesh->vertices[vertex].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[vertex];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->triangleEdgeSets[i].evaluate(ne, com, rot,
                                                      etype,
                                                      stats)){
            changed = true;
          }
        }
      }
#endif

#if 1
      int n_vertices = colContext->mesh->getNVertices();
      for(int i=0;i<n_vertices;i++){
        if(colContext->sets[i].getNCandidates() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, false);

          Vector4<T> com;
          Quaternion<T> rot;

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->sets[i].evaluate(p, com, rot,
                                          etype, stats)){
            changed = true;
          }
        }
      }

      for(int i=0;i<n_vertices;i++){
        if(colContext->backSets[i].getNCandidates() > 0){
          OrientedVertex<T> p;
          colContext->mesh->getDisplacedOrientedVertex(&p, i, true);

          Vector4<T> com;
          Quaternion<T> rot;

          if(colContext->mesh->vertices[i].type == Mesh::Rigid){
            int objectId = colContext->mesh->vertexObjectMap[i];
            com = colContext->mesh->centerOfMassDispl[objectId];
            rot = colContext->mesh->orientationsDispl[objectId];
          }

          if(colContext->backSets[i].evaluate(p, com, rot,
                                              etype, stats)){
            changed = true;
          }
        }
      }
#endif

      if(changed){
        stats.n_geometry_check++;
      }
      return changed;
    }

    /**************************************************************************/
    virtual void checkNonLinearTerms(){
    }

    /**************************************************************************/
    virtual ~IECGeo(){
    }
  protected:
    /**************************************************************************/
   CollisionContext<T, CC>* colContext;
  };
}

#endif/*IECGEO_HPP*/
