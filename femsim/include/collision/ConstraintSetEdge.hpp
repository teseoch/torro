/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTSETEDGE_HPP
#define CONSTRAINTSETEDGE_HPP

#include "datastructures/DCEList.hpp"
#include "geo/Edge.hpp"
#include "math/EvalType.hpp"

#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateEdge;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetEdge:
    public AbstractConstraintSet<T, Edge<T>, Edge<T>, CC>{
  public:
    /**************************************************************************/
    typedef ConstraintCandidateEdge<T, CC> CandidateType;

    /**************************************************************************/
    typedef AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC> Candidate;

    /**************************************************************************/
    typedef typename Map<int, Candidate*>::Iterator CandidateMapIterator;

    /**************************************************************************/
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;

    /**************************************************************************/
    ConstraintSetEdge();

    /**************************************************************************/
    virtual ~ConstraintSetEdge();

    /**************************************************************************/
    virtual bool evaluate(Edge<T>& p,
                          Vector4<T>& com,
                          Quaternion<T>& rot,
                          const EvalType& etype,
                          EvalStats& stats);

    /**************************************************************************/
    virtual bool checkAndUpdate(Edge<T>& p,
                                Vector4<T>& com,
                                Quaternion<T>& rot,
                                EvalStats& stats,
                                bool friction,
                                bool force,
                                T bnorm,
                                bool* allPositive);

    /**************************************************************************/
    virtual void incrementalUpdate(bool onlyForced = false);

    /**************************************************************************/
    virtual void update();

    /**************************************************************************/
    void showEdgeCandidateState(int edge);

    /**************************************************************************/
    void updateCandidateStartPosition(int id,
                                      const Edge<T>& edge,
                                      const Vector4<T>& com,
                                      const Quaternion<T>& rot);

    /**************************************************************************/
    int vertexId[2];
    int faceId[2];
  };
}

#endif/*CONSTRAINTSETEDGE_HPP*/
