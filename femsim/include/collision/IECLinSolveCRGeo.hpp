/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef IECLINSOLVECRGEO_HPP
#define IECLINSOLVECRGEO_HPP

#include "collision/constraints/ContactConstraintCR.hpp"
#include "math/IECLinSolveCR.hpp"
#include "collision/ConstraintSet.hpp"
#include "collision/ConstraintSetFace.hpp"
#include "collision/IECGeo.hpp"
#include "math/constraints/GeneralConstraint.hpp"

namespace tsl{
  double frictionNorm;
  double forceNorm;

  /**************************************************************************/
  template<int N, class T, class CC>
  class IECLinSolveCRGeo : public IECLinSolveCR<N, T, CC>,
                           public IECGeo<T, CC>{
  public:
    /**************************************************************************/
    typedef typename Tree<CC*>::Iterator ConstraintTreeIterator;

    /**************************************************************************/
    IECLinSolveCRGeo(int d,
                     CollisionContext<T, CC>* c):
      IECLinSolveCR<N, T, CC>(d), IECGeo<T, CC>(c){

      /*Register additional vectors*/
      frictionScratch1 = new VectorC<T>(d);
      frictionScratch2 = new VectorC<T>(d);

      this->vectors.append(frictionScratch1);
      this->vectors.append(frictionScratch2);
    }

    /**************************************************************************/
    virtual ~IECLinSolveCRGeo(){
      delete frictionScratch1;
      delete frictionScratch2;
    }

    /**************************************************************************/
    /*Disables all constraints, i.e., calls the disableConstraint
      function of all candidates, which removes the constraints from
      the system.*/
    virtual void disableAllConstraints(){
      this->disableAllCandidates();
    }

    /**************************************************************************/
    /*Checks the validity of the solution by:

      1: checking that all vertices and faces are inside the extended
      bounding volumes. If not, an additional collision check is
      required.

      2: Check if the distances of all constraints are correct, i.e.,
      that the problem is collision free.

      3: If the constraint distances are not correct, they are updated
      by updating the constraint given the current configuration.
    */
    virtual bool checkConstraints(bool friction, EvalStats& stats){
      bool changed1 = false;
      bool changed2 = false;
      int nConstraintsPre = this->mat->getNConstraints();

      /*First check if all candidate lists have all their candidates
        in their bounding boxes.*/
      changed2 = this->checkBoundingVolumes();

      /*Check all constraints if they are properly resolved*/
      changed1 = this->checkGeometry(friction, this->bnorm, stats);

      if(changed2){
        /*Some bounding volumes were not sufficient, so new candidate
          pairs are added. These pairs must be checked further for
          possible collisions*/

        EvalType etype;
        etype.enableGeometryCheck();
        etype.enablePenetrationCheck();
        EvalStats s2;

        /*Evaluate possible new constraints*/
        this->evaluateConstraints(etype, s2);
      }

      if(changed1){
        /*Update Constraint values in b*/
        IECLinSolveCR<N, T, CC>::updateConstraints();
      }

      int nConstraintsApre = this->mat->getNConstraints();

      if(nConstraintsApre != nConstraintsPre){
        /*Constraints have been added, notify solver to update
          preconditioner scaling.*/
        this->constraintSetModified = true;
      }

      return changed1 || changed2;
    }

    /**************************************************************************/
    /*Updates the candidate lists and updates the initial geometric
      information*/
    virtual void updateConstraints(){
      this->updateCandidateLists();
      IECLinSolveCR<N, T, CC>::updateConstraints();
    }

    /**************************************************************************/
    /*Perform an alternative update of the constraints by using the
      local geometry */
    virtual bool evaluateConstraints(const EvalType& etype, EvalStats& stats){
      bool changed = false;

      if(etype.geometryCheck()){
        /*Evaluates all candidate pairs for new collisions*/
        int nConstraintsPre = this->mat->getNConstraints();

        changed = this->evaluateCandidates(etype, stats);

        int nConstraintsApre = this->mat->getNConstraints();

        if(nConstraintsApre != nConstraintsPre){
          this->constraintSetModified = true;
        }
      }else{
        /*Evaluates existing constraints*/
        if(IECLinSolveCR<N, T, CC>::evaluateConstraints(etype, stats)){
          changed = true;
        }
      }

      if(changed){
        /*Update Constraint values in rhs of the system (b)*/
        IECLinSolveCR<N, T, CC>::updateConstraints();
        this->clean_iterations = 0;

        *frictionScratch1 = *this->b2;
      }
      return changed;
    }

    /**************************************************************************/
    /*Adds a constraint to the problem and compute the preconditioner
      entries for the added constraint*/
    virtual int addConstraint(AbstractRowConstraint<N, T>* c){
      int r = IECLinSolveCR<N, T, CC>::addConstraint(c);

      /*Initialize preconditioner*/
      c->computePreconditioner(*this->C);
      c->computePreconditionerMatrix(*this->C,
                                     *this->C2,
                                     *this->mat,
                                     this->constraintConnectivity,
                                     this->preconditionerScaling);
      return r;
    }

    /**************************************************************************/
    /*Computes the scaling of the preconditioner entries of the
      constraints based on their connectivity and similarity.*/
    virtual void computePreconditionerScaling(){
      this->preconditionerScaling->clear();

      for(int i=0;i<this->mat->getNConstraints();i++){
        AbstractRowConstraint<N, T>* c1 =
          (AbstractRowConstraint<N, T>*) this->mat->getActiveConstraint(i);

        T scale = (T)0.0;

        /*Check for general constraint*/
        if(c1->getSubType() == GENERAL_CONSTRAINT_SUBTYPE){
          GeneralConstraint<N, T>* gc = (GeneralConstraint<N, T>*) c1;

          int maxDegree = 0;
          for(int j=0;j<gc->getSize();j++){
            int node = gc->getIndex(j);

            if(node == -1){
              warning("Is this is a constraint with an undefined index");
              c1->print(std::cout);
            }

            int localDegree = this->constraintConnectivity->getDegree(node);

            maxDegree = Max(maxDegree, localDegree);
            (*this->preconditionerScaling)[node * 3] =
              (*this->preconditionerScaling)[node * 3] + (T)1.0;
          }

          scale = (T)maxDegree;

          continue;
        }

        if(c1->getSubType() != CONTACT_CONSTRAINT_SUBTYPE){
          error("unknown subtype, %d", c1->getSubType());
        }

        int nDeformableVertices = 0;
        int nRigidObjects = 0;

        /*Check the amount of rigid and deformable vertices in the
          current constraint*/
        Tree<int> deformableVertices;
        Tree<int> rigidObjects;

        for(int j=0;j<c1->getSize();j++){
          int col = c1->getIndex(j);

          if(col != Constraint::undefined){
            if(col < this->colContext->gridFile->getNGridVertices()){
              deformableVertices.uniqueInsert(col);
            }else{
              rigidObjects.uniqueInsert(col);
            }
          }
        }

        nRigidObjects = rigidObjects.size()/2;
        nDeformableVertices = deformableVertices.size();

        CC* cc1 = (CC*)c1;

        Tree<CC*> connectedConstraints;

        connectedConstraints.uniqueInsert(cc1);

        for(int j=0;j<c1->getSize();j++){
          int col = c1->getIndex(j);

          if(col != Constraint::undefined){
            int nConnectedItems = this->constraintConnectivity->getDegree(col);

            bool rigidObject = false;

            if(col >= this->colContext->gridFile->getNGridVertices()){
              /*Rigid object, skip tensor*/
              j++;
              rigidObject = true;
            }

            if(nRigidObjects != 0 && nDeformableVertices != 0){
              /*Rigid-deformable case */
              /*Instead of checking all the constraints that have the
                same rigid-body id (which can be a lot), search for
                the other deformable ids instead.*/

              if(rigidObject == true){
                continue;
              }
            }

            for(int k=0;k<nConnectedItems;k++){
              int connectedItem =
                this->constraintConnectivity->getConnectedItem(col, k);

              AbstractRowConstraint<N, T>* c2 =
                (AbstractRowConstraint<N, T>*)this->mat->getConstraint(connectedItem);

              if(c2->getValue(1, 0).col == -1){
                /*General constraint, skip*/
                continue;
              }

              if(nRigidObjects != 2 && nDeformableVertices == 0){
                /*Rigid-rigid case*/
                /*All ids must be equal*/
                int objectsCounted = 0;

                for(int l=0;l<c2->getSize();l++){
                  int idx = c2->getIndex(l);

                  if(rigidObjects.find(idx) != rigidObjects.end()){
                    objectsCounted++;
                  }
                }

                if(objectsCounted == 4){
                  /*Two similar object -> 4 identical ids*/
                }else{
                  continue;
                }
              }else if(nRigidObjects != 0 && nDeformableVertices != 0){
                /*Rigid-deformable*/
                /*Rigid id and at least one deformable id must be equal */
                int nFound = 0;
                for(int l=0;l<c2->getSize();l++){
                  if(deformableVertices.find(c2->getIndex(l)) !=
                     deformableVertices.end()){
                    nFound++;
                  }
                }

                if(nFound == 0){
                  continue;
                }
              }else{
                /*Deformable-deformable*/
                /*Depending on the configuration, only one identical
                  id is sufficient.*/
              }

              CC* cc2 = (CC*)c2;

              connectedConstraints.uniqueInsert(cc2);
            }
          }
        }

        ConstraintTreeIterator it = connectedConstraints.begin();

        int nMatches = 0;

        if(nRigidObjects != 0 && nDeformableVertices == 0){
          /*Pure rigid-rigid/static constraint*/

          if(nRigidObjects == 1){
            /*Rigid object with static object, skip*/
            scale = (T)1.0;
          }else{
            /*Rigid - rigid case*/
            int objectA = c1->getIndex(0);
            int objectB = c1->getIndex(2);

            //Vector4<T> n1 = cc1->getRow(0, 0)/this->colContext->getDT();
            //Vector4<T> r1 = cc1->getRow(0, 1)/this->colContext->getDT();

            it = connectedConstraints.begin();

            while(it != connectedConstraints.end()){
              CC* cc = *it++;

              if(cc->status != Active){
                //continue;
              }

              if(objectA == cc->getIndex(0) ||
                 objectA == cc->getIndex(2)){
                if(objectB == cc->getIndex(0) ||
                   objectB == cc->getIndex(2)){
                  DBG(message("found match"));
                  //TODO: measure similarity
                  //if(cc->status == Aactive){
                  /*Check similarity normals*/
#if 0
                  Vector4<T> n2, r2;
                  if(objectA == cc->getIndex(0)){
                    n2 =   cc->getRow(0, 2)/this->colContext->getDT();
                    r2 =   cc->getRow(0, 3)/this->colContext->getDT();
                  }else{
                    n2 =  cc1->getRow(0, 0)/this->colContext->getDT();
                    r2 =  cc1->getRow(0, 1)/this->colContext->getDT();
                  }

                  if( Abs(dot(n1, n2)) > 0.95){
                    //T factor = (T)1.0 - Abs((T)1.0 - Abs(dot(r1, r2))/(r1.length() * r2.length()));
                    //scale += Max(0.0, Min(factor, 1.0));
                  }else{

                  }

                  //Vector4<T> n1 =
                  //cc1->getRow(0, 0)/this->colContext->getDT();
                  //Vector4<T> n2 =
                  //cc->getRow(0, 2)/this->colContext->getDT();

                  //Vector4<T> r1 =
                  //cc1->getRow(0, 1)/this->colContext->getDT();
                  //Vector4<T> r2 =
                  //cc->getRow(0, 3)/this->colContext->getDT();
#endif
                  scale += (T)1.0;
                  //}
                  nMatches++;
                }
              }
            }
          }
        }else if(nRigidObjects != 0 && nDeformableVertices != 0){
          /*Rigid deformable case*/

          /*A rigid / deformable constraint has only one rigid
            object*/
          Tree<int>::Iterator iit = rigidObjects.begin();
          int objectA = *iit++;

          iit = deformableVertices.begin();

          Vector4<T> reference;

          for(int j=0;j<cc1->getSize();j++){
            if(cc1->getIndex(j) == objectA){
              reference = cc1->getRow(0, j);
              reference.normalize();
              break;
            }
          }

          T beps = (T)0.1;

          int verticesA1[3] = {-1,-1,-1};

          T valuesA1[3] = {-1,-1,-1};

          int idxA1 = 0;

          T largest = (T)0.0;
          T localContrib = (T)0.0;

          //Vector4<T> currentWeights;
          //int currentVertexIndices[4];
          //int currentIndex = 0;

          for(int j=0;j<cc1->getSize();j++){
            int idx = cc1->getIndex(j);

            if(idx >= this->colContext->gridFile->getNGridVertices()){
              /*Rigid object*/
              continue;
            }

            if(idx < 0){
              continue;
            }

            Vector4<T> normal = cc1->getRow(0, j) / this->colContext->getDT();

            localContrib = Abs(dot(normal, reference));

            //currentWeights[currentIndex] = localContrib;
            //currentVertexIndices[currentIndex] = cc1->getIndex(j);
            //currentIndex++;

            if(localContrib > largest){
              DBG(principleVertex = cc1->getIndex(j));
              largest = localContrib;
            }

            verticesA1[idxA1] = idx;
            valuesA1[idxA1] = localContrib;

            if(valuesA1[idxA1] < beps){
              verticesA1[idxA1] = -1;
            }

            idxA1++;
          }

          DBG(message("largest = %10.10e, %d", largest, principleVertex));

          /*Find combinations*/
          it = connectedConstraints.begin();

          while(it != connectedConstraints.end()){
            CC* cc = *it++;

            bool objectFound = false;

            if(cc->status != Active){
              //continue;
            }

            int verticesA2[3] = {-1,-1,-1};
            T valuesA2[3] = {-1,-1,-1};

            for(int j=0;j<cc->getSize();j++){
              int col = cc->getIndex(j);

              if(col == objectA){
                objectFound = true;
              }

              for(int k=0;k<3;k++){
                if(verticesA1[k] == col){
                  verticesA2[k] = col;

                  Vector4<T> normal =
                    cc->getRow(0, j) / this->colContext->getDT();

                  valuesA2[k] = Abs(dot(normal, reference));

                  if(valuesA2[k] < beps){
                    verticesA2[k] = -1;
                  }
                }
              }
            }

            int count = 0;

            for(int i=0;i<3;i++){
              for(int j=0;j<3;j++){
                if(verticesA1[i] == verticesA2[j]){
                  count++;
                  break;
                }
              }
            }

            if(count == 3 && objectFound){
              T differenceA = (T)0.0;
              int cnt = 0;
              for(int i = 0;i<3;i++){
                if(valuesA2[i] != (T)-1.0){
                  differenceA += Sqr(valuesA1[i] - valuesA2[i]);
                  cnt++;
                }
              }

              differenceA /= (T)cnt;

              //differenceA = Min((long double)1.0,
              //                Max(Sqrt(differenceA), (long double)0.0));

              differenceA = Clamp01((T)1.0 - Sqrt(differenceA));

              //scale += (((Max((T)0.0, Min((T)1.0 - differenceA, (T)1.0)))));
              //scale += Pow(((long double)1.0 - (differenceA)),
              //           (long double)1.0)*(long double)1.0;

              //scale += (T)1.75*Pow(((differenceA)*(T)1.0),
              //             (T)0.5) * (T)0.5;

              DBG(warning("Scale increment = %10.10e",
                          Pow(((differenceA)*(T)1.0),
                              (T)0.5) * (T)0.5));
              //scale += differenceA / (T)2.0;
              //scale += (Sqr(differenceA))*Abs(differenceA);//(T)0.0;
              //scale += differenceA;
              //scale += (T)1.0;
              DBG(message("difference = %10.10e", differenceA));
            }
          }
        }else{
          /*Deformable deformable*/
          /*Check for deformable / static case*/
          Vector4<T> reference;

          for(int j=0;j<cc1->getSize();j++){
            if(cc1->getIndex(j) != Constraint::undefined){
              reference = cc1->getRow(0, j);
              reference.normalize();
              break;
            }
          }

          bool valid = false;

          for(int j=0;j<c1->getSize();j++){
            if(dot(reference, cc1->getRow(0, j)) < 0){
              /*There is an opposite part in the constraint->
                deformable - deformable case*/
              valid = true;
            }
          }

          if(valid){
            /*Figure out principle vertexA,B*/
            T beps = (T)0.1;

            int verticesA1[3] = {-1,-1,-1};
            int verticesB1[3] = {-1,-1,-1};

            T valuesA1[3] = {-1,-1,-1};
            T valuesB1[3] = {-1,-1,-1};

            int idxA1 = 0;
            int idxB1 = 0;

            T largestA = (T)0.0;
            T largestB = (T)0.0;

            /*Since reference is a normal vector, and normal is
              divided by dt, their dot product is the barycentric
              weight of the corresponding vertex.*/
            T normA = 0.0;
            T normB = 0.0;

            for(int i=0;i<c1->getSize();i++){
              if(cc1->getIndex(i) != -1){
                Vector4<T> normal =
                  cc1->getRow(0, i) / this->colContext->getDT();

                T val = dot(normal, reference);

                if(val > 0){
                  normA += Sqr(val);

                  if(Abs(val) > largestA){
                    largestA = Abs(val);
                    DBG(vertexA = cc1->getIndex(i));
                  }
                  verticesA1[idxA1] = cc1->getIndex(i);
                  valuesA1[idxA1] = val;

                  if(valuesA1[idxA1] < beps){
                    verticesA1[idxA1] = -1; //check this
                  }
                  idxA1++;
                }else{
                  normB += Sqr(val);

                  if(Abs(val) > largestB){
                    largestB = Abs(val);
                    DBG(vertexB = cc1->getIndex(i));
                  }
                  verticesB1[idxB1] = cc1->getIndex(i);
                  valuesB1[idxB1] = -val;

                  if(valuesB1[idxB1] < beps){
                    verticesB1[idxB1] = -1;
                  }
                  idxB1++;
                }
              }
            }

            /*In case of forced constraints, the weights might be out
              of the edge/triangle*/
            normA = Sqrt(normA);
            normB = Sqrt(normB);

            for(int i=0;i<idxA1;i++){
              valuesA1[i] /= normA;
            }

            for(int i=0;i<idxB1;i++){
              valuesB1[i] /= normB;
            }

            ConstraintTreeIterator it = connectedConstraints.begin();

            while(it != connectedConstraints.end()){
              CC* cc = *it++;

              int verticesA2[3] = {-1,-1,-1};
              int verticesB2[3] = {-1,-1,-1};

              T valuesA2[3] = {-1,-1,-1};
              T valuesB2[3] = {-1,-1,-1};

              normA = normB = (T)0.0;
#if 0
              bool vertexAFound = false;
              bool vertexBFound = false;

              T valA = 0, valB = 0;

              int found = 0;
#endif
              if(cc->status != Active){
                //continue;
              }

              for(int i=0;i<cc->getSize();i++){
                int col = cc->getIndex(i);
                /*Check a*/
                for(int j=0;j<3;j++){
                  if(col == verticesA1[j]){
                    verticesA2[j] = col;
                    valuesA2[j] =
                      dot(reference, cc->getRow(0, i) /
                          this->colContext->getDT());

                    normA += Sqr(valuesA2[j]);

                    if(valuesA2[j] < beps){
                      verticesA2[j] = -1;
                    }
                  }

                  if(col == verticesB1[j]){
                    verticesB2[j] = col;
                    valuesB2[j] =
                      -dot(reference, cc->getRow(0, i) /
                           this->colContext->getDT());

                    normB += Sqr(valuesB2[j]);

                    if(valuesB2[j] < beps){
                      verticesB2[j] = -1;
                    }
                  }
                }
              }

              normA = Sqrt(normA);
              normB = Sqrt(normB);

              for(int i=0;i<3;i++){
                if(valuesA2[i] != (T)-1.0){
                  valuesA2[i] /= normA;
                }

                if(valuesB2[i] != (T)-1.0){
                  valuesB2[i] /= normB;
                }
              }

              int count = 0;
              int countA = 0;
              int countB = 0;
              int countUndefA = 0;
              int countUndefB = 0;

              for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                  if(verticesA1[i] == verticesA2[j]){
                    //if(verticesA1[i] != -1){
                      count++;
                      break;
                      //}
                  }
                }
              }

              for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                  if(verticesB1[i] == verticesB2[j]){
                    //if(verticesB1[i] != -1){
                      count++;
                      break;
                      //}
                  }
                }
              }

              for(int i=0;i<3;i++){
                if(verticesA1[i] == -1){
                  countUndefA++;
                }
                if(verticesA2[i] == -1){
                  countUndefA--;
                }
              }

              for(int i=0;i<3;i++){
                if(verticesB1[i] == -1){
                  countUndefB++;
                }
                if(verticesB2[i] == -1){
                  countUndefB--;
                }
              }

              //if(countA == 2 && countB == 2){
              if(count == 6 && countUndefA == 0 && countUndefB == 0){
                T differenceA = (T)0.0;
                T differenceB = (T)0.0;
#if 1
                int countDA = 0;
                for(int i = 0;i<3;i++){
                  if(valuesA2[i] != -(T)1.0){
                    differenceA += Sqr(valuesA1[i] - valuesA2[i]);
                    countDA++;
                  }
                }

                int countDB = 0;
                for(int i = 0;i<3;i++){
                  if(valuesB2[i] != -(T)1.0){
                    differenceB += Sqr(valuesB1[i] - valuesB2[i]);
                    countDB++;
                  }
                }
#else
                for(int i=0;i<3;i++){
                  for(int j=0;j<3;j++){
                    if(verticesA1[i] == verticesA2[j] && verticesA1[i] != -1){
                      differenceA += Sqr(valuesA1[i] * valuesA2[j]);
                    }
                  }
                }

                for(int i=0;i<3;i++){
                  for(int j=0;j<3;j++){
                    if(verticesB1[i] == verticesB2[j] && verticesB1[i] != -1){
                      differenceB += Sqr(valuesB1[i] * valuesB2[j]);
                    }
                  }
                }
#endif

#if 1
                //differenceA = Clamp01((T)1.0 - Sqrt(differenceA));
                //differenceB = Clamp01((T)1.0 - Sqrt(differenceB));
#else
                differenceA = Clamp01((T)Sqrt(differenceA));
                differenceB = Clamp01((T)Sqrt(differenceB));
#endif


#if 1
                T difference = ((T)1.0 - Sqrt((differenceA / ((T) countDA)) +
                                              (differenceB / ((T) countDB))));
#else
                T difference = Clamp01(differenceA + differenceB);
#endif
                scale += difference;

                if(difference > (T)1.0){
                  error("difference too large");
                }
                if(difference < (T)0.0){
                  error("difference too large");
                }
              }else if( (countA == 2 && countB == 0) ||
                        (countA == 0 && countB == 2) ){
              }
            }
          }

          scale = (Max(scale, (T)1.0));
        }

        int cindex = c1->row_id;
        int offset = c1->getOffset();

        T* xdata = this->preconditionerScaling->getExtendedData();

        for(int k=0;k<offset;k++){
          xdata[cindex * offset + k] = (T)(Max(scale, (T)1.0));
        }
      }
    }

    /**************************************************************************/
    /*Evaluation functions at start of new solve*/
    virtual void evaluateStart(EvalStats& stats){
      /*Full evaluation of constraints*/
      EvalType etype;
      etype.enableGeometryCheck();
      etype.enablePenetrationCheck();
      etype.enableFrictionCheck();
      etype.enableEvaluateAllGeometry();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();
      etype.enableVolumeCheck();

      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForcePenetration();

      this->evaluateConstraints(etype, stats);
    }

    /**************************************************************************/
    /*Evaluation when method converges*/
    virtual bool evaluateAtConvergence(EvalStats& stats){
      /*Do not search for new collisions in geometry*/
      EvalType etype;

      etype.enableFrictionCheck();
      //etype.enableGeometryCheck();
      etype.enablePenetrationCheck();
      //etype.enableEvaluateAllGeometry();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();

      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForceVolume();

      etype.enableForcePenetration();

      return this->evaluateConstraints(etype, stats);
    }

    /**************************************************************************/
    /*Evaluation at final convergence, triggers constraint update*/
    virtual bool evaluateAtConvergenceFinal(EvalStats& stats){
      EvalType etype;

      etype.enableFrictionCheck();
      etype.enablePenetrationCheck();
      etype.enableKineticFrictionUpdate();
      etype.enableKineticVectorUpdate();
      etype.enableForceFriction();/*Explicitly set friction*/
      etype.enableForceVolume();

      etype.enableForcePenetration();

      etype.enableGeometryCheck();
      etype.enableEvaluateAllGeometry();

      return this->evaluateConstraints(etype, stats);
    }

    /**************************************************************************/
    /*Regular constraint evaluation*/
    virtual bool evaluateRegular(int iter,
                                 T r1,
                                 T r2,
                                 bool* lastFine,
                                 EvalStats& stats){
      EvalType etype;
      bool check = false;
      *lastFine = false;

      int interval = -1;
      T relative = r2/((T)(DISTANCE_TOL*1.0));

      interval = Max(1, 3*(int)Floor(Log(relative)/(3.0*Log(2.0))));

      if(iter % interval == 0){
        etype.enableKineticVectorUpdate();
        check = true;
        *lastFine = true;

        etype.enableKineticFrictionUpdate();
        etype.enableFrictionCheck();

        check = true;
        *lastFine = true;

        etype.enablePenetrationCheck();
        etype.enableVolumeCheck();
        check = true;
        *lastFine = true;
      }

      if(check){
        currentBNorm = computeRHSNorm();

        this->b2->clear();
        return evaluateConstraints(etype, stats);
      }
      return false;
    }

    /**************************************************************************/
    /*When a state has changed, all kinetic friction constraints
      update the RHS stored in b2.*/
    virtual void evaluateAccept(){
      EvalType etype;
      EvalStats stats;
      etype.enableAcceptFriction();

      this->evaluateConstraints(etype, stats);
      *this->ab2 = *this->b2;
    }

  protected:
    /**************************************************************************/
    T computeRHSNorm(){
      return 1000000000.0;
      T sum = 0;
      int height = this->mat->getHeight();

      spmvc(*frictionScratch1, *this->mat, *this->x, ConstraintsCol);
      VectorC<T>::sub(*frictionScratch1, *frictionScratch1, *this->b2);

      for(int i=0;i<height;i++){
        sum += Sqr((*frictionScratch1)[i]) / (*this->mat)[i][i];
        //forceSum += Sqr((*this->b)[i]) / (*this->mat)[i][i];
      }

      DBG(message("B2 norm = %10.10e", sum));

      frictionNorm = sum;
      //forceNorm = forceSum;

      return sum;
    }

    /**************************************************************************/
    T computeFrictionDifference(){
      return 0;
    }

    /**************************************************************************/
    T currentBNorm;

    VectorC<T>* frictionScratch1;
    VectorC<T>* frictionScratch2;
  };
}

#endif/*IECLINSOLVECRGEO_HPP*/
