/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTCANDIDATETRIANGLEEDGE_HPP
#define CONSTRAINTCANDIDATETRIANGLEEDGE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/Compare.hpp"
#include "geo/Edge.hpp"
#include "collision/ConstraintTol.hpp"
#include "math/EvalType.hpp"
#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetTriangleEdge;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateTriangleEdge :
    public AbstractConstraintCandidate<T, Edge<T>, Vector4<T>, CC>{
  public:
    /**************************************************************************/
    ConstraintCandidateTriangleEdge(int edgeId,
                                    int vertexId,
                                    CollisionContext<T, CC>* ctx);

    /**************************************************************************/
    virtual ~ConstraintCandidateTriangleEdge(){
    }

    /**************************************************************************/
    virtual void recompute(AbstractConstraintSet<T, Edge<T>, Vector4<T>, CC>* c,
                           Edge<T>& p,
                           Vector4<T>& com,
                           Quaternion<T>& rot);

    /**************************************************************************/
    virtual bool checkCollision(const Edge<T>& p,
                                const Vector4<T>& com,
                                const Quaternion<T>& rot,
                                T* dist = 0,
                                bool* plane = 0);

    /**************************************************************************/
    virtual bool evaluate(const Edge<T>& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          Vector4<T>* bb = 0,
                          Vector4<T>* __bb = 0,
                          bool* updated = 0);

    /**************************************************************************/
    virtual void updateGeometry();

    /**************************************************************************/
    virtual void updateConstraint(const Edge<T>& x,
                                  const Edge<T>& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot,
                                  bool* skipped,
                                  EvalStats& stats);

    /**************************************************************************/
    virtual void updateInitial(const Edge<T>& x,
                               const Edge<T>& p,
                               const Vector4<T>& com,
                               const Quaternion<T>& rot,
                               bool* skipped,
                               EvalStats& stats);

    /**************************************************************************/
    virtual bool enableConstraint(const Edge<T>& s,
                                  const Edge<T>& p,
                                  Vector4<T>* gap = 0);

    /**************************************************************************/
    virtual bool disableConstraint();

    /**************************************************************************/
    virtual void updateInitialState(const Edge<T>& x,
                                    const Vector4<T>& com,
                                    const Quaternion<T>& rot,
                                    bool thisSet){
    }

    virtual void showCandidateState(const Edge<T>& s){
    }


    /**************************************************************************/
    Vector4<T> colPoint;
    Vector4<T> baryi;

    Vector4<T> cn;

    bool tangentialDefined;
    Vector4<T> tangentReference[2];
  };

  /**************************************************************************/
  template<class T, class CC>
  class Compare<ConstraintCandidateTriangleEdge<T, CC>* >{
  public:
    static bool less(const ConstraintCandidateTriangleEdge<T, CC>*& a,
                     const ConstraintCandidateTriangleEdge<T, CC>*& b){
      tslassert(a!=0);
      tslassert(b!=0);
      return a->getCandidateId() < b->getCandidateId();
    }

    /**************************************************************************/
    static bool equal(const ConstraintCandidateTriangleEdge<T, CC>*& a,
                      const ConstraintCandidateTriangleEdge<T, CC>*& b){
      tslassert(a!=0);
      tslassert(b!=0);
      return a->getCandidateId() == b->getCandidateId();
    }
  };
}

#endif/*CONSTRAINTCANDIDATETRIANGLEEDGE_HPP*/
