/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONTACTCONSTRAINTCR_HPP
#define CONTACTCONSTRAINTCR_HPP

#include "collision/constraints/ContactConstraint2.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T, class CC>
  class IECLinSolveCRGeo;

  /**************************************************************************/
  template<int N, class T>
  class ContactConstraintCR : public ContactConstraint<N, T>{
  public:
    /**************************************************************************/
    typedef ContactConstraintCR<N, T>                     SelfType;
    typedef IECLinSolveCRGeo<N, T, SelfType >             SolverType;

    /**************************************************************************/
    template<class TT>
    friend class VectorC;

    /**************************************************************************/
    template<int M, class TT>
    friend class SpMatrixC;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolve;

    /**************************************************************************/
    template<int M, class TT, class CC>
    friend class IECLinSolveCR;

    /**************************************************************************/
    static const int interval = 3;

    /**************************************************************************/
    ContactConstraintCR();

    /**************************************************************************/
    virtual T& getConstraintValue(int i);

    /**************************************************************************/
    virtual const T& getConstraintValue(int i)const;

    /**************************************************************************/
    /*Different versions*/
    virtual void forceFriction(Vector4<T>& lm,
                               Vector4<T>& lr,
                               VectorC<T>& x,
                               VectorC<T>& b,
                               bool* changed,
                               bool* frictionChanged,
                               EvalStats& stats);

    /**************************************************************************/
    /*Different versions*/
    virtual void evaluateAndUpdateFriction(Vector4<T>& lm,
                                           Vector4<T>& lr,
                                           VectorC<T>& x,
                                           VectorC<T>& b,
                                           bool* changed,
                                           bool* frictionChanged,
                                           EvalStats& stats);

    /**************************************************************************/
    virtual void update(VectorC<T>& b,
                        VectorC<T>&x,
                        VectorC<T>& b2);

    /**************************************************************************/
    virtual bool valid(const VectorC<T>& v)const;

    /**************************************************************************/
    /*Different versions*/
    virtual void evaluateKineticFriction(Vector4<T>& lm,
                                         Vector4<T>& lr,
                                         VectorC<T>& x,
                                         VectorC<T>& b,
                                         bool updateKinetic,
                                         bool updateVector,
                                         bool* changed,
                                         bool* frictionChanged,
                                         EvalStats& stats);

    /**************************************************************************/
    virtual void evaluateDepth(VectorC<T>& x,
                               VectorC<T>& b,
                               bool evaluatePenetration,
                               Vector4<T>& lm,
                               Vector4<T>& lr,
                               bool* penetrationViolated,
                               bool* changed,
                               bool* active,
                               int height,
                               int row,
                               bool removeDuplicates,
                               EvalStats& stats);

    /**************************************************************************/
    virtual void forceEvaluateDepth(VectorC<T>& x,
                                    VectorC<T>& b,
                                    bool evaluatePenetration,
                                    Vector4<T>& lm,
                                    Vector4<T>& lr,
                                    bool* penetrationViolated,
                                    bool* changed,
                                    bool* active,
                                    int height,
                                    int row,
                                    bool removeDuplicates,
                                    EvalStats& stats);

    /**************************************************************************/
    /*Evaluates Hx - b <=> lambda*/
    virtual bool evaluate(VectorC<T>& x,
                          VectorC<T>& b,
                          T meps,
                          const EvalType& etype,
                          EvalStats& stats,
                          bool* active = 0,
                          VectorC<T>* kb = 0);

    /**************************************************************************/
    virtual void updateRHS(VectorC<T>* kb)const;

    /**************************************************************************/
    virtual void multiply(VectorC<T>& r,
                          const VectorC<T>& x)const;

    /**************************************************************************/
    virtual void multiplyTransposed(VectorC<T>& r, const VectorC<T>& x)const;

    /**************************************************************************/
    virtual void computeFBFunction(VectorC<T>& np,
                                   VectorC<T>& sf,
                                   VectorC<T>& kf,
                                   SpMatrixC<N, T>& C,
                                   const VectorC<T>& x,
                                   const VectorC<T>& b,
                                   const VectorC<T>& b2);

    /**************************************************************************/
    virtual AbstractBlockConstraint<N, T>* constructBlockConstraint()const;

    /**************************************************************************/
    virtual RowConstraintMultiplier<N, T>* constructConstraintMultiplier()const;

    /**************************************************************************/
    virtual void computePreconditioner(VectorC<T>&C)const;

    /**************************************************************************/
    virtual void computePreconditionerMatrix(VectorC<T>& DD,
                                             SpMatrixC<N, T>& C,
                                             const SpMatrixC<N, T>& B,
                                             AdjacencyList* l,
                                             VectorC<T>* scaling = 0)const;

    /**************************************************************************/
    Vector4<T> kineticVector;
    Vector4<T> acceptedKineticVector;
    Vector4<T> cumulativeVector;
    Vector4<T> tangent1;
    Vector4<T> tangent2;
    Vector4<T> normal;
    RunningAverage<T, T, 5> averageNPMagnitude;
    RunningAverage<T, T, 5> averageT1Magnitude;
    RunningAverage<T, T, 5> averageT2Magnitude;
    RunningAverage<T, T, 5> averageFrictionMagnitude;
    RunningAverage<Vector4<T>, T, 5> averageFrictionDirection;
  };

  /**************************************************************************/
  template<int N, class T>
  class Compare<ContactConstraintCR<N, T>*>{
  public:
    /**************************************************************************/
    typedef ContactConstraintCR<N, T>* CCP;

    /**************************************************************************/
    static bool less(const CCP& a, const CCP& b){
      return a->row_id < b->row_id;
    }

    /**************************************************************************/
    static bool equal(const CCP& a, const CCP& b){
      return a->row_id == b->row_id;
    }
  };

  /**************************************************************************/
  /*Configuration templates for this type of constraints*/

  /**************************************************************************/
  template<int N, class T>
  struct HasFriction<ContactConstraintCR<N, T> > : public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct ApproximatesFriction<ContactConstraintCR<N, T> > : public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct HasFrictionCone<ContactConstraintCR<N, T> > : public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct DeactivatesConstraints<ContactConstraintCR<N, T> >: public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct ClampsWeights<ContactConstraintCR<N, T> > : public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct UsesFullPreconditioner<ContactConstraintCR<N, T> > : public TrueType{};

  /**************************************************************************/
  template<int N, class T>
  struct ActiveConstraints<ContactConstraintCR<N, T> > : public FalseType{};
}

#endif/*CONTACTCONSTRAINTCR_HPP*/
