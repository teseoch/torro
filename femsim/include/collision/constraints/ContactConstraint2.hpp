/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONTACTCONSTRAINT_HPP
#define CONTACTCONSTRAINT_HPP

#include "math/constraints/Constraint.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"
#include "math/VectorC.hpp"
#include "math/RunningAverage.hpp"

namespace tsl{
  /**************************************************************************/
#define CONTACT_CONSTRAINT_SUBTYPE 1234

  /**************************************************************************/
  /*Basic class for Contact Constraints. Subclasses are specific for
    specific solver methods*/
  template<int N, class T>
  class RowConstraintMultiplier;

  /**************************************************************************/
  template<int N, class T>
  class ContactConstraint : public AbstractRowConstraint<N, T>{
  public:
    /**************************************************************************/
    template<class TT>
    friend class VectorC;

    /**************************************************************************/
    template<int M, class TT>
    friend class SpMatrixC;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolve;

    /**************************************************************************/
    template<int M, class TT, class CC>
    friend class IECLinSolveCR;

    /**************************************************************************/
    static const int interval = 3;

    /**************************************************************************/
    ContactConstraint();

    /**************************************************************************/
    virtual void preMultiply(VectorC<T>& r, const VectorC<T>& x,
                             const SpMatrixC<N, T>* mat,
                             bool transposed = false,
                             const VectorC<T>* mask = 0)const;

    /**************************************************************************/
    void setMu(T m);

    /**************************************************************************/
    void setRow(const Vector4<T>& r, T fc, int i, int j, int idx);

    /**************************************************************************/
    Vector4<T> getRow(int i, int j)const;

    /**************************************************************************/
    virtual T& getConstraintValue(int i) = 0;

    /**************************************************************************/
    virtual const T& getConstraintValue(int i)const = 0;

    /**************************************************************************/
    int getProjectionType(int l)const{
      if(l==0){
        return 1;
      }
      return 1000 + (int)((T)mu *(T) 1000.0);
    }

    /**************************************************************************/
    ConstraintStatus getStatus()const;

    /**************************************************************************/
    void loadLocalValues(const VectorC<T>& x,
                         const VectorC<T>& b,
                         Vector4<T>& lm, Vector4<T>& lr,
                         Vector4<T>& lc)const;

    /**************************************************************************/
    virtual void forceFriction(Vector4<T>& lm,
                               Vector4<T>& lr,
                               VectorC<T>& x,
                               VectorC<T>& b,
                               bool* changed,
                               bool* frictionChanged,
                               EvalStats& stats) = 0;

    /**************************************************************************/
    virtual void evaluateAndUpdateFriction(Vector4<T>& lm,
                                           Vector4<T>& lr,
                                           VectorC<T>& x,
                                           VectorC<T>& b,
                                           bool* changed,
                                           bool* frictionChanged,
                                           EvalStats& stats) = 0;

    /**************************************************************************/
    virtual void update(VectorC<T>& b, VectorC<T>&x, VectorC<T>& b2) = 0;

    /**************************************************************************/
    virtual bool valid(const VectorC<T>& v)const = 0;

    /**************************************************************************/
    void destroy(VectorC<T>& b, VectorC<T>&x){
    }

    /**************************************************************************/
    /*initialize constraint*/
    void init(SpMatrixC<N, T>* owner);

    /**************************************************************************/
    virtual void evaluateKineticFriction(Vector4<T>& lm,
                                         Vector4<T>& lr,
                                         VectorC<T>& x,
                                         VectorC<T>& b,
                                         bool updateKinetic,
                                         bool updateVector,
                                         bool* changed,
                                         bool* frictionChanged,
                                         EvalStats& stats) = 0;

    /**************************************************************************/
    virtual void evaluateDepth(VectorC<T>& x,
                               VectorC<T>& b,
                               bool evaluatePenetration,
                               Vector4<T>& lm,
                               Vector4<T>& lr,
                               bool* penetrationViolated,
                               bool* changed,
                               bool* active,
                               int height,
                               int row,
                               bool removeDuplicates,
                               EvalStats& stats) = 0;

    /**************************************************************************/
    virtual void forceEvaluateDepth(VectorC<T>& x,
                                    VectorC<T>& b,
                                    bool evaluatePenetration,
                                    Vector4<T>& lm,
                                    Vector4<T>& lr,
                                    bool* penetrationViolated,
                                    bool* changed,
                                    bool* active,
                                    int height,
                                    int row,
                                    bool removeDuplicates,
                                    EvalStats& stats) = 0;

    /**************************************************************************/
    /*Evaluates Hx - b <=> lambda*/
    virtual bool evaluate(VectorC<T>& x,
                          VectorC<T>& b,
                          T meps,
                          const EvalType& etype,
                          EvalStats& stats,
                          bool* active = 0,
                          VectorC<T>* kb = 0) = 0;

    /**************************************************************************/
    bool project(VectorC<T>& x,
                 VectorC<T>& b,
                 T meps,
                 const EvalType& etype,
                 bool* active = 0,
                 VectorC<T>* kb = 0);

    /**************************************************************************/
    virtual void updateRHS(VectorC<T>* kb)const = 0;

    /**************************************************************************/
    virtual void multiply(VectorC<T>& r,
                          const VectorC<T>& x)const = 0;

    /**************************************************************************/
    virtual void multiplyTransposed(VectorC<T>& r,
                                    const VectorC<T>& x)const = 0;

    /**************************************************************************/
    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns, int row,
                       Vector<T>* rhs)const;

    /**************************************************************************/
    void resetMultipliers(VectorC<T>& x, Vector4<T>& oldValues)const;

    /**************************************************************************/
    void cacheMultipliers(Vector4<T>& oldValues, VectorC<T>& x)const;

    /**************************************************************************/
    void print(std::ostream& os)const;

    /**************************************************************************/
    void printIndices(std::ostream& os)const;

    /**************************************************************************/
    virtual void computeFBFunction(VectorC<T>& np,
                                   VectorC<T>& sf,
                                   VectorC<T>& kf,
                                   SpMatrixC<N, T>& C,
                                   const VectorC<T>& x,
                                   const VectorC<T>& b,
                                   const VectorC<T>& b2) = 0;

    /**************************************************************************/
    int getIndividualSize()const{
      return this->getSize()*3;
    }

    /**************************************************************************/
    ConstraintElement<T> getValue(int row, int col)const{
      tslassert(row >=0 && row <= 3);

      int subMatrix = col / 3;
      int matrixCol = col % 3;

      ConstraintElement<T> ret;

      if(index[subMatrix] < 0){
        ret.col = -1;
        ret.value = (T)0.0;
      }else{
        ret.col = index[subMatrix]*3 + matrixCol;
        ret.value = normals[subMatrix][row][matrixCol];
        if(row != 0 && mu == (T)0.0){
          ret.value = 0.0;
        }
      }

      return ret;
    }

    /**************************************************************************/
    virtual void collectElements(List<ConstraintElement<T> >& elements)const;

    /**************************************************************************/
    int getIndex(int i)const;

    /**************************************************************************/
    void setIndex(int i, int idx);

    /**************************************************************************/
    Matrix44<T> normals[5];

    /**************************************************************************/
    Matrix44<T> normalsT[5];


    /**************************************************************************/
    /*The constraint values for the associated constraints. Used to
      evaluate a constraint. After initializing the solver, the values
      stored in c are copied to a particular vector constraint of the
      vector which should contain these values.*/
    Vector4<T>  c;

    /**************************************************************************/
    T mu; /*Friction coefficient*/
    ConstraintFrictionStatus frictionStatus[2];
    T setMultipliers[3];
    T acceptedMultipliers[3];

    int index[5];

    mutable T zero;
    mutable T cache;
    mutable T cacheT1;
    mutable T cacheT2;

    mutable AbstractBlockConstraint<N, T>* blockConstraint;

    mutable T usedScale;
  };

  /**************************************************************************/
  template<int N, class T>
  class Compare<ContactConstraint<N, T>*>{
  public:
    /**************************************************************************/
    typedef ContactConstraint<N, T>* CCP;
    static bool less(const CCP& a, const CCP& b){
      return a->row_id < b->row_id;
    }

    /**************************************************************************/
    static bool equal(const CCP& a, const CCP& b){
      return a->row_id == b->row_id;
    }
  };

  /**************************************************************************/
  /*Indicates constraint type T handles friction*/
  template<typename T>
  struct HasFriction : public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T approximates friction*/
  template<typename T>
  struct ApproximatesFriction : public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T uses friction cone*/
  template<typename T>
  struct HasFrictionCone : public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T deactivates constraints*/
  template<typename T>
  struct DeactivatesConstraints: public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T clamps weights*/
  template<typename T>
  struct ClampsWeights : public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T uses full preconditioner*/
  template<typename T>
  struct UsesFullPreconditioner : public FalseType{};

  /**************************************************************************/
  /*Indicates constraint type T is always active*/
  template<typename T>
  struct ActiveConstraints : public FalseType{};
}

#endif/*CONTACTCONSTRAINT_HPP*/
