/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONTACTBLOCKCONSTRAINT_HPP
#define CONTACTBLOCKCONSTRAINT_HPP

#include "math/constraints/Constraint.hpp"
#include "math/constraints/AbstractBlockConstraint.hpp"
#include "math/Matrix44.hpp"

namespace tsl{
  /**************************************************************************/
  template<int N, class T>
  class ContactBlockConstraint :
    public AbstractBlockConstraint<N, T>{
  public:
    /**************************************************************************/
    template<class TT>
    friend class VectorC;

    /**************************************************************************/
    template<int M, class TT>
    friend class SpMatrixC;

    /**************************************************************************/
    template<int M, class TT>
    friend class IECLinSolve;

    /**************************************************************************/
    template<int M, class TT, class CC>
    friend class IECLinSolveCR;

    /**************************************************************************/
    template<int M, class TT>
    friend class ContactConstraint;

    /**************************************************************************/
    template<int M, class TT>
    friend class ContactConstraintCR;

    /**************************************************************************/
    template<int M, class TT>
    friend class ContactConstraintGS;

    /**************************************************************************/
    template<int M, class TT>
    friend class ContactConstraintSP;

    /**************************************************************************/
    ContactBlockConstraint(bool full);

    /**************************************************************************/
    virtual ~ContactBlockConstraint(){
    }

    /**************************************************************************/
    virtual void collectElements(List<ConstraintElement<T> >& elements)const{
    }

    /**************************************************************************/
    virtual void multiplyAHSHA(VectorC<T>& r,
                               const VectorC<T>& x,
                               const SpMatrixC<N, T>* mat,
                               bool transposed,
                               const VectorC<T>* mask)const;

    /**************************************************************************/
    void generalMultiply(VectorC<T>& r,
                         const VectorC<T>& x,
                         bool transposed = false)const;

    /**************************************************************************/
    /*Performs a multiplication r = Q x*/
    virtual void multiply(VectorC<T>& r,
                          const VectorC<T>& x)const;

    /**************************************************************************/
    /*Performs a multiplication r = Q^T x*/
    virtual void multiplyTransposed(VectorC<T>& r,
                                    const VectorC<T>& x)const;

    /**************************************************************************/
    void setIndex(int i,
                  int idx){
      tslassert(i>=0);
      tslassert(i<5);
      index[i] = idx;
    }

    /**************************************************************************/
    int getIndex(int i)const{
      tslassert(i>=0);
      tslassert(i<5);
      return index[i];
    }

    /**************************************************************************/
    int getProjectionType(int l)const{
      return 0;
    }

    /**************************************************************************/
    void print(std::ostream& os)const{
      os << "Block constraint" << std::endl;
    }

    /**************************************************************************/
    ConstraintStatus getStatus()const{
      return this->status;
    }

    /**************************************************************************/
    void destroy(VectorC<T>&b,
                 VectorC<T>& x){
      //tslassert(this->indexMap.size() == 0);
      //tslassert(this->indices.size() == 0);
    }

    /**************************************************************************/
    void extractValues(SpMatrix<N, T>* L,
                       SpMatrix<N, T>* LT,
                       Tree<int>* columns,
                       int row,
                       Vector<T>* rhs)const{
    }

    /**************************************************************************/
  protected:
    int index[5];
    Matrix44<T> S;
    Matrix44<T> OS;
    Matrix44<T> SHA[5];
    Matrix44<T> AHS[5];
    Matrix44<T> HA[5];
    Matrix44<T> AHT[5];

    Matrix44<T> SHAT[5];
    Matrix44<T> AHST[5];
    Matrix44<T> HAT[5];

    const bool fullPreconditioner;
  };
}

#endif/*CONTACTBLOCKCONSTRAINT_HPP*/
