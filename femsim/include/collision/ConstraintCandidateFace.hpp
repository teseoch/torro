/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTCANDIDATEFACE_HPP
#define CONSTRAINTCANDIDATEFACE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/Compare.hpp"
#include "geo/Triangle.hpp"
#include "collision/ConstraintTol.hpp"
#include "math/EvalType.hpp"

#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSet;

  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  enum CollisionOrientation{FrontFaceCollision,
                            BackFaceCollision,
                            UndefinedCollisionOrientation};

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateFace:
    public AbstractConstraintCandidate<T, OrientedVertex<T>, Triangle<T>, CC>{
  public:
    /**************************************************************************/
    ConstraintCandidateFace(CollisionContext<T, CC>* n,
                            int f,
                            int v,
                            bool b);

    /**************************************************************************/
    virtual ~ConstraintCandidateFace(){
    }

    /**************************************************************************/
    /*Recompute geometric properties of constraint*/
    virtual void recompute(AbstractConstraintSet<T, OrientedVertex<T>,
                           Triangle<T>, CC>* c,
                           OrientedVertex<T>& p,
                           Vector4<T>& com,
                           Quaternion<T>& rot);

    /**************************************************************************/
    /*Check if there is an intersection */
    virtual bool checkCollision(const OrientedVertex<T>& p,
                                const Vector4<T>& com,
                                const Quaternion<T>& rot,
                                T* dist = 0,
                                bool* plane = 0);

    /**************************************************************************/
    virtual bool evaluate(const OrientedVertex<T>& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          Vector4<T>* bb1 = 0,
                          Vector4<T>* bb2 = 0,
                          bool* updated = 0);

    /**************************************************************************/
    virtual void updateGeometry();

    /**************************************************************************/
    virtual void updateConstraint(const OrientedVertex<T>& x,
                                  const OrientedVertex<T>& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot,
                                  bool* skipped,
                                  EvalStats& stats);

    /**************************************************************************/
    virtual void updateInitial(const OrientedVertex<T>& x,
                               const OrientedVertex<T>& p,
                               const Vector4<T>& com,
                               const Quaternion<T>& rot,
                               bool* skipped,
                               EvalStats& stats);

    /**************************************************************************/
    virtual bool enableConstraint(const OrientedVertex<T>& s,
                                  const OrientedVertex<T>& p,
                                  Vector4<T>* gapOveride = 0);

    /**************************************************************************/
    virtual bool disableConstraint();

    /**************************************************************************/
    bool enableSlidedConstraint(Vector4<T> gap,
                                ConstraintSet<T, CC>* c,
                                OrientedVertex<T> p,
                                Vector4<T> com,
                                Quaternion<T> rot);

    /**************************************************************************/
    virtual void showCandidateState(const OrientedVertex<T>& xu);

    /**************************************************************************/
    virtual void updateInitialState(const OrientedVertex<T>& v,
                                    const Vector4<T>& com,
                                    const Quaternion<T>& rot,
                                    bool thisSet);

    /**************************************************************************/
    Vector4<T> baryi; /*Interpolated barycentric coordinates*/
    Vector4<T> bary2; /*Barycentric coordinates at current iteration*/

    Vector4<T> gap;/*Distance from initial config to point of
                     contact. Used to compute tangential gaps.*/

    int indices[3];

    /*Indices of edges creating this triangle*/
    int edgeIndices[3];

    //List<int> edgeConstraints;

    bool updated;

    bool tangentialDefined;
    Vector4<T> tangentReference[2];

    //CollisionOrientation orientation;

    bool vertexCollapsed;
  };

  /**************************************************************************/
  template<class T, class CC>
  class Compare<ConstraintCandidateFace<T, CC>*>{
  public:
    /**************************************************************************/
    static bool less(const ConstraintCandidateFace<T, CC>*& a,
                     const ConstraintCandidateFace<T, CC>*& b){
      return a->getCandidateId() < b->getCandidateId();
    }

    /**************************************************************************/
    static bool equal(const ConstraintCandidateFace<T, CC>*& a,
                      const ConstraintCandidateFace<T, CC>*& b){
      return a->getCandidateId() == b->getCandidateId();
    }
  };
}

#endif/*CONSTRAINTCANDIDATEFACE_HPP*/
