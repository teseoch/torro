/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINTCANDIDATEEDGE_HPP
#define CONSTRAINTCANDIDATEEDGE_HPP

#include "math/constraints/Constraint.hpp"
#include "datastructures/DCEList.hpp"
#include "math/Compare.hpp"
#include "geo/Edge.hpp"
#include "collision/ConstraintTol.hpp"
#include "math/EvalType.hpp"

#include "collision/AbstractConstraintSet.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetEdge;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintCandidateEdge:
    public AbstractConstraintCandidate<T, Edge<T>, Edge<T>, CC>{
  public:
    /**************************************************************************/
    ConstraintCandidateEdge(int eid,
                            int oeid,
                            bool backFace,
                            CollisionContext<T, CC>* ctx);

    /**************************************************************************/
    virtual ~ConstraintCandidateEdge(){
    }

    /**************************************************************************/
    virtual void recompute(AbstractConstraintSet<T, Edge<T>, Edge<T>, CC>* c,
                           Edge<T>& p,
                           Vector4<T>& com,
                           Quaternion<T>& rot);

    /**************************************************************************/
    /*p = current, x = start*/
    virtual bool checkCollision(const Edge<T>& p,
                                const Vector4<T>& com,
                                const Quaternion<T>& rot,
                                T* dist = 0,
                                bool* plane = 0);

    /**************************************************************************/
    /*Evaluate signed distance*/
    virtual bool evaluate(const Edge<T>& edge,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          Vector4<T>* bb1 = 0,
                          Vector4<T>* bb2 = 0,
                          bool* updated = 0);

    /**************************************************************************/
    /*Update position of edge*/
    virtual void updateGeometry();

    /**************************************************************************/
    virtual bool enableConstraint(const Edge<T>& s,
                                  const Edge<T>& p,
                                  Vector4<T>* gap = 0);

    /**************************************************************************/
    virtual void updateConstraint(const Edge<T>& x,
                                  const Edge<T>& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot,
                                  bool* skipped,
                                  EvalStats& stats);

    /**************************************************************************/
    virtual void updateInitial(const Edge<T>& x,
                               const Edge<T>& p,
                               const Vector4<T>& com,
                               const Quaternion<T>& rot,
                               bool* skipped,
                               EvalStats& stats);

    /**************************************************************************/
    void updateInactiveConstraint(ConstraintSetEdge<T, CC>* c,
                                  const Edge<T>& x,
                                  const Edge<T>& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot);

    /**************************************************************************/
    virtual bool disableConstraint();

    /**************************************************************************/
    virtual void showCandidateState(const Edge<T>& pu);

    /**************************************************************************/
    virtual void updateInitialState(const Edge<T>& edge,
                                    const Vector4<T>& com,
                                    const Quaternion<T>& rot,
                                    bool thisSet);

    /**************************************************************************/
    bool hasIntersectedBefore;


    Vector4<T> gap;/*Distance from initial config to point of
                     contact. Used to compute tangential gaps.*/

    Vector4<T> colPoint; /*Projected collision point on this edge*/

    //int indices[2];

    Vector4<T> baryi1;   /*Barycentric coordinates of colPoint*/
    Vector4<T> baryi2;   /*Barycentric coordinates of colPoint on subject edge*/

    Vector4<T> dv; /**/
    Vector4<T> refXy; /*Reference distance vector used for correcting
                        signs X-Y*/
    Vector4<T> refYx;

    Vector4<T> refYx0; /*Reference at enabling*/

    bool anyReferenceSet; /*True if referencedv's are set in previous steps.*/

    bool tangentialDefined;
    Vector4<T> tangentReference[2];

    bool fixed;

    Vector4<T> cn;/*Used when updating the constraint, E->P*/

    bool collapsed;
  };

  /**************************************************************************/
  template<class T, class CC>
  class Compare<ConstraintCandidateEdge<T, CC>* >{
  public:
    static bool less(const ConstraintCandidateEdge<T, CC>*& a,
                     const ConstraintCandidateEdge<T, CC>*& b){
      tslassert(a!=0);
      tslassert(b!=0);
      return a->getCandidateId() < b->getCandidateId();
    }

    /**************************************************************************/
    static bool equal(const ConstraintCandidateEdge<T, CC>*& a,
                      const ConstraintCandidateEdge<T, CC>*& b){
      tslassert(a!=0);
      tslassert(b!=0);
      return a->getCandidateId() == b->getCandidateId();
    }
  };
}

#endif/*CONSTRAINTCANDIDATEEDGE_HPP*/
