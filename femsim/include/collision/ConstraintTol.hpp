/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CONSTRAINT_TOL_HPP
#define CONSTRAINT_TOL_HPP

/*Moved to Collision Context*/
#define SAFETY_DISTANCE (1e-8)
#define DISTANCE_EPSILON (10*2e-6)//5e-6/*2e-5 3e-5*1*//*7e-6*/
#define DISTANCE_TOL (10*1e-6)//5e-6/*1e-5 2e-5*/

#define MU 0.5

/*At the moment only HV_EPS is used in smoothing the friction vector*/
#define JV_EPS  1e-6*1
#define LAMBDA_EPS 1e-8*1
#define HV_EPS  1e-6*1
#define GAMMA_EPS -1e-7*0

#define BARY_EPS 1e-6
#define EDGE_DIST 1e-5

#endif/*CONSTRAINT_TOL_HPP*/
