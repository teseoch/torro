/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef COLLISIONCONTEXT_HPP
#define COLLISIONCONTEXT_HPP

#include "geo/GridFile.hpp"
#include "datastructures/SurfaceTree.hpp"
#include "datastructures/DCEList.hpp"
#include "datastructures/List.hpp"

namespace tsl{
  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSet;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetFaces;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetEdge;

  /**************************************************************************/
  template<class T, class CC>
  class ConstraintSetTriangleEdge;

  /**************************************************************************/
  template<int N, class T>
  class SpMatrixC;

  /**************************************************************************/
  template<int N, class T>
  class IECLinSolve;
  /**************************************************************************/
  /*The collision context class contains all datastructures and
    functions for quering the geometry for (potential) collisions.

    It contains for each feature in the geometry a list of nearby
    potentially colliding features. In case of a collision, the
    potential collisions initialize constraints and add them to the
    solvers.
  */
  template<class T, class CC>
  class CollisionContext{
  public:
    /**************************************************************************/
    CollisionContext():gridFile(0), stree(0), mesh(0), mat(0), solver(0),
                       sets(0), backSets(0), edgeSets(0),
                       faceFaceSets(0), v(0), bboxVertex(0),
                       bboxEdge(0), dt((T)0){

      distance_tol      = (T)1e-5;
      distance_epsilon  = (T)2e-5;
      min_edge_distance = (T)1e-3;
      safety_distance   = (T)1e-8;
      total_epsilon     = safety_distance + distance_epsilon;
    }

    /**************************************************************************/
    CollisionContext(GridFile<T>* _gridFile,
                     DCTetraMesh<T>* _mesh,
                     IECLinSolve<2, T>* _solver,
                     T _dt):gridFile(_gridFile),
                            mesh(_mesh),
                            solver(_solver),
                            dt(_dt){

      distance_tol      = (T)1e-5;
      distance_epsilon  = (T)2e-5;
      min_edge_distance = (T)1e-3;
      safety_distance   = (T)1e-8;
      total_epsilon     = safety_distance + distance_epsilon;
    }

    void setSolver(IECLinSolve<2, T>* _solver){
      solver = _solver;
      mat = solver->getMatrix();
      v   = solver->getx();
    }

    void initialize(){

      stree  = new SurfaceTree<T>(mesh);
      stree->constructTree();

      int nVertices = mesh->getNVertices();

      sets = new ConstraintSet<T, CC>[nVertices];
      for(int i=0;i<nVertices;i++){
        sets[i].setContext(this);
        sets[i].setSetId(i);
        mesh->getOrientedVertex(&sets[i].getStartPosition(), i, false);
        sets[i].setType(mesh->vertices[i].type);
      }

      backSets = new ConstraintSet<T, CC>[nVertices];
      for(int i=0;i<nVertices;i++){
        backSets[i].setContext(this);
        backSets[i].setSetId(i);
        mesh->getOrientedVertex(&backSets[i].getStartPosition(), i, true);
        backSets[i].setType(mesh->vertices[i].type);
        backSets[i].setBackFace();
      }

      edgeSets = new ConstraintSetEdge<T, CC>[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        mesh->setCurrentEdge(i);
        edgeSets[i].setContext(this);
        edgeSets[i].setSetId(i);
        edgeSets[i].vertexId[0]   = mesh->getOriginVertex();
        edgeSets[i].vertexId[1]   = mesh->getTwinOriginVertex();
        edgeSets[i].faceId[0]     = mesh->getFace();
        edgeSets[i].faceId[1]     = mesh->getTwinFace();
        edgeSets[i].setType(mesh->halfEdges[i].type);
        mesh->getEdge(&edgeSets[i].getStartPosition(), i, false);
      }

      backEdgeSets = new ConstraintSetEdge<T, CC>[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        mesh->setCurrentEdge(i);
        backEdgeSets[i].setContext(this);
        backEdgeSets[i].setSetId(i);
        backEdgeSets[i].vertexId[0]   = mesh->getOriginVertex();
        backEdgeSets[i].vertexId[1]   = mesh->getTwinOriginVertex();
        backEdgeSets[i].faceId[0]     = mesh->getFace();
        backEdgeSets[i].faceId[1]     = mesh->getTwinFace();
        backEdgeSets[i].setBackFace();
        backEdgeSets[i].setType(mesh->halfEdges[i].type);
        mesh->getEdge(&backEdgeSets[i].getStartPosition(), i, true);
      }

      faceFaceSets = new ConstraintSetFaces<T, CC>[mesh->getNHalfFaces()];
      for(int i=0;i<mesh->getNHalfFaces();i++){
        faceFaceSets[i].faceId = i;
        faceFaceSets[i].setContext(this);
      }

      triangleEdgeSets =
        new ConstraintSetTriangleEdge<T, CC>[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        triangleEdgeSets[i].setContext(this);
        triangleEdgeSets[i].setSetId(i);
        triangleEdgeSets[i].setType(mesh->halfEdges[i].type);
      }

      collapsedEdges     = new bool[mesh->getNHalfEdges()];
      collapsedBackEdges = new bool[mesh->getNHalfEdges()];

      for(int i=0;i<mesh->getNHalfEdges();i++){
        collapsedEdges[i]     = false;
        collapsedBackEdges[i] = false;
      }

      initializeBBoxes();
    }

    /**************************************************************************/
    ~CollisionContext(){
      delete stree;

      deleteBBoxes();

      delete [] sets;
      delete [] backSets;
      delete [] edgeSets;
      delete [] backEdgeSets;
      delete [] faceFaceSets;
      delete [] triangleEdgeSets;
      delete [] collapsedEdges;
      delete [] collapsedBackEdges;
    }

    /**************************************************************************/
    /*Extracts all potential colliding vertex-face pairs*/
    void extractPotentialVertexFace(int vertex,
                                    List<int>& faces,
                                    bool internal);

    /**************************************************************************/
    /*Extracts all potential colliding edge-edge pairs*/
    void extractPotentialEdgeEdges(int edge,
                                   List<int>& edges,
                                   bool internal);

    /**************************************************************************/
    /*Extracts all potential colliding edge-vertex pairs*/
    void extractPotentialEdgeVertices(int edge,
                                      Tree<int>& vertices);

    /**************************************************************************/
    /*Initializes the bounding boxes and extended bounding boxes of
      the features*/
    void initializeBBoxes();

    /**************************************************************************/
    /*Deletes all bounding boxes*/
    void deleteBBoxes();

    /**************************************************************************/
    /*Updates all bounding boxes*/
    void updateBBoxes(bool keepCurrentBoxes = false);

    /**************************************************************************/
    /*Checks if a vertex has moved outside its extended bounding
      box. If so, a collision of this vertex might be
      missed. By finding these cases we can explicily test them.*/
    void checkBBoxesVertex(List<int>* invalidVertices);

    /**************************************************************************/
    /*Checks if an edge has moved outside its extended bounding
      box. If so, a collision of this edge might be
      missed. By finding these cases we can explicily test them.*/
    void checkBBoxesEdge(List<int>* invalidEdges);

    /**************************************************************************/
    void updateStartEdge(int indexA,
                         int indexB,
                         const Edge<T>& edgeA,
                         const Vector4<T>& comA,
                         const Quaternion<T>& rotA,
                         const Edge<T>& edgeB,
                         const Vector4<T>& comB,
                         const Quaternion<T>& rotB,
                         bool backEdge);

    /**************************************************************************/
    /*GridFile contains the description of the whole scene*/
    GridFile<T>*         gridFile;

    /*SurfaceTree is a BVH of all surface triangles existing in the scene*/
    SurfaceTree<T>*      stree;

    /*DCTetraMesh is a doubly connected edge list of the FEM mesh +
      surface meshes of the complete geometry*/
    DCTetraMesh<T>*      mesh;

    /*The enriched sparse matrix containing the linear problem of the
      simulation and the constraints due to collisions detected and
      additional constraints specified in the gridFile.*/
    SpMatrixC<2, T>*    mat;

    /*The solver that is used to solve the problem stored in Matrix mat*/
    IECLinSolve<2, T>*   solver;

    /*Vertex-Face potential constraint set (front-facing geometry)*/
    ConstraintSet<T, CC>*             sets;

    /*Vertex-Face potential constraint set (back-facing geometry)*/
    ConstraintSet<T, CC>*             backSets;

    /*Edge-Edge potential constraint set (front-facing geometry)*/
    ConstraintSetEdge<T, CC>*         edgeSets;

    /*Edge-Edge potential constraint set (back-facing geometry)*/
    ConstraintSetEdge<T, CC>*         backEdgeSets;

    /*Face-Face potential constraint set (front and back-facing geometry)*/
    ConstraintSetFaces<T, CC>*        faceFaceSets;

    /*Triangle-Edge potential constraint set (front and back-facing geometry)*/
    ConstraintSetTriangleEdge<T, CC>* triangleEdgeSets;

    /*Pointer to velocity vector, used to compute displacements and
      extended bounding boxes*/
    Vector<T>*  v;

    /*Additional bounding boxes of vertices and edges (stored as 2 vector4)*/
    Vector4<T>* bboxVertex;
    Vector4<T>* bboxEdge;

    /*In order to check quickly if an edge is collapsed*/
    bool* collapsedEdges;
    bool* collapsedBackEdges;

    T dt;

    /**************************************************************************/
    T getDT()const{
      return dt;
    }

    /**************************************************************************/
    void setDT(T d){
      dt = d;
    }

    /**************************************************************************/
    SpMatrixC<2, T>* getMatrix()const{
      return mat;
    }

    /**************************************************************************/
    IECLinSolve<2, T>* getSolver()const{
      return solver;
    }

    /**************************************************************************/
    /*Debug interface*/
    void showStateVertexFace(int vertex,
                             int face,
                             bool backFace);

    /**************************************************************************/
    void showStateEdgeEdge(int edgeA,
                           int edgeB,
                           bool backFace);

    /**************************************************************************/
    void showStateEdgeVertex(int edge, int vertex);

    /*The allowed constraint error, this shouls also be set in the
      solver*/
    T distance_tol;

    /*The epsilon added to the initial distance. This distance should
      be larger than distance_tol. If a constraint has an error of +-
      distance_tol, the distance can still be negative, causing
      problems in future collision detection steps. By adding an
      epsilon larger than distance_tol to the initial problem, the
      final solution should always be positive.*/
    T distance_epsilon;

    /*Minimum distance between vertex and edge of the same triangle*/
    T min_edge_distance;

    /*Minimum distance respected by the root finders*/
    T safety_distance;

    /*Sum of safety_distance and distance_epsilon*/
    T total_epsilon;
  };
}

#endif/*COLLISIONCONTEXT_HPP*/
