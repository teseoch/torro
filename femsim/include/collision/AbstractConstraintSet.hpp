/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef ABSTRACTCONSTRAINTSET_HPP
#define ABSTRACTCONSTRAINTSET_HPP

#include "datastructures/DCEList.hpp"
#include "collision/ConstraintCandidateStatus.hpp"
#include "datastructures/Tree.hpp"
#include "datastructures/Map.hpp"
#include "math/constraints/AbstractRowConstraint.hpp"

namespace tsl{
#if 0
    typedef struct _indices{
    union {
      struct edge{ /*Constraint set edge*/
        int vertexIds[2];
        int faceIds[2];
        int edgeId;
      };
      struct vertex{
        int vertexId;
      };
      struct face{
        int faceId;
        int vertexIds[3];
        int edgeIds[3];
      };
    }CandidateIndices;
  };
#endif

  /**************************************************************************/
  template<class T, class ST, class CT, class CC>
  class AbstractConstraintSet;

  /**************************************************************************/
  template<class T, class CC>
  class CollisionContext;

  /**************************************************************************/
  template<class T, class ST, class CT, class CC>
  class AbstractConstraintCandidate{
  public:
    /**************************************************************************/
    AbstractConstraintCandidate(bool b, CollisionContext<T, CC>* _ctx){
      distance0 = distancec = distance00 = (T)0.0;
      status = Disabled;
      backFace = b;
      forced = false;
      intersection0 = intersectionc = false;
      valid = true;
      flag = false;
      ctx = _ctx;
    }

    /**************************************************************************/
    virtual ~AbstractConstraintCandidate(){
    }

    /**************************************************************************/
    int getCandidateId()const{
      return candidateId;
    }

    /**************************************************************************/
    CandidateStatus getStatus()const{
      return status;
    }

    /**************************************************************************/
    void setStatus(CandidateStatus s){
      status = s;
    }

    /**************************************************************************/
    int getEnabledId()const{
      return enabledId;
    }

    /**************************************************************************/
    void setEnabledId(int id){
      enabledId = id;
    }

    /**************************************************************************/
    T getCurrentDistance(){
      return distancec;
    }

    /**************************************************************************/
    void forceConstraint(){
      this->forced = true;
      warning("forcing constraint - backface = %d", this->backFace);
      //warning("forcing constraint e %d - e %d - backface = %d",
      //        edgeId, otherEdgeId, this->backFace);
    }

    /**************************************************************************/
    void setInitial(CT& Y){
      Y0 = Y;
    }

    /**************************************************************************/
    CT& getInitial(){
      return Y0;
    }

    /**************************************************************************/
    CT& getCurrent(){
      return Yu;
    }

    /**************************************************************************/
    bool isIntersecting()const{
      return intersectionc;
    }

    /**************************************************************************/
    void setCandidateType(Mesh::GeometryType _candidateType){
      candidateType = _candidateType;
    }

    /**************************************************************************/
    void setSetType(Mesh::GeometryType _setType){
      setType = _setType;
    }

    /**************************************************************************/
    bool isValid()const{
      return valid;
    }

    /**************************************************************************/
    void setValid(bool b){
      valid = b;
    }

    /**************************************************************************/
    bool isFlagged()const{
      return flag;
    }

    /**************************************************************************/
    void setFlag(bool f){
      flag = f;
    }

    /**************************************************************************/
    virtual void recompute(AbstractConstraintSet<T, ST, CT, CC>* c,
                           ST& p,
                           Vector4<T>& com,
                           Quaternion<T>& rot) = 0;

    /**************************************************************************/
    virtual bool checkCollision(const ST& p,
                                const Vector4<T>& com,
                                const Quaternion<T>& rot,
                                T* dist = 0,
                                bool* plane = 0) = 0;

    /**************************************************************************/
    virtual bool evaluate(const ST& edge,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          Vector4<T>* bb1 = 0,
                          Vector4<T>* bb2 = 0,
                          bool* updated = 0) = 0;

    /**************************************************************************/
    virtual void updateGeometry() = 0;

    /**************************************************************************/
    virtual void updateConstraint(const ST& x,
                                  const ST& p,
                                  const Vector4<T>& com,
                                  const Quaternion<T>& rot,
                                  bool* skipped,
                                  EvalStats& stats) = 0;

    /**************************************************************************/
    virtual void updateInitial(const ST& x,
                               const ST& p,
                               const Vector4<T>& com,
                               const Quaternion<T>& rot,
                               bool* skipped,
                               EvalStats& stats) = 0;

    /**************************************************************************/
    virtual bool enableConstraint(const ST& s,
                                  const ST& p,
                                  Vector4<T>* gapOveride = 0) = 0;

    /**************************************************************************/
    virtual bool disableConstraint() = 0;

    /**************************************************************************/
    virtual void updateInitialState(const ST& x,
                                    const Vector4<T>& com,
                                    const Quaternion<T>& rot,
                                    bool thisSet) = 0;

    /**************************************************************************/
    virtual void showCandidateState(const ST& pu) = 0;
  protected:
    /**************************************************************************/
    int candidateId;
    int setId;

    T distance0;   /*Distance at begin of timestep*/
    T distancec;   /*Distance at current iteration*/
    T distance00;

    bool intersection0; /*Intersection state at begin of timestep*/
    bool intersectionc; /*Intersection state at current iteration*/

    ST X0; /*Initial entity X (Set entity)*/
    ST X00;/*Initial entity X*/
    ST Xi; /*Linear interpolated entity between X0 and Xu*/

    CT Y0; /*Initial entity Y (This candidate)*/
    CT Y00;/*Initial entity Y*/
    CT Yu; /*Entity at current iteration*/
    CT Yi; /*Linear interpolated entity between Y0 and Yu with
             distance 0*/

    ST enabledXu;
    CT enabledYu;
    ST enabledX0;
    CT enabledY0;

    Vector4<T>    comX0;
    Quaternion<T> rotX0;
    Vector4<T>    comY0;
    Quaternion<T> rotY0;
    Vector4<T>    comX00;
    Quaternion<T> rotX00;
    Vector4<T>    comY00;
    Quaternion<T> rotY00;
    Vector4<T>    comYu;
    Quaternion<T> rotYu;

    Vector4<T>    comXi;
    Quaternion<T> rotXi;
    Vector4<T>    comYi;
    Quaternion<T> rotYi;

    /*Only if ActiveConstraints == true*/
    Vector4<T> cachedMultipliers;

    CandidateStatus status; /*Enabled or disabled*/
    int enabledId; /*Id of constraint in solver*/
    bool backFace;

    bool forced;

    bool valid;

    bool flag;

    Mesh::GeometryType candidateType;
    Mesh::GeometryType setType;

    CollisionContext<T, CC>* ctx;
  };

  /**************************************************************************/
  template<class T, class ST, class CT, class CC>
  class AbstractConstraintSet{
  public:
    /**************************************************************************/
    typedef AbstractConstraintCandidate<T, ST, CT, CC> Candidate;
    typedef typename Map<int, Candidate*>::Iterator CandidateMapIterator;
    typedef typename Tree<Candidate*>::Iterator CandidateTreeIterator;

    /**************************************************************************/
    AbstractConstraintSet(){
      backFace = false;
      typeName = "AbstractConstraintCandidate - AbstractConstraintCandidate";
    }

    /**************************************************************************/
    virtual ~AbstractConstraintSet(){
      CandidateMapIterator it = candidates.begin();

      while(it != candidates.end()){
        Candidate* c = (*it++).getData();
        delete c;
      }
      candidates.clear();
    }

    /**************************************************************************/
    void setContext(CollisionContext<T, CC>* _ctx){
      ctx = _ctx;
    }

    /**************************************************************************/
    void setSetId(int i){
      setId = i;
    }

    /**************************************************************************/
    int getSetId()const{
      return setId;
    }

    /**************************************************************************/
    void setBackFace(){
      backFace = true;
    }

    /**************************************************************************/
    void setType(Mesh::GeometryType _type){
      type = _type;
    }

    /**************************************************************************/
    Mesh::GeometryType getType()const{
      return type;
    }

    /**************************************************************************/
    ST& getStartPosition(){
      return startPosition;
    }

    /**************************************************************************/
    int getForcedPotentialsSize()const{
      return forcedPotentials.size();
    }

    /**************************************************************************/
    virtual void incrementalUpdate(bool onlyForced = false) = 0;

    /**************************************************************************/
    virtual void update() = 0;

    /**************************************************************************/
    virtual bool evaluate(ST& p,
                          Vector4<T>& com,
                          Quaternion<T>& rot,
                          const EvalType& etype,
                          EvalStats& stats) = 0;

    /**************************************************************************/
    virtual bool checkAndUpdate(ST& p,
                                Vector4<T>& com,
                                Quaternion<T>& rot,
                                EvalStats& stats,
                                bool friction,
                                bool force,
                                T bnorm,
                                bool* allPositive) = 0;

    /**************************************************************************/
    virtual void deactivateAll(){
      CandidateMapIterator it = candidates.begin();

      while(it != candidates.end()){
        Candidate* c = (*it++).getData();
        if(c->getStatus() == Enabled){
          c->disableConstraint();
        }
      }
    }

    /**************************************************************************/
    int getNCandidates()const{
      return candidates.size();
    }

    /**************************************************************************/
    Map<int, Candidate*>& getCandidates(){
      return candidates;
    }

    /**************************************************************************/
    void forceConstraint(int candidateId){
      CandidateMapIterator it = candidates.begin();

      warning("forcing %s %d, %d, backface %d",
              typeName, setId, candidateId, backFace);

      while(it != candidates.end()){
        Candidate* candidate = (*it++).getData();

        if(candidate->getCandidateId() == candidateId){
          candidate->forceConstraint();
          return;
        }
      }

      warning("request for forcing a constraint for which no candidate exists!! Added to hotlist.");
      forcedPotentials.uniqueInsert(candidateId, candidateId);

      incrementalUpdate(true);

      it = candidates.begin();
      while(it != candidates.end()){
        Candidate* candidate = (*it++).getData();

        if(candidate->getCandidateId() == candidateId){
          candidate->forceConstraint();
          return;
        }
      }
    }

    /**************************************************************************/
    /*Called by IECGeo::checkGeometry after checkAndUpdate for all
      sets return false, i.e., no constraints need to be updated.

      checkAndUpdate calls basicUpdateCheck, which (In
      NonAcitveConstraintMode) calls updateConstraint in case the
      distance is off for active constraints. Candidate pairs that
      have a positive distance and an inactive constraint are not
      updated. Pairs with active constraints that have a distance
      mismatch are updated. This check doesn't remove constraints for
      which the collision point is off the edge of face. In those
      cases the constraints are updated, but they are not reported as
      updated.

      Once there are no constraints reported that have updated, the
      second stage is performed by calling updateInitial for each
      set. For faces updateInitial does NOT update the initial state,
      but it finds a neighboring face that can take over the
      contact/constraint through enableSlidedConstraint if the crossed
      edge is not already active with another edge connected to the
      sliding vertex.

      For edges the initial state is updated, and constraints are
      disabled if the contact point slides of the edge.
    */
    virtual bool updateInitial(ST& p,
                               Vector4<T>& com,
                               Quaternion<T>& rot,
                               EvalStats& stats,
                               bool friction, bool force,
                               T bnorm){
      CandidateMapIterator it = this->candidates.begin();

      /*if true, the solver is notified*/
      bool constraintsChanged = false;

      /*Simple plane point check for a fast detection in change*/
      while(it != this->candidates.end()){
        Candidate* c = (*it++).getData();

#if 0
        CC* cc = (CC*)ctx->getMatrix()->getConstraint(c->enabledId);

        /*Update geometry information of associated faces*/
        try{
          c->updateGeometry();
        }catch(Exception* e){
          warning("Exception updateGeometry 1 %s", e->getError().c_str());
          error("exception!!");
          delete e;
          cc->status = Inactive;
          c->setValid(false);
        if(c->status == Enabled){
          c->disableConstraint();
        }
        continue;
        }

        if(!c->isValid()){
          if(c->status == Enabled){
            error("Invalid candidate is enabled");
          }
          continue;
        }
#endif

        this->startPosition = p;
        this->startCenterOfMass = com;
        this->startOrientation = rot;

        bool skipped = false;

        c->updateInitial(this->startPosition, p, com, rot, &skipped, stats);

        if(skipped){
          message("vf constraint updated");
          constraintsChanged = true;
        }
      }
      return constraintsChanged;
    }

    /**************************************************************************/
    void basicEvaluation(Tree<Candidate*>& changedIndices,
                         const EvalType& etype,
                         EvalStats& stats,
                         bool& constraintsChanged,
                         const bool disableInactive,
                         const bool activeConstraintsMode){
      CandidateMapIterator it = candidates.begin();

      while(it != candidates.end()){
        Candidate* cc = (*it++).getData();

        if(changedIndices.findIndex(cc) == -1){
          if(cc->getStatus() == Enabled){

            AbstractRowConstraint<2, T>* ccc =
              (AbstractRowConstraint<2, T>*)
              ctx->getMatrix()->getConstraint(cc->getEnabledId());

            bool active = false;

            bool changed = ccc->evaluate(*ctx->getSolver()->getx(),
                                         *ctx->getSolver()->getb(),
                                         0, etype, stats, &active,
                                         ctx->getSolver()->getb2());

            if(changed && active && etype.penetrationCheck()){
              /*If a constraint has been changed, but still active,
                update preconditioner*/

              ccc->computePreconditioner(*ccc->preconditioner);
              constraintsChanged = true;
            }

            /*Disable (remove) constraint if allowed*/
            if(activeConstraintsMode){
              /*Keep constraint*/
            }else{
              if(active == false && etype.penetrationCheck()){
                if(disableInactive){
                  constraintsChanged = true;
                  //message("not active");
                  cc->disableConstraint();
                }
              }
            }

            /*Notifies solver*/
            if(changed){
              constraintsChanged = true;
            }
          }
        }else{
          //Evaluate new constraint in order to update RHS
          if(cc->getStatus() == Enabled){

            AbstractRowConstraint<2, T>* ccc =
              (AbstractRowConstraint<2, T>*)
              ctx->getMatrix()->getConstraint(cc->getEnabledId());

            EvalType etype2;
            bool active = false;

            ccc->evaluate(*ctx->getSolver()->getx(),
                          *ctx->getSolver()->getb(),
                          0, etype2, stats, &active,
                          ctx->getSolver()->getb2());
          }
        }
      }
    }

    /**************************************************************************/
    void basicRootFinding(const ST& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          Tree<Candidate*>& changedCandidates,
                          Tree<Candidate*>& changedIndices,
                          bool& constraintsChanged,
                          const bool activeConstraintMode){
      CandidateTreeIterator it2 = changedCandidates.begin();
      /*Find closest colliding constraint*/

#if 1
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

        if(activeConstraintMode){
          if(c->getStatus() == Enabled){
            //continue;
          }
        }else{
          if(c->getStatus() == Enabled){
            continue;
          }
        }

        T dist = 10000.0;

        if(c->checkCollision(p, com, rot, &dist)){
          DBG(message("collision"));
          if(c->getStatus() != Enabled){
            DBG(message("enabling constraint"));
            if(c->enableConstraint(this->startPosition, p)){
              constraintsChanged = true;
              changedIndices.insert(c, 1);
            }
          }
        }
      }
#else
      /*Figure out the ´closest´ intersection point. This basically
        turns out to be the closest root.*/
      Candidate* closestCandidate = 0;
      T closestRoot = (T)100000.0;
      while(it2 != changedCandidates.end()){
        Candidate* c = *it2++;

        if(activeConstraintMode){
        }else{
          if(c->status == Enabled){
            continue;
          }
        }

        T root = (T)0.0;
        if(c->checkCollision(p, com, rot, &root)){
          if(c->status != Enabled){
            if(root < closestRoot){
              root = closestRoot;
              closestCandidate = c;
            }
          }
        }
      }
      if(closestCandidate){
        if(closestCandidate->enableConstraint(this->startPosition, p)){
          constraintsChanged = true;
          changedIndices.insert(closestCandidate, 1);
        }
      }
#endif
    }

    /**************************************************************************/
    void updateStartPositions(const ST& v,
                              const Vector4<T>& com,
                              const Quaternion<T>& rot,
                              Candidate* c,
                              bool updateSet){
      this->startPosition = v;
      this->startCenterOfMass = com;
      this->startOrientation = rot;

      //c->updateInitialState(v, com, rot, updateSet);
#if 1
      CandidateMapIterator it = this->candidates.begin();
      while(it != this->candidates.end()){
        Candidate* candidate = (*it++).getData();
        candidate->updateInitialState(v, com, rot, updateSet);
      }
#endif
      warning("%s UPDATING INITIAL STATE!!!\n\n\n\n\n\n\n\n\n\n\n\n\n",
              typeName);
      //exit(0);
    }


    bool basicUpdateCheck(AbstractConstraintCandidate<T, ST, CT, CC>* c,
                          const ST& p,
                          const Vector4<T>& com,
                          const Quaternion<T>& rot,
                          const bool offGeo,
                          const bool activeConstraintsMode,
                          bool& constraintsChanged,
                          bool* allPositive,
                          EvalStats& stats,
                          const T eps,
                          const T tol,
                          const T sdist,
                          const T f1, const T f2,
                          const T f3, const T f4){
      CC* cc = (CC*)ctx->getMatrix()->getConstraint(c->getEnabledId());

#if 1
      (warning("updating %s constraint, distance = %10.10e, %10.10e, %10.10e",
               typeName,
               c->getCurrentDistance(),
               c->getCurrentDistance() - eps,
               c->getCurrentDistance() - tol));
      if(activeConstraintsMode){
#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->getCurrentDistance() > sdist){
            /*Constraint is not active, distance is positive*/
            return false;//continue;
          }
        }else{
          //if(c->getCurrentDistance() -
          //(T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
          if(c->getCurrentDistance() < (eps + tol) &&
             c->getCurrentDistance() > tol){
            /*Constraint is active, distance is positive*/
            return false;//continue;
          }
        }
#else
        if(!cc->valid(*ctx->getSolver()->getx())){
          if(c->getCurrentDistance() > eps * 2.0){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            c->disableConstraint();
            return false;//continue;
          }
        }
#endif//0
      }else{//!activeConstraintsMode
        if(cc->status != Active){
          /*Non-Active constrain*/
          if(c->getCurrentDistance() > eps * f1){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            //c->disableConstraint();
            //continue;
          }

          if(c->getCurrentDistance() > eps * f2){
            DBG(message("skip: distancec > 0"));
            /*Constraint is not active, distance is positive.*/
            if(!offGeo){
              //continue;
            }
          }
        }else{
          /*Active constrain*/
          if(c->getCurrentDistance() - eps + tol > (T)0.0){
            /*Constraint is active, distance is positive.*/
            if(!offGeo){
              //continue;
            }
          }
          if(c->getCurrentDistance() - eps - (T)5 * tol > (T)0.0){
            /*Constraint is active, distance is positive.*/
            if(!offGeo){
              //continue;
            }
          }
          if(Abs(c->getCurrentDistance() - eps) < tol){
            if(!offGeo){
              //continue;
            }
          }
        }

#if 0
        if(!cc->valid(*ctx->getSolver()->getx())){
          DBG(message("not valid"));
          if(c->getCurrentDistance() > sdist){
            /*Constraint is not active or multiplier is very small,
              distance is positive*/
            DBG(message("skip: distancec > 0"));
            return false;//continue;
          }
        }else{
          //if(c->getCurrentDistance() -
          //(T)DISTANCE_EPSILON + (T)DISTANCE_TOL > (T)0.0){
          //  continue;
          //}
          if(Abs(c->getCurrentDistance() - eps) < tol){
            return false;//continue;
          }
        }

#endif//0
      }//activeConstraintsMode
#endif//1
      /*Geometric distance and constraint distance are not in sync, update*/

      /*Update constraint does check validity!*/
      bool skipped = false;
      try{
        //c->updateConstraint(this->startPosition, p, com, rot, &skipped);

        if(activeConstraintsMode){
          if(!cc->valid(*ctx->getSolver()->getx())){
            if(c->getCurrentDistance() > eps * 1.0){
              return false;//continue;
            }
          }else{
            if(c->getCurrentDistance() - eps + tol > (T)0.0){
              /*Constraint is active, distance is positive.*/
              return false;//continue;
            }
            if(c->getCurrentDistance() - eps - f3 * tol > (T)0.0){
              /*Constraint is active, distance is positive.*/
              return false;//continue;
            }
          }
        }else{//!activeConstraintsMode
          if(cc->status != Active){
            if(c->getCurrentDistance() < sdist
               /*|| c->getCurrentDistance() > DISTANCE_EPSILON +DISTANCE_TOL*/){
              //c->disableConstraint();
              //constraintsChanged = true;
              //continue;
            }

            if(offGeo){
              //c->disableConstraint();
              //continue;
            }

            if(c->getCurrentDistance() > eps * 10.0 &&
               !c->isIntersecting()){
              /*Constraint is not active, distance is positive.*/
              if(!offGeo){
                //c->disableConstraint();
                //continue;
              }else{
                //continue;
              }
            }
            if(c->getCurrentDistance() > eps - tol * f4 &&
               !c->isIntersecting()){
              if(offGeo){
                //c->disableConstraint();
              }
              //if(!offGeo){
              return false;//continue;
                //}
            }else{
              if(!offGeo){
                /*Not active, on edge, too close, activate constraint*/
                //cc->status = Active;
              }
            }
          }

          if(cc->status == Active){
#if 0
            if(c->getCurrentDistance() - eps + tol > (T)0.0){
              /*Constraint is active, distance is positive.*/
              if(!offGeo){
                return false;//continue;
              }
            }
            if(c->getCurrentDistance() - eps - (T)1*tol > (T)0.0){
              /*Constraint is active, distance is positive.*/
              if(!offGeo){
                return false;//continue;
                }
            }
#endif
            if(c->getCurrentDistance() < sdist){
              //c->disableConstraint();
              //continue;
            }

            if(c->getCurrentDistance()
               /*- (T)DISTANCE_EPSILON + (T)DISTANCE_TOL*/ > (T)10.0 * sdist
               &&
               c->getCurrentDistance() - (T)1*eps - (T)1*tol < (T)0.0){
              /*Constraint is active, distance is positive.*/
              if(!offGeo){
                return false;//continue;
              }
              if(offGeo){
                //c->disableConstraint();
                return false;//continue;
              }
            }
          }
        }//activeConstraintsMode

        c->updateConstraint(this->startPosition, p, com, rot, &skipped,
                            stats);

        if(skipped){
          /*But trigger a new update*/
          //constraintsChanged = true;
          return false;//continue;
        }

        if(c->getCurrentDistance() < eps){
          *allPositive = false;
        }

#if 0
        if(cc->status != Active){
          cc->status = Active;
          cc->frictionStatus[0] = Static;
          cc->frictionStatus[1] = Static;
        }
#endif
        message("Updated and enabled EE constraint");
      }catch(DegenerateCaseException* e){
        message("disabled due to exception in updateConstraint");
        c->disableConstraint();
        std::cerr << e->getError();
        //error("Exception occured while updating constraint");

        delete e;
        return false;//continue;
      }

      constraintsChanged = true;

      DBG(message("%s CONSTRAINT NOT CORRECT %d, %d\n\n\n",
                  typeName,
                  this->setId, c->getCandidateId()));
      DBG(message("real distance = %10.10e",
                  c->getCurrentDistance() + sdist));
      (warning("%s CONSTRAINT NOT CORRECT %d, %d, backface = %d\n\n\n",
               typeName,
               this->setId, c->getCandidateId(), this->backFace));
      (warning("real distance = %10.10e",
               c->getCurrentDistance() + sdist));
      return true;
    }

    /**************************************************************************/
  protected:
    int setId;
    bool backFace;

    Mesh::GeometryType type;

    ST            startPosition;
    Vector4<T>    startCenterOfMass;
    Quaternion<T> startOrientation;
    ST            lastPosition;
    Vector4<T>    lastCenterOfMass;
    Quaternion<T> lastOrientation;

    Tree<int> candidatesTree;     /*Stores face ids of candidates*/
    Tree<int> forcedPotentials;

    /*List of faces close by vertex lastPosition*/
    Map<int, Candidate*> candidates;
    const char*   typeName;

    CollisionContext<T, CC>* ctx;
  };
}

#endif/*ABSTRACTCONSTRAINTSET_HPP*/
