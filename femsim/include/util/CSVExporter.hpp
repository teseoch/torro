/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef CSVEXPORTER_HPP
#define CSVEXPORTER_HPP

#include "math/Math.hpp"
#include "core/tsldefs.hpp"
#include "stdio.h"
#include <map>
#include <string>

namespace tsl{
  /**************************************************************************/
  /*Class for creating a comma separated file consisting of a number
    of named columns and a number of rows. Once all values are set,
    saveRow will write all values to the assigned file.*/
  class CSVExporter{
  public:
    /**************************************************************************/
    CSVExporter(FILE* f);

    /**************************************************************************/
    ~CSVExporter();

    /**************************************************************************/
    void addColumn(const char* columnName);

    /**************************************************************************/
    void setValue(const char* column, float value);

    /**************************************************************************/
    void setValue(const char* column, double value);

    /**************************************************************************/
    void setValue(const char* column, uint value);

    /**************************************************************************/
    void setValue(const char* column, int value);

    /**************************************************************************/
    void setValue(const char* column, long value);

    /**************************************************************************/
    void setValue(const char* column, ulong value);

    /**************************************************************************/
    void setValue(const char* column, const char* value);

    /**************************************************************************/
    void saveHeader();

    /**************************************************************************/
    void saveRow();

    /**************************************************************************/
    void clear(){
      n_columns = 0;
      valueMap.clear();
      indexMap.clear();
    }
  protected:
    /**************************************************************************/
    FILE* file;
    bool headerWritten;
    std::map<std::string, std::string> valueMap;
    std::map<int, std::string> indexMap;
    int n_columns;
  };
}

#endif/*CSVEXPORTER_HPP*/
