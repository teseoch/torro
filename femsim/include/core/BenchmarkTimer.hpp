/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef BENCHMARKTIMER_HPP
#define BENCHMARKTIMER_HPP

#include "core/tsldefs.hpp"
#include <map>
#include <string>

namespace tsl{
  /**************************************************************************/
  typedef struct _timer{
    struct timeval total_time;
    struct timeval last_time;
    int n;
    int state;
  }timer;

  /**************************************************************************/
  /*BenchmarkTimer provides functionality for timing different parts
    of the same (iterative) algorithm. It provider a set of named
    timers and keeps track of the number of timed instances.*/
  class BenchmarkTimer{
  public:
    /**************************************************************************/
    BenchmarkTimer(){
      timeMap.clear();
    }

    /**************************************************************************/
    void start(const char* timerName);

    /**************************************************************************/
    void stop(const char* timerName);

    /**************************************************************************/
    long getAverageTimeUSec(const char* timerName);

    /**************************************************************************/
    long getTotalTimeUSec(const char* timerName);

    /**************************************************************************/
    long getTotalCalls(const char* timerName);

    /**************************************************************************/
    long getAccumulativeUSec();

    /**************************************************************************/
    void printAverageUSec(const char* timerName);

    /**************************************************************************/
    void printTotalUSec(const char* timerName);

    /**************************************************************************/
    void printAllAverageUSec();

    /**************************************************************************/
    void printAllTotalUSec();

    /**************************************************************************/
    void printAccumulativeUSec();

    /**************************************************************************/
    void resetTimers(){
      timeMap.clear();
    }

    /**************************************************************************/
  protected:
    std::map<std::string, timer>  timeMap;

  private:
    /**************************************************************************/
    BenchmarkTimer(const BenchmarkTimer&);

    /**************************************************************************/
    BenchmarkTimer& operator=(const BenchmarkTimer&);
  };

#ifdef BENCHMARK
#define TIME(timer, name, call)     \
  timer.start(name);                \
  call;                             \
  timer.stop(name);
#else
#define TIME(timer, name, call)                 \
  call;
#endif
}

#endif/*BENCHMARKTIMER_HPP*/
