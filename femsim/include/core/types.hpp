/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef TYPES_HPP
#define TYPES_HPP

namespace tsl{

  /*Templates for determining variable types at compiletime*/
  /**************************************************************************/
  template<class T, T v>
  struct IntegralConstant{
    static const T value = v;
    typedef IntegralConstant<T, v> type;
    operator T(){
      return value;
    }
  };

  /**************************************************************************/
  typedef IntegralConstant<bool, false> FalseType;
  typedef IntegralConstant<bool, true > TrueType;

  /**************************************************************************/
  template<typename T> struct IsIntegralType               : public FalseType{};
  template<> struct IsIntegralType<bool>                   : public TrueType{};
  template<> struct IsIntegralType<char>                   : public TrueType{};
  template<> struct IsIntegralType<int>                    : public TrueType{};
  template<> struct IsIntegralType<short int>              : public TrueType{};
  template<> struct IsIntegralType<long int>               : public TrueType{};
  template<> struct IsIntegralType<long long int>          : public TrueType{};

  /**************************************************************************/
  template<> struct IsIntegralType<unsigned char>          : public TrueType{};
  template<> struct IsIntegralType<unsigned int>           : public TrueType{};
  template<> struct IsIntegralType<unsigned short int>     : public TrueType{};
  template<> struct IsIntegralType<unsigned long int>      : public TrueType{};
  template<> struct IsIntegralType<unsigned long long int> : public TrueType{};

  template<> struct IsIntegralType<signed char>          : public TrueType{};
  //template<> struct IsIntegralType<signed int>           : public trueType{};
  //template<> struct IsIntegralType<signed short int>     : public trueType{};
  //template<> struct IsIntegralType<signed long int>      : public trueType{};
  //template<> struct IsIntegralType<signed long long int> : public trueType{};

  /**************************************************************************/
  template<typename T> struct IsSignedIntegralType         : public FalseType{};
  template<> struct IsSignedIntegralType<signed char>      : public TrueType{};
  template<> struct IsSignedIntegralType<signed int>       : public TrueType{};
  template<> struct IsSignedIntegralType<signed short int> : public TrueType{};
  template<> struct IsSignedIntegralType<signed long int>  : public TrueType{};
  template<> struct IsSignedIntegralType<signed long long int> : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsUnsignedIntegralType               : public FalseType{};
  template<> struct IsUnsignedIntegralType<bool>                   : public TrueType{};
  template<> struct IsUnsignedIntegralType<unsigned char>          : public TrueType{};
  template<> struct IsUnsignedIntegralType<unsigned int>           : public TrueType{};
  template<> struct IsUnsignedIntegralType<unsigned short int>     : public TrueType{};
  template<> struct IsUnsignedIntegralType<unsigned long int>      : public TrueType{};
  template<> struct IsUnsignedIntegralType<unsigned long long int> : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsRealType    : public FalseType{};
  template<> struct IsRealType<float>       : public TrueType{};
  template<> struct IsRealType<double>      : public TrueType{};
  template<> struct IsRealType<long double> : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsFloatType   : public FalseType{};
  template<> struct IsFloatType<float>      : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsDoubleType  : public FalseType{};
  template<> struct IsDoubleType<double>    : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsLongDoubleType    : public FalseType{};
  template<> struct IsLongDoubleType<long double> : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsPointerType            : public FalseType{};
  template<typename T> struct IsPointerType<T*>        : public TrueType{};

  /**************************************************************************/
  template<typename T> struct IsPointerPointerType     : public FalseType{};
  template<typename T> struct IsPointerPointerType<T**>: public TrueType{};
}

#endif/*TYPES_HPP*/
