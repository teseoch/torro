/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef DAEMON_HPP
#define DAEMON_HPP

namespace tsl{
  /**************************************************************************/
  /*Functions for creating daemon processes. Depending on the type of
    initialization, a process runs as a normal process in a console,
    or creates a deamon process which is detached from the console. In
    case of a daemon process, its stdout and stderr filestreams may be
    redirected to a file.*/
  int continue_as_daemon_process();

  /**************************************************************************/
  int is_daemon();

  /**************************************************************************/
  void redirect_std_file_descriptors();

  /**************************************************************************/
  void redirect_std_file_descriptors_null();

}

#endif/*DAEMON_HPP*/
