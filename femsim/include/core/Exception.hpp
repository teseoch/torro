/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include "core/tsldefs.hpp"
#include <string>

namespace tsl{
  /**************************************************************************/
  /*General exception class*/
  class Exception{
  public:
    /**************************************************************************/
    Exception(const int l, const char* f, const char* m):
      line(l), file(f), msg(m){
    };

    /**************************************************************************/
    virtual ~Exception(){
    };

    /**************************************************************************/
    virtual const std::string getError() const = 0;
  protected:
    /**************************************************************************/
    const int line;
    const char* file;
    const char* msg;
  };

  /**************************************************************************/
  class TimeOutException : public Exception{
  public:
    /**************************************************************************/
    TimeOutException(const int l, const char* f, const char* m=0):
      Exception(l, f, m){
    };

    /**************************************************************************/
    virtual ~TimeOutException(){
    };

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class FileNotFoundException : public Exception{
  public:
    /**************************************************************************/
    FileNotFoundException(const int l, const char* f, const char* m=0):
      Exception(l, f, m){
    };

    /**************************************************************************/
    virtual ~FileNotFoundException(){
    };

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class CUDAException : public Exception{
  public:
    /**************************************************************************/
    CUDAException(const int l, const char* f, const char* m=0):
      Exception(l, f, m){
    }

    /**************************************************************************/
    virtual ~CUDAException(){
    }

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class ThreadPoolException : public Exception{
  public:
    /**************************************************************************/
    ThreadPoolException(const int l, const char* f, const char* m=0):
      Exception(l, f, m){
    }

    /**************************************************************************/
    virtual ~ThreadPoolException(){
    }

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class MathException : public Exception{
  public:
    /**************************************************************************/
    MathException(const int l, const char* f, const char* m=0):
      Exception(l, f, m){
    }

    /**************************************************************************/
    virtual ~MathException(){}

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class SolutionNotFoundException : public MathException{
  public:
    /**************************************************************************/
    SolutionNotFoundException(const int l, const char* f, const char* m=0):
      MathException(l, f, m){
    }

    /**************************************************************************/
    virtual ~SolutionNotFoundException(){}

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class AlgException : public Exception{
  public:
    /**************************************************************************/
    AlgException(const int l, const char* f, const char* m = 0):
      Exception(l, f, m){
    }

    /**************************************************************************/
    virtual ~AlgException(){}

    /**************************************************************************/
    const std::string getError() const;
  };

  /**************************************************************************/
  class DegenerateCaseException : public AlgException{
  public:
    /**************************************************************************/
    DegenerateCaseException(const int l, const char* f, const char* m = 0):
      AlgException(l, f, m){
    }

    /**************************************************************************/
    virtual ~ DegenerateCaseException(){}

    /**************************************************************************/
    const std::string getError() const;
  };
}

#endif/*EXCEPTION_HPP*/
