/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef TSLDEFS_H
#define TSLDEFS_H

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifndef NULL
#define NULL 0
#endif

#define OPENGL_3

#define DEFAULT_BLOCK_SIZE 8

#ifdef _WIN32
#include <Windows.h>
typedef unsigned int uint;
typedef unsigned long ulong;
#undef max
#undef min

/**************************************************************************/
/*Define gettimeofday, timeradd and timersub functions for windows*/
int gettimeofday(struct timeval * tp, struct timezone * tzp);

#define timeradd(a, b, result)                           \
  do {                                                   \
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;        \
    (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;     \
    if ((result)->tv_usec >= 1000000)                    \
      {                                                  \
        ++(result)->tv_sec;                              \
        (result)->tv_usec -= 1000000;                    \
      }                                                  \
  } while (0)
#define timersub(a, b, result)                            \
  do {                                                    \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;         \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;      \
    if ((result)->tv_usec < 0) {                          \
      --(result)->tv_sec;                                 \
      (result)->tv_usec += 1000000;                       \
    }                                                     \
  } while (0)
#else
#include <sys/time.h>
#include <sys/types.h>
#endif

#include <limits.h>

#include <stdlib.h>

namespace tsl{
  /**************************************************************************/
#if defined __ANDROID__
  typedef unsigned long ulong;
#endif
  typedef unsigned char uchar;

  /**************************************************************************/
  /*Assert statement is enabled when _DEBUG flag is enabled*/
#ifdef _DEBUG
#define tslassert(exp)   ((exp)?((void)0):(void)CGF::_tslassert(#exp, __FILE__, __LINE__))
#else
#define tslassert(exp)   ((void)0)
#endif

  /**************************************************************************/
  /*Initialization types for applications*/
  enum TSLInit{Console, Daemon, DaemonNullRedirected};

  class ThreadPool;

  /**************************************************************************/
  /*Global functions*/
  extern void init(TSLInit type = Console);

  /**************************************************************************/
  extern void destroy();

  /**************************************************************************/
  extern ThreadPool* getThreadPool(int i);

  /**************************************************************************/
  extern void warning(const char* format = "", ... );

  /**************************************************************************/
  extern void message(const char* format = "", ... );

  /**************************************************************************/
  extern void error(const char* format = "", ... );

  /**************************************************************************/
  extern void _tslassert(const char* expr,
                         const char* filename,
                         int line);

  /**************************************************************************/
  extern void usleep(int n);

  /**************************************************************************/
  /*Templated Swap functions*/
  template<class T>
  void swap(T** a, T** b){
    T* tmp = *a;
    *a = *b;
    *b = tmp;
  }

  /**************************************************************************/
  template<class T>
  void swap(T* a, T* b){
    T tmp = *a;
    *a = *b;
    *b = tmp;
  }

  /**************************************************************************/
  /*Malloc functions for aligned memory allocation*/
  inline void alignedMalloc(void** p, size_t align, size_t size){
#ifdef _WIN32
    *p = _aligned_malloc(size, align);
#else
    int r = posix_memalign(p, align, size);
    if(r != 0){
      error("POSIX_MEMALIGN_ERROR %d", r);
    }
#endif
  }

  /**************************************************************************/
  /*Free function to free aligned memory*/
  inline void alignedFree(void* p){
#ifdef _WIN32
    _aligned_free(p);
#else
    free(p);
#endif
  }

  /**************************************************************************/
  /*Checks if p is (align)-bytes aligned*/
  inline bool alignment(const void* p, unsigned int align){
#ifdef _DEBUG
    const unsigned long addr = (unsigned long)p;
    if(addr % align == 0){
      return true;
    }
    warning("pointer %p not %d bytes aligned, offset = %d", p, align, addr%align);
    return false;
#else
    return true;
#endif
  }

  /**************************************************************************/
  const char* getCurrentDir(char* buff, uint size);

  /**************************************************************************/
  /*Debug macros*/
  /*Print function name*/
#ifdef _DEBUG
#ifdef _WIN32
#define PRINT_FUNCTION message("%s", __FUNCTION__);
#else
#define PRINT_FUNCTION message("%s", __PRETTY_FUNCTION__);
#endif
#else
#define PRINT_FUNCTION
#endif

  /**************************************************************************/
  /*Prints a value or object, and its name*/
#ifdef _DEBUG
#define PRINT(x)                    \
  message("%s =>", #x);             \
  std::cout << (x) << std::endl;
#else
#define PRINT(x)
#endif

  /**************************************************************************/
  /*Debug macros*/
#ifdef _DEBUG
#define DBG(x) x;
#else
#define DBG(x)
#endif

#ifdef _DEBUG
#define START_DEBUG
#else
#define START_DEBUG if(false){
#endif

#ifdef _DEBUG
#define END_DEBUG
#else
#define END_DEBUG }
#endif

#ifndef CUDA
#define NO_CUDA
#endif

}

#endif/*TSLDEFS_H*/
