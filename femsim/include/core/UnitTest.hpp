/* Copyright (C) 2012-2019 by Mickeal Verschoor*/

#ifndef UNITTEST_HPP
#define UNITTEST_HPP
#include "core/tsldefs.hpp"

namespace tsl{

  /**************************************************************************/
  /*Interface for register and run testcode. Test functions can be
    defined in (for example) cpp files of the corresponding class.*/

  /**************************************************************************/
  /*Runs all registered tests*/
  void runUnitTests();

  /**************************************************************************/
  typedef struct test_result{
    test_result(bool s, const char* f, int l){
      success = s;
      file = f;
      line = l;
    }
    bool success;
    const char* file;
    int   line;
  }test_result;

  /**************************************************************************/
  typedef struct test_function{
    /*Function pointer to testfunction*/
    test_result (*func)(void);

    /*Description of the test*/
    char name[1024];

    /*Pointer to next testfunction struct*/
    test_function* next;

    void test(){
      test_result result = (*func)();
      if(result.success == false){
        warning("Test           : %s failed", name);
        warning("               : %s line %d", result.file, result.line);
      }else{
        message("Test           : %s success", name);
      }
    }
  }test_function;

  /**************************************************************************/
  void registerTestFunction(test_result (*func)(void),
                            const char* descr,
                            test_function** pptest);

  /**************************************************************************/
  /*Macro for registering a testfunction*/
#define REGISTER_FUNCTION(func, node)           \
  registerTestFunction(&func, #func, node);
}

#endif/*UNITTEST_HPP*/
