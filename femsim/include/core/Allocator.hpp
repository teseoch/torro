/* Copyright (C) 2013-2019 by Mickeal Verschoor*/

#ifndef ALLOCATOR_HPP
#define ALLOCATOR_HPP

namespace tsl{
  /**************************************************************************/
  /*Allocator for managing allocation, deallocation, construction and
    destruction of objects of type T.*/
  template<class T>
  class Allocator{
  public:
    /**************************************************************************/
    typedef T value_type;

    /**************************************************************************/
    typedef T* pointer_type;

    /**************************************************************************/
    typedef const T* const_pointer;

    /**************************************************************************/
    typedef T& reference;

    /**************************************************************************/
    typedef const T& const_reference;

    /**************************************************************************/
    template<class U>
    struct rebind{
      typedef Allocator<U> other;
    };

    /**************************************************************************/
    Allocator(){
      nAllocated = 0;
    }

    /**************************************************************************/
    ~Allocator(){
      if(nAllocated != 0){
        message("Allocator destructor %s %d still allocated",
                __PRETTY_FUNCTION__, nAllocated);
      }
    }

    /**************************************************************************/
    __attribute__((always_inline)) T* allocate(long unsigned int n){
      nAllocated++;
      return (T*)malloc(n * sizeof(T));
    }

    /**************************************************************************/
    __attribute__((always_inline)) void deallocate(T* p){
      free(p);
      nAllocated--;
    }

    /**************************************************************************/
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n){
      T* p = allocate(n);
      for(long unsigned int i=0;i<n;i++){
        construct(&(p[i]));
      }
      return p;
    }

    /**************************************************************************/
    template<class A>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a){
      T* p = allocate(n);
      for(long unsigned int i=0;i<n;i++){
        construct(&(p[i]), a);
      }
      return p;
    }

    /**************************************************************************/
    template<class A, class B>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a, B b){
      T* p = allocate(n);
      for(long unsigned int i=0;i<n;i++){
        construct(&(p[i]), a, b);
      }
      return p;
    }

    /**************************************************************************/
    template<class A, class B, class C>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a, B b, C c){
      T* p = allocate(n);
      for(long unsigned int i=0;i<n;i++){
        construct(&(p[i]), a, b, c);
      }
      return p;
    }

    /**************************************************************************/
    __attribute__((always_inline)) void construct(T* p){
      new((void*)p) T();
    }

    /**************************************************************************/
    template<class A>
    __attribute__((always_inline)) void construct(T* p, A a){
      new((void*)p) T(a);
    }

    /**************************************************************************/
    template<class A, class B>
    __attribute__((always_inline)) void construct(T* p, A a, B b){
      new((void*)p) T(a, b);
    }

    /**************************************************************************/
    template<class A, class B, class C>
    __attribute__((always_inline)) void construct(T* p, A a, B b, C c){
      new((void*)p) T(a, b, c);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void construct(T* p, const T& val){
      new((void*)p) T(val);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void destroy(T* p){
      ((T*)p)->~T();
    }

    /**************************************************************************/
    template<class U>
    __attribute__((always_inline)) void destroy(U* p){
      p->~U();
    }

    /**************************************************************************/
    __attribute__((always_inline)) void destroyAndDeallocate(T* p){
      destroy(p);
      deallocate(p);
    }

  protected:
    /**************************************************************************/
    int nAllocated;
  };

  /**************************************************************************/
  template<class T>
  class StaticAllocator{
  public:
    /**************************************************************************/
    typedef T value_type;

    /**************************************************************************/
    typedef T* pointer_type;

    /**************************************************************************/
    typedef const T* const_pointer;

    /**************************************************************************/
    typedef T& reference;

    /**************************************************************************/
    typedef const T& const_reference;

    /**************************************************************************/
    template<class U>
    struct rebind{
      typedef StaticAllocator<U> other;
    };

    /**************************************************************************/
    __attribute__((always_inline)) T* allocate(long unsigned int n){
      return alloc.allocate(n);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void deallocate(T* p){
      alloc.deallocate(p);
    }

    /**************************************************************************/
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n){
      return alloc.allocateAndConstruct(n);
    }

    /**************************************************************************/
    template<class A>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a){
      return alloc.allocateAndConstruct(n, a);
    }

    /**************************************************************************/
    template<class A, class B>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a, B b){
      return alloc.allocateAndConstruct(n, a, b);
    }

    /**************************************************************************/
    template<class A, class B, class C>
    __attribute__((always_inline)) T* allocateAndConstruct(long unsigned int n, A a, B b, C c){
      return alloc.allocateAndConstruct(n, a, b, c);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void construct(T* p){
      alloc.construct(p);
    }

    /**************************************************************************/
    template<class A>
    __attribute__((always_inline)) void construct(T* p, A a){
      alloc.construct(p, a);
    }

    /**************************************************************************/
    template<class A, class B>
    __attribute__((always_inline)) void construct(T* p, A a, B b){
      alloc.construct(p, a, b);
    }

    /**************************************************************************/
    template<class A, class B, class C>
    __attribute__((always_inline)) void construct(T* p, A a, B b, C c){
      alloc.construct(p, a, b, c);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void construct(T* p, const T& val){
      alloc.construct(p, val);
    }

    /**************************************************************************/
    __attribute__((always_inline)) void destroy(T* p){
      alloc.destroy(p);
    }

    /**************************************************************************/
    template<class U>
    __attribute__((always_inline)) void destroy(U* p){
      alloc.destroy();
    }

    /**************************************************************************/
    __attribute__((always_inline)) void destroyAndDeallocate(T* p){
      alloc.destroyAndDeallocate(p);
    }

  protected:
    /**************************************************************************/
    static Allocator<T> alloc;
  };

  template<class T>
  Allocator<T> StaticAllocator<T>::alloc;

}

#endif/*ALLOCATOR_HPP*/
